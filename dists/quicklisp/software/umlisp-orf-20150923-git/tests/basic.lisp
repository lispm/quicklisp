;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: umlisp -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          basic.lisp
;;;; Purpose:       Basic tests for UMLisp
;;;; Author:        Kevin M. Rosenberg
;;;; Date Started:  May 2003
;;;;
;;;; $Id$
;;;;
;;;; This file, part of UMLisp, is
;;;;    Copyright (c) 2000-2002 by Kevin M. Rosenberg, M.D.
;;;;
;;;; UMLisp users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License.
;;;; *************************************************************************

(in-package #:umlisp-tests)

(deftest qs.1 (umlisp::query-string mrcon (cui lui))
  "select CUI,LUI from MRCON")

(deftest qs.1e (umlisp::query-string-eval 'mrcon '(cui lui))
  "select CUI,LUI from MRCON")

(deftest qs.2 (umlisp::query-string mrcon (cui lui) 0)
  "select CUI,LUI from MRCON and KCUILRL<=0")

(deftest qs.2e (umlisp::query-string-eval 'mrcon '(cui lui) 0)
  "select CUI,LUI from MRCON and KCUILRL<=0")

(deftest qs.3 (umlisp::query-string mrcon (cui lui) nil cui 5)
  "select CUI,LUI from MRCON where CUI=5")

(deftest qs.3e (umlisp::query-string-eval 'mrcon '(cui lui) nil 'cui 5)
  "select CUI,LUI from MRCON where CUI=5")

(deftest qs.4 (umlisp::query-string mrcon (cui lui) nil kpfstr "Abc")
  "select CUI,LUI from MRCON where KPFSTR='Abc'")

(deftest qs.4e (umlisp::query-string-eval 'mrcon '(cui lui) nil 'kpfstr "Abc")
  "select CUI,LUI from MRCON where KPFSTR='Abc'")

(deftest qs.5 (umlisp::query-string mrcon (cui lui) 2 cui 5 :single t)
  "select CUI,LUI from MRCON where CUI=5 and KCUILRL<=2 limit 1")

(deftest qs.5e (umlisp::query-string-eval 'mrcon '(cui lui) 2 'cui 5 :single t)
  "select CUI,LUI from MRCON where CUI=5 and KCUILRL<=2 limit 1")

(deftest qs.6 (umlisp::query-string mrdef (sab def) 2 cui 39 :lrl ksrl :single t)
  "select SAB,DEF from MRDEF where CUI=39 and KSRL<=2 limit 1")

(deftest qs.6e (umlisp::query-string-eval 'mrdef '(sab def) 2 'cui 39 :lrl 'ksrl :single t)
  "select SAB,DEF from MRDEF where CUI=39 and KSRL<=2 limit 1")

(deftest qs.7 (umlisp::query-string mrdef (sab def) 2 cui 39 :lrl ksrl :order (cui asc))
  "select SAB,DEF from MRDEF where CUI=39 and KSRL<=2 order by CUI asc")

(deftest qs.7e (umlisp::query-string-eval 'mrdef '(sab def) 2 'cui 39 :lrl 'ksrl :order '(cui asc))
  "select SAB,DEF from MRDEF where CUI=39 and KSRL<=2 order by CUI asc")

(deftest qs.8 (umlisp::query-string mrdef (sab def) 2 cui 39 :lrl ksrl
                                    :order (cui asc def desc))
  "select SAB,DEF from MRDEF where CUI=39 and KSRL<=2 order by CUI asc,DEF desc")

(deftest qs.8e (umlisp::query-string-eval 'mrdef '(sab def) 2 'cui 39 :lrl 'ksrl
                                    :order '(cui asc def desc))
  "select SAB,DEF from MRDEF where CUI=39 and KSRL<=2 order by CUI asc,DEF desc")

(deftest ui.1 (umlisp::parse-cui "C0002341") 2341)
(deftest ui.2 (umlisp::parse-lui "L0002341") 2341)
(deftest ui.3 (umlisp::parse-sui "S0000000") 0)
(deftest ui.4 (umlisp::parse-tui "T123") 123)
(deftest ui.5 (fmt-cui 2341) "C0002341")
(deftest ui.6 (fmt-lui 2341) "L0002341")
(deftest ui.7 (fmt-sui 2341) "S0002341")
(deftest ui.8 (fmt-tui 231) "T231")
(deftest ui.9 (fmt-tui 231) "T231")
(deftest ui.10 (fmt-eui 231) "E0000231")
(deftest ui.11 (umlisp::make-cuisui 5 11) 50000011)
(deftest ui.12 (umlisp::decompose-cuisui 50000011) 5 11)
(deftest ui.13 (umlisp::parse-eui "E00002311") 2311)
(deftest ui.14 (umlisp::parse-lui "1234") 1234)
(deftest ui.15 (umlisp::parse-lui 1234) 1234)

(defun f2 (&key (srl *current-srl*))
  "Return list of all ucon's"
  (umlisp::with-umlisp-query ('mrcon (cui kpfstr kcuilrl) srl nil nil)
    (make-instance 'ucon :cui (ensure-integer cui) :pfstr kpfstr
                   :lrl (ensure-integer kcuilrl))))

(defun f1 (&key (srl *current-srl*))
  "Return list of all ucon's"
  (umlisp::with-sql-connection (db)
    (clsql:map-query
     'list
     #'(lambda (cui pfstr cuilrl)
         (make-instance 'ucon :cui (ensure-integer cui)
                        :pfstr pfstr
                        :lrl (ensure-integer cuilrl)))
     (umlisp::query-string 'mrcon '(cui kpfstr kcuilrl) srl nil nil)
     :database db)))
