(in-package :cl-user)

(asdf:defsystem #:flac-parser
  :description "A parser for FLAC audio files."
  :author ("Michael Fiano <mail@michaelfiano.com>"
           "Peter Keller <psilord@cs.wisc.edu>")
  :maintainer "Michael Fiano <mail@michaelfiano.com>"
  :license "MIT"
  :homepage "https://github.com/mfiano/flac-parser"
  :bug-tracker "https://github.com/mfiano/flac-parser/issues"
  :source-control (:git "git@github.com:mfiano/flac-parser.git")
  :version "1.0.0"
  :encoding :utf-8
  :long-description #.(uiop:read-file-string
                       (uiop/pathname:subpathname *load-pathname* "README.md"))
  :depends-on (#:alexandria
               #:fast-io
               #:babel
               #:bit-smasher)
  :pathname "src"
  :serial t
  :components
  ((:file "package")
   (:file "definitions")
   (:file "common")
   (:file "metadata")
   (:file "parser")
   (:file "type-check-rules")
   (:file "type-check")
   (:file "flac-file")
   (:file "documentation")))
