;;;; -*- Mode: Lisp -*-

;;;; new-op.asd --
;;;; Please see file COPYING for copyright and licensing information.

(asdf:defsystem :new-op
  :author "Marco Antoniotti"

  :license "BSD"
  
  :description "The NEW-OP System.

A (not so) new NEW operator that subsumes all the 'make-...' functions
and methods of Common Lisp."

  :components ((:file "new-op-pkg")
               (:file "new-op"
                :depends-on ("new-op-pkg"))))

;;;; end of file -- new-op.asd --
