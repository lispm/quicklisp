;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp -*-

(asdf:defsystem "simpsamp"
  :version "0.1"
  :maintainer "J.P. Larocque"
  :author "J.P. Larocque"
  :licence "ISC-style permissive"
  :description "Fast algorithms for simple random sampling from sets,
without replacement."
  :components (;; Core algorithms, as macros.
	       (:file "core"
                :depends-on ("package"))
	       ;; Input iterators, as macros.
	       (:file "input"
		:depends-on ("package"))
	       ;; Output accumulators, as macros.
	       (:file "output"
		:depends-on ("package"))
	       ;; Output via looping macros.
	       (:file "out-do"
		:depends-on ("core"
			     "input"
			     "unbounded-iterator"
			     "package"))
	       ;; Output via mapping (for effect).
	       (:file "out-map"
		:depends-on ("out-do"
			     "package"))
	       ;; Output as lists.
	       (:file "out-list"
		:depends-on ("out-do"
			     "output"
			     "unbounded-iterator"
			     "package"))
	       ;; Output as vectors.
	       (:file "out-vector"
		:depends-on ("core"
			     "input"
			     "output"
			     "out-do"
			     "package"))
	       ;; Output as hash-tables.
	       (:file "out-hash-table"
		:depends-on ("output"
			     "out-do"
			     "package"))
	       ;; Dependency hack; see comment in file.
	       (:file "unbounded-iterator"
		:depends-on ("core"
			     "package"))
	       ;; Generic functions on sequences.
	       (:file "generic-functions"
		:depends-on ("out-map"
			     "out-list"
			     "out-vector"
			     "package"))
	       ;; Testing.
	       (:file "test"
		:depends-on ("test-core"
			     "test-outputs"
			     "package"))
	       ;; Testing of the core algorithms.
	       (:file "test-core"
		:depends-on ("core"
			     "input"))
	       ;; Testing of the output methods.
	       (:file "test-outputs"
		:depends-on ("output"
			     "out-do"
			     "out-map"
			     "out-list"
			     "out-vector"
			     "out-hash-table"
			     "unbounded-iterator"
			     "package"))
	       ;; Package definition.
               (:file "package"))
  :depends-on ((:version "jpl-util" "0.2")))
