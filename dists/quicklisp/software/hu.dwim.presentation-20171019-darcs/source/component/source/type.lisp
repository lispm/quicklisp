;;; -*- mode: Lisp; Syntax: Common-Lisp; -*-
;;;
;;; Copyright (c) 2009 by the authors.
;;;
;;; See LICENCE for details.

(in-package :hu.dwim.presentation)

;;;;;;
;;; symbol/type-name/inspector

(def (component e) symbol/type-name/inspector (t/alternator/inspector)
  ())
