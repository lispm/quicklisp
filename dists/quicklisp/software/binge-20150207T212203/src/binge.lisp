;;;  binge.lisp --- functions exported from the binge package

;;;  Copyright (C) 2004 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-604606A9751978C39182EFA55FC1F022.lisp,v 1.19 2004/12/16 11:55:31 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-604606A9751978C39182EFA55FC1F022.lisp,v 1.19 2004/12/16 11:55:31 wcp Exp $")

(in-package :binge)

(defvar *gcc-builtin-va_list*
  (make-instance 'binge-decl:c-typedef-declaration
		 :name "__builtin_va_list"
		 :type :vararg
		 :documentation
		 "In gcc lingo this is the equivalent of \"...\"")
  "If you parse gcc header files you might need this declaration
added to your namespace.")

(defgeneric make-ffi (output namespace &key constants types functions variables backend)
  (:documentation
   "Write a FFI to OUTPUT from C declarations found in NAMESPACE.
Select those you need sepcifying CONSTANTS, TYPES (struct, union,
enum or typedef), FUNCTIONS and VARIABLES.  If any of them is
given the value T, all the declarations in that category are
written."))

;; Here I don't want to bother the user to specify what is a struct,
;; union or a typedef.  He just tells the name in TYPES and this
;; method will try to get anything by that name.
(defun gather-all-requested-declarations (namespace constants types functions variables)
  ;; if nothing was specified, do everything
  (when (and (not constants) (not types) (not functions) (not variables))
    (setf constants t
	  types t
	  functions t
	  variables t))
  (flet ((lookup (finder desc list)
	   (loop
	      for item in list
	      for obj = (if (listp item)
			    (destructuring-bind (name &key doc alt lib) item
			      (let ((obj (funcall finder namespace name)))
				(when obj
				  (when doc
				    (setf (c-documentation obj) doc))
				  (when alt
				    (setf (c-alternate-name obj) alt))
				  (when lib
				    (setf (c-library obj) lib)))
				obj))
			      (funcall finder namespace item))
	      if obj
		do (setf (c-required obj) t) and
	        collect obj
	      else
	        do (warn "~A ~S is not among the declared ones" desc item)))
	 ;; Here we assume that struct are in the same namespace as
	 ;; unions and enum.  This assumption should be removed one
	 ;; day -wcp11/3/04.
	 (find-types (list)
	   (loop
	      for name in list
	      for struct = (find-c-struct namespace name)
	      for type = (find-c-typedef namespace name)
	      when struct
		do (setf (c-required struct) t) and
	        collect struct
	      when type
		do (setf (c-required type) t) and
	        collect type
	      unless (or struct type)
	        do (warn "type ~S is not among the declared ones" name))))
    (let ((const-decls (if (eq constants t)
			   (get-all-c-constants namespace)
			   (lookup #'find-c-constant "constant" constants)))
	  (type-decls (if (eq types t)
			  (append (get-all-c-typedefs namespace)
				  (get-all-c-structs namespace))
			  (find-types types)))
	  (func-decls (if (eq functions t)
			  (get-all-c-functions namespace)
			  (lookup #'find-c-function "function" functions)))
	  (var-decls (if (eq variables t)
			 (get-all-c-variables namespace)
			 (lookup #'find-c-variable "variable" variables))))
      (append const-decls type-decls var-decls func-decls))))

(defmethod make-ffi ((output stream) namespace &key constants types functions variables (backend *backend*))
  (let* ((binge-compiler:*backend* backend)
	 (decls (gather-all-requested-declarations namespace constants types functions variables))
	 (all-decls (complete-declarations decls namespace)))
    (write-package output all-decls)
    ;; This is necessary to accomodate SBCL and its useless defconstant
    (format output-stream
	    "(defmacro define-C-constant (name value &rest etc)~@
  #+sbcl (list* 'defvar name value etc)~@
  #-sbcl (list* 'defconstant name value etc))~2%")
    (write-declarations output all-decls))
  (values))

(defmethod make-ffi ((output pathname) namespace &key constants types functions variables backend)
  (with-open-file (out output :direction :output :if-exists :rename)
    (binge-backend:write-prologue out *backend*)
    (make-ffi out namespace
	      :constants constants
	      :types types
	      :functions functions
	      :variables variables
	      :backend (or backend
			   (make-instance (class-of *backend*)
					  :package (string-upcase (pathname-name output)))))
    (binge-backend:write-epilogue out *backend*)))

(defmethod make-ffi ((output (eql t)) namespace &key constants types functions variables backend)
  (make-ffi *standard-output* namespace
	    :constants constants
	    :types types
	    :functions functions
	    :variables variables
	    :backend backend))

(defmethod make-ffi ((output (eql nil)) namespace &key constants types functions variables backend)
  (with-output-to-string (string-stream)
    (make-ffi string-stream namespace
	      :constants constants
	      :types types
	      :functions functions
	      :variables variables
	      :backend backend)))
