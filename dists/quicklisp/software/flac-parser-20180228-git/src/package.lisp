(in-package :cl-user)

(defpackage #:flac-parser
  (:use #:cl
        #:alexandria)
  (:export #:open-flac))
