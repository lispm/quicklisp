;;;; -*- Mode: Lisp; indent-tabs-mode: nil -*-

(in-package :tcr.extensible-sequences)

;;; Definitions

(deftype function-designator ()
  "A function designator; either a globally fbound symbol, or a function."
  '(or symbol function))

(defun copy-seq (sequence)
  "Return a shallow copy of SEQUENCE."
  (subseq sequence 0))

(defun count (item sequence &key (start 0) end from-end key test test-not)
  "Return the number of elements in SEQUENCE satisfying a test with ITEM,
   which defaults to EQL."
  (declare (optimize speed))
  (check-sequence-bounds sequence start end)
  (let ((key  (canonicalize-key key))
        (test (canonicalize-test test test-not))
        (count 0))
    (declare (sequence-length count))
    (dosequence (elt sequence count :start start :end end :from-end from-end)
      (when (funcall test item (funcall key elt))
        (incf count)))))

(define-api count-if
    (predicate sequence &key start end from-end key)
    (function-designator sequence &key
                         (:start sequence-index)
                         (:end (or null sequence-index))
                         (:from-end t)
                         (:key function-designator)
      => sequence-length)
  "Return the number of elements in SEQUENCE satisfying PREDICATE."
  (declare (optimize speed))
  (check-sequence-bounds sequence start end)
  (let ((key (canonicalize-key key))
        (predicate (ensure-function predicate))
        (count 0))
    (declare (sequence-length count))
    (dosequence (elt sequence count :start start :end end :from-end from-end)
      (when (funcall predicate (funcall key elt))
        (incf count)))))

(defun count-if-not (predicate sequence &key (start 0) end from-end key)
  "Return the number of elements in SEQUENCE not satisfying PREDICATE."
  (declare (optimize speed))
  (check-sequence-bounds sequence start end)
  (let ((predicate (ensure-function predicate)))
    (declare (type (function (t) t) predicate))
    (count-if (complement predicate) sequence
              :start start :end end :from-end from-end :key key)))

(define-api elt
    (sequence index)
    (sequence sequence-index => t)
  "Returns the element of SEQUENCE specified by INDEX."
  (declare (optimize speed))
  (dosequence ((elt idx) sequence)
    (when (= idx index)
      (return-from elt elt)))
  (error 'sequence-index-error
         :sequence sequence :index index))

(define-api (setf elt)
    (new-value sequence index)
    (t sequence sequence-index => t)
  "Modifies SEQUENCE at INDEX."
  (declare (optimize speed))
  (dosequence ((_ idx) sequence nil :place ptr)
    (when (= idx index)
      (return-from elt (setf (ptr) new-value))))
  (error 'sequence-index-error
         :sequence sequence :index index))

(defun fill (sequence item &key (start 0) end)
  (declare (optimize speed))
  (check-sequence-bounds sequence start end)
  (dosequence (_ sequence sequence :start start :end end :place elt-ptr)
    (setf (elt-ptr) item)))

(defun find (item sequence &key (start 0) end from-end key test test-not)
  "Returns the first (or, if FROM-END is non-nil, last) element in SEQUENCE
   which satisfies a test with ITEM, which defaults to EQL."
  (declare (optimize speed))
  (check-sequence-bounds sequence start end)
  (let ((key  (canonicalize-key key))
        (test (canonicalize-test test test-not))
        (count 0))
    (declare (sequence-length count))
    (dosequence (elt sequence count :start start :end end :from-end from-end)
      (when (funcall test item (funcall key elt))
        (return elt)))))

(defun find-if (predicate sequence &key (start 0) end from-end key)
  "Returns the first (or, if FROM-END is non-nil, last) element in SEQUENCE
   which satisfies PREDICATE."
  (declare (optimize speed))
  (check-sequence-bounds sequence start end)
  (let ((key (canonicalize-key key))
        (predicate (ensure-function predicate)))
    (dosequence (elt sequence nil :start start :end end :from-end from-end)
      (when (funcall predicate (funcall key elt))
        (return elt)))))

(defun find-if-not (predicate sequence &key (start 0) end from-end key)
  "Returns the first (or, if FROM-END is non-nil, last) element in SEQUENCE
   which does not satisfy PREDICATE."
  (declare (optimize speed))
  (check-sequence-bounds sequence start end)
  (let ((predicate (ensure-function predicate)))
    (declare (type (function (t) t) predicate))
    (find-if (complement predicate) sequence
             :start start :end end :from-end from-end :key key)))

(define-api length
    (sequence)
    (sequence => sequence-length)
  "Returns an integer that is the length of SEQUENCE."
  (declare (optimize speed))
  (let ((length 0))
    (declare (sequence-length length))
    (dosequence (_ sequence length)
      (incf length))))

(defun nreverse (sequence)
  "Destructively reverse SEQUENCE."
  (declare (optimize speed))
  (with-sequence-iterator (forward sequence :place forward-ptr)
    (with-sequence-iterator (backward sequence :place backward-ptr :from-end t)
      (do-sequence-iterators* (((__ forward-idx) forward)
                               ((_ backward-idx) backward))
        (if (< forward-idx backward-idx)
            (rotatef (forward-ptr) (backward-ptr))
            (return sequence))))))

(defun reverse (sequence)
  "Return a new sequence containing the same elements but in reverse order."
  (declare (optimize speed))
  (let ((length (length sequence)))
    (let ((result (make-sequence-like sequence length)))
      (dosequences* ((_   result   result :place result-ptr)
                     (elt sequence (assert nil) :from-end t))
        (setf (result-ptr) elt)))))

(defun subseq (seq start &optional end)
  "Return a copy of a subsequence of SEQUENCE starting with element number
   START and continuing to the end of SEQUENCE or the optional END."
  (declare (optimize speed))
  (check-sequence-bounds seq start end)
  (let ((result (make-sequence-like seq (- end start))))
    (dosequences* ((_   result result       :place result-ptr)
                   (elt seq    (assert nil) :start start :end end))
      (setf (result-ptr) elt))))

(defun (setf subseq) (new-subseq seq start &optional end)
  "Destructively replace the elements of SEQ between the bounding indices START
   and END with elements from NEW-SUBSEQ."
  (declare (optimize speed))
  (check-sequence-bounds seq start end)
  ;; Regardless of which runs out first, we want to return NEW-SUBSEQ
  (dosequences* ((new-elt new-subseq new-subseq)
                 (_ seq new-subseq :start start :end end :place elt-ptr))
    (setf (elt-ptr) new-elt)))

;;; FIXME: SEQUENCE-ITERATORS:COPY-SEQUENCE-ITERATOR is not yet implemented.

#+nil
(defun search (sequence1 sequence2 &key start1 end1 start2 end2 from-end test key)
  "Returns the index of the leftmost element of the first (or, if FROM-END is non-nil,
   last) subsequence of SEQUENCE2 that matches SEQUENCE1."
  (check-sequence-bounds sequence1 start1 end1)
  (check-sequence-bounds sequence2 start2 end2)
  (setq test (if test (ensure-function test) #'eql))
  (setq key  (if key  (ensure-function key)  #'identity))
  (flet ((compare (e1 e2)
           (funcall test (funcall key e1) (funcall key e2))))
    (with-sequence-iterator (it1 sequence1 :start start1 :end end1 :from-end from-end)
      (with-sequence-iterator (it2 sequence2 :start start2 :end end2 :from-end from-end)
        (multiple-value-bind (more? elt1 idx1) (it1)
          (unless more?
            (return-from my-search nil))
          (do-sequence-iterator ((elt2 idx2) it2)
            (when (compare elt1 elt2)
              (do-sequence-iterators*
                  (((elt1 idx1) (copy-sequence-iterator it1) (return-from my-search idx2))
                   ((elt2 idx2) (copy-sequence-iterator it2)))
                (unless (compare elt1 elt2)
                  (return)))))
          nil)))))

;;; FIXME: These are just stubs that just call the CL equivalent. Fix that.

;;; This doesn't handle &rest, but it's "throwaway code"
(defmacro define-cl-stub (name lambda-list)
  `(defun ,name ,(cl:mapcar (lambda (arg)
                              (cond ((eq arg 'test) '(test #'eql))
                                    ((eq arg 'key) '(key #'identity))
                                    ((cl:search "START" (symbol-name arg)) `(,arg 0))
                                    (t arg)))
                            lambda-list)
     (,(find-symbol (symbol-name name) :cl)
       ,@ (loop for arg in lambda-list with reached-keys = nil
            unless (cl:member arg lambda-list-keywords)
            if reached-keys
            nconc (list (intern (symbol-name arg) :keyword) arg)
            else collect arg
            when (eq arg '&key) do (setf reached-keys t)))))

(defun concatenate (result-type &rest sequences)
  (declare (dynamic-extent sequences))
  (apply 'cl:concatenate result-type sequences))

(define-cl-stub delete (item sequence &key from-end test test-not start end key))

(define-cl-stub delete-duplicates (sequence &key from-end test test-not start end key))

(define-cl-stub delete-if (predicate sequence &key from-end start end key))

(define-cl-stub delete-if-not (predicate sequence &key from-end start end key))

(defun make-sequence (result-type size &key (initial-element nil iep))
  (if iep
      (cl:make-sequence result-type size :initial-element initial-element)
      (cl:make-sequence result-type size)))

(defun map (result-type function &rest sequences)
  (declare (dynamic-extent sequences))
  (apply 'cl:map result-type function sequences))

(defun map-into (result-sequence function &rest sequences)
  (declare (dynamic-extent sequences))
  (apply 'cl:map-into result-sequence function sequences))

(define-cl-stub merge (result-type sequence1 sequence2 predicate &key key))

(define-cl-stub mismatch (sequence1 sequence2 &key from-end test test-not key start1 start2 end1 end2))

(define-cl-stub nsubstitute (new old sequence &key from-end test test-not start end count key))

(define-cl-stub nsubstitute-if (new predicate sequence &key from-end start end count key))

(define-cl-stub nsubstitute-if-not (new predicate sequence &key from-end start end count key))

(define-cl-stub position (item sequence &key from-end test test-not start end key))

(define-cl-stub position-if (predicate sequence &key from-end start end key))

(define-cl-stub position-if-not (item sequence &key from-end start end key))

(define-cl-stub reduce (function sequence &key key from-end start end initial-value))

(define-cl-stub remove (item sequence &key from-end test test-not start end key))

(define-cl-stub remove-duplicates (sequence &key from-end test test-not start end key))

(define-cl-stub remove-if (predicate sequence &key from-end start end key))

(define-cl-stub remove-if-not (predicate sequence &key from-end start end key))

(define-cl-stub replace (sequence1 sequence2 &key start1 end1 start2 end2))

(define-cl-stub search (sequence1 sequence2 &key start1 end1 start2 end2 from-end test key))

(define-cl-stub sort (sequence predicate &key key))

(define-cl-stub stable-sort (sequence predicate &key key))

(define-cl-stub substitute (new old sequence &key from-end test test-not start end count key))

(define-cl-stub substitute-if (new predicate sequence &key from-end start end count key))

(define-cl-stub substitute-if-not (new predicate sequence &key from-end start end count key))
