;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: umlisp -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:     sql-classes.lisp
;;;; Purpose:  Routines for reading UMLS objects from SQL database
;;;; Author:   Kevin M. Rosenberg
;;;; Created:  Apr 2000
;;;;
;;;; $Id$
;;;;
;;;; This file, part of UMLisp, is
;;;;    Copyright (c) 2000-2003 by Kevin M. Rosenberg, M.D.
;;;;
;;;; UMLisp users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License.
;;;; *************************************************************************

(in-package #:umlisp-orf)


(defvar *current-srl* nil)
(defun current-srl ()
  *current-srl*)
(defun current-srl! (srl)
  (setq *current-srl* srl))

(defmacro query-string (table fields &optional srl where-name where-value
                        &key (lrl "KCUILRL") single distinct order like)
  (let* ((%%fields (format nil "select ~A~{~:@(~A~)~^,~} from ~:@(~A~)"
                           (if distinct "distinct " "") fields table))
         (%%order (if order (format nil " order by ~{~:@(~A~) ~(~A~)~^,~}"
                                    order)
                      ""))
         (%%lrl (format nil " and ~:@(~A~)<=" lrl))
         (%%where (when where-name
                    (format nil " where ~:@(~A~)~A" where-name
                          (if like " like " "")))))
    `(concatenate
      'string
      ,%%fields
      ,@(when %%where (list %%where))
      ,@(when %%where
              `((typecase ,where-value
                  (fixnum
                   (concatenate 'string "='" (prefixed-fixnum-string ,where-value #\0 10) "'"))
                  (number
                   (concatenate 'string "='" (write-to-string ,where-value) "'"))
                  (null
                   " is null")
                  (t
                   (format nil ,(if like "'%~A%'" "='~A'") ,where-value)))))
      (if ,srl (concatenate 'string ,%%lrl (write-to-string ,srl)) "")
      ,@(when %%order (list %%order))
      ,@(when single (list " limit 1")))))

(defun query-string-eval (table fields &optional srl where-name where-value
                          &key (lrl "KCUILRL") single distinct order like)
  (concatenate
   'string
   (format nil "select ~A~{~:@(~A~)~^,~} from ~:@(~A~)"
           (if distinct "distinct " "") fields table)
   (if where-name (format nil " where ~:@(~A~)" where-name) "")
   (if where-name
       (format nil
               (typecase where-value
                 (number "=~D")
                 (null " is null")
                 (t
                  (if like " like '%~A%""='~A'")))
               where-value)
       "")
   (if srl (format nil " and ~:@(~A~)<=~D" lrl srl) "")
   (if order (format nil " order by ~{~:@(~A~) ~(~A~)~^,~}" order) "")
   (if single " limit 1" "")))


(defmacro umlisp-query (table fields srl where-name where-value
                     &key (lrl "KCUILRL") single distinct order like
                        (query-cmd 'mutex-sql-query))
  "Query the UMLisp database. Return a list of umlisp objects whose name
is OBJNAME from TABLE where WHERE-NAME field = WHERE-VALUE with FIELDS"
  `(,query-cmd
    (query-string ,table ,fields ,srl ,where-name ,where-value
     :lrl ,lrl :single ,single :distinct ,distinct :order ,order :like ,like)))

(defmacro umlisp-query-eval (table fields srl where-name where-value
                     &key (lrl "KCUILRL") single distinct order like)
  "Query the UMLisp database. Return a list of umlisp objects whose name
is OBJNAME from TABLE where WHERE-NAME field = WHERE-VALUE with FIELDS"
  `(mutex-sql-query
    (query-string-eval ,table ,fields ,srl ,where-name ,where-value
     :lrl ,lrl :single ,single :distinct ,distinct :order ,order :like ,like)))

;; only WHERE-VALUE and SRL are evaluated
(defmacro collect-umlisp-query ((table fields srl where-name where-value
                                    &key (lrl "KCUILRL") distinct single
                                    order like (query-cmd 'mutex-sql-query))
                                &body body)
  (let ((value (gensym))
        (r (gensym)))
    (if single
        `(let* ((,value ,where-value)
                (tuple (car (umlisp-query ,table ,fields ,srl ,where-name ,value
                                          :lrl ,lrl :single ,single
                                          :distinct ,distinct :order ,order
                                          :like ,like
                                          :query-cmd ,query-cmd))))
          ,@(unless where-name `((declare (ignore ,value))))
          (when tuple
                (destructuring-bind ,fields tuple
                  ,@body)))
        `(let ((,value ,where-value))
           ,@(unless where-name `((declare (ignore ,value))))
           (let ((,r '()))
             (dolist (tuple (umlisp-query ,table ,fields ,srl ,where-name ,value
                                          :lrl ,lrl :single ,single :distinct ,distinct
                                          :order ,order :like ,like))
               (push (destructuring-bind ,fields tuple ,@body) ,r))
             (nreverse ,r))
           #+ignore
           (loop for tuple in
                 (umlisp-query ,table ,fields ,srl ,where-name ,value
                               :lrl ,lrl :single ,single :distinct ,distinct
                               :order ,order :like ,like)
               collect (destructuring-bind ,fields tuple ,@body))))))

(defmacro collect-umlisp-query-eval ((table fields srl where-name where-value
                                         &key (lrl "KCUILRL") distinct single
                                         order like)
                                  &body body)
  (let ((value (gensym))
        (r (gensym))
        (eval-fields (cadr fields)))
    (if single
        `(let* ((,value ,where-value)
                (tuple (car (umlisp-query-eval ,table ,fields ,srl ,where-name ,value
                                               :lrl ,lrl :single ,single
                                               :distinct ,distinct :order ,order
                                               :like ,like))))
          (when tuple
            (destructuring-bind ,eval-fields tuple
              ,@body)))
        `(let ((,value ,where-value)
               (,r '()))
           (dolist (tuple (umlisp-query-eval ,table ,fields ,srl ,where-name ,value
                                             :lrl ,lrl :single ,single :distinct ,distinct
                                             :order ,order :like ,like))
             (push (destructuring-bind ,eval-fields tuple ,@body) ,r))
           (nreverse ,r)
           #+ignore
           (loop for tuple in
                 (umlisp-query-eval ,table ,fields ,srl ,where-name ,value
                                    :lrl ,lrl :single ,single :distinct ,distinct
                                    :order ,order :like ,like)
               collect (destructuring-bind ,eval-fields tuple ,@body))))))

;;;
;;; Read from SQL database

(defmacro ensure-cui-integer (cui)
  `(if (stringp ,cui)
    (setq ,cui (parse-cui ,cui))
    ,cui))

(defmacro ensure-lui-integer (lui)
  `(if (stringp ,lui)
    (setq ,lui (parse-lui ,lui))
    ,lui))

(defmacro ensure-sui-integer (sui)
  `(if (stringp ,sui)
    (setq ,sui (parse-sui ,sui))
    ,sui))

(defmacro ensure-tui-integer (tui)
  `(if (stringp ,tui)
    (setq ,tui (parse-tui ,tui))
    ,tui))

(defmacro ensure-eui-integer (eui)
  `(if (stringp ,eui)
    (setq ,eui (parse-eui ,eui))
    ,eui))

(defun find-ucon-cui (cui &key (srl *current-srl*))
  "Find ucon for a cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrcon (kpfstr kcuilrl) srl cui cui :single t)
    (make-instance 'ucon :cui cui :pfstr kpfstr
                   :lrl (ensure-integer kcuilrl))))

(defun find-ucon-cui-sans-pfstr (cui &key (srl *current-srl*))
  "Find ucon for a cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrcon (kcuilrl) srl cui cui :single t)
    (make-instance 'ucon :cui cui :lrl (ensure-integer kcuilrl)
                   :pfstr nil)))

(defun find-pfstr-cui (cui &key (srl *current-srl*))
  "Find preferred string for a cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrcon (kpfstr) srl cui cui :single t)
    kpfstr))

(defun find-ucon-lui (lui &key (srl *current-srl*))
  "Find list of ucon for lui"
  (ensure-lui-integer lui)
  (collect-umlisp-query (mrcon (cui kpfstr kcuilrl) srl lui lui
                            :distinct t)
    (make-instance 'ucon :cui (ensure-integer cui) :pfstr kpfstr
                   :lrl (ensure-integer kcuilrl))))

(defun find-ucon-sui (sui &key (srl *current-srl*))
  "Find list of ucon for sui"
  (ensure-sui-integer sui)
  (collect-umlisp-query (mrcon (cui kpfstr kcuilrl) srl sui sui :distinct t)
    (make-instance 'ucon :cui (ensure-integer cui) :pfstr kpfstr
                   :lrl (ensure-integer kcuilrl))))

(defun find-ucon-cuisui (cui sui &key (srl *current-srl*))
  "Find ucon for cui/sui"
  (ensure-cui-integer cui)
  (ensure-sui-integer sui)
  (when (and cui sui)
    (collect-umlisp-query (mrcon (kpfstr kcuilrl) srl kcuisui
                              (make-cuisui cui sui))
      (make-instance 'ucon :cui cui
                     :pfstr kpfstr
                     :lrl (ensure-integer kcuilrl)))))

(defun find-ucon-str (str &key (srl *current-srl*))
  "Find ucon that are exact match for str"
  (collect-umlisp-query (mrcon (cui kpfstr kcuilrl) srl str str :distinct t)
    (make-instance 'ucon :cui (ensure-integer cui) :pfstr kpfstr
                   :lrl (ensure-integer kcuilrl))))

(defun find-ucon-all (&key (srl *current-srl*))
  "Return list of all ucon's"
  (with-sql-connection (db)
    (clsql:map-query
     'list
     #'(lambda (tuple)
         (destructuring-bind (cui pfstr cuilrl) tuple
           (make-instance 'ucon :cui (ensure-integer cui)
                          :pfstr pfstr
                          :lrl (ensure-integer cuilrl))))
     (query-string mrcon (cui kpfstr kcuilrl) srl nil nil
                   :order (cui asc) :distinct t)
     :database db)))

(defun find-ucon-all2 (&key (srl *current-srl*))
  "Return list of all ucon's"
  (collect-umlisp-query (mrcon (cui kpfstr kcuilrl) srl nil nil :order (cui asc)
                            :distinct t)
    (make-instance 'ucon :cui (ensure-integer cui)
                   :pfstr kpfstr
                   :lrl (ensure-integer kcuilrl))))

(defun find-cui-ucon-all (&key (srl *current-srl*))
  "Return list of CUIs for all ucons"
  (collect-umlisp-query (mrcon (cui) srl nil nil :order (cui asc)
                               :distinct t)
                        cui))

(defun map-ucon-all (fn &key (srl *current-srl*))
  "Map a function over all ucon's"
  (with-sql-connection (db)
    (clsql:map-query
     nil
     #'(lambda (tuple)
         (destructuring-bind (cui pfstr cuilrl) tuple
           (funcall fn
                    (make-instance 'ucon :cui (ensure-integer cui)
                                   :pfstr pfstr
                                   :lrl (ensure-integer cuilrl)))))
     (query-string mrcon (cui kpfstr kcuilrl) srl nil nil :order (cui asc)
                   :distinct t)
     :database db)))


(defun find-udef-cui (cui &key (srl *current-srl*))
  "Return a list of udefs for cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrdef (sab def) srl cui cui :lrl "KSRL")
    (make-instance 'udef :sab sab :def def)))

(defun find-usty-cui (cui &key (srl *current-srl*))
  "Return a list of usty for cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrsty (tui sty) srl cui cui :lrl "KLRL")
    (make-instance 'usty :tui (ensure-integer tui) :sty sty)))

(defun find-usty-word (word &key (srl *current-srl*))
  "Return a list of usty that match word"
  (collect-umlisp-query (mrsty (tui sty) srl sty word :lrl klrl :like t
                            :distinct t)
    (make-instance 'usty :tui (ensure-integer tui) :sty sty)))

(defun find-urel-cui (cui &key (srl *current-srl*))
  "Return a list of urel for cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrrel (rel cui2 rela sab sl mg kpfstr2) srl cui1
                            cui :lrl "KSRL")
    (make-instance 'urel :cui1 cui :rel rel
                   :cui2 (ensure-integer cui2) :rela rela :sab sab :sl sl
                   :mg mg :pfstr2 kpfstr2)))

(defun find-cui2-urel-cui (cui &key (srl *current-srl*))
  "Return a list of urel for cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrrel (cui2) srl cui1
                               cui :lrl "KSRL")
                        cui2))

(defun find-urel-cui2 (cui2 &key (srl *current-srl*))
  "Return a list of urel for cui2"
  (ensure-cui-integer cui2)
  (collect-umlisp-query (mrrel (rel cui1 rela sab sl mg kpfstr2) srl cui2
                            cui2 :lrl "KSRL")
    (make-instance 'urel :cui2 cui2 :rel rel
                   :cui1 (ensure-integer cui1) :rela rela :sab sab :sl sl
                   :mg mg :pfstr2 kpfstr2)))

(defun find-ucon-rel-cui2 (cui2 &key (srl *current-srl*))
  (ensure-cui-integer cui2)
  (loop for cui in (remove-duplicates
                    (mapcar #'cui1 (find-urel-cui2 cui2 :srl srl)))
        collect (find-ucon-cui cui :srl srl)))

(defun find-ucoc-cui (cui &key (srl *current-srl*))
  "Return a list of ucoc for cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrcoc (cui2 soc cot cof coa kpfstr2) srl cui1
                            cui :lrl klrl :order (cof asc))
    (setq cui2 (ensure-integer cui2))
    (when (eql 0 cui2) (setq cui2 nil))
    (make-instance 'ucoc :cui1 cui :cui2 (ensure-integer cui2)
                   :soc soc :cot cot :cof (ensure-integer cof) :coa coa
                   :pfstr2 kpfstr2)))

(defun find-ucoc-cui2 (cui2 &key (srl *current-srl*))
  "Return a list of ucoc for cui2"
  (ensure-cui-integer cui2)
  (collect-umlisp-query (mrcoc (cui1 soc cot cof coa kpfstr2) srl cui2
                            cui2 :lrl klrl :order (cof asc))
    (when (zerop cui2) (setq cui2 nil))
    (make-instance 'ucoc :cui1 (ensure-integer cui1) :cui2 cui2
                   :soc soc :cot cot :cof (ensure-integer cof) :coa coa
                   :pfstr2 kpfstr2)))

(defun find-ucon-coc-cui2 (cui2 &key (srl *current-srl*))
  "List of ucon with co-occurance cui2"
  (ensure-cui-integer cui2)
  (mapcar
   #'(lambda (cui) (find-ucon-cui cui :srl srl))
   (remove-duplicates (mapcar #'cui1 (find-ucoc-cui2 cui2 :srl srl)))))

(defun find-ulo-cui (cui &key (srl *current-srl*))
  "Return a list of ulo for cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrlo (isn fr un sui sna soui) srl cui cui
                           :lrl "KLRL")
    (make-instance 'ulo :isn isn :fr (ensure-integer fr) :un un
                   :sui (ensure-integer sui) :sna sna :soui soui)))

(defun find-uatx-cui (cui &key (srl *current-srl*))
  "Return a list of uatx for cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mratx (sab rel atx) srl cui cui :lrl ksrl)
    (make-instance 'uatx :sab sab :rel rel :atx atx)))


(defun find-uterm-cui (cui &key (srl *current-srl*))
  "Return a list of uterm for cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrcon (lui lat ts kluilrl) srl cui cui
                            :lrl kluilrl :distinct t)
    (make-instance 'uterm :lui (ensure-integer lui) :cui cui
                   :lat lat :ts ts :lrl (ensure-integer kluilrl))))

(defun find-uterm-lui (lui &key (srl *current-srl*))
  "Return a list of uterm for lui"
  (ensure-lui-integer lui)
  (collect-umlisp-query (mrcon (cui lat ts kluilrl) srl lui lui
                             :lrl kluilrl :distinct t)
    (make-instance 'uterm :cui (ensure-integer cui) :lui lui
                   :lat lat :ts ts :lrl (ensure-integer kluilrl))))

(defun find-uterm-cuilui (cui lui &key (srl *current-srl*))
  "Return single uterm for cui/lui"
  (ensure-cui-integer cui)
  (ensure-lui-integer lui)
  (collect-umlisp-query (mrcon (lat ts kluilrl) srl kcuilui
                             (make-cuilui cui lui)
                             :lrl kluilrl :single t)
    (make-instance 'uterm :cui cui :lui lui :lat lat :ts ts
                   :lrl (ensure-integer kluilrl))))

(defun find-ustr-cuilui (cui lui &key (srl *current-srl*))
  "Return a list of ustr for cui/lui"
  (ensure-cui-integer cui)
  (ensure-lui-integer lui)
  (collect-umlisp-query (mrcon (sui stt str lrl) srl kcuilui
                            (make-cuilui cui lui) :lrl lrl)
    (make-instance 'ustr :sui (ensure-integer sui) :cui cui :lui lui
                   :cuisui (make-cuisui cui sui) :stt stt :str str
                   :lrl (ensure-integer lrl))))

(defun find-ustr-cuisui (cui sui &key (srl *current-srl*))
  "Return the single ustr for cuisui"
  (ensure-cui-integer cui)
  (ensure-sui-integer sui)
  (collect-umlisp-query (mrcon (lui stt str lrl) srl kcuisui
                            (make-cuisui cui sui) :lrl lrl :single t)
    (make-instance 'ustr :sui sui :cui cui :cuisui (make-cuisui cui sui)
                   :lui (ensure-integer lui) :stt stt :str str
                   :lrl (ensure-integer lrl))))

(defun find-ustr-sui (sui &key (srl *current-srl*))
  "Return the list of ustr for sui"
  (ensure-sui-integer sui)
  (collect-umlisp-query (mrcon (cui lui stt str lrl) srl sui sui
                            :lrl lrl)
    (make-instance 'ustr :sui sui :cui cui :stt stt :str str
                   :cuisui (make-cuisui (ensure-integer cui) sui)
                   :lui (ensure-integer lui) :lrl (ensure-integer lrl))))

(defun find-ustr-sab (sab &key (srl *current-srl*))
  "Return the list of ustr for sab"
  (collect-umlisp-query (mrso (kcuisui) srl sab sab :lrl srl)
    (let ((cuisui (ensure-integer kcuisui)))
      (apply #'find-ustr-cuisui
             (append
              (multiple-value-list (decompose-cuisui cuisui))
              (list :srl srl))))))

(defun find-ustr-all (&key (srl *current-srl*))
  "Return list of all ustr's"
    (with-sql-connection (db)
      (clsql:map-query
       'list
       #'(lambda (tuple)
           (destructuring-bind (cui lui sui stt lrl pfstr) tuple
             (make-instance 'ustr :cui (ensure-integer cui)
                            :lui (ensure-integer lui) :sui (ensure-integer sui)
                            :stt stt :str pfstr
                            :cuisui (make-cuisui (ensure-integer cui)
                                                 (ensure-integer sui))
                          :lrl (ensure-integer lrl))))
       (query-string mrcon (cui lui sui stt lrl kpfstr) srl nil nil :lrl lrl
                     :distinct t
                     :order (sui asc))
       :database db)))

(defun find-string-sui (sui &key (srl *current-srl*))
  "Return the string associated with sui"
  (ensure-sui-integer sui)
  (collect-umlisp-query (mrcon (str) srl sui sui :lrl lrl :single t)
    str))

(defun find-uso-cuisui (cui sui &key (srl *current-srl*))
  (ensure-sui-integer sui)
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrso (sab code srl tty) srl kcuisui
                           (make-cuisui cui sui) :lrl srl)
      (make-instance 'uso :sab sab :code code :srl srl :tty tty)))

(defun find-ucxt-cuisui (cui sui &key (srl *current-srl*))
  (ensure-cui-integer cui)
  (ensure-sui-integer sui)
  (collect-umlisp-query (mrcxt (sab code cxn cxl rnk cxs cui2 hcd rela xc)
                            srl kcuisui (make-cuisui cui sui) :lrl ksrl)
    (make-instance 'ucxt :sab sab :code code
                   :cxn (ensure-integer cxn) :cxl cxl :cxs cxs :hcd hcd
                   :rela rela :xc xc :rnk (ensure-integer rnk)
                   :cui2 (ensure-integer cui2))))

(defun find-usat-ui (cui &optional (lui nil) (sui nil) &key (srl *current-srl*))
  (ensure-cui-integer cui)
  (ensure-lui-integer lui)
  (ensure-sui-integer sui)
  (let ((ls "select CODE,ATN,SAB,ATV from MRSAT where "))
    (cond
      (sui (string-append ls "KCUISUI='"
                          (integer-string (make-cuisui cui sui) 14)
                          "'"))
      (lui (string-append ls "KCUILUI='"
                          (integer-string (make-cuilui cui lui) 14)
                          "' and sui='0'"))
      (t (string-append ls "cui='" (prefixed-fixnum-string cui nil 7)
                        "' and lui='0' and sui='0'")))
    (when srl
      (string-append ls " and KSRL<=" (prefixed-fixnum-string srl nil 3)))
    (loop for tuple in (mutex-sql-query ls) collect
          (destructuring-bind (code atn sab atv) tuple
            (make-instance 'usat :code code :atn atn :sab sab :atv atv)))))

(defun find-usty-tui (tui)
  "Find usty for tui"
  (ensure-tui-integer tui)
  (collect-umlisp-query (mrsty (sty) nil tui tui :single t)
    (make-instance 'usty :tui tui :sty sty)))

(defun find-usty-sty (sty)
  "Find usty for a sty"
  (collect-umlisp-query (mrsty (tui) nil sty sty :single t)
    (make-instance 'usty :tui (ensure-integer tui) :sty sty)))

(defun find-usty-all ()
  "Return list of usty's for all semantic types"
  (collect-umlisp-query (mrsty (tui) nil nil nil :distinct t)
    (find-usty-tui tui)))

(defun find-usab-all ()
  "Find usab for a key"
  (collect-umlisp-query (mrsab (vcui rcui vsab rsab son sf sver vstart vend imeta
                                  rmeta slc scc srl tfr cfr cxty ttyl atnl lat
                                  cenc curver sabin) nil nil nil)
    (make-instance 'usab :vcui (ensure-integer vcui)
                   :rcui (ensure-integer rcui) :vsab vsab :rsab rsab :son son
                   :sf sf :sver sver :vstart vstart :vend vend :imeta imeta
                   :rmeta rmeta :slc slc :scc scc  :srl (ensure-integer srl)
                   :tfr (ensure-integer tfr) :cfr (ensure-integer cfr)
                   :cxty cxty :ttyl ttyl :atnl atnl :lat lat :cenc cenc
                   :curver curver :sabin sabin)))

(defun find-usab-by-key (key-name key)
  "Find usab for a key"
  (collect-umlisp-query-eval ('mrsab '(vcui rcui vsab rsab son sf sver vstart
                                    vend imeta rmeta slc scc srl tfr cfr cxty
                                    ttyl atnl lat cenc curver sabin)
                                  nil key-name key :single t)
    (make-instance 'usab :vcui (ensure-integer vcui)
                   :rcui (ensure-integer rcui) :vsab vsab :rsab rsab :son son
                   :sf sf :sver sver :vstart vstart :vend vend :imeta imeta
                   :rmeta rmeta :slc slc :scc scc :srl (ensure-integer srl)
                   :tfr (ensure-integer tfr) :cfr (ensure-integer cfr)
                   :cxty cxty :ttyl ttyl :atnl atnl :lat lat :cenc cenc
                   :curver curver :sabin sabin)))

(defun find-usab-rsab (rsab)
  "Find usab for rsab"
  (find-usab-by-key 'rsab rsab))

(defun find-usab-vsab (vsab)
  "Find usab for vsab"
  (find-usab-by-key 'vsab vsab))

(defun find-cui-max ()
  (ensure-integer (caar (mutex-sql-query "select max(CUI) from MRCON"))))

;;;; Cross table find functions

(defun find-ucon-tui (tui &key (srl *current-srl*))
  "Find list of ucon for tui"
  (ensure-tui-integer tui)
  (collect-umlisp-query (mrsty (cui) srl tui tui :lrl klrl :order (cui asc))
    (find-ucon-cui (ensure-integer cui) :srl srl)))

(defun find-ucon-word (word &key (srl *current-srl*) (like nil))
  "Return list of ucons that match word. Optionally, use SQL's LIKE syntax"
  (collect-umlisp-query-eval ('mrxw_eng '(cui) srl 'wd word :like like :distinct t
                                     :lrl 'klrl :order '(cui asc))
    (find-ucon-cui cui :srl srl)))

(defun find-ucon-normalized-word (word &key (srl *current-srl*) (like nil))
  "Return list of ucons that match word, optionally use SQL's LIKE syntax"
  (collect-umlisp-query-eval ('mrxnw_eng '(cui) srl 'nwd word :like like :distinct t
                                      :lrl 'klrl :order '(cui asc))
    (find-ucon-cui cui :srl srl)))

(defun find-cui-normalized-word (word &key (srl *current-srl*) (like nil))
  "Return list of cui that match word, optionally use SQL's LIKE syntax"
  (collect-umlisp-query-eval ('mrxnw_eng '(cui) srl 'nwd word :like like :distinct t
                                         :lrl 'klrl :order '(cui asc))
                             cui))

(defun find-lui-normalized-word (word &key (srl *current-srl*) (like nil))
  "Return list of cui that match word, optionally use SQL's LIKE syntax"
  (collect-umlisp-query-eval ('mrxnw_eng '(lui) srl 'nwd word :like like :distinct t
                                         :lrl 'klrl :order '(cui asc))
                             lui))

(defun find-sui-normalized-word (word &key (srl *current-srl*) (like nil))
  "Return list of cui that match word, optionally use SQL's LIKE syntax"
  (collect-umlisp-query-eval ('mrxnw_eng '(sui) srl 'nwd word :like like :distinct t
                                         :lrl 'klrl :order '(cui asc))
                             sui))

(defun find-ustr-word (word &key (srl *current-srl*))
  "Return list of ustrs that match word"
  (collect-umlisp-query (mrxw_eng (cui sui) srl wd word :lrl klrl
                               :order (cui asc sui asc))
    (find-ustr-cuisui (ensure-integer cui) (ensure-integer sui) :srl srl)))

(defun find-ustr-normalized-word (word &key (srl *current-srl*))
  "Return list of ustrs that match word"
  (collect-umlisp-query (mrxnw_eng (cui sui) srl nwd word :lrl klrl
                                 :order (cui asc sui asc))
    (find-ustr-cuisui (ensure-integer cui) (ensure-integer sui) :srl srl)))

(defun find-uterm-word (word &key (srl *current-srl*))
  "Return list of uterms that match word"
  (collect-umlisp-query (mrxw_eng (cui lui) srl wd word :lrl klrl
                               :order (cui asc lui asc))
    (find-uterm-cuilui (ensure-integer cui) (ensure-integer lui) :srl srl)))

(defun find-uterm-normalized-word (word &key (srl *current-srl*))
  "Return list of uterms that match word"
  (collect-umlisp-query (mrxnw_eng (cui lui) srl nwd word :lrl klrl
                                 :order (cui asc lui asc))
    (find-uterm-cuilui (ensure-integer cui) (ensure-integer lui) :srl srl)))

(defun find-ucon-noneng-word (word &key (srl *current-srl*) (like nil))
  "Return list of ucons that match non-english word"
  (collect-umlisp-query-eval ('mrxw_noneng '(cui) srl 'wd word :like like
                                        :distinct t :lrl 'klrl :order '(cui asc))
    (find-ucon-cui cui :srl srl)))

(defun find-ustr-noneng-word (word &key (srl *current-srl*))
  "Return list of ustrs that match non-english word"
  (collect-umlisp-query (mrxw_noneng (cui sui) srl wd word :lrl klrl
                                  :order (cui asc sui asc))
    (find-ustr-cuisui (ensure-integer cui) (ensure-integer sui) :srl srl)))

;; Special tables

(defun find-usrl-all ()
  (collect-umlisp-query (usrl (sab srl) nil nil nil :order (sab asc))
    (make-instance 'usrl :sab sab :srl (ensure-integer srl))))

;;; Multiword lookup and score functions

(defun find-uobj-multiword (str obj-lookup-fun sort-fun key srl
                            only-exact-if-match)
  (let ((uobjs '()))
    (dolist (word (delimited-string-to-list str #\space))
      (setq uobjs (append uobjs (funcall obj-lookup-fun word :srl srl))))
    (let ((sorted
           (funcall sort-fun str
                    (delete-duplicates uobjs :test #'= :key key))))
      (if (and (plusp (length sorted))
               only-exact-if-match
               (multiword-match str (pfstr (first sorted))))
          (first sorted)
        sorted))))

(defun find-ucon-multiword (str &key (srl *current-srl*)
                                     (only-exact-if-match t))
  (find-uobj-multiword str #'find-ucon-word #'sort-score-pfstr-str
                       #'cui srl only-exact-if-match))

(defun find-uterm-multiword (str &key (srl *current-srl*)
                                      (only-exact-if-match t))
  (find-uobj-multiword str #'find-uterm-word #'sort-score-pfstr-str
                       #'lui srl only-exact-if-match))

(defun find-ustr-multiword (str &key (srl *current-srl*)
                                     (only-exact-if-match t))
  (find-uobj-multiword str #'find-ustr-word #'sort-score-ustr-str
                       #'sui srl only-exact-if-match))

(defun sort-score-pfstr-str (str uobjs)
  "Return list of sorted and scored ucons. Score by match of str to ucon-pfstr"
  (sort-score-umlsclass-str uobjs str #'pfstr))

(defun sort-score-ustr-str (str ustrs)
  "Return list of sorted and scored ucons. Score by match of str to ucon-pfstr"
  (sort-score-umlsclass-str ustrs str #'str))

(defun sort-score-umlsclass-str (objs str lookup-func)
  "Sort a list of objects based on scoring to a string"
  (let ((scored '()))
    (dolist (obj objs)
      (push (list obj (score-multiword-match str (funcall lookup-func obj)))
       scored))
    (mapcar #'car (sort scored #'> :key #'cadr))))


;;; LEX SQL functions

(defun find-lexterm-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrwd (wrd) nil eui eui :single t)
    (make-instance 'lexterm :eui eui :wrd wrd)))

(defun find-lexterm-word (wrd)
  (collect-umlisp-query (lrwd (eui) nil wrd wrd)
    (make-instance 'lexterm :eui (ensure-integer eui)
                   :wrd (copy-seq wrd))))

;; LEX SQL Read functions

(defun find-labr-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrabr (bas abr eui2 bas2) nil eui eui)
    (make-instance 'labr :eui eui :bas bas :abr abr :bas2 bas2
                   :eui2 (ensure-integer eui2))))

(defun find-labr-bas (bas)
  (collect-umlisp-query (labr (eui abr eui2 bas2) nil bas bas)
    (make-instance 'labr :eui (ensure-integer eui) :abr abr :bas2 bas2
                   :bas (copy-seq bas) :eui2 (ensure-integer eui2))))

(defun find-lagr-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lragr (str sca agr cit bas) nil eui eui)
    (make-instance 'lagr :eui eui :str str :sca sca :agr agr
                   :cit cit :bas bas)))

(defun find-lcmp-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrcmp (bas sca com) nil eui eui)
    (make-instance 'lcmp :eui eui :bas bas :sca sca :com com)))

(defun find-lmod-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrmod (bas sca psn_mod fea) nil eui eui)
    (make-instance 'lmod :eui eui :bas bas :sca sca :psnmod psn_mod :fea fea)))

(defun find-lnom-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrnom (bas sca eui2 bas2 sca2) nil eui eui)
    (make-instance 'lnom :eui eui :bas bas :sca sca :bas2 bas2 :sca2 sca2
                   :eui2 (ensure-integer eui2))))

(defun find-lprn-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrprn (bas num gnd cas pos qnt fea) nil eui eui)
    (make-instance 'lprn :eui eui :bas bas :num num :gnd gnd
                   :cas cas :pos pos :qnt qnt :fea fea)))

(defun find-lprp-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrprp (bas str sca fea) nil eui eui)
    (make-instance 'lprp :eui eui :bas bas :str str :sca sca :fea fea)))

(defun find-lspl-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrspl (spv bas) nil eui eui)
    (make-instance 'lspl :eui eui :spv spv :bas bas)))

(defun find-ltrm-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrtrm (bas gen) nil eui eui)
    (make-instance 'ltrm :eui eui :bas bas :gen gen)))

(defun find-ltyp-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrtyp (bas sca typ) nil eui eui)
    (make-instance 'ltyp :eui eui :bas bas :sca sca :typ typ)))

(defun find-lwd-wrd (wrd)
  (make-instance 'lwd :wrd wrd
                 :euilist (collect-umlisp-query (lrwd (eui) nil wrd wrd)
                                                (ensure-integer eui))))

;;; Semantic Network SQL access functions

(defun find-sdef-ui (ui)
  (collect-umlisp-query (srdef (rt sty_rl stn_rtn def ex un rh abr rin)
                            nil ui ui :single t)
    (make-instance 'sdef :rt rt :ui ui :styrl sty_rl :stnrtn stn_rtn
                   :def def :ex ex :un un :rh rh :abr abr :rin rin)))

(defun find-sstre1-ui (ui)
  (collect-umlisp-query (srstre1 (ui2 ui3) nil ui ui)
    (make-instance 'sstre1 :ui ui :ui2 (ensure-integer ui2)
                   :ui3 (ensure-integer ui3))))

(defun find-sstre1-ui2 (ui2)
  (collect-umlisp-query (srstre1 (ui ui3) nil ui2 ui2)
    (make-instance 'sstre1 :ui (ensure-integer ui) :ui2 ui2
                   :ui3 (ensure-integer ui3))))

(defun find-sstr-rl (rl)
  (collect-umlisp-query (srstre (sty_rl sty_rl2 ls) nil rl rl)
    (make-instance 'sstr :rl rl :styrl sty_rl :styrl2 sty_rl2 :ls ls)))

(defun find-sstre2-sty (sty)
  (collect-umlisp-query (srstre2 (rl sty2) nil sty sty)
    (make-instance 'sstre2 :sty (copy-seq sty) :rl rl :sty2 sty2)))

(defun find-sstr-styrl (styrl)
  (collect-umlisp-query (srstr (rl sty_rl2 ls) nil styrl styrl)
    (make-instance 'sstr :styrl styrl :rl rl :styrl2 sty_rl2 :ls ls)))


;;; **************************
;;; Local Classes
;;; **************************


(defun make-ustats ()
  (with-sql-connection (conn)
    (ignore-errors (sql-execute "drop table USTATS" conn))
    (sql-execute "create table USTATS (NAME varchar(160), COUNT bigint, SRL integer)" conn)

    (dotimes (srl 4)
      (insert-ustats-count conn "Concept Count" "MRCON" "distinct CUI" "KCUILRL" srl)
      (insert-ustats-count conn "Term Count" "MRCON" "distinct KCUILUI" "KCUILRL" srl)
      (insert-ustats-count conn "Distinct Term Count" "MRCON" "distinct LUI" "KLUILRL" srl)
      (insert-ustats-count conn "String Count" "MRCON" "*" "LRL" srl)
      (insert-ustats-count conn "Distinct String Count" "MRCON" "distinct SUI" "LRL" srl)
      (insert-ustats-count conn "Associated Expression Count" "MRATX" "*" "KSRL" srl)
      (insert-ustats-count conn "Context Count" "MRCXT" "*" "KSRL" srl)
      (insert-ustats-count conn "Co-occuring Concept Count" "MRCOC" "*" "KLRL" srl)
      (insert-ustats-count conn "Definition Count" "MRDEF" "*" "KSRL" srl)
      (insert-ustats-count conn "Locator Count" "MRLO" "*" "KLRL" srl)
      (insert-ustats-count conn "Rank Count" "MRRANK" "*" "KSRL" srl)
      (insert-ustats-count conn "Relationship Count" "MRREL" "*" "KSRL" srl)
      (insert-ustats-count conn "Semantic Type Count" "MRSTY" "*" "KLRL" srl)
      (insert-ustats-count conn "Simple Attribute Count" "MRSAT" "*" "KSRL" srl)
      (insert-ustats-count conn "Source Count" "MRSO" "*" "SRL" srl)
      (insert-ustats-count conn "Word Index Count" "MRXW_ENG" "*" "KLRL" srl)
      (insert-ustats-count conn "Normalized Word Index Count" "MRXNW_ENG" "*" "KLRL" srl)
      (insert-ustats-count conn "Normalized String Index Count" "MRXNS_ENG" "*" "KLRL" srl)
      (insert-ustats-count conn "Bonus Attribute Name Count" "BONUS_ATN" "*" nil srl)
      (insert-ustats-count conn "Bonus Relationship Count" "BONUS_REL" "*" nil srl)
      (insert-ustats-count conn "Bonus Source Abbreviation Count" "BONUS_SAB" "*" nil srl)
      (insert-ustats-count conn "Bonus Term Type Count" "BONUS_TTY" "*" nil srl))
    (sql-execute "create index USTATS_SRL on USTATS (SRL)" conn))
  (find-ustats-all))

(defun insert-ustats-count (conn name table count-variable srl-control srl)
  (insert-ustats conn name (find-count-table conn table srl count-variable srl-control) srl))

(defun find-count-table (conn table srl count-variable srl-control)
  (cond
   ((stringp srl-control)
    (ensure-integer
     (caar (sql-query (format nil "select count(~a) from ~a where ~a <= ~d"
                              count-variable table srl-control srl)
                      conn))))
   ((null srl-control)
    (ensure-integer
     (caar (sql-query (format nil "select count(~a) from ~a"
                              count-variable table )
                      conn))))
   (t
    (error "Unknown srl-control")
    0)))

(defun insert-ustats (conn name count srl)
  (sql-execute (format nil "insert into USTATS (name,count,srl) values ('~a',~d,~d)"
                       name count (if srl srl 3))
               conn))

(defun find-ustats-all (&key (srl *current-srl*))
  (if srl
      (collect-umlisp-query (ustats (name count srl) nil srl srl
                                    :order (name asc))
                            (make-instance 'ustats :name name
                                           :hits (ensure-integer count)
                                           :srl (ensure-integer srl)))
    (collect-umlisp-query (ustats (name count srl) nil nil nil
                                  :order (name asc))
                          (make-instance 'ustats :name name
                                         :hits (ensure-integer count)
                                         :srl (ensure-integer srl)))))

(defun find-ustats-srl (srl)
  (collect-umlisp-query (ustats (name count) nil srl srl :order (name asc))
                           (make-instance 'ustats :name name :hits (ensure-integer count))))



(defun find-bsab-sab (sab)
 (collect-umlisp-query (bonus_sab (name count) nil sab sab :single t)
     (make-instance 'bsab :sab sab :name name :hits (ensure-integer count))))

(defun find-bsab-all ()
 (collect-umlisp-query (bonus_sab (sab name count) nil nil nil :order (sab asc))
     (make-instance 'bsab :sab sab :name name :hits (ensure-integer count))))

(defun find-btty-tty (tty)
 (collect-umlisp-query (bonus_tty (name count) nil tty tty :single t)
     (make-instance 'btty :tty tty :name name :hits (ensure-integer count))))

(defun find-btty-all ()
 (collect-umlisp-query (bonus_tty (tty name count) nil nil nil :order (tty asc))
  (make-instance 'btty :tty tty :name name :hits (ensure-integer count))))

(defun find-brel-rel (rel)
  (collect-umlisp-query (bonus_rel (sab sl rel rela count) nil rel rel)
    (make-instance 'brel :sab sab :sl sl :rel rel :rela rela
                    :hits (ensure-integer count))))
