#|
 This file is a part of Colleen
 (c) 2016 Shirakumo http://tymoon.eu (shinmera@tymoon.eu)
 Author: Nicolas Hafner <shinmera@tymoon.eu>
|#

(asdf:defsystem pathname-utils-test
  :version "1.0.0"
  :license "Artistic"
  :author "Nicolas Hafner <shinmera@tymoon.eu>"
  :maintainer "Nicolas Hafner <shinmera@tymoon.eu>"
  :description "Tests for the pathname-utils system."
  :homepage "https://github.com/Shinmera/pathname-utils"
  :serial T
  :components ((:file "test"))
  :depends-on (:pathname-utils :parachute)
  :perform (asdf:test-op (op c) (uiop:symbol-call :parachute :test :pathname-utils-test)))
