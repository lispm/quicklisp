;;; Bourbaki -- Equivalence relations
;;; Converted 27.9.2006

(module "equiv0"

  (meta (:comment . "Equivalence relations.")
        (:description . "Prove the various transitivity theorems once and for all."))

  (symkind "WFF")
  (symkind "OBJ")
  (export [!!equiv0-ax])
  ;; hack need some way to do this cleanly
  (in-context *root-context*
    (alias "prop-ax" () [equiv0-ax]))
  (import [!!prop])
  ;(import [(require "equiv0-ax" "equiv0-ax" :provide `(("prop-ax" ,!prop)))])

  (th-pattern !ax-tr)
  (th-pattern !ax-sym)

  (th "refl<" (obj ![x y])
      (ass [→ = x y = x x])
      (proof
        (suppose ([= x y]) ([= x x])
          [infer ax-tr x y x]
          [infer ax-sym x y]
          [ax-mp [= y x] [= x x]])))

  (th "refl>" (obj ![x y])
      (ass [→ = x y = y y])
      (proof
        [refl< y x]
        [+syl ax-sym x y [= y y]]))

  (th "com" (obj ![x y])
      (ass [↔ = x y = y x])
      (proof
        [→ = x y = y x]
        [→ = y x = x y]
        [>bii [= x y] [= y x]]))
  (th-pattern !com)

  (th "tr>" (obj ![x y z])
      (ass [→ = x y → = x z = y z])
      (proof
        [→ = y x → = x z = y z]
        [+syl ax-sym x y [→ = x z = y z]]))
  (th-pattern !tr>)

  (th "tr<" (obj ![x y z])
      (ass [→ = x y → = z x = z y])
      (proof
        [→ = z x → = x y = z y]
        [!im!com12i [= z x] [= x y] [= z y]]))
  (th-pattern !tr<)

  (th "eqt<" (obj ![x y z])
      (ass [→ = x y ↔ = x z = y z])
      (proof
        [→ = x y → = y z = x z]
        [→ = y x → = x z = y z]
        [+syl ax-sym x y [→ = x z = y z]]
        [>bid [= x y] [= x z] [= y z]]))
  (th-pattern !eqt<)

  (th "eqt>" (obj ![x y z])
      (ass [→ = x y ↔ = z x = z y])
      (proof
        [↔ = x z = z x]
        [↔ = y z = z y]
        [!bi!eqti [= x z] [= z x] [= y z] [= z y]]
        [→ = x y ↔ = x z = y z]
        [b2i syl [= x y] [↔ = x z = y z] [↔ = z x = z y]]))
  (th-pattern !eqt>)

  (th "eqt" (obj ![a b c d])
      (ass [→ ∧ = a b = c d ↔ = a c = b d])
      (proof
        [→ = a b ↔ = a c = b c]
        [→ = c d ↔ = b c = b d]
        [+syl !an!sim< [= a b] [= c d] [↔ = a c = b c]]
        [+syl !an!sim> [= a b] [= c d] [↔ = b c = b d]]
        [!bi!trd [∧ = a b = c d] [= a c] [= b c] [= b d]]))
  (th-pattern !eqt)

  (th "eqti" (obj ![a b c d])
      (hypo [= a b] [= c d])
      (ass [↔ = a c = b d])
      (proof
        [>andi [= a b] [= c d]]
        [infer eqt a b c d]))
  (th-pattern !eqti)

  (th "eqtd" (obj ![a b c d] wff "φ")
      (hypo [→ φ = a b] [→ φ = c d])
      (ass [→ φ ↔ = a c = b d])
      (proof
        [jca φ [= a b] [= c d]]
        [ded eqt a b c d φ]))
  (th-pattern !eqtd)

  (th "eqtd2" (obj ![a b c d] wff ![φ ψ])
      (hypo [→ φ = a b] [→ ψ = c d])
      (ass [→ ∧ φ ψ ↔ = a c = b d])
      (proof
        [+syl !an!sim< φ ψ [= a b]]
        [+syl !an!sim> φ ψ [= c d]]
        [eqtd a b c d [∧ φ ψ]]))
  (th-pattern !eqtd2)

  (th "eqtd2r" (obj ![a b c d] wff ![φ ψ])
      (hypo [→ φ = a b] [→ ψ = c d])
      (ass [→ ∧ ψ φ ↔ = a c = b d])
      (proof
        [eqtd2 a b c d φ ψ]
        [+syl !an!com φ ψ [↔ = a c = b d]]))
  (th-pattern !eqtd2r)

  (th "tr-an" (obj ![a b c])
      (ass [→ ∧ = a b = b c = a c])
      (proof
        [→ = a b → = b c = a c]
        [imp [= a b] [= b c] [= a c]]))
  (th-pattern !tr-an)

  (th "tr-an>" (obj ![a b c])
      (ass [→ ∧ = a b = a c = b c])
      (proof
        [→ = a b ↔ = a c = b c]
        [ded bi> [= a c] [= b c] [= a b]]
        [imp [= a b] [= a c] [= b c]]))
  (th-pattern !tr-an>)

  (th "tr-an<" (obj ![a b c])
      (ass [→ ∧ = a c = b c = a b])
      (proof
        [→ = b c ↔ = a b = a c]
        [ded bi< [= a b] [= a c] [= b c]]
        [imp [= b c] [= a c] [= a b]]
        [+syl !an!com [= a c] [= b c] [= a b]]))
  (th-pattern !tr-an<)

  (th "tri" (obj ![a b c])
      (hypo [= a b] [= b c])
      (ass [= a c])
      (proof
        [infer ax-tr a b c]
        [ax-mp [= b c] [= a c]]))

  (th "tri-r" (obj ![a b c])
      (hypo [= a b] [= b c])
      (ass [= c a])
      (proof
        [tri a b c]
        [infer ax-sym a c]))

  (th "tri>" (obj ![a b c])
      (hypo [= a b] [= a c])
      (ass [= b c])
      (proof
        [infer ax-sym a b]
        [tri b a c]))

  (th "tri<" (obj ![a b c])
      (hypo [= a b] [= c b])
      (ass [= a c])
      (proof
        [infer ax-sym c b]
        [tri a b c]))

  (th "2tri" (obj ![a b c d])
      (hypo [= a b] [= b c] [= c d])
      (ass [= a d])
      (proof
        [tri a b c]
        [tri a c d]))

  (th ">2tri" (obj ![a b c d])
      (hypo [= a b] [= a c] [= b d])
      (ass [= c d])
      (proof
        [infer ax-sym a c]
        [2tri c a b d]))

  (th ">2tri-r" (obj ![a b c d])
      (hypo [= a b] [= a c] [= b d])
      (ass [= d c])
      (proof
        [>2tri a b c d]
        [infer ax-sym c d]))

  (th "<2tri" (obj ![a b c d])
      (hypo [= a b] [= c a] [= d b])
      (ass [= c d])
      (proof
        [infer ax-sym d b]
        [2tri c a b d]))

  (th "<2tri-r" (obj ![a b c d])
      (hypo [= a b] [= c a] [= d b])
      (ass [= d c])
      (proof
        [<2tri a b c d]
        [infer ax-sym c d]))

  (th "trd" (obj ![a b c] wff "φ")
      (hypo [→ φ = a b] [→ φ = b c])
      (ass [→ φ = a c])
      (proof
        [ded ax-tr a b c φ]
        [mpd [= b c] [= a c] φ]))

  (th "trd-r" (obj ![a b c] wff "φ")
      (hypo [→ φ = a b] [→ φ = b c])
      (ass [→ φ = c a])
      (proof
        [trd a b c φ]
        [ded ax-sym a c φ]))

  (th "trd>" (obj ![a b c] wff "φ")
      (hypo [→ φ = a b] [→ φ = a c])
      (ass [→ φ = b c])
      (proof
        [ded ax-sym a b φ]
        [trd b a c φ]))

  (th "trd<" (obj ![a b c] wff "φ")
      (hypo [→ φ = a b] [→ φ = c b])
      (ass [→ φ = a c])
      (proof
        [ded ax-sym c b φ]
        [trd a b c φ]))

  (th "2trd" (obj ![a b c d] wff "φ")
      (hypo [→ φ = a b] [→ φ = b c] [→ φ = c d])
      (ass [→ φ = a d])
      (proof
        [trd a b c φ]
        [trd a c d φ]))

  (th ">2trd" (obj ![a b c d] wff "φ")
      (hypo [→ φ = a b] [→ φ = a c] [→ φ = b d])
      (ass [→ φ = c d])
      (proof
        [ded ax-sym a c φ]
        [2trd c a b d φ]))

  (th "<2trd" (obj ![a b c d] wff "φ")
      (hypo [→ φ = a b] [→ φ = c a] [→ φ = d b])
      (ass [→ φ = c d])
      (proof
        [ded ax-sym d b φ]
        [2trd c a b d φ]))

  (th "<2trd-r" (obj ![a b c d] wff "φ")
      (hypo [→ φ = a b] [→ φ = c a] [→ φ = d b])
      (ass [→ φ = d c])
      (proof
        [<2trd a b c d φ]
        [ded ax-sym c d φ]))

  (th "rp-1<" (obj ![a b c] wff "φ")
      (hypo [→ φ = a b] [= c a])
      (ass [→ φ = c b])
      (proof
        [infer ax1 [= c a] φ]
        [trd c a b φ]))

  (th "rp-1<r" (obj ![a b c] wff "φ")
      (hypo [→ φ = a b] [= c a])
      (ass [→ φ = b c])
      (proof
        [rp-1< a b c φ]
        [ded ax-sym c b φ]))

  (th "rp-1>" (obj ![a b c] wff "φ")
      (hypo [→ φ = a b] [= a c])
      (ass [→ φ = c b])
      (proof
        [infer ax-sym a c]
        [rp-1< a b c φ]))

  (th "rp-1>r" (obj ![a b c] wff "φ")
      (hypo [→ φ = a b] [= a c])
      (ass [→ φ = b c])
      (proof
        [infer ax-sym a c]
        [rp-1<r a b c φ]))

  (th "rp-2>" (obj ![a b c] wff "φ")
      (hypo [→ φ = a b] [= b c])
      (ass [→ φ = a c])
      (proof
        [infer ax1 [= b c] φ]
        [trd a b c φ]))

  (th "rp-2>r" (obj ![a b c] wff "φ")
      (hypo [→ φ = a b] [= b c])
      (ass [→ φ = c a])
      (proof
        [rp-2> a b c φ]
        [ded ax-sym a c φ]))

  (th "rp-2<" (obj ![a b c] wff "φ")
      (hypo [→ φ = a b] [= c b])
      (ass [→ φ = a c])
      (proof
        [infer ax-sym c b]
        [rp-2> a b c φ]))

  (th "rp-2<r" (obj ![a b c] wff "φ")
      (hypo [→ φ = a b] [= c b])
      (ass [→ φ = c a])
      (proof
        [infer ax-sym c b]
        [rp-2>r a b c φ]))

  (th "trd-2" (obj ![a b c] wff ![φ ψ])
      (hypo [→ φ = a b] [→ ψ = b c])
      (ass [→ ∧ φ ψ = a c])
      (proof
        [+syl !an!sim< φ ψ [= a b]]
        [+syl !an!sim> φ ψ [= b c]]
        [trd a b c [∧ φ ψ]]))

  (th "trd-2r" (obj ![a b c] wff ![φ ψ])
      (hypo [→ φ = a b] [→ ψ = b c])
      (ass [→ ∧ ψ φ = a c])
      (proof
        [trd-2 a b c φ ψ]
        [+syl !an!com φ ψ [= a c]]))
  
  (th ">2trg" (obj ![a b c d] wff "φ")
      (hypo [→ φ = a b] [= a c] [= b d])
      (ass [→ φ = c d])
      (proof
        [rp-1> a b c φ]
        [rp-2> c b d φ]))
  
  (th "<2trg" (obj ![a b c d] wff "φ")
      (hypo [→ φ = a b] [= c a] [= d b])
      (ass [→ φ = c d])
      (proof
        [rp-1< a b c φ]
        [rp-2< c b d φ]))
  
  ;; TODO: eq2tr

  (th "eqt<n" (obj ![a b c])
      (ass [→ = a b ↔ ≠ a c ≠ b c])
      (proof
        [→ = a b ↔ = a c = b c]
        [ded neqt [= a c] [= b c] [= a b]]
        [df-neq a c]
        [df-neq b c]
        [!bi!<trg [= a b] [¬ = a c] [¬ = b c] [≠ a c] [≠ b c]]))
  (th-pattern !eqt<n)

  (th "eqt>n" (obj ![a b c])
      (ass [→ = a b ↔ ≠ c a ≠ c b])
      (proof
        [→ = a b ↔ = c a = c b]
        [ded neqt [= c a] [= c b] [= a b]]       
        [df-neq c a]
        [df-neq c b]
        [!bi!<trg [= a b] [¬ = c a] [¬ = c b] [≠ c a] [≠ c b]]))
  (th-pattern !eqt>n)

  (th "eqneqi" (obj ![a b c d])
      (hypo [↔ = a b = c d])
      (ass [↔ ≠ a b ≠ c d])
      (proof
        [infer neqt [= a b] [= c d]]
        [df-neq a b]
        [df-neq c d]
        [!bi!<tr [¬ = a b] [¬ = c d] [≠ a b] [≠ c d]]))

  (th "eqneqd" (obj ![a b c d] wff "φ")
      (hypo [→ φ ↔ = a b = c d])
      (ass [→ φ ↔ ≠ a b ≠ c d])
      (proof
        [ded neqt [= a b] [= c d] φ]
        [df-neq a b]
        [df-neq c d]
        [!bi!<trg φ [¬ = a b] [¬ = c d] [≠ a b] [≠ c d]]))

  (th "neqcom" (obj ![a b])
      (ass [↔ ≠ a b ≠ b a])
      (proof
        [↔ = a b = b a]
        [eqneqi a b b a]))
  (th-pattern !neqcom)

) ; End module "equiv"
