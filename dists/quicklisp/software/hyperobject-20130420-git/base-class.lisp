;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          base-class.lisp
;;;; Purpose:       Definition of basic hyperobject class
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  Apr 2000
;;;;
;;;; $Id$
;;;;
;;;; This file is Copyright (c) 2000-2003 by Kevin M. Rosenberg
;;;; *************************************************************************

(in-package #:hyperobject)


(defclass hyperobject ()
  ()
  (:metaclass hyperobject-class)
  (:description "Basic hyperobject class"))

(defmethod print-object ((obj hyperobject) s)
  (print-unreadable-object (obj s :type t :identity nil)
    (funcall (obj-data-printer (get-view-id obj :compact-text))
             obj s nil)))

