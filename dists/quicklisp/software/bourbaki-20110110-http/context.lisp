;;; Copyright (c) Juha Arpiainen 2005-06

;;; This file is part of Bourbaki.

;;; Bourbaki is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.

;;; Bourbaki is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Bourbaki; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; -------

;;;; Functions related to the hierarchical context system:
;;;; - Creating contexts
;;;; - Seeking symbols and theorems from contexts
;;;; - Importing and exporting symbols and theorems

(in-package :bourbaki)

(defun seek-ctx-imports (name &optional (context *current-context*))
  "Helper function for seek-sym.
Seeks for a theorem or symbol in imported contexts."
  (dolist (ref (context-imports context))
    (let ((res (gethash name (context-syms (symref-target ref)))))
      (when res
        (return-from seek-ctx-imports
                     (compose-ref ref res))))))

(defun seek-ctx-exports (name ref)
  (dolist (exp (context-exports (symref-target ref)))
    (let ((res (gethash name (context-syms (symref-target exp)))))
      (when res
        (return-from seek-ctx-exports
                     (compose-ref (compose-ref ref exp) res))))))

;; NOTE: The separate check for :arg and :loc is an optimization
;; and may lead to unexpected results with imports/exports
(defun seek-sym (mode name1 &rest names)
  "Seeks for a theorem or context or symbol."
  (let ((ref (case mode
               (:abs
                 (seek-ctx-imports name1 *root-context*))
               (:rel
                 (seek-ctx-imports name1)))))
    (when (null ref) (error "Seek-sym: not found[~A]~%" name1))
    (labels ((seek-aux (ref names)
               (if (null names)
                 ;; XXX variables don't inherint vars from parent context
                 (progn (if (and (symref-target ref)
                                 (member (context-class (symref-target ref)) '(:arg :loc)))
                          (setf (symref-fn ref)
                                (load-time-value #'(lambda () nil))))
                        ref)
                 (if (eq (symref-class ref) :simple-meta)
                   ;; simple metatheorems
                   (let ((ref2 (apply #'seek-sym :abs names)))
                     (funcall (symref-fn ref) (symref-target ref2)))
                   ;; other types of reference
                   (let ((ref (seek-ctx-exports (car names) ref)))
                     (cond
                       ((null ref) (error "Symbol ~A not found" (car names)))
                       ((and (symref-target ref)
                             (member (context-class (symref-target ref)) '(:arg :loc)))
                        ;; TODO: change var/loc semantics to allow this
                        (error "Attempt to use local via export"))
                       (t (seek-aux ref (rest names)))))))))
      (seek-aux ref names))))
      
;; Make a context, link up to parent
(defun create-context (&key (name "") type (parent *current-context*) class)
  "Create a subcontext of `parent' with specified type"
  (let ((ctx (make-context :name name :class class :type type)))
    (when parent
      (setf (context-imports ctx) (context-imports parent)   ; link imports,
            (context-distinct ctx) (context-distinct parent) ; distinct variable conditions (DVC),
            (context-vars ctx) (context-vars parent)         ; variables and ...
            (gethash :full-name (context-meta ctx)) (cons name (gethash :full-name (context-meta parent)))
            (gethash :parent (context-meta ctx)) parent)
      ;; Special case: definitions do not inherit hypotheses
      (if (member class '(:axiom :theorem :context :module) :test #'eq)
        (setf (context-hypo ctx) (context-hypo parent))))    ; ... hypotheses to parent
    (push (make-symref :target ctx :args-needed 0 :class :implicit
                       :fn #'(lambda () (makevarlist-r ctx)))
          (context-imports ctx)) ; import self
    (push (make-symref :target ctx :args-needed 0 :class :implicit
                       :fn (load-time-value #'(lambda (&rest x) x)))
          (context-exports ctx)) ; export self
    ctx))

;;; Seek for a subcontext, creating it if it did not already exist
;;; XXX now automatically create & replace previous XXX
(defun seek-create-ctx (name &optional (class :context) (context *current-context*))
  (let ((ctx (gethash name (context-syms context)))) ; do not seek from imported contexts
    (if nil #| ctx |# (symref-target ctx)
      (progn
        (setf ctx (create-context :name name :parent context :class class))
        (symref-target (insert-sym (wrap-ctx ctx) context))))))

(defun insert-sym (ref &optional (context *current-context*) (name (context-name (symref-target ref))))
  (setf (gethash name (context-syms context))
        ref))

;(defun subctxp (ctx1 ctx2)
;  (cond ((null ctx1) nil)
;        ((eq ctx1 ctx2) t)
;        (t (subctxp (gethash :parent (context-meta ctx1)) ctx2))))

;;; Create a fresh root context, insert some modules from the current root
;;; then load the named module and copy into current - -
;;; return the loaded context
;;; TODO: provide is obsolate?
(defun require (name module &key provide)
  (aif (seek-ctx-imports name *root-context*)
    (progn (format t "require: ~a found, will not load~%" name)
           (return-from require it)))
  (let ((new-root (mkrootctx)))
    (insert-export (list *root-context*) :where new-root)
    (dolist (i provide)
      (setf (gethash (first i) (context-syms new-root)) (second i)))
    (let ((*root-context* new-root))
      (load-aux module))
    (let ((result (gethash module (context-syms new-root))))
      (setf (gethash name (context-syms *root-context*)) result)
      (seek-ctx-imports name *root-context*))))

;;; Insert a context to the import list of `where'
;;; Also import all contexts exported from `where' with
;;; a composite symref function ("transitivity of imports")
;;; Keep the reference to self at beginning of the list
(defun insert-import (target &key (where *current-context*) proof)
;  (let ((ref (make-symref :target ctx
;                          :class :explicit
;                          :args-needed 0
;                          :fn #'(lambda () vars))))
  (let ((ref (pattern-to-ref target :import)))
    (if proof (setf (symref-proof ref) #'(lambda () proof))) ;; TODO: patterns...
    (dolist (expr (reverse (context-exports (car target)))) ; Propagate exports
      (push-second (compose-ref ref expr)
                   (context-imports where)))))

;;; Insert a context to the import and export lists of where
;;; Also add all exports of `ctx' with a composite symref
;;; function ("transitivity of exports")
;;; Keep the reference to self at beginning of the list
(defun insert-export (target &key (where *current-context*) proof)
  (insert-import target :where where :proof proof)
  (let ((ctx (car target))
        (pat (pattern-to-ref target :export)))
    (let ((ref (make-symref :target ctx
                            :class :explicit
                            :args-needed 0
                            ;; TODO: patterns with proof
                            :proof (if proof #'(lambda (&rest args)
                                                 (let ((smap (smap where args)))
                                                   (replace-proof-vars proof smap))))
                            :fn #'(lambda (&rest args)
                                    (let ((my-args (subseq args (symref-args-needed pat)))
                                          (pat-args (subseq args 0 (symref-args-needed pat))))
                                      (let ((svars (apply (symref-fn pat) pat-args))
                                            (smap (smap where my-args)))
                                        (mapcar #'(lambda (x) (replace-vars x smap))
                                                svars)))))))
      (dolist (exp (reverse (context-exports ctx)))
        (push-second (compose-ref ref exp)
                     (context-exports where))))))
;          (ref (make-symref :target ctx
;                            :class :explicit
;                            :args-needed 0
;                            :fn #'(lambda (&rest args)
;                                    (let ((smap (smap where args)))
;                                      (mapcar #'(lambda (x) (replace-vars x smap))
;                                              vars))))))
;    (dolist (exp (reverse (context-exports ctx))) ; Propagate exports
;      (push-second (compose-ref ref exp)
;                   (context-exports where)))))

;;; Wrap ctx in a symref with identity function
(defun wrap-ctx (ctx &optional (nvars (length (context-vars ctx))))
  (make-symref :target ctx
               :args-needed nvars
               :class :sym
               :fn (load-time-value #'(lambda (&rest x) x))
               :proof nil))

;(defun parse-thr-ref (tree &optional ref args)
;  (let ((smap (smap (car tree) (cdr tree))))
;    (make-subproof :ref tree
;                   :hypo (mapcar #'(lambda (x) (replace-vars x smap))
;                                 (context-hypo (car tree))) 
 ;                  :sub (if (and ref (symref-proof ref))
 ;                         (let ((pf (apply-ref-proof ref args)))
  ;                          (format t "Has associated proof ~a~%" pf)
  ;                          pf))
  ;                 :assert (mapcar #'(lambda (x) (replace-vars x smap))
  ;                                 (context-assert (car tree))))))

(defun parse-thr-ref (tree &optional ref args)
  (let ((smap (smap (car tree) (cdr tree))))
    (let ((last-line (make-subproof :ref tree
                                    :hypo (mapcar #'(lambda (x) (replace-vars x smap))
                                                  (context-hypo (car tree)))
                                    :sub nil
                                    :assert (mapcar #'(lambda (x) (replace-vars x smap))
                                                    (context-assert (car tree)))))
          (first-line (if (and ref (symref-proof ref))
                        (apply-ref-proof ref args))))
      (if first-line
        (make-subproof :hypo (subproof-hypo first-line)
                       :assert (subproof-assert last-line)
                       :ref nil
                       :sub (list first-line last-line))
        last-line))))

(defun symtree-parse-aux ()
  "Helper function for symtree-parse"
  (declare (special *parse-str*))
  (if (null *parse-str*) nil
    ;; get a symref for the operator symbol
    (let ((op (pop *parse-str*)))
      (typecase op
        (list op)
        (context op)
        (symref
          ;; get the arguments and apply the symref function to get result
          ;; ;; TODO: if (symref-proof op), replace placeholder vars in the proof with args
          (let ((count (symref-args-needed op))
                (args nil))
            (if (< count 0)
              (do () ((null *parse-str*))
                (push (symtree-parse-aux) args))
              (dotimes (i count)
                (push (symtree-parse-aux) args)))
            ;; TODO: consider using proof-fn if the op is a metatheorem
            (if (and *current-proof* (symref-target op) (theorem-sym-p (symref-target op)))
              ;; If we are parsing a theorem reference, return also the associated proof
              ;; Note that apply-ref-* is a destructive operation on args
              (parse-thr-ref (apply-ref op (copy-list args)) op args)
              ;; Otherwise just return the parsed symtree
              (apply-ref op args))))
        (otherwise op)))))

(defun symtree-parse (str)
  "Parse an expression returned by symstr-reader"
  (let ((*parse-str* str))
    (declare (special *parse-str*))
    (symtree-parse-aux)))

;; Ensure that argument is context
(defun mkcontext (ref)
  (typecase ref
    (context ref)
    (symref (symref-target ref))))

;; Create a new context tree
(defun flush ()
  (setf *theorem-patterns* (make-pattern-tree)
        *root-context* (mkrootctx)
        *current-context* *root-context*))

;(defun filter-syms (ctx classes)
;  (sort (delete-if-not #'(lambda (x) (and (eq (symref-class x) :sym)
;                                          (member (context-class (symref-target x)) classes)))
;                       (loop for i being the hash-values in (context-syms ctx)
;                             collect i))
;        #'string< :key (lambda (x) (context-name (symref-target x)))))
