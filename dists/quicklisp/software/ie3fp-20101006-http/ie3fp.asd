;;;  ie3fp.asd --- system definition

;;;  Copyright (C) 2009 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: IE3FP

#+cmu (ext:file-comment "$Module: ie3fp.asd, Time-stamp: <2009-05-28 18:10:41 wcp> $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

(in-package :cl-user)

(defpackage :ie3fp-system
  (:use :common-lisp :asdf))

(in-package :ie3fp-system)

(defsystem ie3fp
    :name "IE3FP"
    :author "Walter C. Pelissero <walter@pelissero.de>"
    :maintainer "Walter C. Pelissero <walter@pelissero.de>"
    :description "IEEE 754 en/de-coder"
    :long-description
    "A collection of functions to encode and decode floating point
numbers conforming the IEEE-754 standard."
    :licence "LGPL"
    :components ((:file "code")))
