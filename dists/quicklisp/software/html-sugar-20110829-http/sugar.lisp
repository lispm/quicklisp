;;;  sugar.lisp --- collection of various HTML commodities

;;;  Copyright (C) 2006, 2007, 2008, 2009 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: html sugar

#+cmu (ext:file-comment "$Module: sugar.lisp $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

(in-package :html-sugar)


(defun session-variable (req name)
  (websession-variable (websession-from-req req) (string name)))

(defun (setf session-variable) (value req name)
  (setf (websession-variable (websession-from-req req) (string name)) value))

(defmacro with-wa ((ent) &body body)
  "Create a local binding of WA to the current web action \(taken
from ENT)."
  `(be wa (getf (entity-plist ,ent) 'webaction)
     (declare (ignorable wa))
     ,@body))

(defmacro with-session-variables ((req &rest variables) &body body)
  `(symbol-macrolet ,(mapcar #'(lambda (a)
				 `(,a (session-variable ,req ',a)))
			     variables)
     ,@body))

(defun request-query-values (key req &key (post t) (uri t) (test #'equal)
			     (external-format *default-aserve-external-format*))
  "Just like REQUEST-QUERY-VALUE but KEY is expected to accur
more than once in the query, so return all its values in a list."
  (loop
     for (k . v) in (request-query req :post post :uri uri
				   :external-format external-format)
     when (funcall test key k)
     collect v))

(defun query-value-string (request variable-name &optional multiple)
  "Similar to REQUEST-QUERY-VALUE but case insensitive.  If MULTIPLE
is true, the returned value is a list of values; one for each
occurrence of VARIABLE-NAME in the query REQUEST."
  (be name-string (string-downcase variable-name)
    (if multiple
	(request-query-values name-string request :test #'equalp)
	(request-query-value name-string request :test #'equalp))))

(defun query-value (request variable-name &optional multiple)
  "Similar to QUERY-VALUE-STRING, but interpret the string value as
Lisp value."
  (flet ((read-val (value)
	   (if (stringp value)
	       (ignore-errors
		 (setf value (string-trim-whitespace value))
		 ;; T and NIL are special cases
		 (cond ((string-equal "T" value)
			t)
		       ((string-equal "NIL" value)
			nil)
		       ;; for anything else use the Lisp reader
		       (t
			;; interned symbols should go into the keyword
			;; package
			(with-package :keyword
			  (read-from-string value)))))
	       ;; the values can be added by the program itsef
	       value)))
    (be result (query-value-string request variable-name multiple)
      (if result
	  (values (if multiple
		      (mapcar #'read-val result)
		      (read-val result))
		  t)
	  (values nil nil)))))

;; in case of multiple values this setter doesn't do its job the way
;; it's sussposed to -wcp1/12/06.
(defun (setf query-value) (value request variable-name &optional multiple)
  (declare (ignore multiple))
  (setf (request-query-value (string-downcase variable-name) request)
	value))

(defmacro with-query-values ((request &rest variables) &body body)
  (be lets (loop
	      for variable in variables
	      for name = (string variable)
	      for multiple? = (string-ends-with "*" name)
	      for object? = (string-starts-with "$" name)
	      collect
		(list variable
		      (cond ((and multiple? object?)
			     `(query-value ,request
					   ,(string-downcase (subseq name 1 (1- (length name))))
					   t))
			    (multiple?
			     `(request-query-values ,(string-downcase (subseq name 0 (1- (length name))))
						    ,request))
			    (object?
			     `(query-value ,request
					   ,(string-downcase (subseq name 1))))
			    (t
			     `(request-query-value ,(string-downcase name) ,request)))))
      `(let ,lets
	 ,@body)))

#+(OR)
(defmacro with-query-string-values ((request &rest variables) &body body)
  `(let ,(mapcar #'(lambda (v)
		     (if (consp v)
			 `(,(car v) (request-query-values ,(string-downcase (car v)) ,request))
			 `(,v (request-query-value ,(string-downcase v) ,request))))
		 variables)
     ,@body))

(defmacro with-request-values ((request &rest variables) &body body)
  `(symbol-macrolet ,(mapcar #'(lambda (v)
				 `(,v (request-variable-value ,request ,(string-downcase v))))
			     variables)
     ,@body))

(defmacro defclp (name session-variables &body forms)
  "Define a Common Lisp Page function.  This can be then referred
in a CLP page in the usual way.  This macro captures some
variables which can be used by FORMS.  These are: REQ, ENT, ARGS,
BODY, WA, SESSION.  See the AServe documentation for their
meaning."
  `(def-clp-function ,name (req ent args body)
     (declare (ignorable args body))
     (with-session-variables (req ,@session-variables)
       ,@forms)))

(defmacro defwa (name variables &body forms)
  "Define a web action (aka web page).  A web action is supposed
to return either :CONTINUE or a string of the name of the page to
redirect the browser to.  This macro captures some variables which
can be used by FORMS.  These are: REQ, ENT, WA, SESSION.  See the
AServe documentation for their meaning."
  (be query-values '()
      session-variables '()
    (loop
       for var in variables
       for name = (string var)
       for session? = (char= #\% (char name 0))
       do (if session?
	      (push var session-variables)
	      (push var query-values)))
    `(defun ,name (req ent)
       (declare (ignorable req ent))
       (with-query-values (req ,@query-values)
	 (with-session-variables (req ,@session-variables)
	   ,@forms)))))

(defvar *default-language* "en")

(defmacro defpage (name (&key variables title
			      (language *default-language*) output-page-head
			      body-args timeout)
		   &body body)
  "Define a web page.  Header and footer are standard.  BODY is
supposed to output the dynamic part of the page."
  `(defwa ,name ,variables
     (with-http-response (req ent ,@(when timeout (list :timeout timeout)))
       (with-http-body (req ent)
	 (html
	  ((:html lang ,language "xml:lang" ,language)
	   ,(when output-page-head
		  `(,output-page-head ,title))
	   ((:body ,@body-args)
	    ,@body)))))
     ;; don't go on (this is definitive)
     nil))

(defun current-page (request entity)
  "Return the current page path excluding prefix, session ID (if
present), and query variables."
  (net.aserve::match-prefix (net.aserve::prefix entity)
			    (puri:uri-path (request-raw-uri request))))

(defun encode-url (req ent page &optional query-values-alist)
  "Encode an url using when appropriate the session id.
QUERY-VALUES-ALIST is a list of pairs where the car of each pair
is a string or a keyword and the cdr is its value."
  (be path (locate-action-path (getf (entity-plist ent) 'webaction)
			       (if (eq page :current)
				   (current-page req ent)
				   page)
			       (websession-from-req req))
      anchor (awhen (assoc 'anchor query-values-alist)
	       (setf query-values-alist
		     (delete 'anchor query-values-alist :key #'car))
	       (cdr it))
    (if query-values-alist
	(s+ path "?"
	    (query-to-form-urlencoded
	     (loop
		for (tag . value) in query-values-alist
		;; skip variables with null values as, if a variable
		;; is missing, it's considered automatically with
		;; value NIL
		when value
		collect
		  (cons (string-downcase tag)
			;; This is also done by QUERY-TO-FORM-
			;; URLENCODED, but it does the wrong
			;; formatting for values.
			(if (stringp value)
			    value
			    (with-output-to-string (out)
			       (write value :stream out :escape t :pretty nil :readably nil))))))
	    (if anchor
		(format nil "#~A" anchor)
		""))
	path)))

;; This macro uses names already captured by the macros above
(defmacro url (page &rest query-values)
  "Encode an URL based on current status.  This means that the
URL may contain any additonal information required to keep the
association to the webactions' session.  This macro expect to
find a couple of bindings in the current lexical environment: REQ
and ENT.  These are normally guaranted by DEFWA and DEFCLP
macros."
  `(encode-url req ent ,page
	       ,(when query-values
		      (cons 'list (loop
				     for (tag value) on query-values by #'cddr
				     collect `(cons ,tag ,value))))))

(defun html-select (name choices &key selected multiple)
  "Output HTML for a <SELECT> input item.  NAME is the name to be
given to the input item.  CHOICES is a list of pairs:

  \(id . description)

The descriptions fill the selection box while the ids are passed
to the web server.  If SELECTED tells which items of the
selection should be selected by default.  If MULTIPLE tell to
make a multiple choice selection input item of that size in
lines."
  (be test (if (stringp selected)
	       #'string-equal
	       #'eql)
    (unless (listp selected)
      (setf selected (list selected)))
    (flet ((output-options ()
	     (dolist (c choices)
	       (if (find (car c) selected :test test)
		   (html ((:option selected t value (car c))
			  (:princ-safe (cdr c))))
		   (html ((:option value (car c))
			  (:princ-safe (cdr c))))))))
      (if multiple
	  (html ((:select name name size multiple multiple t)
		 (output-options)))
	  (html ((:select name name size 1)
		 (output-options)))))))

(defun html-radio (name choices &key selected vertical)
  "Output HTML for a <RADIO> input item.  NAME is the name to be
given to the input item.  CHOICES is a list of pairs:

  \(id . description)

The descriptions fill the selection box while the ids are passed
to the web server.  If SELECTED tells which item of the radio
buttons groub should be selected by default.  If VERTICAL is true
lays the radio buttons vertically rather than horizontally."
   (flet ((make-radio (choice)
	    (destructuring-bind (id . description) choice
	      (if (and selected
		       (eq id selected))
		  (html (:label
			 ((:input type "radio" name name value id checked t))
			 (:princ-safe description)))
		  (html (:label
			 ((:input type "radio" name name value id))
			 (:princ-safe description)))))
	    (if vertical
		(html :br))))
     (dolist (c choices)
       (make-radio c))))

(defun html-checkbox (name &key checked label (value t))
  (flet ((make-chekbox ()
	   (if checked
	       (html ((:input type "checkbox" name (string-downcase name) value value checked t)))
	       (html ((:input type "checkbox" name (string-downcase name) value value))))))
    (if label
	(html ((:label)
	       (make-chekbox)
	       (:princ-safe label)))
	(make-chekbox))))

(defmacro html-table ((&rest args) &body body)
  "Generate an HTML table with ARGS and run BODY with TR defined
locally as a macro that outputs table's TR elements of class ODD
or EVEN, according to their position within tha table."
  `(be table-row-number 0
     (declare (ignorable table-row-number)
	      (special table-row-number))
     ;; This TR macrolet is for backward compatibility.  New code
     ;; should use the :TR* element instead (see below).
     (macrolet ((tr (&rest row)
		  (with-gensyms (output-row)
		    `(flet ((,output-row ()
			      (html ,@row)))
		       (if (oddp (incf table-row-number))
			   (html ((:tr class "odd")
				    (,output-row)))
			   (html ((:tr class "even")
				  (,output-row)))))))
		(thead (&rest headers)
		  `(html
		    (:thead
		     (:tr ,@(mapcar #'(lambda (hdr)
					(if (consp hdr)
					    hdr
					    `(:th ,hdr)))
				    headers))))))
       (html
	((:table ,@args)
	 ,@body)))))

(defvar *default-form-class* "form"
  "The default CSS class of the form tables generated by WITH-HTML-FORM." )

;; The with- name is just to get the right indentation in Emacs
(defmacro with-html-form ((url &key (class *default-form-class*) submit reset style (method "post")) &rest fields)
  "Creates an HTML form of CSS class CLASS.  SUBMIT is the label
on the OK button.  If RESET is true add a reset button.  If RESET
is a string use that as the label on the reset button.  Each
field in FIELDS is a list of which the first element is a string
\(the field description) and the rest is HTML code to appear next
to it.  Local to this block the macro INPUT is defined.  If the
first element is the keyword :HIDDEN, the field won't have a
table entry but it will be hidden from the user; in this case the
other elements are the name and value of the field."
  (be hidden-fields (remove-if #'stringp fields :key #'car)
      table-fields (remove-if #'keywordp fields :key #'car)
    `(macrolet ((input (type name &rest args)
		  `(html ((:input type ,type name ,(string-downcase name) ,@args)))))
       (html
	((:div :class ,class ,@(when style (list :style style)))
	 ((:form action ,(if (eq url :current)
			     '(url :current)
			     url)
		 method ,method)
	  ,@(mapcar #'(lambda (f)
			`((:input type "hidden" name ,(string-downcase (cadr f)) value ,(caddr f))))
		    hidden-fields)
	  ((:table)
	   ,@(mapcar #'(lambda (f)
			 `(html (:tr (:td ,(car f)) (:td ,@(cdr f)))))
		     table-fields))
	  ((:input type "submit" ,@(when submit
					 (list 'value submit))))
	  ,@(when reset
		  `(((:input type "reset" ,@(when (stringp reset)
						  (list 'value reset))))))))))))

(defun html-bookmarks (req ent selector bookmarks)
  (setf selector (string-downcase selector))
  (be current (query-value req selector)
    (html
     ((:ul id "navibar")
      (dolist (bm bookmarks)
	(if (eq (car bm) current)
	    (html ((:li class "current")
		   (:princ-safe (cdr bm))))
	    (html (:li ((:a href (encode-url req ent :current
					     (cons (cons selector (car bm))
						   (remove-if #'(lambda (pair)
								  (string-equal (car pair) selector))
							      (request-query req)))))
			(:princ-safe (cdr bm))))))))
     ((:div id "pageline")))))

(defvar *button-style* :fancy-link
  "Type of HTML rendering for button items.  This can
be :IMPLICIT-FORM or :FANCY-LINK.  See the function BUTTON for
further details.  Both require support from the stylesheet,
though a bit less for the implicit form.  Something like this
would do:

form.button {
  display: inline;
  margin: .5em;
}


a.button {
  border: outset;
  display: inline;
  font-weight: bold;
  padding: .3em .8em .5em .8em;
  background-color: #BBB;
  margin: .3em .5em .3em .5em;
  color: #000;
  text-decoration: none;
}

a.button img {
  border: none;
  padding: 0;
  margin: 0;
}

a.button:hover {
  background-color: #DDD;
}

a.button:active {
  border: none;
  background-color: #DDD;
}")

(defun button (label url &key description colour image)
  "Create an HTML link to URL having the look of a button.
If IMAGE is available use it, otherwise render LABEL as a rectangular
button.  IMAGE can be a string or a pathname."
  (ecase *button-style*
    (:implicit-form
     (html
      ((:form action url method "post" class "button")
       (if image
	   (html
	    ((:input type "image" title description src image)))
	   (html
	    ((:input type "submit" value label
		     title description
		     style (if colour
			       (format nil "background:~A" colour)
			       ""))))))))
    (:fancy-link
     (if image
	 (html
	  ((:a href url class "button" title description)
	   ((:img src image))))
	 (html
	  ((:a href url class "button" title description
	       style (if colour
			 (format nil "background: ~A" colour)
			 ""))
	   (:princ-safe label)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass better-password-authoriser (password-authorizer)
  ()
  (:documentation
   "Convenience base class to diversify AUTHORIZE method (below)."))

(defclass crypt-password-authoriser (better-password-authoriser)
  ()
  (:documentation
   "Access authoriser for Unix-like accounts.  This class is similar
to PASSWORD-AUTHORISER but passwords in ALLOWED are ancrypted with
Unix crypt(3).  User passwords are supposed to be taken from
/etc/passwd \(or /etc/master.passwd) or encrypted in the same way."))

(defgeneric authoriser-get-user-password (authoriser user-name)
  (:documentation
   "Return the password associated with USER-NAME."))

(defmethod authoriser-get-user-password ((authoriser better-password-authoriser) (user-name string))
  "Default method that looks up USER-NAME in the alist contained
by the ALLOWED slot of AUTHORISER."
  (cdr (assoc user-name (net.aserve::password-authorizer-allowed authoriser)
	      :test #'string=)))

(uffi:def-function ("crypt" %crypt) ((key :cstring) (salt :cstring))
  :returning :cstring
  :module "crypt")

;; NOTE: crypt(3) is not thread safe, as it returns a pointer to a
;; static buffer, thus we need to synchronise its calls.
(be crypt-lock (make-lock "crypt(3) lock")
  (defun crypt (key salt)
    "Encrypt KEY using SALT."
    (with-lock-held (crypt-lock)
      (uffi:with-cstring (key-cstring key)
	(uffi:with-cstring (salt-cstring salt)
	  (uffi:convert-from-cstring
	   (%crypt key-cstring salt-cstring)))))))

(defmethod authoriser-deny-page ((auth better-password-authoriser))
  (html
   (:html
    (:body
     (:h1 "Access denied")
     "The access to this page is restricted." :br
     "You need to enter a valid name and password to see it."))))

(defmethod authoriser-check-password ((auth better-password-authoriser) name password)
  (declare (ignore auth))
  (awhen (authoriser-get-user-password auth name)
    (string= it password)))

(defmethod authoriser-check-password ((auth crypt-password-authoriser) name password)
  (declare (ignore auth))
  (awhen (authoriser-get-user-password auth name)
    (string= it (crypt password it))))

(defmethod authorize ((auth better-password-authoriser)
		      (req http-request)
		      (ent net.aserve::entity))
  "Check if REQ is valid request, return T if yes, :DONE if we request
for a new name and password."
  (multiple-value-bind (name password) (get-basic-authorization req)
    (if (and name
	     (authoriser-check-password auth name password))
	t
	;; no valid name/password given, ask for it (again)
	(with-http-response (req net.aserve::*dummy-computed-entity*
				 :response *response-unauthorized*
				 :format :text)
	  (set-basic-authorization req (net.aserve::password-authorizer-realm auth))
	  (when (member ':use-socket-stream (request-reply-strategy req))
	    (setf (request-reply-strategy req)
		  '(:string-output-stream
		    :post-headers)))
	  (with-http-body (req net.aserve::*dummy-computed-entity*)
	    (authoriser-deny-page auth))
	  :done))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; pity this is not external
(defun redirect-to (req ent url)
  (net.aserve::redirect-to req ent url))

(defun write-html-sequence (sequence &key (stream *html-stream*) (start 0) end)
  "Write a sequence to HTML stream.  This is more general than
WRITE-HTML-STRING and thus more appropriate to write arbitrary
sequences."
  (unless end
    (setf end (length sequence)))
  (be type (array-element-type sequence)
    (cond ((subtypep type (stream-element-type stream))
	   (write-sequence sequence stream
			   :start start
			   :end end))
	  ((subtypep type '(unsigned-byte 8))
	   (loop
	      for i from start below end
	      do (write-char (code-char (aref sequence i))
			     stream)))
	  (t
	   (loop
	      for i from start below end
	      do (write-byte (char-code (schar sequence i))
			     stream))))))

(defun compress-css-stream (in out)
  (flet ((copy-string ()
	   (loop
	      for c = (read-char in nil)
	      while c
	      do (write-char c out)
	      when (char= #\\ c)
	      do (write-char (read-char in) out)
	      until (char= c #\")))
	 (skip-comment ()
	   (loop
	      for c = (read-char in nil)
	      while c
	      when (and (char= #\* c)
			(char= #\/ (peek-char nil in)))
	      do
		(read-char in)
		(return)))
	 (skip-whitespace ()
	   (peek-char t in nil)))
    (loop
       for c = (read-char in nil)
       while c
       do (case c
	    (#\"
	     (write-char c out)
	     (copy-string))
	    (#\/
	     (if (char= #\* (peek-char nil in))
		 (progn (read-char in)
			(skip-comment)
			(skip-whitespace))
		 (write-char c out)))
	    ((#\space #\tab #\newline #\return)
	     (write-char #\space out)
	     (skip-whitespace))
	    (t (write-char c out))))))

(defun compress-css (css)
  "Remove superfluous whitespace and comments from CSS, which is
supposed to be a Cascading Style Sheet in the form of a string."
  (with-input-from-string (in css)
    (with-output-to-string (out)
      (compress-css-stream in out))))

;; The :TR* pseudo HTML tag.  This special <tr> gets class "even" or
;; "odd" depending on the position in the table.  It works only within
;; HTML-TABLE macro as it uses TABLE-ROW-NUMBER.
(eval-when (:compile-toplevel :load-toplevel)
  (net.html.generator::def-special-html :tr*
      #'(lambda (ent args argsp form)
	  (declare (ignore ent argsp))
	  `(html (,(if (getf args 'class)
		       `(:tr ,@args)
		       `(:tr class (if (evenp (incf table-row-number)) "even" "odd") ,@args)) ,@form)))
    #'net.html.generator::html-standard-print))

