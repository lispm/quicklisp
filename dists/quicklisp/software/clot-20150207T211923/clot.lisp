;;;  clot.lisp --- Common Lisp plotting primitives

;;;  Copyright (C) 2005, 2006, 2007, 2008, 2009, 2010 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: clot

#+cmu (ext:file-comment "$Module: clot.lisp $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA


(in-package :cl-user)

(defpackage :clot
  (:use :common-lisp :cl-gd :sclf)
  (:shadow #:draw-line
	   #:draw-polygon
	   #:draw-filled-ellipse
	   #:draw-filled-circle
	   #:draw-rectangle
	   #:draw-rectangle*
	   #:draw-freetype-string
	   #:draw-arc
	   #:fill-image
	   #:without-transformations)
  (:export #:plot-line-chart
	   #:plot-pie-chart
	   #:plot-bar-chart
	   #:plot-gantt-chart
	   ;; misc utils
	   #:colour #:find-rgb-colour
	   #:with-translation
	   #:with-rotation
	   #:with-scaling
	   #:with-subimage
	   #:fill-image
	   #:view-chart
	   #:*default-background-colour*))

(in-package :clot)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Colours

(defvar *default-background-colour* "ivory")

(deflazy *rgb-table*
    (or (ignore-errors (read-rgb-table #P"/usr/X11R6/lib/X11/rgb.txt"))
	(read-rgb-table #P"/usr/local/lib/X11/rgb.txt")))

(defun find-rgb-colour (name)
  (gethash name (force *rgb-table*)))

(defgeneric colour (code &key image alpha)
  (:documentation
   "Return a possibily new allocated colour indicated by CODE.
ALPHA is in the range 0 to 1.  IMAGE defaults to
*DEFAULT-IMAGE*."))

(defmethod colour ((code vector) &key (image *default-image*) alpha)
  (find-color (elt code 0)
	      (elt code 1)
	      (elt code 2)
	      :image image :resolve t
	      :alpha (when (and alpha
				(not (= alpha 1)))
		       (round (* alpha 127)))))

(defmethod colour ((code list) &key (image *default-image*) alpha)
  (colour (coerce code 'vector) :image image :alpha alpha))

(defmethod colour ((code null) &key (image *default-image*) alpha)
  (colour "black" :image image :alpha alpha))

(defmethod colour ((value integer) &key (image *default-image*) alpha)
  ;; This appears to be the internal type of GD colours.
  (declare (ignore image alpha))
  value)

(defmethod colour ((colour cl-gd::anti-aliased-color) &key (image *default-image*) alpha)
  (declare (ignore image alpha))
  colour)

(defmethod colour ((name string) &key (image *default-image*) alpha)
  (be rgb (find-rgb-colour name)
    (assert rgb (rgb) "Colour ~S not in the RGB database." name)
    (colour rgb :image image :alpha alpha)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fonts

(defvar *font-search-path*
  '("/usr/X11R6/lib/X11/fonts/**/*.ttf"
    "/usr/local/lib/X11/fonts/**/*.ttf"))

(defvar *axis-font* "Vera")

(defun find-font (name)
  (loop
     for path in *font-search-path*
     for pathname = (merge-pathnames name path)
     for choices = (directory pathname)
     when choices
     return (car choices)))

(defvar *font-cache* (make-hash-table :test 'equal))

(defun font-path (font)
  (if (stringp font)
      (or (gethash font *font-cache*)
	  (setf (gethash font *font-cache*)
		(find-font font)))
      font))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Some basic coordinate transformation primitives as
;;  cl-gd:with-transformation is useless for my purposes

(eval-when (:load-toplevel :compile-toplevel :execute)
(defun make-ctm-matrix (&optional elements)
  "Make two dimensional array for a Coordinates Transformation Matrix."
  (if elements
      (make-array '(3 3)
		  :element-type 'number
		  :initial-contents elements)
      (make-array '(3 3)
		  :element-type 'number)))

(defun make-ctm (a b c d tx ty)
  (make-ctm-matrix (list (list a  b  0)
			 (list c  d  0)
			 (list tx ty 1)))))

(defconst +identity-ctm+
  (make-ctm 1 0
	    0 1
	    0 0))

(defun transpose-ctm (ctm)
  "Transpose the matrix CTM."
  (be new (make-ctm-matrix)
    (macrolet ((copy (x y)
		 `(setf (aref new ,x ,y)
			(aref ctm ,y ,x))))
      (copy 0 0) (copy 1 0) (copy 2 0)
      (copy 0 1) (copy 1 1) (copy 2 1)
      (copy 0 2) (copy 1 2) (copy 2 2))
    new))

(defun submatrix (ctm exclude-i exclude-j)
  "Return the list of elements of CTM excluding those on column
EXCLUDE-I and row EXCLUDE-J."
  (loop
     for j from 0 to 2
     append (loop
	       for i from 0 to 2
	       unless (or (= exclude-i i)
			  (= exclude-j j))
	       collect (aref ctm j i))))

(defun submatrix-determinant (ctm exclude-i exclude-j)
  "Return the determinant of the submatrix of CTM obtained
removing all the elements lying on column EXCLUDE-I and row
EXCLUDE-J."
  (labels ((det (a b  c d)
	     ;; return the determinant of a 2x2 matrix.
	     (- (* a d) (* b c))))
    (apply #'det (submatrix ctm exclude-i exclude-j))))

(defun ctm-determinant (ctm)
  "Return the determinant of the matrix CTM."
  (+ (* (submatrix-determinant ctm 0 0)
	(aref ctm 0 0))
     (* (submatrix-determinant ctm 1 0)
	(aref ctm 0 1)
	-1)
     (* (submatrix-determinant ctm 2 0)
	(aref ctm 0 2))))

(defun invert-ctm (ctm)
  (be ctm-det (ctm-determinant ctm)
      new (make-ctm-matrix)
      (assert (not (zerop ctm-det)))
      (loop
	 for j from 0 to 2
	 do (loop
	       for i from 0 to 2
	       for det = (submatrix-determinant ctm i j)
	       do (setf (aref new j i)
			(* (/ det ctm-det)
			   (if (oddp (+ i j))
			       -1 1)))))
      (transpose-ctm new)))


(defvar *default-ctm* +identity-ctm+)

(defstruct (clot-image (:include cl-gd::image))
  (ctm *default-ctm*)
  ;; these wouldn't be necessary if gd accepted my CTM
  (angle 0)
  (x-scaling 1)
  (y-scaling 1)
  cartesian-mode)

(defun ctm-multiply (m1 m2)
  "Multiply two Coordinates Transformation Matrices."
  (macrolet ((make-mults ()
	       (flet ((mult (i j)
			`(+ (* (aref m1 ,i 0) (aref m2 0 ,j))
			    (* (aref m1 ,i 1) (aref m2 1 ,j))
			    (* (aref m1 ,i 2) (aref m2 2 ,j)))))
		 (cons 'list
		       (loop
			  for i from 0 below 3
			  collect (cons 'list
					(loop
					   for j from 0 below 3
					   collect (mult i j))))))))
    (make-ctm-matrix (make-mults))))

(defun translation-ctm (x y)
  (make-ctm 1 0
	    0 1
	    x y))

(defun scaling-ctm (x y)
  (make-ctm x 0
	    0 y
	    0 0))

(defun rotation-ctm (radians)
  (make-ctm (cos radians)	(sin radians)
	    (- (sin radians))	(cos radians)
	    0			0))

(defmacro with-cartesian-coordinates ((&key (image '*default-image*)) &body body)
  `(let-slots ((clot-image-cartesian-mode t)) ,image
     ,@body))

(defmacro with-ctm ((ctm &key (image '*default-image*)) &body body)
  "Execute BODY applying the Coordinates Transformation Matrix
CTM to IMAGE.  Any existing ctm is disregarded but replaced on
exit of BODY."
  `(let-slots ((clot-image-ctm ,ctm)) ,image
     ,@body))

(defmacro with-additional-ctm ((ctm &key (image '*default-image*)) &body body)
  "Execute BODY applying the Coordinates Transformation Matrix
CTM to IMAGE in addition to the one already applied to it."
  (with-gensyms (img)
    `(be ,img ,image
       (with-ctm ((ctm-multiply ,ctm (clot-image-ctm ,img))
		  :image ,img)
	 ,@body))))

(defmacro with-translation ((x-offset y-offset &key (image '*default-image*)) &body body)
  "Execute BODY with translating the x and y coordinates of IMAGE
of respectively X-OFFSET and Y-OFFSET."
  `(with-additional-ctm ((translation-ctm ,x-offset ,y-offset) :image ,image)
     ,@body))

(defmacro with-scaling ((x-scale y-scale &key (image '*default-image*)) &body body)
  "Execute BODY with X-SCALE and Y-SCALE scaling transformation
to IMAGE coordinates."
  (with-gensyms (img sx sy)
    `(let ((,img ,image)
	   (,sx ,x-scale)
	   (,sy ,y-scale))
       (with-additional-ctm ((scaling-ctm ,sx ,sy) :image ,img)
	 (let-slots ((clot-image-x-scaling (* ,sx (clot-image-x-scaling ,img)))
		     (clot-image-y-scaling (* ,sy (clot-image-y-scaling ,img))))
	     ,img
	   ,@body)))))

(defmacro with-rotation ((angle &key (image '*default-image*)) &body body)
  "Execute BODY with ANGLE transformation to IMAGE coordinates."
  (with-gensyms (img teta)
    `(let ((,img ,image)
	   (,teta ,angle))
       (with-additional-ctm ((rotation-ctm ,teta) :image ,img)
	 (let-slots ((clot-image-angle (+ ,teta (clot-image-angle ,img)))) ,img
	   ,@body)))))

(defmacro without-transformations ((&optional (image '*default-image*)) &body body)
  "Execute BODY without any transformation to IMAGE coordinates."
  `(with-ctm (*default-ctm* ,image)
     ,@body))

(defmacro with-subimage ((v1-x v1-y v2-x v2-y &key (image '*default-image*)) &body body)
  "Execut body working on a part of IMAGE.  The coordinate system
is scaled accordingly."
  (with-gensyms (x1 y1 x2 y2 img)
    `(let ((,x1 ,v1-x)
	   (,y1 ,v1-y)
	   (,x2 ,v2-x)
	   (,y2 ,v2-y)
	   (,img ,image))
       (with-translation (,x1 ,y1 :image ,img)
	 (with-scaling ((/ (abs (- ,x2 ,x1)) (image-width ,img))
			(/ (abs (- ,y2 ,y1)) (image-height ,img))
			:image ,img)
	   ,@body)))))

(defun transform (x y ctm)
  "Apply CTM transformation to X and Y coordinates."
  (let* ((a (aref ctm 0 0))
	 (b (aref ctm 0 1))
	 (c (aref ctm 1 0))
	 (d (aref ctm 1 1))
	 (tx (aref ctm 2 0))
	 (ty (aref ctm 2 1)))
    (values (+ (* x a) (* y c) tx)
	    (+ (* x b) (* y d) ty))))

(defun itransform (x y ctm)
  "Inverse of TRANSFORM."
  (transform x y (invert-ctm ctm)))

(defun as32bit (x)
  (logand (round x) #xFFFF))

(defun transform-image-coordinates (x y image)
  (multiple-value-bind (x y)
      (transform x y (clot-image-ctm image))
    (values (as32bit x)
	    (as32bit y))))

(defun radians->degrees (radians)
  (* (/ radians pi) 180))

(defun transform-image-angle (angle image)
  (+ angle (clot-image-angle image)))

(defun image-y (image y)
  "Just height - Y if IMAGE is in cartesian mode otherwise Y
itself."
  (if (clot-image-cartesian-mode image)
      (- (image-height image) y)
      y))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Some wrappers of CL-GD functions to make use of CTMs

(defun draw-line (from-x from-y to-x to-y &key (color *default-color*) (image *default-image*) thickness)
  (multiple-value-bind (x1 y1)
      (transform-image-coordinates from-x from-y image)
    (multiple-value-bind (x2 y2)
	(transform-image-coordinates to-x to-y image)
      (cl-gd:with-thickness ((or thickness (thickness image)) :image image)
	(cl-gd::gd-image-line (cl-gd::img image)
			      x1 (image-y image y1)
			      x2 (image-y image y2)
			      (colour color :image image))))))

(defun draw-polygon (vertices &key (color *default-color*) (image *default-image*) filled)
  (cl-gd:draw-polygon (loop
			 for x1 = (pop vertices)
			 for y1 = (pop vertices)
			 while x1
			 append (multiple-value-bind (x y)
				    (transform-image-coordinates x1 y1 image)
				  (list x (image-y image y))))
		      :filled filled :image image
		      :color (colour color :image image)))

(defun draw-filled-ellipse (center-x center-y width height  &key
			    (color *default-color*) (image *default-image*))
  (multiple-value-bind (x y)
      (transform-image-coordinates center-x center-y image)
    (let ((w (* width (clot-image-x-scaling image)))
	  (h (* height (clot-image-y-scaling image))))
      (cl-gd::gd-image-filled-ellipse (cl-gd::img image) x (image-y image y) w h
				      (colour color :image image)))))

(defun draw-filled-circle (center-x center-y radius  &key
			   (color *default-color*) (image *default-image*)
			   (preserve-aspect t))
  (multiple-value-bind (x y)
      (transform-image-coordinates center-x center-y image)
    (multiple-value-bind (w h)
	(transform-image-coordinates (* 2 radius) (* 2 radius) image)
      (when preserve-aspect
	(setf w (/ (+ w h)))
	(setf h w))
      (cl-gd::gd-image-filled-ellipse
       (cl-gd::img image) x (image-y image y)
       (as32bit w)
       (as32bit h)
       (colour color :image image)))))

(defun draw-bullet (center-x center-y radius  &key
		    (color *default-color*) (image *default-image*))
  "Like draw filled-circle but RADIUS is expressed in raw IMAGE
pixels (they are not transformed by any CTM)."
  (multiple-value-bind (x y)
      (transform-image-coordinates center-x center-y image)
    (cl-gd::gd-image-filled-ellipse
     (cl-gd::img image) x (image-y image y)
     (as32bit (* 2 radius))
     (as32bit (* 2 radius))
     (colour color :image image))))

;; We have to rewrite the rectangle primitives because the GD ones are
;; not aware of the rotation angle so their resulting rectangle has
;; always sides parallel to the image.  This is not strictly necessary
;; for plotting charts but it's fun to do.

(defun draw-rectangle (points &key filled color thickness (image *default-image*))
  (destructuring-bind (x1 y1 x2 y2) points
    (cl-gd:with-thickness ((or thickness (thickness image)) :image image)
      (draw-polygon (list x1 y1
			  x2 y1
			  x2 y2
			  x1 y2)
		    :filled filled :color color :image image))))

(defun draw-rectangle* (point1-x point1-y point2-x point2-y &key
			filled thickness
			(color *default-color*) (image *default-image*))
  (draw-rectangle (list point1-x point1-y point2-x point2-y)
		  :filled filled :color color :thickness thickness :image image))

(defun invert-colour (colour &key (image *default-image*))
  (setf colour (colour colour :image image))
  (flet ((compon (name)
	   (- 256 (color-component name colour :image image))))
    (find-color (compon :red)
		(compon :green)
		(compon :blue) :image image :resolve t)))

(defun point (x y)
  (cons x y))

(defun point-x (point)
  (car point))

(defun point-y (point)
  (cdr point))

(defun points-distance (p1 p2)
  (sqrt (+ (expt (abs (- (point-x p1) (point-x p2))) 2)
	   (expt (abs (- (point-y p1) (point-y p2))) 2))))

(defun physical-bbox-to-virtual (bbox image)
  (be ctm (invert-ctm (clot-image-ctm image))
    (flet ((xform (point)
	     (multiple-value-list
	      (transform (elt bbox (* point 2))
			 (elt bbox (1+ (* point 2)))
			 ctm))))
      (concatenate 'vector (mapcan #'xform '(0 1 2 3))))))

(defun bbox->dimensions (bbox)
  "Given a bounding box BBOX as returned by DRAW-FREETYPE-STRING,
return a list of two values: the width and the height."
  (list (points-distance (point (elt bbox 0)
				(elt bbox 1))
			 (point (elt bbox 2)
				(elt bbox 3)))
	(points-distance (point (elt bbox 0)
				(elt bbox 1))
			 (point (elt bbox 6)
				(elt bbox 7)))))

(defun draw-freetype-string (x y string &key (anti-aliased t) (point-size 12.0d0) (angle 0.0d0)
			     (convert-chars t) line-spacing (font-name *default-font*) do-not-draw
			     (color *default-color*) (image *default-image*)
			     (justification :bottom-left))
  "Just like CL-GD:DRAW-FREETYPE-STRING but make use of the CTM."
  (setf color (colour color :image image))
  (setf font-name (font-path font-name))
  ;; this is not perfect; it takes in count only the width, as we
  ;; can't modify the aspect ratio of fonts
  (setf point-size (as32bit (* point-size (clot-image-x-scaling image))))
  (flet ((draw (x y not-draw)
	   (cl-gd:draw-freetype-string x y string
				       :anti-aliased anti-aliased
				       :point-size point-size
				       :angle (transform-image-angle angle image)
				       :convert-chars convert-chars
				       :line-spacing line-spacing
				       :font-name font-name
				       :do-not-draw not-draw
				       :color color
				       :image image)))
    ;; we transform the coordinates to the fisical pixels on the
    ;; image; position (0 0) is on the top-left corner
    (multiple-value-bind (tx ty)
	(transform-image-coordinates x y image)
      (setf ty (image-y image ty))
      (let ((dx 0)
	    (dy 0))
	(unless (eq justification :bottom-left)
	  ;; the bbox is a vector of four corners (eight elements):
	  ;; bottom left, bottom right, top right, top left
	  (let* ((bbox (draw tx ty t))
		 ;; the starting point to draw is the baseline; find
		 ;; how deep the letters go
		 (depth (round (points-distance (point (elt bbox 0)
						       (elt bbox 1))
						(point tx ty)))))
	    (destructuring-bind (w h) (mapcar #'round (bbox->dimensions bbox))
	      (case justification
		(:top-left
		 (setf dy (- h depth)))
		(:center-left
		 (setf dy (- (round h 2) depth)))
		(:bottom-right
		 (setf dx (- w)))
		(:top-right
		 (setf dx (- w))
		 (setf dy (- h depth)))
		(:center-right
		 (setf dx (- w))
		 (setf dy (- (round h 2) depth)))
		(:center
		 (setf dx (- (round w 2)))
		 (setf dy (- (round h 2) depth)))
		(:top-center
		 (setf dx (- (round w 2)))
		 (setf dy (- h depth)))
		(:bottom-center
		 (setf dx (- (round w 2))))))))
	;; bring back the bounding box coordinates to what the program
	;; expects (not the physical image coordinates)
	(physical-bbox-to-virtual
	 (draw (+ tx dx) (+ ty dy) do-not-draw) image)))))

(defun draw-arc (center-x center-y width height start end &key
		 straight-line center-connect filled (color *default-color*) (image *default-image*))
  (check-type image clot-image)
  (multiple-value-bind (x y)
      (transform-image-coordinates center-x center-y image)
    (let ((w (as32bit (* width (clot-image-x-scaling image))))
	  (h (as32bit (* height (clot-image-y-scaling image)))))
      ;; for some peculiar reason angles in gd-image(-filled)-arc are
      ;; measured clockwise unless we swap start and end
      (let ((e (as32bit (mod (radians->degrees (transform-image-angle (- start) image)) 360)))
	    (s (as32bit (mod (radians->degrees (transform-image-angle (- end) image)) 360))))
	(when (and (= s e 0)
		   (not (= start end)))
	  (setf e 360))
	(cond ((not (or straight-line filled center-connect))
	       (cl-gd::gd-image-arc (cl-gd::img image) x (image-y image y) w h s e
				    (colour color :image image)))
	      (t
	       (cl-gd::gd-image-filled-arc (cl-gd::img image) x (image-y image y) w h s e
					   (colour color :image image)
					   (logior (if straight-line cl-gd::+gd-chord+ 0)
						   (if filled 0 cl-gd::+gd-no-fill+)
						   (if center-connect cl-gd::+gd-edged+ 0)))))))))

;; it may not be thread safe -wcp1/3/07.
(defmacro with-default-colour ((colour &key (image *default-image*)) &body body)
  `(let ((*default-color* (colour ,colour :image ,image)))
    ,@body))

(defun fill-image (x y &key border (color *default-color*) (image *default-image*))
  (check-type border (or null integer))
  (check-type image clot-image)
  (multiple-value-bind (tx ty)
      (transform-image-coordinates x y image)
    (if border
        (cl-gd::gd-image-fill-to-border (cl-gd::img image) tx (image-y image ty)
					border (colour color :image image))
        (cl-gd::gd-image-fill (cl-gd::img image) tx (image-y image ty)
			      (colour color :image image)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Redefine CL-GD:CREATE-IMAGE to create CLOT images instead
(eval-when (:compile-toplevel :load-toplevel)
  (fmakunbound 'cl-gd:create-image))

(defun cl-gd:create-image (width height &optional true-color)
  "Allocates and returns a GD image structure with size WIDTH x
HEIGHT. Creates a true color image if TRUE-COLOR is true. You are
responsible for destroying the image after you're done with it. It is
advisable to use WITH-IMAGE instead."
  (check-type width integer)
  (check-type height integer)
  (be image-ptr (if true-color
		    (cl-gd::gd-image-create-true-color width height)
		    (cl-gd::gd-image-create width height))
    (when (cl-gd::null-pointer-p image-ptr)
      (error "Could not allocate image of size ~A x ~A" width height))
    (be image (make-clot-image :img image-ptr)
      image)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Some utilities

(defun values-metadata (values)
  "Return the metadata of DATA in several values.  These
informations are necessary to plot a chart of any kind."
  (values (reduce #'min values)
	  (reduce #'max values)
	  ;; the length of the X axis
	  (length values)))

(defun pie-metadata (data)
  (be values (mapcar #'caddr data)
    (values (mapcar #'car data)		; the labels
	    (mapcar #'cadr data)	; the colours
	    values
	    (length values)		; number of slices
	    (reduce #'min values)
	    (reduce #'max values)
	    (reduce #'+ values))))

(defun chart-metadata (data)
  "Return the metadata of DATA in several values.  These
informations are necessary to plot a chart of any kind."
  (when data
    (be just-the-values (mapcar #'cddr data)
      (values (mapcar #'car data)	; the labels
	      (mapcar #'cadr data)	; the colours
	      just-the-values		; the values
	      (length data)		; number of data rows
	      ;; the minimum value
	      (reduce #'min (mapcar #'(lambda (values)
					(reduce #'min values))
				    just-the-values))
	      ;; the maximum value
	      (reduce #'max (mapcar #'(lambda (values)
					(reduce #'max values))
				    just-the-values))
	      ;; the length of the X axis
	      (length (car just-the-values))))))

(defun gantt-metadata (data)
  ;; get the first span as start values
  (be a-nonempty-schedule (find-if #'cdr data)
    (destructuring-bind (start colour duration &optional label)
	(if a-nonempty-schedule
	    (cadr a-nonempty-schedule)
	    (list 0 nil 0))
      (declare (ignore colour label))
      (be end (+ start duration)
	  shortest-duration duration
	  longest-duration duration
	(loop for resource in data
	   do (loop
		 for (beginning colour duration &optional label) in (cdr resource)
		 for finish = (+ beginning duration)
		 when (< beginning start)
		 do (setf start beginning)
		 when (< end finish)
		 do (setf end finish)
		 when (< duration shortest-duration)
		 do (setf shortest-duration duration)
		 when (< longest-duration duration)
		 do (setf longest-duration duration)))
	(values (length data)
		(mapcar #'car data)
		start
		end
		shortest-duration
		longest-duration)))))

(defun change-colour-brightness (colour delta &key (image *default-image*))
  "Return a colour which is a brightened/darkened version of
COLOUR.  DELTA should be in the range -1 < x < 1."
  (setf colour (colour colour :image image))
  (flet ((compon (name)
	   (max 0 (min 255
		       (round
			(* (color-component name colour :image image)
			   (1+ delta)))))))
    (find-color (compon :red)
		(compon :green)
		(compon :blue) :image image :resolve t)))

(defun draw-line-on-chart (values &key fill bullets thickness (image *default-image*))
  (when (eq bullets t)
    (setf bullets *default-color*))
  (do* ((previous-y (pop values) y)
	(y #1=(pop values) #1#)
	(x 1 (1+ x)))
       ((not y))
    (if fill
	(draw-polygon (list (1- x) 0
			    (1- x) previous-y
			    x y
			    x 0)
		      :filled t
		      :image image)
	(progn
	  (draw-line (1- x) previous-y x y :thickness thickness :image image)
	  (when (and bullets values)
	    (draw-bullet x y 4 :color bullets :image image))))))

(defun draw-bars-on-chart (values &key (offset 0) (width 1) border (image *default-image*))
  (when (eq border t)
    (setf border (colour "black" :image image)))
  (do ((y #1=(pop values) #1#)
       (x 0 (1+ x)))
      ((not y))
    (be bbox (list (+ x offset) 0
		   (+ x offset) y
		   (+ x width offset) y
		   (+ x width offset) 0)
      (draw-polygon bbox :filled t :image image)
      (when border
	(draw-polygon bbox :color border :image image)))))

(defun draw-histogram-on-chart (values &key fill (image *default-image*))
  (when (eq fill t)
    (setf fill (change-colour-brightness *default-color* .9 :image image)))
  (flet ((draw-slice (x y)
	   (when fill
	     (draw-rectangle (list x y
				   (1+ x) 0)
			   :filled t
			   :color fill
			   :image image))
	   (draw-line x y (1+ x) y :image image)))
    (draw-slice 0 (car values))
  (do* ((previous-y (pop values) y)
	(y #1=(pop values) #1#)
	(x 1 (1+ x)))
       ((not y))
    (draw-slice x y)
    (draw-line x previous-y x y :image image))))

(defun read-rgb-table (pathname)
  (be table (make-hash-table :test 'equalp)
    (with-open-file (stream pathname)
      (loop
	 for line = (read-line stream nil)
	 while line
	 do
	 (awhen (position #\! line)
	   (setf line (subseq line 0 it)))
	 (with-input-from-string (stream line)
	   (let* ((red (read stream nil))
		  (green (read stream nil))
		  (blue (read stream nil))
		  (descr (read-line stream nil)))
	     (when (and red green blue descr)
	       (be name (string-trim-whitespace descr)
		 (setf (gethash name table)
		       (vector red green blue))))))))
    table))

(defun draw-x-axis (x-labels label-position label-angle vgrid font-size vertical-offset)
  (unless (< (length x-labels) 2)
    (loop
       ;; notch-length is a negative value!
       with notch-length = (/ vertical-offset 3)
       with step = (if (eq label-position :between-notches)
		       (/ (image-width)
			  (length x-labels))
		       (/ (image-width)
			  (1- (length x-labels))))
       for i from 0
       for label in x-labels
       for x = (* i step)
       do (draw-line x 0 x notch-length)
       if (eq label-position :on-notch)
       do (draw-freetype-string x (+ vertical-offset (* 1.5 notch-length)) label
				:angle (* pi label-angle)
				:justification :bottom-center
				:point-size font-size)
       else
       do (draw-freetype-string (+ x (/ step 2)) vertical-offset label
				:angle (* pi label-angle)
				:point-size font-size
				:justification :bottom-center)
       do (when vgrid
	    (draw-line x 0 x (image-height) :color vgrid)))))

(defun y-axis-label-positions (min max target-steps)
  "Return two values, a list of numbers to be used as both Y axis
labels and positions, and the number of decimals to render the
labels with.  The length of the list depends on TARGET-STEPS but
it could be rounded to some other value to make the labels look
better."
  ;;  If min=max we can't produce any useful label.
  (if (= min max)
      (values (list min) 0)
      (be* range (abs (- max min))
	   label-distance (/ range target-steps)
	(be significant-digits (truncate (log label-distance 10))
	  (when (< significant-digits 0)
	    (decf significant-digits))
	  (be* rounder (expt 10 significant-digits)
	       step (round-to label-distance rounder)
	    (when (zerop step)
	      (setf step (round-to label-distance (/ rounder 10)))
	      (decf significant-digits))
	    (values (if (< min 0 max)
			(append (loop
				   with values
				   for i from 0 downto min by step
				   do (push i values)
				   finally (return values))
				(cdr (loop
					for i from 0 to max by step
					collect i)))
			(loop
			   for i from (ceiling-to min step) to max by step
			   collect i))
		    (if (< significant-digits 0)
			(- significant-digits)
			0)))))))

(defun compute-axis-best-point-size (labels font max-point-size available-space &key (image *default-image*))
  "Calculate the point size to be used to draw the largest string
of LABELS with FONT within the AVAILABLE-SPACE using at most
MAX-POINT-SIZE.  Return three values the point size and the width
and height of the largest string drawn at that point size."
  (multiple-value-bind (w h) (max-text-dimensions labels font max-point-size :image image)
    (if (> available-space w)
	(values max-point-size w h)
	(be point-size (* max-point-size (/ available-space w))
	  (multiple-value-bind (w h) (max-text-dimensions labels font max-point-size :image image)
	    (values point-size w h))))))


(defun draw-xy-frame (x-labels y-labels &key hgrid vgrid (color *default-color*)
		      (image *default-image*) background
		      (x-label-position :on-notch)
		      (y-label-position :on-notch)
		      (max-point-size 10d0))
  "Draw just the frame for a chart marking the axis accordingly.
The frame is drawn in COLOR.  The X axis will be marked with
X-LABELS, whereas the Y axis will be marked with Y-LABELS.  If
HGRID is provided, draw horizontal lines in the correspondence of
the Y markings across the chart.  If VGRID is provided, draw
vertical lines in the correspondence of the X markings across the
chart.  Both HGRID and VGRID can be just T, in which case a light
gray colour is used.  If BACKGROUND is specified the chart area
is filled with that colour.  If BACKGROUND is T a white colour is
used.  IMAGE is the working image, or the default image if it's
not provided.  The CHART-TYPE can be :LINE or :HISTOGRAM; this
affects the type and position of markings on the X axis."
  (with-default-colour (color :image image)
    (let ((grid-colour "light gray"))
      (when (eq background t)
	(setf background "white"))
      (when (eq vgrid t)
	(setf vgrid grid-colour))
      (when (eq hgrid t)
	(setf hgrid grid-colour))
      (with-default-font (*axis-font*)
	(with-default-image (image)
	  ;; fill the background if required
	  (when background
	    (draw-rectangle* 0 0 (image-width image) (image-height image)
			     :color background :filled t))
	  (multiple-value-bind (ps width height)
	      (compute-axis-best-point-size x-labels *axis-font* max-point-size
					    (/ (image-width image)
					       (length x-labels)))
	    (declare (ignore width))
	    (draw-x-axis x-labels x-label-position 0 vgrid ps (- height)))
	  ;; draw the y axis
	  (be step (/ (image-height image) (length y-labels))
	      font-size (multiple-value-bind (off-x off-y) (transform-image-coordinates 0 0 image)
			  (declare (ignore off-y))
			  (compute-axis-best-point-size y-labels *axis-font* max-point-size
							(- off-x 6)))
	    (loop
	       for pos from 0
	       for label in y-labels
	       for y = (* pos step)
	       do
		 (draw-line 0 y -5 y)
		 (when hgrid
		   (draw-line 0 y (image-width image) y :color grid-colour))
		 (draw-freetype-string -5 (if (eq y-label-position :on-notch)
					      y
					      (+ y (/ step 2)))
				       label
				       :justification :center-right
				       :point-size font-size)))))
      ;; Draw the actual frame border.  This is last as to be able
      ;; to cover what has been drawn this far.
      (draw-rectangle* 0 0 (image-width image) (image-height image) :image image))))


(defun draw-chart-frame (x-labels min max &key hgrid vgrid (color *default-color*)
			 (image *default-image*) background (chart-type :line))
  (multiple-value-bind (y-label-positions decimals)
      (y-axis-label-positions min max 10)
    (be y-labels (loop
		    for pos in y-label-positions
		    collect (if (zerop decimals)
				(format nil "~D" pos)
				(format nil "~,VF" decimals pos)))
      (draw-xy-frame x-labels y-labels :hgrid hgrid :vgrid vgrid :color color :image image
		     :background background
		     :x-label-position (if (eq chart-type :line)
					   :on-notch
					   :between-notches)))))

(defun string-graphical-dimensions (string font point-size &optional (image *default-image*))
  "Return the dimensions in pixels of the draw area if we had to
render STRING with FONT and POINT-SIZE."
  (bbox->dimensions
   (draw-freetype-string 0 0 string
			 :image image
			 :do-not-draw t :font-name font
			 :point-size point-size
			 ;; just use the first colour
			 ;; among the ones that have been
			 ;; used this far; it's not
			 ;; displayed anyway
			 :color 0)))

(defun max-text-dimensions (strings font point-size &key (image *default-image*))
  "Return the maximum dimensions (width and height) of the
graphical output if we had to draw LINES of text (a list) with
FONT and POINT-SIZE."
  (loop
     for str in strings
     for (w h) = (string-graphical-dimensions str font point-size image)
     maximizing w into max-width
     maximizing h into max-height
     finally (return (values max-width max-height))))

(defun columns-to-fit-aspect-ratio (cell-width cell-height n-cells aspect-ratio)
  "Return how many columns of N-CELLS rectangular cells of size
CELL-WIDTHxCELL-HEIGHT we need to respect ASPECT-RATIO."
  (be* area (* cell-width cell-height n-cells)
       ideal-area-width (sqrt (* aspect-ratio area))
    (max 1 (round ideal-area-width cell-width))))

(defun split-list (list chunks-size)
  "Split LIST into chunks of length CHUNKS-SIZE."
  (loop
     for chunk = (loop
		    for i from 1 to chunks-size
		    while list
		    collect (pop list))
     while chunk
     collect chunk))

(defun make-caption (labels colours &key (font *axis-font*) (point-size 10d0) (aspect 2) (image *default-image*))
  (multiple-value-bind (w h) (max-text-dimensions labels font point-size
						  :image image)
    (be* n (length labels)
	 interspace .3
	 n-columns (columns-to-fit-aspect-ratio w h n aspect)
	 groups-size (ceiling (/ n n-columns))
	 columns (split-list (mapcar #'cons labels colours) groups-size)
	 max-width (min (/ (image-width image) 2)
			(* w (length columns)))
	 max-height (min (/ (image-height image) 4)
			 (* h (1+ interspace) (length (car columns))))
	 tot-h (* h (1+ interspace) (length (car columns)))
	 ps (min (* point-size (/ max-width w))
		 (* point-size (/ max-height tot-h)))
	 left-margin ps
	 right-margin ps
	 colour-square-size ps
	 font-height (* h (1+ interspace))
	 top-margin 0
	 bottom-margin (/ font-height 2)
	 box-width (+ left-margin colour-square-size 5 w right-margin)
	 box-height (+ top-margin tot-h bottom-margin)
      (loop
	 for column in columns
	 for colnum from 0
	 do (with-default-font (font)
	      (draw-rectangle* (* colnum box-width) 0 (* (1+ colnum) box-width) box-height
			       :color (colour #(200 255 255) :alpha .5 :image image)
			       :filled t
			       :image image)
	      (with-default-colour ("black" :image image)
		(loop
		   for (l . c) in column
		   for i from 0
		   do (let ((x (+ left-margin (* colnum box-width)))
			    (y (+ bottom-margin (* i font-height))))
			(draw-freetype-string (+ x (+ colour-square-size 5)) y l
					      :point-size ps :image image)
			(draw-rectangle* x y (+ x colour-square-size) (+ colour-square-size y)
					 :color c :filled t :image image)
			(draw-rectangle* x y (+ x colour-square-size) (+ colour-square-size y)
					 :image image))))))
      (values box-width box-height))))

(defun plot-line-chart (data &key x-axis-labels fill bullets hgrid vgrid line-width (image *default-image*))
  "Plot a line chart on *DEFAULT-IMAGE* of DATA.  If FILL is true
the space below the lines is filled with the same colour as the
line, therefore it's important in which order the data appears in
DATA as to avoid hiding parts of the chart.  If BULLETS is true,
draw small bullet point in the line segment edges.  DATA is
expected to be an alist where each sublist is:

	(name colour value ...)

Name is the name of the series of values, colour is the colour to
be used to render the line of that series.  The rest is a list of
values of the series."
  (multiple-value-bind (series-labels colours data number-of-series min max x-length)
      (chart-metadata data)
    (declare (ignore number-of-series))
    (assert (or (not x-axis-labels)
		(= x-length (length x-axis-labels))))
    ;; Give a 5% extra vertical room to avoid that the
    ;; highest/lowest values touch the frame of the chart and make
    ;; sure the zero is on the chart
    (setf max (if (< max 0)
		  0
		  (* max 1.05))
	  min (if (> min 0)
		  0
		  (* min 1.05)))
    ;; if the chart is flat and boring, don't bother to plot anything
    (unless (= max min 0)
      (unless x-axis-labels
	(setf x-axis-labels (loop for i from 1 to x-length
			       collect (format nil "~A" i))))
      (with-cartesian-coordinates (:image image)
	(multiple-value-bind (caption-width caption-height)
	    (make-caption series-labels colours :image image)
	  (declare (ignore caption-width))
	  (with-subimage (60 (+ caption-height 30)
			     (- (image-width image) 5) (- (image-height image) 5)
			     :image image)
	    (draw-chart-frame x-axis-labels min max
			      :color "black"
			      :hgrid hgrid :vgrid vgrid
			      :chart-type :line
			      :image image)
	    (with-scaling ((/ (image-width image) (1- x-length))
			   (/ (image-height image) (- max min))
			   :image image)
	      ;; adjust to position of the 0 value
	      (with-translation (0 (if (< min 0)
				       (- min)
				       0)
				   :image image)
		(loop
		   for label in series-labels
		   for colour in colours
		   for values in data
		   do (with-default-colour (colour :image image)
			(draw-line-on-chart values
					    :fill fill
					    :bullets bullets
					    :thickness line-width
					    :image image)))))
	    ;; often charts overlap the frame border: redraw it
	    (draw-rectangle* 0 0 (image-width image) (image-height image)
			     :color "black" :image image)))))))

(defun plot-bar-chart (data &key x-axis-labels (bar-width 1) vgrid hgrid (image *default-image*))
  "Plot a histogram chart on *DEFAULT-IMAGE* of DATA.  DATA is
expected to be an alist where each sublist is:

	(name colour value ...)

Name is the name of the series of values, colour is the colour to
be used to render the bar of that series.  The rest is a list of
values of the series.  BAR-WIDTH is the horizontal magnification
factor of the bar size, where the normal value of 1 means they
are drawn side by side next to each other.  Values x > 1 makes
the bars overlap, and values x < 1 draws them scattered."
  (multiple-value-bind (series-labels colours data number-of-series min max x-length)
      (chart-metadata data)
    (assert (or (not x-axis-labels)
		(= x-length (length x-axis-labels))))
    ;; if the chart is flat and boring, don't bother to plot anything
    (unless (= max min 0)
      ;; Give a 5% extra vertical room to avoid that the
      ;; highest/lowest values touch the frame of the chart and make
      ;; sure the zero is on the chart
      (setf max (if (< max 0)
		    0
		    (* max 1.05))
	    min (if (> min 0)
		    0
		    (* min 1.05)))
      (unless x-axis-labels
	(setf x-axis-labels (loop for i from 1 to x-length
			       collect (format nil "~A" i))))
      (with-cartesian-coordinates (:image image)
	(multiple-value-bind (caption-width caption-height)
	    (make-caption series-labels colours :image image)
	  (declare (ignore caption-width))
	  (with-subimage (60 (+ caption-height 30)
			     (- (image-width image) 5) (- (image-height image) 5)
			     :image image)
	    (draw-chart-frame x-axis-labels min max
			      :color "black"
			      :hgrid hgrid :vgrid vgrid
			      :chart-type :histogram
			      :image image)
	    (with-scaling ((/ (image-width image) x-length)
			   (/ (image-height image) (- max min))
			   :image image)
	      ;; adjust to position of the 0 value
	      (with-translation (0 (if (< min 0)
				       (- min)
				       0)
				   :image image)
		(loop
		   with w = (/ bar-width number-of-series)
		   for i from 0
		   for label in series-labels
		   for colour in colours
		   for values in data
		   do (with-default-colour (colour :image image)
			(draw-bars-on-chart values
					    :width w
					    :offset (if (< bar-width 1)
							(+ (/ i number-of-series)
							   (/ (* (/ number-of-series)
								 (- 1 bar-width)) 2))
							(* i
							   (- 1 w)
							   (if (> number-of-series 1)
							       (/ (1- number-of-series))
							       0)))
					    :border t
					    :image image)))))
	    ;; often charts overlap the frame border: redraw it
	    (draw-rectangle* 0 0 (image-width image) (image-height image)
			     :color "black" :image image)))))))

(defun plot-histogram-chart (data &key x-axis-labels shaded vgrid hgrid (image *default-image*))
  "Plot a histogram chart on *DEFAULT-IMAGE* of DATA.  DATA is
expected to be an alist where each sublist is:

	(name colour value ...)

Name is the name of the series of values, colour is the colour to
be used to render the bar of that series.  The rest is a list of
values of the series."
  (multiple-value-bind (series-labels colours data number-of-series min max x-length)
      (chart-metadata data)
    (declare (ignore number-of-series))
    (assert (or (not x-axis-labels)
		(= x-length (length x-axis-labels))))
    ;; if the chart is flat and boring, don't bother to plot anything
    (unless (= max min 0)
      ;; Give a 5% extra vertical room to avoid that the
      ;; highest/lowest values touch the frame of the chart and make
      ;; sure the 0 is visible
      (setf max (if (< max 0)
		    0
		    (* max 1.05))
	    min (if (> min 0)
		    0
		    (* min 1.05)))
      (unless x-axis-labels
	(setf x-axis-labels (loop for i from 1 to x-length
			       collect (format nil "~A" i))))
      (with-cartesian-coordinates (:image image)
	(multiple-value-bind (caption-width caption-height)
	    (make-caption series-labels colours :image image)
	  (declare (ignore caption-width))
	  (with-subimage (60 (+ caption-height 30)
			     (- (image-width image) 5) (- (image-height image) 5)
			     :image image)
	    (draw-chart-frame x-axis-labels min max
			      :color "black"
			      :hgrid hgrid :vgrid vgrid
			      :chart-type :histogram
			      :image image)
	    (with-scaling ((/ (image-width image) x-length)
			   (/ (image-height image) (- max min))
			   :image image)
	      ;; adjust to position of the 0 value
	      (with-translation (0 (if (< min 0)
				       (- min)
				       0)
				   :image image)
		(loop
		   for i from 0
		   for label in series-labels
		   for colour in colours
		   for values in data
		   do (with-default-colour (colour :image image)
			(draw-histogram-on-chart values :fill shaded :image image)))))
	    ;; often charts overlap the frame border: redraw it
	    (draw-rectangle* 0 0 (image-width image) (image-height image)
			     :color "black" :image image)))))))

(defun polar->cartesian (angle radius)
  (values (* radius (cos angle))
	  (* radius (sin angle))))

(defun simplify-pie-chart-data (data others-label)
  (flet ((totalise (data)
	   (reduce #'+ (mapcar #'caddr data))))
    (be* total (totalise data)
	 substantial-values (remove-if #'(lambda (entry)
					   (< (* 100 (/ (caddr entry) total)) 2))
				       data)
      (if (or (not substantial-values)
	      (<= (- (length data) (length substantial-values)) 1))
	  data
	  (be left-out (set-difference data substantial-values :key #'car)
	    (append substantial-values
		    (list (list others-label
				;; just pick the colour of the first
				;; of those that didn't make it to the
				;; chart
				(cadr (car left-out))
				(totalise left-out)))))))))

(defun plot-pie-chart (data &key shaded simplified (others-label "others") (image *default-image*))
  "Plot a pie chart of DATA on IMAGE.  DATA is expected to be an alist
where each sublist is:

	(name colour value)

Name is a label for that value, colour is the colour to be used to
render the slice of that series.  If SHADED is true the pie will be
draw atop of its own shadow giving a cheap 3D effect.  The colour of
the shadow can be chosen passing a colour as SHADE.  If SIMPLIFIED is
true, don't show slices that would be too small to be seen on the
chart and clump them together under the name OTHERS-LABEL."
  (when simplified
    (setf data (simplify-pie-chart-data data others-label)))
  ;; put the big scorers on the top of the caption box
  (setf data (sort data #'< :key #'caddr))
  (multiple-value-bind (value-labels colours values number-of-slices min max total)
      (pie-metadata data)
    (declare (ignore number-of-slices min max))
    ;; if data was all zero don't draw anything
    (unless (zerop total)
      (with-cartesian-coordinates (:image image)
	(multiple-value-bind (caption-width caption-height)
	    (make-caption (loop
			     for l in value-labels
			     for v in values
			     collect (format nil "~A (~A%)" l (round (* 100 (/ v total)))))
			  colours
			  :image image)
	  (declare (ignore caption-width))
	  (be shadow-y-offset (if shaded
				  (* .04 (image-height image))
				  ;; give at least 3 pixels distance
				  ;; between the pie and the caption
				  3)
	    (with-subimage ((* (image-width image) .1) (+ caption-height shadow-y-offset)
			    ;; leave a margin of few pixels
			    (- (image-width image) 10) (- (image-height image) 5)
			    :image image)
	      (be two-pi #.(* 2 pi)
		(with-translation ((/ (image-width image) 2) (/ (image-height image) 2)
				   :image image)
		  (when shaded
		    (when (eq shaded t)
		      (setf shaded "gray"))
		    (draw-arc 10 (- shadow-y-offset)
			      (image-width image) (image-height image)
			      0 two-pi
			      :center-connect t :filled t
			      :color shaded
			      :image image))
		  (with-default-font (*axis-font*)
		    (loop
		       for subtotal = 0 then (+ subtotal value)
		       for value in values
		       for colour in colours
		       for start = (* (/ subtotal total) two-pi)
		       for end = (* (/ (+ subtotal value) total) two-pi)
		       for percent = (round (* (/ value total) 100))
		       do
			 (draw-arc 0 0
				   (image-width image) (image-height image)
				   start
				   end
				   :center-connect t :filled t
				   :color colour
				   :image image)
			 (cl-gd:with-thickness (2 :image image)
			   (draw-arc 0 0
				     (image-width image) (image-height image)
				     start
				     end
				     :center-connect t :filled nil
				     :color (colour "white")
				     :image image))))
		  (draw-arc 0 0
			    (image-width image) (image-height image)
			    0 two-pi
			    :center-connect nil :filled nil
			    :color "black"
			    :image image))))))))))

(defun fit-text-in-box (width height text-list &key (font *axis-font*) (max-point-size 12d0) (image *default-image*))
  (setf max-point-size (/ max-point-size
			  (clot-image-x-scaling image)))
  (multiple-value-bind (req-width req-height)
      (max-text-dimensions text-list font
			   max-point-size
			   :image image)
    (min max-point-size
	 (* max-point-size (/ width req-width))
	 (* max-point-size (/ height req-height)))))

(defun plot-gantt-chart (data &key (period #.(* 24 60 60)) span-border (bar-width .8)
			 hgrid vgrid time-formatter vrulers (image *default-image*) do-not-draw)
  "Plot a Gantt chart on *DEFAULT-IMAGE* of DATA.  DATA is
expected to be an alist where each sublist is:

	(name (start colour duration [label]) ...)

Name is the name a resource, start the beginning of the
allocation of resource to a job, colour is the colour of the job,
duration is the length of the job allocated on that resource.  If
DO-NOT-DRAW is true don't draw but just do the calculations of
the bounding boxes of the spans."
  (multiple-value-bind (number-of-resources resource-names start end shortest longest)
      (gantt-metadata data)
    (declare (ignore shortest longest))
    (setf start (floor-to start period)
	  end (ceiling-to end period))
    (with-cartesian-coordinates (:image image)
      (with-subimage (60 30 (- (image-width image) 5) (- (image-height image) 5)
			 :image image)
	(prog1				; return the bounding boxes of all the spans
	    (unless (= start end)
	      (with-scaling ((/ (image-width image) (- end start))
			     (/ (image-height image) number-of-resources)
			     :image image)
		(flet ((draw-span (y start end colour label)
			 (be* border (/ (- 1 bar-width) 2)
			      y-bottom (+ y border)
			      y-top (+ y  1 (- border))
			   (draw-rectangle* start y-bottom
					    end y-top
					    :color colour :filled t
					    :image image)
			   (when span-border
			     (be border (max 1 (round (/ (image-height image) number-of-resources) 25))
			       (draw-rectangle* start y-bottom
						end y-top
						:color (if (eq span-border t)
							   (change-colour-brightness colour -0.3 :image image)
							   span-border)
						:thickness border
						:filled nil
						:image image)))
			   (when label
			     (be font-size (fit-text-in-box (- end start)
							    (- y-top y-bottom)
							    (list label)
							    :max-point-size 10d0
							    :image image)
			       ;; skip unreadable labels (this should
			       ;; be calculated -wcp5/3/07)
			       (when (> font-size 1000)
				 (draw-freetype-string (/ (+ start end) 2)
						       (/ (+ y-bottom y-top) 2)
						       label :justification :center
						       :font-name *axis-font*
						       :point-size font-size
						       :image image)))))))
		  (loop
		     for (name . spans) in data
		     for i from 0
		     collect (loop
				for (x colour duration . rest) in spans
				for end = (+ x duration (- start))
				for label = (car rest)
				unless do-not-draw
				do (draw-span i (- x start) end colour label)
				collect (multiple-value-bind (x1 y1) (transform-image-coordinates (- x start) i image)
					  (multiple-value-bind (x2 y2) (transform-image-coordinates end (1+ i) image)
					    ;; convert the cartesian coordinates back to "normal"
					    (list x1 (- (image-height image) y1) x2 (- (image-height image) y2)))))))))
	  (unless do-not-draw
	    (draw-xy-frame (loop
			      for i from start below end by period
			      collect (if time-formatter
					  (funcall time-formatter i)
					  (format nil "~A" (sclf:time-string i))))
			   resource-names
			   :color "black"
			   :hgrid hgrid :vgrid vgrid
			   :x-label-position :between-notches
			   :y-label-position :between-notches
			   :image image))
	  (when (and vrulers (not do-not-draw))
	    (with-scaling ((/ (image-width image) (- end start)) (image-height image)
			   :image image)
	      (loop
		 for i from 0 to (/ (- end start) period)
		 do (loop
		       for offset in vrulers
		       for x = (+ offset (* i period))
		       do (draw-line x  0 x (image-height image) :color "light gray"
				     :image image))))))))))

(defun view-image (&optional (image *default-image*))
  (with-temp-file (stream)
    (write-image-to-stream stream :png :image image)
    (close stream)
    (run-program "xv" (list (pathname stream)))))

(defun view-chart (type width height data &rest args)
  (with-image* (width height)
    (fill-image 0 0 :color *default-background-colour*)
    (apply (ecase type
	     (:line #'plot-line-chart)
	     (:histogram #'plot-histogram-chart)
	     (:bar #'plot-bar-chart)
	     (:pie #'plot-pie-chart)
	     (:gantt #'plot-gantt-chart))
	   data args)
    (view-image)))
