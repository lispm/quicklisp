;;; -*- mode: Lisp; Syntax: Common-Lisp; -*-
;;;
;;; Copyright (c) 2009 by the authors.
;;;
;;; See LICENCE for details.

(in-package :hu.dwim.presentation)

;;;;;;
;;; place/editor

(def (component e) place/editor (t/editor place/presentation)
  ()
  (:documentation "An PLACE/EDITOR edits existing values of a TYPE at a PLACE."))
