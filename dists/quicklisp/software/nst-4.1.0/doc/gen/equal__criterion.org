# DO NOT EDIT --- generated from source code.
*** Criterion =equal=
The =:equal= criterion checks a form using =eql=.  The criterion argument and the form under test are both evaluated at testing time.

