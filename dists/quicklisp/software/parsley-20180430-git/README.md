# parsley

A toolset for parsing binary data formats.

## Overview

Currently, this just defines some common readers for primitive binary data types. This project is of
alpha status, and its API or scope may change drastically at any time.

## Install

```lisp
(ql:quickload :parsley)
```

## License

Copyright © 2017-2018 [Michael Fiano](mailto:mail@michaelfiano.com).

Licensed under the MIT License.
