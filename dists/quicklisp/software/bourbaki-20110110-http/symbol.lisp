;;; Copyright (c) Juha Arpiainen 2005-06

;;; This file is part of Bourbaki.

;;; Bourbaki is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.

;;; Bourbaki is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Bourbaki; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; -------

;;; *****
;;; Symbols, symbol kinds
;;; *****

(in-package :bourbaki)

;;; test if an expression of type1 can be substituted for a type2 variable
(defun subkindp (type1 type2)
  (cond ((eq type1 type2) t)
        ((null type1) nil)
        (t (subkindp (gethash :super  (context-meta type1)) type2))))

(defvar *mkvar-counter* 0)

(defun mkvar (type class &key (name (format nil "V~A" (incf *mkvar-counter*))) (bound nil))
  "Create a symbol of specified type"
  (let ((var (make-context :type type :class class :vars nil :name name)))
    (setf (gethash :full-name (context-meta var)) name)
    (if bound (setf (gethash :bound (context-meta var)) t))
    var))

(defun theorem-sym-p (sym)
  (and (typep sym 'context)
       (member (context-class sym) '(:theorem :axiom :definition))))
