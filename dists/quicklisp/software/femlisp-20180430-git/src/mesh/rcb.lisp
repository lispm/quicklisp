(in-package :fl.mesh)

(file-documentation
 "Recursive coordinate bisection")

(defun calculate-bounding-box (points)
  "Calculate a bounding box for the given point.  The result is a pair of
  vectors where the first contains the minimal values and the second the
  maximal values."
  (when points
    (let* ((point1 (first points))
           (minima (copy-seq point1))
           (maxima (copy-seq point1)))
      (loop for point in (rest points) do
        (loop for x across point
              and i from 0
              do (_f min (aref minima i) x)
                 (_f max (aref maxima i) x)))
      (cons minima maxima))))

;;; (calculate-bounding-box (list #d(1.0 1.0) #d(0.0 0.0) #d(0.5 0.5)))

(defun bisection-parameters (box)
  "From a given bounding box calculate a reasonable separating dimension
and a separating coordinate."
  (destructuring-bind (min . max) box
    (let* ((diff (m- max min))
           (maxdiff (reduce #'max diff))
           (maxdim (position maxdiff diff :test #'=)))
      (values maxdim
              (average (aref min maxdim)
                       (aref max maxdim))))))
#+(or)
(bisection-parameters
 (calculate-bounding-box (list #d(1.0 1.0) #d(0.0 0.0) #d(0.5 0.5))))

(defun bisect (cells axis bcoord)
  "Bisect a list of cells along the axis given by @arg{axis} and at the
coordinate @arg{coord}."
  (flet ((test< (cell)
           (< (aref (midpoint cell) axis) bcoord)))
    (values (filter-if #'test< cells)
            (remove-if #'test< cells))))

(defun recursive-bisection (cells)
  "Cells should be the 'substance' cells, i.e. the highest dimensional
cells of a mesh."
  (if (single? cells)
      (first cells)
      (let ((box (calculate-bounding-box
                  (mapcar #'midpoint cells))))
        (multiple-value-bind (bdim bcoord)
            (bisection-parameters box)
          (multiple-value-bind (cells1 cells2)
              (bisect cells bdim bcoord)
            (cond
              ((and cells1 cells2)
               (cons (recursive-bisection cells1)
                     (recursive-bisection cells2)))
              (t
                ;; this may happen, because cells on different levels may have the same midpoint
               (cons (car cells)
                       (recursive-bisection (rest cells))))))))))

(defun interface-table (cell-tree)
  "The result is a table which maps each cell to a pair which consists of
the minimum and the maximum index of an n-dimensional father."
  (lret ((table (make-hash-table)))
    (labels ((descend (tree id)
               (cond
                 ((atom tree)
                  (loop+ ((cell (subcells tree)))
                    do
                    (let ((minmax (or (gethash cell table)
                                      (cons id id))))
                      (setf (gethash cell table)
                            (cons (min id (car minmax))
                                  (max id (cdr minmax)))))))
                 (t (descend (car tree) (* 2 id))
                    (descend (cdr tree) (1+ (* 2 id)))))))
      (descend cell-tree 0))))

#+(or)
(defun postprocess-table (table)
  "Ensure that cell boundaries are later than the cells.  This ensures that
direct solving works for Crouzeix-Raviart discretizations.  However, for
our matrix structures this is bad from a performance point of view, because
more work is spent on small matrix blocks.  An important improvement would
be to switch to a direct sparse solver library at some later stage."
  (labels ((flag-cell (cell parent-index parent-depth)
             (let ((entry (gethash cell table))
                   (recurse-p t))
               (unless (consp entry)
                 (setf (gethash cell table) (cons entry 0)
                       recurse-p t))
               (destructuring-bind (my-index . my-depth)
                   (gethash cell table)
                 (when (and parent-index
                            (>= parent-index my-index))
                   (setf my-index parent-index
                         my-depth (1+ parent-depth)
                         (gethash cell table) (cons my-index my-depth)
                         recurse-p t))
                 (when recurse-p
                   (loop for side across (boundary cell) do
                     (flag-cell side my-index my-depth)))))))
    (loop for cell being each hash-key of table do
      (flag-cell cell nil nil))))

(defun rcb-sort-items (items &key (item->cell #'identity) root-cells)
  (let* ((tree (recursive-bisection
                (or root-cells
                    (mapcar item->cell items))))
         (table (interface-table tree)))
    (labels ((get-ids (cell)
               (gethash (funcall item->cell cell) table))
             (compare (cell1 cell2)
               (let ((ids1 (get-ids cell1))
                     (ids2 (get-ids cell2)))
                 (or (< (cdr ids1) (cdr ids2))
                     (and (= (cdr ids1) (cdr ids2))
                          (or (> (car ids1) (car ids2))
                              (and (= (car ids1) (car ids2))
                                   (> (dimension cell1) (dimension cell2)))))))))
      (safe-sort items #'compare))))

(defun rcb-ordered-cells (skel)
  (let ((dim (dimension skel)))
    (rcb-sort-items
     (find-cells (constantly t) skel)
     :root-cells (find-cells (_ (= dim (dimension _))) skel))))

#+(or)
(rcb-ordered-cells (refine (skeleton (n-cube 2))))
