(in-package :box.dgen)

(defun staircase-suitable-p (stage nh)
  (let ((cell (origin nh)))
    (and (featuresp cell :room)
         (not (adjacent-junction-p stage cell)))))

(defun choose-upstairs (stage)
  (with-slots (%x1 %x2 %y1 %y2) (genie:gen :elt (generator stage) :sequence (stage-rooms stage))
    (genie:gen :elt (generator stage) :sequence
         (mapcar
          #'origin
          (collect
           stage
           (layout :rect :maximum 0)
           #'staircase-suitable-p
           :x1 %x1
           :x2 (- %x2 (width stage))
           :y1 %y1
           :y2 (- %y2 (height stage)))))))

(defun make-upstairs (stage)
  (let ((cell (choose-upstairs stage)))
    (setf (distance cell) 0)
    cell))

(defun choose-downstairs (stage region-id)
  (loop :for cell = (genie:gen :elt (generator stage) :sequence (cells (get-region stage region-id)))
        :while (adjacent-junction-p stage cell)
        :finally (return cell)))

(defun make-downstairs (stage source)
  (let ((queue (cl-speedy-queue:make-queue (* (width stage) (height stage))))
        (goal source))
    (cl-speedy-queue:enqueue goal queue)
    (loop :until (cl-speedy-queue:queue-empty-p queue)
          :do (loop :with current = (cl-speedy-queue:dequeue queue)
                    :with current-distance = (1+ (distance current))
                    :with n = (cell-nh stage current (layout :orthogonal))
                    :with carved = (nfilter n #'carved)
                    :for cell :in carved
                    :when (minusp (distance cell))
                      :do (cl-speedy-queue:enqueue cell queue)
                          (setf (distance cell) current-distance)
                          (when (and (> (distance cell) (distance goal))
                                     (staircase-suitable-p stage n))
                            (setf goal cell))))
    (choose-downstairs stage (region-id goal))))

(defun create-stairs (stage)
  (let* ((upstairs (make-upstairs stage))
         (downstairs (make-downstairs stage upstairs)))
    (add-feature upstairs :stairs-up)
    (add-feature downstairs :stairs-down)))
