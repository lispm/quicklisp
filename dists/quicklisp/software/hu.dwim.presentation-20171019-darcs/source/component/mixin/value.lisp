;;; -*- mode: Lisp; Syntax: Common-Lisp; -*-
;;;
;;; Copyright (c) 2009 by the authors.
;;;
;;; See LICENCE for details.

(in-package :hu.dwim.presentation)

;;;;;;
;;; component-value/mixin

(def (component eas) component-value/mixin ()
  ((component-value
    :type t
    :computed-in computed-universe/session
    :documentation "The current COMPONENT-VALUE."))
  (:documentation "A COMPONENT that represents a single COMPONENT-VALUE."))

(def layered-method refresh-component :before ((self component-value/mixin))
  (when (slot-boundp self 'component-value)
    (bind ((value (component-value-of self))
           (class (component-dispatch-class self))
           (prototype (component-dispatch-prototype self))
           (reused-value (reuse-component-value self class prototype value)))
      (unless (eq value reused-value)
        ;; TODO: is this the right way to avoid overwriting computations in component-value?
        (bind ((cs (standard-instance-access self (slot-definition-location (find-slot (class-of self) 'component-value)))))
          (if (hu.dwim.computed-class::computed-state-p cs)
              (setf (hu.dwim.computed-class::cs-value cs) reused-value)
              (setf (component-value-of self) reused-value)))))))

(def method clone-component ((self component-value/mixin))
  (aprog1 (call-next-method)
    ;; TODO: computed state?
    (setf (component-value-of it) (component-value-of self))))

;;;;;;
;;; component-value-type/mixin

(def (component eas) component-value-type/mixin ()
  ((component-value-type
    t
    :type t
    :documentation "The type of possible COMPONENT-VALUEs."))
  (:documentation "A COMPONENT that represents a single COMPONENT-VALUE of COMPONENT-VALUE-TYPE."))

(def method clone-component ((self component-value-type/mixin))
  (aprog1 (call-next-method)
    (setf (component-value-type-of it) (component-value-type-of self))))

;;;;;;
;;; selected-type/mixin

(def (component e) selected-type/mixin ()
  ((selected-type
    t
    :type t
    :documentation "The currently selected type for COMPONENT-VALUEs.")))

;;;;;;
;;; component-result/mixin

;; TODO: rename, move?
(def (component e) component-result/mixin ()
  ((result-limit 1 :type (or null number))
   (result (make-instance 'empty/layout) :type component)))

(def (function e) render-result-for (component)
  (awhen (result-of component)
    (render-component it)))

;; TODO better name! make-result-inspector?
(def (layered-function e) make-result (component class prototype value)
  (:method ((component component-result/mixin) class prototype value)
    (make-inspector value)))
