(in-package :box.dgen)

(defclass stage-room ()
  ((%x1 :reader x1
        :initarg :x1)
   (%x2 :reader x2
        :initarg :x2)
   (%y1 :reader y1
        :initarg :y1)
   (%y2 :reader y2
        :initarg :y2)
   (%width :reader width
           :initarg :w)
   (%height :reader height
            :initarg :h)))

(defmethod initialize-instance :after ((object stage-room) &key)
  (with-slots (%x1 %x2 %y1 %y2 %width %height) object
    (setf %x2 (+ %x1 %width)
          %y2 (+ %y1 %height))))

(defun estimate-rooms (stage)
  (with-slots (%width %height %room-size-min %room-size-max %room-density) stage
    (let* ((min (expt %room-size-min 2))
           (max (expt %room-size-max 2))
           (distance (abs (- max min)))
           (average (if (zerop distance) max (/ distance 2))))
      (floor (* (/ (* %width %height) average) %room-density)))))

(defgeneric intersectsp (source target))

(defmethod intersectsp ((source stage-room) (target stage-room))
  (and (> (x2 source) (x1 target))
       (< (x1 source) (x2 target))
       (> (y2 source) (y1 target))
       (< (y1 source) (y2 target))))

(defmethod intersectsp ((source stage-room) (target stage))
  (dolist (target (stage-rooms target))
    (when (intersectsp source target)
      (return target))))

(defun place-room (stage room)
  (with-slots (%x1 %x2 %y1 %y2) room
    (loop :with region = (make-region stage)
          :for x :from %x1 :below %x2
          :do (loop :for y :from %y1 :below %y2
                    :for cell = (cell stage x y)
                    :do (carve stage cell :region-id region :feature :room))))
  (push room (stage-rooms stage)))

(defun make-room (stage)
  (with-slots (%generator %width %height %room-size-min %room-size-max) stage
    (let* ((w (genie:gen :int %generator :parityp t :min %room-size-min :max %room-size-max))
           (h (genie:gen :int %generator :parityp t :min %room-size-min :max %room-size-max))
           (x (genie:gen :int %generator :parityp t :min 1 :max (- %width w)))
           (y (genie:gen :int %generator :parityp t :min 1 :max (- %height h)))
           (room (make-instance 'stage-room :x1 x :y1 y :w w :h h)))
      (unless (intersectsp room stage)
            (place-room stage room)))))

(defun add-rooms (stage)
  (loop :with max = (estimate-rooms stage)
        :with tries = 0
        :until (or (= (length (stage-rooms stage)) max)
                   (>= tries 1000))
        :do (make-room stage)
            (incf tries)))
