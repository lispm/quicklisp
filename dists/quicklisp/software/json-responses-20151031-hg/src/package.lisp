;;;; package.lisp

(defpackage #:ca.pinecode.json-responses
  (:use #:cl #:hunchentoot #:cl-json)
  (:nicknames #:json-responses)
  (:export #:json-response
           ;; 1xx
           #:continue-response
           #:switching-protocols-response
           ;; 2xx
           #:ok-response
           #:created-response
           #:accepted-response
           #:non-authoritative-information-response
           #:no-content-response
           #:reset-content-response
           #:partial-content-response
           ;; 3xx
           #:multiple-choices-response
           #:moved-permanently-response
           #:moved-temporarily-response
           #:see-other-response
           #:not-modified-response
           #:use-proxy-response
           #:temporary-redirect-response
           ;; 4xx
           #:bad-request-response
           #:authorization-required-response
           #:forbidden-response
           #:not-found-response
           #:method-not-allowed-response
           #:not-acceptable-response
           #:proxy-authentication-required-response
           #:request-timeout-response
           #:conflict-response
           #:gone-response
           #:length-required-response
           #:precondition-failed-response
           #:request-entity-too-large-response
           #:request-uri-too-long-response
           #:unsupported-media-type-response
           #:requested-range-not-satisfiable-response
           #:expectation-failed-response
           ;; 5xx
           #:internal-server-error-response
           #:not-implemented-response
           #:bad-gateway-response
           #:service-unavailable-response
           #:gateway-timeout-response
           #:http-version-not-supported-response))
