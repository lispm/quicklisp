;;;  lexer.lisp --- lexical analiser of cobol sources

;;;  Copyright (C) 2005, 2008 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: cobstor

#+cmu (ext:file-comment "$Module: lexer.lisp $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

;;; Commentary:
;;;
;;; This lexical analyser is able to cope with the vast majority of
;;; Cobol language artifacts, even those that are normally not found
;;; in record descriptions.  The only assumption here is that the
;;; Cobol source conforms to the so-called free form format.  That is,
;;; the traditional first 7 columns of each line of the old Cobol
;;; format are missing.  Comments start with an asterisk as the first
;;; character of the line.

(in-package :cobstor-generator)

(defvar *decimal-point* #\.
  "The character used to represent a decimal point in numbers.
This can be changes any time according to local national
conventions.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass parser-offset (stream-offset)
  ((file :initarg :file
	 :type (or string pathname)
	 :reader off-file)
   (token :initarg :token
	  :type integer
	  :reader off-token))
  (:documentation
   "Type of objects returned by GET-POSITION applied to PARSER-STREAMs."))

(defmethod print-object ((obj parser-offset) stream)
  (cond (*print-pretty*
	 (format stream "~A line ~A column ~A (token ~A)" (namestring (off-file obj))
		 (off-line obj) (off-column obj) (off-token obj)))
	(t
	 (print-unreadable-object (obj stream :type t)
	   (format stream "file=~A line=~A column=~A token=~A"
		   (namestring (off-file obj)) (off-line obj)
		   (off-column obj) (off-token obj))))))

(defmethod later-position ((pos1 parser-offset) (pos2 parser-offset))
  (> (off-token pos1) (off-token pos2)))

;; this is just for fun
(defmethod later-position ((pos1 stream-offset) (pos2 stream-offset))
  (or (> (off-line pos1) (off-line pos2))
      (and (= (off-line pos1) (off-line pos2))
	   (> (off-column pos1) (off-column pos2)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass parser-stream (stateful-stream)
  ((grammar :initarg :grammar
	    :type grammar
	    :reader grammar)
   (token :initform 0
	  :reader token
	  :type integer))
  (:documentation
   "Type of stream used by cobstor parser."))

(defmethod get-position ((stream parser-stream))
  (make-instance 'parser-offset
		 :file (pathname (real-stream stream))
		 :line (line stream)
		 :column (column stream)
		 :token (token stream)))

(defun new-token-position (stream)
  (declare (type parser-stream stream))
  (prog1 (get-position stream)
    (incf (slot-value stream 'token))))

(defun make-parser-stream (pathname-or-stream grammar)
  (make-instance 'parser-stream
		 :grammar grammar
		 :stream (if (or (pathnamep pathname-or-stream)
				 (stringp pathname-or-stream))
			     (open pathname-or-stream)
			     pathname-or-stream)))

(defmacro with-parser-stream ((var pathname-or-stream grammar) &body body)
  `(with-open-stream (,var (make-parser-stream ,pathname-or-stream ,grammar))
     ,@body))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(eval-when (:load-toplevel :compile-toplevel :execute)
  (defun make-presence-table (chars)
    (loop
       for c across chars
       with arr = (make-array char-code-limit :element-type 'bit :initial-element 0)
       do (setf (sbit arr (char-code c)) 1)
       finally (return arr))))

(defconst +word-start-table+
  (make-presence-table "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"))

(defconst +word-component-table+
  (make-presence-table "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_0123456789"))

(defun word-start-p (c)
  (declare (type character c))
  (= 1 (sbit +word-start-table+ (char-code c))))

(defun word-component-p (c)
  (declare (type character c))
  (= 1 (sbit +word-component-table+ (char-code c))))

(defun skip-whitespace (stream)
  (loop
     for c = (peek-char nil stream)
     while (whitespace-p c)
     do (read-char stream)))

(defun skip-to-eol (stream)
  (peek-char #\newline stream nil))

(defun read-string (stream)
  (declare (type parser-stream stream))
  (labels ((collect-string ()
	     (let ((string (loop
			      for c of-type character = (read-char stream)
			      when (char= #\" c)
			      return (concatenate 'string string)
			      collect c into string)))
	       (if (eq #\" (peek-char nil stream nil))
		   (concatenate 'string string (string (read-char stream)) (collect-string))
		   string))))
    (be position (new-token-position stream)
      (make-token :type 'string
		  :value (collect-string)
		  :position position))))

(defun read-word (stream first-chars)
  (declare (type parser-stream stream)
	   (type simple-string first-chars))
  (let* ((position (new-token-position stream))
	 (word (loop
		  for c = (peek-char nil stream)
		  while (word-component-p c)
		  collect (read-char stream) into word
		  finally (return (string-upcase (concatenate 'string first-chars word))))))
    (make-token :type (if (grammar-keyword-p word (grammar stream))
			  'keyword
			  'identifier)
		:value word
		:position position)))

(defun read-integer (stream first-digit)
  (loop
     for c = (peek-char nil stream)
     while (digit-char-p c)
     collect (read-char stream) into word
     finally (return (concatenate 'string (string first-digit) word))))

(defun read-number (stream first-digit)
  (declare (type parser-stream stream)
	   (type character first-digit))
  (let* ((position (new-token-position stream))
	 (number (read-integer stream first-digit))
	 (maybe-point (peek-char nil stream)))
    (cond ((char= *decimal-point* maybe-point)
	   (let* ((point (read-char stream))
		  (first-decimal (peek-char nil stream)))
	     (if (digit-char-p first-decimal)
		 ;; the decimal point is followed by a digit so we
		 ;; compose a floating point number
		 (list
		  (make-token :type 'float
			      :value (read-from-string
				      (format nil "~A.~A" number
					      (read-integer stream (read-char stream))))
			      :position position))
		 ;; the decimal point is not followed by a digit so we
		 ;; treat them number as an integer and the point as a
		 ;; separate token (a keyword)
		 (cons
		  (make-token :type 'integer
			      :value (read-from-string number)
			      :position position)
		  (unless (char= point #\,)
		    ;; stray commas are whitespace in Cobol
		    (list
		     (make-token :type 'keyword
				 :value (string point)
				 :position (new-token-position stream))))))))
	  ((word-component-p maybe-point)
	   ;; It's not a number but just a bloody identifier
	   ;; starting with digits.  Mad, innit?
	   (list (read-word stream number)))
	  (t ;; it's just an integer
	   (list
	    (make-token :type 'integer
			:value (read-from-string number)
			:position position))))))

(defun read-picture (stream)
  (declare (type parser-stream stream))
  (let* ((position (new-token-position stream))
	 (picture (loop
		     for c = (peek-char nil stream)
		     until (whitespace-p c)
		     collect (read-char stream) into picture
		     finally (return (concatenate 'string picture))))
	 (last (1- (length picture)))
	 (ending-dot (char= #\. (char picture last)))
	 (token (make-token :type 'picture-string
			    :value (if ending-dot
				       (subseq picture 0 last)
				       picture)
			    :position position)))
    (if ending-dot
	(list token
	      (make-token :type 'keyword
			  :value "."
			  :position (new-token-position stream)))
	(list token))))

(defmethod read-next-tokens ((stream parser-stream))
  "Return a list of the next few tokens.  Practically most of the
times this should be a list of one element, but there are cases
in which the lexer has to scan ahead to figure out the end of the
first token; by then it has already read another one.  Since
pushing back the extra token in the input stream is not feasible,
we allow this function to return a list of tokens."
  (loop
     for bol of-type boolean = (beginning-of-line-p stream)
     for c of-type (or null character) = (read-char stream nil)
     do (cond ((not c)
	       (return nil))
	      ((and (char= c #\*) bol)
	       ;; skip comments
	       (skip-to-eol stream))
	      (t (cond ((char= c #\")
			(return (list (read-string stream))))
		       ((digit-char-p c)
			(return (read-number stream c)))
		       ((word-start-p c)
			(let ((word (read-word stream (string c))))
			  (cond ((or (string-equal "pic" (token-value word))
				     (string-equal "picture" (token-value word)))
				 (skip-whitespace stream)
				 ;; read-picture may return two tokens if the picture is
				 ;; followed by a dot (not a decimal point)
				 (return (cons word (read-picture stream))))
				(t (return (list word))))))
		       ((and (not (whitespace-p c))
			     ;; Cobol considers this punctuation the
			     ;; same as whitespace
			     (not (char= c #\,))
			     (not (char= c #\;)))
			;; anything else is considered a keyword even though it's
			;; just a single character
			(return
			  (list
			   (make-token :type 'keyword
				       :value (string c)
				       :position (new-token-position stream))))))))))


;; for debugging purposes -wcp25/7/03.
#+(OR)
(defun scan-file (pathname grammar)
  (with-parser-stream (stream pathname grammar)
    (loop
       for toks = (read-next-tokens stream)
       until (null toks)
       append toks)))
