DEFENUM
=======

Copyright (c) 2014-2018 Marco Antoniotti, all rights reserved.

The DEFENUM facility provides C++ and Java styled 'enum' in Common
Lisp.  Please refer to the HTML documentation for more information.


A NOTE ON FORKING
-----------------

Of course you are free to fork the project subject to the current
licensing scheme.  However, before you do so, I ask you to consider
plain old "cooperation" by asking me to become a developer.
It helps keeping the entropy level at an acceptable level.


Enjoy
