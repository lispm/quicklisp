(in-package :agnostic-lizard)

(defmacro eval-with-current-environment ((var) &body code &environment env)
  "Evaluate code in a lexical environment where var is bound to the lexical
environment object corresponding to the macro call position"
  `',(funcall (eval `(lambda (,var) ,@code)) env))

(defun separate-declarations (entries &key (enable-docstring t))
  "Separate a list of entries into declarations and forms, with a possible docstring, if enabled"
  (loop
    with docstring := (not enable-docstring)
    with declarations := nil
    with forms := nil
    for header := t then header-continued
    for entry in entries
    for operator := (and (consp entry) (car entry))
    for header-continued :=
    (and
      header
      (or
        (and (stringp entry) (not docstring) (setf docstring entry) t)
        (eq operator 'declare)))
    do (if header-continued (push entry declarations) (push entry forms))
    finally
    (return
      ; (lambda () "asd") is the same as (constantly "asd")
      (if (and (null forms) (stringp docstring)
               (eq docstring (car declarations)))
        (list (reverse (cdr declarations)) (list docstring))
        (list (reverse declarations) (reverse forms))))))

(defun go-tag-p (x) (or (integerp x) (symbolp x)))

(defun lambda-list-variable-names (l)
  (loop
    for name in l
    unless (find name lambda-list-keywords)
    collect 
    (cond
      ((symbolp name) name)
      ((symbolp (first name)) (first name))
      (t (second (first name))))
    when (and (listp name) (third name)) collect (third name)))

