;;;  sbcl.lisp --- output module for SBCL FFI

;;;  Copyright (C) 2004 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-094E68BA066A6EEA274B9E976384BB22.lisp,v 1.23 2004/05/04 17:54:10 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-094E68BA066A6EEA274B9E976384BB22.lisp,v 1.23 2004/05/04 17:54:10 wcp Exp $")

(in-package :binge-backend)

(defclass sbcl-backend (backend) ())

(defmethod use-packages ((backend sbcl-backend))
  (declare (ignore backend))
  '("COMMON-LISP" "SB-ALIEN"))

(defmethod ffi-sizeof (thing (backend sbcl-backend))
  (format nil "(alien-size ~A :bytes)" thing))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod c-object->ffi-notation ((object c-argument) (backend sbcl-backend))
  (c-object->ffi-notation (c-type object) backend))

(defmethod c-object->ffi-notation ((object c-slot) (backend sbcl-backend))
  (format nil "(~A ~A)"
	  (c-name object)
	  (c-object->ffi-notation (c-type object) backend)))

(defmethod c-object->ffi-notation ((object c-pointer) (backend sbcl-backend))
  (case (c-type object)
    (:void
     ;; pointer to anything
     "(* T)")
    ((:char :unsigned-char)
     ;; this is not always true but it's a safe bet
     "c-string")
    (t
     (format nil "(* ~A)" (c-object->ffi-notation (c-type object) backend)))))

(defmethod c-object->ffi-notation ((object c-user-type-ref) (backend sbcl-backend))
  (c-object-lisp-name object backend))

(defmethod c-object->ffi-notation ((object c-struct-ref) (backend sbcl-backend))
  (format nil "(struct ~A)" (c-object-lisp-name object backend)))

(defmethod c-object->ffi-notation ((object c-union-ref) (backend sbcl-backend))
  (format nil "(union ~A)" (c-object-lisp-name object backend)))

(defmethod c-object->ffi-notation ((object c-enum-ref) (backend sbcl-backend))
  (format nil "(enum ~A)" (c-object-lisp-name object backend)))

(defmethod c-object->ffi-notation ((object c-function) (backend sbcl-backend))
  (format nil "(function ~A~{ ~A~})"
	  (c-object->ffi-notation (c-type object) backend)
	  (mapcar #'(lambda (arg)
		      (c-object->ffi-notation arg backend))
		  (c-arguments object))))

(defmethod c-object->ffi-notation ((object c-array) (backend sbcl-backend))
  (format nil "(array ~A~{ ~A~})"
	  (c-object->ffi-notation (c-type object) backend)
	  (convert-array-dimensions (c-dimensions object) backend)))

(defmethod c-object->ffi-notation ((object symbol) (backend sbcl-backend))
  (declare (ignore backend))
  (case object
    (:void "void")
    (:char "char")
    (:short "short")
    (:int "int")
    (:long "long")
    (:unsigned-char "unsigned-char")
    (:unsigned-short "unsigned-short")
    (:unsigned-int "unsigned-int")
    (:unsigned-long "unsigned-long")
    (:float "float")
    (:double "double")
    (t
     (error 'binge-compiler:compilation-unsupported-error
	    :item object))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun sbcl-format-compound (type declaration backend)
  (format nil "(define-alien-type nil~%  (~A ~I~A~@[ ~<~@{~A~^ ~_~}~:>~]))"
	  type
	  (c-object-lisp-name declaration backend)
	  (mapcar #'(lambda (slot)
		      (c-object->ffi-notation slot backend))
		  (c-slots declaration))))

(defmethod c-object->ffi-notation ((decl c-typedef-declaration) (backend sbcl-backend))
  (format nil "(define-alien-type ~A ~A)"
	  (c-object-lisp-name decl backend)
	  (c-object->ffi-notation (c-type decl) backend)))

(defmethod c-object->ffi-notation ((decl c-struct-declaration) (backend sbcl-backend))
  (sbcl-format-compound "struct" decl backend))

(defmethod c-object->ffi-notation ((decl c-union-declaration) (backend sbcl-backend))
  (sbcl-format-compound "union" decl backend))

(defmethod c-object->ffi-notation ((decl c-enum-declaration) (backend sbcl-backend))
  (format nil "(define-alien-type nil~%  (enum ~A ~<~@{~A~^ ~:_~}~:>))"
	  (c-object-lisp-name decl backend)
	  (mapcar #'(lambda (c)
		      (if (consp c)
			  (format nil "(:~@[~A~]~A ~S)" (backend-prefix backend) (car c) (cadr c))
			  (format nil ":~@[~A~]~A" (backend-prefix backend) c)))
		  (c-constants decl))))

(defmethod c-object->ffi-notation ((decl c-function-declaration) (backend sbcl-backend))
  (format nil "(define-alien-routine (~S ~A) ~A~@[~&  ~<~@{~A~^ ~:_~}~:>~])"
	  (c-name decl)
	  (c-object-lisp-name decl backend)
	  (c-object->ffi-notation (c-type decl) backend)
	  (loop
	     for arg in (c-arguments decl)
	     for i from 1 by 1
	     collect (if (c-name arg)
			 (format nil "(~A ~A)" (c-name arg)
				 (c-object->ffi-notation (c-type arg) backend))
			 ;; the use of a dash in the name of arguments
			 ;; guarantees we won't have any name clash
			 (format nil "(arg-~A ~A)" i
				 (c-object->ffi-notation (c-type arg) backend))))))

(defmethod c-object->ffi-notation ((decl c-variable-declaration) (backend sbcl-backend))
  (format nil "(define-alien-variable (~S ~A) ~A)"
	  (c-name decl)
	  (c-object-lisp-name decl backend)
	  (c-object->ffi-notation (c-type decl) backend)))

(setf binge-compiler:*backend* (make-instance 'sbcl-backend))
(export 'sbcl-backend)		     ; so that we can use this backend
