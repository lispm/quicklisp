;;;  grammar.lisp --- incomplete C grammar

;;;  Copyright (C) 2004 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-95163745BE190F27CB354CE184430E70.lisp,v 1.20 2004/04/15 22:50:01 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-95163745BE190F27CB354CE184430E70.lisp,v 1.20 2004/04/15 22:50:01 wcp Exp $")

(in-package :binge-grammar)

(deflazy define-grammar
  (let ((*package* #.*package*)
	(*compile-print* (and npg::*debug* t)))
    (reset-grammar)
    (format t "~&creating C grammar...~%")
    (populate-grammar)
    (let ((grammar (npg:generate-grammar #'string=)))
      (reset-grammar)
      (npg:print-grammar-figures grammar)
      grammar)))

(defun typify-names (base-type name-completion-functions container-class)
  "Create objects of type CONTAINER-CLASS typifying names of
variables/slots.  The typification is done calling function in
NAME-COMPLETION-FUNCTIONS passing the BASE-TYPE, which is the
missing bit to fully qualify a C name."
  (declare (type list name-completion-functions))
  (mapcar #'(lambda (f)
	      (destructuring-bind (type name) (funcall f base-type)
		(make-instance container-class :name name :type type)))
	  name-completion-functions))

(defun populate-grammar ()

(defrule top-level-statement
    := declaration ";" :rest
    := function-definition :rest
    := :eof)

;; We need to encapsulate the reduced values of certain productions in
;; a list because the parser expects always a list and some rules
;; don't return a list.  This is the most convenient place where to do
;; this.
(defrule declaration
    := single-declaration
    :reduce (list single-declaration)
    := multi-declaration)

(defrule single-declaration
    := forward-declaration
    := struct-declaration
    := union-declaration
    := enum-declaration
    := constant-declaration)

(defrule multi-declaration
    := typedef-declaration
    ;; variables should be last in this rule because of ambiguity with
    ;; other productions
    := variables-declaration)

(defrule type
    := const-or-volatile? type-identifier
    :reduce type-identifier)

(defrule type-identifier
    := primitive-type
    := "struct" identifier
    :reduce (make-instance 'c-struct-ref :ref-name identifier)
    := "union" identifier
    :reduce (make-instance 'c-union-ref :ref-name identifier)
    := struct-declaration
    := union-declaration
    := enum-declaration
    := identifier
    :reduce (make-instance 'c-user-type-ref :ref-name identifier))

(defrule primitive-type
    := "signed"
    :reduce :int
    := "signed"? "char"
    :reduce :char
    := "signed"? "int"
    :reduce :int
    := "signed"? "short" "int"?
    :reduce :short
    := "signed"? "long" "int"?
    :reduce :long
    := "signed"? "long" "long" "int"?
    :reduce :long-long
    := "unsigned" "char"
    :reduce :unsigned-char
    := "unsigned" "int"?
    :reduce :unsigned-int
    := "unsigned" "short" "int"?
    :reduce :unsigned-short
    := "unsigned" "long" "int"?
    :reduce :unsigned-long
    := "unsigned" "long" "long" "int"?
    :reduce :unsigned-long-long
    := "float"
    :reduce :float
    := "double"
    :reduce :double
    := "long" "double"
    :reduce :long-double
    ;; not a variable type but used for functions and pointers
    := "void"
    :reduce :void)

(defrule typedef-declaration
    ;; Looks odd but multiple types can defined with a single typedef
    ;; statement simply separating them with commas
    := "typedef" type (+ name-with-attributes ",")
    :reduce (typify-names type name-with-attributes
			  'c-typedef-declaration))

(defrule struct-declaration
    := "struct" identifier slots
    :reduce (make-instance 'c-struct-declaration :name identifier :slots slots)
    := "struct" slots
    :reduce (make-instance 'c-struct-declaration :slots slots))

;; We return the forward declarations as incomplete types
(defrule forward-declaration
    := "struct" identifier
    :reduce (make-instance 'c-struct-declaration :name identifier :slots '())
    := "union" identifier
    :reduce (make-instance 'c-union-declaration :name identifier :slots '()))

(defrule function-signature
    := "(" function-signature-arguments ")")

(defrule function-signature-arguments
  := "void"	    ; this first because of ambiguity with other types
  :reduce '()
  := (* function-argument ",")
  := (+ function-argument ",") "," "..."
  :reduce (append $1 (list (make-instance 'c-argument :name nil :type :vararg)))
  :=)

(defrule function-argument
    := type name-with-attributes
    :reduce (destructuring-bind (type name) (funcall name-with-attributes type)
	      (make-instance 'c-argument :name name :type type)))

(defrule storage-class-specifier
    := "auto"
    := "register"
    := "extern"
    := "static")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; The following rules, up to the next separator, are the trickery to
;;; digest the baroque C syntax for the declaration of the variable
;;; type and name.  We basicaly create an implicit stack with
;;; closures, that we unwind when we complete the variables with the
;;; basic type.

(defrule variables-declaration
    := storage-class-specifier? type (+ name-with-attributes ",")
    :reduce (if (string= "static" storage-class-specifier)
		'()
		(typify-names type name-with-attributes
			      'c-variable-declaration)))

(defrule slot-variables-declaration
    := type (+ name-with-attributes ",") ";"
    :reduce (typify-names type name-with-attributes 'c-slot))

(defrule name-with-attributes
    := name-with-attributes1
    := identifier ":" integer
    :reduce #'(lambda (base-type)
		(list (make-instance 'c-bitfield :type base-type :bits integer)
		      identifier))
    := const-or-volatile? "*" const-or-volatile? name-with-attributes
    :reduce #'(lambda (base-type)
		(funcall name-with-attributes
			 (make-instance 'c-pointer :type base-type))))

(defrule name-with-attributes1
    := name-with-attributes1 function-signature
    :reduce #'(lambda (base-type)
		(funcall name-with-attributes1
			 (make-instance 'c-function
					:type base-type
					:arguments function-signature)))
    := name-with-attributes1 array-dimension+
    :reduce #'(lambda (base-type)
		(funcall name-with-attributes1
			 (make-instance 'c-array
					:type base-type
					:dimensions array-dimension)))
    := simple-name)

(defrule simple-name
    := "(" name-with-attributes ")"
    := identifier?
    :reduce #'(lambda (base-type)
		(list base-type identifier)))

(defrule const-or-volatile
    := "const"
    := "volatile")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defrule enum-declaration
    := "enum" enum-body
    :reduce (make-instance 'c-enum-declaration :constants enum-body)
    := "enum" identifier enum-body
    :reduce (make-instance 'c-enum-declaration :name identifier :constants enum-body))

(defrule enum-body
    := "{" enum-constants "}")

(defrule enum-constants
    := (+ enum-argument ","))

(defrule enum-argument
    := identifier
    := identifier "=" expression)

(defrule union-declaration
    := "union" slots
    :reduce (make-instance 'c-union-declaration :slots slots)
    := "union" identifier slots
    :reduce (make-instance 'c-union-declaration :name identifier :slots slots))

(defrule slots
    := "{" slot-variables-declaration+ "}"
    ;; we concatenate because each slot may contain more than one
    ;; variable declaration as in: "int a, b;"
    :reduce (mapcan #'identity slot-variables-declaration))

;; this is to parse our fake declaration to get the value of symbolic
;; constants from the preprocessor
(defrule constant-declaration
    := "const" string "=" expression
    :reduce (make-instance 'c-constant-declaration :name string :value expression))

(defrule array-dimension
  := "[" expression "]"
  := "[" "]"
  :reduce (list))

(defrule function-declaration
  := storage-class-specifier? type name-with-attributes function-signature
  :reduce (if (string= "static" storage-class-specifier)
		'()
		(destructuring-bind (type name) (funcall name-with-attributes type)
		  (list (make-instance 'c-function-declaration
				 :type type
				 :arguments function-signature
				 :name name)))))

;; in case we are fed with a .c source file we collect the defintions
;; as well, but we skip the body (we wouldn't be able to parse it
;; anyway)
(defrule function-definition
  := function-declaration "{"
  :reduce (cons :skip-block function-declaration))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; The following rules, up to the next separator, are for numeric
;;; expressions.  Beware, they are mostly untested.

(defrule expression
    := expression "||" expression1
    :tag 'or
    := expression1)

(defrule expression1
    := expression1 "&&" expression2
    :tag 'and
    := expression2)

(defrule expression2
    := expression2 "==" expression3
    :tag '=
    := expression2 ">=" expression3
    :tag '>=
    := expression2 "<=" expression3
    :tag '<=
    := expression2 "!=" expression3
    :tag (list 'not (list '= expression2 expression3))
    := expression2 ">" expression3
    :tag '>
    := expression2 "<" expression3
    :tag '<
    := expression3)

(defrule expression3
    := expression3 "+" expression4
    :tag '+
    := expression3 "-" expression4
    :tag '-
    := expression3 ">>" expression4
    :reduce `(ash ,expression3 (- ,expression4))
    := expression3 "<<" expression4
    :reduce `(ash ,expression3 ,expression4)
    := expression3 "|" expression4
    :tag 'logior
    := expression4)

;; high priority expression
(defrule expression4
    := expression4 "*" expression5
    :tag '*
    := expression4 "/" expression5
    ;; this is hard to guess because we don't know the type of the
    ;; operands
    :tag '/
    := expression4 "%" expression5
    :tag 'mod
    := expression4 "&" expression5
    :tag 'logand
    := expression5)

(defrule expression5
    := "~" expression5
    :tag 'lognot
    := "!" expression5
    :tag 'not
    := "++" expression5
    :tag 'incf
    := "--" expression5
    :tag 'decf
    := expression5 "++"
    :tag 'incf				;not quite
    := expression5 "--"
    :tag 'decf				;not quite
    := expression6)

(defrule expression6
    := constant
    := "(" expression ")")

(defrule constant
  := number
  := character
  := string
  := identifier
  := "sizeof" "(" function-argument ")"
  :tag :sizeof)

(defrule number
  := integer
  :reduce integer
  := "-" integer
  :reduce (- integer)
  := float
  := "-" float
  :reduce (- float))

)					; end of POPULATE-GRAMMAR
