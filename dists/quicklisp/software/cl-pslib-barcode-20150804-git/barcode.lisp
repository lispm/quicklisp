;; This software is Copyright (c) cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(in-package :cl-pslib-barcode)


(defparameter *bar-width* .8) ;; in millimiter
(defparameter *bar-height* 40) ;; in millimiter

(defun draw-bar (psdoc x w h &optional (color (cl-colors:rgb 0 0 0)))
  (ps:setcolor psdoc ps:+color-type-fillstroke+ color)
  (ps:rect psdoc x 0 w h))


(defun draw-bars (psdoc bars w h)
  (ps:save psdoc)
  (let ((w (do ((x 0 (+ x *bar-width*))
		(c-bars bars (rest c-bars)))
	       ((not c-bars) x)
	     (draw-bar psdoc x w h (if (first c-bars) cl-colors:+black+ cl-colors:+white+))
	     (ps:fill-path psdoc))))

    (ps:restore psdoc)
    w))


(defun barcode-width (bars single-w)
  (* single-w (length bars)))

(defclass barcode ()
  ((height 
    :initform *bar-height*
    :initarg :height
    :accessor height)
   (width
    :initform 0
    :accessor width)
   (single-bar-width
    :initform *bar-width*
    :initarg single-bar-width
    :accessor bar-width)))

(defgeneric parse (object codes))
(defgeneric draw (object psdoc))
