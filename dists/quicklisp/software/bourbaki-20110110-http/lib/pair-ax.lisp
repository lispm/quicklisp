;; Ordered pairs

(module "pair-ax"

  (bimport [!!logic-ax])
  (bimport [!!set-extent-ax])
  (bimport [!!descr-ax])

  (prim st ":" (st ![a b])
    (meta (:html-fn . #'(lambda (line stream)
                          (html-img "langle" stream)
                          (html-dump-symtree (third line) stream)
                          (format stream ",")
                          (html-dump-symtree (second line) stream)
                          (html-img "rangle" stream)))))

  (def "df-1st" st "1st" (st "a") (sv ![x y])
       [the x ∃ y = a : x y])
  (def "df-2nd" st "2nd" (st "a") (sv ![x y])
       [the x ∃ y = a : y x])
  (def "df-conv" st "conv" (st "a") (sv ![x y z])
       [the x ∃ y ∃ z ∧ = x : y z = a : z y])

  (def "df-is-pair" pr "is-pair" (st "a") (sv ![x y])
       [∃ x ∃ y = a : x y])

  (ax "ax-pair" (st ![a b c d])
      (ass [↔ = : a b : c d ∧ = a c = b d]))

) ; end module "pair-ax"
