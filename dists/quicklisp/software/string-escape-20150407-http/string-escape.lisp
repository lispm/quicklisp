;;; string-escape.lisp - Emacs and Python style string escapes in #"..." form
;;; Copyright (c) 2014 Devon Sean McCullough
;;; Licensed under the GNU GPL

;;; #"Emacs style string"
;;; #"""Python style triple quoted string"""

(defpackage #:string-escape
  (:use #:cl)
  (:export |#"-reader|))

(in-package #:string-escape)

(defun |#"-reader| (stream subchar arg)
  "Reader macro accepts #\"...\" C compatible string literals
and #\"\"\"...\"\"\" Python compatible string literals,
both augmented with the \\e, \\d, \\s and \\<NewLine> Emacs escapes."
  ;; TODO:
  ;; Unicode and hex codes over two digits
  ;; ISSUES:
  ;; #"""\x""" ==> "\\x" by strict Python syntax -- maybe flush
  ;; #""" " """ baffles Emacs & Slime colorizing -- workaround by ;"
  ;; Maybe #n" might enable variations, error checking or something.
  (labels ((number (base max-digits)
	      "Parse number in BASE up to MAX-DIGITS."
	      (loop for number = 0 then (+ (* base number) n)
		    for digits upfrom 0
		    for n = (and (< digits max-digits)
				 (digit-char-p (peek-char nil stream) base))
		    do (if n
			   (read-char stream)
			 (return (values number digits))))))
    (assert (char= #\" subchar))
    (when arg (error "Reader syntax #~D\" not defined"
		     arg))
    (cond ((char/= #\" (peek-char nil stream))	; #"...
	   (coerce (delete-if #'null
		    (loop
		       for c = (read-char stream)
		       until (char= #\" c)
		       collect (if (char/= #\\ c)
				   c
				   (let ((d (read-char stream)))
				     (case d
				       (#\NewLine nil)	; Emacs & Python
				       (#\Space nil)	; Emacs
				       (#\a (code-char 7))
				       (#\b #\BackSpace)
				       (#\e (code-char 27))	; Emacs
				       (#\d #\RubOut)   	; Emacs
				       (#\f #\Page)
				       (#\n #\LineFeed)
				       (#\r #\Return)
				       (#\s #\Space)    	; Emacs
				       (#\t #\Tab)
				       (#\v (code-char 11))
				       ((#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7)
					(unread-char d stream)
					(code-char (number 8 3)))
				       (#\x
					(code-char (number 16 2)))
				       (t d))))))
		   'string))
	  ((char/= #\" (progn (read-char stream)
			      (peek-char nil stream)))
	   (make-string 0))		; #""
	  (t
	   (read-char stream)			; #"""...
	   (coerce (nreverse
		    (cddr
		     (nreverse
		      (loop
			 for c = (read-char stream)
			 for quotes = (if (char= #\" c)
					  (1+ quotes)
					  0)
			 until (= 3 quotes)
			 nconc (if (char/= #\\ c)
				     (list c)
				     (let ((d (read-char stream)))
				       (case d
					 (#\NewLine nil)	; Emacs & Python
					 (#\Space nil)  	; Emacs
					 (#\a (list (code-char 7)))
					 (#\b (list #\BackSpace))
					 (#\e (list (code-char 27)))	; Emacs
					 (#\d (list #\RubOut))   	; Emacs
					 (#\f (list #\Page))
					 (#\n (list #\LineFeed))
					 (#\r (list #\Return))
					 (#\s (list #\Space))    	; Emacs
					 (#\t (list #\Tab))
					 (#\v (list (code-char 11)))
					 ((#\" #\' #\\) (list d))
					 ((#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7)
					  (unread-char d stream)
					  (list (code-char (number 8 3))))
					 (#\x
					  (let ((e (peek-char nil stream)))
					    (multiple-value-bind (number digits) (number 16 2)
					      (case digits
						(2 (list (code-char number)))
						(1 (list c d e))
						(0 (list c d))
						(t (assert nil))))))
					 (t (list c d)))))))))
		   'string)))))

(set-dispatch-macro-character #\# #\" #'|#"-reader|)

; (set-dispatch-macro-character #\# #\" nil)	; should disable but not evidently sanctioned in HyperSpec
; (get-dispatch-macro-character #\# #\")

#|
	The C Programming Language, 2nd Ed.
 Chapter 2 - Types, Operators and Expressions
  2.3 Constants
	\a	alert (bell) character	\\  	backslash         
	\b	backspace             	\?  	question mark     
	\f	formfeed              	\'  	single quote      
	\n	newline               	\"  	double quote      
	\r	carriage return       	\ooo	octal number      
	\t	horizontal tab        	\xhh	hexadecimal number
	\v	vertical tab
http://zanasi.chem.unisa.it/download/C.pdf

	GNU Emacs Lisp Reference Manual	version 24.3
 2.3.3.1 Basic Char Syntax
     ?\a => 7                 ; control-g, `C-g'
     ?\b => 8                 ; backspace, <BS>, `C-h'
     ?\t => 9                 ; tab, <TAB>, `C-i'
     ?\n => 10                ; newline, `C-j'
     ?\v => 11                ; vertical tab, `C-k'
     ?\f => 12                ; formfeed character, `C-l'
     ?\r => 13                ; carriage return, <RET>, `C-m'
     ?\e => 27                ; escape character, <ESC>, `C-['
     ?\s => 32                ; space character, <SPC>
     ?\\ => 92                ; backslash character, `\'
     ?\d => 127               ; delete character, <DEL>
2.3.3.2 General Escape Syntax
...	\uNNNN ... Unicode code point `U+NNNN' ... four digits
...	\U00NNNNNN ... code point `U+NNNNNN' ... hexadecimal
...	The Unicode Standard only defines code points up to `U+10FFFF'
...	\x41 ... any number of hex digits
...	\101' ... up to octal code 777
2.3.8.2 Non-ASCII Characters in Strings
...	`\ ' (backslash and space) is just like
...	backslash-newline; it does not contribute any character
...	but it does terminate any preceding ... escape.
https://www.gnu.org/software/emacs/manual/pdf/elisp.pdf

	The Python Language Reference	Release 2.7.8
 2.4.1 String literals
	Escape Sequence 	Meaning                                                     	Notes
	\newline        	Ignored                                                     
	\\              	Backslash (\)                                               
	\'              	Single quote (’)                                            
	\"              	Double quote (\")                                          
	\a              	ASCII Bell (BEL)                                            
	\b              	ASCII Backspace (BS)                                        
	\f              	ASCII Formfeed (FF)                                         
	\n              	ASCII Linefeed (LF)                                         
	\N{name}        	Character named name in the Unicode database (Unicode only) 
	\r              	ASCII Carriage Return (CR)                                  
	\t              	ASCII Horizontal Tab (TAB)                                  
	\uxxxx          	Character with 16-bit hex value xxxx (Unicode only)         	(1)  
	\Uxxxxxxxx      	Character with 32-bit hex value xxxxxxxx (Unicode only)     	(2)  
	\v              	ASCII Vertical Tab (VT)                                     
	\ooo            	Character with octal value ooo                              	(3,5)
	\xhh            	Character with hex value hh                                 	(4,5)
  Notes:
	1. Individual code units which form parts of a surrogate pair can be encoded using this escape sequence.
	2. Any Unicode character can be encoded this way, but characters outside the Basic Multilingual Plane (BMP)
	   will be encoded using a surrogate pair if Python is compiled to use 16-bit code units (the default).
	3. As in Standard C, up to three octal digits are accepted.
	4. Unlike in Standard C, exactly two hex digits are required.
	5. In a string literal, hexadecimal and octal escapes denote the byte with the given value;
	   it is not necessary that the byte encodes a character in the source character set.
	   In a Unicode literal, these escapes denote a Unicode character with the given value.
  Unlike Standard C, all unrecognized escape sequences are left in the string unchanged,
  i.e., the backslash is left in the string.  (This behavior is useful when debugging:
  if an escape sequence is mistyped, the resulting output is more easily recognized as broken.)
  It is also important to note that the escape sequences marked as “(Unicode only)”
  in the table above fall into the category of unrecognized escapes for non-Unicode string literals.
https://docs.python.org/2/archives/python-2.7.8-docs-pdf-letter.tar.bz2
|#

(provide '#:string-escape)

;;; string-escape.lisp end
