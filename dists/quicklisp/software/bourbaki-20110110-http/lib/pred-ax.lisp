(module "pred-ax"

  (meta (:comment . "Axioms for predicate calculus.")
        (:description . "Pure predicate calculus (without equality or substitution).
Converted from the predicate calculus part of set.mm (theorems 861 - 9xx)."))

  (symkind "WFF")
  (symkind "VAR")

  (export [!!prop-ax])

  ;; Declare the Universal Quantifier
  (prim wff "∀" ((:bound var) "x" wff "φ"))
;    (meta (:html-sym . "forall")
;          (:html-fn . #'polish-prefix)))

  ;; Axioms for Predicate Calculus

  ; Axiom of Specialization
  (ax "ax4" (var "x" wff "φ")
      (meta (:comment . "Axiom of Specialization.")
            (:description . "A quantified wff implies the wff without a quantifier
(i.e. an instance, or special case, of the generalized wff). In other words if
something is true for all x, it is true for any specific x (that would typically
occur as a free variable in the wff substituted for phi). This is one of the 4 axioms
of what we call \"pure\" predicate calculus. Unlike the more typical textbook Axiom of
Specialization, we cannot choose a variable different from x for the special case.
That is dealt with later when substitution is introduced. Axiom scheme C5' in [Megill]
p. 448. Also appears as Axiom B5 of [Tarski] p. 67 (under his system S2, defined on the
last paragraph on p. 77). Note that the converse of this axiom does not hold in general,
but a weaker inference form of the converse holds and is expressed as rule
<a href=\"pred-ax/ax-gen.html\">ax-gen</a>. Conditional forms of the converse are given
by ax-12, ax-16, and ax-17."))
      (ass [→ ∀ x φ φ]))

  ; Axiom of Quantified Implication
  (ax "ax5" (var "x" wff ![φ ψ])
      (meta (:comment . "Axiom of Quantified Implication.")
            (:description . "This axiom moves a quantifier from outside to inside
an implication, quantifying psi. Notice that x must not be a free variable in the
antecedent of the quantified implication, and we express this by binding phi to
\"protect\" the axiom from a phi containing a free x. One of the 4 axioms of pure
predicate calculus. Axiom scheme C4' in [Megill] p. 448. It is a special case of
Lemma 5 of [Monk2] p. 108."))
      (ass [→ ∀ x → ∀ x φ ψ → ∀ x φ ∀ x ψ]))

  ; Axiom of Quantified Negation
  (ax "ax6" (var "x" wff "φ")
      (meta (:comment . "Axiom of Quantified Negation.")
            (:description . "This axiom is used to manipulate negated quantifiers.
One of the 4 axioms of pure predicate calculus. Axiom scheme C7' in [Megill] p. 448.
An equivalent variant appears as Axiom C5-2 of [Monk2] p. 113."))
      (ass [→ ¬ ∀ x ¬ ∀ x φ φ]))

  ; Axiom of Quantifier Commutation
  (ax "ax7" (var ![x y] wff "φ")
      (meta (:comment . "Axiom of Quantifier Commutation.")
            (:description . "This axiom says universal quantifiers can be swapped.
One of the 4 axioms of pure predicate calculus. Axiom scheme C6' in [Megill] p. 448.
Also appears as Lemma 12 of [Monk2] p. 109."))
      (ass [→ ∀ x ∀ y φ ∀ y ∀ x φ]))

  ; Axiom of Generalization
  (ax "ax-gen" (var "x" wff "φ")
      (meta (:comment . "Rule of Generalization.")
            (:description . "The postulated inference rule of pure predicate calculus.
See e.g. Rule 2 of [Hamilton] p. 74. This rule says that if something is unconditionally
true, then it is true for all values of a variable. For example, if we have proved
x = x, we can conclude A. x (x = x) or even A. y (x = x)."))
      (hypo [φ])
      (ass [∀ x φ]))

  ;; Define the Existential Quantifier
  (def "df-ex" wff "∃" ((:bound var) "x" wff "φ") ()
       [¬ ∀ x ¬ φ]
    (meta (:comment . "Define existential quantification.")
          (:description . "E. x phi means \"there exists at least one set x such that
phi is true.\" Definition of [Margaris] p. 49.")))
;          (:html-sym . "exists")
;          (:html-fn . #'polish-prefix)))

  ;; Define the relation "x is (effectively) bound in ph"
  ;; ('Bvp' = Bound variable - pr)
  (def "df-Bvp" wff "Bvp" ((:bound var) "x" wff "φ") ()
       [∀ x → φ ∀ x φ]
    (meta (:comment . "Define (effectively) bound variable.")))
;          (:html-fn . #'(lambda (tree stream)
;                          (format stream "(")
;                          (html-dump-symtree (third tree) stream)
;                          (format stream " bound in ")
;                          (html-dump-symtree (second tree) stream)
;                          (format stream ")")))))

) ; end context pred-ax
