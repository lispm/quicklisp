;;; Copyright (c) Juha Arpiainen 2005-06

;;; This file is part of Bourbaki.

;;; Bourbaki is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.

;;; Bourbaki is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Bourbaki; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; -------

;;; Global variables

(in-package :bourbaki)

(defun mkrootctx ()
  (let ((temp (make-context :name "" :class :context)))
    (setf (context-imports temp) (list (make-symref :args-needed 0
                                                    :class :implicit
                                                    :target temp
                                                    :fn (load-time-value #'(lambda () nil)))))
    (setf (context-exports temp) (list (make-symref :args-needed 0
                                                    :class :implicit
                                                    :target temp
                                                    :fn (load-time-value #'(lambda () nil)))))
    temp))

(defvar *root-context* (mkrootctx))

;;; Set by parser macros and the verifier
(defvar *current-context* *root-context*)
(defvar *meta-context* nil)
(defvar *current-proof* nil)

(defvar *theorem-patterns* (make-pattern-tree))

;;; Higher value -> more checks made by the verifier
;(defvar *verify-level* 0)

;;; Currently selects function used to print symtrees
(defvar *bourbaki-debug* nil)

;;; Location of theorem files, relative to bourbaki path
(defvar *bourbaki-library-path* "lib")

(defvar *bourbaki-version* '(3 7)) ; Version 3.7
