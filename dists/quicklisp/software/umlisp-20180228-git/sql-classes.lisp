;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: umlisp -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:     sql-classes.lisp
;;;; Purpose:  Routines for reading UMLS objects from SQL database
;;;; Author:   Kevin M. Rosenberg
;;;; Created:  Apr 2000
;;;;
;;;; This file, part of UMLisp, is
;;;;    Copyright (c) 2000-2010 by Kevin M. Rosenberg, M.D.
;;;;
;;;; UMLisp users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License.
;;;; *************************************************************************

(in-package #:umlisp)


(defvar *current-srl* nil)
(defun current-srl ()
  *current-srl*)
(defun set-current-srl (srl)
  (setq *current-srl* srl))

(defmacro query-string (table fields srl where-name where-value
                        &key (lrl "KCUILRL") single distinct order like limit
                        filter)
  (let* ((%%fields (format nil "SELECT ~A~{~:@(~A~)~^,~} FROM ~:@(~A~)"
                           (if distinct "DISTINCT " "") fields table))
         (%%order (if order (format nil " ORDER BY ~{~:@(~A~) ~(~A~)~^,~}"
                                    order)
                      ""))
         (%%lrl (format nil " AND ~:@(~A~)<=" lrl))
         (%%where (when where-name
                    (format nil " WHERE ~:@(~A~)~A" where-name
                          (if like " like " ""))))
         (%filter (gensym "FILTER-"))
         (%single (gensym "SINGLE-"))
         (%limit (gensym "LIMIT-")))
    `(let ((,%limit ,limit)
           (,%single ,single)
           (,%filter ,filter))
       (concatenate
        'string
        ,%%fields
        ,@(when %%where (list %%where))
        ,@(when %%where
            `((typecase ,where-value
                #+ignore
                (fixnum
                 (concatenate 'string "='" (prefixed-fixnum-string ,where-value #\0 10) "'"))
                (number
                 (concatenate 'string "='" (write-to-string ,where-value) "'"))
                (null
                 " IS NULL")
                (t
                 (format nil ,(if like "'%~A%'" "='~A'") ,where-value)))))
        (if ,%filter (concatenate 'string
                                  ,(if %%where " AND " " WHERE ")
                                  ,%filter) "")
        (if ,srl (concatenate 'string ,%%lrl (write-to-string ,srl)) "")
        ,@(when %%order (list %%order))
        (cond
         ((and ,%single ,%limit)
          (error  "Can't set single (~S) and limit (~S)" ,%single ,%limit))
         (,%single
          " LIMIT 1")
         (,%limit
          (format nil " LIMIT ~D" ,%limit))
         (t
          ""))))))

(defun query-string-eval (table fields srl where-name where-value
                          &key (lrl "KCUILRL") single distinct order like limit filter)
  (when single (setq limit 1))
  (concatenate
   'string
   (format nil "SELECT ~A~{~:@(~A~)~^,~} FROM ~:@(~A~)"
           (if distinct "DISTINCT " "") fields table)
   (if where-name (format nil " WHERE ~:@(~A~)" where-name) "")
   (if where-name
       (format nil
               (typecase where-value
                 (number "='~D'")
                 (null " IS NULL")
                 (t
                  (if like " LIKE '%~A%'" "='~A'")))
               where-value)
       "")
   (if filter (concatenate 'string " AND " filter) nil)
   (if srl (format nil " AND ~:@(~A~)<=~D" lrl srl) "")
   (if order (format nil " ORDER BY ~{~:@(~A~) ~(~A~)~^,~}" order) "")
   (if limit (format nil " LIMIT ~D" limit) "")))


(defmacro umlisp-query (table fields srl where-name where-value
                     &key (lrl "KCUILRL") single distinct order like
                     limit filter (query-cmd 'mutex-sql-query))
  "Query the UMLisp database. Return a list of umlisp objects whose name
is OBJNAME from TABLE where WHERE-NAME field = WHERE-VALUE with FIELDS"
  `(,query-cmd
    (query-string ,table ,fields ,srl ,where-name ,where-value
     :lrl ,lrl :single ,single :distinct ,distinct :order ,order :like ,like
     :filter ,filter :limit ,limit)))

(defmacro umlisp-query-eval (table fields srl where-name where-value
                     &key (lrl "KCUILRL") single distinct order like
                     filter limit)
  "Query the UMLisp database. Return a list of umlisp objects whose name
is OBJNAME from TABLE where WHERE-NAME field = WHERE-VALUE with FIELDS"
  `(mutex-sql-query
    (query-string-eval ,table ,fields ,srl ,where-name ,where-value
     :lrl ,lrl :single ,single :distinct ,distinct :order ,order :like ,like
     :filter ,filter :limit ,limit)))

;; only WHERE-VALUE and SRL are evaluated
(defmacro collect-umlisp-query ((table fields srl where-name where-value
                                    &key (lrl "KCUILRL") distinct single
                                    order like (query-cmd 'mutex-sql-query)
                                    filter limit)
                                &body body)
  (let ((value (gensym))
        (r (gensym)))
    (if single
        (if (and limit (> limit 1))
            (error "Can't set limit along with single.")
          `(let* ((,value ,where-value)
                  (tuple (car (umlisp-query ,table ,fields ,srl ,where-name ,value
                                            :lrl ,lrl :single ,single
                                            :distinct ,distinct :order ,order
                                            :like ,like :filter ,filter
                                            :query-cmd ,query-cmd))))
             ,@(unless where-name `((declare (ignore ,value))))
             (when tuple
               (destructuring-bind ,fields tuple
                 ,@body))))
        `(let ((,value ,where-value))
           ,@(unless where-name `((declare (ignore ,value))))
           (let ((,r '()))
             (dolist (tuple (umlisp-query ,table ,fields ,srl ,where-name ,value
                                          :lrl ,lrl :single ,single :distinct ,distinct
                                          :order ,order :filter ,filter :like ,like
                                          :limit ,limit))
               (push (destructuring-bind ,fields tuple ,@body) ,r))
             (nreverse ,r))
           #+ignore
           (loop for tuple in
                 (umlisp-query ,table ,fields ,srl ,where-name ,value
                               :lrl ,lrl :single ,single :distinct ,distinct
                               :order ,order :like ,like :filter ,filter :limit ,limit)
               collect (destructuring-bind ,fields tuple ,@body))))))

(defmacro collect-umlisp-query-eval ((table fields srl where-name where-value
                                         &key (lrl "KCUILRL") distinct single
                                         order like filter limit)
                                  &body body)
  (let ((value (gensym))
        (r (gensym))
        (eval-fields (cadr fields)))
    (if single
        `(let* ((,value ,where-value)
                (tuple (car (umlisp-query-eval ,table ,fields ,srl ,where-name ,value
                                               :lrl ,lrl :single ,single
                                               :distinct ,distinct :order ,order
                                               :like ,like :filter ,filter
                                               :limit ,limit))))
          (when tuple
            (destructuring-bind ,eval-fields tuple
              ,@body)))
        `(let ((,value ,where-value)
               (,r '()))
           (dolist (tuple (umlisp-query-eval ,table ,fields ,srl ,where-name ,value
                                             :lrl ,lrl :single ,single :distinct ,distinct
                                             :order ,order :like ,like
                                             :filter ,filter :limit ,limit))
             (push (destructuring-bind ,eval-fields tuple ,@body) ,r))
           (nreverse ,r)
           #+ignore
           (loop for tuple in
                 (umlisp-query-eval ,table ,fields ,srl ,where-name ,value
                                    :lrl ,lrl :single ,single :distinct ,distinct
                                    :order ,order :like ,like :filter ,filter
                                    :limit ,limit)
               collect (destructuring-bind ,eval-fields tuple ,@body))))))

;;;
;;; Read from SQL database

(defmacro ensure-cui-integer (cui)
  `(if (stringp ,cui)
    (setq ,cui (parse-cui ,cui))
    ,cui))

(defmacro ensure-lui-integer (lui)
  `(if (stringp ,lui)
    (setq ,lui (parse-lui ,lui))
    ,lui))

(defmacro ensure-sui-integer (sui)
  `(if (stringp ,sui)
    (setq ,sui (parse-sui ,sui))
    ,sui))

(defmacro ensure-aui-integer (aui)
  `(if (stringp ,aui)
    (setq ,aui (parse-aui ,aui))
    ,aui))

(defmacro ensure-rui-integer (rui)
  `(if (stringp ,rui)
    (setq ,rui (parse-rui ,rui))
    ,rui))

(defmacro ensure-tui-integer (tui)
  `(if (stringp ,tui)
    (setq ,tui (parse-tui ,tui))
    ,tui))

(defmacro ensure-eui-integer (eui)
  `(if (stringp ,eui)
    (setq ,eui (parse-eui ,eui))
    ,eui))

(defun make-ucon-cui (cui)
  (ensure-cui-integer cui)
  (when cui
    (make-instance 'ucon :cui cui)))

(defun find-ucon-cui (cui &key (srl *current-srl*) without-pfstr)
  "Find ucon for a cui. If set SAB, the without-pfstr is on by default"
  (ensure-cui-integer cui)
  (unless cui (return-from find-ucon-cui nil))

  (if without-pfstr
      (collect-umlisp-query (mrconso (kcuilrl) srl cui cui :single t)
                            (make-instance 'ucon :cui cui :lrl (ensure-integer kcuilrl)
                                           :pfstr nil)))
   (or
     (collect-umlisp-query (mrconso (kcuilrl str) srl cui cui :single t :filter "KPFENG=1")
        (make-instance 'ucon :cui cui :pfstr str
                       :lrl kcuilrl))
     (collect-umlisp-query (mrconso (kcuilrl str) srl cui cui :single t)
        (make-instance 'ucon :cui cui :pfstr str
                       :lrl kcuilrl))))

(defun find-uconso-cui (cui &key sab (srl *current-srl*))
  "Find uconso for a cui."
  (ensure-cui-integer cui)
  (unless cui (return-from find-uconso-cui nil))

  (collect-umlisp-query (mrconso (lat ts lui stt sui ispref aui saui scui sdui sab tty code str
                                      srl suppress cvf kpfeng kcuisui kcuilui kcuilrl
                                      kluilrl ksuilrl) srl cui cui
                                      :filter (if sab (concatenate 'string "SAB='" sab "'") nil))
    (make-instance 'uconso :cui cui :lat lat :ts ts :lui lui :stt stt :sui sui :ispref ispref
                   :aui aui :saui saui :scui scui :sdui sdui :sab sab :tty tty :code code
                   :str str :srl srl :suppress suppress :cvf cvf :kpfeng kpfeng
                   :kcuisui kcuisui :kcuilui kcuilui :kcuilrl kcuilrl :kluilrl kluilrl
                   :ksuilrl ksuilrl)))

(defun find-uconso-cuisui (cuisui &key sab (srl *current-srl*))
  "Find uconso for a cuisui."
  (collect-umlisp-query (mrconso (cui lat ts lui stt sui ispref aui saui scui sdui sab tty code str
                                      srl suppress cvf kpfeng kcuisui kcuilui kcuilrl
                                      kluilrl ksuilrl) srl kcuisui cuisui
                                      :filter (if sab (concatenate 'string "SAB='" sab "'") nil))
    (make-instance 'uconso :cui cui :lat lat :ts ts :lui lui :stt stt :sui sui :ispref ispref
                   :aui aui :saui saui :scui scui :sdui sdui :sab sab :tty tty :code code
                   :str str :srl srl :suppress suppress :cvf cvf :kpfeng kpfeng
                   :kcuisui kcuisui :kcuilui kcuilui :kcuilrl kcuilrl :kluilrl kluilrl
                   :ksuilrl ksuilrl)))


(defun find-uconso-code (code &key first sab (srl *current-srl*) (like nil))
  "Return list of uconso objects that match code. Optional, filter for SAB. Optionally, use SQL's LIKE syntax"
  (collect-umlisp-query (mrconso (cui sab) srl code code :like like :distinct t
                                 :lrl klrl
                                 :filter (if sab (concatenate 'string "SAB='" sab "'") nil))
    (let ((uconsos (find-uconso-cui cui :sab sab :srl srl)))
      (if first
          (first uconsos)
          uconsos))))

(defun find-uconso-sui (sui &key sab (srl *current-srl*))
  "Find uconso for a sui. If set SAB, the without-pfstr is on by default"
  (ensure-sui-integer sui)
;;  (unless (and sui (stringp sab))
  (unless sui
    (return-from find-uconso-sui nil))

  (collect-umlisp-query (mrconso (cui lat ts lui stt sui ispref aui saui scui sdui sab tty code str
                                      srl suppress cvf kpfeng kcuisui kcuilui kcuilrl
                                      kluilrl ksuilrl) srl sui sui
                                      :distinct t
                                      :filter (if sab (concatenate 'string "SAB='" sab "'") nil))
    (make-instance 'uconso :cui cui :lat lat :ts ts :lui lui :stt stt :sui sui :ispref ispref
                   :aui aui :saui saui :scui scui :sdui sdui :sab sab :tty tty :code code
                   :str str :srl srl :suppress suppress :cvf cvf :kpfeng kpfeng
                   :kcuisui kcuisui :kcuilui kcuilui :kcuilrl kcuilrl :kluilrl kluilrl
                   :ksuilrl ksuilrl)))

(defun find-pfstr-cui (cui &key (srl *current-srl*))
  "Find preferred string for a cui"
  (ensure-cui-integer cui)
  (or
   (collect-umlisp-query (mrconso (str) srl cui cui :distinct t
                                  :filter " KPFENG=1" :single t)
      str)
   (collect-umlisp-query (mrconso (str) srl cui cui :distinct t
                                  :single t)
                         str)))

(defun find-lrl-cui (cui &key (srl *current-srl*))
  "Find LRL for a cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrconso (kcuilrl) srl cui cui :distinct t :single t)
                        (ensure-integer kcuilrl)))

(defun find-ucon-lui (lui &key (srl *current-srl*))
  "Find list of ucon for lui"
  (ensure-lui-integer lui)
  (unless lui (return-from find-ucon-lui nil))
  (or
   (collect-umlisp-query (mrconso (cui kcuilrl str) srl lui lui
                                  :filter " KPFENG=1" :single t)
      (make-instance 'ucon :cui (ensure-integer cui)
                     :pfstr str :lrl (ensure-integer kcuilrl)))
   (collect-umlisp-query (mrconso (cui kcuilrl str) srl lui lui
                                  :single t)
      (make-instance 'ucon :cui (ensure-integer cui)
                     :pfstr str :lrl (ensure-integer kcuilrl)))))

(defun find-ucon-sui (sui &key (srl *current-srl*))
  "Find list of ucon for sui"
  (ensure-sui-integer sui)
  (collect-umlisp-query (mrconso (cui kcuilrl) srl sui sui :distinct t)
    (make-instance 'ucon :cui (ensure-integer cui) :pfstr (find-pfstr-cui cui)
                   :lrl (ensure-integer kcuilrl))))

(defun find-ucon-aui (aui &key (srl *current-srl*))
  "Find list of ucon for aui"
  (ensure-aui-integer aui)
  (collect-umlisp-query (mrconso (cui kcuilrl) srl aui aui :distinct t)
    (make-instance 'ucon :cui (ensure-integer cui) :pfstr (find-pfstr-cui cui)
                   :lrl (ensure-integer kcuilrl))))

(defun find-ucon-cuisui (cuisui &key (srl *current-srl*))
  "Find ucon for cui/sui"
  (collect-umlisp-query (mrconso (cui kcuilrl) srl kcuisui cuisui)
    (make-instance 'ucon :cui cui
                   :pfstr (find-pfstr-cui cui)
                   :lrl (ensure-integer kcuilrl))))

(defun find-ucon-cui-sui (cui sui &key (srl *current-srl*))
  "Find ucon for cui/sui"
  (ensure-cui-integer cui)
  (ensure-sui-integer sui)
  (when (and cui sui)
    (find-ucon-cui-sui cui sui :srl srl)))

(defun find-ucon-str (str &key (srl *current-srl*))
  "Find ucon that are exact match for str"
  (collect-umlisp-query (mrconso (cui kcuilrl) srl str str :distinct t)
    (make-instance 'ucon :cui (ensure-integer cui) :pfstr (find-pfstr-cui cui)
                   :lrl (ensure-integer kcuilrl))))

(defun find-ucon-all (&key (srl *current-srl*))
  "Return list of all ucon's"
  (with-sql-connection (db)
    (clsql:map-query
     'list
     #'(lambda (tuple)
         (destructuring-bind (cui cuilrl) tuple
             (make-instance 'ucon :cui (ensure-integer cui)
                            :pfstr (find-pfstr-cui cui)
                            :lrl (ensure-integer cuilrl))))
     (query-string mrconso (cui kcuilrl) srl nil nil
                   :order (cui asc) :distinct t)
     :database db)))

(defun find-ucon-all2 (&key (srl *current-srl*))
  "Return list of all ucon's"
  (collect-umlisp-query (mrconso (cui kcuilrl) srl nil nil :order (cui asc)
                            :distinct t)
    (make-instance 'ucon :cui (ensure-integer cui)
                   :pfstr (find-pfstr-cui cui)
                   :lrl (ensure-integer kcuilrl))))

(defun find-cui-ucon-all (&key (srl *current-srl*))
  "Return list of CUIs for all ucons"
  (collect-umlisp-query (mrconso (cui) srl nil nil :order (cui asc)
                               :distinct t)
                        cui))

(defun map-ucon-all (fn &key (srl *current-srl*))
  "Map a function over all ucon's"
  (with-sql-connection (db)
    (clsql:map-query
     nil
     #'(lambda (tuple)
         (destructuring-bind (cui cuilrl) tuple
           (funcall fn (make-instance 'ucon :cui (ensure-integer cui)
                                      :pfstr (find-pfstr-cui cui)
                                      :lrl (ensure-integer cuilrl)))))
     (query-string mrconso (cui kcuilrl) srl nil nil :order (cui asc)
                   :distinct t)
     :database db)))


(defun find-udef-cui (cui &key (srl *current-srl*))
  "Return a list of udefs for cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrdef (sab def suppress) srl cui cui :lrl "KSRL"
                               :distinct t)
    (make-instance 'udef :sab sab :def def :suppress suppress)))

(defun find-udoc-key (key)
  "Return list of abbreviation documentation for a key"
  (collect-umlisp-query (mrdoc (value type expl) nil dockey key)
    (make-instance 'udoc :dockey key :dvalue value :dtype type :expl expl)))

(defun find-udoc-value (value)
  "Return abbreviation documentation"
  (collect-umlisp-query (mrdoc (dockey type expl) nil value value)
    (make-instance 'udoc :dockey dockey :dvalue value :dtype type :expl expl)))

(defun find-udoc-key-value (dockey value)
  (collect-umlisp-query (mrdoc (type expl) nil dockey dockey :filter (format nil "VALUE='~A'" value))
      (make-instance 'udoc :dockey dockey :dvalue value :dtype type :expl expl)))

(defun find-udoc-all ()
  "Return all abbreviation documentation"
  (collect-umlisp-query (mrdoc (dockey value type expl) nil nil nil)
    (make-instance 'udoc :dockey dockey :dvalue value :dtype type :expl expl)))

(defun find-usty-cui (cui &key (srl *current-srl*))
  "Return a list of usty for cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrsty (tui sty) srl cui cui :lrl "KLRL")
    (make-instance 'usty :tui (ensure-integer tui) :sty sty)))

(defun find-usty-word (word &key (srl *current-srl*))
  "Return a list of usty that match word"
  (collect-umlisp-query (mrsty (tui sty) srl sty word :lrl klrl :like t
                            :distinct t)
    (make-instance 'usty :tui (ensure-integer tui) :sty sty)))

(defun find-urel-cui (cui &key (srl *current-srl*) filter without-pfstr2)
  "Return a list of urel for cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrrel (aui1 rel stype1 cui2 aui2 stype2 rela rui srui sab sl rg dir suppress cvf)
                               srl cui1 cui :lrl "KSRL" :filter filter)
    (let ((rel
      (make-instance 'urel :cui1 cui :aui1 (ensure-integer aui1) :stype1 stype1 :rel rel
                     :cui2 (ensure-integer cui2) :aui2 (ensure-integer aui2) :stype2 stype2
                     :rui (ensure-integer rui) :srui srui :rela rela :sab sab :sl sl :rg rg :dir dir
                     :suppress suppress :cvf cvf)))
      (unless without-pfstr2
        (setf (slot-value rel 'pfstr2) (find-pfstr-cui cui2)))
      rel)))

(defun find-urel-rui (rui &key (srl *current-srl*))
  "Return the urel for a rui"
  (ensure-rui-integer rui)
  (collect-umlisp-query (mrrel (aui1 rel stype1 cui1 cui2 aui2 stype2 rela rui srui sab sl rg dir suppress cvf)
                               srl rui rui :lrl "KSRL" :single t)
    (make-instance 'urel :cui1 cui1 :aui1 (ensure-integer aui1) :stype1 stype1 :rel rel
                   :cui2 (ensure-integer cui2) :aui2 (ensure-integer aui2) :stype2 stype2
                   :rui (ensure-integer rui) :srui srui :rela rela :sab sab :sl sl :rg rg :dir dir
                   :suppress suppress :cvf cvf :pfstr2 (find-pfstr-cui cui2))))

(defun find-cui2-urel-cui (cui &key (srl *current-srl*))
  "Return a list of urel for cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrrel (cui2) srl cui1
                               cui :lrl "KSRL")
                        cui2))

(defun find-urel-cui2 (cui2 &key (srl *current-srl*))
  "Return a list of urel for cui2"
  (ensure-cui-integer cui2)
  (collect-umlisp-query (mrrel (rel cui1 aui1 stype1 aui2 stype2 rela rui srui sab sl rg dir suppress cvf)
                               srl cui2 cui2 :lrl "KSRL")
    (make-instance 'urel :cui2 cui2 :rel rel :aui2 (ensure-integer aui2)
                   :stype2 stype2 :rui (ensure-integer rui) :srui srui
                   :stype1 stype1 :cui1 (ensure-integer cui1)
                   :aui1 (ensure-integer aui1)
                   :rela rela :sab sab :sl sl :rg rg :dir dir :suppress suppress :cvf cvf
                   :pfstr2 (find-pfstr-cui cui2))))

(defun find-ucon-rel-cui2 (cui2 &key (srl *current-srl*))
  (ensure-cui-integer cui2)
  (loop for cui in (remove-duplicates
                    (mapcar #'cui1 (find-urel-cui2 cui2 :srl srl)))
        collect (find-ucon-cui cui :srl srl)))

#+mrcoc (defun find-ucoc-cui (cui &key (srl *current-srl*))
  "Return a list of ucoc for cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrcoc (aui1 cui2 aui2 sab cot cof coa) srl cui1
                            cui :lrl klrl :order (cof asc))
    (setq cui2 (ensure-integer cui2))
    (when (eql 0 cui2) (setq cui2 nil))
    (make-instance 'ucoc :cui1 cui :aui1 (ensure-integer aui1)
                   :cui2 (ensure-integer cui2) :aui2 (ensure-integer aui2)
                   :cot cot :cof (ensure-integer cof) :coa coa :sab sab
                   :pfstr2 (find-pfstr-cui cui2))))

#+mrcoc (defun find-ucoc-cui2 (cui2 &key (srl *current-srl*))
  "Return a list of ucoc for cui2"
  (ensure-cui-integer cui2)
  (collect-umlisp-query (mrcoc (cui1 aui1 aui2 sab cot cof coa) srl cui2
                            cui2 :lrl klrl :order (cof asc))
    (when (zerop cui2) (setq cui2 nil))
    (make-instance 'ucoc :cui1 (ensure-integer cui1) :cui2 cui2
                   :aui1 (ensure-integer aui1) :aui2 (ensure-integer aui2)
                   :sab sab :cot cot :cof (ensure-integer cof) :coa coa
                   :pfstr2 (find-pfstr-cui cui2))))

#+mrcoc (defun find-ucon-coc-cui2 (cui2 &key (srl *current-srl*))
  "List of ucon with co-occurance cui2"
  (ensure-cui-integer cui2)
  (mapcar
   #'(lambda (cui) (find-ucon-cui cui :srl srl))
   (remove-duplicates (mapcar #'cui1 (find-ucoc-cui2 cui2 :srl srl)))))


(defun find-uterm-cui (cui &key (srl *current-srl*))
  "Return a list of uterm for cui"
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrconso (lui lat ts kluilrl) srl cui cui
                            :lrl kluilrl :distinct t)
    (make-instance 'uterm :lui (ensure-integer lui) :cui cui
                   :lat lat :ts ts :lrl (ensure-integer kluilrl))))

(defun find-uterm-lui (lui &key (srl *current-srl*))
  "Return a list of uterm for lui"
  (ensure-lui-integer lui)
  (collect-umlisp-query (mrconso (cui lat ts kluilrl) srl lui lui
                             :lrl kluilrl :distinct t)
    (make-instance 'uterm :cui (ensure-integer cui) :lui lui
                   :lat lat :ts ts :lrl (ensure-integer kluilrl))))

(defun find-uterm-cuilui (cui lui &key (srl *current-srl*))
  "Return single uterm for cui/lui"
  (ensure-cui-integer cui)
  (ensure-lui-integer lui)
  (collect-umlisp-query (mrconso (lat ts kluilrl) srl kcuilui
                             (make-cuilui cui lui)
                             :lrl kluilrl :single t)
    (make-instance 'uterm :cui cui :lui lui :lat lat :ts ts
                   :lrl (ensure-integer kluilrl))))

(defun find-ustr-cuilui (cui lui &key (srl *current-srl*))
  "Return a list of ustr for cui/lui"
  (ensure-cui-integer cui)
  (ensure-lui-integer lui)
  (collect-umlisp-query (mrconso (sui stt str suppress ksuilrl) srl kcuilui
                                 (make-cuilui cui lui) :lrl ksuilrl :distinct t)
                (make-instance 'ustr :sui (ensure-integer sui) :cui cui :lui lui
                   :cuisui (make-cuisui cui sui) :stt stt :str str :suppress suppress
                   :lrl (ensure-integer ksuilrl))))

(defun find-ustr-cuisui (cuisui &key (srl *current-srl*))
  "Return the single ustr for cuisui"
  (collect-umlisp-query (mrconso (cui lui sui stt str suppress ksuilrl) srl kcuisui
                                 cuisui :lrl ksuilrl :single t)
    (make-instance 'ustr :sui sui :cui cui :cuisui cuisui
                   :lui (ensure-integer lui) :stt stt :str str :suppress suppress
                   :lrl (ensure-integer ksuilrl))))

(defun find-ustr-cui-sui (cui sui &key (srl *current-srl*))
  "Return the single ustr for cuisui"
  (ensure-cui-integer cui)
  (ensure-sui-integer sui)
  (find-ustr-cuisui (make-cuisui cui sui) :srl srl))

(defun find-ustr-sui (sui &key (srl *current-srl*))
  "Return the list of ustr for sui"
  (ensure-sui-integer sui)
  (collect-umlisp-query (mrconso (cui lui stt str suppress ksuilrl) srl sui sui
                            :lrl ksuilrl :distinct t)
    (make-instance 'ustr :sui sui :cui cui :stt stt :str str
                   :cuisui (make-cuisui (ensure-integer cui) sui)
                   :suppress suppress
                   :lui (ensure-integer lui) :lrl (ensure-integer ksuilrl))))

(defun find-ustr-sab (sab &key (srl *current-srl*))
  "Return the list of ustr for sab"
  (collect-umlisp-query (mrconso (kcuisui) srl sab sab :lrl srl)
    (let ((cuisui (ensure-integer kcuisui)))
      (find-ustr-cuisui cuisui :srl srl))))

(defun find-ustr-all (&key (srl *current-srl*))
  "Return list of all ustr's"
    (with-sql-connection (db)
      (clsql:map-query
       'list
       #'(lambda (tuple)
           (destructuring-bind (cui lui sui stt ksuilrl suppress) tuple
             (make-instance 'ustr :cui (ensure-integer cui)
                            :lui (ensure-integer lui) :sui (ensure-integer sui)
                            :stt stt :str (find-pfstr-cui cui)
                            :cuisui (make-cuisui (ensure-integer cui)
                                                 (ensure-integer sui))
                            :suppress suppress
                            :lrl (ensure-integer ksuilrl))))
       (query-string mrconso (cui lui sui stt ksuilrl) srl nil nil :lrl ksuilrl
                     :distinct t
                     :order (sui asc))
       :database db)))

(defun find-string-sui (sui &key (srl *current-srl*))
  "Return the string associated with sui"
  (ensure-sui-integer sui)
  (collect-umlisp-query (mrconso (str) srl sui sui :lrl ksuilrl :single t)
    str))

(defun find-uso-cuisui (cui sui &key (srl *current-srl*))
  (ensure-sui-integer sui)
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrconso (aui sab code srl tty saui sdui scui lat str ts)
                                 srl kcuisui (make-cuisui cui sui) :lrl srl)
    (make-instance 'uso :aui aui :sab sab :code code :srl srl :tty tty
                   :cui cui :sui sui :saui saui :sdui sdui :scui scui
                   :lat lat :str str :ts ts)))

(defun find-uso-cui (cui &key (srl *current-srl*) (english-only nil) limit)
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrconso (aui sab code srl tty saui sdui scui lat str sui ts)
                                 srl cui cui :lrl srl :limit limit
                                 :filter (when english-only "LAT='ENG'"))
     (make-instance 'uso :aui aui :sab sab :code code :srl srl :tty tty
                    :cui cui :sui sui :saui saui :sdui sdui :scui scui
                    :lat lat :str str :ts ts)))

(defun find-uso-aui (aui &key (srl *current-srl*))
  (ensure-sui-integer aui)
  (collect-umlisp-query (mrconso (sab cui sui code srl tty saui sdui scui lat
                                      str ts) srl aui aui :lrl srl :single t)
    (make-instance 'uso :aui aui :cui cui :sab sab :code code :srl srl :tty tty
                   :sui sui :saui saui :sdui sdui :scui scui :lat lat
                   :str str :ts ts)))

(defun find-uhier-cui (cui &key (srl *current-srl*))
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrhier (aui cxn paui sab rela ptr hcd cvf)
                            srl cui cui :lrl ksrl)
    (make-instance 'uhier :cui cui :aui (ensure-integer aui)
                   :cxn (ensure-integer cxn)
                   :paui (ensure-integer paui)
                   :sab sab :rela rela :ptr ptr :hcd hcd :cvf cvf)))

(defun find-uhier-all (&key (srl *current-srl*))
  (collect-umlisp-query (mrhier (cui aui cxn paui sab rela ptr hcd cvf)
                            srl nil nil :lrl ksrl)
    (make-instance 'uhier :cui cui :aui (ensure-integer aui)
                   :cxn (ensure-integer cxn)
                   :paui (ensure-integer paui)
                   :sab sab :rela rela :ptr ptr :hcd hcd :cvf cvf)))

(defun find-usat-ui (cui &optional lui sui &key (srl *current-srl*))
  (ensure-cui-integer cui)
  (ensure-lui-integer lui)
  (ensure-sui-integer sui)
  (let ((ls "SELECT CODE,ATN,SAB,ATV FROM MRSAT WHERE "))
    (cond
      (sui (string-append ls "KCUISUI='"
                          (integer-string (make-cuisui cui sui) 14)
                          "'"))
      (lui (string-append ls "KCUILUI='"
                          (integer-string (make-cuilui cui lui) 14)
                          "' and sui='0'"))
      (t (string-append ls "cui='" (prefixed-fixnum-string cui nil 7)
                        "' and lui='0' and sui='0'")))
    (when srl
      (string-append ls " and KSRL<=" (prefixed-fixnum-string srl nil 3)))
    (loop for tuple in (mutex-sql-query ls) collect
          (destructuring-bind (code atn sab atv) tuple
            (make-instance 'usat :code code :atn atn :sab sab :atv atv)))))

(defun find-usty-tui (tui)
  "Find usty for tui"
  (ensure-tui-integer tui)
  (collect-umlisp-query (mrsty (sty) nil tui tui :single t)
    (make-instance 'usty :tui tui :sty sty)))

(defun find-usty-sty (sty)
  "Find usty for a sty"
  (collect-umlisp-query (mrsty (tui) nil sty sty :single t)
    (make-instance 'usty :tui (ensure-integer tui) :sty sty)))

(defun find-usty-all ()
  "Return list of usty's for all semantic types"
  (collect-umlisp-query (mrsty (tui) nil nil nil :distinct t)
    (find-usty-tui tui)))

(defun find-usab-all ()
  "Return all usab objects"
  (collect-umlisp-query (mrsab (vcui rcui vsab rsab son sf sver vstart vend imeta
                                  rmeta slc scc srl tfr cfr cxty ttyl atnl lat
                                  cenc curver sabin ssn scit) nil nil nil)
    (make-instance 'usab :vcui (ensure-integer vcui)
                   :rcui (ensure-integer rcui) :vsab vsab :rsab rsab :son son
                   :sf sf :sver sver :vstart vstart :vend vend :imeta imeta
                   :rmeta rmeta :slc slc :scc scc  :srl (ensure-integer srl)
                   :tfr (ensure-integer tfr) :cfr (ensure-integer cfr)
                   :cxty cxty :ttyl ttyl :atnl atnl :lat lat :cenc cenc
                   :curver curver :sabin sabin :ssn ssn :scit scit)))

(defun find-usab-by-key (key-name key &key current)
  "Find usab for a key"
  (collect-umlisp-query-eval ('mrsab '(vcui rcui vsab rsab son sf sver vstart
                                       vend imeta rmeta slc scc srl tfr cfr cxty
                                       ttyl atnl lat cenc curver sabin
                                       ssn scit)
                                     nil key-name key :single t
                                     :filter (when current "RMETA=''"))
     (make-instance 'usab :vcui (ensure-integer vcui)
                    :rcui (ensure-integer rcui) :vsab vsab :rsab rsab :son son
                    :sf sf :sver sver :vstart vstart :vend vend :imeta imeta
                    :rmeta rmeta :slc slc :scc scc :srl (ensure-integer srl)
                    :tfr (ensure-integer tfr) :cfr (ensure-integer cfr)
                    :cxty cxty :ttyl ttyl :atnl atnl :lat lat :cenc cenc
                    :curver curver :sabin sabin
                    :ssn ssn :scit scit)))

(defun find-usab-rsab (rsab &key (current t))
  "Find usab for rsab"
  (find-usab-by-key 'rsab rsab))

(defun find-usab-vsab (vsab &key (current t))
  "Find usab for vsab"
  (find-usab-by-key 'vsab vsab))

(defun find-cui-max ()
  (ensure-integer (caar (mutex-sql-query "select max(CUI) from MRCON"))))

(defun find-umap-cui (cui)
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrmap (mapsetsab mapsubsetid maprank fromid fromsid fromexpr
                                          fromtype fromrule fromres rel rela toid tosid
                                          toexpr totype torule tores maprule maptype
                                          mapatn mapatv cvf)
                               nil mapsetcui cui)
    (make-instance 'umap :mapsetcui cui :mapsetsab mapsetsab :mapsubsetid mapsubsetid
                   :maprank (ensure-integer maprank) :fromid fromid :fromsid fromsid
                   :fromexpr fromexpr :fromtype fromtype :fromrule fromrule :fromres fromres
                   :rel rel :rela rela :toid toid :tosid tosid :toexpr toexpr :totype totype
                   :torule torule :tores tores :maprule maprule :maptype maptype :mapatn mapatn
                   :mapatv mapatv :cvf cvf)))

(defun find-usmap-cui (cui)
  (ensure-cui-integer cui)
  (collect-umlisp-query (mrsmap (mapsetsab fromexpr fromtype rel rela toexpr totype cvf)
                               nil mapsetcui cui)
    (make-instance 'usmap :mapsetcui cui :mapsetsab mapsetsab
                   :fromexpr fromexpr :fromtype fromtype
                   :rel rel :rela rela :toexpr toexpr :totype totype
                   :cvf cvf)))

;;;; Cross table find functions

(defun find-ucon-tui (tui &key (srl *current-srl*))
  "Find list of ucon for tui"
  (ensure-tui-integer tui)
  (collect-umlisp-query (mrsty (cui) srl tui tui :lrl klrl :order (cui asc))
    (find-ucon-cui (ensure-integer cui) :srl srl)))

(defun mrconso-query-word-cui (word sab srl like)
  (format nil "SELECT DISTINCT c.cui FROM MRCONSO c,MRXW_ENG x WHERE x.WD~A'~A' AND x.cui=c.cui~A~A"
          (if like " LIKE " "=")
          (clsql-sys::sql-escape-quotes word)
          (etypecase sab
            (string
             (format nil " AND c.sab='~A'" (clsql-sys::sql-escape-quotes sab)))
            (cons
             (format nil " AND c.sab IN (~{'~A'~^,~})"
                     (mapcar 'clsql-sys::sql-escape-quotes sab)))
            (null
             ""))
          (if srl (format nil " AND KCUILRL <= ~A" srl) "")))

(defun mrconso-query-word-sui (word sab srl like)
  (format nil "SELECT DISTINCT c.sui FROM MRCONSO c,MRXW_ENG x WHERE x.WD~A'~A' AND x.sui=c.sui~A~A"
          (if like " LIKE " "=")
          (clsql-sys::sql-escape-quotes word)
          (etypecase sab
            (string
             (format nil " AND c.sab='~A'" (clsql-sys::sql-escape-quotes sab)))
            (cons
             (format nil " AND c.sab IN (~{'~A'~^,~})"
                     (mapcar 'clsql-sys::sql-escape-quotes sab)))
            (null
             ""))
          (if srl (format nil " AND KCUILRL <= ~A" srl) "")))

(defun find-uconso-word (word &key sab (srl *current-srl*) (like nil))
  "Return list of uconso that match word. Optionally, matching SAB. Optionally, use SQL's LIKE syntax"
  (cond
    (sab
     (let ((sui-query (mrconso-query-word-sui word sab srl like))
           (uconsos nil))
       (dolist (sui (remove-duplicates (sort (mapcar 'car (mutex-sql-query sui-query)) #'<)))
         (setq uconsos (nconc uconsos (find-uconso-sui sui :sab sab))))
       (remove-duplicates uconsos :key 'cui)))
    (t
     (collect-umlisp-query-eval ('mrxw_eng '(kcuisui) srl 'wd word :like like
                                           :lrl 'klrl :order '(kcuisui asc))
       (find-uconso-cuisui kcuisui :srl srl)))))

(defun find-ucon-word (word &key sab (srl *current-srl*) (like nil))
  "Return list of ucon that match word in matching SAB. Optionally, use SQL's LIKE syntax"
  (cond
    (sab
     (let ((query (mrconso-query-word-cui word sab srl like)))
       (loop for tuple in (mutex-sql-query query)
             collect (make-instance 'ucon :cui (first tuple)))))
    (t
     (collect-umlisp-query-eval ('mrxw_eng '(cui) srl 'wd word :like like :distinct t
                                           :lrl 'klrl :order '(cui asc))
       (find-ucon-cui cui :srl srl)))))

(defun find-ucon-normalized-word (word &key (srl *current-srl*) (like nil))
  "Return list of ucons that match word, optionally use SQL's LIKE syntax"
  (collect-umlisp-query-eval ('mrxnw_eng '(cui) srl 'nwd word :like like :distinct t
                                      :lrl 'klrl :order '(cui asc))
    (find-ucon-cui cui :srl srl)))

(defun find-cui-normalized-word (word &key (srl *current-srl*) (like nil))
  "Return list of cui that match word, optionally use SQL's LIKE syntax"
  (collect-umlisp-query-eval ('mrxnw_eng '(cui) srl 'nwd word :like like :distinct t
                                         :lrl 'klrl :order '(cui asc))
                             cui))

(defun find-lui-normalized-word (word &key (srl *current-srl*) (like nil))
  "Return list of cui that match word, optionally use SQL's LIKE syntax"
  (collect-umlisp-query-eval ('mrxnw_eng '(lui) srl 'nwd word :like like :distinct t
                                         :lrl 'klrl :order '(cui asc))
                             lui))

(defun find-sui-normalized-word (word &key (srl *current-srl*) (like nil))
  "Return list of cui that match word, optionally use SQL's LIKE syntax"
  (collect-umlisp-query-eval ('mrxnw_eng '(sui) srl 'nwd word :like like :distinct t
                                         :lrl 'klrl :order '(cui asc))
    sui))

(defun find-ustr-word (word &key sab (srl *current-srl*) (like nil))
  "Return list of ustr that match word in matching SAB. Optionally, use SQL's LIKE syntax"
  (cond
    (sab
     (let ((query (format nil "SELECT c.sui,c.cui,c.lui,c.str,c.lrl,c.stt,c.suppress,c.cuisui FROM MRCONSO c,MRXW_ENG x WHERE x.WD ~A '~A' AND x.cui=c.cui AND x.lui=c.lui AND x.sui=c.sui~A~A"
                          (if like "LIKE" "=")
                          (clsql-sys::sql-escape-quotes word)
                          (typecase sab
                            (string
                             (format nil " AND c.sab='~A'" (clsql-sys::sql-escape-quotes sab)))
                            (cons
                             (format nil " AND c.sab IN (~('~A'~^,~))"
                                     (mapcar 'clsql-sys::sql-escape-quotes sab)))
                            (null
                             ""))
                          (if srl (format nil " AND KCUILRL <= ~D" srl) ""))))
       (loop for tuple in (mutex-sql-query query)
          collect (destructuring-bind (sui cui lui str lrl stt suppress cuisui) tuple
                    (make-instance 'ustr :sui sui :cui cui :lui lui :str str :lrl lrl
                                   :stt stt :suppress suppress :cuisui cuisui)))))
    (t
     (collect-umlisp-query (mrxw_eng (kcuisui) srl wd word :lrl klrl
                                     :order (cui asc sui asc))
       (find-ustr-cuisui kcuisui :srl srl)))))

(defun find-ustr-normalized-word (word &key (srl *current-srl*))
  "Return list of ustrs that match word"
  (collect-umlisp-query (mrxnw_eng (kcuisui) srl nwd word :lrl klrl
                                 :order (cui asc sui asc))
    (find-ustr-cuisui kcuisui :srl srl)))

(defun find-uterm-word (word &key (srl *current-srl*))
  "Return list of uterms that match word"
  (collect-umlisp-query (mrxw_eng (cui lui) srl wd word :lrl klrl
                               :order (cui asc lui asc))
    (find-uterm-cuilui (ensure-integer cui) (ensure-integer lui) :srl srl)))

(defun find-uterm-normalized-word (word &key (srl *current-srl*))
  "Return list of uterms that match word"
  (collect-umlisp-query (mrxnw_eng (cui lui) srl nwd word :lrl klrl
                                 :order (cui asc lui asc))
    (find-uterm-cuilui (ensure-integer cui) (ensure-integer lui) :srl srl)))

(defun find-ucon-noneng-word (word &key (srl *current-srl*) (like nil))
  "Return list of ucons that match non-english word"
  (collect-umlisp-query-eval ('mrxw_noneng '(cui) srl 'wd word :like like
                                        :distinct t :lrl 'klrl :order '(cui asc))
    (find-ucon-cui cui :srl srl)))

(defun find-ustr-noneng-word (word &key (srl *current-srl*))
  "Return list of ustrs that match non-english word"
  (collect-umlisp-query (mrxw_noneng (kcuisui) srl wd word :lrl klrl
                                  :order (cui asc sui asc))
    (find-ustr-cuisui kcuisui :srl srl)))

;; Special tables

(defun find-usrl-all ()
  (collect-umlisp-query (usrl (sab srl) nil nil nil :order (sab asc))
    (make-instance 'usrl :sab sab :srl (ensure-integer srl))))

;;; Multiword lookup and score functions

(defun find-uobj-multiword (str obj-lookup-fun sort-fun key srl
                                only-exact-if-match limit &key extra-lookup-args)
  (let ((uobjs '()))
    (dolist (word (delimited-string-to-list str #\space))
      (setq uobjs (nconc uobjs
                         (kmrcl:flatten (apply obj-lookup-fun word :srl srl extra-lookup-args)))))
    (let ((sorted
           (funcall sort-fun str
                    (delete-duplicates uobjs :test #'= :key key))))
      (let ((len (length sorted)))
        (cond
         ((zerop len)
          (return-from find-uobj-multiword nil))
         ((and only-exact-if-match (multiword-match str (pfstr (first sorted))))
          (first sorted))
         (limit
          (if (and (plusp limit) (> len limit))
              (subseq sorted 0 limit)
            limit))
         (t
          sorted))))))

(defun find-ucon-multiword (str &key (srl *current-srl*)
                                     (only-exact-if-match t)
                                     limit
                                     sab)
  (find-uobj-multiword str #'find-ucon-word #'sort-score-pfstr-str
                       #'cui srl only-exact-if-match limit
                       :extra-lookup-args (list :sab sab)))

(defun find-uconso-multiword (str &key (srl *current-srl*)
                                     (only-exact-if-match t)
                                     limit
                                     sab)
  (find-uobj-multiword str #'find-uconso-word #'sort-score-pfstr-str
                       #'cui srl only-exact-if-match limit
                       :extra-lookup-args (list :sab sab)))

(defun find-uterm-multiword (str &key (srl *current-srl*)
                                      (only-exact-if-match t)
                                      limit)
  (find-uobj-multiword str #'find-uterm-word #'sort-score-pfstr-str
                       #'lui srl only-exact-if-match limit))

(defun find-ustr-multiword (str &key (srl *current-srl*)
                                     (only-exact-if-match t)
                                     limit
                                     sab)
  (find-uobj-multiword str #'find-ustr-word #'sort-score-ustr-str
                       #'sui srl only-exact-if-match limit
                       :extra-lookup-args (list :sab sab)))

(defun sort-score-pfstr-str (str uobjs)
  "Return list of sorted and scored ucons. Score by match of str to ucon-pfstr"
  (sort-score-umlsclass-str uobjs str #'pfstr))

(defun sort-score-ustr-str (str ustrs)
  "Return list of sorted and scored ucons. Score by match of str to ucon-pfstr"
  (sort-score-umlsclass-str ustrs str #'str))

(defun sort-score-umlsclass-str (objs str lookup-func)
  "Sort a list of objects based on scoring to a string"
  (let ((scored '()))
    (dolist (obj objs)
      (push (list obj (score-multiword-match str (funcall lookup-func obj)))
       scored))
    (mapcar #'car (sort scored #'> :key #'cadr))))


;;; LEX SQL functions

(defun find-lexterm-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrwd (wrd) nil eui eui :single t)
    (make-instance 'lexterm :eui eui :wrd wrd)))

(defun find-lexterm-word (wrd)
  (collect-umlisp-query (lrwd (eui) nil wrd wrd)
    (make-instance 'lexterm :eui (ensure-integer eui)
                   :wrd (copy-seq wrd))))

;; LEX SQL Read functions

(defun find-labr-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrabr (bas abr eui2 bas2) nil eui eui)
    (make-instance 'labr :eui eui :bas bas :abr abr :bas2 bas2
                   :eui2 (ensure-integer eui2))))

(defun find-labr-bas (bas)
  (collect-umlisp-query (labr (eui abr eui2 bas2) nil bas bas)
    (make-instance 'labr :eui (ensure-integer eui) :abr abr :bas2 bas2
                   :bas (copy-seq bas) :eui2 (ensure-integer eui2))))

(defun find-lagr-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lragr (str sca agr cit bas) nil eui eui)
    (make-instance 'lagr :eui eui :str str :sca sca :agr agr
                   :cit cit :bas bas)))

(defun find-lcmp-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrcmp (bas sca com) nil eui eui)
    (make-instance 'lcmp :eui eui :bas bas :sca sca :com com)))

(defun find-lmod-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrmod (bas sca psn_mod fea) nil eui eui)
    (make-instance 'lmod :eui eui :bas bas :sca sca :psnmod psn_mod :fea fea)))

(defun find-lnom-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrnom (bas sca eui2 bas2 sca2) nil eui eui)
    (make-instance 'lnom :eui eui :bas bas :sca sca :bas2 bas2 :sca2 sca2
                   :eui2 (ensure-integer eui2))))

(defun find-lprn-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrprn (bas num gnd cas pos qnt fea) nil eui eui)
    (make-instance 'lprn :eui eui :bas bas :num num :gnd gnd
                   :cas cas :pos pos :qnt qnt :fea fea)))

(defun find-lprp-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrprp (bas str sca fea) nil eui eui)
    (make-instance 'lprp :eui eui :bas bas :str str :sca sca :fea fea)))

(defun find-lspl-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrspl (spv bas) nil eui eui)
    (make-instance 'lspl :eui eui :spv spv :bas bas)))

(defun find-ltrm-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrtrm (bas gen) nil eui eui)
    (make-instance 'ltrm :eui eui :bas bas :gen gen)))

(defun find-ltyp-eui (eui)
  (ensure-eui-integer eui)
  (collect-umlisp-query (lrtyp (bas sca typ) nil eui eui)
    (make-instance 'ltyp :eui eui :bas bas :sca sca :typ typ)))

(defun find-lwd-wrd (wrd)
  (make-instance 'lwd :wrd wrd
                 :euilist (collect-umlisp-query (lrwd (eui) nil wrd wrd)
                                                (ensure-integer eui))))

;;; Semantic Network SQL access functions

(defun find-sdef-ui (ui)
  (collect-umlisp-query (srdef (rt sty_rl stn_rtn def ex un rh abr rin)
                            nil ui ui :single t)
    (make-instance 'sdef :rt rt :ui ui :styrl sty_rl :stnrtn stn_rtn
                   :def def :ex ex :un un :rh rh :abr abr :rin rin)))

(defun find-sstre1-ui (ui)
  (collect-umlisp-query (srstre1 (ui2 ui3) nil ui ui)
    (make-instance 'sstre1 :ui ui :ui2 (ensure-integer ui2)
                   :ui3 (ensure-integer ui3))))

(defun find-sstre1-ui2 (ui2)
  (collect-umlisp-query (srstre1 (ui ui3) nil ui2 ui2)
    (make-instance 'sstre1 :ui (ensure-integer ui) :ui2 ui2
                   :ui3 (ensure-integer ui3))))

(defun find-sstr-rl (rl)
  (collect-umlisp-query (srstre (sty_rl sty_rl2 ls) nil rl rl)
    (make-instance 'sstr :rl rl :styrl sty_rl :styrl2 sty_rl2 :ls ls)))

(defun find-sstre2-sty (sty)
  (collect-umlisp-query (srstre2 (rl sty2) nil sty sty)
    (make-instance 'sstre2 :sty (copy-seq sty) :rl rl :sty2 sty2)))

(defun find-sstr-styrl (styrl)
  (collect-umlisp-query (srstr (rl sty_rl2 ls) nil styrl styrl)
    (make-instance 'sstr :styrl styrl :rl rl :styrl2 sty_rl2 :ls ls)))


;;; **************************
;;; Local Classes
;;; **************************


(defun find-count-table (conn table srl count-variable srl-control)
  (with-sql-connection (conn)
  (cond
   ((stringp srl-control)
    (ensure-integer
     (caar (sql-query (format nil "select count(~a) from ~a where ~a <= ~d"
                              count-variable table srl-control srl)
                      conn))))
   ((null srl-control)
    (ensure-integer
     (caar (sql-query (format nil "select count(~a) from ~a"
                              count-variable table )
                      conn))))
   (t
    (error "Unknown srl-control")
    0))))

(defun insert-ustats (conn name count srl)
  (sql-execute (format nil "insert into USTATS (name,count,srl) values ('~a',~d,~d)"
                       name count (if srl srl 3))
               conn))

(defun find-ustats-all (&key (srl *current-srl*))
  (if srl
      (collect-umlisp-query (ustats (name count srl) nil srl srl
                                    :order (name asc))
                            (make-instance 'ustats :name name
                                           :hits (ensure-integer count)
                                           :srl (ensure-integer srl)))
    (collect-umlisp-query (ustats (name count srl) nil nil nil
                                  :order (name asc))
                          (make-instance 'ustats :name name
                                         :hits (ensure-integer count)
                                         :srl (ensure-integer srl)))))

(defun find-ustats-srl (srl)
  (collect-umlisp-query (ustats (name count) nil srl srl :order (name asc))
                           (make-instance 'ustats :name name :hits (ensure-integer count))))
