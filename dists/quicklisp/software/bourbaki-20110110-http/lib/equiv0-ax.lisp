;;;;;;;;; Axioms for an equivalence relation

(module "equiv0-ax"

  (meta (:comment . "Axioms for equivalence relations"))

  (symkind "WFF")
  (symkind "OBJ")
  (export [!!prop-ax])

  (prim wff "=" (obj ![a b]))
;    (meta (:html-sym . "eq")
;          (:html-fn . #'binary-infix)))

  ; Symmetry
  (ax "ax-sym" (obj ![x y])
      (ass [→ = x y = y x]))

  ; Transitivity
  (ax "ax-tr" (obj ![x y z])
      (ass [→ = x y → = y z = x z]))

  (def "df-neq" wff "≠" (obj ![x y]) ()
       [¬ = x y])
;    (meta (:html-sym . "ne")
;          (:html-fn . #'binary-infix)))

) ; End module "equiv-ax"
