(in-package :femlisp-ddo-test)

;;; (connect-to-mpi-workers)
#+(or)
(setf lfarm:*kernel*
      (lfarm:make-kernel
       '(
         ("norton" 20000)
         ("norton" 20001)
         ("norton" 20002)
         ("norton" 20003))))

;;; number of workers
(defparameter *n* (lfarm:kernel-worker-count))
(ddo (defparameter *n* (mpi-size)) *n*)

;; (ddo (load "packages.lisp"))
;;; (ddo (asdf:load-system :femlisp-ddo))  ; Problem: asdf is not working in parallel
(ddo (in-package :femlisp-ddo-test))

(ddo (mpi-rank))
(ddo (dbg-on :distribute))
(ddo (distributed-data))
(ddo (synchronize))

;;; local
(defparameter *my-mesh*
  (uniform-mesh-on-box-domain (n-cube-domain 2) #(2 2)))
(defparameter *my-refined-meshes*
  (make-array 0 :fill-pointer t))
(vector-push-extend *my-mesh* *my-refined-meshes*)

(time
 (loop repeat 6 do
   (let ((mesh (vector-last *my-refined-meshes*)))
     (vector-push-extend (refine mesh) *my-refined-meshes*))))

;;; remote

(ddo (defparameter *my-mesh* nil)
     (defparameter *my-refined-meshes* nil))

;; generate a distributed mesh
(ddo
  (let ((nr-workers *n*))
    (when (< (mpi-rank) nr-workers)
      (setq *my-mesh*
            (uniform-mesh-on-box-domain (n-cube-domain 2) #(2 2)))
      (femlisp-ddo::distribute-mesh *my-mesh* nr-workers))))

;; the following resetting leaves some data rooted in some memoization tables
(ddo (setq *my-mesh* nil)
     (gc :full t)
     (synchronize)
     (distributed-data))

;; doing a check on a quadrangle fills the memoization tables with new data
;; and the distributed objects are discarded by GC
(ddo
  (check (n-cube 2))
  (gc :full t)
  (synchronize)
  (distributed-data))

;; generate the distributed mesh again
(ddo
  (let ((nr-workers *n*))
    (when (< (mpi-rank) nr-workers)
      (setq *my-mesh*
            (uniform-mesh-on-box-domain (n-cube-domain 2) #(2 2)))
      (femlisp-ddo::distribute-mesh *my-mesh* nr-workers))))

;; and refine it
(time
 (ddo
   (setq *my-refined-meshes*
         (make-array 0 :fill-pointer t))
   (vector-push-extend *my-mesh* *my-refined-meshes*)
   (time
    (loop repeat 8 do
      (let ((mesh (vector-last *my-refined-meshes*)))
        (vector-push-extend (refine mesh) *my-refined-meshes*))))))

(ddo (nr-of-cells (vector-last *my-refined-meshes*)))

(ddo (room))

;;; flush everything
(ddo (setq *my-mesh* nil
           *my-refined-meshes* nil)
     (check (n-cube 2))  ; for flushing memoization tables
     (gc :full t)
     (synchronize)
     (distributed-data))
