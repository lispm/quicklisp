(in-package :doubly-linked-list)

(defstruct (dlist (:constructor %make-dlist (&key test)))
  "A doubly linked list that holds sequential nodes with links to the previous and next node."
  head
  tail
  test)

(defstruct node
  "A doubly linked list node with references to the previous and next node in the list."
  key
  value
  previous
  next)

(defmethod print-object ((object dlist) stream)
  (print-unreadable-object (object stream :type t)
    (format stream "~S" (dlist-elements object))))

(defmethod print-object ((object node) stream)
  (print-unreadable-object (object stream :type t)
    (format stream "(~S ~S)" (node-key object) (node-value object))))

(defun make-dlist (&key (test #'eql))
  "Create a new doubly linked list. TEST is a function used for comparing the keys of nodes."
  (%make-dlist :test test))

(defun find-node (dlist key &key from-end)
  "Find the first node in DLIST with the given KEY. If FROM-END is non-nil, then DLIST is searched
in reverse, from end to start."
  (when (dlist-head dlist)
    (let ((start-func (if from-end #'dlist-tail #'dlist-head))
          (hop-func (if from-end #'node-previous #'node-next)))
      (do ((node (funcall start-func dlist) (funcall hop-func node)))
          ((or (null node) (funcall (dlist-test dlist) (node-key node) key))
           node)))))

(defun %insert-node (dlist before after key value)
  (let ((node (make-node :key key :value value :previous before :next after)))
    (if before
        (setf (node-next before) node)
        (setf (dlist-head dlist) node))
    (if after
        (setf (node-previous after) node)
        (setf (dlist-tail dlist) node))
    node))

(defgeneric insert-node (where dlist key value &key &allow-other-keys)
  (:documentation "Insert a new node constructed from KEY and VALUE into DLIST. WHERE specifies
  where in DLIST to place the new node."))

(defmethod insert-node ((where (eql :head)) dlist key value &key)
  "Insert a new node constructed from KEY and VALUE into DLIST. The node is placed at the beginning
of DLIST."
  (%insert-node dlist nil (dlist-head dlist) key value))

(defmethod insert-node ((where (eql :tail)) dlist key value &key)
  "Insert a new node constructed from KEY and VALUE into DLIST. The node is placed at the end of
DLIST."
  (%insert-node dlist (dlist-tail dlist) nil key value))

(defmethod insert-node ((where (eql :before)) dlist key value &key from-end target-key)
  "Insert a new node constructed from KEY and VALUE into DLIST. The node is placed before the node
having a key of TARGET-KEY. If FROM-END is non-nil, then DLIST is searched in reverse, from end to
start."
  (when-let ((node (find-node dlist target-key :from-end from-end)))
    (%insert-node dlist (node-previous node) node key value)))

(defmethod insert-node ((where (eql :after)) dlist key value &key from-end target-key)
  "Insert a new node constructed from KEY and VALUE into DLIST. The node is placed after the node
having a key of TARGET-KEY. If FROM-END is non-nil, then DLIST is searched in reverse, from end to
start."
  (when-let ((node (find-node dlist target-key :from-end from-end)))
    (%insert-node dlist node (node-next node) key value)))

(defun remove-node (dlist key &key from-end)
  "Remove the first node found having KEY from DLIST. If FROM-END is non-nil, then DLIST is searched
in reverse, from end to start."
  (when-let ((node (find-node dlist key :from-end from-end)))
    (let ((before (node-previous node))
          (after (node-next node)))
      (if before
          (setf (node-next before) after)
          (setf (dlist-head dlist) after))
      (if after
          (setf (node-previous after) before)
          (setf (dlist-tail dlist) before)))
    node))

(defun remove-nodes (dlist &rest keys)
  "Remove all nodes from DLIST with the given KEYS."
  (dolist (key keys)
    (remove-node dlist key))
  dlist)

(defun dlist-elements (dlist)
  "Get an association list of node keys and values of DLIST."
  (loop :for node = (dlist-head dlist) :then (node-next node)
        :while node
        :collect (cons (node-key node) (node-value node))))
