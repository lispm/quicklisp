;;;  package.lisp --- package definition

;;;  Copyright (C) 2007, 2008, 2009 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: html sugar

#+cmu (ext:file-comment "$Module: package.lisp $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

(cl:in-package :cl-user)

(defpackage :html-sugar
  (:use :common-lisp :net.aserve :net.html.generator :sclf)
  (:export #:html-select
	   #:html-checkbox
	   #:html-radio
	   #:html-table #:tr #:thead
	   #:better-password-authoriser
	   #:crypt-password-authoriser #:authoriser-get-user-password
	   #:authoriser-deny-page #:authoriser-check-password
	   #:crypt
	   #:with-session-variables
	   #:with-query-values #:with-query-string-values
	   #:with-request-values
	   #:request-query-values
	   #:session-variable
	   #:defwa #:defclp #:defpage
	   #:*default-language*
	   #:req #:ent #:args #:body #:wa #:session
	   #:current-page
	   #:encode-url #:url #:anchor
	   #:query-value #:query-value-string
	   #:html-bookmarks
	   #:with-html-form #:*default-form-class* #:input
	   #:button #:*button-style*
	   #:redirect-to
	   #:table-row-number
	   #:write-html-sequence
	   #:compress-css-stream
	   #:compress-css))
