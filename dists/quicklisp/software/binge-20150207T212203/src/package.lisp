;;;  package.lisp --- list of packages

;;;  Copyright (C) 2004-2006, 2009 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-21908DC59A0D75C429A2951C213D0AF6.lisp,v 1.22 2004/04/16 10:41:51 wcp Exp $

;;;  This program is free software; you can redistribute it and/or
;;;  modify it under the terms of the GNU General Public License as
;;;  published by the Free Software Foundation; either version 2, or
;;;  (at your option) any later version.
;;;  This program is distributed in the hope that it will be useful,
;;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;  General Public License for more details.
;;;  You should have received a copy of the GNU General Public License
;;;  along with this program; see the file COPYING.  If not, write to
;;;  the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;;  Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-21908DC59A0D75C429A2951C213D0AF6.lisp,v 1.22 2004/04/16 10:41:51 wcp Exp $")

(in-package :cl-user)

(defpackage :binge-ss
  (:use :common-lisp
	#+sbcl :sb-gray
	#+cmu :ext
	#+lispworks :stream)
  (:export #:stateful-stream
	   #:open-stateful-stream
	   #:with-open-stateful-stream
	   #:get-position
	   #:file-offset
	   #:ss-bol
	   #:ss-line #:line
	   #:ss-column #:column
	   #:fpos-column
	   #:fpos-line
	   #:fpos-file))

(defpackage :binge-decl
  (:use :common-lisp)
  (:export #:make-c-reference
	   #:primitive-type-p
	   #:anonymous-declaration-p
	   ;; classes
	   #:c-object
	   #:c-declaration
	   #:c-function
	   #:c-function-declaration
	   #:c-user-type-ref
	   #:c-typedef-declaration
	   #:c-struct-ref
	   #:c-struct-declaration
	   #:c-union-ref
	   #:c-union-declaration
	   #:c-enum-ref
	   #:c-enum-declaration
	   #:c-variable-declaration
	   #:c-constant-declaration
	   #:c-bitfield
	   #:c-pointer
	   #:c-array
	   #:c-slot
	   #:c-argument
	   #:c-container
	   #:c-typed-object
	   #:c-named-object
	   #:c-typed-name
	   ;; slot accessors
	   #:c-name
	   #:c-type
	   #:c-slots
	   #:c-dimensions
	   #:c-arguments
	   #:c-type-ref
	   #:c-documentation
	   #:c-library
	   #:c-alternate-name
	   #:c-constants
	   #:c-ref-name
	   #:c-value
	   #:c-required
	   #:c-bits))

(defpackage :binge-grammar
  (:use :common-lisp
	:sclf :npg :binge-decl)
  (:export #:define-grammar
	   #:lookup-user-type
	   #:reset-types-pool
	   #:user-type
	   #:top-level-statement
	   #:identifier
	   #:integer
	   #:character
	   #:float
	   #:string))

(defpackage :binge-lexer
  (:use :common-lisp
	:sclf :npg :binge-grammar :binge-ss)
  (:export #:make-parsable-stream
	   #:with-parsable-stream
	   #:with-parsable-file
	   #:scan-file
	   #:skip-block))

(defpackage :binge-parser
  (:use :common-lisp
	:sclf :binge-decl)
  (:export #:lookup-user-type
	   #:extract
	   #:extract-from-headers
	   #:explain
	   #:*gcc-null-defines*
	   #:*c-preprocessor*
	   #:*c-compiler*))

(defpackage :binge-backend
  (:use :common-lisp
	:sclf :binge-decl)
  (:export #:write-prologue
	   #:backend
	   #:use-packages
	   #:write-epilogue
	   #:c-object-lisp-name
	   #:c-object->ffi-notation
	   #:backend-package
	   #:backend-define-package))

(defpackage :binge-compiler
  (:use :common-lisp
	:binge-decl :binge-parser :sclf :binge-backend)
  (:export #:compilation-unsupported-error
	   #:populate-namespace
	   #:c-namespace
	   #:complete-declarations
	   #:write-declarations
	   #:write-package
	   #:*backend*
	   #:*compress-types*
	   #:add-declaration
	   ;; accessors to hash table content
	   #:get-all-c-constants
	   #:get-all-c-typedefs
	   #:get-all-c-structs
	   #:get-all-c-functions
	   #:get-all-c-variables
	   ;; find methods
	   #:find-c-function
	   #:find-c-struct
	   #:find-c-union
	   #:find-c-enum
	   #:find-c-variable
	   #:find-c-constant
	   #:find-c-typedef))

(defpackage :binge
  (:use :common-lisp
	:binge-decl :binge-compiler :binge-parser :sclf)
  (:export #:make-ffi
	   #:extract
	   #:extract-from-headers
	   #:populate-namespace
	   #:*gcc-null-defines*
	   #:c-namespace
	   #:*c-preprocessor*
	   #:*backend*
	   #:*gcc-builtin-va_list*
	   #:*compress-types*))

