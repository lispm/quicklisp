;;; Copyright (c) Juha Arpiainen 2005-06

;;; This file is part of Bourbaki.

;;; Bourbaki is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.

;;; Bourbaki is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Bourbaki; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; -------

;;; *****
;;; Utilities for pattern matching
;;; *****

(in-package :bourbaki)

;;; Pattern trees:
;;;
;;; A pattern tree is a tree of nested hash-tables with symbols and integers
;;; as keys. The leaves of the tree typically contain symrefs.
;;; TODO: function for removing patterns?


;;; Match a wff against a pattern tree. While going down the tree, keep track
;;; of variable substitutions in a vector. At each level,
;;; - recurse by the next symbol in wff
;;; - for i = 1 ... (length vect), compare ith substitution against the next
;;;   subexpression in wff, recurse if equal
;;; - take next subexpression as (i+1)th variable and recurse
;;; At each leaf, apply the collected substitutions and return resulting symtrees
(defun match-tree (tree wff)
  (labels ((rec (tree expr vect)
             (if (null tree) nil
               (if (null expr)
                 ;; Reached a leaf: apply the symref function to substitutions we
                 ;; have collected, return resulting symtrees
                 (mapcar #'(lambda (x)
                             (cons (symref-target x)
                                   (nreverse (funcall (symref-fn x) vect))))
                         (pattern-tree-ref tree))
                 (nconc ;; Match against the exact next symbol
                        (multiple-value-bind (sym tail) (flatten-1 expr)
                          (aif (gethash sym (pattern-tree-syms tree))
                            (rec it tail vect)))
                        ;; Match against previous variables
                        (loop for i from 0 below (length vect)
                              nconc (aif (gethash i (pattern-tree-syms tree))
                                      (if (wff-equal (car expr) (aref vect i))
                                        (rec it (cdr expr) vect))))
                        ;; Match against next higher variable
                        (prog1
                          (rec (gethash (vector-push-extend (car expr) vect)
                                        (pattern-tree-syms tree))
                               (cdr expr) vect)
                          (vector-pop vect)))))))
    (rec tree wff (load-time-value (make-array 5 :adjustable t :fill-pointer 0)))))

;; Simpler version of match-tree in the case each symbol of wff must match exactly
;; (no substitutions are collected).
(defun in-tree (tree wff)
  (labels ((rec (tree expr)
             (if (null tree) nil
               (if (null expr)
                 (pattern-tree-ref tree)
                 (multiple-value-bind (sym tail) (flatten-1 expr)
                   (aif (gethash sym (pattern-tree-syms tree))
                     (rec it tail)))))))
    (rec tree wff)))

;;; Inserting patterns into a tree:
;;; To make matching simpler, permute variable indices
;;; so that their first occurences are in increasing order

;; Permute variables in a wff pattern so that they occur
;; in increasing order; Example:
;; (permute-vars [→ ∧ 1 3 ∨ 2 0] ref)
;; => [→ ∧ 0 1 ∨ 2 3]
(defun permute-vars (pat ref)
  (let ((expr (flatten pat))
        (vals nil)
        (nvals 0))
    ;; Collect the integers in expr in the reverse order of
    ;; first occurence
    (dolist (sym expr)
      (if (integerp sym)
        (setf vals (adjoin sym vals))))
    (setf nvals (length vals))
    ;; Construct the needed permutation and its inverse
    (let* ((smap (pairlis (nreverse vals)
                          (int-list nvals)))
           (r-smap (sort smap #'> :key #'car)))
      (values (nsublis smap expr) (permute-ref ref r-smap)))))

;; Apply the inverse permutation to a vector and forward
;; result to original symref function
(defun permute-ref (ref smap)
  (make-symref
    :args-needed (length smap)
    :target (symref-target ref)
    :fn #'(lambda (vect)
            (apply (symref-fn ref)
                   (loop for i in smap
                         collect (aref vect (cdr i)))))))

;; Insert a wff into a pattern tree, creating subtrees if necessary
(defun insert-pat (tree ref)
  (let ((val (r-int-list (symref-args-needed ref))))
    ;; Replace the variables of ref by integers
    (let* ((thref (apply-ref ref val))
           (th-smap (smap (car thref) (cdr thref))))
      (labels ((rec (tree expr ref)
                 (if (null expr)
                   (push ref (pattern-tree-ref tree))
                   (let ((branch (gethash (car expr) (pattern-tree-syms tree))))
                     ;; Create a new subtree if necessary
                     (unless branch
                       (setf branch (make-pattern-tree))
                       (setf (gethash (car expr) (pattern-tree-syms tree)) branch))
                     (rec branch (cdr expr) ref)))))
        (dolist (pat (context-assert (car thref)))
          (multiple-value-call #'rec tree (permute-vars (replace-vars pat th-smap) ref)))))))

;; Simple version of insert-pat when the pattern has no variables
(defun insert-wff (tree wff &optional (val t))
  (labels ((rec (tree expr)
             (if (null expr)
               (setf (pattern-tree-ref tree) val)
               (let ((branch (gethash (car expr) (pattern-tree-syms tree))))
                 (unless branch
                   (setf branch (make-pattern-tree))
                   (setf (gethash (car expr) (pattern-tree-syms tree)) branch))
                 (rec branch (cdr expr))))))
    (rec tree (flatten wff))))

;;; TODO: for RPN proofs
;(defun unify-line (pat wff smap)
;  (declare (ignore pat))
;  (declare (ignore wff))
;  (declare (ignore smap))
;  )

;;; TODO: patterns with integers as variables,
;;; for import/export statements and the like
;(defun num-pat-vars (tree)
;  (declare (ignore tree))
  ;; TODO: highest numebered integer in tree
;  nil)

;(defun numeric-smap (vars)
;  (loop for i from 1 to (length vars)
;        for v in vars
;        collect (cons i v)))

;(defun numeric-pat-fn (tree)
;  #'(lambda (&rest vars)
;      (let ((smap (numeric-smap vars)))
;        (replace-vars tree smap))))


;;; if-match: match a symtree against a pattern, execute body
;;; with the pattern variables bound to corresponding subexpressions
;;; Some trickery is required because the pattern itself can't be parsed
;;; until runtime. Here we compute the matcher function the first time
;;; the if-match is executed and store the closure as the value of a
;;; gensym special variable. This may not be completely portable.

;;; Treat symbols whose name begins with '?' as pattern variables for if-match
(defun pat-var-p (expr)
  (and expr (symbolp expr)
       (char= (aref (symbol-name expr) 0) #\?)))

(defun pat-vars-in (expr)
  (if (atom expr)
    (if (pat-var-p expr) (list expr))
    (union (pat-vars-in (car expr))
           (pat-vars-in (cdr expr)))))

;; Set the global value of a gensym to expr when executed for the first time
(defmacro let-once ((var expr) &body body)
  (let ((temp (gensym)))
    `(let ((,var (if (boundp ',temp)
                   (symbol-value ',temp)
                   (setf (symbol-value ',temp) ,expr))))
       ,@body)))

(defmacro if-match (pat expr then &optional else)
  (with-gensyms (matcher binds)
    ;; Collect the pattern variables in pat. We cheat by collecting
    ;; the variables before pat is evaluated. Thus all the variables
    ;; should occur quoted in the pat form
    (let ((vars (pat-vars-in pat)))
      `(let-once (,matcher (matcher-fn ,pat))
         (let ((,binds (mapcar #'(lambda (v) (cons v nil)) ',vars)))
           (if (funcall ,matcher ,expr ,binds)
             (let (,@(loop for i from 0
                           for v in vars
                           collect `(,v (cdr (nth ,i ,binds)))))
               ,then)
             ,else))))))

(defmacro match-case (expr &body cases)
  (labels ((rec (cases)
             (if cases
               (if (eq (caar cases) t)
                 `(progn ,@(cdar cases))
                 `(if-match ,(caar cases) ,expr
                    (progn ,@(cdar cases))
                    ,(rec (cdr cases))))
               nil)))
    (rec cases)))

(defun matcher-fn (pat)
  (cond
    ((null pat) (load-time-value #'(lambda (exp binds)
                                     (declare (ignore binds))
                                     (null exp))))
    ((consp pat) (let ((am (matcher-fn (car pat)))
                       (dm (matcher-fn (cdr pat))))
                   #'(lambda (exp binds)
                       (if (funcall am (car exp) binds)
                         (funcall dm (cdr exp) binds)
                         nil))))
    ((symbolp pat) #'(lambda (exp binds)
                       (aif (cdr (assoc pat binds))
                         (wff-equal it exp)
                         (progn
                           (setf (cdr (assoc pat binds)) exp)
                           t))))
    (t #'(lambda (exp binds)
           (declare (ignore binds))
           (eq pat exp)))))
