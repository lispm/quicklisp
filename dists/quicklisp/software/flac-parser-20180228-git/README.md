# flac-parser

A parser for FLAC audio file metadata.

## Overview

This will parse files encoded in the binary FLAC audio format. This will present
to the user an object representing the concrete syntax tree of the format.
Currently this does not parse audio frames - only the various different metadata
blocks.

## Install

``` lisp
(ql:quickload :flac-parser)
```

## Usage

The following will return a `FLAC-FILE` object with the information about the
FLAC file:

```lisp
(open-flac "/path/to/file.flac")
```

## License

Copyright © 2016 Michael Fiano <mail@michaelfiano.com>.

Licensed under the MIT License.

A copy of the license is available [here](LICENSE).
