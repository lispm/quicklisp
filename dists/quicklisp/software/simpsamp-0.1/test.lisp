(in-package #:simpsamp)

(defun test (&rest test-core-args &key (stream *standard-output*) &allow-other-keys)
  (format stream "~&Testing output methods...")
  (test-output)
  (format stream "passed.~&")
  (format stream "~&Testing core algorithms...~&")
  (apply #'test-core test-core-args)
  (format stream "~&NOTE: Interpretation of core algorithm test ~
                  results is your responsibility.~&")
  (values))
