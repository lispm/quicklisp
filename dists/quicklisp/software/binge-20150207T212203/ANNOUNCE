BINGE is a Bovine foreign function INterface GEnerator for Common Lisp
I have been working at as part of a project to write an interface to a
considerably large collection of C functions.

So far BINGE has proven to be able to parse various FreeBSD and Linux
header files and generate FFI for some well known libraries such as
curses, zlib, and Berkeley DB.  These are included among the examples.

While not a complete solution in the loathsome task of making a FFI,
it cuts substantially on the development time, saving the programmer
from the most tedious work.  That is, it is able to automatically
generate the FFI to C functions and global variables and extract
semiautomatically most of the symbolic constants (#defines).

Although BINGE should be still considered in a draft status, it is
already rather useful in its current shape.  It has been successfully
compiled on SBCL, CMUCL and LispWorks, so the code stands good chances
to be portable.

One of the strengths of BINGE is its multi-backend design that allows
cross compilation of FFIs and makes the addition of new backends a
matter of few hours.  Currently backends for SBCL, CMUCL, LispWorks,
UFFI and CLISP are provided.

Little or no documentation is provided but the inclusion of some
examples should make its use rather straightforward.

The UFFI and CLISP backends are courtesy of Aurelio Bignoli, who has
also helped in the testing and contributed in some design decisions.

The source code of the latest version of BINGE can be found here

    http://www.pelissero.de/software/index.html

where you can also find NPG, which is a prerequisite.
