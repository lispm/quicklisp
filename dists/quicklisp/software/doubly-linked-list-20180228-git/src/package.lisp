(in-package :cl-user)

(defpackage #:doubly-linked-list
  (:use #:cl)
  (:import-from #:alexandria
                #:when-let)
  (:export #:dlist-head
           #:dlist-tail
           #:make-dlist
           #:node-key
           #:node-value
           #:find-node
           #:insert-node
           #:remove-node
           #:remove-nodes
           #:dlist-elements))
