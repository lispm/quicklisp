;;; ************
;;; Metatheorems
;;; ************

;; hack -- to avoid loading this file multiple times
(module "pred-meta"
;  (import [!!pred-ax])
  (import [!!pred])
;  (export [!!prop-meta])

;; Inference with generalization
;; if thr is of the form "∀ x φ → ψ"
;; return φ => ψ
  (extend-metath "infer" (ref)
    (let* ((pfl (parse-subproof ref))
           (assert (car (subproof-assert pfl))))
      (if-match [→ ∀ ?x ?phi ?psi] assert
        (if (provenp ?phi)
          (linear-subproof (?phi) (?psi)
            [ax-gen ,?x ,?phi]
            pfl
            [ax-mp [∀ ,?x ,?phi] ,?psi])))))
  ) ;; end module "pred-meta"
