%module "sane-lowlevel"

%insert("lisphead") %{
(in-package :sane-lowlevel)

;; SWIG doesn't realise that this #define list is actually a bitfield,
;; so give it some help!
(defbitfield capability-flags
  :SANE_CAP_SOFT_SELECT
  :SANE_CAP_HARD_SELECT
  :SANE_CAP_SOFT_DETECT
  :SANE_CAP_EMULATED
  :SANE_CAP_AUTOMATIC
  :SANE_CAP_INACTIVE
  :SANE_CAP_ADVANCED
  :SANE_CAP_ALWAYS_SETTABLE)
%}

%include "/usr/include/sane/sane.h"