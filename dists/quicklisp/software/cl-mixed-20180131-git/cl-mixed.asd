#|
 This file is a part of cl-mixed
 (c) 2017 Shirakumo http://tymoon.eu (shinmera@tymoon.eu)
 Author: Nicolas Hafner <shinmera@tymoon.eu>
|#

(asdf:defsystem cl-mixed
  :version "2.0.0"
  :license "Artistic"
  :author "Nicolas Hafner <shinmera@tymoon.eu>"
  :maintainer "Nicolas Hafner <shinmera@tymoon.eu>"
  :description "Bindings to libmixed, a sound mixing and processing library."
  :homepage "https://github.com/Shirakumo/cl-mixed"
  :serial T
  :components ((:file "package")
               (:file "low-level")
               (:file "toolkit")
               (:file "c-object")
               (:file "buffer")
               (:file "packed-audio")
               (:file "segment")
               (:file "mixer")
               (:file "segment-sequence")
               (:module "segments"
                :components ((:file "basic-mixer")
                             (:file "delay")
                             (:file "fader")
                             (:file "frequency-pass")
                             (:file "generator")
                             (:file "ladspa")
                             (:file "noise")
                             (:file "packer")
                             (:file "pitch")
                             (:file "repeat")
                             (:file "space-mixer")
                             (:file "unpacker")
                             (:file "virtual")
                             (:file "volume-control")))
               (:file "documentation"))
  :depends-on (:alexandria
               :cffi
               :trivial-features
               :trivial-garbage
               :documentation-utils))
