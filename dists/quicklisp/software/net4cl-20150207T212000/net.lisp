;;;  net.lisp --- network primitives

;;;  Copyright (C) 2005, 2006, 2007, 2008, 2009, 2010 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: net4cl

#+cmu (ext:file-comment "$Module: net.lisp $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

(in-package :cl-user)

(defpackage :net4cl
  (:nicknames :net)
  (:use :common-lisp :sclf)
  (:export #:open-socket
	   #:with-open-socket
	   #:make-server-socket
	   #:with-server-socket
	   #:find-service
	   #:do-connections
	   #:close-socket
	   #:get-host-name
	   #:get-address-by-hostname
	   #:get-hostname-by-address
	   #:*services-file*
	   #:socket-stream
	   #:socket-real-socket
	   #:accept
	   #:parse-ip-address))

(in-package :net4cl)

#+(and clisp (not rawsock))
(warn "This CLISP has no support for raw sockets (rawsock package), thus you won't be able to use Unix-domain sockets.")

(defconst +whitespace-chars+ '(#\space #\tab #\return #\newline #\page))

(defvar *services-file* #P"/etc/services")

;; Apparently the CMUCL folks decided that the nice effort Unix makes
;; to unify the interface between different socket types, wasn't worth
;; considering.  The following class hierarchy tries to mend this
;; damage.

(defclass socket ()
  ((real-socket :reader socket-real-socket
		:initarg :socket
		:documentation
		"System specific socket object.")
   (status :reader socket-status
	   :type (member :open :closed)
	   :initform :open
	   :initarg :status
	   :documentation
	   "Current status of socket.  When newly created
it's :OPEN.  It becomes :CLOSED after a CLOSE-SOCKET.")))

(defclass connected-socket (socket)
  ((stream :reader socket-stream
	   :initarg :stream
	   :documentation "System specific stream object of socket.")))

(defclass server-socket (socket)
  ())

(defclass unix-server-socket (server-socket)
  ((pathname :accessor socket-pathname
	     :initarg :pathname
	     :documentation "Pathname of the Unix domain slocket.")))

(defclass inet-server-socket (server-socket)
  ())

(defun close-real-socket (socket)
  "Close a system specific socket.  Not the class defined in this
module."
  #+cmu (extensions:close-socket socket)
  #+sbcl (sb-bsd-sockets:socket-close socket)
  #+clisp (rawsock:sock-close socket)
  #-(or cmu sbcl clisp) (error "Don't know how to close a Lisp socket."))

(defun close-real-server-socket (socket)
  #-clisp (close-real-socket socket)
  #+clisp (socket:socket-server-close socket))

(defgeneric close-socket (socket &key abort))

(defun find-service (service-name &optional (protocol "tcp"))
  "Return the port number associated to a particular service.
This is looked up in /etc/services."
  (with-open-file (stream *services-file*)
    (loop
       for line = (read-line stream nil)
       while line
       do (let ((good-stuff (aif (position #\# line)
				 (subseq line 0 it)
				 line)))
	    (awhen (split-at +whitespace-chars+ good-stuff)
	      (destructuring-bind (name port &rest aliases) it
		(when (or (string= service-name name)
			  (member service-name aliases :test #'string=))
		  (destructuring-bind (port-num proto) (split-string-at-char port #\/)
		    (when (string-equal proto protocol)
		      (return (values (read-from-string port-num))))))))))))

#+(and clisp rawsock)
(progn
  (declaim (inline make-sockaddr))
  (defun make-sockaddr (pathname)
    (rawsock:make-sockaddr :unix
			   (ext:convert-string-to-bytes (namestring pathname) 
							#+unicode charset:ascii
							#-unicode :default))))

#+clisp 
(progn
  (declaim (inline translate-cmu-buffering))
  (defun translate-cmu-buffering (buffering)
    (not (eq buffering :none))))

(defun open-local-socket (pathname buffering element-type)
  (declare (type pathname pathname))
  #+(and clisp rawsock)
  (be socket (rawsock:open-unix-socket pathname)
    (make-instance 'connected-socket
		   :socket socket
		   :stream (ext:make-stream socket :element-type element-type
					    :buffered (translate-cmu-buffering buffering))))
  #+(and clisp (not rawsock))
  (declare (ignore pathname buffering element-type))
  #+(and clisp (not rawsock))
  (error "This CLISP doesn't provide Unix sockets.")
  #+cmu
  (be socket (ext:connect-to-unix-socket (namestring pathname))
    (make-instance 'connected-socket
		   :socket socket
		   :stream (sys:make-fd-stream socket :input t :output t
					       :buffering (or buffering :full)
					       :element-type element-type)))
  #+sbcl
  (be socket (make-instance 'sb-bsd-sockets:local-socket :type :stream :protocol :tcp)
    (sb-bsd-sockets:socket-connect socket pathname)
    (make-instance 'connected-socket
		   :socket socket
		   :stream (sb-bsd-sockets:socket-make-stream socket :input t :output t
							      :buffering buffering
							      :element-type element-type))))

(defun get-hostname-by-address (ip-addr)
  "Return the hostname associated to IP-ADDR, or NIL if unknown.
IP-ADDR is supposed to be a vector of type (VECTOR (UNSIGNED-BYTE 8) 4)."
  (ignore-errors
    #+sbcl (sb-bsd-sockets:host-ent-name
	    (sb-bsd-sockets:get-host-by-address ip-addr))
    #+cmu (ext:host-entry-name (ext:lookup-host-entry ip-addr))))

(defun get-address-by-hostname (host)
  "Return the IP address that belongs to HOST or NIL if unknown.
Return a vector of type (VECTOR (UNSIGNED-BYTE 8) 4)."
  (ignore-errors
    #+sbcl (sb-bsd-sockets:host-ent-address
	    (sb-bsd-sockets:get-host-by-name host))
    #+cmu (car (ext:host-entry-addr-list (ext:lookup-host-entry host)))))

(defun parse-ip-address (string)
  (coerce (mapcar #'parse-integer (split-string-at-char string #\.))
	  '(vector (unsigned-byte 8))))

(defun open-inet-socket (host port buffering element-type)
  "Open a TCP socket to HOST at PORT."
  #+clisp
  (be stream (socket:socket-connect (if (integerp port)
					port
					(find-service port))
				    host :buffered (translate-cmu-buffering buffering)
				    :element-type element-type)
    (make-instance 'connected-socket
		   ;; here we use only one fd of the two returned
		   :socket (socket:stream-handles stream)
		   :stream stream))
  #+cmu
  (be socket (ext:connect-to-inet-socket host (if (integerp port)
						  port
						  (find-service port)))
    (make-instance 'connected-socket
		   :socket socket
		   :stream (sys:make-fd-stream socket :input t :output t
					       :buffering (or buffering :full)
					       :element-type element-type)))
  #+sbcl
  (be socket (make-instance 'sb-bsd-sockets:inet-socket :type :stream :protocol :tcp)
    (sb-bsd-sockets:socket-connect socket (sb-bsd-sockets:host-ent-address (sb-bsd-sockets:get-host-by-name host))
				   (if (integerp port)
				       port
				       (find-service port)))
    (make-instance 'connected-socket
		   :socket socket
		   :stream (sb-bsd-sockets:socket-make-stream socket :input t :output t
							      :buffering (or buffering :full)
							      :element-type element-type))))

(defun open-socket (&key pathname host port buffering (element-type '(unsigned-byte 8)))
  "Return a connected socket.  Either specify PATHNAME (for a
Unix domain socket), or HOST and PORT, for an internet socket.
Buffering is shamefully Lisp implementation dependent."
  (cond (pathname
	 (open-local-socket pathname buffering element-type))
	((and host port)
	 (open-inet-socket host port buffering element-type))
	(t
	 (error "Must specify HOST+PORT or PATHNAME when calling this function."))))

(defun make-server-socket (description)
  "Make a server socket based on DESCRIPTION.  If it's a string
it's a service name.  If it's a number must be a port number.  If
it's a pathname it's the Unix domain (local) socket."
  (typecase description
    (pathname		    ; it's a pathname for a Unix domain socket
     #+cmu (make-instance 'unix-server-socket
			  :socket (ext:create-unix-listener (namestring description))
			  :pathname description)
     #+sbcl (let ((socket (make-instance 'sb-bsd-sockets:local-socket :type :stream)))
	      (sb-bsd-sockets:socket-bind socket (namestring description))
	      (sb-bsd-sockets:socket-listen socket 5)
	      (make-instance 'unix-server-socket
			     :socket socket
			     :pathname description))
     #+(and clisp rawsock)
     (be socket (rawsock:socket :unix :stream 0)
       (rawsock:bind socket (make-sockaddr description))
       (rawsock:sock-listen socket)
       (make-instance 'unix-server-socket
		      :socket socket
		      :pathname description))
     #+(and clisp (not rawsock))
     (error "This CLISP doesn't provide Unix sockets."))
    (string				; it's a service name
     #+sbcl (be socket (make-instance 'sb-bsd-sockets:inet-socket :type :stream :protocol :tcp)
	      (sb-bsd-sockets:socket-bind socket (vector 0 0 0 0) (find-service description))
	      (sb-bsd-sockets:socket-listen socket 5)
	      (make-instance 'inet-server-socket :socket socket))
     #+cmu (make-instance 'inet-server-socket
			  :socket (ext:create-inet-listener (find-service description)))
     #+clisp (make-instance 'inet-server-socket
			    :socket (socket:socket-server (find-service description))))
    (integer				; it's a port number
      #+sbcl (be socket (make-instance 'sb-bsd-sockets:inet-socket :type :stream :protocol :tcp)
	       (sb-bsd-sockets:socket-bind socket (vector 0 0 0 0) description)
	       (sb-bsd-sockets:socket-listen socket 5)
	       (make-instance 'inet-server-socket :socket socket))
      #+cmu (make-instance 'inet-server-socket
			   :socket (ext:create-inet-listener description))
      #+clisp (make-instance 'inet-server-socket
			     :socket (socket:socket-server description)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod close-socket ((socket socket) &key abort)
  (declare (ignore abort))
  (with-slots (status real-socket) socket
    (when (eq status :open)
      (close-real-socket real-socket)
      (setf real-socket nil
	    status :closed))))

#+clisp
(defmethod close-socket ((socket inet-server-socket) &key abort)
  (declare (ignore abort))
  (with-slots (status real-socket) socket
    (when (eq status :open)
      (close-real-server-socket real-socket)
      (setf real-socket nil
	    status :closed))))

(defmethod close-socket ((socket unix-server-socket) &key abort)
  (declare (ignore abort))
  (call-next-method)
  ;; this shouldn't be necessary
  (when (probe-file (socket-pathname socket))
    (delete-file (socket-pathname socket))))

(defmethod close-socket ((socket connected-socket) &key abort)
  (when (eq :open (socket-status socket))
    (call-next-method)
    (close (socket-stream socket) :abort abort)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro with-server-socket ((socket port) &body body)
  "Execute BODY in a context where client SOCKET is bound to a
server socket listening to PORT.  Make sure to close the socket
upon exit of the code block."
  `(let ((,socket (make-server-socket ,port)))
     (unwind-protect (progn ,@body)
       (ignore-errors
	 (close-socket ,socket :abort t)))))

(defmacro with-open-socket ((socket &rest args) &body body)
  "Execute BODY in a context where client SOCKET is bound to a
connected socket made by calling OPEN-SOCKET with ARGS.  Make
sure to close the socket upon exit of the code block."
  `(let ((,socket (open-socket ,@args)))
     (unwind-protect (progn ,@body)
       ;; if we got the door slammed on our face while writing to the
       ;; MTA, don't panic if the close can't flush it's buffer
       (ignore-errors
	 (close-socket ,socket)))))

(defgeneric accept (server-socket &key element-type buffering timeout))

;; It should be noticed that the current implementation of timeout in
;; ACCEPT makes use of UNIX-SELECT, which AFAICT doesn't play nicely
;; with the collaborative multiprocessing of CMUCL -wcp15/6/05.

#+cmu
(flet ((do-accept (accepter server-socket buffering element-type)
	 (be socket (funcall accepter (socket-real-socket server-socket))
	   (make-instance 'connected-socket
			  :socket socket
			  :stream (sys:make-fd-stream socket :input t :output t
						      :buffering (or buffering :full)
						      :element-type element-type))))
       (wait-connection (server-socket timeout)
	 (be descriptor (socket-real-socket server-socket)
	   (multiple-value-bind (result readable)
	       (unix:unix-select (1+ descriptor) (ash 1 descriptor) 0 0 timeout)
	     (declare (ignore readable))
	     (when (not result)
	       (error "unable to select on socket ~S" server-socket))
	     ;; if result = 0, we timed out
	     (not (zerop result))))))

  (defmethod accept ((server-socket inet-server-socket) &key element-type buffering timeout)
    (when (or (not timeout)
	      (wait-connection server-socket timeout))
      (do-accept #'ext:accept-tcp-connection server-socket buffering element-type)))

  (defmethod accept ((server-socket unix-server-socket) &key element-type buffering timeout)
    (when (or (not timeout)
	      (wait-connection server-socket timeout))
      (do-accept #'ext:accept-unix-connection server-socket buffering element-type))))

#+sbcl
(defmethod accept ((server-socket server-socket) &key element-type buffering timeout)
  (flet ((do-accept (server-socket buffering element-type)
	   (be socket (sb-bsd-sockets:socket-accept (socket-real-socket server-socket))
	     (make-instance 'connected-socket
			    :socket socket
			    :stream (sb-bsd-sockets:socket-make-stream socket :input t :output t
								       :buffering buffering
								       :element-type element-type))))
	 (wait-connection (server-socket)
	   (be descriptor (sb-bsd-sockets:socket-file-descriptor (socket-real-socket server-socket))
	     (multiple-value-bind (result readable)
		 (sb-unix:unix-select (1+ descriptor) (ash 1 descriptor) 0 0 timeout)
	       (declare (ignore readable))
	       (unless result
		 (error "unable to select on socket ~S" server-socket))
	       ;; if result = 0, we timed out
	       (not (zerop result))))))
    (when (or (not timeout)
	      (wait-connection server-socket))
      (do-accept server-socket buffering element-type))))

#+clisp
(defmethod accept ((server-socket inet-server-socket) &key element-type buffering timeout)
  (be stream (socket:socket-accept server-socket
				   :element-type element-type
				   :buffered (translate-cmu-buffering buffering)
				   :timeout timeout)
    (make-instance 'connected-socket
		   :socket (socket:stream-handles stream)
		   :stream stream)))

#+(and clisp rawsock)
(defmethod accept ((server-socket unix-server-socket) &key element-type buffering timeout)
  (be socket (socket-real-socket server-socket)
    (when (eq (socket:socket-status (cons socket :input) (or timeout 0)) :input)
      (be* new-socket (rawsock:accept  socket (make-sockaddr (socket-pathname server-socket)))
	   socket-stream (ext:make-stream new-socket :element-type element-type :buffered (translate-cmu-buffering buffering))
	(make-instance 'connected-socket
	               :socket new-socket
	               :stream socket-stream)))))

(defmacro do-connections ((socket socket-description &key (buffering :none) (element-type '(quote (unsigned-byte 8))) timeout keep-open) &body body)
  (with-gensyms (server-socket)
    `(with-server-socket (,server-socket ,socket-description)
       (loop
	  (be ,socket (accept ,server-socket :element-type ,element-type :timeout ,timeout :buffering ,buffering)
	    (unless ,socket
	      (return))
	    (unwind-protect
		 (progn ,@body)
	      ,@ (case keep-open
		      ((t) '())
		      ((nil) `((close-socket ,socket)))
		      (t `((unless ,keep-open
			     (close-socket ,socket)))))))))))

(defun get-host-name ()
  #+cmu (unix:unix-gethostname)
  #+sbcl (sb-unix:unix-gethostname)
  #+clisp (posix:uname-nodename (posix:uname))
  #-(or cmu sbcl clisp) (error "Unsupported Lisp system."))
