;;; declare a symbol type "well formed formula"
(symkind "WFF")

(module "prop-ax"

  (meta (:comment . "Axioms and definitions for propositional calculus.")
        (:description . "We take implication and negation as primitive.
The biconditional must also be taken primitive (it cannot be used to
define itself)."))

  ;;; ************
  ;;; Primitive symbols
  ;;; ************

  ;; declare a binary primitive symbol (implication)
  (prim wff "→" (wff ![φ ψ])
    (meta ;(:html-sym . "to")
          ;(:html-fn . #'binary-infix)
          (:comment . "Logical implication.")
          (:description . "If phi and psi are wff's, so is (phi -> psi)
or \"phi implies psi.\" The resulting wff is (interpreted as) false when
phi is true and psi is false; it is true otherwise. (Think of the truth
table for an OR gate with input phi connected through an inverter.) The
left-hand wff is called the anteedent, and the right-hand wff is called
the consequent. In the case of (phi -> (psi -> chi)), the middle psi may
be informally called either an antecedent or part of the consequent
depending on context.")))

  ;; declare a unary primitive symbol (negation)
  (prim wff "¬" (wff "φ")
    (meta ;(:html-sym . "lnot")
          ;(:html-fn . #'unary-prefix)
          (:comment . "Logical negation.")
          (:description . "If phi is a wff, so is [not] phi or \"not phi\".
In classical logic (which is our logic), a wff is interpreted as either
true or false. So if phi is true, then not phi is false; if phi is false,
then not phi is true. Traditionally, Greek letters are used to represent
wffs, and we follow this convention. In propositional calculus, we define
only wffs built up from other wffs, i.e. there is no starting or \"atomic\"
wff. Later, in predicate calculus, we will extent the basic wff definition
by including atomic wffs.")))

  ;; binary primitive symbol (bi-implication)
  (prim wff "↔" (wff ![φ ψ]))
;    (meta (:html-sym . "leftrightarrow")
;          (:html-fn . #'binary-infix)))

  ;; set ↔ as the equality operator for the wff type
;  (setf (symkind-eq-op wff) (symref-target !↔))
  (set-symkind-eq-op wff !↔)

;  (prim pr "True" ())

;  (def "df-False" pr "False" () ()
;    [¬ True])

  ;;; ************
  ;;; Axioms (and a definition)
  ;;; ************

  ;; Axiom "Simp"
  (ax "ax1" (wff ![φ ψ])
      (meta (:comment . "Axiom <i>Simp</i>.")
            (:description . "Axiom A1 of [Margaris], p.49. One of the
3 axioms of propositional calculus. The 3 axioms are also given as
Definition 2.1 of [Hamilton] p. 28. This axiom is called <i>Simp</i> or
\"the principle of simplification\" in <i>Principia Mathematica</i>
(Theorem *2.02, p. 100) because \"it enables us to pass from the
joint assertion of phi and psi to the assertion of phi simply.\"</p>
<p>Propositional calculus (axioms ax1 through ax3 and rule ax-mp) can
be thought of as assertnig formulas that are universally \"true\" when
their variables are replaced by any combination of \"true\" and \"false\".
Propositional calculus was first formalized by Frege in 1879, using as his
axioms (in addition to rule ax-mp) the wffs ax1, ax2, [pm2.04], con3,
nega, and negb. Around 1930 Lukasiewicz simplified the system by
eliminating the third (which follows from the first two, as you can see by
looking at the proof of [pm2.04]) and replacing the last three with our
ax3. (Thanks to Ted Ulrich for this information.)</p>
<p>The theorems of propositional calculus are also called <i>tautologies</i>.
Tautologies can be proved very simply using truth tables, based on the
true/false interpretation of propositional calculus. To do this, we assign
all possible combinations of true and false to the wff variables and verify
that the result always evaluates to true. This is called the <i>semantic</i>
approach. Our approach is called the <i>syntactic</i> approach, in which
everything is derived from axioms. A metatheorem called the Completeness
Theorem for Propositional Calculus shows that the two approaches are
equivalent and even provides an algorithm for automatically generating
syntactic proofs from a truth table. Those proofs, however, tend to be long,
and the much shorter proofs that we show here were found manually."))

      (ass [→ φ → ψ φ]))

  ;; Axiom "Frege"
  (ax "ax2" (wff ![φ ψ χ])
      (meta (:comment . "Axiom <i>Frege</i>.")
            (:description . "Axiom A2 of [Margaris] p. 49. One of the
3 axioms of propositional calculus. It distributes an antecedent over two
consequents. This axiom was part of Frege's original system and is known
as <i>Frege</i> in the literature. It is also proved as Theorem *2.77 of
[WhiteheadRussell] p. 108."))
      
      (ass [→ → φ → ψ χ → → φ ψ → φ χ]))

  ;; Axiom "Transp"
  (ax "ax3" (wff ![φ ψ])
      (meta (:comment . "Axiom <i>Transp</i>.")
            (:description . "Axiom A3 of [Margaris] p. 49. One of the
3 axioms of propositional calculus. It swaps or transposes the order of the
consequents when negation is removed. An informal example is that the
statement \"if there are no clouds in the sky, it is not raining\" implies
the statement \"if it is raining, there are clouds in the sky.\" This axiom
is called <i>Transp</i> or \"the principle of transposition\" in Principia
Mathematca (Theorem *2.17, p. 103)."))
      
      (ass [→ → ¬ φ ¬ ψ → ψ φ]))

  ;; The Law of Modus Ponens
  (ax "ax-mp" (wff ![φ ψ])
      (meta (:comment . "Rule of Modus Ponens.")
            (:description . "The postulated inference rule of propositional
calculus. See e.g. rule 1 of [Hamilton] p. 73. The rule says, \"if phi is
true, and phi implies psi, then psi must also be true.\" This rule is
sometimes called \"detachment\", since it detaches the minor premise from
the major premise."))
      
      (hypo [→ φ ψ])
      (hypo [φ])
      (ass [ψ]))

;  (ax "ax-True" ()
;      (ass [True]))

  ;; definition of the biconditional
  ;; we cannot use ↔ to define itself, have to use an axiom
  (ax "df-bi" (wff ![φ ψ])
      (ass [¬ → [→ ↔ φ ψ ¬ → → φ ψ ¬ → ψ φ] ¬ [→ ¬ → → φ ψ ¬ → ψ φ ↔ φ ψ]]))

  ;; Define conjunction, disjunction
  (def "df-an" wff "∧" (wff ![φ ψ]) ()
    [¬ → φ ¬ ψ])
;    (meta (:html-sym . "wedge")
;          (:html-fn . #'binary-infix)))

  (def "df-or" wff "∨" (wff ![φ ψ]) ()
    [→ ¬ φ ψ])
;    (meta (:html-sym . "vee")
;          (:html-fn . #'binary-infix)))
 
) ; end context "prop-ax"
