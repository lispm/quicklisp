;;;  field.lisp --- the record field classes

;;;  Copyright (C) 2005, 2006, 2007, 2008, 2009 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: cobstor

#+cmu (ext:file-comment "$Module: field.lisp $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

(in-package :cobstor)

(defvar *treat-blank-as-zero* t
  "If true blanks in numeric fields with storage type `display'
are treated as zeros.")

(defvar *numeric-field-sign-convention* :acu
  "Notation used by numerical fields in presence of a sign.  This
can be :ACU :MF :VAX :IBM :NCR.")

(defvar *binary-field-size-convention* :acu
  "The storage convention for binary fields.  Normally this
is :ACU but it can also be :ACU1, :ACU-TIGHT or :MF.")

(defvar *default-sign-position* :trailing
  "Position of a separate sign in numeric fields if not
explicitly specified.  It can be :LEADING or :TRAILING.")

(defvar *packed-decimal-field-class* 'cbl-comp3-field
  "Class to use to represent packed decimal fields.  The Cobol
manual says: Data items described as PACKED-DECIMAL are identical
to COMP-3. You can cause unsigned PACKED-DECIMAL to be treated as
COMP-6 by using a compile-time option.")

(defvar *binary-field-class* 'cbl-comp4-field
  "Class to use to represent binary fields.  The Cobol manual
says: By default, a BINARY data item is identical to a COMP-4
data item. The compile-time option \"-D5\" treats BINARY data items
as COMP-5 items instead.")

;; Considering that we compile using IBM numeric memory layout (option
;; -Dci of ccbl), the COMP-4 looks good."
(defvar *comp-field-class* 'cbl-comp4-field
  "Class to use to represent comp fields.  The Cobol manual says:
In VAX/COBOL compatibility mode, a COMP data item is the same as
COMP-4 and is treated as binary data. In RM/COBOL compatibility
lmode, COMP is the same as COMP-2. You can use compile-time
options to change the default behavior.")

(defvar *default-endianness* :big-endian
  "What kind of CPU should be emulated when handling fields which
depend on the endianness of the architecture.  Allowed values
are :BIG-ENDIAN (like Motorola CPUs) and :LITTLE-ENDIAN (like
Intel CPUs).")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftype ubyte () '(unsigned-byte 8))

(deftype field-storage (&optional size)
  `(array ubyte (,(or size '*))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-condition cbl-condition (error)
  ()
  (:documentation
   "Base abstract class for all the conditions of the cobstor
package."))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun get-area-field-value (storage offset length)
  (subseq storage offset (+ offset length)))

(defun set-area-field-value (value storage offset length)
  (cond ((not value)
	 (fill storage 0 :start offset :end (+ offset length)))
	((stringp value)
	 (set-string-field-value value storage offset length))
	(t (replace storage value :start1 offset :end1 (+ offset length)))))

;;
;; STRING
;;

(defconst +whitespace-bytes+ (mapcar #'char-code +whitespace+)
  "List of whitespace characters as unsigned bytes.")

(defun get-string-field-value (storage offset length)
  (be end -1
    (do ((i 0 (1+ i)))
	((= i length))
      (be b (aref storage (+ offset i))
	(when (zerop b)
	  (return))
	(unless (find b +whitespace-bytes+)
	  (setf end i))))
    (incf end)
    (be string (make-string end)
      (loop
	 for i from 0 below end
	 for j from 0
	 do (setf (schar string j)
		  (code-char (aref storage (+ i offset)))))
      string)))

(defun set-string-field-value (value storage offset length)
  (if value
      (progn
	(unless (stringp value)
	  (setf value (format nil "~A" value)))
	(loop
	   with end = (+ offset length)
	   for i from offset below end
	   for j from 0 below (length value)
	   do (setf (aref storage i) (char-code (char value j)))
	   finally (when (< i end)
		     (fill storage #.(char-code #\space) :start i :end end))))
      (fill storage 0 :start offset :end (+ offset length)))
  value)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; NUMERIC
;;

(defun signed-positive-digit-coding-table ()
  (ecase *numeric-field-sign-convention*
    ((:acu :ibm :vax :ncr) #.(string->byte-vector "{ABCDEFGHI"))
    (:mf #.(string->byte-vector "0123456789"))))

(defun signed-negative-digit-coding-table ()
  (ecase *numeric-field-sign-convention*
    ((:acu :ibm :vax :ncr) #.(string->byte-vector "}JKLMNOPQR"))
    (:mf #.(string->byte-vector "pqrstuvwxy"))))

(defun unsigned-digit-coding-table ()
  #.(string->byte-vector "0123456789"))

(defun get-numeric-field-value (storage offset size signed sign-separated-p sign-position decimals)
  (let ((positive-table
	 (signed-positive-digit-coding-table))
	(negative-table
	 (signed-negative-digit-coding-table))
	(start offset)
	(last (+ offset size -1))
	(value 0)
	(sign 1))
    (labels ((byte-at (position)
	       (aref storage position))
	     (decode-signed-digit-at (position)
	       (let ((c (byte-at position)))
		 (aif (position c negative-table)
		      (values it -1)
		      (values (position c positive-table) 1))))
	     (decode-unsigned-digit-at (position)
	       (let ((byte (byte-at position)))
		 (or (position byte (unsigned-digit-coding-table))
		     (when (and *treat-blank-as-zero*
				(= byte #.(char-code #\space)))
		       0))))
	     (negative-marker-p (byte)
	       (= #.(char-code #\-) byte)))
      (when signed
	(if sign-separated-p
	    (if (eq sign-position :leading)
		(progn
		  (setf sign (if (negative-marker-p (byte-at start)) -1 1))
		  (incf start))
		(progn
		  (setf sign (if (negative-marker-p (byte-at last)) -1 1))
		  (decf last)))
	    (if (eq sign-position :leading)
		(progn
		  (multiple-value-setq (value sign)
		    (decode-signed-digit-at start))
		  (incf start))
		(decf last))))
	(loop
	   for i from start to last
	   for digit = (decode-unsigned-digit-at i)
	   when digit			; skip if rubbish
	   do (setf value (+ (* value 10) digit))
	   finally
	     (when (and signed
			(not sign-separated-p)
			(eq sign-position :trailing))
	       (multiple-value-setq (digit sign)
		   (decode-signed-digit-at (1+ last)))
	       (when digit		; digit may be rubbish
		 (setf value (+ (* value 10) digit))))
	     (return
	       (* sign
		  ;; adjust for decimals
		  (/ value
		     (expt 10 decimals))))))))

(defun set-numeric-field-value (value storage offset size signed sign-separated-p sign-position decimals)
  (unless value
    (setf value 0))
  (let ((absolute-value (truncate (* (abs value) (expt 10 decimals))))
	(negative (< value 0))
	(start offset)
	(end (+ offset size -1)))
    (flet ((pop-unit ()
	     (prog1 (mod absolute-value 10)
	       (setf absolute-value (truncate absolute-value 10))))
	   (make-sign ()
	     (if negative
		 #.(char-code #\-)
		 #.(char-code #\+))))
      (when signed
	;; encode the sign or the digit containing the sign
	(if sign-separated-p
	    ;; encode the sign as separate character '+' or '-'
	    (if (eq sign-position :leading)
		(progn
		  (setf (aref storage start) (make-sign))
		  (incf start))
		(progn
		  (setf (aref storage end) (make-sign))
		  (decf end)))
	    ;; encode the sign as part of a digit
	    (if (eq sign-position :trailing)
		(be last-digit (pop-unit)
		  (setf (aref storage end)
			(aref (if negative
				 (signed-negative-digit-coding-table)
				 (signed-positive-digit-coding-table))
			     last-digit))
		  (decf end))
		(incf start))))
      (loop
	 for i from end downto start
	 do
	 (setf (aref storage i)
	       (aref (unsigned-digit-coding-table) (pop-unit))))
      (when (and signed
		 (not sign-separated-p)
		 (eq sign-position :leading))
	(setf (aref storage 0)
	      (aref (if negative
		       (signed-negative-digit-coding-table)
		       (signed-positive-digit-coding-table))
		   (pop-unit)))))
    value))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; COMP-1
;;

(defun get-comp1-field-value (storage offset)
  (flet ((val (pos)
	   (aref storage pos)))
    (if (zerop (logand (val offset) #x80))
	;; positive value
	(+ (* 256 (val offset))
	   (val (1+ offset)))
	;; negative value
	(- (+ (* 256 (logand (val offset) #x7F))
	      (val (1+ offset)))))))

(defun set-comp1-field-value (value storage offset)
  (unless value
    (setf value 0))
  (let* ((negative (< value 0))
	 (absolute-value (truncate (abs value))))
    (setf (aref storage offset)
	  (logior (logand (ash absolute-value -8) #x7F)
		  (if negative
		      #x80
		      0)))
    (setf (aref storage (1+ offset))
	  (logand absolute-value #xFF))
    value))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; COMP-2
;;

(defun get-comp2-field-value (storage offset size signed decimals)
  (loop
     with value = 0
     for i from offset below (+ offset size)
     for byte = (aref storage i)
     do (setf value (+ byte (* 10 value)))
     finally (return
	       (/ (if signed
		      (be sign (aref storage i)
			;; we assume that in any storage convention
			;; anything besides 0x0D is positive
			(if (= sign #x0D)
			    (- value)
			    value))
		      value)
		  (expt 10 decimals)))))

(defun set-comp2-field-value (value storage offset size signed decimals)
  (unless value
    (setf value 0))
  (loop
     for i from (1- size) downto 0
     for v = (truncate (abs (* value (expt 10 decimals)))) then (truncate v 10)
     do (setf (aref storage (+ offset i)) (mod v 10))
     finally (when signed
	       (setf (aref storage (+ offset size))
		     (if (< value 0)
			 #x0D
			 (if (eq *numeric-field-sign-convention* :acu)
			     #x0B
			     #x0C)))))
  value)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; COMP-3
;;

(defun get-comp3-field-value (storage offset size signed decimals)
  (labels ((left (byte)
	     (logand (ash byte -4) #x0F))
	   (right (byte)
	     (logand byte #x0F))
	   (conv (byte)
	     (+ (* 10 (left byte))
		(right byte))))
    (loop
       with value = 0
       for i from offset below (+ offset size -1)
       for v = (conv (aref storage i))
       do (setf value (+ (* 100 value) v))
       finally
	 (setf value (+ (* value 10)
			(left (aref storage i))))
	 (return
	   (/
	    (if (and signed
		     (= #x0D (right (aref storage i))))
		;; we assume that in any storage convention anything
		;; besides 0x0D is positive
		(- value)
		value)
	    (expt 10 decimals))))))

(defun set-comp3-field-value (value storage offset size signed decimals)
  (unless value
    (setf value 0))
  ;; remove the decimal point shifting the decimals to the left
  (be absolute-value (truncate (* (abs value) (expt 10 decimals)))
    (flet ((set-byte (pos left-nibble right-nibble)
	     (setf (aref storage (+ offset pos))
		   (logior (ash (logand left-nibble #x0F) 4)
			   (logand right-nibble #x0F))))
	   (pop-unit ()
	     (prog1 (mod absolute-value 10)
	       (setf absolute-value (truncate absolute-value 10)))))
      ;; set the rightmost byte with the rightmost digit and the sign
      (set-byte (1- size)
		;; the first digit goes in the left nibble
		(pop-unit)
		;; the sign goes in the right nibble
		(case *numeric-field-sign-convention*
		  (:vax (cond ((not signed)
			       #x0C)
			      ((< value 0)
			       #x0D)
			      (t #x0C)))
		  (:ncr (cond ((not signed)
			       #x0F)
			      ((< value 0)
			       #x0D)
			      (t #x0B)))
		  (otherwise (cond ((not signed)
				    #x0F)
				   ((< value 0)
				    #x0D)
				   (t #x0F)))))
      (loop
	 for i from (- size 2) downto 0
	 do
	 (let* ((right (pop-unit))
		(left (pop-unit)))
	   (set-byte i left right)))))
  value)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; COMP-4/5
;;

(defun size-of-comp4/5 (digits signed)
  "Return the size of the storage accupied by COMP-4 Cobol field
with DIGITS and SIGN.  This is according to the AcuCobol
documentation that considers at most 31 digits."
  (assert (< digits 32))
  (case *binary-field-size-convention*
    (:acu (cond ((<= 1 digits 4) 2)
		((<= 5 digits 9) 4)
		((<= 10 digits 18) 8)
		((<= 19 digits 28) 12)
		((> digits 28) 16)))
    (:acu1 (cond ((<= 1 digits 2) 1)
		 ((<= 3 digits 4) 2)
		 ((<= 5 digits 9) 4)
		 ((<= 10 digits 18) 8)
		 ((<= 19 digits 28) 12)
		 ((> digits 28) 16)))
    (:acu-tight (cond ((<= 1 digits 2) 1)
		      ((<= 3 digits 4) 2)
		      ((<= 5 digits 6) 3)
		      ((<= 7 digits 9) 4)
		      ((<= 10 digits 11) 5)
		      ((<= 12 digits 14) 6)
		      ((<= 15 digits 16) 7)
		      ((<= 17 digits 18) 8)
		      ((<= 19 digits 21) 9)
		      ((<= 22 digits 23) 10)
		      ((<= 24 digits 26) 11)
		      ((<= 27 digits 28) 12)
		      ((> digits 28) 13)))
    (:mf (cond ((<= 1 digits 2) 1)
	       ((<= 3 digits 4) 2)
	       ((<= 5 digits 7) 3)
	       ((<= 8 digits 9) 4)
	       ((<= 10 digits 12) 5)
	       ((<= 13 digits 14) 6)
	       ((<= 15 digits 16) 7)
	       ((<= 17 digits 18) 8)
	       ((= 19 digits)
		(if signed 9 8))
	       ((<= 20 digits 21) 9)
	       ((<= 22 digits 23) 10)
	       ((= 24 digits)
		(if signed 11 10))
	       ((<= 25 digits 26) 11)
	       ((<= 27 digits 28) 12)
	       ((> digits 28) 13)))))

(defun get-field-value-comp4/5-be (storage offset size decimals)
  (flet ((val (pos)
	   (aref storage (+ offset pos))))
    (be negative (not (zerop (logand #x80 (val 0))))
      (loop
	 with value = 0
	 for i from 0 below size
	 do (setf value (+ (* value 256)
			   (if negative
			       (logxor (val i) #xFF)
			       (val i))))
	 finally (return
		   (/ (if negative
			  (- (1- value))
			  value)
		      (expt 10 decimals)))))))

(defun set-field-value-comp4/5-be (value storage offset size decimals)
  (unless value
    (setf value 0))
  (be negative (< value 0)
      absolute-value (truncate (* (abs value)
				  (expt 10 decimals)))
    (when negative
      (incf absolute-value))
    (loop
       for i from (1- size) downto 0
       do (setf (aref storage (+ offset i))
		(if negative
		    (logxor (logand absolute-value #xFF) #xFF)
		    (logand absolute-value #xFF)))
	 (setf absolute-value (ash absolute-value -8))
       finally (when negative
		 (setf (aref storage offset)
		       (logior #x80 (aref storage 0))))))
  value)

(defun get-field-value-comp4/5-le (storage offset size decimals)
  (flet ((val (pos)
	   (aref storage (+ offset pos))))
    (be negative (not (zerop (logand #x80 (val (1- size)))))
      (loop
	 with value = 0
	 for i from (1- size) downto 0
	 do (setf value (+ (* value 256)
			   (if negative
			       (logxor (val i) #xFF)
			       (val i))))
	 finally (return
		   (/ (if negative
			  (- (1- value))
			  value)
		      (expt 10 decimals)))))))

(defun set-field-value-comp4/5-le (value storage offset size decimals)
  (unless value
    (setf value 0))
  (be negative (< value 0)
      absolute-value (truncate (* (abs value) (expt 10 decimals)))
    (when negative
      (incf absolute-value))
    (loop
       for i from 0 below size
       do (setf (aref storage (+ offset i))
		(if negative
		    (logxor (logand absolute-value #xFF) #xFF)
		    (logand absolute-value #xFF)))
	 (setf absolute-value (ash absolute-value -8))
       finally (when negative
		 (setf (aref storage (1- i))
		       (logior #x80 (aref storage (1- i)))))))
  value)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; COMP-6
;;

(defun get-comp6-field-value (storage offset size decimals)
  (labels ((left (byte)
	     (logand (ash byte -4) #x0F))
	   (right (byte)
	     (logand byte #x0F))
	   (conv (byte)
	     (+ (* 10 (left byte))
		(right byte))))
    (loop
       with value = 0
       for i from 0 below size
       for v = (conv (aref storage (+ offset i)))
       do (setf value (+ (* 100 value) v))
       finally (return
		 (/ value
		    (expt 10 decimals))))))

(defun set-comp6-field-value (value storage offset size decimals)
  (unless value
    (setf value 0))
  ;; remove the decimal point shifting the decimals to the left
  (be absolute-value (truncate (* (abs value) (expt 10 decimals)))
    (flet ((set-byte (pos left-nibble right-nibble)
	     (setf (aref storage (+ offset pos))
		   (logior (ash (logand left-nibble #x0F) 4)
			   (logand right-nibble #x0F))))
	   (pop-unit ()
	     (prog1 (mod absolute-value 10)
	       (setf absolute-value (truncate absolute-value 10)))))
      (loop
	 for i from (1- size) downto 0
	 do (let* ((right (pop-unit))
		   (left (pop-unit)))
	      (set-byte i left right)))))
  value)
