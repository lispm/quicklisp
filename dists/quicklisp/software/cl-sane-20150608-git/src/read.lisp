(in-package :sane)

(defclass blockwise-binary-input-stream (trivial-gray-stream-mixin)
  ((refill-buffer-function)
   (buflen                 :initarg  :buflen :initform 256)
   (buffer                 :initform nil)
   (buffer-fill            :initform 0)
   (buffer-pointer         :initform 0)
   (eof                    :initform nil))

  (:documentation
"This is an input stream that reads input from a function,
REFILL-BUFFER-FUNCTION, which has the signature

   (REFILL-BUFFER-FUNCTION buffer length)

where BUFFER is an array of length LENGTH. The function must return two
values: the number of bytes read and nil/t depending on whether we've got to
eof.

There is a STREAM-READ-BYTE function specialized on this class, which 'does
the right thing'."))

(defmethod initialize-instance :after ((stream blockwise-binary-input-stream)
                                       &key)
  (setf (slot-value stream 'buffer)
        (make-array (slot-value stream 'buflen)
                    :element-type '(unsigned-byte 8))))

(defmethod stream-open (stream)
  (:documentation "Open a stream object"))

(defmethod stream-open ((stream blockwise-binary-input-stream))
  "On the superclass, this is a no-op."  nil)

(defclass sane-blocking-stream (blockwise-binary-input-stream)
  ((device         :reader   device     :initarg :device)
   (parameters     :reader   parameters :initform nil))

  (:documentation
"SANE-BLOCKING-STREAM masks reading from a scanner device as reading from a
stream. Once STREAM-OPEN has been called on the stream, the PARAMETERS slot will
contain an instance of the SANE::PARAMETERS class valid for the scanner. In
particular, since sane_start will have been called, the members will be valid at
this point."))

(defmethod initialize-instance :before ((stream sane-blocking-stream)
                                        &key device)
  (unless (subtypep (type-of device) 'device)
    (error "Cannot create a sane-blocking-stream with a device of type ~A"
           (type-of device)))
  
  (setf (slot-value stream 'device) device)

  (setf (slot-value stream 'refill-buffer-function)
        (lambda (buffer length) (refill-buffer! buffer (handle device)
                                                length))))

(defmethod stream-open ((stream sane-blocking-stream))
  "Open the scanner as a stream. After this operation, the PARAMETERS slot is
valid on STREAM."
  (ensure-ok (sane-lowlevel::sane_start (handle (device stream)))
             "Could not start scanning operation.")
  (setf (slot-value stream 'parameters) (get-parameters (device stream))))

(defmethod stream-read-byte ((stream blockwise-binary-input-stream))
  (with-slots (buffer buffer-fill buffer-pointer eof refill-buffer-function)
      stream
    (cond
      ((< buffer-pointer buffer-fill)
       (incf buffer-pointer)
       (aref buffer (1- buffer-pointer)))
      
      (eof :eof)
      
      (t
       (multiple-value-bind (length-read now-eof)
           (funcall refill-buffer-function buffer (slot-value stream 'buflen))
         (setf eof now-eof)
         (setf buffer-fill length-read)
         (setf buffer-pointer 0)
         (stream-read-byte stream))))))

(defun refill-buffer! (buffer handle length)
  (let ((eof nil))

    (with-foreign-objects ((fbuffer :uchar length)
                           (length-read :int))
      
      (case (sane-lowlevel::sane_read handle fbuffer
                                      length length-read)
        (:SANE_STATUS_GOOD nil)

        ((:SANE_STATUS_EOF :SANE_STATUS_CANCELLED)
         (setf eof t))
    
        (t
         (error "Unhandled error refilling buffer.")))

      (iterate (for i from 0 to (1- (mem-ref length-read :int)))
               (setf (aref buffer i) (mem-aref fbuffer :uchar i)))

      (values (mem-ref length-read :int) eof))))

(defmacro with-open-scanner-stream (stream device &body forms)
  "Open up a binary gray stream STREAM reading from DEVICE and execute FORMS
with it open."

  `(let ((,stream (make-instance 'sane-blocking-stream :device ,device)))
     (stream-open ,stream)

     ,@forms))
