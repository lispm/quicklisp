;;; ************
;;; Metatheorems
;;; ************

;; hack -- to avoid loading this file multiple times
 ;; end module "pred-meta"
;; Bourbaki - Pure Predicate Calculus (without free variables)
;;; For all "∀"
;;; There exists "∃"

;;; Ongoing project: replace all theorems with [→ φ ∀ x φ]
;;; hypotheses with corresponding [Bvp x φ]
;;; at the moment, the replacing theorem is named with a '

;;; Ongoing project: systematic naming for all equality theorems
;;; name: eq-<sym>; eq-<sym>-i

(module "pred"

  (meta (:comment . "Pure predicate calculus."))
  
  (symkind "WFF")
  (symkind "VAR")

  (export [!!pred-ax])

  (providing (("prop-ax" . !/pred-ax) "WFF")
    (export [!!prop]))

  (local "al" ()
    (meta (:comment . "Properties of universal quantification.")))
  (local "ex" ()
    (meta (:comment . "Properties of existential quantification.")))
  (local "bv" ()
    (meta (:comment . "Bound variable hypothesis builders.")))

  (th-pattern !ax4)

;; Inference with generalization
;; if thr is of the form "∀ x φ → ψ"
;; return φ => ψ
  (extend-metath "infer" (ref)
    (let* ((pfl (parse-subproof ref))
           (assert (car (subproof-assert pfl))))
      (if-match [→ ∀ ?x ?phi ?psi] assert
        (if (provenp ?phi)
          (linear-subproof (?phi) (?psi)
            [ax-gen ,?x ,?phi]
            pfl
            [ax-mp [∀ ,?x ,?phi] ,?psi])))))

;; Theorems

(th "gen2" (var ![x y] wff "φ")
    (meta (:comment . "Generalization applied twice."))
    (hypo [φ])
    (ass [∀ x ∀ y φ])
    (proof
      [ax-gen y φ]
      [ax-gen x [∀ y φ]]))

(th "mpg" (var "x" wff ![φ ψ])
    (meta (:comment . "Modus ponens combined with generalization."))
    (hypo [→ ∀ x φ ψ] [φ])
    (ass [ψ])
    (proof
      [ax-gen x φ]
      [ax-mp [∀ x φ] ψ]))

;(th "mpgbi" (var "x" wff ![φ ψ])
;    (meta (:comment . "Modus ponens on biconditional combined with generalization."))
;    (hypo [↔ ∀ x φ ψ] [φ])
;    (ass [ψ])
;    (proof
;      [(infer !bi>) [∀ x φ] ψ]
;      [mpg x φ ψ]))

;(th "mpgbir" (var "x" wff ![φ ψ])
;    (meta (:comment . "Modus ponens on biconditional combined with generalization."))
;    (hypo [↔ φ ∀ x ψ] [ψ])
;    (ass [φ])
;    (proof
;      [(infer !bi<) φ [∀ x ψ]]
;      [mpg x ψ φ]))

(th "ax6e" (var "x" wff "φ")
    (meta (:comment . "Abbreviated version of ax6.")
          (:description . "a6e in set.mm."))
    (ass [→ ∃ x ∀ x φ φ])
    (proof
      [df-ex x [∀ x φ]]
      [b2i ded ax6 x φ [∃ x ∀ x φ]]))

(in-context !al
  (th "imt" (var "x" wff ![φ ψ])
    (meta (:comment . "Distributive law of universal quantification over implication (one direction).")
          (:description . "Theorem 19.20 of [Margaris] p. 90; 19.20 in set.mm."))
    (ass [→ ∀ x → φ ψ → ∀ x φ ∀ x ψ])
    (proof
      [ax4 x φ]
      [infer syl> [∀ x φ] φ ψ]
      [+syl ax4   x [→ φ ψ] [→ ∀ x φ ψ]]
      [infer ax5 x [→ φ ψ] [→ ∀ x φ ψ]]
      [ax5 x φ ψ]
      [syl [∀ x → φ ψ] [∀ x → ∀ x φ ψ] [→ ∀ x φ ∀ x ψ]]))

  (th "imti" (var "x" wff ![φ ψ])
    (meta (:comment . "Inference quantifying both antecedent and consequent.")
          (:description . "19.20i in set.mm."))
    (hypo [→ φ ψ])
    (ass [→ ∀ x φ ∀ x ψ])
    (proof
      [+syl  ax4 x φ ψ]
      [infer ax5 x φ ψ]))
  (th-pattern !imti)

  (th "2imti" (var "x" wff ![φ ψ χ])
    (meta (:comment . "Inference quantifying antecedent, nested antecedent, and consequent.")
          (:description . "19.20ii in set.mm."))
    (hypo [→ φ → ψ χ])
    (ass [→ ∀ x φ → ∀ x ψ ∀ x χ])
    (proof
      [imti x φ [→ ψ χ]]
      [imt x ψ χ]
      [syl [∀ x φ] [∀ x → ψ χ] [→ ∀ x ψ ∀ x χ]]))
  (th-pattern !2imti)

  (th "imtd" (var "x" wff ![φ ψ χ])
    (meta (:comment . "Deduction quantifying both antecedent and consequent.")
          (:description . "19.20d in set.mm."))
    (hypo [Bvp x φ] [→ φ → ψ χ])
    (ass [→ φ → ∀ x ψ ∀ x χ])
    (proof
      [infer df-Bvp x φ]
      [infer ax4 x [→ φ ∀ x φ]]
      [2imti x φ ψ χ]
      [syl φ [∀ x φ] [→ ∀ x ψ ∀ x χ]]))

  (th "eqt" (var "x" wff ![φ ψ])
    (meta (:comment . "Equality theorem for universal quantification.")
          (:description . "Theorem 19.15 of [Margaris] p. 90; 19.15 in set.mm."))
    (ass [→ ∀ x ↔ φ ψ ↔ ∀ x φ ∀ x ψ])
    (proof
      [bi> φ ψ]
      [bi< φ ψ]
      [2imti x [↔ φ ψ] φ ψ]
      [2imti x [↔ φ ψ] ψ φ]
      [>bid [∀ x ↔ φ ψ] [∀ x φ] [∀ x ψ]]))
  (th-pattern !eqt)

  (th "eqti" (var "x" wff ![φ ψ])
    (meta (:comment . "Inference adding universal quantifier to both sides of an equivalence.")
          (:description . "albii in set.mm."))
    (hypo [↔ φ ψ])
    (ass [↔ ∀ x φ ∀ x ψ])
    (proof
      [infer eqt x φ ψ]))
  (th-pattern !eqti) )

(in-context !bv
  (th "exp" (var "x" wff "φ")
    (meta (:comment . "Convert a bound-variable predicate to primitive symbols."))
    (hypo [Bvp x φ])
    (ass [→ φ ∀ x φ])
    (proof
      [infer df-Bvp x φ]
      [infer ax4 x [→ φ ∀ x φ]]))
  (th-pattern !exp)

  (th "imp" (var "x" wff "φ")
    (meta (:comment . "Convert from primitive symbols to the bound variable predicate."))
    (hypo [→ φ ∀ x φ])
    (ass [Bvp x φ])
    (proof
      [ax-gen x [→ φ ∀ x φ]]
      [infer df-Bvp x φ]))
  (th-pattern !imp)

  (th "def" (var "x" wff ![φ ψ])
    (meta (:comment . "If a variable is bound in the definiens, it is bound in the definiendum."))
    (hypo [Bvp x ψ] [↔ φ ψ])
    (ass [Bvp x φ])
    (proof
      [exp x ψ]
      [!al!eqti x φ ψ]
      [<imtr ψ [∀ x ψ] φ [∀ x φ]]
      [imp x φ]))

  (th "true" (var "x" wff "φ")
    (meta (:comment . "No variable if (effectively) free in a theorem.")
          (:description . "hbth in set.mm."))
    (hypo [φ])
    (ass [Bvp x φ])
    (proof
      [ax-gen x φ]
      [infer ax1 [∀ x φ] φ]
      [imp x φ]))
  (th-pattern !true)

  (th "al-b" (var "x" wff "φ")
    (meta (:comment . "x is not free in A. x phi.")
          (:description . "Appendix example in [Megill] p. 450; hba1 in set.mm."))
    (ass [Bvp x [∀ x φ]])
    (proof
      [id [∀ x φ]]
      [infer ax5 x φ [∀ x φ]]
      [imp x [∀ x φ]]))
  (th-pattern !al-b)

  (th "bvp" (var "x" wff "φ")
    (meta (:comment . "x is not free in \"x bound in phi\"."))
      (ass [Bvp x [Bvp x φ]])
      (proof
        [al-b x [→ φ ∀ x φ]]
        [df-Bvp x φ]
        [def x [Bvp x φ] [∀ x → φ ∀ x φ]]))
  (th-pattern !bvp)

  (th "neg" (var "x" wff "φ")
    (meta (:comment . "If x is not free in phi, it is not free in ! phi.")
          (:description . "hbne in set.mm."))
    (hypo [Bvp x φ])
    (ass [Bvp x ¬ φ])
    (proof
      [exp x φ]
      [infer con φ [∀ x φ]]
      [!al!imti x [¬ ∀ x φ] [¬ φ]]
      [ax6 x φ]
      [infer con< [∀ x ¬ ∀ x φ] φ]
      [syl [¬ φ] [∀ x ¬ ∀ x φ] [∀ x ¬ φ]]
      [imp x [¬ φ]]))
  (th-pattern !neg)

  (th "al" (var ![x y] wff "φ")
    (meta (:comment . "If x is not free in phi, it is not free in A. y phi.")
          (:description . "hbal in set.mm."))
    (hypo [Bvp x φ])
    (ass [Bvp x ∀ y φ])
    (proof
      [exp x φ]
      [!al!imti y φ [∀ x φ]]
      [ax7 y x φ]
      [syl [∀ y φ] [∀ y ∀ x φ] [∀ x ∀ y φ]]
      [imp x [∀ y φ]]))
  (th-pattern !al)

  (th "ex" (var ![x y] wff "φ")
    (meta (:comment . "If x is not free in phi, it is not free in Ex. y phi.")
          (:description . "hbex in set.mm."))
    (hypo [Bvp x φ])
    (ass [Bvp x ∃ y φ])
    (proof
      [neg x φ]
      [al x y [¬ φ]]
      [neg x [∀ y ¬ φ]]
      [df-ex y φ]
      [def x [∃ y φ] [¬ ∀ y ¬ φ]]))
  (th-pattern !ex)

  (th "im" (var "x" wff ![φ ψ])
    (meta (:comment . "If x is not free in phi and psi, it is not free in phi -> psi.")
          (:description . "hbim in set.mm."))
    (hypo [Bvp x φ] [Bvp x ψ])
    (ass [Bvp x → φ ψ])
    (proof
      [neg x φ]
      [exp x [¬ φ]]
      [exp x ψ]
      [<neg φ ψ]
      [!/pred!al!imti x [¬ φ] [→ φ ψ]]
      [syl [¬ φ] [∀ x ¬ φ] [∀ x → φ ψ]]
      [ax1 ψ φ]
      [!/pred!al!imti x ψ [→ φ ψ]]
      [syl ψ [∀ x ψ] [∀ x → φ ψ]]
      [!/pred!im!ja φ ψ [∀ x → φ ψ]]
      [imp x [→ φ ψ]]))
  (th-pattern !im)

  (th "or" (var "x" wff ![φ ψ])
    (meta (:comment . "If x is not free in phi and psi, it is not free in phi \\/ psi.")
          (:description . "hbor in set.mm."))
    (hypo [Bvp x φ] [Bvp x ψ])
    (ass [Bvp x ∨ φ ψ])
    (proof
      [neg x φ]
      [im x [¬ φ] ψ]
      [df-or φ ψ]
      [def x [∨ φ ψ] [→ ¬ φ ψ]]))
  (th-pattern !or)

  (th "an" (var "x" wff ![φ ψ])
    (meta (:comment . "If x is not free in phi and psi, it is not free in phi /\\ psi.")
          (:description . "hban in set.mm."))
    (hypo [Bvp x φ] [Bvp x ψ])
    (ass [Bvp x ∧ φ ψ])
    (proof
      [neg x ψ]
      [im x φ [¬ ψ]]
      [neg x [→ φ ¬ ψ]]
      [df-an φ ψ]
      [def x [∧ φ ψ] [¬ → φ ¬ ψ]]))
  (th-pattern !an)

  (th "bi" (var "x" wff ![φ ψ])
    (meta (:comment . "If x is not free in phi and psi, it is not free in phi <-> psi.")
          (:description . "hbbi in set.mm."))
    (hypo [Bvp x φ] [Bvp x ψ])
    (ass [Bvp x ↔ φ ψ])
    (proof
      [im x φ ψ]
      [im x ψ φ]
      [an x [→ φ ψ] [→ ψ φ]]
      [dfbi φ ψ]
      [def x [↔ φ ψ] [∧ → φ ψ → ψ φ]]))
  (th-pattern !bi)

  (th "ex-b" (var "x" wff "φ")
    (meta (:comment . "x is not free in Ex. x phi.")
          (:description . "hbe1 in set.mm."))
    (ass [Bvp x ∃ x φ])
    (proof
      [al-b x [¬ φ]]
      [neg x [∀ x ¬ φ]]
      [df-ex x φ]
      [def x [∃ x φ] [¬ ∀ x ¬ φ]]))
  (th-pattern !ex-b)

  (th "negt" (var "x" wff "φ")
    (meta (:comment . "A closed form of <a href=\"pred/bv/neg.html\">neg</a>.")
          (:description . "hbnt in set.mm."))
    (ass [→ Bvp x φ Bvp x ¬ φ])  
    (proof
      [con φ [∀ x φ]]
      [!/pred!al!2imti x [→ φ ∀ x φ] [¬ ∀ x φ] [¬ φ]]
      [ax6 x φ]
      [infer con< [∀ x ¬ ∀ x φ] φ]
      [!/pred!im!rp32 [∀ x → φ ∀ x φ] [∀ x ¬ ∀ x φ] [∀ x ¬ φ] [¬ φ]]
      [!/pred!al!imti x [∀ x → φ ∀ x φ] [→ ¬ φ ∀ x ¬ φ]]
      [al-b x [→ φ ∀ x φ]]
      [infer df-Bvp x [∀ x → φ ∀ x φ]]
      [infer ax4 x [→ ∀ x → φ ∀ x φ ∀ x ∀ x → φ ∀ x φ]]
      [syl [∀ x → φ ∀ x φ] [∀ x ∀ x → φ ∀ x φ] [∀ x → ¬ φ ∀ x ¬ φ]]
      [df-Bvp x φ]
      [df-Bvp x [¬ φ]]
      [<imtr [∀ x → φ ∀ x φ] [∀ x → ¬ φ ∀ x ¬ φ] [Bvp x φ] [Bvp x ¬ φ]]))
  (th-pattern !negt) )

(in-context !ex
  (th "spec1" (var "x" wff "φ")
    (meta (:comment . "If a wff is true, it is true for at least one instance.")
          (:description . "19.8a in set.mm."))
    (ass [→ φ ∃ x φ])
    (proof
      [ax4 x [¬ φ]]
      [infer con> [∀ x ¬ φ] φ]
      [ded df-ex x φ φ]))
  (th-pattern !spec1))

(th "al->ex" (var "x" wff "φ")
    (meta (:comment . "If a wff is true for all x, it is true for at least one x.")
          (:description . "Theorem 19.2 of [Margaris] p. 89; 19.2 in set.mm."))
    (ass [→ ∀ x φ ∃ x φ])
    (proof
      [ax4 x φ]
      [ded !ex!spec1 x φ [∀ x φ]]))
(th-pattern !al->ex)

(in-context !bv
  (th "eq.al" (var "x" wff "φ")
    (meta (:comment . "A wff may be quantified with a variable not free in it.")
          (:description . "19.3r in set.mm."))
    (hypo [Bvp x φ])
    (ass  [↔ φ ∀ x φ])
    (proof
      [exp x φ]
      [ax4 x φ]
      [>bii φ [∀ x φ]]))
  (th-pattern !eq.al) )

(in-context !al
  (th "com" (var ![x y] wff "φ")
    (meta (:comment . "Commute universal quantifiers.")
          (:description . "Theorem 19.5 of [Margaris] p. 89; alcom in set.mm."))
    (ass [↔ ∀ x ∀ y φ ∀ y ∀ x φ])
    (proof
      [ax7 x y φ]
      [ax7 y x φ]
      [>bii [∀ x ∀ y φ] [∀ y ∀ x φ]]))
  (th-pattern !com)

  (th "neg" (var "x" wff "φ")
    (meta (:description . "Theorem 19.7 of [Margaris] p. 89; alnex in set.mm."))
    (ass [↔ ∀ x ¬ φ ¬ ∃ x φ])
    (proof
      [df-ex x φ]
      [conb>i [∃ x φ] [∀ x ¬ φ]]))
  (th-pattern !neg)

  (th "df.ex" (var "x" wff "φ")
    (meta (:comment . "Universal quantifier defined in terms of existential.")
          (:description . "Theorem 19.6 of [Margaris] p. 89; alex in set.mm."))
    (ass [↔ ∀ x φ ¬ ∃ x ¬ φ])
    (proof
      [bneg> φ]
      [eqti x φ [¬ ¬ φ]]
      [neg x [¬ φ]]
      [bitr [∀ x φ] [∀ x ¬ ¬ φ] [¬ ∃ x ¬ φ]]))
  (th-pattern !df.ex))

(in-context !bv
  (th "eq.ex" (var "x" wff "φ")
    (meta (:comment . "A wff may be quantified with a variable not free in it.")
          (:description . "Variation of Theorem 19.9 of [Margaris] p. 89."))
    (hypo [Bvp x φ])
    (ass [↔ φ ∃ x φ])
    (proof
      [exp x φ]
      [!/pred!ex!spec1 x φ]
      [infer con φ [∀ x φ]]
      [!/pred!al!imti x [¬ ∀ x φ] [¬ φ]]
      [infer con [∀ x ¬ ∀ x φ] [∀ x ¬ φ]]
      [ax6 x φ]
      [syl [¬ ∀ x ¬ φ] [¬ ∀ x ¬ ∀ x φ] φ]
      [+syl df-ex x φ φ]
      [>bii [φ] [∃ x φ]]))
  (th-pattern !eq.ex)

  (th "eq.ext" (var "x" wff "φ")
    (meta (:comment . "A closed version of one direction of eq.ex.")
          (:description . "19.9t in set.mm."))
    (ass [→ Bvp x φ → ∃ x φ φ])
    (proof
      [negt x φ]
      [ded df-Bvp x [¬ φ] [Bvp x φ]]
      [ded ax4 x [→ ¬ φ ∀ x ¬ φ] [Bvp x φ]]
      [ded con< φ [∀ x ¬ φ] [Bvp x φ]]
      [df-ex x φ]
      [b2i !/pred!im!rp32 [Bvp x φ] [¬ ∀ x ¬ φ] φ [∃ x φ]])) )

(in-context !ex
  (th "neg" (var "x" wff "φ")
    (meta (:description . "Theormem 19.14 of [Margaris] p. 90; exnal in set.mm."))
    (ass [↔ ∃ x ¬ φ ¬ ∀ x φ])
    (proof
      [!al!df.ex x φ]
      [conb>i [∀ x φ] [∃ x ¬ φ]]))
  (th-pattern !neg)

  (th "imt" (var "x" wff ![φ ψ])
    (meta (:comment . "Theorem adding existential quantifier to antecedent and consequent.")
          (:description . "Theorem 19.22 of [Margaris] p. 90; 19.22 in set.mm."))
    (ass [→ ∀ x → φ ψ → ∃ x φ ∃ x ψ])
    (proof
      [con φ ψ]
      [!al!2imti x [→ φ ψ] [¬ ψ] [¬ φ]]
      [ded con [∀ x ¬ ψ] [∀ x ¬ φ] [∀ x → φ ψ]]
      [df-ex x φ]
      [df-ex x ψ]
      [<imtrg [∀ x → φ ψ] [¬ ∀ x ¬ φ] [¬ ∀ x ¬ ψ] [∃ x φ] [∃ x ψ]]))
  (th-pattern !imt)

  (th "imti" (var "x" wff ![φ ψ])
    (meta (:comment . "Inference adding existential quantifier to antecedent and consequent.")
          (:description . "19.22i in set.mm."))
    (hypo [→ φ ψ])
    (ass [→ ∃ x φ ∃ x ψ])
    (proof
      [infer imt x φ ψ])))

(in-context !al
  (th "im.ex" (var "x" wff ![φ ψ])
    (meta (:comment . "A transformation of quantifiers and logical connectives.")
          (:description . "alinexa in set.mm."))
    (ass [↔ ∀ x → φ ¬ ψ ¬ ∃ x ∧ φ ψ])
    (proof
      [!im!df.nan φ ψ]
      [eqti x [→ φ ¬ ψ] [¬ ∧ φ ψ]]
      [neg x [∧ φ ψ]]
      [bitr [∀ x → φ ¬ ψ] [∀ x ¬ ∧ φ ψ] [¬ ∃ x ∧ φ ψ]]))
  (th-pattern !im.ex))

(in-context !ex
  (th "an.al" (var "x" wff ![φ ψ])
    (meta (:comment . "A transformation of quantifiers and logical connectives.")
          (:description . "exanali in set.mm."))
    (ass [↔ ∃ x ∧ φ ¬ ψ ¬ ∀ x → φ ψ])
    (proof
      [!im!df.an φ ψ]
      [!al!eqti x [→ φ ψ] [¬ ∧ φ ¬ ψ]]
      [!al!neg x [∧ φ ¬ ψ]]
      [bitr [∀ x → φ ψ] [∀ x ¬ ∧ φ ¬ ψ] [¬ ∃ x ∧ φ ¬ ψ]]
      [conb>i [∀ x → φ ψ] [∃ x ∧ φ ¬ ψ]]))
  (th-pattern !an.al)

  (th "com1" (var ![x y] wff "φ")
    (meta (:comment . "Commute existential quantifiers.")
          (:description . "One direction of Theorem 19.11 of [Margaris] p. 89; excomim in set.mm."))
    (ass [→ ∃ x ∃ y φ ∃ y ∃ x φ])
    (proof
      [spec1 x φ]
      [infer imt y φ [∃ x φ]]
      [infer imt x [∃ y φ] [∃ y ∃ x φ]]
      [!bv!ex-b x φ]
      [!bv!ex   x y [∃ x φ]]
      [!bv!eq.ex x [∃ y ∃ x φ]]
      [b2i syl [∃ x ∃ y φ] [∃ x ∃ y ∃ x φ] [∃ y ∃ x φ]]))
  (th-pattern !com1)

  (th "com" (var ![x y] wff "φ")
    (meta (:comment . "Commute existential quantifiers.")
          (:description . "Theorem 19.11 of [Margaris] p. 89; excom in set.mm."))
    (ass [↔ ∃ x ∃ y φ ∃ y ∃ x φ])
    (proof
      [com1 x y φ]
      [com1 y x φ]
      [>bii [∃ x ∃ y φ] [∃ y ∃ x φ]]))
  (th-pattern !com))

(th "exal-swap" (var ![x y] wff "φ")
    (meta (:comment . "Swap existential and universal quantifiers.")
          (:description . "Theorem 19.12 of [Margaris] p. 89; 19.12 in set.mm.
Assuming the converse is a mistake sometimes made by beginners!"))
    (ass [→ ∃ x ∀ y φ ∀ y ∃ x φ])
    (proof
      [!bv!al-b y φ]
      [!bv!ex y x [∀ y φ]]
      [!bv!exp y [∃ x ∀ y φ]]
      [ax4 y φ]
      [infer !ex!imt x [∀ y φ] φ]
      [infer !al!imt y [∃ x ∀ y φ] [∃ x φ]]
      [syl [∃ x ∀ y φ] [∀ y ∃ x ∀ y φ] [∀ y ∃ x φ]]))
(th-pattern !exal-swap)

(in-context !al
  (th "di-bi<" (var "x" wff ![φ ψ])
    (meta (:comment . "Quantifying an equivalence with a variable not free on the left side.")
          (:description . "Theorem 19.16 of [Margaris] p. 90; 19.16 in set.mm."))
    (hypo [Bvp x φ])
    (ass [→ ∀ x ↔ φ ψ ↔ φ ∀ x ψ])
    (proof
      [eqt x φ ψ]
      [!bv!eq.al x φ]
      [infer ax1 [↔ φ ∀ x φ] [∀ x ↔ φ ψ]]
      [!bi!trd [∀ x ↔ φ ψ] φ [∀ x φ] [∀ x ψ]]))

  (th "di-bi>" (var "x" wff ![φ ψ])
    (meta (:comment . "Quantifying an equivalence with a variable not free on the right side.")
          (:description . "Theorem 19.17 of [Margaris] p. 90; 19.17 in set.mm."))
    (hypo [Bvp x ψ])
    (ass [→ ∀ x ↔ φ ψ ↔ ∀ x φ ψ])
    (proof
      [eqt x φ ψ]
      [!bv!eq.al x ψ]
      [!bi!comi ψ [∀ x ψ]]
      [infer ax1 [↔ ∀ x ψ ψ] [∀ x ↔ φ ψ]]
      [!bi!trd [∀ x ↔ φ ψ] [∀ x φ] [∀ x ψ] ψ])) )

(in-context !ex
  (th "eqt" (var "x" wff ![φ ψ])
    (meta (:comment . "Theorem adding existential quantifier to both sides of an equivalence.")
          (:description . "Theorem 19.18 of [Margaris] p. 90; 19.18 in set.mm."))
    (ass [→ ∀ x ↔ φ ψ ↔ ∃ x φ ∃ x ψ])
    (proof
      [bi> φ ψ]
      [infer !al!imt x [↔ φ ψ] [→ φ ψ]]
      [!ex!imt x φ ψ]
      [syl [∀ x ↔ φ ψ] [∀ x → φ ψ] [→ ∃ x φ ∃ x ψ]]
      [bi< φ ψ]
      [infer !al!imt x [↔ φ ψ] [→ ψ φ]]
      [!ex!imt x ψ φ]
      [syl [∀ x ↔ φ ψ] [∀ x → ψ φ] [→ ∃ x ψ ∃ x φ]]
      [>bid [∀ x ↔ φ ψ] [∃ x φ] [∃ x ψ]]))
  (th-pattern !eqt)

  (th "eqti" (var "x" wff ![φ ψ])
    (meta (:comment . "Inference adding existential quantifier to both sides of an equivalence."))
    (hypo [↔ φ ψ])
    (ass [↔ ∃ x φ ∃ x ψ])
    (proof
      [infer eqt x φ ψ])))

;; use (ginfer !ex!eqt)
;(th "eqt-∃-i" (var "x" wff ("φ" "ψ"))
;    (hypo [↔ φ ψ])
;    (ass [↔ ∃ x φ ∃ x ψ])
;    (proof
;      [ax-gen x [↔ φ ψ]]
;      [eqt-∃ x φ ψ]
;      [ax-mp [∀ x ↔ φ ψ] [↔ ∃ x φ ∃ x ψ]]))

(th "imgen>" (var "x" wff ![φ ψ])
    (meta (:comment . "Generalize the consequent of an implication with a variable not free in the antecedent.")
          (:description . "Theorem 19.21 of [Margaris] p. 90; 19.21 in set.mm."))
    (hypo [Bvp x φ])
    (ass [↔ ∀ x → φ ψ → φ ∀ x ψ])
    (proof
      [!bv!exp x φ]
      [!al!imt x φ ψ]
      [!im!rp32 [∀ x → φ ψ] [∀ x φ] [∀ x ψ] φ]
      ;; --
      [!bv!al-b x ψ]
      [!bv!im x φ [∀ x ψ]]
      [!bv!exp x [→ φ ∀ x ψ]]
      [ax4 x ψ]
      [infer syl< [∀ x ψ] ψ φ]
      [infer !al!imt x [→ φ ∀ x ψ] [→ φ ψ]]
      [syl [→ φ ∀ x ψ] [∀ x → φ ∀ x ψ] [∀ x → φ ψ]]
      [>bii [∀ x → φ ψ] [→ φ ∀ x ψ]]))
(th-pattern !imgen>)

(th "imgen2>" (var ![x y] wff ![φ ψ])
    (meta (:comment . "Generalize the consequent of an implication with two variables.")
          (:description . "Theorem 19.21 of [Margaris] p. 90 but with 2 quantifiers; 19.21-2 in set.mm."))
    (hypo [Bvp x φ] [Bvp y φ])
    (ass [↔ ∀ x ∀ y → φ ψ → φ ∀ x ∀ y ψ])
    (proof
      [imgen> y φ ψ]
      [infer !al!eqt x [∀ y → φ ψ] [→ φ ∀ y ψ]]
      [imgen> x φ [∀ y ψ]]
      [bitr [∀ x ∀ y → φ ψ] [∀ x → φ ∀ y ψ] [→ φ ∀ x ∀ y ψ]]))

; bi> imgen>
;(th "stdpc5" (var "x" wff ("φ" "ψ"))
;    (hypo [Bvp x φ])
;    (ass [→ ∀ x → φ ψ → φ ∀ x ψ])
;    (proof
;      [imgen> x φ ψ]
;      [(infer !bi>) [∀ x → φ ψ] [→ φ ∀ x ψ]]))

(th "imgen>i" (var "x" wff ![φ ψ])
    (meta (:comment . "Inference generalizing the consequent of an implication.")
          (:description . "Inference from Theorem 19.21 of [Margaris] p. 90; 19.21ai in set.mm."))
    (hypo [Bvp x φ] [→ φ ψ])
    (ass [→ φ ∀ x ψ])
    (proof
      [ax-gen x [→ φ ψ]]
      [infer imgen> x φ ψ]))
(th-pattern !imgen>i)

(th "imgen>d" (var "x" wff ![φ ψ χ])
    (meta (:comment . "Deduction generalizing the consequent of an implication.")
          (:description . "Deduction from Theorem 19.21 of [Margaris] p. 90; 19.21ad in set.mm."))
    (hypo [Bvp x φ] [Bvp x ψ] [→ φ → ψ χ])
    (ass [→ φ → ψ ∀ x χ])
    (proof
      [!bv!an x φ ψ]
      [imp φ ψ χ]
      [imgen>i x [∧ φ ψ] χ]
      [exp φ ψ [∀ x χ]]))
(th-pattern !imgen>d)

; ded ax4
;(th "imgen>ri" (var "x" wff ("φ" "ψ"))

(in-context !ex
  (th "imtd" (var "x" wff ![φ ψ χ])
    (meta (:description . "Deduction from Theorem 19.22 of [Margaris] p. 90; 19.22d in set.mm."))
    (hypo [Bvp x φ] [→ φ → ψ χ])
    (ass [→ φ → ∃ x ψ ∃ x χ])
    (proof
      [imgen>i x φ [→ ψ χ]]
      [imt x ψ χ]
      [syl φ [∀ x → ψ χ] [→ ∃ x ψ ∃ x χ]])))

(th "imgen<" (var "x" wff ![φ ψ])
    (meta (:comment . "Generalize the antecedent of an implication with a variable not free in the consequent.")
          (:description . "Theorem 19.23 of [Margaris] p. 90; 19.23 in set.mm."))
    (hypo [Bvp x ψ])
    (ass [↔ ∀ x → φ ψ → ∃ x φ ψ])
    (proof
      [!ex!imt x φ ψ]
      [!bv!eq.ex x ψ]
      [b2i !im!rp33 [∀ x → φ ψ] [∃ x φ] [∃ x ψ] ψ]
      [!bv!ex-b x φ]
      [!bv!im x [∃ x φ] ψ]
      [!ex!spec1 x φ]
      [infer syl> φ [∃ x φ] ψ]
      [imgen>i x [→ ∃ x φ ψ] [→ φ ψ]]
      [>bii [∀ x → φ ψ] [→ ∃ x φ ψ]]))
(th-pattern !imgen<)

(th "imgen<i" (var "x" wff ![φ ψ])
    (meta (:comment . "Inference generalizing the antecedent of an implication.")
          (:description . "Inference from Theorem 19.23 of [Margaris] p. 90; 19.23ai in set.mm."))
    (hypo [Bvp x ψ] [→ φ ψ])
    (ass [→ ∃ x φ ψ])
    (proof
      [ax-gen x [→ φ ψ]]
      [infer imgen< x φ ψ]))
(th-pattern !imgen<i)
;      [im.bldex x φ ψ]
 ;     [fv.eqex x ψ]
  ;    [!im!brp22-r [∃ x φ] [∃ x ψ] ψ]))

; +syl !ex!spec1
;(th "imgen<ri" (var "x" wff ("φ" "ψ"))
;    (hypo [→ ∃ x φ ψ])
;    (ass [→ φ ψ])

(th "imgen<d" (var "x" wff ![φ ψ χ])
    (meta (:comment . "Deduction generalizing the antecedent of an implication.")
          (:description . "Deduction from Theorem 19.23 of [Margaris] p. 90; 19.23ad in set.mm."))
    (hypo [Bvp x φ] [Bvp x χ] [→ φ → ψ χ])
    (ass [→ φ → ∃ x ψ χ])
    (proof
      [imgen>i x φ [→ ψ χ]]
      [imgen< x ψ χ]
      [b2i syl φ [∀ x → ψ χ] [→ ∃ x ψ χ]]))
(th-pattern !imgen<d)

(in-context !al
  (th "di-an" (var "x" wff ![φ ψ])
    (meta (:comment . "Universal quantification distributes over conjunction.")
          (:description . "Theorem 19.26 of [Margaris] p. 90; 19.26 in set.mm."))
    (ass [↔ ∀ x ∧ φ ψ ∧ ∀ x φ ∀ x ψ])
    (proof
      [!an!sim< φ ψ]
      [infer imt x [∧ φ ψ] φ]
      [!an!sim> φ ψ]
      [infer imt x [∧ φ ψ] ψ]
      [jca [∀ x ∧ φ ψ] [∀ x φ] [∀ x ψ]]
      [>and φ ψ]
      [2imti x φ ψ [∧ φ ψ]]
      [imp [∀ x φ] [∀ x ψ] [∀ x ∧ φ ψ]]
      [>bii [∀ x ∧ φ ψ] [∧ ∀ x φ ∀ x ψ]]))

  (th "di-an>" (var "x" wff ![φ ψ])
    (meta (:comment . "Quantifying a conjunction with a variable not free in the first conjunct.")
          (:description . "Theorem 19.27 of [Margaris] p. 90; 19.27 in set.mm."))
    (hypo [Bvp x ψ])
    (ass [↔ ∀ x ∧ φ ψ ∧ ∀ x φ ψ])
    (proof
      [di-an x φ ψ]
      [!bv!eq.al x ψ]
      [infer !an!eqt> ψ [∀ x ψ] [∀ x φ]]
      [b2i bitr [∀ x ∧ φ ψ] [∧ ∀ x φ ∀ x ψ] [∧ ∀ x φ ψ]]))

  (th "di-an<" (var "x" wff ![φ ψ])
    (meta (:comment . "Quantifying a conjunction with a variable not free in the second conjunct.")
          (:description . "Theorem 19.28 of [Margaris] p. 90; 19.28 in set.mm."))
    (hypo [Bvp x φ])
    (ass [↔ ∀ x ∧ φ ψ ∧ φ ∀ x ψ])
    (proof
      [di-an x φ ψ]
      [!bv!eq.al x φ]
      [infer !an!eqt< φ [∀ x φ] [∀ x ψ]]
      [b2i bitr [∀ x ∧ φ ψ] [∧ ∀ x φ ∀ x ψ] [∧ φ ∀ x ψ]])) )

; rename!
(in-context !ex
  (th "di-an>r" (var "x" wff ![φ ψ])
    (meta (:description . "Theorem 19.29 of [Margaris] p. 90; 19.29 in set.mm."))
    (ass [→ ∧ ∀ x φ ∃ x ψ ∃ x ∧ φ ψ])
    (proof
      [!al!imt x φ [¬ ψ]]
      [!al!neg x ψ]
      [b2i !im!rp33 [∀ x → φ ¬ ψ] [∀ x φ] [∀ x ¬ ψ] [¬ ∃ x ψ]]
      [infer con [∀ x → φ ¬ ψ] [→ ∀ x φ ¬ ∃ x ψ]]
      [df-an [∀ x φ] [∃ x ψ]]
      [neg x [→ φ ¬ ψ]]
      [<imtr [¬ → ∀ x φ ¬ ∃ x ψ] [¬ ∀ x → φ ¬ ψ]
             [∧ ∀ x φ ∃ x ψ] [∃ x ¬ → φ ¬ ψ]]
      [df-an φ ψ]
      [ded [infer eqt x [∧ φ ψ] [¬ → φ ¬ ψ]] [∧ ∀ x φ ∃ x ψ]]))

  (th "di-an<r" (var "x" wff ![φ ψ])
    (meta (:description . "Variation of Theorem 19.29 of [Margaris] p. 90; 19.29r in set.mm."))
    (ass [→ ∧ ∃ x φ ∀ x ψ ∃ x ∧ φ ψ])
    (proof
      [di-an>r x ψ φ]
      [!an!com [∃ x φ] [∀ x ψ]]
      [!an!com φ ψ]
      [infer eqt x [∧ φ ψ] [∧ ψ φ]]
      [<imtr [∧ ∀ x ψ ∃ x φ] [∃ x ∧ ψ φ]
             [∧ ∃ x φ ∀ x ψ] [∃ x ∧ φ ψ]])) )

;(th "19.29r2" (var ![x y] wff ![φ ψ])
;    (meta (:description . "Variation of Theorem 19.29 of [Margaris] p. 90 with double quantification."))
;    (ass [→ ∧ ∃ x ∃ y φ ∀ x ∀ y ψ ∃ x ∃ y ∧ φ ψ])
;    (proof
;      [!ex!di-an<r x [∃ y φ] [∀ y ψ]]
;      [!ex!di-an<r y φ ψ]
;      [(ginfer !ex!imt) x [∧ ∃ y φ ∀ y ψ] [∃ y ∧ φ ψ]]
;      [syl [∧ ∃ x ∃ y φ ∀ x ∀ y ψ] [∃ x ∧ ∃ y φ ∀ y ψ] [∃ x ∃ y ∧ φ ψ]]))

;(th "19.29x" (var ![x y] wff ![φ ψ])
;    (meta (:description . "Variation of Theorem 19.29 of [Margaris] p. 90 with mixed quantification."))
;    (ass [→ ∧ ∃ x ∀ y φ ∀ x ∃ y ψ ∃ x ∃ y ∧ φ ψ])
;    (proof
;      [!ex!di-an<r x [∀ y φ] [∃ y ψ]]
;      [!ex!di-an>r y φ ψ]
;      [(ginfer !ex!eqt) x [∧ ∀ y φ ∃ y ψ] [∃ y ∧ φ ψ]]
;      [syl [∧ ∃ x ∀ y φ ∀ x ∃ y ψ] [∃ x ∧ ∀ y φ ∃ y ψ] [∃ x ∃ y ∧ φ ψ]]))

(in-context !ex
  (th "di-im" (var "x" wff ![φ ψ])
    (meta (:description . "Theorem 19.35 of [Margaris] p. 90; 19.25 in set.mm.
This theorem is useful for moving an implication (in the form of the right-hand side)
into the scope of a single existential quantifier."))
    (ass [↔ ∃ x → φ ψ → ∀ x φ ∃ x ψ])
    (proof
      [!al!di-an x φ [¬ ψ]]
      [!an!df.nim φ ψ]
      [infer !al!eqt x [∧ φ ¬ ψ] [¬ → φ ψ]]
      [df-an [∀ x φ] [∀ x ¬ ψ]]
      [>bitr [∀ x ∧ φ ¬ ψ] [∧ ∀ x φ ∀ x ¬ ψ]
             [∀ x ¬ → φ ψ] [¬ → ∀ x φ ¬ ∀ x ¬ ψ]]
      [conb>i [∀ x ¬ → φ ψ] [→ ∀ x φ ¬ ∀ x ¬ ψ]]
      [df-ex x ψ]
      [infer !im!eqt> [∃ x ψ] [¬ ∀ x ¬ ψ] [∀ x φ]]
      [df-ex x [→ φ ψ]]
      [<bitr [→ ∀ x φ ¬ ∀ x ¬ ψ] [¬ ∀ x ¬ → φ ψ]
             [→ ∀ x φ ∃ x ψ] [∃ x → φ ψ]]
      [!bi!comi [→ ∀ x φ ∃ x ψ] [∃ x → φ ψ]]))
  (th-pattern !di-im)

  (th "di-im>" (var "x" wff ![φ ψ])
    (meta (:comment . "Quantifying an implication with a variable not free in the consequent.")
          (:description . "Theorem 19.36 of [Margaris] p. 90; 19.36 in set.mm."))
    (hypo [Bvp x ψ])
    (ass [↔ ∃ x → φ ψ → ∀ x φ ψ])
    (proof
      [di-im x φ ψ]
      [!bv!eq.ex x ψ]
      [infer !im!eqt> ψ [∃ x ψ] [∀ x φ]]
      [b2i bitr [∃ x → φ ψ] [→ ∀ x φ ∃ x ψ] [→ ∀ x φ ψ]]))

  (th "di-im<" (var "x" wff ![φ ψ])
    (meta (:comment . "Quantifying an implication with a variable not free in the antecedent.")
          (:description . "Theorem 19.37 of [Margaris] p. 90; 19.37 in set.mm."))
    (hypo [Bvp x φ])
    (ass [↔ ∃ x → φ ψ → φ ∃ x ψ])
    (proof
      [di-im x φ ψ]
      [!bv!eq.al x φ]
      [infer !im!eqt< [φ] [∀ x φ] [∃ x ψ]]
      [b2i bitr [∃ x → φ ψ] [→ ∀ x φ ∃ x ψ] [→ φ ∃ x ψ]])) )

(th "19.38" (var "x" wff ![φ ψ])
    (meta (:description . "From set.mm. Theorem 19.38 of [Margaris] p. 90."))
    (ass [→ → ∃ x φ ∀ x ψ ∀ x → φ ψ])
    (proof
      [Bvp x [∃ x φ]]
      [Bvp x [∀ x ψ]]
      [Bvp x [→ ∃ x φ ∀ x ψ]]      
      [→ φ ∃ x φ]
      [→ ∀ x ψ ψ]
      [!im!imti φ [∃ x φ] [∀ x ψ] ψ]
      [imgen>i [x] [→ ∃ x φ ∀ x ψ] [→ φ ψ]]))

(th "19.39" (var "x" wff ![φ ψ])
    (meta (:description . "From set.mm. Theorem 19.39 of [Margaris] p. 90."))
    (ass [→ → ∃ x φ ∃ x ψ ∃ x → φ ψ])
    (proof
      [→ ∀ x φ ∃ x φ]
      [infer syl> [∀ x φ] [∃ x φ] [∃ x ψ]]
      [↔ ∃ x → φ ψ → ∀ x φ ∃ x ψ]
      [b2i syl [→ ∃ x φ ∃ x ψ] [→ ∀ x φ ∃ x ψ] [∃ x → φ ψ]]))

(th "19.24" (var "x" wff ![φ ψ])
    (meta (:description . "From set.mm. Theorem 19.24 of [Margaris] p. 90."))
    (ass [→ → ∀ x φ ∀ x ψ ∃ x → φ ψ])
    (proof
      [→ ∀ x ψ ∃ x ψ]
      [infer syl< [∀ x ψ] [∃ x ψ] [∀ x φ]]
      [↔ ∃ x → φ ψ → ∀ x φ ∃ x ψ]
      [b2i syl [→ ∀ x φ ∀ x ψ] [→ ∀ x φ ∃ x ψ] [∃ x → φ ψ]]))

(th "19.25" (var ![x y] wff ![φ ψ])
    (meta (:description . "From set.mm. Theorem 19.25 of [Margaris] p. 90."))
    (ass [→ ∀ y ∃ x → φ ψ → ∃ y ∀ x φ ∃ y ∃ x ψ])
    (proof
      [↔ ∃ x → φ ψ → ∀ x φ ∃ x ψ]
      [b2i !al!imti y [∃ x → φ ψ] [→ ∀ x φ ∃ x ψ]]
      [!ex!imt y [∀ x φ] [∃ x ψ]]
      [syl [∀ y ∃ x → φ ψ] [∀ y → ∀ x φ ∃ x ψ] [→ ∃ y ∀ x φ ∃ y ∃ x ψ]]))

(th "alexor-pre" (var "x" wff ![φ ψ])
    (meta (:comment . "Distribution of disjunction with quantifiers.")
          (:description . "19.30 in set.mm; Theorem 19.30 of [Margaris] p. 90."))
    (ass [→ ∀ x ∨ φ ψ ∨ ∀ x φ ∃ x ψ])
    (proof
      [!al!imt x [¬ ψ] φ]
      [!or!com φ ψ]
      [df-or ψ φ]
      [bitr [∨ φ ψ] [∨ ψ φ] [→ ¬ ψ φ]]
      [!al!eqti x [∨ φ ψ] [→ ¬ ψ φ]]
      [!or!com [∀ x φ] [¬ ∀ x ¬ ψ]]
      [df-ex x ψ]
      [infer !or!eqt> [∃ x ψ] [¬ ∀ x ¬ ψ] [∀ x φ]]
      [!im!df.or [∀ x ¬ ψ] [∀ x φ]]
      [<bitr [∨ ∀ x φ ¬ ∀ x ¬ ψ] [∨ ¬ ∀ x ¬ ψ ∀ x φ]
             [∨ ∀ x φ ∃ x ψ] [→ ∀ x ¬ ψ ∀ x φ]]
      [!im!<tr [∀ x → ¬ ψ φ] [→ ∀ x ¬ ψ ∀ x φ]
               [∀ x ∨ φ ψ] [∨ ∀ x φ ∃ x ψ]]))
(th-pattern !alexor-pre)

(in-context !al
  (th "di-or<" (var "x" wff ![φ ψ])
    (meta (:comment . "Universal quantification with a variable not free in the left disjunct.")
          (:description . "19.32 in set.mm. Theorem 19.32 in [Margaris] p. 90."))
    (hypo [Bvp x φ])
    (ass [↔ ∀ x ∨ φ ψ ∨ φ ∀ x ψ])
    (proof
      [Bvp x ¬ φ]
      [imgen> x [¬ φ] ψ]
      [df-or φ ψ]
      [eqti x [∨ φ ψ] [→ ¬ φ ψ]]
      [df-or φ [∀ x ψ]]
      [<bitr [∀ x → ¬ φ ψ] [→ ¬ φ ∀ x ψ]
             [∀ x ∨ φ ψ] [∨ φ ∀ x ψ]]))
  (th-pattern !or<)

  (th "di-or>" (var "x" wff ![φ ψ])
    (meta (:comment . "Universal quantification with a variable not free in the right disjunct.")
          (:description . "19.31 in set.mm. Theorem 19.31 in [Margaris] p. 90."))
    (hypo [Bvp x ψ])
    (ass [↔ ∀ x ∨ φ ψ ∨ ∀ x φ ψ])
    (proof
      [di-or< x ψ φ]
      [!or!com φ ψ]
      [eqti x [∨ φ ψ] [∨ ψ φ]]
      [!or!com [∀ x φ] ψ]
      [<bitr [∀ x ∨ ψ φ] [∨ ψ ∀ x φ]
             [∀ x ∨ φ ψ] [∨ ∀ x φ ψ]]))
  (th-pattern !or>) )

(in-context !ex
  (th "di-or" (var "x" wff ![φ ψ])
    (meta (:comment . "Distribution of existential quantifier over disjunction.")
          (:description . "19.43 in set.mm. Theorem 19.43 in [Margaris] p. 90."))
    (ass [↔ ∃ x ∨ φ ψ ∨ ∃ x φ ∃ x ψ])
    (proof
      [↔ ¬ ∨ φ ψ ∧ ¬ φ ¬ ψ]
      [!al!eqti x [¬ ∨ φ ψ] [∧ ¬ φ ¬ ψ]]      
      [!al!di-an x [¬ φ] [¬ ψ]]
      [↔ ∀ x ¬ φ ¬ ∃ x φ]
      [↔ ∀ x ¬ ψ ¬ ∃ x ψ]
      [!an!eqti [∀ x ¬ φ] [¬ ∃ x φ] [∀ x ¬ ψ] [¬ ∃ x ψ]]
      [!bi!2tri [∀ x ¬ ∨ φ ψ] [∀ x ∧ ¬ φ ¬ ψ] [∧ ∀ x ¬ φ ∀ x ¬ ψ] [∧ ¬ ∃ x φ ¬ ∃ x ψ]]
      [infer neqt [∀ x ¬ ∨ φ ψ] [∧ ¬ ∃ x φ ¬ ∃ x ψ]]
      [df-ex [x] [∨ φ ψ]]
      [↔ ∨ [∃ x φ] [∃ x ψ] ¬ ∧ [¬ ∃ x φ] [¬ ∃ x ψ]]
      [<bitr [¬ ∀ x ¬ ∨ φ ψ] [¬ ∧ ¬ ∃ x φ ¬ ∃ x ψ]
             [∃ x ∨ φ ψ] [∨ ∃ x φ ∃ x ψ]]))
  (th-pattern !di-or)

  (th "di-or>" (var "x" wff ![φ ψ])
    (meta (:comment . "Existential quantification with a variable not free in the left disjunct.")
          (:description . "19.44 in set.mm. Theorem 19.44 in [Margaris] p. 90."))
    (hypo [Bvp x ψ])
    (ass [↔ ∃ x ∨ φ ψ ∨ ∃ x φ ψ])
    (proof
      [di-or x φ ψ]
      [↔ ψ ∃ x ψ]
      [infer !or!eqt> ψ [∃ x ψ] [∃ x φ]]
      [b2i bitr [∃ x ∨ φ ψ] [∨ ∃ x φ ∃ x ψ] [∨ ∃ x φ ψ]]))
  (th-pattern !di-or>)

  (th "di-or<" (var "x" wff ![φ ψ])
    (meta (:comment . "Existential quantification with a variable not free in the right disjunct.")
          (:description . "19.45 in set.mm. Theorem 19.45 in [Margaris] p. 90."))
    (hypo [Bvp x φ])
    (ass [↔ ∃ x ∨ φ ψ ∨ φ ∃ x ψ])
    (proof
      [di-or x φ ψ]
      [↔ φ ∃ x φ]
      [infer !or!eqt< φ [∃ x φ] [∃ x ψ]]
      [b2i bitr [∃ x ∨ φ ψ] [∨ ∃ x φ ∃ x ψ] [∨ φ ∃ x ψ]]))
  (th-pattern !di-or<) )

(in-context !al
  (th "di-or" (var "x" wff ![φ ψ])
    (meta (:comment . "Distribution of universal quantifier over disjunction (one direction).")
          (:description . "19.33 in set.mm. Theorem 19.33 in [Margaris] p. 90."))
    (ass [→ ∨ ∀ x φ ∀ x ψ ∀ x ∨ φ ψ])
    (proof
      [or< φ ψ]
      [imti x φ [∨ φ ψ]]
      [or> φ ψ]
      [imti x ψ [∨ φ ψ]]
      [jaoi [∀ x φ] [∀ x ψ] [∀ x ∨ φ ψ]]))
  (th-pattern !di-or))

(th "alexor" (var "x" wff ![φ ψ])
    (meta (:comment . "Distribution of disjunction with quantifiers.")
          (:description . "19.34 in set.mm. Theorem 19.34 of [Margaris] p. 90."))
    (ass [→ ∨ ∀ x φ ∃ x ψ ∃ x ∨ φ ψ])
    (proof
      [→ ∀ x φ ∃ x φ]
      [infer !or!imt< [∀ x φ] [∃ x φ] [∃ x ψ]]
      [!ex!di-or x φ ψ]
      [b2i syl [∨ ∀ x φ ∃ x ψ] [∨ ∃ x φ ∃ x ψ] [∃ x ∨ φ ψ]]))

(in-context !ex
  (th "di-an" (var "x" wff ![φ ψ])
    (meta (:comment . "Distribution of existential quantifier over conjunction (one direction).")
          (:description . "19.40 in set.mm. Theorem 19.40 of [Margaris] p. 90."))
    (ass [→ ∃ x ∧ φ ψ ∧ ∃ x φ ∃ x ψ])
    (proof
      [!an!sim< φ ψ]
      [infer imt x [∧ φ ψ] φ]
      [!an!sim> φ ψ]
      [infer imt x [∧ φ ψ] ψ]
      [jca [∃ x ∧ φ ψ] [∃ x φ] [∃ x ψ]]))
  (th-pattern !di-an)

  (th "di-an>" (var "x" wff ![φ ψ])
    (meta (:comment . "Existential quantification with a variable not free in the right conjunct.")
          (:description . "19.41 in set.mm. Theorem 19.41 of [Margaris] p. 90."))
    (hypo [Bvp x ψ])
    (ass [↔ ∃ x ∧ φ ψ ∧ ∃ x φ ψ])
    (proof
      [df-ex x [∧ φ ψ]]
      [Bvp x [¬ ψ]]
      [!al!di-or> x [¬ φ] [¬ ψ]]
      [↔ ¬ ∧ φ ψ ∨ ¬ φ ¬ ψ]
      [!al!eqti x [¬ ∧ φ ψ] [∨ ¬ φ ¬ ψ]]
      [↔ ¬ ∧ ∃ x φ ψ ∨ ¬ ∃ x φ ¬ ψ]
      [↔ ∀ x ¬ φ ¬ ∃ x φ]
      [infer !or!eqt< [∀ x ¬ φ] [¬ ∃ x φ] [¬ ψ]]
      [b2i bitr [¬ ∧ ∃ x φ ψ] [∨ ¬ ∃ x φ ¬ ψ] [∨ ∀ x ¬ φ ¬ ψ]]
      [<bitr [∀ x ∨ ¬ φ ¬ ψ] [∨ ∀ x ¬ φ ¬ ψ]
             [∀ x ¬ ∧ φ ψ] [¬ ∧ ∃ x φ ψ]]
      [conb>i [∀ x ¬ ∧ φ ψ] [∧ ∃ x φ ψ]]
      [b2i bitr [∃ x ∧ φ ψ] [¬ ∀ x ¬ ∧ φ ψ] [∧ ∃ x φ ψ]]))
  (th-pattern !di-an>)

  (th "di-an<" (var "x" wff ![φ ψ])
    (meta (:comment . "Existential quantification with a variable not free in the left conjunct.")
          (:description . "19.42 in set.mm. Theorem 19.42 of [Margaris] p. 90."))
    (hypo [Bvp x φ])
    (ass [↔ ∃ x ∧ φ ψ ∧ φ ∃ x ψ])
    (proof
      [di-an> x ψ φ]
      [!an!com φ ψ]
      [infer eqt [x] [∧ φ ψ] [∧ ψ φ]]
      [!an!com [φ] [∃ x ψ]]
      [<bitr [∃ x ∧ ψ φ] [∧ ∃ x ψ φ]
             [∃ x ∧ φ ψ] [∧ φ ∃ x ψ]]))
  (th-pattern !di-an<))

(in-context !al
  (th "rot4" (var ![x y z w] wff "φ")
    (meta (:comment . "Rotate 4 universal quantifiers twice.")
          (:description . "From set.mm."))
    (ass [↔ ∀ x ∀ y ∀ z ∀ w φ ∀ z ∀ w ∀ x ∀ y φ])
    (proof
      [com y z [∀ w φ]]
      [com y w φ]
      [eqti z [∀ y ∀ w φ] [∀ w ∀ y φ]]
      [bitr [∀ y ∀ z ∀ w φ] [∀ z ∀ y ∀ w φ] [∀ z ∀ w ∀ y φ]]
      [eqti x [∀ y ∀ z ∀ w φ] [∀ z ∀ w ∀ y φ]]
      [com x z [∀ w ∀ y φ]]
      [com x w [∀ y φ]]
      [eqti z [∀ x ∀ w ∀ y φ] [∀ w ∀ x ∀ y φ]]
      [2bitr [∀ x ∀ y ∀ z ∀ w φ] [∀ x ∀ z ∀ w ∀ y φ]
             [∀ z ∀ x ∀ w ∀ y φ] [∀ z ∀ w ∀ x ∀ y φ]])))

(in-context !ex
  (th "com13" (var ![x y z] wff "φ")
    (meta (:comment . "Swap 1st and 3rd existential quantifiers.")
          (:description . "From set.mm."))
    (ass [↔ ∃ x ∃ y ∃ z φ ∃ z ∃ y ∃ x φ])
    (proof
      [com x y [∃ z φ]]
      [com x z φ]
      [infer eqt y [∃ x ∃ z φ] [∃ z ∃ x φ]]
      [com y z [∃ x φ]]
      [2bitr [∃ x ∃ y ∃ z φ] [∃ y ∃ x ∃ z φ] [∃ y ∃ z ∃ x φ] [∃ z ∃ y ∃ x φ]]))

  (th "rot3" (var ![x y z] wff "φ")
    (meta (:comment . "Rotate existential quantifiers.")
          (:description . "From set.mm."))
    (ass [↔ ∃ x ∃ y ∃ z φ ∃ y ∃ z ∃ x φ])
    (proof
      [com13 x y z φ]
      [com z y [∃ x φ]]
      [bitr [∃ x ∃ y ∃ z φ] [∃ z ∃ y ∃ x φ] [∃ y ∃ z ∃ x φ]]))

  (th "rot4" (var ![x y z w] wff "φ")
    (meta (:comment . "Rotate existential quantifiers twice.")
          (:description . "From set.mm."))
    (ass [↔ ∃ x ∃ y ∃ z ∃ w φ ∃ z ∃ w ∃ x ∃ y φ])
    (proof
      [com13 y z w φ]
      [infer eqt x [∃ y ∃ z ∃ w φ] [∃ w ∃ z ∃ y φ]]
      [com13 x w z [∃ y φ]]
      [bitr [∃ x ∃ y ∃ z ∃ w φ] [∃ x ∃ w ∃ z ∃ y φ] [∃ z ∃ w ∃ x ∃ y φ]])) )

(th "naxgen" (var "x" wff "φ")
    (meta (:comment . "Generalization rule for negated wff.")
          (:description . "nex in set.mm."))
    (hypo [¬ φ])
    (ass [¬ ∃ x φ])
    (proof
      [ax-gen x [¬ φ]]
      [↔ ∀ x ¬ φ ¬ ∃ x φ]
      [b2i ax-mp [∀ x ¬ φ] [¬ ∃ x φ]]))

(th "naxgend" (var "x" wff ![φ ψ])
    (meta (:comment . "Deduction for generalization rule for negated wff.")
          (:description . "nexd in set.mm."))
    (hypo [Bvp x φ] [→ φ ¬ ψ])
    (ass [→ φ ¬ ∃ x ψ])
    (proof
      [imgen>i x φ [¬ ψ]]
      [↔ ∀ x ¬ ψ ¬ ∃ x ψ]
      [b2i syl φ [∀ x ¬ ψ] [¬ ∃ x ψ]]))

(in-context !bv
  (th "imt" (var "x" wff ![φ ψ])
    (meta (:comment . "A closed form of !bv!im.")
          (:description . "hbim1 in set.mm."))
    (hypo [Bvp x φ] [→ φ → ψ ∀ x ψ])
    (ass [Bvp x → φ ψ])
    (proof
      [infer ax2 φ ψ [∀ x ψ]]
      [imgen> x φ ψ]
      [b2i syl [→ φ ψ] [→ φ ∀ x ψ] [∀ x → φ ψ]]
      [Bvp x → φ ψ])))

(in-context !al
  (th "eqtd" (var "x" wff ![φ ψ χ])
    (meta (:comment . "Formula-building rule for universal quantifier (deduction rule).")
          (:description . "albid in set.mm."))
    (hypo [Bvp x φ] [→ φ ↔ ψ χ])
    (ass [→ φ ↔ ∀ x ψ ∀ x χ])
    (proof
      [imgen>i x φ [↔ ψ χ]]
      [eqt x ψ χ]
      [syl φ [∀ x ↔ ψ χ] [↔ ∀ x ψ ∀ x χ]])))

(in-context !ex
  (th "eqtd" (var "x" wff ![φ ψ χ])
    (meta (:comment . "Formula-building rule for existential quantifier (deduction rule).")
          (:description . "exbid in set.mm."))
    (hypo [Bvp x φ] [→ φ ↔ ψ χ])
    (ass [→ φ ↔ ∃ x ψ ∃ x χ])
    (proof
      [imgen>i x φ [↔ ψ χ]]
      [eqt x ψ χ]
      [syl φ [∀ x ↔ ψ χ] [↔ ∃ x ψ ∃ x χ]])))

(th "exan-pre" (var "x" wff ![φ ψ])
    (meta (:comment . "Place a conjunct in the scope of an existential quantifier.")
          (:description . "exan in set.mm."))
    (hypo [∧ ∃ x φ ψ])
    (ass [∃ x ∧ φ ψ])
    (proof
      [Bvp x [∃ x φ]]
      [!al!di-an> x ψ [∃ x φ]]
      [!an!com [∃ x φ] ψ]
      [b2i ax-mp [∧ ∃ x φ ψ] [∧ ψ ∃ x φ]]
      [ax-gen x [∧ ψ ∃ x φ]]
      [b2i ax-mp [∀ x ∧ ψ ∃ x φ] [∧ ∀ x ψ ∃ x φ]]
      [!ex!di-an>r x ψ φ]
      [ax-mp [∧ ∀ x ψ ∃ x φ] [∃ x ∧ ψ φ]]
      [!an!com ψ φ]
      [infer !ex!eqt x [∧ ψ φ] [∧ φ ψ]]
      [b2i ax-mp [∃ x ∧ ψ φ] [∃ x ∧ φ ψ]]))

(in-context !al
  (th "dfbi" (var "x" wff ![φ ψ])
    (meta (:comment . "Split a biconditional and distribute quantifier.")
          (:description . "albi in set.mm."))
    (ass [↔ ∀ x ↔ φ ψ ∧ ∀ x → φ ψ ∀ x → ψ φ])
    (proof
      [!bi!df.an φ ψ]
      [eqti x [↔ φ ψ] [∧ → φ ψ → ψ φ]]
      [di-an x [→ φ ψ] [→ ψ φ]]
      [bitr [∀ x ↔ φ ψ] [∀ x ∧ → φ ψ → ψ φ] [∧ ∀ x → φ ψ ∀ x → ψ φ]]))

  (th "2dfbi" (var ![x y] wff ![φ ψ])
    (meta (:comment . "Split a biconditional and distribute 2 quantifiers.")
          (:description . "2albi in set.mm."))
    (ass [↔ ∀ x ∀ y ↔ φ ψ ∧ ∀ x ∀ y → φ ψ ∀ x ∀ y → ψ φ])
    (proof
      [dfbi y φ ψ]
      [eqti x [∀ y ↔ φ ψ] [∧ ∀ y → φ ψ ∀ y → ψ φ]]
      [di-an x [∀ y → φ ψ] [∀ y → ψ φ]]
      [bitr [∀ x ∀ y ↔ φ ψ] [∀ x ∧ ∀ y → φ ψ ∀ y → ψ φ] [∧ ∀ x ∀ y → φ ψ ∀ x ∀ y → ψ φ]])))

(in-context !bv
  (th "negd" (var "x" wff ![φ ψ])
    (meta (:comment . "A deduction form of bound-variable hypothesis builder !bv!neg.")
          (:description . "hbnd in set.mm."))
    (hypo [Bvp x φ] [→ φ Bvp x ψ])
    (ass [→ φ Bvp x ¬ ψ])
    (proof
      [negt x ψ]
      [syl φ [Bvp x ψ] [Bvp x ¬ ψ]]))

  (th "imd" (var "x" wff ![φ ψ χ])
    (meta (:comment . "Deduction form of bound-variable hypothesis builder !bv!im.")
          (:description . "hbimd in set.mm."))
    (hypo [Bvp x φ] [→ φ Bvp x ψ] [→ φ Bvp x χ])
    (ass [→ φ Bvp x → ψ χ])
    (proof
      [negd x φ ψ]
      [ded df-Bvp x [¬ ψ] φ]
      [ded ax4 x [→ ¬ ψ ∀ x ¬ ψ] φ]
      [com12i φ [¬ ψ] [∀ x ¬ ψ]]
      [<neg ψ χ]
      [!/pred!al!imti x [¬ ψ] [→ ψ χ]]
      [!/pred!im!rp33 [¬ ψ] [φ] [∀ x ¬ ψ] [∀ x → ψ χ]]
      ; --
      [ded df-Bvp x χ φ]
      [ded ax4 x [→ χ ∀ x χ] φ]
      [com12i φ χ [∀ x χ]]
      [ax1 χ ψ]
      [!/pred!al!imti x χ [→ ψ χ]]
      [!/pred!im!rp33 χ φ [∀ x χ] [∀ x → ψ χ]]
      [!/pred!im!ja ψ χ [→ φ ∀ x → ψ χ]]
      [com12i [→ ψ χ] [φ] [∀ x → ψ χ]]
      [imgen>i x φ [→ → ψ χ ∀ x → ψ χ]]
      [ded df-Bvp x [→ ψ χ] φ]))

  ;; TODO: convert to Bvp
  (th "and" (var "x" wff ![φ ψ χ])
    (meta (:comment . "Deduction form of bound-variable hypothesis builder !bv!an.")
          (:description . "hband in set.mm."))
    (hypo [→ φ → ψ ∀ x ψ] [→ φ → χ ∀ x χ])
    (ass [→ φ → ∧ ψ χ ∀ x ∧ ψ χ])
    (proof
      [!/pred!an!imtd φ ψ [∀ x ψ] [χ] [∀ x χ]]
      [!/pred!al!di-an x ψ χ]
      [b2i !/pred!im!rp33 φ [∧ ψ χ] [∧ ∀ x ψ ∀ x χ] [∀ x ∧ ψ χ]]))

  (th "bid" (var "x" wff ![φ ψ χ])
    (meta (:comment . "Deduction form of bound-variable hypothesis builder !bv!bi.")
          (:description . "hbbid in set.mm."))
    (hypo [Bvp x φ] [→ φ Bvp x ψ] [→ φ Bvp x χ])
    (ass [→ φ Bvp x ↔ ψ χ])
    (proof
      [imd x φ ψ χ]
      [imd x φ χ ψ]
      [ded df-Bvp x [→ ψ χ] φ]
      [ded df-Bvp x [→ χ ψ] φ]
      [ded ax4 x [→ [→ ψ χ] ∀ x [→ ψ χ]] φ]
      [ded ax4 x [→ [→ χ ψ] ∀ x [→ χ ψ]] φ]
      [!/pred!an!imtd φ [→ ψ χ] [∀ x → ψ χ] [→ χ ψ] [∀ x → χ ψ]]
      [↔ ↔ ψ χ ∧ → ψ χ → χ ψ]
      [!/pred!al!dfbi x ψ χ]
      [<imtrg [φ] [∧ → ψ χ → χ ψ] [∧ ∀ x → ψ χ ∀ x → χ ψ]
              [↔ ψ χ] [∀ x ↔ ψ χ]]
      [imgen>i x φ [→ ↔ ψ χ ∀ x ↔ ψ χ]]
      [ded df-Bvp x [↔ ψ χ] φ]))

  (th "syl" (var "x" wff ![φ ψ])
    (hypo [Bvp x φ] [→ ∀ x φ ψ])
    (ass [→ φ ψ])
    (proof
      [→ φ ∀ x φ]
      [syl φ ∀ x φ ψ]))

  (th "defd" (var "x" wff ![φ ψ χ])
    (meta (:comment . "Deduction applying bound-variable predicate to a biconditional."))
    (hypo [Bvp x φ] [Bvp x χ] [→ φ ↔ ψ χ])
    (ass [→ φ Bvp x ψ])
    (proof
      [!/pred!al!eqtd x φ ψ χ]
      [!/pred!im!eqtd φ ψ χ [∀ x ψ] [∀ x χ]]
      [ded !bi< [→ ψ ∀ x ψ] [→ χ ∀ x χ] φ]
      [→ χ ∀ x χ]
      [mpi φ [→ χ ∀ x χ] [→ ψ ∀ x ψ]]
      [imgen>i x φ [→ ψ ∀ x ψ]]
      [ded df-Bvp x ψ φ]
      ))

  (th "eqti" (var "x" wff ![φ ψ])
      (hypo [↔ φ ψ])
      (ass [↔ Bvp x φ Bvp x ψ])
      (proof
        [!/pred!al!eqti x φ ψ]
        [!/pred!im!eqti φ ψ [∀ x φ] [∀ x ψ]]
        [!/pred!al!eqti x [→ φ ∀ x φ] [→ ψ ∀ x ψ]]
        [df-Bvp x φ]
        [df-Bvp x ψ]
        [!/pred!bi!<tr [∀ x → φ ∀ x φ] [∀ x → ψ ∀ x ψ] [Bvp x φ] [Bvp x ψ]]))
  
  (th "ald" (var ![x y] wff ![φ ψ])
    (meta (:comment . "Deduction from bound-variable hypothesis builder !bv!al.")
          (:description . "hbald in set.mm."))
    (hypo [Bvp x φ] [Bvp y φ] [→ φ Bvp x ψ])  ; [→ φ → ψ ∀ x ψ]
    (ass [→ φ Bvp x ∀ y ψ])       ; ∀ y ψ ∀ x ∀ y ψ]
    (proof
      [ded df-Bvp x ψ φ]
      [ded ax4 x [→ ψ ∀ x ψ] φ]
      [!/pred!al!imtd y φ ψ [∀ x ψ]]
      [ax7 y x ψ]
      [!/pred!im!rp33 φ [∀ y ψ] [∀ y ∀ x ψ] [∀ x ∀ y ψ]]
      [imgen>i x φ [→ ∀ y ψ ∀ x ∀ y ψ]]
      [ded df-Bvp x [∀ y ψ] φ]))

  (th "exd" (var ![x y] wff ![φ ψ])
    (meta (:comment . "Deduction from bound-variable hypothesis builder !bv!ex.")
          (:description . "hbexd in set.mm."))
    (hypo [Bvp x φ] [Bvp y φ] [→ φ Bvp x ψ]) ; [→ ψ ∀ x ψ]
    (ass [→ φ [Bvp x [∃ y ψ]]])    ; [→ ∃ y ψ ∀ x ∃ y ψ]
    (proof
      [negd x φ ψ]
      [ald x y φ [¬ ψ]]
      [negd x φ [∀ y ¬ ψ]]
      [df-ex y ψ]
      [eqti x [∃ y ψ] [¬ ∀ y ¬ ψ]]
      [b2i !/pred!syl φ [Bvp x ¬ ∀ y ¬ ψ] [Bvp x ∃ y ψ]])) )
#|
(th "imgen>g" (var "x" wff ![φ ψ])
    (meta (:comment . "Closed form of Theorem 19.21 of [Margaris] p. 90.")
          (:description . "19.21g in set.mm."))
    (ass [→ ∀ x → φ ∀ x φ ↔ ∀ x → φ ψ → φ ∀ x ψ])
    (proof
      [!al!di-im x φ ψ]
      [ded syl< [∀ x φ] [∀ x ψ] [φ] [∀ x → φ ψ]]
      [com12i [∀ x → φ ψ] [→ φ ∀ x φ] [→ φ ∀ x ψ]]
      [+syl ax4 x [→ φ ∀ x φ] [→ ∀ x → φ ψ → φ ∀ x ψ]]
      [!bv!al-b x [→ φ ∀ x φ]]
      [ax4 x [→ φ ∀ x φ]]
      [Bvp x [∀ x ψ]]
;      [!bv!fv-alx x ψ]
      [(infer !ax1) [→ ∀ x ψ ∀ x ∀ x ψ] [∀ x → φ ∀ x φ]]
      [!bv!imd x [∀ x → φ ∀ x φ] [φ] [∀ x ψ]]
      [ax4 x ψ]
      [infer !syl< [∀ x ψ] ψ φ]
      [!al!imti x [→ φ ∀ x ψ] [→ φ ψ]]
      [!im!rp33 [∀ x → φ ∀ x φ] [→ φ ∀ x ψ] [∀ x → φ ∀ x ψ] [∀ x → φ ψ]]
      [>bid [∀ x → φ ∀ x φ] [∀ x → φ ψ] [→ φ ∀ x ψ]]))
|#
(th "exintr" (var "x" wff ![φ ψ])
    (meta (:comment . "Introduce a conjunct in the scope of an existential quantifier.")
          (:description . "From set.mm."))
    (ass [→ ∀ x → φ ψ → ∃ x φ ∃ x ∧ φ ψ])
    (proof
      [!bv!al-b x [→ φ ψ]]
      [!an!join φ ψ]
      [+syl ax4 x [→ φ ψ] [→ φ ∧ φ ψ]]
      [!ex!imtd x [∀ x → φ ψ] [φ] [∧ φ ψ]]))

(in-context !al
  (th "arrange" (var ![x y] wff ![φ ψ])
    (meta (:comment . "Rearrange universal quantifiers.")
          (:description . "aaan in set.mm."))
    (hypo [Bvp y φ] [Bvp x ψ])
    (ass [↔ ∀ x ∀ y ∧ φ ψ ∧ ∀ x φ ∀ y ψ])
    (proof
      [!al!di-an< y φ ψ]
      [!al!eqti x [∀ y ∧ φ ψ] [∧ φ ∀ y ψ]]
      [Bvp x [∀ y ψ]]
      [!al!di-an> x φ [∀ y ψ]]
      [bitr [∀ x ∀ y ∧ φ ψ] [∀ x ∧ φ ∀ y ψ] [∧ ∀ x φ ∀ y ψ]])))

(in-context !ex
  (th "arrange" (var ![x y] wff ![φ ψ])
    (meta (:comment . "Rearrange existential quantifiers.")
          (:description . "eeor in set.mm."))
    (hypo [Bvp y φ] [Bvp x ψ])
    (ass [↔ ∃ x ∃ y ∨ φ ψ ∨ ∃ x φ ∃ y ψ])
    (proof
      [!ex!di-or< y φ ψ]
      [infer !ex!eqt x [∃ y ∨ φ ψ] [∨ φ ∃ y ψ]]
      [Bvp x [∃ y ψ]]
      [!ex!di-or> x φ [∃ y ψ]]
      [bitr [∃ x ∃ y ∨ φ ψ] [∃ x ∨ φ ∃ y ψ] [∨ ∃ x φ ∃ y ψ]])))

(th "qexmid" (var "x" wff "φ")
    (meta (:comment . "Quantified \"excluded middle\".")
          (:description . "From set.mm. Exercises 9.2a of Boolos, p. 111, Computability and Logic."))
    (ass [∃ x → φ ∀ x φ])
    (proof
      [→ ∀ x φ ∃ x ∀ x φ]
      [infer [↔ ∃ x [→ φ ∀ x φ] [→ ∀ x φ ∃ x ∀ x φ]]]))


(in-context !ex
  (th "eqtd" (var "x" wff ![φ ψ χ])
    (meta (:comment . "Deduction adding existential quantifier to both sides of a biconditional."))
    (hypo [Bvp x φ] [→ φ ↔ ψ χ])
    (ass [→ φ ↔ ∃ x ψ ∃ x χ])
    (proof
      [→ φ ∀ x φ]
      [!al!imti x φ [↔ ψ χ]]
      [eqt x ψ χ]
      [3syl φ [∀ x φ] [∀ x ↔ ψ χ] [↔ ∃ x ψ ∃ x χ]])))

) ; end context "pred"
