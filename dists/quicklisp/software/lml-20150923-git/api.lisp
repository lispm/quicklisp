;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          api.lisp
;;;; Purpose:       Macros for generating API documentation
;;;; Programmer:    Kevin M. Rosenberg based on Matthew Danish's code
;;;; Date Started:  Nov 2002
;;;;
;;;; $Id$
;;;;
;;;; This file, part of LML, is Copyright (c) 2002 by Kevin M. Rosenberg
;;;; and Copyright (c) 2002 Matthew Danish
;;;;
;;;; LML users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License v2
;;;; (http://www.gnu.org/licenses/gpl.html)
;;;; *************************************************************************

(in-package #:lml)

;;; Copyright (c) 2002 Matthew Danish.
;;; All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:
;;; 1. Redistributions of source code must retain the above copyright
;;;    notice, this list of conditions and the following disclaimer.
;;; 2. Redistributions in binary form must reproduce the above copyright
;;;    notice, this list of conditions and the following disclaimer in the
;;;    documentation and/or other materials provided with the distribution.
;;; 3. The name of the author may not be used to endorse or promote products
;;;    derived from this software without specific prior written permission.
;;;
;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
;;; IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
;;; OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
;;; IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
;;; INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
;;; NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
;;; THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; For an example, see Matthew Danish's cl-ftp documentation at
;;; http://www.mapcar.org/~mrd/cl-sql/

(defmacro api-list (&body body)
  `(ul ,@(loop for item in body collect `(li ,item))))

(defun stringify (x)
  (let ((*print-case* :downcase))
    (if (null x)
        "()"
        (format nil "~A" x))))

(defmacro with-class-info ((class-name superclasses &rest slot-docs) &body other-info)
  `(p (i "Class ") (b ,(stringify class-name))
    (i " derived from ") ,(stringify superclasses) " -- " (br)
    (i "Initargs:") (br)
    (ul
     ,@(loop for (slot-name slot-desc slot-default) in slot-docs collect
             `(li (tt ,(format nil ":~A" slot-name))
               " -- " ,slot-desc " -- " (i "Default: ")
               ,(if (eql slot-default :n/a)
                    "Not specified"
                    (format nil "~S" slot-default)))))
    ,@other-info))

(defmacro with-macro-info ((macro-name &rest lambda-list) &body other-info)
  `(p (i "Macro ") (b ,(stringify macro-name)) " "
    (tt ,(stringify lambda-list)) (br)
    ,@other-info))

(defmacro with-function-info ((function-name &rest lambda-list) &body other-info)
  `(p (i "Function ") (b ,(stringify function-name)) " "
    (tt ,(stringify lambda-list))
    (br) ,@other-info))

(defmacro with-condition-info ((condition-name supers &rest slot-docs) &body other-info)
  `(p (i "Condition ") (b ,(stringify condition-name))
    (i " derived from ") ,(stringify supers) " -- " (br)
    (i "Slots:") (br)
    (ul
     ,@(loop for (slot-name slot-desc slot-reader slot-initarg slot-default) in slot-docs collect
             `(li (tt ,(stringify slot-name))
               " -- " ,slot-desc " -- " (i " Default: ")
               ,(if (eql slot-default :n/a)
                    "Not specified"
                    (format nil "~S" slot-default)))))
    ,@other-info))

(defmacro with-functions (&rest slots)
  `(progn ,@(loop for (fn description . args) in slots collect
                  `(with-function-info (,fn ,@(if args args
                                                  '(connection-variable)))
                    ,description))))
