# DO NOT EDIT --- generated from source code.
#+LaTeX: \label{add-failure:primary}
For use within user-defined NST criteria: add a failure to a result.
#+begin_example
(add-failure RESULT-REPORT [ :format FORMAT-STRING ] [ :args ARGUMENT-LIST ])
#+end_example

