;;;  clot.asd --- System description for the Cobol Storage library

;;;  Copyright (C) 2005 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: clot

#+cmu (ext:file-comment "$Module: clot.asd, Time-stamp: <2005-12-07 18:31:28 wcp> $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

#+nil
(error "The feature NIL has been defined.  This has been reserved to comment out sexps.")

#-(or cmu sbcl)
(warn "This code hasn't been tested on your Lisp system.")

(defpackage :clot-system
  (:use :common-lisp :asdf #+asdfa :asdfa)
  (:export #:*base-directory*
	   #:*compilation-epoch*))

(in-package :clot-system)

(defvar *base-directory*
  (make-pathname :name nil :type nil :version nil
                 :defaults (parse-namestring *load-truename*)))

(defparameter *compilation-epoch* (get-universal-time))

(defsystem clot
    :name "CLOT"
    :author "Walter C. Pelissero <walter@pelissero.de>"
    :maintainer "Walter C. Pelissero <walter@pelissero.de>"
    :description "Cheap Lisp plOTting library"
    :long-description
    "CLot is a simple and small package to plot data charts using CL-GD."
    :licence "LGPL"
    :depends-on (:sclf :cl-gd)
    :components
    ((:file "clot")))

