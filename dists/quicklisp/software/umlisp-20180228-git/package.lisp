;;;; -*- Mode: Lisp -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:     package.lisp
;;;; Purpose:  Package definition for UMLisp
;;;; Author:   Kevin M. Rosenberg
;;;; Created:  Apr 2000
;;;;
;;;; This file, part of UMLisp, is
;;;;    Copyright (c) 2000-2010 by Kevin M. Rosenberg, M.D.
;;;;
;;;; UMLisp users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License.
;;;; *************************************************************************

(in-package #:cl-user)

(eval-when (:compile-toplevel :load-toplevel :execute) ;; enclose reader macro
  (defpackage #:umlisp
    (:nicknames #:u)
    (:use #:kmrcl #:common-lisp #:hyperobject)
    (:export
     #:dummy
     .
     ;; From classes.lisp
     #1=(#:umlsclass
         #:ucon #:uterm #:ustr #:usrl #:uso #:urank #:udef #:usat #:usab
         #:urel
         #:usty #:uxw #:uxnw  #:uxns
         #:lexterm #:labr #:lagr #:lcmp #:lmod #:lnom #:lprn #:lprp #:lspl #:ltrm
         #:ltyp #:lwd #:sdef #:sstr #:sstre1 #:sstre2
         #:sty #:tui #:def #:sab #:srl #:tty #:rank #:suppress #:atn #:atv #:vcui
         #:rmeta #:imeta #:scit #:scc #:slc
         #:rcui #:vsab #:code #:saui #:scui #:sdui #:ispref
         #:rl #:sty2 #:ui #:ui2 #:ui3 #:eui #:bas #:eui2 #:bas2 #:rui
         #:cui #:aui #:lui #:sui #:wd #:lat #:nstr :cuilist
         #:rsab #:lat
         #:s#def #:s#sty #:s#term #:s#str #:s#lo #:s#sat #:s#rel
         #:s#so
         #:pfstr #:pfstr2 #:lrl #:def #:ts #:cui1 #:cui2 #:rela #:sl #:mg #:rel
         #:soc #:cot #:cof #:coa #:isn #:fr #:un #:sna #:soui #:hcd #:stt #:str
         #:kpfeng :cvf
         #:udoc #:dockey #:dvalue #:dtype #:expl

         ;; From class-support.lisp
         #:ucon-has-tui
         #:english-term-p #:remove-non-english-terms #:remove-english-terms
         #:fmt-eui #:fmt-aui
         #:display-con #:display-term #:display-str
         #:pfstr #:pf-ustr
         #:cui-p #:lui-p #:sui-p #:tui-p #:eui-p
         #:rel-abbr-info #:filter-urels-by-rel
         #:mesh-number #:ucon-ustrs
         #:lat-abbr-info #:stt-abbr-info
         #:uso-unique-codes #:ucon-has-sab

         ;; From sql.lisp
         #:*umls-sql-db*
         #:umls-sql-user
         #:set-umls-sql-user
         #:umls-sql-passwd
         #:set-umls-sql-passwd
         #:umls-sql-db
         #:set-umls-sql-db
         #:umls-sql-host
         #:set-umls-sql-host
         #:umls-sql-type
         #:set-umls-sql-type
         #:with-sql-connection
         #:with-sql-connection
         #:sql-disconnect
         #:sql-disconnect-pooled

         ;; From utils.lisp
         #:fmt-cui
         #:fmt-lui
         #:fmt-sui
         #:fmt-tui
         #:find-uterm-in-ucon
         #:find-ustr-in-uterm
         #:find-ustr-in-ucon
         #:*current-srl*
         #:parse-cui #:parse-lui #:parse-sui #:parse-tui #:parse-eui

         ;; From sql-classes.lisp
         #:current-srl
         #:set-current-srl
         #:find-udef-cui
         #:find-usty-cui
         #:find-usty-word
         #:find-urel-cui
         #:find-cui2-urel-cui
         #:find-urel-cui2
         #:find-ucon-rel-cui2
         #:find-usty-sty
         #:suistr
         #:print-umlsclass
         #:find-ucon-cui #:make-ucon-cui
         #:find-ucon-aui
         #:find-uconso-cui
         #:find-uconso-sui
         #:find-uconso-cuisui
         #:find-uconso-word
         #:find-uconso-code
         #:find-ucon-lui
         #:find-ucon-sui
         #:find-ucon-cui-sui
         #:find-ucon-cuisui
         #:find-ucon-str
         #:find-ucon-all
         #:find-cui-ucon-all
         #:map-ucon-all
         #:find-uterm-cui
         #:find-uterm-lui
         #:find-uterm-cuilui
         #:find-uterm-in-ucon
         #:find-ustr-cuilui
         #:find-ustr-cuisui
         #:find-ustr-cui-sui
         #:find-ustr-sui
         #:find-ustr-sab
         #:find-ustr-all
         #:find-string-sui
         #:find-udoc-key
         #:find-udoc-value
         #:find-udoc-key-value
         #:find-uso-cuisui
         #:find-uso-cui
         #:find-uso-aui
         #:find-usat-ui
         #:find-usab-all
         #:find-usab-rsab
         #:find-usab-vsab
         #:find-pfstr-cui
         #:find-ustr-in-uterm
         #:find-usty-tui
         #:find-usty-all
         #:find-usty_freq-all
         #:find-usrl-all
         #:find-usrl_freq-all
         #:find-cui-max
         #:find-ucon-tui
         #:find-ucon-word
         #:find-ucon-normalized-word
         #:find-cui-normalized-word
         #:find-lui-normalized-word
         #:find-sui-normalized-word
         #:find-ustr-word
         #:find-ustr-normalized-word
         #:find-uterm-multiword
         #:find-uterm-word
         #:find-uterm-normalized-word
         #:find-ucon-multiword
         #:find-uconso-multiword
         #:find-ucon-normalized-multiword
         #:find-ustr-multiword
         #:find-ustr-normalized-multiword
         #:find-lexterm-eui
         #:find-lexterm-word
         #:find-labr-eui
         #:find-labr-bas
         #:find-lagr-eui
         #:find-lcmp-eui
         #:find-lmod-eui
         #:find-lnom-eui
         #:find-lprn-eui
         #:find-lprp-eui
         #:find-lspl-eui
         #:find-ltrm-eui
         #:find-ltyp-eui
         #:find-lwd-wrd
         #:find-sdef-ui
         #:find-sstre1-ui
         #:find-sstre1-ui2
         #:find-sstr2-sty
         #:find-sstr-rl
         #:find-sstr-styrl
         #:display-con
         #:display-term
         #:display-str
         #:find-ustats-all
         #:find-ustats-srl
         #:find-bsab-sab
         #:find-bsab-all
         #:find-btty-all
         #:find-btty-tty
         #:find-brel-rel

         ;; composite.lisp
         #:tui-finding
         #:tui-sign-or-symptom
         #:tui-disease-or-syndrome
         #:ucon-is-tui?
         #:find-ucon2-tui
         #:find-ucon2-rel-tui
         #:find-ucon2-str&sty
         #:find-ucon2-rel-str&sty
         #:find-ucon2_freq-tui-all
         #:find-ucon2_freq-rel-tui-all
         #:ucon_freq
         #:ustr_freq
         #:usty_freq
         #:usrl_freq

         ;; from data-structures.lisp
         #:umls-path
         #:set-umls-path

         ;; Removed features
         ;; MRCOC
         ;; #:ucoc #:s#coc #:find-ucoc-cui #:find-ucoc-cui2 #:find-ucon-coc-cui2 #:find-ucon2-coc-tui #:find-ucon2_freq-coc-tui #:find-ucon2-coc-str&sty #:find-ucon2_freq-coc-tui-all

         )))

  (defpackage umlisp-user
    (:use  #:kmrcl #:common-lisp #:hyperobject)
    (:import-from :umlisp . #1#)
    (:export . #1#)
    (:documentation "User package for UMLisp")))
