;;; Sample file for Bourbaki version 3.7

;; Usage:
;;
;; load the file:
;; > (load "doc/example1.lisp")
;;
;; verify the theorem:
;; > (verify !id)
;;
;; show information about the theorem:
;; > (print-theorem !id)
;; > (show-proof !id)

;; Declare the wff type
(symkind "WFF")

;; The implication symbol
(prim wff "->" (wff ![x y]))

;; the axioms
(ax "ax1" (wff ![A B])
  (ass [-> A -> B A]))
(ax "ax2" (wff ![A B C])
  (ass [-> -> A -> B C -> -> A B -> A C]))

;; the rule of inference (modus ponens)
(ax "ax-mp" (wff ![A B])
  (hypo [A] [-> A B])
  (ass [B]))

;; theorem: identity law for '->'
;; compare with id1 in set.mm
(th "id" (wff "A")
  (ass [-> A A])
  (proof
    [ax1 A [-> A A]]
    [ax2 A [-> A A] A]
    [ax-mp [-> A -> -> A A A]
           [-> -> A -> A A -> A A]]
    [ax1 A A]
    [ax-mp [-> A -> A A] [-> A A]]))
