#|  MINI-CAS, a MINImal Computer Algebra System

    Copyright (c) 2011 Daniel Herring

    Distributed under the Boost Software License, Version 1.0.
       (See accompanying file LICENSE_1_0.txt or copy at
             http://www.boost.org/LICENSE_1_0.txt)
|#

(defsystem :mini-cas
  :components
  ((:file "mini-cas")
   (:file "basic-ops")
   (:file "full-derivative")
   (:file "simplify")
   (:file "arrays")
   (:file "substitute")
   (:file "test"))
  :serial t)
