(in-package #:simpsamp)

(defun hash-table-sample-of-hash-table (n hash-table)
  (declare (type jpl-util:array-dimension n)
	   (type hash-table hash-table)
	   (optimize speed))
  (with-hash-table-accumulator (set-test/size accumulate)
    (do-sample-of-hash-table (key value n hash-table
			      :effective-n-var n
			      :initial-form (set-test/size
					     (hash-table-test hash-table) n))
      (accumulate key value))))
