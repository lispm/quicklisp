(defpackage :agnostic-lizard
  (:use :common-lisp)
  (:export
    #:metaenv
    #:walker-metaenv
    #:macro-walker-metaenv

    #:metaenv-macroexpand-all

    #:metaenv-function-like-entries
    #:metaenv-variable-like-entries
    #:metaenv-blocks
    #:metaenv-tags

    #:metaenv-ensure-name-from-environment
    #:metaenv-ensure-names-from-environment

    #:macroexpand-all
    #:walk-form

    #:macro-walk-form
    #:macro-macroexpand-all

    ; helpers for reader tricks
    #:wrap-every-form-reader
    #:install-wrap-every-form-reader
    #:wrap-rest-of-input
    #:with-wrap-every-form-reader
    ))
