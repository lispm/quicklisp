;; Cl-pslib-barcode example
;; Copyright (C) 2012  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :cl-pslib-barcode)

(defparameter *output* "pslib-barcode.ps")

(defparameter *page-size* ps:+a4-page-size+)

(defparameter *code* "$START-Bbarcode")

(let* ((doc (make-instance 'ps:psdoc :page-size *page-size*))
       (barcode (make-instance 'code128)))
  (brcd:parse barcode *code*)
  (ps:open-doc doc  *output*)
  (ps:begin-page doc)
  (ps:save doc)

  (ps:save doc)
  (ps:setcolor doc ps:+color-type-fillstroke+ cl-colors:+red+)
  (ps:rect doc 0 0 (ps:width (ps:page-size doc))
	   (ps:height (ps:page-size doc)))
  (ps:fill-path doc)
  (ps:restore doc)

  (ps:translate doc (- (/ (ps:width (ps:page-size doc)) 2) (/ (brcd:width barcode) 2))
    		(/ (ps:height (ps:page-size doc)) 2))

  (brcd:draw barcode doc)

  (ps:restore doc)

  (ps:end-page doc)
  (ps:close-doc doc)
  (ps:shutdown))


