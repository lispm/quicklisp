;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defsystem :levenshtein
  :components ((:file "package")
               (:file "levenshtein" :depends-on ("package"))))
