;;; Copyright (c) Juha Arpiainen 2005-06

;;; This file is part of Bourbaki.

;;; Bourbaki is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.

;;; Bourbaki is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Bourbaki; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; -------

;;; *****
;;; Declaring and checking Distinct Variable Conditions (DVCs).
;;; *****


(in-package :bourbaki)

;;; Add one dvc to context thr
(defun dvc-1 (cnd thr)
  (destructuring-bind (x . y) cnd
    (if (and (not (eq x y))
             (eq (context-class x) :arg)            
             (eq (context-class y) :arg))
      (setf (context-distinct thr) (adjoin cnd (context-distinct thr) :test #'pair-eq)))))

;;; Add conditions (x . y) for all x in list1, y in list2
(defun dvc (list1 list2 thr)
  (let ((product (cartesian-product list1 list2)))
    (dolist (cnd product)
      (dvc-1 cnd thr))))

;;; Add conditions (x . y) for all x, y in lst
(defun dvc-list (lst thr)
  (let ((symlist (mapcar #'(lambda (x) (mkcontext x)) lst)))
    (dvc symlist symlist thr)))

;;; Make one variable distinct from all the others
(defun dvc-var (var thr)
  (dvc (list (mkcontext var)) (context-vars thr) thr))


;;; For all distinct variable conditions (var1 . var2) in thref,
;;; get the variables that occur in their substitutions (vars1, vars2).
;;; Then check that (x . y) is in condlist for all x in vars1, y in vars2.
(defun dvc-satisfied (thref smap condlist varlist)
  "Check that distinct variable conditions are satisfied in a proof step."
  (dolist (cnd (context-distinct thref) t)
    (let ((exp1 (cdr (assoc (car cnd) smap)))
          (exp2 (cdr (assoc (cdr cnd) smap))))
      (let ((vars1 (collect-vars exp1 varlist))
            (vars2 (collect-vars exp2 varlist)))
        (dolist (cnd2 (cartesian-product vars1 vars2))
          (if (eq (car cnd2) (cdr cnd2))
            (error "DVC (~A . ~A) for ~A not satisfied: ~A occurs in both"
                   (context-name (car cnd))
                   (context-name (cdr cnd))
                   (context-name thref)
                   (context-name (car cnd2))))
          (unless (or (eq (context-class (car cnd2)) :loc) ; local variables are implicitly distinct
                      (eq (context-class (cdr cnd2)) :loc)
                      (member cnd2 condlist :test #'pair-eq))
            (error "DVC (~A . ~A) for ~A not satisfied: (~A . ~A) missing"
                   (context-name (car cnd))
                   (context-name (cdr cnd))
                   (context-name thref)
                   (context-name (car cnd2))
                   (context-name (cdr cnd2)))))))))
