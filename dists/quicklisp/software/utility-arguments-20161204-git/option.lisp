;;;; option.lisp

;;; Copyright (C) 2016, Frank A. U. <fau@riseup.net>
;;;
;;; Permission to use, copy, modify, and/or distribute this software for any
;;; purpose with or without fee is hereby granted, provided that the above
;;; copyright notice and this permission notice appear in all copies.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


(in-package :utility-arguments)


(defclass option ()
  ((key   :initform nil :initarg :key)
   (short :initform nil :initarg :short)
   (long  :initform nil :initarg :long)
   (type  :initform nil :initarg :type)))


(defclass option-flag (option) ())
(defclass option-with-argument (option) ())
(defclass option-with-optional-argument (option) ())
(defclass option-with-sequence (option) ())
(defclass option-with-optional-sequence (option) ())


(defmethod initialize-instance :after ((self option) &key key short long)
  (check-type key (and (not (eql nil)) symbol))

  (when (not (or short long))
    (error "Option has neither short nor long designator."))
  (when short
    (check-type short character))
  (when long
    (check-type long string)))


(defun make-option-flag (&key key short long)
  "Make a new option flag.

Its number of appearances on a utility argument list is the assumed
value.

The parameters KEY, SHORT, and LONG are documented in
make-option-with-argument."
  (make-instance 'option-flag :key key :short short :long long))
(export 'make-option-flag)


(defun make-option-with-argument (&key key short long type)
  "Make a new option that receives one argument.

KEY is a symbol that correlates the option with a keyword parameter on
a usage lambda list.

SHORT is a case-sensitive character.  LONG is a case-insensitive but
case-preserving string.  Both designators are equivalent and request
the option on a utility argument list.  At least one of the two must
be specified.

TYPE is a Lisp type specifier the object must conform to the Lisp
reader builds from the received argument.  The built object becomes
the assumed value.

TYPE is a function of two string arguments: the first of which is the
received argument for the option, and the second the designator that
requested the option.  The object returned becomes the assumed value.

TYPE is nil, the received argument is the assumed value."
  (make-instance 'option-with-argument :key key :short short :long long :type type))
(export 'make-option-with-argument)


(defun make-option-with-optional-argument (&key key short long type)
  "Make a new option that optionally receives one argument.

If the option doesn't receive an argument, nil is assumed.

The parameters KEY, SHORT, LONG, and TYPE are documented in
make-option-with-argument."
  (make-instance 'option-with-optional-argument :key key :short short :long long :type type))
(export 'make-option-with-optional-argument)


(defun make-option-with-sequence (&key key short long type)
  "Make a new option that receives a sequence of arguments.

Multiple arguments in a sequence are separated from each other by the
separator registered with *separator*.

Received arguments are subject to type parsing as described in
make-option-with-argument.  The assumed values are collected into a
list by order of utility arguments.

The parameters KEY, SHORT, LONG, and TYPE are documented in
make-option-with-argument."
  (make-instance 'option-with-sequence :key key :short short :long long :type type))
(export 'make-option-with-sequence)


(defun make-option-with-optional-sequence (&key key short long type)
  "Make a new option that optionally receives a sequence of arguments.

If the option doesn't receive an argument, nil is assumed.  See also
make-option-with-sequence.

The parameters KEY, SHORT, LONG, and TYPE are documented in
make-option-with-argument."
  (make-instance 'option-with-optional-sequence :key key :short short :long long :type type))
(export 'make-option-with-optional-sequence)


(defgeneric find-option (item options)
  (:documentation "Find option identifiable by ITEM.

ITEM is either a symbol, character, or a string.  OPTIONS is a list of
option objects."))
(export 'find-option)


(defmethod find-option ((key symbol) options)
  (or (find-if (lambda (o)
                 (eq (slot-value o 'key) key))
               options)
      (no-such-option key)))


(defmethod find-option ((short character) options)
  (or (find-if (lambda (o)
                 (equal (slot-value o 'short) short))
               options)
      (no-such-option short)))


(defmethod find-option ((long string) options)
  (or (find-if (lambda (o)
                 (string-equal (slot-value o 'long) long))
               options)
      (no-such-option long)))


(defun parse-option-argument (string option designator)
  (with-slots (type) option
    (if (functionp type)
        (funcall type string designator)

        (case type
          ((nil string) string)

          (t (multiple-value-bind (obj pos)
                 (handler-case
                     (uiop:safe-read-from-string string :package *package*)
                   (error ()
                     (invalid-option-argument designator string type)))

               (when (or (< pos (length string)) (not (typep obj type)))
                 (invalid-option-argument designator string type))
               obj))))))


;;;; option.lisp ends here
