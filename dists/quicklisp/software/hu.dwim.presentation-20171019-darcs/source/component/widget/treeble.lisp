;;; -*- mode: Lisp; Syntax: Common-Lisp; -*-
;;;
;;; Copyright (c) 2009 by the authors.
;;;
;;; See LICENCE for details.

(in-package :hu.dwim.presentation)

;;;;;;
;;; treeble/widget

(def (component e) treeble/widget (tree/component
                                   standard/widget
                                   root-nodes/mixin
                                   columns/mixin
                                   collapsible/mixin
                                   selection/mixin)
  (;; TODO expander-column-index should be marked by a special column type, or something similar. this way it's very fragile...
   (expander-column-index 0 :type integer)
   (expand-nodes-by-default #t :type boolean)))

(def (macro e) treeble/widget ((&rest args &key &allow-other-keys) &body root-nodes)
  `(make-instance 'treeble/widget ,@args :root-nodes (list ,@root-nodes)))

(def method component-style-class ((self treeble/widget))
  (string+ "content-border " (call-next-method)))

(def render-xhtml treeble/widget
  (bind (((:read-only-slots root-nodes) -self-))
    (with-render-style/component (-self-)
      <table <thead <tr (:class "nodrow") ,(render-treeble-columns -self-)>>
        <tbody ,(foreach #'render-component root-nodes)>>)))

(def render-csv treeble/widget
  (write-csv-line (columns-of -self-))
  (write-csv-line-separator)
  (foreach #'render-component (root-nodes-of -self-)))

(def render-ods treeble/widget
  <table:table
    <table:table-row ,(foreach #'render-component (columns-of -self-))>
    ,(foreach #'render-component (root-nodes-of -self-))>)

(def (layered-function e) render-treeble-columns (treeble)
  (:method ((self treeble/widget))
    (foreach #'render-component (columns-of self))))
