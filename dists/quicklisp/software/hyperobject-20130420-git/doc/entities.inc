<!ENTITY hyperobject "<application><emphasis>Hyperobject</emphasis></application>">
<!ENTITY uffi "<application><emphasis>UFFI</emphasis></application>">
<!ENTITY ffi "<acronym>FFI</acronym>">
<!ENTITY cmucl "<application>CMUCL</application>">
<!ENTITY scl "<application>SCL</application>">
<!ENTITY lw "<application>Lispworks</application>">
<!ENTITY sbcl "<application>SBCL</application>">
<!ENTITY openmcl "<application>OpenMCL</application>">
<!ENTITY mcl "<application>MCL</application>">
<!ENTITY acl "<application>AllegroCL</application>">
<!ENTITY cl "<application>ANSI Common Lisp</application>">
<!ENTITY t "<constant>T</constant>">
<!ENTITY nil "<constant>NIL</constant>">
<!ENTITY null "<constant>NULL</constant>">
<!ENTITY c "<computeroutput>C</computeroutput>">
<!ENTITY asdf "<application>ASDF</application>">
<!ENTITY kmrcl "<application>KMRCL</application>">
<!ENTITY clsql "<application>CLSQL</application>">
