#+cmu (setq ext:*gc-verbose* nil)

(asdf:operate 'asdf:load-op 'lml2)
(in-package :lml2)
(let ((cwd (parse-namestring (lml-cwd))))
  (process-dir cwd))
(lml-quit)
