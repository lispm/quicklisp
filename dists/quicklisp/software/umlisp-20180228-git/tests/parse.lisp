;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: umlisp -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          parse.lisp
;;;; Purpose:       Parsing tests for UMLisp
;;;; Author:        Kevin M. Rosenberg
;;;; Date Started:  May 2003
;;;;
;;;; $Id$
;;;;
;;;; This file, part of UMLisp, is
;;;;    Copyright (c) 2000-2002 by Kevin M. Rosenberg, M.D.
;;;;
;;;; UMLisp users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License.
;;;; *************************************************************************

(in-package #:umlisp-tests)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (import '(umlisp::*umls-files* umlisp::*umls-cols*)))

(setq *rt-parse*
  '(
    (deftest :parse/1
        (umlisp::decompose-fil "abc")
      "abc" nil)

    (deftest :parse/2
        (umlisp::decompose-fil "dir/abc")
      "abc" "dir")

    (deftest :parse/3
        (umlisp::decompose-fil nil)
      nil nil)

    (deftest :parse/4
        (umlisp::filename-to-tablename "test")
      "test")

    (deftest :parse/5
        (umlisp::filename-to-tablename "TEST.AB.RRF")
      "TEST_AB")))

;; specific for UMLS2007AA
(when (probe-file (umlisp::umls-pathname "MRFILES.RRF"))
  (umlisp::ensure-ucols+ufiles)
  (setq
   *rt-parse*
   (append
    *rt-parse*
    '(
      (deftest uparse.1 (length *umls-files*) 63)
      (deftest uparse.2 (length *umls-cols*) 452)
      (deftest uparse.3
          (sort (mapcar #'u::col (umlisp::ucols (umlisp::find-ufile "MRCONSO.RRF")))
           #'string<)
        ("AUI" "CODE" "CUI" "CVF" "ISPREF" "KCUILRL" "KCUILUI" "KCUISUI" "KLUILRL"
         "KPFENG" "KSUILRL" "LAT" "LUI" "SAB" "SAUI" "SCUI" "SDUI" "SRL" "STR" "STT"
         "SUI" "SUPPRESS" "TS" "TTY"))
      (deftest uparse.4
          (equal
           (sort (mapcar #'u::col (umlisp::ucols (umlisp::find-ufile "MRCONSO.RRF")))
                 #'string<)
           (sort (copy-seq (umlisp::fields (umlisp::find-ufile "MRCONSO.RRF")))
                 #'string<))
        t)
      (deftest uparse.5
          (sort
           (umlisp::custom-colnames-for-filename "MRCONSO.RRF")
           #'string<)
        ("KCUILRL" "KCUILUI" "KCUISUI" "KLUILRL" "KPFENG" "KSUILRL"))
      (deftest uparse.6
          (compiled-function-p
           (umlisp::custom-value-fun
            (umlisp::find-ucol "KCUISUI" "MRCONSO.RRF")))
        t)
      ))))
