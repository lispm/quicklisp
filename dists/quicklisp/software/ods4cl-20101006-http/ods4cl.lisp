;;;  ods4cl.lisp --- library to create Open Document Spreadsheets

;;;  Copyright (C) 2006, 2007, 2008 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: ods4cl

#+cmu (ext:file-comment "$Module: ods4cl.lisp $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

;;; This code comes from a lot of guesswork, reverse engineering, and
;;; the usual trial ad error procedure.  Especially the XML preambles
;;; have been copied verbatim from OpenOffice documents without making
;;; any effort to understand them.
;;;
;;; It may work for you.  It just does everything I need in practical
;;; applications.

(cl:in-package :cl-user)

(defpackage :ods4cl
  (:use :common-lisp
	:sclf)
  (:export #:make-spreadsheet))

(in-package :ods4cl)

(defun write-preamble (stream)
  (princ "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<office:document-content
   xmlns:office=\"urn:oasis:names:tc:opendocument:xmlns:office:1.0\"
   xmlns:style=\"urn:oasis:names:tc:opendocument:xmlns:style:1.0\"
   xmlns:text=\"urn:oasis:names:tc:opendocument:xmlns:text:1.0\"
   xmlns:table=\"urn:oasis:names:tc:opendocument:xmlns:table:1.0\"
   xmlns:draw=\"urn:oasis:names:tc:opendocument:xmlns:drawing:1.0\"
   xmlns:fo=\"urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0\"
   xmlns:xlink=\"http://www.w3.org/1999/xlink\"
   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"
   xmlns:meta=\"urn:oasis:names:tc:opendocument:xmlns:meta:1.0\"
   xmlns:number=\"urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0\"
   xmlns:svg=\"urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0\"
   xmlns:chart=\"urn:oasis:names:tc:opendocument:xmlns:chart:1.0\"
   xmlns:dr3d=\"urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0\"
   xmlns:math=\"http://www.w3.org/1998/Math/MathML\"
   xmlns:form=\"urn:oasis:names:tc:opendocument:xmlns:form:1.0\"
   xmlns:script=\"urn:oasis:names:tc:opendocument:xmlns:script:1.0\"
   xmlns:ooo=\"http://openoffice.org/2004/office\"
   xmlns:ooow=\"http://openoffice.org/2004/writer\"
   xmlns:oooc=\"http://openoffice.org/2004/calc\"
   xmlns:dom=\"http://www.w3.org/2001/xml-events\"
   xmlns:xforms=\"http://www.w3.org/2002/xforms\"
   xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"
   xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
   office:version=\"1.0\">
  <office:scripts/>
  <office:font-face-decls>
    <style:font-face style:name=\"Lucida Sans Unicode\"
		     svg:font-family=\"&apos;Lucida Sans Unicode&apos;\"
		     style:font-pitch=\"variable\"/>
    <style:font-face style:name=\"Lucidasans\"
		     svg:font-family=\"Lucidasans\"
		     style:font-pitch=\"variable\"/>
    <style:font-face style:name=\"Arial\" svg:font-family=\"Arial\"
		     style:font-family-generic=\"swiss\"
		     style:font-pitch=\"variable\"/>
  </office:font-face-decls>
  <office:automatic-styles>
    <style:style style:name=\"co1\" style:family=\"table-column\">
      <style:table-column-properties fo:break-before=\"auto\" style:use-optimal-column-width=\"true\"/>
    </style:style>
    <style:style style:name=\"ro1\" style:family=\"table-row\">
      <style:table-row-properties fo:break-before=\"auto\" style:use-optimal-row-height=\"true\"/>
    </style:style>
    <style:style style:name=\"ta1\" style:family=\"table\" style:master-page-name=\"Default\">
      <style:table-properties table:display=\"true\" style:writing-mode=\"lr-tb\"/>
  </style:style>
  </office:automatic-styles>" stream))


(defun escape-string (string)
  "Escape STRING making it safe to be inserted into XML code."
  (with-output-to-string (out)
    (loop
       for c across string
       do (case c
	    (#\& (write-sequence "&amp;" out))
	    (#\< (write-sequence "&lt;" out))
	    (#\> (write-sequence "&gt;" out))
	    (#\' (write-sequence "&apos;" out))
	    (#\" (write-sequence "&quot;" out))
	    (t (write-char c out))))))

(defun write-cell (cell stream)
  (typecase cell
    (number
     (format stream "
	  <table:table-cell office:value-type=\"float\" office:value=\"~F\">
	    <text:p>~:*~F</text:p>
	  </table:table-cell>" cell))
    (t
     (format stream "
	  <table:table-cell office:value-type=\"string\">
	    <text:p>~A</text:p>
	  </table:table-cell>" (escape-string cell)))))

(defun write-row (row stream)
  (dolist (cell row)
    (write-cell cell stream)))

(defun write-grid (grid stream)
  (dolist (row grid)
    (princ "
	<table:table-row table:style-name=\"ro1\">" stream)
    (write-row row stream)
    (princ "
	</table:table-row>" stream)))

(defun write-sheets (sheets stream)
  (dolist (sheet sheets)
    (be name (car sheet)
	grid (cdr sheet)
      (format stream "
      <table:table table:name=\"~A\" table:style-name=\"ta1\" table:print=\"false\">
        <table:table-columns>
~{	  <table:table-column table:style-name=\"co1\"/>
~*~}        </table:table-columns>"
	      name (car grid))
      (write-grid grid stream))
    (princ "
      </table:table>" stream)))

(defun write-spreadsheet-body (sheets stream)
  (princ "
  <office:body>
    <office:spreadsheet>" stream)
  (write-sheets sheets stream)
  (princ "
    </office:spreadsheet>
  </office:body>" stream))

(defun write-document (sheets stream)
  "Write an ODS spreadsheet to STREAM.  SHEETS is a list of
spreadsheets each one consists of a name and a grid.  The grid is
a list rows and each row is a list of cells."
  (write-preamble stream)
  (write-spreadsheet-body sheets stream)
  (princ "
</office:document-content>
" stream)
  (values))

(defun make-styles-file (path)
  (with-open-file (out (make-pathname :defaults path
				      :name "styles"
				      :type "xml")
		       :direction :output)
    (format out "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<office:document-styles
   xmlns:office=\"urn:oasis:names:tc:opendocument:xmlns:office:1.0\"
   xmlns:style=\"urn:oasis:names:tc:opendocument:xmlns:style:1.0\"
   xmlns:text=\"urn:oasis:names:tc:opendocument:xmlns:text:1.0\"
   xmlns:table=\"urn:oasis:names:tc:opendocument:xmlns:table:1.0\"
   xmlns:draw=\"urn:oasis:names:tc:opendocument:xmlns:drawing:1.0\"
   xmlns:fo=\"urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0\"
   xmlns:xlink=\"http://www.w3.org/1999/xlink\"
   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"
   xmlns:meta=\"urn:oasis:names:tc:opendocument:xmlns:meta:1.0\"
   xmlns:number=\"urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0\"
   xmlns:svg=\"urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0\"
   xmlns:chart=\"urn:oasis:names:tc:opendocument:xmlns:chart:1.0\"
   xmlns:dr3d=\"urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0\"
   xmlns:math=\"http://www.w3.org/1998/Math/MathML\"
   xmlns:form=\"urn:oasis:names:tc:opendocument:xmlns:form:1.0\"
   xmlns:script=\"urn:oasis:names:tc:opendocument:xmlns:script:1.0\"
   xmlns:ooo=\"http://openoffice.org/2004/office\"
   xmlns:ooow=\"http://openoffice.org/2004/writer\"
   xmlns:oooc=\"http://openoffice.org/2004/calc\"
   xmlns:dom=\"http://www.w3.org/2001/xml-events\" office:version=\"1.0\">
  <office:font-face-decls>
    <style:font-face style:name=\"Lucida Sans Unicode\"
		     svg:font-family=\"&apos;Lucida Sans Unicode&apos;\"
		     style:font-pitch=\"variable\"/>
    <style:font-face style:name=\"Lucidasans\"
		     svg:font-family=\"Lucidasans\"
		     style:font-pitch=\"variable\"/>
    <style:font-face style:name=\"Arial\" svg:font-family=\"Arial\"
		     style:font-family-generic=\"swiss\"
		     style:font-pitch=\"variable\"/>
  </office:font-face-decls>
  <office:styles>
    <style:default-style style:family=\"table-cell\">
      <style:table-cell-properties style:decimal-places=\"2\"/>
      <style:paragraph-properties style:tab-stop-distance=\"1.25cm\"/>
      <style:text-properties style:font-name=\"Arial\" fo:language=\"it\"
			     fo:country=\"IT\" style:font-name-asian=\"Lucida Sans Unicode\"
			     style:language-asian=\"none\" style:country-asian=\"none\"
			     style:font-name-complex=\"Lucidasans\"
			     style:language-complex=\"none\" style:country-complex=\"none\"/>
    </style:default-style>
    <number:number-style style:name=\"N0\">
      <number:number number:min-integer-digits=\"1\"/>
    </number:number-style>
    <style:style style:name=\"Default\" style:family=\"table-cell\"/>
    <style:style style:name=\"Result\" style:family=\"table-cell\"
		 style:parent-style-name=\"Default\">
      <style:text-properties fo:font-style=\"italic\"
			     style:text-underline-style=\"solid\"
			     style:text-underline-width=\"auto\"
			     style:text-underline-color=\"font-color\" fo:font-weight=\"bold\"/>
    </style:style>
    <style:style style:name=\"Result2\" style:family=\"table-cell\"
		 style:parent-style-name=\"Result\" style:data-style-name=\"N104\"/>
    <style:style style:name=\"Heading\" style:family=\"table-cell\"
		 style:parent-style-name=\"Default\">
      <style:table-cell-properties style:text-align-source=\"fix\"
				   style:repeat-content=\"false\"/>
      <style:paragraph-properties fo:text-align=\"center\"/>
      <style:text-properties fo:font-size=\"16pt\"
			     fo:font-style=\"italic\" fo:font-weight=\"bold\"/>
    </style:style>
    <style:style style:name=\"Heading1\" style:family=\"table-cell\"
		 style:parent-style-name=\"Heading\">
      <style:table-cell-properties style:rotation-angle=\"90\"/>
    </style:style>
  </office:styles>
  <office:automatic-styles>
    <style:page-layout style:name=\"pm1\">
      <style:page-layout-properties style:writing-mode=\"lr-tb\"/>
      <style:header-style>
	<style:header-footer-properties fo:min-height=\"0.751cm\"
					fo:margin-left=\"0cm\" fo:margin-right=\"0cm\"
					fo:margin-bottom=\"0.25cm\"/>
      </style:header-style>
      <style:footer-style>
	<style:header-footer-properties fo:min-height=\"0.751cm\"
					fo:margin-left=\"0cm\" fo:margin-right=\"0cm\"
					fo:margin-top=\"0.25cm\"/>
      </style:footer-style>
    </style:page-layout>
    <style:page-layout style:name=\"pm2\">
      <style:page-layout-properties style:writing-mode=\"lr-tb\"/>
      <style:header-style>
	<style:header-footer-properties fo:min-height=\"0.751cm\"
					fo:margin-left=\"0cm\"
					fo:margin-right=\"0cm\"
					fo:margin-bottom=\"0.25cm\"
					fo:border=\"0.088cm solid
						   #000000\"
					fo:padding=\"0.018cm\"
					fo:background-color=\"#c0c0c0\">
	  <style:background-image/>
	</style:header-footer-properties>
      </style:header-style>
      <style:footer-style>
	<style:header-footer-properties fo:min-height=\"0.751cm\"
					fo:margin-left=\"0cm\"
					fo:margin-right=\"0cm\"
					fo:margin-top=\"0.25cm\"
					fo:border=\"0.088cm solid
						   #000000\" fo:padding=\"0.018cm\"
					fo:background-color=\"#c0c0c0\">
	  <style:background-image/>
	</style:header-footer-properties>
      </style:footer-style>
    </style:page-layout>
  </office:automatic-styles>
  <office:master-styles>
    <style:master-page style:name=\"Default\"
		       style:page-layout-name=\"pm1\">
      <style:header>
	<text:p>
	  <text:sheet-name>
	    ???</text:sheet-name>
	</text:p>
      </style:header>
      <style:header-left style:display=\"false\"/>
      <style:footer>
	<text:p>
	  Page
	  <text:page-number>
	    1
	  </text:page-number>
	</text:p>
      </style:footer>
      <style:footer-left style:display=\"false\"/>
    </style:master-page>
    <style:master-page style:name=\"Report\"
		       style:page-layout-name=\"pm2\">
      <style:header>
	<style:region-left>
	  <text:p>
	    <text:sheet-name>
	      ???</text:sheet-name>
	    (<text:title>???</text:title>)
	  </text:p>
	</style:region-left>
	<style:region-right>
	  <text:p>
	    <text:date style:data-style-name=\"N2\"
		       text:date-value=\"2006-02-16\">
	      16/02/2006
	    </text:date>
	    , <text:time>
	      12:40:04
	    </text:time>
	  </text:p>
	</style:region-right>
      </style:header>
      <style:header-left style:display=\"false\"/>
      <style:footer>
	<text:p>
	  Page
	  <text:page-number>
	    1
	  </text:page-number>
	  /
	  <text:page-count>
	    99
	  </text:page-count>
	</text:p>
      </style:footer>
      <style:footer-left style:display=\"false\"/>
    </style:master-page>
  </office:master-styles>
</office:document-styles>~%")))

(defun make-meta-file (path sheets &key language owner)
  (with-open-file (out (make-pathname :defaults path
				      :name "meta"
				      :type "xml")
		       :direction :output)
    (format out "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<office:document-meta
   xmlns:office=\"urn:oasis:names:tc:opendocument:xmlns:office:1.0\"
   xmlns:xlink=\"http://www.w3.org/1999/xlink\"
   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"
   xmlns:meta=\"urn:oasis:names:tc:opendocument:xmlns:meta:1.0\"
   xmlns:ooo=\"http://openoffice.org/2004/office\" office:version=\"1.0\">
  <office:meta>
    <meta:generator>
      ODS4CL ~A
    </meta:generator>
    <meta:initial-creator>
      ~A
    </meta:initial-creator>
    <meta:creation-date>
      ~A
    </meta:creation-date>
    <dc:creator>
      ~2:*~A
    </dc:creator>
    <dc:date>
      ~A
    </dc:date>
    <dc:language>
      ~A
    </dc:language>
    <meta:document-statistic meta:table-count=\"~A\" meta:cell-count=\"~A\"/>
</office:meta>
</office:document-meta>~%"
	    #.(time-string (get-universal-time))
	    (or owner "unknown")
	    (iso-time-string (get-universal-time))
	    (or language "en-GB")
	    (length sheets)
	    0000)))

(defun make-mimetype-file (path)
  (with-open-file (out (make-pathname :defaults path
				      :name "mimetype"
				      :type nil)
		       :direction :output)
    (princ "application/vnd.oasis.opendocument.spreadsheet" out)))

(defun make-manifest-file (path)
  (be file (make-pathname :defaults path
			  :directory (append (pathname-directory path) '("META-INF"))
			  :name "manifest"
			  :type "xml")
    (ensure-directories-exist file)
    (with-open-file (out file
			 :direction :output)
      (format out "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<!DOCTYPE manifest:manifest PUBLIC \"-//OpenOffice.org//DTD Manifest 1.0//EN\" \"Manifest.dtd\">
<manifest:manifest xmlns:manifest=\"urn:oasis:names:tc:opendocument:xmlns:manifest:1.0\">
 <manifest:file-entry manifest:media-type=\"application/vnd.oasis.opendocument.spreadsheet\" manifest:full-path=\"/\"/>
 <manifest:file-entry manifest:media-type=\"text/xml\" manifest:full-path=\"content.xml\"/>
 <manifest:file-entry manifest:media-type=\"text/xml\" manifest:full-path=\"styles.xml\"/>
 <manifest:file-entry manifest:media-type=\"text/xml\" manifest:full-path=\"meta.xml\"/>
 <manifest:file-entry manifest:media-type=\"text/xml\" manifest:full-path=\"settings.xml\"/>
</manifest:manifest>"))))

(defun make-content-file (path sheets)
  (be file (make-pathname :defaults path
			  :name "content"
			  :type "xml")
    (with-open-file (out file
			 :direction :output)
      (write-document sheets out))))

(defun zip-ods (output-file input-directory)
  ;; Here we use redirection otherwise zip wants to be smart and will,
  ;; in some occasions, change its mind about the name of the output
  ;; file.  We have to use a shell command because the concept of the
  ;; current directory isn't in the Common Lisp standard and not every
  ;; Lisp system has an extension for that.
  (run-shell-command "cd ~A && zip -rD - * > ~A" input-directory output-file))

(defun make-directory-zipped-stream (input-directory)
  "Zip the content of INPUT-DIRECTORY and return a stream object to
the zipped bytes."
  ;; We have to use a shell command because the concept of the current
  ;; directory isn't in the Common Lisp standard and not every Lisp
  ;; system has an extension for that.
  (run-pipe :input *bourne-shell* (list "-c" (format nil "cd ~A && zip -rD - *" input-directory))))

(defun make-spreadsheet (output sheets &key (if-exists :error))
  "Create an ODS spreadsheet from SHEET.  Write the spreadsheet into
OUTPUT which can be either a stream or a pathname.  If the file OUTPUT
already exists and IF-EXISTS is :ERROR, signal an error; if IF-EXISTS
is :REPLACE, silently replace the existing file.  SHEET must be a list
of pages where every page is a list where the first element is the
page name (a string) and the remaining elements are the sheet rows.
Each row is a list of elements."
  (with-temp-directory (dir)
    (flet ((make-directory-tree ()
	     (make-manifest-file dir)
	     (make-mimetype-file dir)
	     (make-styles-file dir)
	     (make-meta-file dir sheets)
	     (make-content-file dir sheets)))
      (cond ((streamp output)
	     (make-directory-tree)
	     (with-open-stream (zip (make-directory-zipped-stream dir))
	       (be buffer (make-string 1024)
		 (loop
		    for chunk-length = (read-sequence buffer zip)
		    do (write-sequence buffer output :end chunk-length)
		    until (zerop chunk-length)))))
	    ((pathnamep output)
	     (when (probe-file output)
	       (ecase if-exists
		 (:error (error "~A exists." output))
		 (:replace (delete-file output))))
	     (make-directory-tree)
	     (zip-ods output dir))
	    (t
	     (error "OUTPUT is neither a pathname nor a stream."))))))
