;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          cl-photo-tests.asd
;;;; Purpose:       ASDF system definitionf for cl-photo testing package
;;;; Author:        Kevin M. Rosenberg
;;;; Date Started:  Apr 2003
;;;;
;;;; $Id$
;;;; *************************************************************************

(defpackage #:cl-photo-tests-system
  (:use #:asdf #:cl))
(in-package #:cl-photo-tests-system)

(operate 'load-op 'cl-photo)


(defsystem cl-photo-tests
    :depends-on (cl-photo rt)
    :components ((:file "tests")))

(defmethod perform ((o test-op) (c (eql (find-system :cl-photo-tests))))
  (operate 'load-op 'cl-photo)
  (or (funcall (intern (symbol-name '#:do-tests)
		       (find-package '#:regression-test)))
      (error "test-op failed")))
