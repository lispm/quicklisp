(in-package :tcr.sequence-iterators.test)

(defun 2+ (n)
  (+ n 2))

(defun 2- (n)
  (- n 2))

(defmacro signals-condition-p (name &body body)
  `(handler-case (prog1 nil ,@body)
     (,(second name) () t)))

(defun maybe-intern (name package)
  (values
   (if package
       (intern name (if (eq t package) *package* package))
       (make-symbol name))))

(defun format-symbol (package control &rest arguments)
  (maybe-intern (apply #'format nil control arguments) package))

(defun type= (type1 type2)
  "Returns a primary value of T is TYPE1 and TYPE2 are the same type,
and a secondary value that is true is the type equality could be reliably
determined: primary value of NIL and secondary value of T indicates that the
types are not equivalent."
  (multiple-value-bind (sub ok) (subtypep type1 type2)
    (cond ((and ok sub)
           (subtypep type2 type1))
          (ok
           (values nil ok))
          (t
           (multiple-value-bind (sub ok) (subtypep type2 type1)
             (declare (ignore sub))
             (values nil ok))))))

(defun sequence-type-of (sequence)
  (let ((type (type-of sequence)))
    (cond ((subtypep type 'list)
           'list)
          ((subtypep type 'vector)
           `(simple-array ,(sequence-element-type sequence) (*)))
          (t
           type))))

(defun sequence-element-type (sequence)
  (etypecase sequence
    (list 't)
    (vector (array-element-type sequence))
    (sequence 't)))

(defun sequence-inferred-element-type (sequence)
  (flet ((upgraded-type (t1 t2)
           (upgraded-array-element-type `(or ,t1 ,t2))))
    (reduce #'upgraded-type sequence :initial-value nil :key #'type-of)))

(defun prototype (type)
  (cond ((type= type 'character) #\x)
        ((type= type 'bit)       1)
        ((type= type 't)         :anything)))

(defvar *default-size* 10)

(defvar *list*
  (loop for i from 0 below *default-size* collect i))

(defvar *vector*
  (map 'vector #'identity *list*))

(defvar *vector-with-fill-pointer*
  (let* ((v (make-array (* *default-size* 2) :fill-pointer *default-size*)))
    (replace v *vector*)
    v))

(defvar *specialized-vector*
  (make-array *default-size* :element-type 'fixnum :initial-contents *vector*))

(defvar *string*
  (let ((*print-base* (1+ *default-size*)))
    (format nil "~{~A~}" *list*)))

(defvar *bit-vector*
  (make-sequence 'bit-vector *default-size* :initial-element 0))


#+nil
(defvar *large-bit-vector*
  (make-array (1- array-dimension-limit)
              :element-type 'bit
              :initial-element 0))

(defvar *empty-list* nil)
(defvar *empty-vector* #())
(defvar *empty-vector-with-fill-pointer*
  (make-array (* *default-size* 2) :fill-pointer 0))
(defvar *empty-specialized-vector*
  (make-array 0 :element-type 'fixnum))
(defvar *empty-string* "")
(defvar *empty-bit-vector* #*)

(defvar *sequence-variables*
  '(*list*                     *empty-list*
    *vector*                   *empty-vector*
    *vector-with-fill-pointer* *empty-vector-with-fill-pointer*
    *specialized-vector*       *empty-specialized-vector*
    *string*                   *empty-string*
    *bit-vector*               *empty-bit-vector*))

(defvar *sequence*)
(defvar *size*)
(defvar *start*)
(defvar *end*)
(defvar *from-end*)

(defmacro define-sequence-test (name (&rest clauses) form &rest values)
  (flet ((parse-clause (clause)
           (destructuring-bind (&key sequences
                                     (contents nil contents-p)
                                     (start 0)
                                     end
                                     from-end) clause
             ;; Filter sequences according to the :SEQUENCES
             ;; parameter.
             (setq sequences
                   (mapcar #'symbol-value
                           (remove-if (ecase sequences
                                        (:all
                                         (constantly nil))
                                        (:non-empty
                                         (lambda (s)
                                           (search "empty" (symbol-name s)
                                                   :test #'char-equal)))
                                        (:empty
                                         (complement
                                          (lambda (s)
                                            (search "empty" (symbol-name s)
                                                    :test #'char-equal)))))
                                          *sequence-variables*)))
             (values
              (if (not contents-p)
                  sequences
                  (let* ((contents      (eval contents))
                         (length        (length contents))
                         (inferred-type (sequence-inferred-element-type contents)))
                    ;; Remove sequences that are not compatible to the
                    ;; :INITIAL-CONTENTS parameter.
                    (loop for seq in sequences
                          ;; Skip over vector-with-fill-pointer
                          ;; because MAKE-SEQUENCE-LIKE won't
                          ;; preserve the fill-pointer anyway.
                          if (and (vectorp seq)
                                  (array-has-fill-pointer-p seq))
                            do (progn)
                          else if (subtypep inferred-type (sequence-element-type seq))
                            collect (make-sequence-like seq length
                                                        :initial-contents contents))))
               start
               end
               from-end)))
         (generate-deftests (name sequences start end from-end)
           (let ((deftest (intern (string 'deftest) *package*)))
             (loop for sequence in sequences
                   for idx from 1
                   for length = (length sequence)
                   collecting
                   `(,deftest ,(let ((*print-pretty* nil)
                                     (*print-right-margin* 100000))
                                 (format-symbol t
                                   "(~A :START ~A :END ~A :FROM-END ~A ~
                                        :TYPE ~A :LENGTH ~D~
                                        ~@[ :FILL-POINTER ~A~])"
                                   name start end from-end (type-of sequence)
                                   (length sequence)
                                   (when (vectorp sequence)
                                     (array-has-fill-pointer-p sequence))))
                        (let* ((*sequence* ',sequence)
                               (*size*     ,length)
                               (*start*    ,start)
                               (*end*      ,end)
                               (*from-end* ,from-end))
                          ,form)
                      ,@(let* ((*sequence*  sequence)
                               (*size*      length)
                               (*start*     (eval start))
                               (*end*       (eval end))
                               (*from-end*  (eval from-end)))
                              (mapcar #'eval values)))))))
    `(progn
       ,@(loop for clause in clauses
               appending (multiple-value-call #'generate-deftests name
                           (parse-clause clause))))))