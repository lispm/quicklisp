;;;; -*- Lisp -*-
;;;;
;;;; $Id:$
;;;;
;;;; ASDF definition file for cl-cli-parser.
;;;;

(defsystem :cli-parser
  :description "A command-line argument parser. Mostly parses options of the same form that getopt parses"
  :version "0.1"
  :author "Denis Bueno"
  :license "Copyright (c) 2005"
  :components ((:file "cli-parser")
;	       (:file "cli-parser-test")
;	       (:file "unit-test")
	       ))
