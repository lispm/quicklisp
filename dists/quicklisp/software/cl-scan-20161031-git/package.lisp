(defpackage #:cl-scan
  (:use #:cl)
  (:import-from #:lparallel
		#:pmap
		#:*kernel*
		#:make-kernel)
  (:import-from #:usocket
		#:socket-connect
		#:socket-close)
  (:import-from #:inferior-shell
		#:run/s)
  (:export :main))
