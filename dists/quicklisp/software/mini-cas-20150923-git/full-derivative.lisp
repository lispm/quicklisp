(in-package :mini-cas)

;;;; protocol for derivatives
(defvar *partial* nil "set true to enable partial derivatives")
(defgeneric full-derivative (form var))
(defgeneric partial-derivative (form var)
  (:method (form var)
    (let ((*partial* t))
      (full-derivative form var))))

(defmethod full-derivative (form var)
  (list 'd form var))
(defmethod full-derivative ((form symbol) var)
  (if *partial*
      (if (cas-equal form var) 1 0)
      (if (cas-equal form var) 1 (list 'd form var))))
(defmethod full-derivative ((form number) var)
  0)

(defgeneric full-derivative-list (car cdr var))
(defmethod full-derivative ((form list) var)
  (full-derivative-list (car form) (cdr form) var))
(defmethod full-derivative-list (car cdr var)
  (list 'd (cons car cdr) var))


;; Ring operations
(defmethod full-derivative-list ((car (eql '+)) cdr var)
  (cons '+
        (loop for x in cdr
           collecting (full-derivative x var))))
(defmethod full-derivative-list ((car (eql '-)) cdr var)
  (cas- (full-derivative (car cdr) var)))
(defmethod full-derivative-list ((car (eql '*)) cdr var)
  (cons '+
        (loop for front = (cas*) then (append front (list x))
           and x in cdr
           and back = (cdr cdr) then (cdr back)
           collecting (append front
                              (list (full-derivative x var))
                              back))))
                              
(defmethod full-derivative-list ((car (eql '/)) cdr var)
  (let ((u (first cdr))
        (v (second cdr)))
    (cas/ (cas+ (cas* v (full-derivative u var))
                 (cas-
                  (cas* u (full-derivative v var))))
            (cas* v v))))
(defmethod full-derivative-list ((car (eql 'sin)) cdr var)
  (let ((u (first cdr)))
    (cas* (cas-cos u) (full-derivative u var))))
(defmethod full-derivative-list ((car (eql 'cos)) cdr var)
  (cas-
   (let ((u (first cdr)))
     (cas* (cas-sin u) (full-derivative u var)))))
