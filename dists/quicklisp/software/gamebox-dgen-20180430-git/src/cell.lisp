(in-package :box.dgen)

(defclass cell ()
  ((%x :reader x
       :initarg :x)
   (%y :reader y
       :initarg :y)
   (%carved :accessor carved
            :initform nil)
   (%region-id :accessor region-id
               :initform nil)
   (%features :accessor features
              :initform '(:wall))
   (%distance :accessor distance
              :initform -1))
  (:documentation "A cell represents a single unit in the stage grid which can contain various
features."))

(defmethod print-object ((object cell) stream)
  (print-unreadable-object (object stream)
    (format stream "X:~S, Y:~S" (x object) (y object))))

(defun make-cell (stage x y)
  (setf (cell stage x y) (make-instance 'cell :x x :y y)))

(defun cell (stage x y)
  "Get a cell in STAGE using the coordinates X and Y."
  (aref (grid stage) x y))

(defun (setf cell) (value stage x y)
  "Set a cell in STAGE to VALUE at the coordinates given by X and Y."
  (setf (aref (grid stage) x y) value))

(defun cell-index (stage cell)
  "Get the cell index in STAGE, given the cell object, CELL."
  (+ (* (y cell) (width stage)) (x cell)))

(defun valid-cell-p (stage x y)
  (when (and (not (minusp x))
             (not (minusp y))
             (< x (width stage))
             (< y (height stage)))
    (cell stage x y)))

(defun stage-border-p (stage cell)
  (with-slots (%x %y) cell
    (or (zerop %x)
        (zerop %y)
        (= %x (1- (width stage)))
        (= %y (1- (height stage))))))

(defun carve (stage cell &key (region-id *region*) feature)
  (setf (carved cell) t
        (region-id cell) region-id)
  (alexandria:when-let ((region (get-region stage region-id)))
    (pushnew cell (cells region)))
  (add-feature cell feature)
  (remove-feature cell :wall))

(defun uncarve (stage cell)
  (with-slots (%carved %region-id %features) cell
    (alexandria:when-let ((region (get-region stage %region-id)))
      (alexandria:deletef (cells region) cell))
    (setf %carved nil
          %region-id nil
          %features '(:wall))))

(defun featuresp (cell &rest features)
  (intersection features (features cell)))

(defun remove-feature (cell feature)
  (alexandria:deletef (features cell) feature))

(defun add-feature (cell feature)
  (pushnew feature (features cell)))
