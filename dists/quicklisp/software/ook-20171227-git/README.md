# OOK!

A Common Lisp compiler and decompiler for the programming language
[__OOK!__](http://esolangs.org/wiki/Ook!).

It is well known that orangutans grok Lisp.

## Author

Marco "Wizzard" Antoniotti, 2015-2018

## License

This library is put in the Public Domain
