#+cmu (setq ext:*gc-verbose* nil)

(asdf:oos 'asdf:load-op :lml)
(in-package :lml)
(let ((cwd (parse-namestring (lml-cwd))))
  (process-dir cwd))
(lml-quit)
