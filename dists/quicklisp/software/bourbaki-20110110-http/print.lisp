;;; Copyright (c) Juha Arpiainen 2005-06

;;; This file is part of Bourbaki.

;;; Bourbaki is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.

;;; Bourbaki is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Bourbaki; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; *****
;;; Printing symtrees and theorems
;;; *****

(in-package :bourbaki)

;;; prints a symbol tree, with optional parentheses for clarity
;;; The following values for mode are possible
;;;   :full prints full name of each symbol (instead of just the last component)
;;;   :paren inserts parentheses around each subexpression with at least 2 arguments
(defun print-symtree-aux (stream str &optional (mode nil) (foo nil))
  (cond ((null str) nil)
        ((typep str 'context)
         (write-string
           (if (eq mode :full)
             (gethash :full-name (context-meta str))
             (context-name str))
           stream))
        (t (if (typep (car str) 'context)
             (progn
               (if (and foo (eq mode :paren) (> (length str) 1)) (write-char #\( stream))
               (print-symtree-aux stream (car str) mode)
               (if (cdr str) (write-char #\Space stream))
               (print-symtree-aux stream (cdr str) mode (> (length (cdr str)) 1))
               (if (and foo (eq mode :paren) (> (length str) 1)) (write-char #\) stream)))
             (progn
               (print-symtree-aux stream (car str) mode foo)
               (if (cdr str) (write-char #\Space stream))
               (print-symtree-aux stream (cdr str) mode foo))))))

;;; prints a symbol tree, optionally delimited by []
(defun print-symtree (tree &optional (stream *standard-output*) (mode nil))
  (write-char #\[ stream)
  (if *bourbaki-debug*
    (print-symtree-debug tree)
    (print-symtree-aux stream tree mode))
  (write-char #\] stream)
  nil)

;;; prints the complete list structure of a symbol tree
(defun print-symtree-debug (str)
  (cond ((null str) nil)
        ((typep str 'context) (write-string (context-name str)))
        (t (write-char #\()
           (print-symtree-debug (car str))
           (dolist (arg (cdr str))
             (print-symtree-debug arg))
           (write-char #\)))))

;;; Prints the variables, hypotheses, assertion, distinct variable conditions and proof of a theorem
;;; TODO: Print local variables, lemmas
;;; paren: use parentheses when printing hypotheses, assertation & proof
(defun print-theorem (ref &optional (mode nil))
  (declare (ignore mode))
  (let ((thr (mkcontext ref)))
    (format t "~A ~A:~%" (if (eq (context-class thr) :context) "Context" "Theorem") (context-name thr))
    (format t "Variables: ")
    (print-symtree-aux *standard-output* (reverse (context-vars thr)))
    (format t "~%Distinct variable conditions: ")
    (dolist (cnd (context-distinct thr))
      (format t "[~A ~A] " (context-name (car cnd)) (context-name (cdr cnd))))
    (format t "~%Hypotheses: ")
    (dolist (hp (context-hypo thr))
      (print-symtree hp))
    (format t "~%Assertion: ")
    (dolist (assr (context-assert thr))
      (print-symtree assr))
    (when (context-proof thr)
      (format t "~%Proof:")
      (do-proof (line (context-proof thr))
        (let ((item (subproof-ref line)))
          (format t "~%~A " (context-name (car item)))
          (dolist (sb (cdr item))
            (print-symtree sb)))))))

(defun show-proof (ref &optional (mode nil))
  (declare (ignore mode))
  (let ((thr (mkcontext ref)))
    (if (null (context-proof thr))
      (return-from show-proof nil))
    (format t "~&Proof for ~A:" (context-name thr))
    (dolist (h (context-hypo thr))
      (format t "~%hp: ") (print-symtree h))
    (do-proof (line (context-proof thr))
      (let ((ln (subproof-ref line)))
        (let ((smap (smap (car ln) (cdr ln))))
          (format t "~%~A => " (context-name (car ln)))
          (dolist (a (context-assert (car ln)))
            (print-symtree (replace-vars a smap))))))
  (terpri)))
