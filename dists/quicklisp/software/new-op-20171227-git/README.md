NEW-OP
======

Copyright (c) 2010-2018 Marco Antoniotti
All rights reserved.

NEW-OP is a small library that introduces a unified "constructor"
operator.  Although nothing new, the operator is called "new" and it
can be used to create any Common Lisp object starting from a type
description.

See the file COPYING for license information.


A NOTE ON FORKING
-----------------

Of course you are free to fork the project subject to the current
licensing scheme.  However, before you do so, I ask you to consider
plain old "cooperation" by asking me to become a developer.
It helps keeping the entropy level at an acceptable level.


Enjoy.
