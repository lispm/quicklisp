(asdf:defsystem #:gamebox-dgen
  :description "A procedural dungeon generator."
  :author "Michael Fiano <mail@michaelfiano.com>"
  :maintainer "Michael Fiano <mail@michaelfiano.com>"
  :license "MIT"
  :homepage "https://www.michaelfiano.com/projects/gamebox-dgen"
  :source-control (:git "git@github.com:mfiano/gamebox-dgen.git")
  :bug-tracker "https://github.com/mfiano/gamebox-dgen/issues"
  :version "1.0.0"
  :encoding :utf-8
  :long-description #.(uiop:read-file-string (uiop/pathname:subpathname *load-pathname* "README.md"))
  :depends-on (#:alexandria
               #:graph
               #:cl-speedy-queue
               #:genie
               #:simple-logger)
  :pathname "src"
  :serial t
  :components
  ((:file "package")
   (:file "neighborhood")
   (:file "region")
   (:file "stage")
   (:file "cell")
   (:file "graph")
   (:file "corridor")
   (:file "room")
   (:file "junction")
   (:file "stairs")))
