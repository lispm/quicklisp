package main

import (
	_ "net/http/pprof"

	"io/ioutil"
	"fmt"
	"log"
	"net"
	"strconv"
)

func main() {
	server, err := net.Listen("tcp", ":" + strconv.Itoa(9999))
	if err != nil {
		panic(err)
	}
	for {
		client, err := server.Accept()
		if err != nil {
			log.Print(err)
		}
		go func(client net.Conn) {
			content, err := ioutil.ReadFile("/etc/hosts")
			if err != nil {
				panic(err)
			}
			client.Write([]byte(fmt.Sprintf("HTTP/1.0 200 OK\r\nContent-Length: %d\r\n\r\n%s\r\n", len(content), string(content))))
			client.Close()
		}(client)
	}
}
