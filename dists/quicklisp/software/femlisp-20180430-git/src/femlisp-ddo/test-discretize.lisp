(in-package :femlisp-ddo-test)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Parallel part
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(format t "~&*** Parallel calculation:~%")

;;; Initialization/termination 

(connect-to-mpi-workers)

(ddo (defparameter *n* (mpi-size))
     *n*)
(ddo (in-package :femlisp-ddo-test)
     *package*)

(ddo
  (princ-to-string (cl-cpu-affinity:get-cpu-affinity-mask)))

(ddo
  (progn (fl.parallel::new-kernel 2) nil))
(ddo
  (progn (fl.parallel::end-kernel) nil))

(time
 (ddo
   (defparameter *my-mesh* nil)
   (defparameter *ansatz-space* nil)
   (defparameter *rhs* nil)
   (defparameter *mat* nil)
   (format t "GC~%")
   (sb-ext:gc :full t)
   (synchronize)
   (reset-distributed-objects)
   (defparameter *n* (mpi-size))
   ;;(dbg-on :ddo-refine)
   ;;(dbg-on :distribute)
   (let ((nr-workers *n*))
     (when (< (mpi-rank) nr-workers)
       (let ((dim 3))
         (setq *my-mesh*
               (uniform-mesh-on-box-domain (n-cube-domain dim) (make-fixnum-vec dim 2))))
       (femlisp-ddo::distribute-mesh *my-mesh* nr-workers)))
   (change-class *my-mesh* '<hierarchical-mesh>)
   (format t "Refining~%")
   (loop repeat 3 do (refine *my-mesh*))
   (format t "Ready with refinement~%")
   (setq *ansatz-space*
         (let* ((order 5)
                (problem (ellsys-model-problem
                          (domain *my-mesh*) '((u 1))
                          :r (diagonal-sparse-tensor (vector #m(1.0)))
                          :f (lambda (x)
                               (vector (ensure-matlisp 1.0)))))
                (fe-class (lagrange-fe order :nr-comps 1 :type :uniform)))
           (make-fe-ansatz-space fe-class problem *my-mesh*)))
   (setq *rhs* (make-ansatz-space-vector *ansatz-space*))
   (setq *mat* (make-ansatz-space-automorphism *ansatz-space*))
   ))

(ddo (distributed-data))
(ddo (nr-of-levels *my-mesh*))
(ddo (nr-of-cells *my-mesh* :highest))
(ddo (x<-0 *rhs*)
     (x<-0 *mat*))

(ddo (dbg-on :memoize))
(ddo (dbg-on :distribute))

(loop repeat 2 do
  (time
   (ddo
     (time
      (assemble-interior *ansatz-space* :surface :matrix *mat* :rhs *rhs*))
     )))

(defun test-discretize-reset ()
  (ddo
    (setq *my-mesh* nil
          *ansatz-space* nil
          *mat* nil
          *rhs* nil)
    (sb-ext:gc :full t)
    (synchronize)
    (distributed-data)))

(test-discretize-reset)

;;; Serial calculation

(format t "~&*** Serial calculation:~%")
(defparameter *my-mesh* nil)
(defparameter *ansatz-space* nil)
(defparameter *rhs* nil)
(defparameter *mat* nil)
(format t "GC~%")
(sb-ext:gc :full t)
(defparameter *n* 1)
(let ((dim 3))
  (setq *my-mesh*
        (uniform-mesh-on-box-domain (n-cube-domain dim) (make-fixnum-vec dim 2))))
(change-class *my-mesh* '<hierarchical-mesh>)
(format t "Refining~%")
(loop repeat 3 do (refine *my-mesh*))
(format t "Ready with refinement~%")
(setq *ansatz-space*
      (let* ((order 5)
             (problem (ellsys-model-problem
                       (domain *my-mesh*) '((u 1))
                       :r (diagonal-sparse-tensor (vector #m(1.0)))
                       :f (lambda (x)
                            (vector (ensure-matlisp 1.0)))))
             (fe-class (lagrange-fe order :nr-comps 1 :type :uniform)))
        (make-fe-ansatz-space fe-class problem *my-mesh*)))
(setq *rhs* (make-ansatz-space-vector *ansatz-space*))
(setq *mat* (make-ansatz-space-automorphism *ansatz-space*))
(loop repeat 2 do
  (format t "Discretizing~%")
  (time
   (assemble-interior *ansatz-space* :surface :rhs *rhs* :matrix *mat*)))

(loop repeat 2 do
  (time
   (ddo
     (let* ((order 5)
            (problem (ellsys-model-problem
                      (domain *my-mesh*) '((u 1))
                      :r (diagonal-sparse-tensor (vector #m(1.0)))
                      :f (lambda (x)
                           (vector (ensure-matlisp (float #I"sin(2*pi*norm(x))" 1.0))))))
            (fe-class (lagrange-fe order :nr-comps 1 :type :uniform))
            (ansatz-space (make-fe-ansatz-space fe-class problem *my-mesh*)))
       (setq *rhs* (make-ansatz-space-vector ansatz-space))
       (assemble-interior ansatz-space :surface :rhs *rhs*)
       (nr-of-entries *rhs*)))))

#|

;;; For interactive work

;;; reset
(ddo (setq *my-mesh* nil
           *rhs* nil)
     (sb-ext:gc :full t)
     (synchronize)
     (distributed-data))

(ddo (mpi-rank))
(ddo (dbg-on :distribute))
(ddo (distributed-data))
(ddo (synchronize))
(ddo (reset-distributed-objects))
(ddo (sb-ext:gc :full t))
(ddo (load "ddo-discretize.lisp"))
(ddo (load "ddo.lisp"))

|#

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Finishing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(lfarm:end-kernel)
(sb-ext:quit)

