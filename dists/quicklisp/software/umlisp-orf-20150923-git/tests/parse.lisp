;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: umlisp -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          parse.lisp
;;;; Purpose:       Parsing tests for UMLisp
;;;; Author:        Kevin M. Rosenberg
;;;; Date Started:  May 2003
;;;;
;;;; $Id$
;;;;
;;;; This file, part of UMLisp, is
;;;;    Copyright (c) 2000-2002 by Kevin M. Rosenberg, M.D.
;;;;
;;;; UMLisp users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License.
;;;; *************************************************************************

(in-package #:umlisp-tests)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (if (probe-file (umlisp::umls-pathname "MRFILES"))
    (pushnew :umls-files cl:*features*)
    (format t "~&Skipping tests based on UMLS distribution~%")))

(import '(umlisp::*umls-files* umlisp::*umls-cols*))

#+umls-files
(progn
  (umlisp::ensure-ucols+ufiles)
  (deftest uparse.1 (length *umls-files*) 52)
  (deftest uparse.2 (length *umls-cols*) 327)
  (deftest uparse.3
      (sort (mapcar #'u::col (umlisp::ucols (umlisp::find-ufile "MRCON")))
            #'string<)
    ("CUI" "KCUILRL" "KCUILUI" "KCUISUI" "KLUILRL" "KPFSTR" "LAT" "LRL" "LUI" "STR"
               "STT" "SUI" "TS"))
  (deftest uparse.4
      (sort (umlisp::fields (umlisp::find-ufile "MRCON"))
            #'string<)
    ("CUI" "KCUILRL" "KCUILUI" "KCUISUI" "KLUILRL" "KPFSTR" "LAT" "LRL" "LUI" "STR"
           "STT" "SUI" "TS"))
  (deftest uparse.5
      (sort
       (umlisp::custom-colnames-for-filename "MRCON")
       #'string<)
    ("KCUILRL" "KCUILUI" "KCUISUI" "KLUILRL" "KPFSTR"))
  (deftest uparse.6
      (compiled-function-p
       (umlisp::custom-value-fun
        (umlisp::find-ucol "KCUISUI" "MRCON")))
    t)
  ) ;; umls-files

#+umls-files
(setq cl:*features* (delete :umls-files cl:*features*))

