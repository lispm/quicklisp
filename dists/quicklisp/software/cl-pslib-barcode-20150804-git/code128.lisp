;; This software is Copyright (c) cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(in-package :cl-pslib-barcode)

(alexandria:define-constant +code128-checksum-modulo+ 103 :test #'=)

(alexandria:define-constant +code128-code-c-valid-regexp+ "[0-9][0-9]" :test #'string=)

(alexandria:define-constant +code128-min-quiet-zone-abs+ 6.4 :test #'=)

(alexandria:define-constant +code128-min-quiet-zone-rel+ 10 :test #'=)

(alexandria:define-constant +code128-table+ 
    '((" " " " "00" (T T NIL T T NIL NIL T T NIL NIL))
      ("!" "!" "01" (T T NIL NIL T T NIL T T NIL NIL))
      ("\"" "\"" "02" (T T NIL NIL T T NIL NIL T T NIL))
      ("#" "#" "03" (T NIL NIL T NIL NIL T T NIL NIL NIL))
      ("$" "$" "04" (T NIL NIL T NIL NIL NIL T T NIL NIL))
      ("%" "%" "05" (T NIL NIL NIL T NIL NIL T T NIL NIL))
      ("&" "&" "06" (T NIL NIL T T NIL NIL T NIL NIL NIL))
      ("'" "'" "07" (T NIL NIL T T NIL NIL NIL T NIL NIL))
      ("(" "(" "08" (T NIL NIL NIL T T NIL NIL T NIL NIL))
      (")" ")" "09" (T T NIL NIL T NIL NIL T NIL NIL NIL))
      ("*" "*" "10" (T T NIL NIL T NIL NIL NIL T NIL NIL))
      ("+" "+" "11" (T T NIL NIL NIL T NIL NIL T NIL NIL))
      ("," "," "12" (T NIL T T NIL NIL T T T NIL NIL))
      ("-" "-" "13" (T NIL NIL T T NIL T T T NIL NIL))
      ("." "." "14" (T NIL NIL T T NIL NIL T T T NIL))
      ("/" "/" "15" (T NIL T T T NIL NIL T T NIL NIL))
      ("0" "0" "16" (T NIL NIL T T T NIL T T NIL NIL))
      ("1" "1" "17" (T NIL NIL T T T NIL NIL T T NIL))
      ("2" "2" "18" (T T NIL NIL T T T NIL NIL T NIL))
      ("3" "3" "19" (T T NIL NIL T NIL T T T NIL NIL))
      ("4" "4" "20" (T T NIL NIL T NIL NIL T T T NIL))
      ("5" "5" "21" (T T NIL T T T NIL NIL T NIL NIL))
      ("6" "6" "22" (T T NIL NIL T T T NIL T NIL NIL))
      ("7" "7" "23" (T T T NIL T T NIL T T T NIL))
      ("8" "8" "24" (T T T NIL T NIL NIL T T NIL NIL))
      ("9" "9" "25" (T T T NIL NIL T NIL T T NIL NIL))
      (":" ":" "26" (T T T NIL NIL T NIL NIL T T NIL))
      (";" ";" "27" (T T T NIL T T NIL NIL T NIL NIL))
      ("<" "<" "28" (T T T NIL NIL T T NIL T NIL NIL))
      ("=" "=" "29" (T T T NIL NIL T T NIL NIL T NIL))
      (">" ">" "30" (T T NIL T T NIL T T NIL NIL NIL))
      ("?" "?" "31" (T T NIL T T NIL NIL NIL T T NIL))
      ("@" "@" "32" (T T NIL NIL NIL T T NIL T T NIL))
      ("A" "A" "33" (T NIL T NIL NIL NIL T T NIL NIL NIL))
      ("B" "B" "34" (T NIL NIL NIL T NIL T T NIL NIL NIL))
      ("C" "C" "35" (T NIL NIL NIL T NIL NIL NIL T T NIL))
      ("D" "D" "36" (T NIL T T NIL NIL NIL T NIL NIL NIL))
      ("E" "E" "37" (T NIL NIL NIL T T NIL T NIL NIL NIL))
      ("F" "F" "38" (T NIL NIL NIL T T NIL NIL NIL T NIL))
      ("G" "G" "39" (T T NIL T NIL NIL NIL T NIL NIL NIL))
      ("H" "H" "40" (T T NIL NIL NIL T NIL T NIL NIL NIL))
      ("I" "I" "41" (T T NIL NIL NIL T NIL NIL NIL T NIL))
      ("J" "J" "42" (T NIL T T NIL T T T NIL NIL NIL))
      ("K" "K" "43" (T NIL T T NIL NIL NIL T T T NIL))
      ("L" "L" "44" (T NIL NIL NIL T T NIL T T T NIL))
      ("M" "M" "45" (T NIL T T T NIL T T NIL NIL NIL))
      ("N" "N" "46" (T NIL T T T NIL NIL NIL T T NIL))
      ("O" "O" "47" (T NIL NIL NIL T T T NIL T T NIL))
      ("P" "P" "48" (T T T NIL T T T NIL T T NIL))
      ("Q" "Q" "49" (T T NIL T NIL NIL NIL T T T NIL))
      ("R" "R" "50" (T T NIL NIL NIL T NIL T T T NIL))
      ("S" "S" "51" (T T NIL T T T NIL T NIL NIL NIL))
      ("T" "T" "52" (T T NIL T T T NIL NIL NIL T NIL))
      ("U" "U" "53" (T T NIL T T T NIL T T T NIL))
      ("V" "V" "54" (T T T NIL T NIL T T NIL NIL NIL))
      ("W" "W" "55" (T T T NIL T NIL NIL NIL T T NIL))
      ("X" "X" "56" (T T T NIL NIL NIL T NIL T T NIL))
      ("Y" "Y" "57" (T T T NIL T T NIL T NIL NIL NIL))
      ("Z" "Z" "58" (T T T NIL T T NIL NIL NIL T NIL))
      ("[" "[" "59" (T T T NIL NIL NIL T T NIL T NIL))
      ("\\" "\\" "60" (T T T NIL T T T T NIL T NIL))
      ("]" "]" "61" (T T NIL NIL T NIL NIL NIL NIL T NIL))
      ("^" "^" "62" (T T T T NIL NIL NIL T NIL T NIL))
      ("_" "_" "63" (T NIL T NIL NIL T T NIL NIL NIL NIL))
      ("NUL" "`" "64" (T NIL T NIL NIL NIL NIL T T NIL NIL))
      ("SOH" "a" "65" (T NIL NIL T NIL T T NIL NIL NIL NIL))
      ("STX" "b" "66" (T NIL NIL T NIL NIL NIL NIL T T NIL))
      ("ETX" "c" "67" (T NIL NIL NIL NIL T NIL T T NIL NIL))
      ("EOT" "d" "68" (T NIL NIL NIL NIL T NIL NIL T T NIL))
      ("ENQ" "e" "69" (T NIL T T NIL NIL T NIL NIL NIL NIL))
      ("ACK" "f" "70" (T NIL T T NIL NIL NIL NIL T NIL NIL))
      ("BEL" "g" "71" (T NIL NIL T T NIL T NIL NIL NIL NIL))
      ("BS" "h" "72" (T NIL NIL T T NIL NIL NIL NIL T NIL))
      ("HT" "i" "73" (T NIL NIL NIL NIL T T NIL T NIL NIL))
      ("LF" "j" "74" (T NIL NIL NIL NIL T T NIL NIL T NIL))
      ("VT" "k" "75" (T T NIL NIL NIL NIL T NIL NIL T NIL))
      ("FF" "l" "76" (T T NIL NIL T NIL T NIL NIL NIL NIL))
      ("CR" "m" "77" (T T T T NIL T T T NIL T NIL))
      ("SO" "n" "78" (T T NIL NIL NIL NIL T NIL T NIL NIL))
      ("SI" "o" "79" (T NIL NIL NIL T T T T NIL T NIL))
      ("DLE" "p" "80" (T NIL T NIL NIL T T T T NIL NIL))
      ("DC1" "q" "81" (T NIL NIL T NIL T T T T NIL NIL))
      ("DC2" "r" "82" (T NIL NIL T NIL NIL T T T T NIL))
      ("DC3" "s" "83" (T NIL T T T T NIL NIL T NIL NIL))
      ("DC4" "t" "84" (T NIL NIL T T T T NIL T NIL NIL))
      ("NAK" "u" "85" (T NIL NIL T T T T NIL NIL T NIL))
      ("SYN" "v" "86" (T T T T NIL T NIL NIL T NIL NIL))
      ("ETB" "w" "87" (T T T T NIL NIL T NIL T NIL NIL))
      ("CAN" "x" "88" (T T T T NIL NIL T NIL NIL T NIL))
      ("EM" "y" "89" (T T NIL T T NIL T T T T NIL))
      ("SUB" "z" "90" (T T NIL T T T T NIL T T NIL))
      ("ESC" "{" "91" (T T T T NIL T T NIL T T NIL))
      ("FS" "|" "92" (T NIL T NIL T T T T NIL NIL NIL))
      ("GS" "}" "93" (T NIL T NIL NIL NIL T T T T NIL))
      ("RS" "~" "94" (T NIL NIL NIL T NIL T T T T NIL))
      ("US" "DEL" "95" (T NIL T T T T NIL T NIL NIL NIL))
      ("FNC3" "FNC3" "96" (T NIL T T T T NIL NIL NIL T NIL))
      ("FNC2" "FNC2" "97" (T T T T NIL T NIL T NIL NIL NIL))
      ("SHIFT" "SHIFT" "98" (T T T T NIL T NIL NIL NIL T NIL))
      ("CODE-C" "CODE-C" "99" (T NIL T T T NIL T T T T NIL))
      ("CODE-B" "FNC4" "CODE-B" (T NIL T T T T NIL T T T NIL))
      ("FNC4" "CODE-A" "CODE-A" (T T T NIL T NIL T T T T NIL))
      ("FNC1" "FNC1" "FNC1" (T T T T NIL T NIL T T T NIL))
      ("START-A" "START-A" "START-A" (T T NIL T NIL NIL NIL NIL T NIL NIL))
      ("START-B" "START-B" "START-B" (T T NIL T NIL NIL T NIL NIL NIL NIL))
      ("START-C" "START-C" "START-C" (T T NIL T NIL NIL T T T NIL NIL))
      ("STOP" "STOP" "STOP" (T T NIL NIL NIL T T T NIL T NIL T T)))
  :test #'equalp)

(alexandria:define-constant +shift+ "SHIFT" :test #'string=)

(alexandria:define-constant +start-a+ "START-A" :test #'string=)

(alexandria:define-constant +start-b+ "START-B" :test #'string=)

(alexandria:define-constant +start-c+ "START-C" :test #'string=)

(alexandria:define-constant +code-a+ "CODE-A" :test #'string=)

(alexandria:define-constant +code-b+ "CODE-B" :test #'string=)

(alexandria:define-constant +code-c+ "CODE-C" :test #'string=)

(alexandria:define-constant +stop+ "STOP" :test #'string=)

(alexandria:define-constant +keyword-prefix+ "$" :test #'string=)

(define-condition code128-symbol-not-found (text-error) 
  ((variant
    :initarg :variant
    :reader  variant)
   (code-symbol
    :initarg :code-symbol
    :reader code-symbol))
  (:report (lambda (condition stream) 
	     (format stream "Symbol ~a (variant ~a) not found" (code-symbol condition) (variant condition)))))

(define-condition code128-parse-error (text-error) 
  ()
  (:report (lambda (condition stream) 
	     (format stream "Parsing error: ~a" (text condition)))))

(defun find-row (pos)
  (nth pos +code128-table+))

(defparameter *current-variant* :a)

(defun lookup (key &key (variant *current-variant*))
  (labels ((lookup-row (row)
	     (let ((pos (ecase variant
			  (:a (list 0))
			  (:b (list 1))
			  (:c (list 2))
			  (:any (list 0 1 2)))))
	       (find-if #'(lambda(p) (string= (nth p row) key)) pos))))
    (values
     (position-if #'lookup-row +code128-table+)
     (find-if #'lookup-row +code128-table+))))

(defun code128-checksum (codes-list &optional (checksum 0) (ct 0))
  (if codes-list
      (let ((val (first codes-list)))
	(code128-checksum (rest codes-list) (+ checksum 
					       (* val (if (= 0 ct) 1 ct)))
			  (1+ ct)))
      (mod checksum +code128-checksum-modulo+)))

(defmacro eval-to-keyword ((keywords str) &body not-found)
  `(cond
     ,@(mapcar #'(lambda (k) `((is-keyword-p ,k ,str) 
			       (values ,k (subseq ,str (length ,k)))))
	       keywords)
     (t 
      (values (progn ,@not-found) nil))))

(defun code128-lexer (codes)
  (cond 
    ((string= (char@ codes 0) +keyword-prefix+)
     (multiple-value-bind (keyword rest-codes)
	 (eval-to-keyword  ((+shift+ +start-a+ +start-b+ +start-c+
				     +code-a+ +code-b+ +code-c+ +shift+)
			    (subseq codes (length +keyword-prefix+))) +keyword-prefix+)
       (if rest-codes
	   (values keyword rest-codes)
	   (values keyword (subseq codes (length +keyword-prefix+))))))
    ((string= codes "")
     (values nil ""))
    (t
     (values (char@ codes 0) (subseq codes 1)))))

(defun tokenize-all (str &optional (codes '()))
  (multiple-value-bind (token rest-codes)
      (code128-lexer str)
    (if token
	(tokenize-all rest-codes (push token codes))
	(reverse codes))))

(defmacro valid-value-p (token codes)
  `(or
    ,@(mapcar #'(lambda (code)
		  `(string= ,code ,token))
	      codes)))

(defun start-code->current-variant (start-code)
  (cond
    ((string= start-code +start-a+)
     :a)
    ((string= start-code +start-b+)
     :b)
    ((string= start-code +start-c+)
     :c)))

(defun parse-code128 (tokens)
  (let* ((start-code (eval-to-keyword ((+start-a+ +start-b+ +start-c+) (first tokens)) nil)))
    (if start-code
	(progn
	  (setf *current-variant* (start-code->current-variant start-code))
	  (let ((raw-code  (append (list (lookup start-code :variant :any))
				   (parse-values-type-any (rest tokens)))))
	    (append raw-code (list (code128-checksum raw-code) (lookup +stop+)))))
	(error 'code128-parse-error :text (format nil "not starting with a valid starting code (~a)" (first tokens))))))

(defun parse-values-type-any (tokens)
  (if tokens
      (let ((keyword (eval-to-keyword ((+shift+ +code-a+ +code-b+ +code-c+) (first tokens)) nil))
	    (saved-current-variant *current-variant*))
	(if keyword
	    (cond
	      ((string= keyword +shift+)
	       (append
		(parse-shift-code-type-a tokens)))
	      ((string= keyword +code-a+)
	       (setf *current-variant* :a)
	       (append
		(let ((*current-variant* saved-current-variant))
		  (list (lookup +code-a+)))
		(parse-values-type-any (rest tokens))))
	      ((string= keyword +code-b+)
	       (setf *current-variant* :b)
	       (append
		(let ((*current-variant* saved-current-variant))
		  (list (lookup +code-b+)))
		(parse-values-type-any (rest tokens))))
	      ((string= keyword +code-c+)
	       (setf *current-variant* :c)
	       (append
		(let ((*current-variant* saved-current-variant))
		  (list (lookup +code-c+)))
		(parse-value-type-c (rest tokens)))))
	    
	    
	    (if (eq *current-variant* :c)
		(parse-value-type-c tokens)
		(let ((lookup-val (lookup (first tokens) :variant *current-variant*)))
		  (if lookup-val
		      (append (list lookup-val)
			      (parse-values-type-any (rest tokens)))
		      (error 'code128-symbol-not-found :code-symbol (first tokens) :variant *current-variant*))))))
      nil))

(defun parse-value-type-c (tokens)
  (if (>= (length tokens) 2)
      (progn
	(let ((code (concatenate 'string (first tokens) (second tokens))))
	  (if (cl-ppcre:scan +code128-code-c-valid-regexp+ code)
	      (append
	       (list (lookup code :variant *current-variant*))
	       (parse-values-type-any (subseq tokens 2)))
	      
	      (error 'code128-parse-error 
		     :text (format nil "Codes is not a two digits string ~a" code)))))

      (error 'code128-parse-error 
	     :text (format nil "Not enough digits for code type c ~a" tokens))))

(defun shift-current-variant ()
  (if (eq *current-variant* :a)
      (setf *current-variant* :b)
      (setf *current-variant* :a)))

(defmacro with-shifted-current-variant (&body body)
  `(let ((*current-variant* (if (eq *current-variant* :a) :b :a)))
     ,@body))


(defun parse-shift-code-type-a (tokens)
  (let ((keyword (eval-to-keyword ((+shift+) (first tokens)) nil)))
    (if keyword
	(append 
	 (list (lookup +shift+))
	 (let* ((rest-tokens (rest tokens))
		(lookup-val (with-shifted-current-variant 
			      (lookup (first rest-tokens) :variant *current-variant*))))
	   (if lookup-val
	       (append (list lookup-val)
		       (parse-values-type-any (rest rest-tokens)))
	       (error 'code128-symbol-not-found :code-symbol (first tokens) :variant :b))))
	(error 'code128-parse-error :text (format nil "~a expected ~a got instead" +shift+ (first tokens))))))

(defclass code128 (barcode)
  ((code
    :initform '()
    :accessor code)))

(defgeneric bars-list (object))

(defgeneric quiet-zone (object))

(defmethod parse ((object code128) codes)
  (with-accessors ((code code) 
		   (width width)) object
    (setf code (parse-code128 (tokenize-all codes)))
    (setf width (+ (* 2 (quiet-zone object))
		   (barcode-width (bars-list object) (bar-width object))))))

(defmethod bars-list ((object code128))
  (reduce #'append
	  (mapcar #'(lambda (cd) (fourth (find-row cd)))
		  (code object))))

(defmethod quiet-zone ((object code128))
  (let ((rel (* +code128-min-quiet-zone-rel+ (bar-width object))))
    (if (< rel +code128-min-quiet-zone-abs+)
	+code128-min-quiet-zone-abs+
	rel)))
  
(defmethod draw ((object code128) (doc ps:psdoc))
  (let ((all-bars (bars-list object)))
    (ps:save doc)
    (ps:setcolor doc ps:+color-type-fillstroke+ cl-colors:+white+)
    (ps:rect doc 0 0 (quiet-zone object) (height object))
    (ps:fill-path doc)
    (ps:translate doc (quiet-zone object) 0)
    ;; ---
    (draw-bars doc all-bars (bar-width object) (height object))
    (ps:save doc)
    (ps:setcolor doc ps:+color-type-fillstroke+ cl-colors:+white+)
    (ps:translate doc (- (width object) (* 2 (quiet-zone object))) 0)
    (ps:rect doc 0 0 (quiet-zone object) (height object))
    (ps:fill-path doc)
    (ps:restore doc)
    ;; ---
    (ps:restore doc)))
