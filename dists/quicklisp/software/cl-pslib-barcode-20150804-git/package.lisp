;; This software is Copyright (c) cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL


(defpackage :cl-pslib-barcode
  (:use :cl :cl-colors)
  (:nicknames :brcd)
  (:export
   :barcode
   :width
   :height
   :parse
   :draw
   :code128-symbol-not-found
   :code128-parse-error
   :code128))


   