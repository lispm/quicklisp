# simple-logger

A simple message logging system.

## Overview

This library provides a simple method of defining log messages and later showing
them depending on the verbosity level.

## Install

``` lisp
(ql:quickload :simple-logger)
```

## Usage

To define a log message, you use `DEFINE-MESSAGE`, supplying a level, category, and message.

`LEVEL` Can be one of `:TRACE`, `:DEBUG`, `:INFO`, `:WARN`, `:ERROR`, or `:FATAL`.

`CATEGORY` A keyword symbol identifier for this message, to be referred to later when you want to
emit it.

`MESSAGE` A string with the message, and any `CL:FORMAT` directives you want to include.

The global logging level by default is set to `:INFO`. This means that, unless the value of
`*CURRENT-LEVEL*` is changed, then any messages defined with `DEFINE-MESSAGE` having a level lower
than `:INFO` will not be displayed when the message is emitted.

To emit a message previously defined with `DEFINE-MESSAGE`, you use `EMIT`, giving it the category
of the message you want to display, along with any other variables that are consumed by the
`CL:FORMAT` string defined.

Example:

``` lisp
;; Change the global logging level to display messages with a level of :INFO or higher.
(setf *current-level* :info)

;; Define some messages.
(define-message :error :logger.example.error
  "Something bad happened. Arguments: ~{~A~^, ~}")
(define-message :debug :logger.example.hidden
  "This message is not displayed.")

;; Emit the example error message. This will print the following:
;; [ERROR] [2016-12-05 21:14:12] Something bad happened. Arguments: 1, 2, 3
(emit :logger.example.error '(1 2 3))

;; Emit the example debug message. This will not do anything at all because *CURRENT-LEVEL* is
;; higher than :DEBUG.

(emit :logger.example.hidden)
```

The global message level can be changed at a later time to start displaying messages that would have
been hidden.

## License

Copyright © 2016-2018 [Michael Fiano](mailto:mail@michaelfiano.com).

Licensed under the MIT License.
