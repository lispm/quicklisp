;;;  uffi.lisp --- output module for UFFI

;;;  Copyright (C) 2004 by Walter C. Pelissero

;;;  Author: Aurelio Bignoli <aurelio@bignoli.it>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-4840B131CC497716AE71680AFD4A6F34.lisp,v 1.11 2004/04/09 10:49:04 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-4840B131CC497716AE71680AFD4A6F34.lisp,v 1.11 2004/04/09 10:49:04 wcp Exp $")

(in-package :binge-backend)

(defclass uffi-backend (backend) ())

(defmethod use-packages ((backend uffi-backend))
  (declare (ignore backend))
  '("COMMON-LISP" "UFFI"))

(defmethod ffi-sizeof (thing (backend uffi-backend))
  (format nil "(size-of-foreign-type ~A)" thing))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod c-object->ffi-notation ((object c-argument) (backend uffi-backend))
  (c-object->ffi-notation (c-type object) backend))

(defmethod c-object->ffi-notation ((object c-slot) (backend uffi-backend))
  (format nil "(~A ~A)"
	  (c-name object)
	  (c-object->ffi-notation (c-type object) backend)))

(defmethod c-object->ffi-notation ((object c-pointer) (backend uffi-backend))
  (case (c-type object)
    (:void
     ;; pointer to anything
     ":pointer-void")
    ((:char :unsigned-char)
     ":cstring")
    (t
     (format nil "(* ~A)" (c-object->ffi-notation (c-type object) backend)))))

(defmethod c-object->ffi-notation ((object c-type-ref) (backend uffi-backend))
  (c-object-lisp-name object backend))

(defmethod c-object->ffi-notation ((object c-function) (backend uffi-backend))
  (error 'binge-compiler:compilation-unsupported-error :item object))

(defmethod c-object->ffi-notation ((object c-array) (backend uffi-backend))
  (format nil "(:array ~A~{ ~A~})" 
	  (c-object->ffi-notation (c-type object) backend)
	  (convert-array-dimensions (c-dimensions object) backend)))

(defmethod c-object->ffi-notation ((object symbol) (backend uffi-backend))
  (case object
    (:void ":void")
    (:char ":char")
    (:short ":short")
    (:int ":int")
    (:long ":long")
    (:unsigned-char ":unsigned-char")
    (:unsigned-short ":unsigned-short")
    (:unsigned-int ":unsigned-int")
    (:unsigned-long ":unsigned-long")
    (:float ":float")
    (:double ":double")
    (t
     (error 'binge-compiler:compilation-unsupported-error
	    :item object))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun uffi-format-compound (type declaration backend)
  (format nil "(def-~A ~A~%  ~@[~<~@{~A~^ ~_~}~:>~])"
	  type
	  (c-object-lisp-name declaration backend)
	  (mapcar #'(lambda (slot)
		      (c-object->ffi-notation slot backend))
		  (c-slots declaration))))

(defmethod c-object->ffi-notation ((decl c-typedef-declaration) (backend uffi-backend))
  (format nil "(def-foreign-type ~A ~A)"
	  (c-object-lisp-name decl backend) 
	  (c-object->ffi-notation (c-type decl) backend)))

(defmethod c-object->ffi-notation ((decl c-struct-declaration) (backend uffi-backend))
  (uffi-format-compound "struct" decl backend))

(defmethod c-object->ffi-notation ((decl c-union-declaration) (backend uffi-backend))
  (uffi-format-compound "union" decl backend))

(defmethod c-object->ffi-notation ((decl c-enum-declaration) (backend uffi-backend))
  (format nil "(def-enum ~A ~<~@{~A~^ ~:_~}~:>)"
	  (c-object-lisp-name decl backend)
	  (mapcar #'(lambda (c)
		      (if (consp c)
			  (format nil "(:~@[~A~]~A ~A)" (backend-prefix backend) (car c) (cadr c))
			  (format nil ":~@[~A~]~A" (backend-prefix backend) c)))
		  (c-constants decl))))

(defmethod c-object->ffi-notation ((decl c-function-declaration) (backend uffi-backend))
  (format nil "(def-function (~S ~A) ~&  (~<~@{~A~^ ~:_~}~:>) :returning ~A)"
	  (c-name decl)
	  (c-object-lisp-name decl backend)
	  (loop
	     for arg in (c-arguments decl)
	     for i from 1 by 1
	     collect (if (c-name arg)
			 (format nil "(~A ~A)" (c-name arg)
				 (c-object->ffi-notation (c-type arg) backend))
			 ;; the use of a dash in the name of arguments
			 ;; guarantees we won't have any name clash
			 (format nil "(arg-~A ~A)" i
				 (c-object->ffi-notation (c-type arg) backend))))
	  (c-object->ffi-notation (c-type decl) backend)))

(defmethod c-object->ffi-notation ((decl c-variable-declaration) (backend uffi-backend))
  (format nil "(def-foreign-var (~S ~A) ~A)"
	  (c-name decl)
	  (c-object-lisp-name decl backend) 
	  (c-object->ffi-notation (c-type decl) backend)))

(setf binge-compiler:*backend* (make-instance 'uffi-backend))
(export 'uffi-backend)		     ; so that we can use this backend

