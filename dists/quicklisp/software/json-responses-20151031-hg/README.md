This library provides a standard set of JSON responses for Hunchentoot
applications. It's been tested on SBCL, CCL, CLISP, and LispWorks but should
work anywhere that Hunchentoot and CL-JSON do. If it doesn't work on your Lisp
of choice open an issue and I'll investigate.

Best enjoyed with [Quicklisp](http://www.quicklisp.org/beta/):

    (ql:quickload 'json-responses)

Or, to run the included test suite:

    (ql:quickload 'json-responses)
    (ql:quickload 'json-responses-test)

One generic function (json-response) can accept :status, :headers, :data, and
:error keyword arguments:

    (json-response :status +http-ok+
                   :headers '(("X-HELLO" . "world"))
                   :data "Hello, world!"
                   :error "Not applicable here, but for demonstration...")

Which would respond with a 200 status code, an X-HELLO header with the
value world, a content-type of application/json, and a JSON body like:

    {
        "data": "Hello, world!",
        "error": "Not applicable here, but for demonstration..."
    }

All keyword arguments are optional and have sane defaults. If both data and error
are omitted then the body is simply null.

The remaining functions can accept :headers, :data, and :error keyword arguments
and have status preset to an appropriate value. :data and :headers default to
nil, :error defaults to nil for 100, 200, and 300-level responses and a simple
error message for 400 and 500-level responses.

The following functions are included:

- json-response
- continue-response
- switching-protocols-response
- ok-response
- created-response
- accepted-response
- non-authoritative-information-response
- no-content-response
- reset-content-response
- partial-content-response
- multiple-choices-response
- moved-permanently-response
- moved-temporarily-response
- see-other-response
- not-modified-response
- use-proxy-response
- temporary-redirect-response
- bad-request-response
- authorization-required-response
- forbidden-response
- not-found-response
- method-not-allowed-response
- not-acceptable-response
- proxy-authentication-required-response
- request-timeout-response
- conflict-response
- gone-response
- length-required-response
- precondition-failed-response
- request-entity-too-large-response
- request-uri-too-long-response
- unsupported-media-type-response
- requested-range-not-satisfiable-response
- expectation-failed-response
- internal-server-error-response
- not-implemented-response
- bad-gateway-response
- service-unavailable-response
- gateway-timeout-response
- http-version-not-supported-response