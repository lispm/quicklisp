;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          cl-photo-tests.lisp
;;;; Purpose:       Cl-Photo tests file
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  Apr 2003
;;;;
;;;; $Id$
;;;;
;;;; This file is Copyright (c) 2000-2003 by Kevin M. Rosenberg
;;;; *************************************************************************

(defpackage #:cl-photo-tests
  (:use #:cl-photo #:cl #:rtest #:kmrcl))
(in-package #:cl-photo-tests)

(rem-all-tests)

(defun verify-results (alist blist)
  (every #'identity
         (mapcar
          (lambda (a b) (numbers-within-percentage a b 0.01))
          alist blist)))

(defmacro def-numeric-test (name test &rest values)
  `(deftest ,name
            (let ((results (multiple-value-list ,test)))
              (verify-results results ',values))
            t))

(def-numeric-test
 :gl.1 (gaussian-lens :focal-length 50 :object-distance 100) 100)

(def-numeric-test
 :gl.2 (gaussian-lens :focal-length 50 :image-distance 200) 66.66667)

(def-numeric-test
 :gl.3 (gaussian-lens :object-distance 100 :image-distance 100) 50)

(def-numeric-test
 :aov.1 (aov 50 36 24) 39.5977 26.9915 46.7930)

(def-numeric-test
 :fov.1 (fov 50 24 16 :magnification 1 :units :mm)
 24 16 28.8444 1 100 100)

(def-numeric-test
 :fov.2 (fov 50 24 16 :magnification 1 :units :feet)
  0.07874 0.052494 0.094634 1 0.328084 100)

(def-numeric-test
 :fov.3 (fov 50 24 16 :image-distance 100 :units :mm)
 24 16 28.8444 1 100 100)

(def-numeric-test
 :fov.4 (fov 50 24 16 :image-distance 100 :units :feet)
  0.07874 0.052494 0.094634 1 0.328084 100)

(def-numeric-test
 :fov.5 (fov 50 24 16 :object-distance 100 :units :mm)
 24 16 28.8444 1 100 100)

(def-numeric-test
 :fov.6 (fov 50 24 16 :object-distance (cl-photo::mm->feet 100) :units :feet)
  0.07874 0.052494 0.094634 1 0.328084 100)

(def-numeric-test
 :mag.1 (magnification :focal-length 50 :image-distance 100 :units :mm) 1)

(def-numeric-test
 :mag.2 (magnification :focal-length 50 :image-distance 100 :units :feet) 1)

(def-numeric-test
 :mag.3 (magnification :focal-length 50 :object-distance 100 :units :mm) 1)

(def-numeric-test
 :mag.4 (magnification :focal-length 50 :object-distance (cl-photo::mm->feet 100)
                       :units :feet) 1)

(def-numeric-test
 :mag.5 (magnification :image-distance 100 :object-distance 100 :units :mm) 1)

(def-numeric-test
 :mag.6 (magnification :image-distance 100 :object-distance (cl-photo::mm->feet 100)
                       :units :feet) 1)

(def-numeric-test
    :cu.1 (close-up :focal-length 65 :magnification 5 :units :mm)
  65 78 390 5 6)

(def-numeric-test
    :cu.2 (close-up :focal-length 65 :object-distance 78 :units :mm)
  65 78 390 5 6)

(def-numeric-test
    :cu.3 (close-up :focal-length 65 :image-distance 390 :units :mm)
  65 78 390 5 6)

(def-numeric-test
    :cu.4 (close-up :object-distance 78 :image-distance 390 :units :mm)
  65 78 390 5 6)

(def-numeric-test
    :cu.5 (close-up :object-distance 78 :magnification 5 :units :mm)
  65 78 390 5 6)

(def-numeric-test
    :cu.6 (close-up :image-distance 390 :magnification 5 :units :mm)
  65 78 390 5 6)

(def-numeric-test
    :cu.7 (close-up :focal-length 65 :magnification 5 :units :feet)
  65 0.2559055 390 5 6)

(def-numeric-test
    :cu.8 (close-up :focal-length 65 :object-distance 0.2559055 :units :feet)
  65 0.2559055 390 5 6)

(def-numeric-test
    :cu.9 (close-up :focal-length 65 :image-distance 390 :units :feet)
  65 0.2559055 390 5 6)

(def-numeric-test
    :cu.10 (close-up :object-distance 0.2559055 :image-distance 390 :units :feet)
  65 0.2559055 390 5 6)

(def-numeric-test
    :cu.11 (close-up :object-distance 0.2559055 :magnification 5 :units :feet)
  65 0.2559055 390 5 6)

(def-numeric-test
    :cu.12 (close-up :image-distance 390 :magnification 5 :units :feet)
  65 0.2559055 390 5 6)

