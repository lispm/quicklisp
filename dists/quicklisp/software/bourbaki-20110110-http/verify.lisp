;;; Copyright (c) Juha Arpiainen 2005-06

;;; This file is part of Bourbaki.

;;; Bourbaki is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.

;;; Bourbaki is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Bourbaki; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; -------

;;; *****
;;; Theorem verification
;;; *****

;;; TODO: check definition soundness

(in-package :bourbaki)

(defun verify-subproof (pf varlist dvc num)
  (aif (subproof-ref pf)
    ;; direct justification
    (progn
      (unless (wffp it)
        (error "Step not wff: ~d" it)
        (return-from verify-subproof nil))
      (let ((smap (smap (car it) (cdr it))))
        (unless (dvc-satisfied (car it) smap dvc varlist)
          (error "Dvc not satisfied")
          (return-from verify-subproof nil))
        (dolist (h (context-hypo (car it)))
          (unless (provenp (replace-vars h smap))
            (error "Missing hypothesis ~d at step ~d" h it)
            (return-from verify-subproof nil)))
        (let ((res (mapcar #'(lambda (x) (replace-vars x smap)) (context-assert (car it)))))
          (dolist (a (subproof-assert pf))
            (unless (member a res :test #'wff-equal)
              (error "Assertion ~d missing at step ~d" a it)
              (return-from verify-subproof nil)))))
      t)
    ;; sequence of subproofs
    (let ((*current-proof* (cons (make-pattern-tree) *current-proof*)))
      (loop for line in (subproof-sub pf)
            for i from 1
            do (when (verify-subproof line varlist dvc (cons i num))
                 (dolist (a (subproof-assert line))
                   (insert-wff (car *current-proof*) a))))
      (dolist (a (subproof-assert pf))
        (unless (provenp a)
          (error "Assertion ~d missing at step ~a~%" a num)
          (return-from verify-subproof nil)))
      t)))

;;; TODO: check for circular definitions
(defun verify-def (defn)
  "Verify soundness of a definition"
  ;; I - check that definition is of the form A == B
  (unless (= (length (context-assert defn)) 1)
    (error "A definition must have exactly 1 assertion."))
  (let ((assr (car (context-assert defn))))
    (unless (= (length assr) 3)
      (error "Definition not of the form [eq lhs rhs]"))
    (let ((eq-op (first assr))
          (lhs (second assr))
          (rhs (third assr)))
      ;; II - check that lhs matches with the symbol being defined
      (unless (eq eq-op (gethash :eq-op (context-meta (wff-type lhs))))
        (error "Invalid equality operator ~a" (context-name eq-op)))
      (unless (eq defn (gethash :def (context-meta (car lhs))))
        (error "Wrong symbol ~a being defined" (context-name (car lhs))))
      (unless (wff-equal (cdr lhs) (makevarlist (car lhs)))
        (error "Mismatch in definition argument lists"))
      ;; III - check bound variable conditions on the rhs
      (let ((free (potential-vars (context-vars defn) (context-distinct defn) rhs)))
        (loop for var in free
              if (gethash :bound (context-meta var))
              do (error "Bound variable ~a occurs free in ~a" (context-name var) rhs)) t))))

;; TODO: cleaner error printing
(defun verify-1 (thr)
  "Verify that the hypotheses and assertion of thr are wff and
the proof is correct."

  (if (null thr) (return-from verify-1 t))
  ;;; XXX currently don't check even well-formedness of any contexts
  ;;; other than :theorem, :axiom, :definition
  (if (not (member (context-class thr) '(:theorem :axiom :definition)))
    (return-from verify-1 t))
  (let ((*current-context* thr))
    (format t "Theorem: ~S~%" (context-name thr))
  
    (cond ((notevery #'wffp (context-assert thr)) (error "Assertion not wff at ~d" thr))
          ((notevery #'wffp (context-hypo thr)) (error "Hypothesis not wff"))
		
          ((eq (context-class thr) :axiom) t) ; No proof check for axioms
          ((eq (context-class thr) :definition) (verify-def thr)) ; Check for definition soundness
          (t (let ((*current-proof* (list (make-pattern-tree))))
               (dolist (h (context-hypo thr))
                 (insert-wff (car *current-proof*) h))
               (verify-subproof (context-proof thr) (context-vars thr) (context-distinct thr) nil))))))

(defun verify (ref &optional (nested t))
  (let ((thr (mkcontext ref)))
    (if (null thr)
      t
      (if nested
        (walk-proof-tree thr #'verify-1 t)
        (verify-1 thr)))))
