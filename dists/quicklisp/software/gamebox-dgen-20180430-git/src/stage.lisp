(in-package :box.dgen)

(defclass stage ()
  ((%width :reader width
           :initarg :width
           :initform 49)
   (%height :reader height
            :initarg :height
            :initform 49)
   (%grid :reader grid)
   (%generator :reader generator
               :accessor %generator)
   (%seed :reader seed
          :initarg :seed
          :initform (genie:make-seed))
   (%regions :accessor regions
             :initform (make-hash-table))
   (%graphs :reader graphs
            :accessor %graphs
            :initform nil)
   (%corridor-windiness :accessor corridor-windiness
                        :initarg :corridor-windiness
                        :initform 0)
   (%room-size-min :reader room-size-min
                   :initarg :room-size-min
                   :initform 3)
   (%room-size-max :reader room-size-max
                   :initarg :room-size-max
                   :initform 11)
   (%room-density :reader room-density
                  :initarg :room-density
                  :initform 0.65)
   (%door-rate :reader door-rate
               :initarg :door-rate
               :initform 0.5)
   (%hollow-walls :reader hollow-walls
                  :initarg :hollow-walls
                  :initform nil)
   (%rooms :accessor stage-rooms
           :initform nil)
   (%dead-ends :accessor dead-ends
               :initform nil))
  (:documentation "A stage represents the generated grid of cells and the data used to render it."))

(defun make-rng (stage)
  (setf (%generator stage) (genie:make-generator (seed stage))))

(defun make-grid (stage)
  (with-slots (%width %height %grid) stage
    (setf %grid (make-array (list %width %height)))
    (dotimes (x %width)
      (dotimes (y %height)
        (make-cell stage x y)))))

(defun ensure-dimensions (stage)
  (with-slots (%width %height %room-size-max) stage
    (flet ((ensure (dimension)
             (let ((size (max (+ (* %room-size-max 2) 3) dimension)))
               (if (evenp size)
                   (incf size)
                   size))))
      (setf %width (ensure %width)
            %height (ensure %height)))))

(defun validate (stage)
  (with-slots (%room-size-min %room-size-max %room-density %corridor-windiness %door-rate) stage
    (setf %room-size-min (alexandria:clamp %room-size-min 3 99)
          %room-size-max (alexandria:clamp %room-size-max %room-size-min 99)
          %room-density (alexandria:clamp %room-density 0.1 1.0)
          %door-rate (alexandria:clamp %door-rate 0 1)
          %corridor-windiness (alexandria:clamp %corridor-windiness 0.0 1.0))
    (when (evenp %room-size-min)
      (incf %room-size-min))
    (when (evenp %room-size-max)
      (incf %room-size-max))
    (ensure-dimensions stage)))

(defun build (stage)
  (add-rooms stage)
  (create-corridors stage)
  (connect-regions stage)
  (erode-dead-ends stage)
  (create-stairs stage)
  (erode-walls stage))

(simple-logger:define-message :info :stage.status
  "Stage generated: ~Ax~A")

(defun make-stage (&rest attrs)
  "Generate the stage.

Accepts several optional arguments to influence the generator:

:SEED specifies the random seed integer to use. This makes it possible to regenerate the same
dungeon at a later time. If this is not specified, it will be chosen randomly.

:WIDTH specifies the width of the stage in cells.

:HEIGHT specifies the height of the stage in cells.

:CORRIDOR-WINDINESS specifies how windy corridors will be, as a range from 0.0 to 1.0. This has
minimal effect for densely packed rooms - see :ROOM-DENSITY below to control this. Default value is
0.0.

:ROOM-DENSITY specifies how densely rooms should be packed in the stage, as a range from 0.1 (10%)
to 1.0 (100%). Note: This is only an estimation. Default value is 0.65.

:ROOM-SIZE-MIN specifies the minimum size in cells the width or height of a room is allowed to be,
within the range 3 to 99. Note: This should be supplied as an odd integer value, else it will be
incremented by 1. Default value is 3.

:ROOM-SIZE-MAX specifies the maximum size in cells the width or height of a room is allowed to be,
within the range ROOM-SIZE-MIN to 99. Note: This should be supplied as an odd integer value, else it
will be incremented by 1. Default value is 11.

:DOOR-RATE specified the percentage of junctions that should be doors, as a range from 0.0 to 1.0. A
junction is an intersection between a room and a corridor. Default value is 0.5."
  (let ((stage (apply #'make-instance 'stage attrs))
        (*region* *region*))
    (make-rng stage)
    (validate stage)
    (make-grid stage)
    (build stage)
    (simple-logger:emit :stage.status (width stage) (height stage))
    stage))

(defun print-stage (stage)
  "Print the generated stage using Unicode line drawing characters."
  (format t "~&")
  (loop :for row :from (1- (height stage)) :downto 0
        :do (loop :for col :below (width stage)
                  :for cell = (cell stage col row)
                  :do (format t "~A"
                              (cond ((featuresp cell :door-horizontal) "──")
                                    ((featuresp cell :door-vertical) "│ ")
                                    ((featuresp cell :stairs-up) "↑↑")
                                    ((featuresp cell :stairs-down) "↓↓")
                                    ((featuresp cell :corridor :room :junction) "  ")
                                    (t "██"))))
            (format t "~%")))
