;;;; json-responses.asd

(asdf:defsystem #:json-responses
  :description "Canned JSON responses for Hunchentoot"
  :author "Brad Melanson <bradmelanson@icloud.com>"
  :license "MIT"
  :depends-on (#:hunchentoot
               #:cl-json)
  :pathname "src/"
  :serial t
  :components ((:file "package")
               (:file "json-responses")))

(asdf:defsystem #:json-responses-test
  :description "Test suite for JSON Responses library"
  :author "Brad Melanson <bradmelanson@icloud.com>"
  :license "MIT"
  :depends-on (#:json-responses
               #:fiveam)
  :pathname "test/"
  :serial t
  :components ((:file "package")
               (:file "tests")))
