;;;; -*- Mode: Lisp -*-

;;;; enumeration-pkg.lisp --
;;;; The ENUMERATIONS package.
;;;;
;;;; See file COPYING for copyright and licensing information.

(defpackage "IT.UNIMIB.DISCO.MA.COMMON-LISP.EXTENSIONS.DATA-AND-CONTROL-FLOW.ENUMERATIONS"
  (:use "COMMON-LISP")
  (:nicknames
   "ENUM"
   "enum"
   "CL.EXTENSIONS.ENUMERATIONS"
   "CL.EXT.ENUMERATIONS"
   "CL.EXTENSIONS.DACF.ENUMERATIONS"
   "CL.EXT.DACF.ENUMERATIONS"
   "COMMON-LISP.EXTENSIONS.DATA-AND-CONTROL-FLOW.ENUMERATIONS")
  (:documentation "The CL Extensions Enumeration Package.

The package containing the API for a generic enumeration protocol in
Common Lisp.


Notes:

The package name is long because it indicates how to position the
library functionality within the breakdown in chapters of the ANSI
specification.

The lower-case \"enum\" package nickname is provided in order to appease
ACL Modern mode.

The \"CL.EXT...\" nicknames are provided as a suggestion about how to
'standardize' package names according to a meaninguful scheme.
")

  (:export
   #:enumeration
   #:enumerationp

   #:enumerate
   #:has-more-elements-p
   #:has-next-p
   #:next
   ;; #:next-element
   #:current
   #:reset
   #:previous
   #:has-previous-p

   #:no-such-element

   #:bi-directional-enumeration
   #:functional-enumeration
   #:bounded-enumeration
   #:array-table-enumeration
   #:list-enumeration
   #:number-enumeration
   #:vector-enumeration
   #:string-enumeration
   #:simple-string-enumeration
   #:hash-table-enumeration

   #:bi-directional-enumeration-p
   #:functional-enumeration-p
   #:bounded-enumeration-p
   #:array-table-enumeration-p
   #:list-enumeration-p
   #:number-enumeration-p
   #:vector-enumeration-p
   #:string-enumeration-p
   #:simple-string-enumeration-p
   #:hash-table-enumeration-p

   #:foreach
   #:range
   )
  )

;;; end of file -- enumerations-pkg.lisp --
