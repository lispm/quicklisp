;;; ************
;;; Metatheorems
;;; ************

;;; Bourbaki -- Classical Propositional Calculus
;;; implication "→" -- negation "¬" -- biconditional "↔"
;;; conjunction "∧" -- disjunction "∨"
;;; Converted from the "set.mm" file of Metamath
;;; Now also export the axioms and metatheorems

(module "prop"
  (symkind "WFF")
  (export [!!prop-ax  ])
  
 (meta (:comment . "Propositional Calculus.")
        (:description . "To keep this module
self-contained, most of the results in <a href=\"equiv.html\">equiv</a>
and <a href=\"order.html\">order</a> are duplicated here."))

  (local "im"
    (meta (:comment . "Properties of the implication.")))
  (local "bi"
    (meta (:comment . "Properties of the biconditional.")))
  (local "an"
    (meta (:comment . "Properties of the conjunction.")))
  (local "or"
    (meta (:comment . "Properties of the disjunction.")))
  (local "xor"
    (meta (:comment . "Properties of negated equivalence.")))
  (local "lemmas")

  (th-pattern !ax1)
  (th-pattern !ax2)
  (th-pattern !ax3)

;;; **********
;;; Some metatheorems
;;; **********

  ;; TODO: multiple assertions
  (metath "infer" (ref &rest goals)
    (declare (ignore goals))
    (let* ((pfl (parse-subproof ref))
           (assert (car (subproof-assert pfl))))
      ;; match implication
      (in-context *meta-context*
        (match-case assert
          ([→ ?x ?y] (linear-subproof (?x) (?y)
                       pfl
                       [ax-mp ,?x ,?y]))
          ;; else match biconditional
          ([↔ ?x ?y] (acond
            ((provenp ?x) (linear-subproof (?x) (?y)
                            pfl
                            [infer bi> ,?x ,?y]
                            [ax-mp ,?x ,?y]))
            ((provenp ?y) (linear-subproof (?y) (?x)
                            pfl
                            [infer bi< ,?x ,?y]
                            [ax-mp ,?y ,?x]))
            (t (error "Infer: cannot find ~a or ~a" ?x ?y))))
          (t pfl)))))

  ;; TODO: multiple assertions
  (metath "ded" (ref var)
    (let* ((pfl (parse-subproof ref))
           (assert (car (subproof-assert pfl))))
      ;; match implication
      (in-context *meta-context*
        (if-match [→ ?x ?y] assert
          (linear-subproof ([→ ,var ,?x]) ([→ ,var ,?y])
            pfl
            [!im!tri ,var ,?x ,?y])
          ;; else match bi-implication
          (if-match [↔ ?x ?y] assert
            (let ((lhs [→ ,var ,?x])
                  (rhs [→ ,var ,?y]))
              (if (provenp lhs)
                (linear-subproof (lhs) (rhs)
                  pfl
                  [infer bi> ,?x ,?y]
                  [!im!tri ,var ,?x ,?y])
                (if (provenp rhs)
                  (linear-subproof (rhs) (lhs)
                    pfl
                    [infer bi< ,?x ,?y]
                    [!im!tri ,var ,?y ,?x])))))))))

  ;; TODO: multiple assertions
  (metath "+syl" (ref var)
    (let* ((pfl (parse-subproof ref))
           (assert (car (subproof-assert pfl))))
      ;; match implication
      (in-context *meta-context*
        (if-match [→ ?x ?y] assert
          (linear-subproof ([→ ,?y ,var]) ([→ ,?x ,var])
            pfl
            [!im!tri ,?x ,?y ,var])
          ;; else match bi-implication
          (if-match [↔ ?x ?y] assert
            (let ((lhs [→ ,?x ,var])
                  (rhs [→ ,?y ,var]))
              (if (provenp lhs)
                (linear-subproof (lhs) (rhs)
                  pfl
                  [infer bi< ,?x ,?y]
                  [!im!tri ,?y ,?x ,var])
                (if (provenp rhs)
                  (linear-subproof (rhs) (lhs)
                    pfl
                    [infer bi> ,?x ,?y]
                    [!im!tri ,?x ,?y ,var])))))))))

  (metath "b2i" (ref)
    (in-context *meta-context*
    (let* ((pfl (parse-subproof ref)))
      (subproof (:assert (subproof-assert pfl))
        (dolist (h (subproof-hypo pfl))
          (match-case h 
            ([→ ?x ?y] (acond
              ((provenp h) (hypo h))
              ((provenp ?y) (hypo it) (line [infer ax1 ,?y ,?x]))
              ;; Convert biconditionals to implications
              ((provenp [↔ ,?x ,?y]) (hypo it) (line [infer bi> ,?x ,?y]))
              ((provenp [↔ ,?y ,?x]) (hypo it) (line [infer bi< ,?y ,?x]))
              (t (error "b2i: cannot prove ~a" h))))
            ;; Handle biconditionals in reverse direction
            ([↔ ?x ?y] (acond
              ((provenp h) (hypo h))
              ((provenp [↔ ,?y ,?x]) (hypo it) (line [infer !bi!com ,?y ,?x]))
              (t (error "b2i: cannot prove ~a" h))))
            (t (hypo h))))
        (line pfl)))))

  (defun dedth-p (thr)
    (gethash :ded (context-meta thr)))

  ;;; Convert a proof segment for {h} => {a}
  ;;; into one for {str -> h} => {str -> a}
  (metath "dedth" (prf str)
    (in-context *meta-context*
      (let ((*current-proof* (list (make-pattern-tree))))
        (labels ((rec (prf)
                   (subproof (:assert (loop for i in (subproof-assert prf)
                                            collect [→ ,str ,i]))
                     ;; Case 1: theorem reference
                     (aif (subproof-ref prf)
                       (progn
;                         (format t "Ded: direct ref ~a~%" it)
                         ;; "Import" statements proved at upper levels
                         (dolist (h (subproof-hypo prf))
                           (unless (provenp [→ ,str ,h])
;                             (format t "Ded: Hypo ~a not found, importing~%" h)
                             (line [infer ax1 ,h ,str])))
                         (if (subproof-hypo prf)
                           (if (dedth-p (car it))
                             (let* ((prev (car (last it)))
                                    (new [∧ ,str ,prev]))
;                               (format t "Ded: thr already in dedth-form~%")
                               (dolist (h (subproof-hypo prf))
                                 (line [imp ,str ,prev ,h]))
                               (line `(,(car it) ,@(butlast it) ,new))
                               (dolist (a (subproof-assert prf))
                                 (line [exp ,str ,prev ,a])))
                             (progn
;                               (format t "Ded: calling dedth-1~%")
                               (line `(,[dedth-1 ,(car it)] ,@(cdr it) ,str))))
                           ;; Simple case: no hypotheses
                           (progn
;                             (format t "Ded: no hypotheses, using ax1~%")
                             (line prf)
                             (dolist (a (subproof-assert prf))
                               (line [infer ax1 ,a ,str]))))))
                       ;; Case 2: sequence of subproofs
                       (progn
;                         (format t "Ded: subproof[~d]~%" (length (subproof-sub prf)))
                         (dolist (s (subproof-sub prf))
                           (line (rec s)))))))
          (dolist (h (subproof-hypo prf))
            (insert-wff (car *current-proof*) [→ ,str ,h]))
          (rec prf)))))

  ;; TODO: for some reason this does not work with sbcl
  (defmacro suppose (hypo assr &body lines)
    `(let ((prf (linear-subproof ,hypo ,assr ,@lines)))
       (let ((num-hypo (length (subproof-hypo prf))))
         (case num-hypo
           (0 prf) ;; TODO: print warning? 
           (t (let ((conj (reduce #'(lambda (x y) [∧ ,x ,y])
                                  (subproof-hypo prf))))
                (subproof (:assert (loop for i in (subproof-assert prf)
                                        collect [→ ,conj ,i]))
                  (if (> num-hypo 1)
                    (line [siman ,conj]) ;; TODO: multiple hypo needs multiple siman
                    (line [id ,conj]))
                  (line [dedth ,prf ,conj]))))))))

  (virtual-metath "dedth-1" (thr)
    (in-context *meta-context*
      (let-once (mp (symref-target !ax-mp))
        (if (eq thr mp)
          (symref-target !mpd)
          (copy-th thr "-d1" (vars distinct)
            (let ((x (var wff :arg)))
              (dolist (hp (context-hypo thr))
                (hypo [→ ,x ,hp]))
              (dolist (assr (context-assert thr))
                (ass [→ ,x ,assr]))
              (proof
                [dedth ,(context-proof thr) ,x])))))))

;;; ************
;;; Theorems
;;; ************

;;; I - Implication

;; → is reflexive
(in-context !im
  (th "refl" (wff "φ")
    (meta (:comment . "Principle of identity")
          (:description . "Theorem *2.08 of Principia Mathematica, p. 101; id1 in set.mm."))
    
    (ass [→ φ φ])
    (proof
      [ax1 φ φ]
      [ax1 φ [→ φ φ]]
      [ax2 φ [→ φ φ] φ]
      [ax-mp [→ φ → → φ φ φ] [→ → φ → φ φ → φ φ]]
      [ax-mp [→ φ → φ φ] [→ φ φ]]))
  (th-pattern !refl)

  (th "refld" (wff ![φ ψ])
    (meta (:comment . "Principle of identity with antecedent.")
          (:description . "From set.mm."))
    (ass [→ φ → ψ ψ])
    (proof
      [refl ψ]
      [infer ax1 [→ ψ ψ] φ]))

  ;; Syllogism: → is transitive
  (th "tri" (wff ![φ ψ χ])
    (meta (:comment . "An inference version of the transitive laws for implication.")
          (:description . "syl in set.mm."))
    (hypo [→ φ ψ] [→ ψ χ])
    (ass [→ φ χ])
    (proof
      [infer ax1 [→ ψ χ] φ]
      [infer infer ax2 φ ψ χ]))

  ; for consistency
  (alias "rp22" (wff ![φ ψ χ])
    [tri φ ψ χ])
  (alias "rp21" (wff ![φ ψ χ])
    [tri χ φ ψ])

  (th "com12i" (wff ![φ ψ χ])
    (meta (:comment . "Inference that swaps (commutes) antecedents in an implication.")
          (:description . "com12 in set.mm."))
    (hypo [→ φ → ψ χ])
    (ass [→ ψ → φ χ])
    (proof
      [ax1 ψ φ]
      [infer ax2 φ ψ χ]
      [tri ψ [→ φ ψ] [→ φ χ]]))
  (th-pattern !com12i)

  (th "tr>" (wff ![φ ψ χ])
    (meta (:comment . "A closed form of syllogism.")
          (:description . "Theorem *2.05 of Principia Mathematica, p. 100; imim2 in set.mm"))
    (ass [→ → φ ψ → → χ φ → χ ψ])
    (proof
      [ax1 [→ φ ψ] χ]
      [ded [ax2 χ φ ψ] [→ φ ψ]]))
  (th-pattern !tr>)

  (th "tr<" (wff ![φ ψ χ])
    (meta (:comment . "A closed form of syllogism.")
          (:description . "Theorem *2.06 of PrincipiaMathematica, 1.100; imim1 in set.mm"))
    (ass [→ → φ ψ → → ψ χ → φ χ])
    (proof
      [tr> ψ χ φ]
      [com12i [→ ψ χ] [→ φ ψ] [→ φ χ]]))
  (th-pattern !tr<)

  (alias "imt>" (wff ![φ ψ χ])
    [tr> φ ψ χ])
  (alias "imt<" (wff ![φ ψ χ])
    [tr< φ ψ χ])
)
(alias "id" (wff "φ")
  [!im!refl φ])
(alias "syl" (wff ![φ ψ χ])
  [!im!tri φ ψ χ])
(alias "syl<" (wff ![φ ψ χ])
  [!im!tr> φ ψ χ])
(alias "syl>" (wff ![φ ψ χ])
  [!im!tr< φ ψ χ])
(alias "com12i" (wff ![φ ψ χ])
  [!im!com12i φ ψ χ])

(in-context !im
  ; also imti
  (th "imti" (wff ![φ ψ χ θ])
    (meta (:comment . "Inference joining two implications.")
          (:description . "imim12i in set.mm"))
    (hypo [→ φ ψ] [→ χ θ])
    (ass [→ → ψ χ → φ θ])
    (proof
      [infer tr> χ θ ψ]
      [infer tr< φ ψ θ]
      [tri [→ ψ χ] [→ ψ θ] [→ φ θ]]))
  (th-pattern !imti)

  (th "2tri" (wff ![φ ψ χ θ])
    (meta (:comment . "Inference chaining two syllogisms.")
          (:description . "3syl in set.mm."))
    (hypo [→ φ ψ] [→ ψ χ] [→ χ θ])
    (ass [→ φ θ])
    (proof
      [tri φ ψ χ]
      [tri φ χ θ])))

(alias "3syl" (wff ![φ ψ χ θ])
  [!im!2tri φ ψ χ θ])

(in-context !im
  (th "rp32" (wff ![φ ψ χ θ])
    
    (meta (:comment . "A syllogism rule of inference.")
          (:description . "syl5 in set.mm.</p><p><i>Naming convention:</i>
Theorem rpi<i>xy</i> replaces the <i>y</i>'th wff in a nested implication
of <i>x</i> wffs."))

    (hypo [→ φ → ψ χ] [→ θ ψ])
    (ass [→ φ → θ χ])
    (proof
      [infer tr< θ ψ χ]
      [tri φ [→ ψ χ] [→ θ χ]]))

  (th "rp33" (wff ![φ ψ χ θ])
    (meta (:comment . "A syllogism rule of inference.")
          (:description . "syl6 in set.mm"))
    (hypo [→ φ → ψ χ] [→ χ θ])
    (ass [→ φ → ψ θ])
    (proof
      [infer tr> χ θ ψ]
      [tri φ [→ ψ χ] [→ ψ θ]]))

  (th "rp43" (wff ![φ ψ χ θ τ])
    (meta (:comment . "A syllogism rule of inference.")
          (:description . "syl7 in set.mm"))
    (hypo [→ φ → ψ → χ θ] [→ τ χ])
    (ass [→ φ → ψ → τ θ])
    (proof
      [infer tr< τ χ θ]
      [rp33 φ ψ [→ χ θ] [→ τ θ]]))

  (th "rp44" (wff ![φ ψ χ θ τ])
    (meta (:comment . "A syllogism rule of inference.")
          (:description . "syl8 in set.mm"))
    (hypo [→ φ → ψ → χ θ] [→ θ τ])
    (ass [→ φ → ψ → χ τ])
    (proof
      [infer tr> θ τ χ]
      [rp33 φ ψ [→ χ θ] [→ χ τ]]))

  (th "trd" (wff ![φ ψ χ θ])
    (meta (:comment . "Syllogism deduction.")
          (:description . "syld in set.mm."))
    (hypo [→ φ → ψ χ] [→ φ → χ θ])
    (ass [→ φ → ψ θ])
    (proof
      [ded [tr> χ θ ψ] φ]
      [infer infer ax2 φ [→ ψ χ] [→ ψ θ]]))

  (th "imtd" (wff ![φ ψ χ θ τ])
    (meta (:comment . "Deduction combining antecedents and consequents.")
          (:description . "imim12d in set.mm"))
    (hypo [→ φ → ψ χ] [→ φ → θ τ])
    (ass [→ φ → → χ θ → ψ τ])
    (proof
      [ded [tr< ψ χ θ] φ]
      [ded [tr> θ τ ψ] φ]
      [trd φ [→ χ θ] [→ ψ θ] [→ ψ τ]])))

;; (infer (infer mpclosed)) == ax-mp
(th "mpclosed" (wff ![φ ψ])
    (meta (:comment . "Closed form of modus ponens.")
          (:description . "Theorem *2.27 in Principia Mathematica; pm2.27 in set.mm.
Also called \"Assertion\"."))
    (ass [→ φ → → φ ψ ψ])
    (proof
      [id [→ φ ψ]]
      [com12i [→ φ ψ] φ ψ]))
(th-pattern !mpclosed)

(in-context !im
  (th "com12" (wff ![φ ψ χ])
    (meta (:comment . "Swap antecedents.")
          (:description . "Theorem *2.04 of Principia Mathematica, p.100; pm2.04 in set.mm."))
    (ass [→ → φ → ψ χ → ψ → φ χ])
    (proof
      [ax2 φ ψ χ]
      [ax1 ψ φ]
      [rp32 [→ φ → ψ χ] [→ φ ψ] [→ φ χ] ψ]))

;; (infer (infer syld-closed)) == syld
  (th "syld-closed" (wff ![φ ψ χ θ])
    (meta (:comment . "A closed form of syllogism deduction.")
          (:description . "Theorem *2.83 of Principia Mathematica, p. 108; pm2.83 in set.mm."))
    (ass [→ → φ → ψ χ → → φ → χ θ → φ → ψ θ])
    (proof
      [tr< ψ χ θ]
      [infer tr> [→ ψ χ] [→ → χ θ → ψ θ] φ]
      [ded ax2 φ [→ χ θ] [→ ψ θ] [→ φ → ψ χ]]))

  (th "com23i" (wff ![φ ψ χ θ])
    (meta (:comment . "Commutation of antecedents. Swap 2nd and 3rd.")
          (:description . "com23 in set.mm."))
    (hypo [→ φ → ψ → χ θ])
    (ass [→ φ → χ → ψ θ])
    (proof
      [com12 ψ χ θ]
      [tri φ [→ ψ → χ θ] [→ χ → ψ θ]]))

  (th "com13i" (wff ![φ ψ χ θ])
    (meta (:comment . "Commutation of antecedents. Swap 1st and 3rd.")
          (:description . "com13 in set.mm."))
    (hypo [→ φ → ψ → χ θ])
    (ass [→ χ → ψ → φ θ])
    (proof
      [com12i φ ψ [→ χ θ]]
      [com23i ψ φ χ θ]
      [com12i ψ χ [→ φ θ]]))

  (th "com3li" (wff ![φ ψ χ θ])
    (meta (:comment . "Commutation of antecedents. Rotate left.")
          (:description . "com3l in set.mm."))
    (hypo [→ φ → ψ → χ θ])
    (ass [→ ψ → χ → φ θ])
    (proof
      [com23i φ ψ χ θ]
      [com13i φ χ ψ θ]))

  (th "com3ri" (wff ![φ ψ χ θ])
    (meta (:comment . "Commutation of antecedents. Rotate right.")
          (:description . "com3r in set.mm."))
    (hypo [→ φ → ψ → χ θ])
    (ass [→ χ → φ → ψ θ])
    (proof
      [com3li φ ψ χ θ]
      [com3li ψ χ φ θ])))

(th "mpi" (wff ![φ ψ χ])
    (meta (:comment . "A nested modus ponens inference.")
          (:description . "From set.mm."))
    (hypo [ψ] [→ φ → ψ χ])
    (ass [→ φ χ])
    (proof
      [com12i φ ψ χ]
      [ax-mp ψ [→ φ χ]]))

(th "mpii" (wff ![φ ψ χ θ])
    (meta (:comment . "A double nested modus ponens inference.")
          (:description . "From set.mm."))
    (hypo [χ] [→ φ → ψ → χ θ])
    (ass [→ φ → ψ θ])
    (proof
      [!im!com3ri φ ψ χ θ]
      [ax-mp χ [→ φ → ψ θ]]))

(th "mpd" (wff ![ψ χ φ])
    (meta (:comment . "A modus ponens deduction.")
          (:description . "From set.mm."))
    (hypo [→ φ ψ] [→ φ → ψ χ])
    (ass [→ φ χ])
    (proof
      [infer infer ax2 φ ψ χ]))

(in-context !im
  (th "rp32d" (wff ![φ ψ χ θ τ])
    (meta (:comment . "A nested syllogism deduction.")
          (:description . "syl5d in set.mm"))
    (hypo [→ φ → ψ → χ θ] [→ φ → τ χ])
    (ass [→ φ → ψ → τ θ])
    (proof
      [ded tr< τ χ θ φ]
      [trd φ ψ [→ χ θ] [→ τ θ]]))

  (th "rp33d" (wff ![φ ψ χ θ τ])
    (meta (:comment . "A nested syllogism deduction.")
          (:description . "syl6d in set.mm"))
    (hypo [→ φ → ψ → χ θ] [→ φ → θ τ])
    (ass [→ φ → ψ → χ τ])
    (proof
      [ded tr> θ τ χ φ]
      [trd φ ψ [→ χ θ] [→ χ τ]]))

  ; for consistency
  (alias "rp22d" (wff ![φ ψ χ θ])
    [trd φ ψ χ θ])
  (alias "rp21d" (wff ![φ ψ χ θ])
    [trd φ θ ψ χ])

  (th "syl9" (wff ![φ ψ χ θ τ])
    (meta (:comment . "A nested syllogism inference with different antecedents.")
          (:description . "From set.mm."))
    (hypo [→ φ → ψ χ] [→ θ → χ τ])
    (ass [→ φ → θ → ψ τ])
    (proof
      [infer ax1 [→ θ → χ τ] φ]
      [rp32d φ θ χ τ ψ]))

  ; rename syl9< ? find better name for syl9*
  (th "syl9r" (wff ![φ ψ χ θ τ])
    (meta (:comment . "A nested syllogism inference with different antecedents.")
          (:description . "From set.mm."))
    (hypo [→ φ → ψ χ] [→ θ → χ τ])
    (ass [→ θ → φ → ψ τ])
    (proof
      [syl9 φ ψ χ θ τ]
      [com12i φ θ [→ ψ τ]]))

  (th "abs" (wff ![φ ψ])
    (meta (:comment . "Absorption of redundant antecedent.")
          (:description . "Also called the \"Contraction\" or \"Hilbert\" axiom.
Theorem *2.43 of Principia Mathematica, p.106; pm2.43 in set.mm."))
    (ass [→ → φ → φ ψ → φ ψ])
    (proof
      [→ φ → → φ ψ ψ]
      [infer ax2 φ [→ φ ψ] ψ]))
  (th-pattern !abs)

  (th "sylc" (wff ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference combined with contraction.")
          (:description . "From set.mm."))
    (hypo [→ φ → ψ χ] [→ θ φ] [→ θ ψ])
    (ass [→ θ χ])
    (proof
      [tri θ φ [→ ψ χ]]
      [mpd ψ χ θ])))

(th "ax2-converse" (wff ![φ ψ χ])
    (meta (:comment . "Converse of axiom <a href=\"prop-ax/ax2.html\">ax2</a>.")
          (:description . "Theorem *2.86 of Principia Mathematica, p. 108; pm2.86 in set.mm."))
    (ass [→ → → φ ψ → φ χ → φ → ψ χ])
    (proof
      [ax1 ψ φ]
      [infer syl> ψ [→ φ ψ] [→ φ χ]]
      [!im!com23i [→ → φ ψ → φ χ] ψ φ χ]))
(th-pattern !ax2-converse)

;; II - Negation

(th "<neg" (wff ![φ ψ])
    (meta (:comment . "From a wff and its negation, anything is true.")
          (:description . "Theorem *2.21 of Principia Mathematica, p. 104;
pm2.21 in set.mm. Also called the Duns Scotus law."))
    (ass [→ ¬ φ → φ ψ])
    (proof
      [→ ¬ φ → ¬ ψ ¬ φ]
      [ded ax3 ψ φ [¬ φ]]))
(th-pattern !<neg)

(th ">neg" (wff ![φ ψ])
    (meta (:comment . "From a wff and its negation, anything is true.")
          (:description . "Theorem 2.24 of Principia Mathematica, p. 104;
pm2.24 in set.mm."))
    (ass [→ φ → ¬ φ ψ])
    (proof
      [→ ¬ φ → φ ψ]
      [→ φ → ¬ φ ψ]))
(th-pattern !>neg)

(th "neg+" (wff "φ")
    (meta (:comment . "Proof by contradiction.")
          (:description . "Theorem 2.18 of Principia Mathematica, p.103;
pm2.18 in set.mm. Also called the Law of Clavius."))
    (ass [→ → ¬ φ φ φ])
    (proof
      [→ ¬ φ → φ ¬ → ¬ φ φ]
      [infer ax2 [¬ φ] φ [¬ → ¬ φ φ]]
      [ded ax3 φ [→ ¬ φ φ] [→ ¬ φ φ]]
      [infer !im!abs [→ ¬ φ φ] φ]))
(th-pattern !neg+)

(th "peirce" (wff ![φ ψ])
    (meta (:comment . "Peirce's axiom.")
          (:description . "This odd-looking theorem is the \"difference\"
between an intuitionistic system of propositional calculus and a classical
system and is not accepted by intuitionists. When Peirce's axiom is added
to an intuitionistic system, the system becomes equivalent to our classical
system <a href=\"prop-ax/ax1.html\">ax1</a> through <a href=\"prop-ax/ax3.html\">ax3</a>.
A curious fact about this theorem is that it requires
<a href=\"prop-ax/ax3.html\">ax3</a> for its proof even though the result has no
negations in it."))
    (ass [→ → → φ ψ φ φ])
    (proof
      [→ ¬ φ → φ ψ]
      [infer syl> [¬ φ] [→ φ ψ] φ]
      [→ → ¬ φ φ φ]
      [syl [→ → φ ψ φ] [→ ¬ φ φ] φ]))

(th "neg<" (wff "φ")
   (meta (:comment . "Double negation.")
         (:description . "Theorem *2.14 of Principia Mathematica, p. 102; nega in set.mm."))
   (ass [→ ¬ ¬ φ φ])
   (proof
     [→ ¬ ¬ φ → ¬ φ φ]
     [→ → ¬ φ φ φ]
     [syl [¬ ¬ φ] [→ ¬ φ φ] φ]))
(th-pattern !neg<)

(th "neg>" (wff "φ")
    (meta (:comment . "Converse of double negation.")
          (:description . "Thoerem *2.12 of Principia Mathematica, p. 101; negb in set.mm."))
    (ass [→ φ ¬ ¬ φ])
    (proof
      [→ ¬ ¬ ¬ φ ¬ φ]
      [infer ax3 [¬ ¬ φ] φ]))
(th-pattern !neg>)

(th "neg-" (wff "φ")
    (meta (:comment . "Reduction ad absurdum.")
          (:description . "Theorem *2.01 of Principia Mathematica, p. 100; pm2.01 in set.mm."))
    (ass [→ → φ ¬ φ ¬ φ])
    (proof
      [→ ¬ ¬ φ φ]
      [infer syl> [¬ ¬ φ] φ [¬ φ]]
      [→ → ¬ ¬ φ ¬ φ ¬ φ]
      [syl [→ φ ¬ φ] [→ ¬ ¬ φ ¬ φ] [¬ φ]]))
(th-pattern !neg-)

(th "con>" (wff ![φ ψ])
    (meta (:comment . "Contraposition.")
          (:description . "Theorem *2.03 of Principia Mathematica, p. 100; con2 in set.mm."))
    (ass [→ → φ ¬ ψ → ψ ¬ φ])
    (proof
      [→ ¬ ¬ φ φ]
      [infer syl> [¬ ¬ φ] φ [¬ ψ]]
      [ded ax3 [¬ φ] ψ [→ φ ¬ ψ]]))
(th-pattern !con>)

(th "con<" (wff ![φ ψ])
    (meta (:comment . "Contraposition.")
          (:description . "Theorem *2.15 of Principia Mathematica, p. 102; con1 in set.mm."))
    (ass [→ → ¬ φ ψ → ¬ ψ φ])
    (proof
      [→ ψ ¬ ¬ ψ]
      [infer syl< ψ [¬ ¬ ψ] [¬ φ]]
      [ded ax3 φ [¬ ψ] [→ ¬ φ ψ]]))
(th-pattern !con<)

(th "con" (wff ![φ ψ])
    (meta (:comment . "Contraposition.")
          (:description . "Theorem *2.16 of Principia Mathematica, p. 103; con3 in set.mm."))
    (ass [→ → φ ψ → ¬ ψ ¬ φ])
    (proof
      [→ ψ ¬ ¬ ψ]
      [infer syl< ψ [¬ ¬ ψ] φ]
      [ded con> φ [¬ ψ] [→ φ ψ]]))
(th-pattern !con)

(th "mto" (wff ![φ ψ])
    (meta (:comment . "The rule of modus tollens."))
    (hypo [→ φ ψ] [¬ ψ])
    (ass [¬ φ])
    (proof
      [infer infer con φ ψ]))

(th "mtoi" (wff ![φ ψ χ])
    (meta (:comment . "Modus tollens inference."))
    (hypo [¬ χ] [→ φ → ψ χ])
    (ass [→ φ ¬ ψ])
    (proof
      [ded con ψ χ φ]
      [mpi φ [¬ χ] [¬ ψ]]))

(th "mtod" (wff ![φ ψ χ])
    (meta (:comment . "Modus tollens deduction."))
    (hypo [→ φ ¬ χ] [→ φ → ψ χ])
    (ass [→ φ ¬ ψ])
    (proof
      [ded con ψ χ φ]
      [mpd [¬ χ] [¬ ψ] φ]))

(th "mto2" (wff ![φ ψ])
    (meta (:comment . "A rule similar to modus tollens."))
    (hypo [ψ] [→ φ ¬ ψ])
    (ass [¬ φ])
    (proof
      [infer infer con> φ ψ]))

(th "pm3.2im" (wff ![φ ψ])
    (meta (:description . "Theorem *3.2 of Principia Mathematica p. 111,
expressed with primitive connectives."))
    (ass [→ φ → ψ ¬ → φ ¬ ψ])
    (proof
      [→ φ → → φ ¬ ψ ¬ ψ]
      [ded con> [→ φ ¬ ψ] ψ φ]))

(th "case" (wff ![φ ψ])
    (meta (:comment . "Proof by cases.")
          (:description . "Theorem *2.61 of Principia Mathematica p. 107, pm2.61 in set.mm.
Useful for eliminating an antecedent."))
    (ass [→ → φ ψ → → ¬ φ ψ ψ])
    (proof
      [→ → φ ψ → → ¬ ψ φ → ¬ ψ ψ]
      [→ → ¬ ψ ψ ψ]
      [!im!rp33 [→ φ ψ] [→ ¬ ψ φ] [→ ¬ ψ ψ] ψ]
      [→ → ¬ φ ψ → ¬ ψ φ]
      [!im!rp32 [→ φ ψ] [→ ¬ ψ φ] ψ [→ ¬ φ ψ]]))
(th-pattern !case)

(th "casei" (wff ![φ ψ])
    (meta (:comment . "Inference eliminating an antecedent.")
          (:description . "pm2.61i in set.mm."))
    (hypo [→ φ ψ] [→ ¬ φ ψ])
    (ass [ψ])
    (proof
      [→ → φ ψ → → ¬ φ ψ ψ]
      [ax-mp [→ φ ψ] [→ → ¬ φ ψ ψ]]
      [ax-mp [→ ¬ φ ψ] ψ]))

(th "cased2" (wff ![φ ψ χ])
    (meta (:comment . "Inference eliminating an antecedent.")
          (:description . "pm2.61d2 in set.mm."))
    (hypo [→ φ → ¬ ψ χ] [→ ψ χ])
    (ass [→ φ χ])
    (proof
      [ded ax1 χ φ ψ]
      [com12i φ [¬ ψ] χ]
      [casei ψ [→ φ χ]]))

(th "cased" (wff ![φ ψ χ])
    (meta (:comment . "Deduction eliminating an antecedent.")
          (:description . "pm2.61d in set.mm."))
    (hypo [→ φ → ψ χ] [→ φ → ¬ ψ χ])
    (ass [→ φ χ])
    (proof
      [com12i φ ψ χ]
      [com12i φ [¬ ψ] χ]
      [casei ψ [→ φ χ]]))

(th "caseii" (wff ![φ ψ χ])
    (meta (:comment . "Inference eliminating two antecedents.")
          (:description . "pm2.61ii in set.mm."))
    (hypo [→ ¬ φ → ¬ ψ χ] [→ φ χ] [→ ψ χ])
    (ass [χ])
    (proof
      [cased2 [¬ φ] ψ χ]
      [casei φ χ]))

(in-context !lemmas
  (th "pm2.6" (wff ![φ ψ])
    (meta (:description . "Theorem *2.6 of Principia Mathematica p. 107."))
    (ass [→ → ¬ φ ψ → → φ ψ ψ])
    (proof
      [case φ ψ]
      [com12i [→ φ ψ] [→ ¬ φ ψ] [ψ]])))

(th "ncase" (wff ![φ ψ])
    (meta (:comment . "Proof by contradiction.")
          (:description . "Theorem *2.65 of Principia Mathematica p. 107;
pm2.65 in set.mm. Useful for eliminating a consequent."))
    (ass [→ → φ ψ → → φ ¬ ψ ¬ φ])
    (proof
      [pm3.2im φ ψ]
      [infer ax2 φ ψ [¬ → φ ¬ ψ]]
      [ded con> φ [→ φ ¬ ψ] [→ φ ψ]]))

(in-context !im
  (th "ja" (wff ![φ ψ χ])
    (meta (:comment . "Inference joining the antecedents of two premises.")
          (:description . "ja in set.mm."))
    (hypo [→ ¬ φ χ] [→ ψ χ])
    (ass [→ → φ ψ χ])
    (proof
      [→ φ → → φ ψ ψ]
      [rp33 φ [→ φ ψ] ψ χ]
      [ded ax1 χ [→ φ ψ] [¬ φ]]
      [casei φ [→ → φ ψ χ]]))
  (th-pattern !ja)

  (th "jc" (wff ![φ ψ χ])
    (meta (:comment . "Inference joinnig the consequents of two premises."))
    (hypo [→ φ ψ] [→ φ χ])
    (ass [→ φ ¬ → ψ ¬ χ])
    (proof
      [pm3.2im ψ χ]
      [sylc ψ χ [¬ → ψ ¬ χ] φ]))
  (th-pattern !jc)

  (th "sim<" (wff ![φ ψ])
    (meta (:comment . "Simplification.")
          (:description . "Similar to Theorem *3.26 (Simp) of Principia Mathematica p. 112."))
    (ass [→ ¬ → φ ¬ ψ φ])
    (proof
      [→ ¬ φ → φ ¬ ψ]
      [infer con< φ [→ φ ¬ ψ]]))
  (th-pattern !sim<)
  
  (th "sim>" (wff ![φ ψ])
    (meta (:comment . "Simplification.")
          (:description . "Similar to Theorem *3.27 (Simp) of Principia Mathematica p. 112."))
    (ass [→ ¬ → φ ¬ ψ ψ])
    (proof
      [→ ¬ ψ → φ ¬ ψ]
      [infer con< ψ [→ φ ¬ ψ]]))
  (th-pattern !sim>)

  (th "imp" (wff ![φ ψ χ])
    (meta (:comment . "Importation theorem expressed with primitive connectives.")
          (:description . "impt in set.mm."))
    (ass [→ → φ → ψ χ → ¬ → φ ¬ ψ χ])
    (proof
      [→ → ψ χ → ¬ χ ¬ ψ]
      [infer tr> [→ ψ χ] [→ ¬ χ ¬ ψ] φ]
      [com23i [→ φ → ψ χ] φ [¬ χ] [¬ ψ]]
      [ded con< χ [→ φ ¬ ψ] [→ φ → ψ χ]]))

  (th "exp" (wff ![φ ψ χ])
    (meta (:comment . "Exportation thoerem expressed with primitive connectives.")
          (:description . "expt in set.mm."))
    (ass [→ → ¬ → φ ¬ ψ χ → φ → ψ χ])
    (proof
      [pm3.2im φ ψ]
      [ded tr< ψ [¬ → φ ¬ ψ] χ φ]
      [com12i φ [→ ¬ → φ ¬ ψ χ] [→ ψ χ]])))

(in-context !bi
  (th "justify" (wff "φ")
    (meta (:comment . "Theorem used to justify definition of biconditional
<a href=\"prop-ax/df-bi.html\">df-bi</a>.")
          (:description . "bijust in set.mm."))
    (ass [¬ → → φ φ ¬ → φ φ])
    (proof
      [→ φ φ]
      [→ → → φ φ ¬ → φ φ ¬ → φ φ]
      [mto2 [→ → φ φ ¬ → φ φ] [→ φ φ]])) )

;; III - bi-implication

(th "bi>" (wff ![φ ψ])
    (meta (:comment . "Property of the biconditional connective.")
          (:description . "bi1 in set.mm."))
    (ass [→ ↔ φ ψ → φ ψ])
    (proof
      [df-bi φ ψ]
      [infer !im!sim< [→ ↔ φ ψ ¬ → → φ ψ ¬ → ψ φ] [→ ¬ → → φ ψ ¬ → ψ φ ↔ φ ψ]]
      [!im!sim< [→ φ ψ] [→ ψ φ]]
      [syl [↔ φ ψ] [¬ → → φ ψ ¬ → ψ φ] [→ φ ψ]]))

(th-pattern !bi>)

(th "bi<" (wff ![φ ψ])
    (meta (:comment . "Property of the biconditional connective.")
          (:description . "bi2 in set.mm."))
    (ass [→ ↔ φ ψ → ψ φ])
    (proof
      [df-bi φ ψ]
      [infer !im!sim< [→ ↔ φ ψ ¬ → → φ ψ ¬ → ψ φ] [→ ¬ → → φ ψ ¬ → ψ φ ↔ φ ψ]]
      [!im!sim> [→ φ ψ] [→ ψ φ]]
      [syl [↔ φ ψ] [¬ → → φ ψ ¬ → ψ φ] [→ ψ φ]]))

(th-pattern !bi<)

(th ">bi" (wff ![φ ψ])
    (meta (:comment . "Property of the biconditional connective.")
          (:comment . "bi3 in set.mm."))
    (ass [→ → φ ψ → → ψ φ ↔ φ ψ])
    (proof
      [df-bi φ ψ]
      [infer !im!sim> [→ ↔ φ ψ ¬ → → φ ψ ¬ → ψ φ] [→ ¬ → → φ ψ ¬ → ψ φ ↔ φ ψ]]
      [infer !im!exp [→ φ ψ] [→ ψ φ] [↔ φ ψ]]))

(th ">bii" (wff ![φ ψ])
    (meta (:comment . "Infer an equivalence from an implication and its converse.")
          (:description . "impbi in set.mm."))
    (hypo [→ φ ψ] [→ ψ φ])
    (ass [↔ φ ψ])
    (proof
      [infer >bi φ ψ]
      [ax-mp [→ ψ φ] [↔ φ ψ]]))

(th-pattern !>bii)

(th "bi>I" (wff ![φ ψ])
    (meta (:comment . "Inference from bi>."))
    (hypo [↔ φ ψ])
    (ass [→ φ ψ])
    (proof
      [infer bi> φ ψ]))
(th "bi<I" (wff ![ψ φ])
    (meta (:comment . "Inference from bi<."))
    (hypo [↔ φ ψ])
    (ass [→ ψ φ])
    (proof
      [infer bi< φ ψ]))

(th-pattern !bi>I)
(th-pattern !bi<I)

(in-context !bi
  (th "cd>" (wff ![φ ψ χ])
    (meta (:comment . "Deduce a commuted implication from a logical equivalence.")
          (:description . "biimpcd in set.mm."))
    (hypo [→ φ ↔ ψ χ])
    (ass [→ ψ → φ χ])
    (proof
      [ded bi> ψ χ φ]
      [com12i φ ψ χ]))

  (th "cd<" (wff ![φ ψ χ])
    (meta (:comment . "Deduce a converse commuted implication from a logical equivalence.")
          (:description . "biimprcd in set.mm."))
    (hypo [→ φ ↔ ψ χ])
    (ass [→ χ → φ ψ])
    (proof
      [ded bi< ψ χ φ]
      [com12i φ χ ψ]))

  (th "df2" (wff ![φ ψ])
    (meta (:comment . "Relate the biconditional connective to primitive connectives.")
          (:description . "bii in set.mm."))
    (ass [↔ ↔ φ ψ ¬ → → φ ψ ¬ → ψ φ])
    (proof
      [bi> φ ψ]
      [bi< φ ψ]
      [!im!jc [↔ φ ψ] [→ φ ψ] [→ ψ φ]]
      [>bi φ ψ]
      [infer !im!imp [→ φ ψ] [→ ψ φ] [↔ φ ψ]]
      [>bii [↔ φ ψ] [¬ → → φ ψ ¬ → ψ φ]])))

(in-context !im
  (th "bcom12" (wff ![φ ψ χ])
    (meta (:comment . "Logical equivalence of commuted antecedents.")
          (:description . "Part of Theorem *4.87 of Principia Mathematica p. 122;
bi2.04 in set.mm."))
    (ass [↔ → φ → ψ χ → ψ → φ χ])
    (proof
      [com12 φ ψ χ]
      [com12 ψ φ χ]
      [>bii [→ φ → ψ χ] [→ ψ → φ χ]])) )

(th "bneg>" (wff "φ")
    (meta (:comment . "Double negation.")
          (:description . "Theorem *4.13 of Principia Mathematica p. 117;
pm4.13 in set.mm."))
    (ass [↔ φ ¬ ¬ φ])
    (proof
      [neg> φ]
      [neg< φ]
      [>bii φ [¬ ¬ φ]]))
(th-pattern !bneg>)

(th "bcon" (wff ![φ ψ])
    (meta (:comment . "Contraposition.")
          (:description . "Theorem *4.1 of Principia Mathematica p. 116; pm4.1 in set.mm."))
    (ass [↔ → φ ψ → ¬ ψ ¬ φ])
    (proof
      [con φ ψ]
      [ax3 ψ φ]
      [>bii [→ φ ψ] [→ ¬ ψ ¬ φ]]))
(th-pattern !bcon)

(th "bcon>" (wff ![φ ψ])
    (meta (:comment . "Contraposition.")
          (:description . "Bidirectional version of <a href=\"prop/con>.html\">con&gt;</a>.
bi2.03 in set.mm."))
    (ass [↔ → φ ¬ ψ → ψ ¬ φ])
    (proof
      [con> φ ψ]
      [con> ψ φ]
      [>bii [→ φ ¬ ψ] [→ ψ ¬ φ]]))
(th-pattern !bcon>)

(th "bcon<" (wff ![φ ψ])
    (meta (:comment . "Contraposition.")
          (:description . "Bidirectional version of <a href=\"prop/con<.html\">con&lt;</a>.
bi2.15 in set.mm."))
    (ass [↔ → ¬ φ ψ → ¬ ψ φ])
    (proof
      [con< φ ψ]
      [con< ψ φ]
      [>bii [→ ¬ φ ψ] [→ ¬ ψ φ]]))
(th-pattern !bcon<)

(in-context !im
  (th "babs" (wff ![φ ψ])
    (meta (:comment . "Antecedent absorption implication.")
          (:description . "Theorem *5.4 of Principia Mathematica p. 125; pm5.4 in set.mm."))
    (ass [↔ → φ → φ ψ → φ ψ])
    (proof
      [abs φ ψ]
      [ax1 [→ φ ψ] [φ]]
      [>bii [→ φ → φ ψ] [→ φ ψ]]))

  (th "di-im<" (wff ![φ ψ χ])
    (meta (:comment . "Distributive law for implication.")
          (:description . "Compare Theorem *5.41 of Principia Mathematica p. 125. imdi in set.mm."))
    (ass [↔ → φ → ψ χ → → φ ψ → φ χ])
    (proof
      [ax2 φ ψ χ]
      [ax2-converse φ ψ χ]
      [>bii [→ φ → ψ χ] [→ → φ ψ → φ χ]])))

(in-context !bi
  (th "refl" (wff "φ")
    (meta (:comment . "Principle of identity for logical equivalence.")
          (:description . "Theorem *4.2 of Principia Mathematica p. 117; pm4.2 in set.mm."))
    (ass [↔ φ φ])
    (proof
      [id φ]
      [>bii φ φ]))

  (th "comi" (wff ![φ ψ])
    (meta (:comment . "Inference from commutative law for logical equivalence.")
          (:description . "bicomi in set.mm."))
    (hypo [↔ φ ψ])
    (ass [↔ ψ φ])
    (proof
      [infer bi> φ ψ]
      [infer bi< φ ψ]
      [>bii ψ φ]))

  (th "tri" (wff ![φ ψ χ])
    (meta (:comment . "An inference from transitive law for logical equivalence.")
          (:description . "bitr in set.mm."))
    (hypo [↔ φ ψ] [↔ ψ χ])
    (ass [↔ φ χ])
    (proof
      [b2i syl φ ψ χ]
      [b2i syl χ ψ φ]
      [>bii φ χ]))

  (th "tri-r" (wff ![φ ψ χ])
    (meta (:comment . "An inference from transitive law for logical equivalence.")
          (:description . "bitr2 in set.mm."))
    (hypo [↔ φ ψ] [↔ ψ χ])
    (ass [↔ χ φ])
    (proof
      [tri φ ψ χ]
      [comi φ χ]))

  ; was <>bitr
  (th "tri>" (wff ![φ ψ χ])
    (meta (:comment . "An inference from transitive law for logical equivalence.")
          (:description . "bitr3 in set.mm."))
    (hypo [↔ ψ φ] [↔ ψ χ])
    (ass [↔ φ χ])
    (proof
      [comi ψ φ]
      [tri φ ψ χ]))
  ; was ><bitr
  (th "tri<" (wff ![φ ψ χ])
    (meta (:comment . "An inference from transitive law for logical equivalence.")
          (:description . "bitr4 in set.mm."))
    (hypo [↔ φ ψ] [↔ χ ψ])
    (ass [↔ φ χ])
    (proof
      [comi χ ψ]
      [tri φ ψ χ])))

(alias "bitr" (wff ![φ ψ χ])
  [!bi!tri φ ψ χ])

(in-context !bi
  ;was 2bitr
  (th "2tri" (wff ![φ ψ χ θ])
    (meta (:comment . "A chained inference from transitive law for logical equivalence.")
          (:description . "3bitr in set.mm."))
    (hypo [↔ φ ψ] [↔ ψ χ] [↔ χ θ])
    (ass [↔ φ θ])
    (proof
      [tri φ ψ χ]
      [tri φ χ θ]))

  ; was >2bitr
  (th ">tr" (wff ![φ ψ χ θ])
    (meta (:comment . "A chained inference from transitive law for logical equivalence.")
          (:description . "3bitr3 in set.mm."))
    (hypo [↔ φ ψ] [↔ φ χ] [↔ ψ θ])
    (ass [↔ χ θ])
    (proof
      [comi φ χ]
      [2tri χ φ ψ θ]))

  ; was >2bitr<
  (th ">tr-r" (wff ![φ ψ χ θ])
    (meta (:comment . "A chained inference from transitive law for logical equivalence.")
          (:description . "3bitr3r in set.mm."))
    (hypo [↔ φ ψ] [↔ φ χ] [↔ ψ θ])
    (ass [↔ θ χ])
    (proof
      [>tr φ ψ χ θ]
      [comi χ θ]))

  ; was <2bitr
  (th "<tr" (wff ![φ ψ χ θ])
    (meta (:comment . "A chained inference from transitive law for logical equivalence.")
          (:description . "This inference is frequently used to apply a definition to both
sides of a logical equivalence. 3bitr4 in set.mm."))
    (hypo [↔ φ ψ] [↔ χ φ] [↔ θ ψ])
    (ass [↔ χ θ])
    (proof
      [comi θ ψ]
      [2tri χ φ ψ θ]))

  ; was <2bitr<
  (th "<tr-r" (wff ![φ ψ χ θ])
    (meta (:comment . "A chained inference from transitive law for logical equivalence.")
          (:description . "3bitr4r in set.mm."))
    (hypo [↔ φ ψ] [↔ χ φ] [↔ θ ψ])
    (ass [↔ θ χ])
    (proof
      [<tr φ ψ χ θ]
      [comi χ θ])))

(alias "2bitr" (wff ![φ ψ χ θ])
  [!bi!2tri φ ψ χ θ])
(alias "<bitr" (wff ![φ ψ χ θ])
  [!bi!<tr φ ψ χ θ])
(alias ">bitr" (wff ![φ ψ χ θ])
  [!bi!>tr φ ψ χ θ])

(in-context !im
  (th "eqt>" (wff ![φ ψ χ])
    (meta (:comment . "Introduce an antecedent to both sides of a logical equivalence.")
          (:description . "Theorem *4.85 of Principia Mathematica p. 122; imbi2 in set.mm."))
    (ass [→ ↔ φ ψ ↔ → χ φ → χ ψ])
    (proof
      (suppose ([↔ φ ψ]) ([↔ → χ φ → χ ψ])
        [infer bi>  φ ψ]
        [infer syl< φ ψ χ]
        [infer bi<  φ ψ]
        [infer syl< ψ φ χ]
        [>bii [→ χ φ] [→ χ ψ]])))

  (th "eqt<" (wff ![φ ψ χ])
    (meta (:comment . "Introduce a consequent to both sides of a logical equivalence.")
          (:description . "Theorem *4.84 of Principia Mathematica p. 122; imbi1 in set.mm."))
    (ass [→ ↔ φ ψ ↔ → φ χ → ψ χ])
    (proof
      (suppose ([↔ φ ψ]) ([↔ → φ χ → ψ χ])
        [infer bi<  φ ψ]
        [infer syl> ψ φ χ]
        [infer bi>  φ ψ]
        [infer syl> φ ψ χ]
        [>bii [→ φ χ] [→ ψ χ]]))) )

(th "neqt" (wff ![φ ψ])
    (meta (:comment . "Negate both sides of a logical equivalence.")
          (:description . "Closed form of negbii in set.mm."))
    (ass [→ ↔ φ ψ ↔ ¬ φ ¬ ψ])
    (proof
      (suppose ([↔ φ ψ]) ([↔ ¬ φ ¬ ψ])
        [infer bi< φ ψ]
        [infer con ψ φ]
        [infer bi> φ ψ]
        [infer con φ ψ]
        [>bii [¬ φ] [¬ ψ]])))

(in-context !im
  (th "eqti" (wff ![φ ψ χ θ])
    (meta (:comment . "Join two logical equivalences to form equivalence of implications.")
          (:description . "imbi12i in set.mm."))
    (hypo [↔ φ ψ] [↔ χ θ])
    (ass [↔ → φ χ → ψ θ])
    (proof
      [infer eqt< φ ψ χ]
      [infer eqt> χ θ ψ]
      [bitr [→ φ χ] [→ ψ χ] [→ ψ θ]])))

(in-context !bi
  ; was bmpi>
  (th "mpi>" (wff ![φ ψ χ])
    (meta (:comment . "An inference from a nested biconditional, related to modus ponens.")
          (:description . "mpbii in set.mm."))
    (hypo [ψ] [→ φ ↔ ψ χ])
    (ass [→ φ χ])
    (proof
      [ded bi> ψ χ φ]
      [mpi φ ψ χ]))

  ; was bmpi<
  (th "mpi<" (wff ![φ ψ χ])
    (meta (:comment . "An inference from a nested biconditional, related to modus ponens.")
          (:description . "mpbiri in set.mm."))
    (hypo [χ] [→ φ ↔ ψ χ])
    (ass [→ φ ψ])
    (proof
      [ded bi< ψ χ φ]
      [mpi φ χ ψ]))

  ; was bmpd>
  (th "mpd>" (wff ![φ ψ χ])
    (meta (:comment . "A deduction from a biconditional, related to modus ponens.")
          (:description . "mpbid in set.mm."))
    (hypo [→ φ ψ] [→ φ ↔ ψ χ])
    (ass [→ φ χ])
    (proof
      [ded bi> ψ χ φ]
      [mpd ψ χ φ]))

  ; was bmpd<
  (th "mpd<" (wff ![φ ψ χ])
    (meta (:comment . "A deduction from a biconditional, related to modus ponens.")
          (:description . "mpbird in set.mm."))
    (hypo [→ φ χ] [→ φ ↔ ψ χ])
    (ass [→ φ ψ])
    (proof
      [ded bi< ψ χ φ]
      [mpd χ ψ φ])))

(in-context !bi
  ; was rpb<<
  (th "trii<" (wff ![φ ψ χ θ])
    (meta (:comment . "A mixed syllogism inference.")
          (:description . "syl5bi in set.mm."))
    (hypo [→ φ ↔ ψ χ] [→ θ ψ])
    (ass [→ φ → θ χ])
    (proof
      [ded bi> ψ χ φ]
      [!im!rp32 φ ψ χ θ]))

  ; was rpb<>
  (th "trii<-r" (wff ![φ ψ χ θ])
    (meta (:comment . "A mixed syllogism inference.")
          (:description . "syl5bir in set.mm."))
    (hypo [→ φ ↔ ψ χ] [→ θ χ])
    (ass [→ φ → θ ψ])
    (proof
      [ded bi< ψ χ φ]
      [!im!rp32 φ χ ψ θ]))

  ; was rpb>>
  (th "trii>" (wff ![φ ψ χ θ])
    (meta (:comment . "A mixed syllogism inference.")
          (:description . "syl6bi in set.mm."))
    (hypo [→ φ ↔ ψ χ] [→ χ θ])
    (ass [→ φ → ψ θ])
    (proof
      [ded bi> ψ χ φ]
      [!im!rp33 φ ψ χ θ]))

  ; was rpb><
  (th "trii>-r" (wff ![φ ψ χ θ])
    (meta (:comment . "A mixed syllogism inference.")
          (:description . "syl6bir in set.mm."))
    (hypo [→ φ ↔ ψ χ] [→ ψ θ])
    (ass [→ φ → χ θ])
    (proof
      [ded bi< ψ χ φ]
      [!im!rp33 φ χ ψ θ])))

(in-context !im
  ; was >imtr
  (th ">tr" (wff ![φ ψ χ θ])
    (meta (:comment . "A mixed syllogism inference.")
          (:description . "Useful for removing a definition
from both sides of an implication. 3imtr3 in set.mm."))
    (hypo [→ φ ψ] [↔ φ χ] [↔ ψ θ])
    (ass [→ χ θ])
    (proof
      [infer bi< φ χ]
      [infer bi> ψ θ]
      [3syl χ φ ψ θ]))

  ; was <imtr
  (th "<tr" (wff ![φ ψ χ θ])
    (meta (:comment . "A mixed syllogism inference.")
          (:description . "Useful for applying a definition
to both sides of an implication. 3imtr4 in set.mm."))
    (hypo [→ φ ψ] [↔ χ φ] [↔ θ ψ])
    (ass [→ χ θ])
    (proof
      [infer bi> χ φ]
      [infer bi< θ ψ]
      [3syl χ φ ψ θ])) )

(alias "<imtr" (wff ![φ ψ χ θ])
  [!im!<tr φ ψ χ θ])
(alias ">imtr" (wff ![φ ψ χ θ])
  [!im!>tr φ ψ χ θ])

; todo: replace with closed form
(th "conb<i" (wff ![φ ψ])
    (meta (:comment . "A contraposition inference.")
          (:description . "con1bii in set.mm."))
    (hypo [↔ ¬ φ ψ])
    (ass [↔ ¬ ψ φ])
    (proof
      [infer bi> [¬ φ] ψ]
      [infer con< φ ψ]
      [infer bi< [¬ φ] ψ]
      [infer con> ψ φ]
      [>bii [¬ ψ] φ]))

(th "conb>i" (wff ![φ ψ])
    (meta (:comment . "A contraposition inference.")
          (:description . "con2bii in set.mm."))
    (hypo [↔ φ ¬ ψ])
    (ass [↔ ψ ¬ φ])
    (proof
      [!bi!comi φ [¬ ψ]]
      [conb<i ψ φ]
      [!bi!comi [¬ φ] ψ]))

;; IV - Conjunction, Disjunction

(in-context !or
  ; was dfor2
  (th "df2" (wff ![φ ψ])
    (meta (:comment . "Logical 'or' expressed in terms of implication only.")
          (:description . "Theorem *5.25 of Principia Mathematica p. 124; dfor2 in set.mm."))
    (ass [↔ ∨ φ ψ → → φ ψ ψ])
    (proof
      [df-or φ ψ]
      [!lemmas!pm2.6 φ ψ]
      [<neg φ ψ]
      [infer syl> [¬ φ] [→ φ ψ] ψ]
      [>bii [→ ¬ φ ψ] [→ → φ ψ ψ]]
      [bitr [∨ φ ψ] [→ ¬ φ ψ] [→ → φ ψ ψ]])))

(in-context !im
  ; was im.or
  (th "df.or" (wff ![φ ψ])
    (meta (:comment . "Implication in terms of disjunction.")
          (:description . "Theorem *4.6 of Principia Mathematica p. 120; imor in set.mm."))
    (ass [↔ → φ ψ ∨ ¬ φ ψ])
    (proof
      [bneg> φ]
      [infer eqt< φ [¬ ¬ φ] ψ]
      [df-or [¬ φ] ψ]
      [!bi!tri< [→ φ ψ] [→ ¬ ¬ φ ψ] [∨ ¬ φ ψ]]))
  (th-pattern !df.or)

  ; was im.an
  (th "df.an" (wff ![φ ψ])
    (meta (:comment . "Express implication in terms of conjunction.")
          (:description . "Theorem 3.4(27) of [Stoll] p. 176; iman in set.mm."))
    (ass [↔ → φ ψ ¬ ∧ φ ¬ ψ])
    (proof
      [bneg> ψ]
      [infer eqt> ψ [¬ ¬ ψ] φ]
      [df-an φ [¬ ψ]]
      [conb>i [∧ φ ¬ ψ] [→ φ ¬ ¬ ψ]]
      [bitr [→ φ ψ] [→ φ ¬ ¬ ψ] [¬ ∧ φ ¬ ψ]]))
  (th-pattern !df.an))

(in-context !an
  ; was an.nim
  (th "df.nim" (wff ![φ ψ])
    (meta (:comment . "Express conjunction in terms of implication.")
          (:description . "annim in set.mm."))
    (ass [↔ ∧ φ ¬ ψ ¬ → φ ψ])
    (proof
      [!im!df.an φ ψ]
      [conb>i [→ φ ψ] [∧ φ ¬ ψ]]))
  (th-pattern !df.nim))

(in-context !im
  ; was im.nan
  (th "df.nan" (wff ![φ ψ])
    (meta (:comment . "Express implication in terms of conjunction.")
          (:description . "imnan in set.mm."))
    (ass [↔ → φ ¬ ψ ¬ ∧ φ ψ])
    (proof
      [df-an φ ψ]
      [conb>i [∧ φ ψ] [→ φ ¬ ψ]]))
  (th-pattern !df.nan))

(in-context !or
  (th "idm" (wff "φ")
    (meta (:comment . "Idempotent law for disjunction.")
          (:description . "Theorem *4.25 of Principia Mathematica p. 117; oridm in set.mm."))
    (ass [↔ ∨ φ φ φ])
    (proof
      [ax1 [φ] [¬ φ]]
      [neg+ φ]
      [>bii [→ ¬ φ φ] [φ]]
      [df-or φ φ]
      [bitr [∨ φ φ] [→ ¬ φ φ] [φ]]))
  (th-pattern !idm)

  (th "com" (wff ![φ ψ])
    (meta (:comment . "Commutative law for disjunction.")
          (:description . "Theorem *4.31 of Principia Mathematica; orcom in set.mm."))
    (ass [↔ ∨ φ ψ ∨ ψ φ])
    (proof
      [bcon< φ ψ]
      [df-or φ ψ]
      [df-or ψ φ]
      [<bitr [→ ¬ φ ψ] [→ ¬ ψ φ] [∨ φ ψ] [∨ ψ φ]]))
  (th-pattern !com)

  (th "elim<" (wff ![φ ψ])
    (meta (:comment . "Elimination of disjunction by denial of a disjunct.")
          (:description . "Theorem *2.55 of Principia Mathematica p. 107; orel1 in set.mm."))
    (ass [→ ¬ φ → ∨ φ ψ ψ])
    (proof
      [df-or φ ψ]
      [infer bi> [∨ φ ψ] [→ ¬ φ ψ]]
      [com12i [∨ φ ψ] [¬ φ] [ψ]]))

  (th "elim>" (wff ![ψ φ])
    (meta (:comment . "Elimination of disjunction by denial of a disjunct.")
          (:description . "Theorem *2.56 of Principia Mathematica p. 107; orel2 in set.mm."))
    (ass [→ ¬ φ → ∨ ψ φ ψ])
    (proof
      [elim< φ ψ]
      [com ψ φ]
      [b2i !im!rp32 [¬ φ] [∨ φ ψ] [ψ] [∨ ψ φ]])) 

  (th "eqt>" (wff ![φ ψ χ])
    (meta (:comment . "Theorem adding a left disjunct to both sides of a logical equivalence.")
          (:description . "Compare orbi2i in set.mm."))
    (ass [→ ↔ φ ψ ↔ ∨ χ φ ∨ χ ψ])
    (proof
      (suppose ([↔ φ ψ]) ([↔ ∨ χ φ ∨ χ ψ])
        [infer !im!eqt> [φ] [ψ] [¬ χ]]
        [df-or χ φ]
        [df-or χ ψ]
        [<bitr [→ ¬ χ φ] [→ ¬ χ ψ] [∨ χ φ] [∨ χ ψ]])))

  (th "eqt<" (wff ![φ ψ χ])
    (meta (:comment . "Theorem adding a right disjunct to both sides of a logical equivalence.")
          (:description . "Theorem *4.37 of Principia Mathematica p. 118; orbi1 in set.mm."))
    (ass [→ ↔ φ ψ ↔ ∨ φ χ ∨ ψ χ])
    (proof
      (suppose ([↔ φ ψ]) ([↔ ∨ φ χ ∨ ψ χ])
        [infer neqt φ ψ]
        [infer !im!eqt< [¬ φ] [¬ ψ] [χ]]
        [df-or φ χ]
        [df-or ψ χ]
        [<bitr [→ ¬ φ χ] [→ ¬ ψ χ] [∨ φ χ] [∨ ψ χ]])))

  (th "eqti" (wff ![φ ψ χ θ])
    (meta (:comment . "Infer the disjunction of two equivalences.")
          (:description . "orbi12i in set.mm."))
    (hypo [↔ φ ψ] [↔ χ θ])
    (ass [↔ ∨ φ χ ∨ ψ θ])
    (proof
      [infer eqt< φ ψ χ]
      [infer eqt> χ θ ψ]
      [bitr [∨ φ χ] [∨ ψ χ] [∨ ψ θ]]))

  (th "12ass" (wff ![φ ψ χ])
    (meta (:comment . "A rearrangement of disjuncts.")
          (:description . "or12 in set.mm."))
    (ass [↔ ∨ φ ∨ ψ χ ∨ ψ ∨ φ χ])
    (proof
      [!im!bcom12 [¬ ψ] [¬ φ] [χ]]
      [df-or φ χ]
      [infer !im!eqt> [∨ φ χ] [→ ¬ φ χ] [¬ ψ]]
      [df-or ψ χ]
      [infer !im!eqt> [∨ ψ χ] [→ ¬ ψ χ] [¬ φ]]
      [!bi!<tr-r [→ ¬ ψ → ¬ φ χ] [→ ¬ φ → ¬ ψ χ]
               [→ ¬ ψ ∨ φ χ]   [→ ¬ φ ∨ ψ χ]]
      [df-or [φ] [∨ ψ χ]]
      [df-or [ψ] [∨ φ χ]]
      [<bitr [→ ¬ φ ∨ ψ χ] [→ ¬ ψ ∨ φ χ]
             [∨ φ ∨ ψ χ]   [∨ ψ ∨ φ χ]]))

  (th "ass" (wff ![φ ψ χ])
    (meta (:comment . "Associative law for disjunction.")
          (:description . "Theorem *4.33 of Principia Mathematica p. 118; orass in set.mm."))
    (ass [↔ ∨ ∨ φ ψ χ ∨ φ ∨ ψ χ])
    (proof
      [12ass χ φ ψ]
      [com [∨ φ ψ] χ]
      [com ψ χ]
      [infer eqt> [∨ ψ χ] [∨ χ ψ] φ]
      [<bitr [∨ χ ∨ φ ψ] [∨ φ ∨ χ ψ]
             [∨ ∨ φ ψ χ] [∨ φ ∨ ψ χ]]))

  (th "4ass" (wff ![φ ψ χ θ])
    (meta (:comment . "Rearrangement of 4 disjuncts.")
          (:description . "or4 in set.mm."))
    (ass [↔ ∨ ∨ φ ψ ∨ χ θ ∨ ∨ φ χ ∨ ψ θ])
    (proof
      [12ass ψ χ θ]
      [infer eqt> [∨ ψ ∨ χ θ] [∨ χ ∨ ψ θ] φ]
      [ass φ ψ [∨ χ θ]]
      [ass φ χ [∨ ψ θ]]
      [<bitr [∨ φ ∨ ψ ∨ χ θ] [∨ φ ∨ χ ∨ ψ θ]
             [∨ ∨ φ ψ ∨ χ θ] [∨ ∨ φ χ ∨ ψ θ]]))

  ; was or.ordi<
  (th "di-or<" (wff ![φ ψ χ])
    (meta (:comment . "Distribution of disjunction over disjunction.")
          (:description . "orordi in set.mm."))
    (ass [↔ ∨ φ ∨ ψ χ ∨ ∨ φ ψ ∨ φ χ])
    (proof
      [idm φ]
      [infer eqt< [∨ φ φ] [φ] [∨ ψ χ]]
      [4ass φ φ ψ χ]
      [!bi!tri> [∨ φ ∨ ψ χ] [∨ ∨ φ φ ∨ ψ χ] [∨ ∨ φ ψ ∨ φ χ]]))

  ; was or.ordi>
  (th "di-or>" (wff ![φ ψ χ])
    (meta (:comment . "Distribution of disjunction over disjunction.")
          (:description . "orordir in set.mm."))
    (ass [↔ ∨ ∨ φ ψ χ ∨ ∨ φ χ ∨ ψ χ])
    (proof
      [idm χ]
      [infer eqt> [∨ χ χ] [χ] [∨ φ ψ]]
      [4ass φ ψ χ χ]
      [!bi!tri> [∨ ∨ φ ψ χ] [∨ ∨ φ ψ ∨ χ χ] [∨ ∨ φ χ ∨ ψ χ]])) )

(th "or>" (wff ![ψ φ])
    (meta (:comment . "Introduction of a disjunct.")
          (:description . "Axiom *1.3 of Principia Mathematica p. 96; olc in set.mm."))
    (ass [→ φ ∨ ψ φ])
    (proof
      [ax1 [φ] [¬ ψ]]
      [ded df-or ψ φ φ]))
(th-pattern !or>)

(th "or<" (wff ![φ ψ])
    (meta (:comment . "Introduction of a disjunct.")
          (:description . "Theorem *2.2 of Principia Mathematica p. 104; orc in set.mm."))
    (ass [→ φ ∨ φ ψ])
    (proof
      [or> ψ φ]
      [!or!com ψ φ]
      [b2i syl φ [∨ ψ φ] [∨ φ ψ]]))
(th-pattern !or<)

(th ">and" (wff ![φ ψ])
    (meta (:comment . "Join antecedents with conjunction.")
          (:description . "Theorem *3.2 of Principia Mathematica p. 111; pm3.2 in set.mm."))
    (ass [→ φ → ψ ∧ φ ψ])
    (proof
      [df-an φ ψ]
      [infer bi< [∧ φ ψ] [¬ → φ ¬ ψ]]
      [infer !im!exp φ ψ [∧ φ ψ]]))

; was <and
(th ">and-r" (wff ![φ ψ])
    (meta (:comment . "Join antecedents with conjunction.")
          (:description . "Theorem *3.21 of Principia Mathematica p. 111; pm3.21 in set.mm."))
    (ass [→ φ → ψ ∧ ψ φ])
    (proof
      [>and ψ φ]
      [com12i [ψ] [φ] [∧ ψ φ]]))

(th ">andi" (wff ![φ ψ])
    (meta (:comment . "Infer conjunction of premises.")
          (:description . "pm3.2i in set.mm."))
    (hypo [φ] [ψ])
    (ass [∧ φ ψ])
    (proof
      [infer infer >and φ ψ]))

(th ">andc" (wff ![φ ψ χ])
    (meta (:comment . "Nested conjunction of antecedents.")
          (:description . "pm3.43i in set.mm."))
    (ass [→ → φ ψ → → φ χ → φ ∧ ψ χ])
    (proof
      [>and ψ χ]
      [infer syl< ψ [→ χ ∧ ψ χ] φ]
      [ded ax2 φ χ [∧ ψ χ] [→ φ ψ]]))

(th "jca" (wff ![φ ψ χ])
    (meta (:comment . "Deduce conjunction of the consequents of two implications
(\"join consequents with 'and'\").")
          (:description . "From set.mm."))
    (hypo [→ φ ψ] [→ φ χ])
    (ass [→ φ ∧ ψ χ])
    (proof
      [!im!jc φ ψ χ]
      [df-an ψ χ]
      [b2i syl φ [¬ → ψ ¬ χ] [∧ ψ χ]]))

(th "jcai" (wff ![φ ψ χ])
    (meta (:comment . "Deduction replacing implication with conjunction.")
          (:description . "From set.mm."))
    (hypo [→ φ ψ] [→ φ → ψ χ])
    (ass [→ φ ∧ ψ χ])
    (proof
      [com12i φ ψ χ]
      [syl φ ψ [→ φ χ]]
      [infer !im!abs φ χ]
      [jca φ ψ χ]))

(in-context !an
  (th "jct<" (wff ![φ ψ])
    (meta (:comment . "Inference conjoining a theorem to the left of a consequent.")
          (:description . "jctl in set.mm."))
    (hypo [ψ])
    (ass [→ φ ∧ ψ φ])
    (proof
      [infer ax1 ψ φ]
      [id φ]
      [jca φ ψ φ]))

  (th "jct>" (wff ![φ ψ])
    (meta (:comment . "Inference conjoining a theorem to the right of a consequent.")
          (:description . "jctr in set.mm."))
    (hypo [ψ])
    (ass [→ φ ∧ φ ψ])
    (proof
      [infer ax1 ψ φ]
      [id φ]
      [jca φ φ ψ]))

; ded jct<
;  (th "jcti<" (wff ![φ ψ χ])
 ;   (meta (:comment . "Inference conjoining a theorem to left of consequent in an implication.")
  ;        (:description . "jctil in set.mm."))
   ; (hypo [→ φ ψ] [χ])
    ;(ass [→ φ ∧ χ ψ])
;    (proof
 ;     [(infer !ax1) χ φ]
  ;    [jca φ χ ψ]))

; ded jct>
;  (th "jcti>" (wff ![φ ψ χ])
 ;   (meta (:comment . "Inference conjoining a theorem to right of consequent in an implication.")
  ;        (:description . "jctir in set.mm."))
   ; (hypo [→ φ ψ] [χ])
    ;(ass [→ φ ∧ ψ χ])
;    (proof
 ;     [(infer !ax1) χ φ]
  ;    [jca φ ψ χ]))

  ; was anc<, anc
  (th "join" (wff ![φ ψ])
    (meta (:comment . "Conjoin antecedent to left of consequent.")
          (:description . "ancl in set.mm."))
    (ass [→ → φ ψ → φ ∧ φ ψ])
    (proof
      [>and φ ψ]
      [infer ax2 φ ψ [∧ φ ψ]]))

  ; was anc>, anc-r
  (th "join-r" (wff ![φ ψ])
    (meta (:comment . "Conjoin antecedent to right of consequent.")
          (:description . "ancr in set.mm."))
    (ass [→ → φ ψ → φ ∧ ψ φ])
    (proof
      [>and-r φ ψ]
      [infer ax2 φ ψ [∧ ψ φ]]))

  ; was anc2<
  (th "join2" (wff ![φ ψ χ])
    (meta (:comment . "Conjoin antecedent to left of consequent in nested implication.")
          (:description . "anc2l in set.mm."))
    (ass [→ → φ → ψ χ → φ → ψ ∧ φ χ])
    (proof
      [>and φ χ]
      [ded syl< χ [∧ φ χ] ψ φ]
      [infer ax2 φ [→ ψ χ] [→ ψ ∧ φ χ]]))

  ; was anc2>
  (th "join2-r" (wff ![φ ψ χ])
    (meta (:comment . "Conjoin antecedent to right of consequent in nested implication.")
          (:description . "anc2r in set.mm."))
    (ass [→ → φ → ψ χ → φ → ψ ∧ χ φ])
    (proof
      [>and-r φ χ]
      [ded syl< χ [∧ χ φ] ψ φ]
      [infer ax2 φ [→ ψ χ] [→ ψ ∧ χ φ]]))

  ; was an.or
  (th "df.or" (wff ![φ ψ])
    (meta (:comment . "Conjunction in terms of disjunction (DeMorgan's law).")
          (:description . "Theorem *4.5 of Principia Mathematica p. 120; anor in set.mm."))
    (ass [↔ ∧ φ ψ ¬ ∨ ¬ φ ¬ ψ])
    (proof
      [df-an φ ψ]
      [!im!df.or [φ] [¬ ψ]]
      [infer neqt [→ φ ¬ ψ] [∨ ¬ φ ¬ ψ]]
      [bitr [∧ φ ψ] [¬ → φ ¬ ψ] [¬ ∨ ¬ φ ¬ ψ]]))
  (th-pattern !df.or)

  ; was nan.or
  (th "neg" (wff ![φ ψ])
    (meta (:comment . "Negated conjunction in terms of disjunction (DeMorgan's law).")
          (:description . "Theorem *4.51 of Principia Mathematica p. 120; ianor in set.mm."))
    (ass [↔ ¬ ∧ φ ψ ∨ ¬ φ ¬ ψ])
    (proof
      [df.or φ ψ]
      [infer neqt [∧ φ ψ] [¬ ∨ ¬ φ ¬ ψ]]
      [bneg> [∨ ¬ φ ¬ ψ]]
      [!bi!tri< [¬ ∧ φ ψ] [¬ ¬ ∨ ¬ φ ¬ ψ] [∨ ¬ φ ¬ ψ]]))
  (th-pattern !neg))

(in-context !or
  ; was nor.an
  (th "neg" (wff ![φ ψ])
    (meta (:comment . "Negated disjunction in terms of conjunction (DeMorgan's law).")
          (:description . "Compare Theorem *4,56 of Principia Mathematica p. 120; ioran in set.mm."))
    (ass [↔ ¬ ∨ φ ψ ∧ ¬ φ ¬ ψ])
    (proof
      [bneg> φ]
      [bneg> ψ]
      [!or!eqti φ [¬ ¬ φ] ψ [¬ ¬ ψ]]
      [infer neqt [∨ φ ψ] [∨ ¬ ¬ φ ¬ ¬ ψ]]
      [!an!df.or [¬ φ] [¬ ψ]]
      [!bi!tri< [¬ ∨ φ ψ] [¬ ∨ ¬ ¬ φ ¬ ¬ ψ] [∧ ¬ φ ¬ ψ]]))
  (th-pattern !neg)

  ; was: or.an
  (th "df.an" (wff ![φ ψ])
    (meta (:comment . "Disjunction in terms of conjunction (DeMorgan's law).")
          (:description . "Compare Theorem *4.57 of Principia Mathematica p. 120; oran in set.mm."))
    (ass [↔ ∨ φ ψ ¬ ∧ ¬ φ ¬ ψ])
    (proof
      [bneg> [∨ φ ψ]]
      [neg φ ψ]
      [infer neqt [¬ ∨ φ ψ] [∧ ¬ φ ¬ ψ]]
      [bitr [∨ φ ψ] [¬ ¬ ∨ φ ψ] [¬ ∧ ¬ φ ¬ ψ]]))
  (th-pattern !df.an) )

(in-context !an
  ; was siman<
  (th "sim<" (wff ![φ ψ])
    (meta (:comment . "Elimination of a conjunct.")
          (:description . "Theorem *3.26 (Simp) of Principia Mathematica p.112; pm3.26 in set.mm."))
    (ass [→ ∧ φ ψ φ])
    (proof
      [df-an φ ψ]
      [!im!sim< φ ψ]
      [b2i syl [∧ φ ψ] [¬ → φ ¬ ψ] φ]))
  (th-pattern !sim<)

  ; was siman>
  (th "sim>" (wff ![φ ψ])
    (meta (:comment . "Elimination of a conjunct.")
          (:description . "Theorem *3.27 (Simp) of Principia Mathematica p. 112; pm3.27 in set.mm."))
    (ass [→ ∧ φ ψ ψ])
    (proof
      [df-an φ ψ]
      [!im!sim> φ ψ]
      [b2i syl [∧ φ ψ] [¬ → φ ¬ ψ] ψ])) 
  (th-pattern !sim>))

(th "siman" (wff ![φ ψ])
    (meta (:description . "Example illustrating multiple theorem assertions."))
    (hypo [∧ φ ψ])
    (ass [φ] [ψ])
    (proof
      [!an!sim< φ ψ]
      [!an!sim> φ ψ]
      [ax-mp [∧ φ ψ] φ]
      [ax-mp [∧ φ ψ] ψ]))

(in-context !an
  ; was banc<
  (th "bjoin" (wff ![φ ψ])
    (meta (:comment . "Conjoin antecedent to left of consequent.")
          (:description . "Theorem *4.7 of Principia Mathematica p. 120; anclb in set.mm."))
    (ass [↔ → φ ψ → φ ∧ φ ψ])
    (proof
      [join φ ψ]
      [sim> φ ψ]
      [infer syl< [∧ φ ψ] ψ φ]
      [>bii [→ φ ψ] [→ φ ∧ φ ψ]]))

  ; was banc>
  (th "bjoin-r" (wff ![φ ψ])
    (meta (:comment . "Conjoin antecedent to right of consequent.")
          (:description . "ancrb in set.mm."))
    (ass [↔ → φ ψ → φ ∧ ψ φ])
    (proof
      [join-r φ ψ]
      [sim< ψ φ]
      [infer syl< [∧ ψ φ] ψ φ]
      [>bii [→ φ ψ] [→ φ ∧ ψ φ]])) )

; was pm3.4
(th "an->im" (wff ![φ ψ])
    (meta (:comment . "Conjunction implies implication.")
          (:description . "Theorem *3.4 of Principia Mathematica p. 113; pm3.4 in set.mm."))
    (ass [→ ∧ φ ψ → φ ψ])
    (proof
      [!an!sim> φ ψ]
      [ded ax1 ψ φ [∧ φ ψ]]))

(in-context !an
  ; was im.bldan<>
  (th "imti" (wff ![φ ψ χ θ])
    (meta (:comment . "Conjoin antecedents and consequents of two premises.")
          (:description . "anim12i in set.mm."))
    (hypo [→ φ ψ] [→ χ θ])
    (ass [→ ∧ φ χ ∧ ψ θ])
    (proof
      [sim< φ χ]
      [syl [∧ φ χ] φ ψ]
      [sim> φ χ]
      [syl [∧ φ χ] χ θ]
      [jca [∧ φ χ] ψ θ]))

  ; was im.bldan<
  (th "imt<" (wff ![φ ψ χ])
    (meta (:comment . "Introduce conjunct to both sides of an implication.")
          (:description . "Theorem *3.45 (Fact) of Principia Mathematica p. 113; pm3.45 in set.mm."))
    (ass [→ → φ ψ → ∧ φ χ ∧ ψ χ])
    (proof
      (suppose ([→ φ ψ]) ([→ ∧ φ χ ∧ ψ χ])
        [id χ]
        [imti φ ψ χ χ])))

  ; was im.bldan>
  (th "imt>" (wff ![φ ψ χ])
    (meta (:comment . "Introduce conjunct to both sides of an implication.")
          (:description . "Closed form of anim2i in set.mm."))
    (ass [→ → φ ψ → ∧ χ φ ∧ χ ψ])
    (proof
      (suppose ([→ φ ψ]) ([→ ∧ χ φ ∧ χ ψ])
        [id χ]
        [imti χ χ φ ψ]))) )

(in-context !or
  ; was im.bldor<>
  (th "imti" (wff ![φ ψ χ θ])
    (meta (:comment . "Disjoin antecedents and consequents of two premises.")
          (:description . "orim12i in set.mm."))
    (hypo [→ φ ψ] [→ χ θ])
    (ass [→ ∨ φ χ ∨ ψ θ])
    (proof
      [infer con φ ψ]
      [infer con χ θ]
      [!an!imti [¬ ψ] [¬ φ] [¬ θ] [¬ χ]]
      [infer con [∧ ¬ ψ ¬ θ] [∧ ¬ φ ¬ χ]]
      [df.an φ χ]
      [df.an ψ θ]
      [<imtr [¬ ∧ ¬ φ ¬ χ] [¬ ∧ ¬ ψ ¬ θ]
             [∨ φ χ]       [∨ ψ θ]]))

  ; was im.bldor<
  (th "imt<" (wff ![φ ψ χ])
    (meta (:comment . "Introduce disjunct to both sides of an implication.")
          (:description . "Closed form of orim1i in set.mm."))
    (ass [→ → φ ψ → ∨ φ χ ∨ ψ χ])
    (proof
      (suppose ([→ φ ψ]) ([→ ∨ φ χ ∨ ψ χ])
        [id χ]
        [imti φ ψ χ χ])))

  ; was im.bldor>
  (th "imt>" (wff ![φ ψ χ])
    (meta (:comment . "Introduce disjunct to both sides of an implication.")
          (:description . "Axiom *1.6 (Sum) of Principia Mathematica p. 97; orim2 in set.mm."))
    (ass [→ → φ ψ → ∨ χ φ ∨ χ ψ])
    (proof
      (suppose ([→ φ ψ]) ([→ ∨ χ φ ∨ χ ψ])
        [id χ]
        [imti χ χ φ ψ]))) )

(th "jao" (wff ![φ ψ χ])
    (meta (:comment . "Disjunction of antecedents.")
          (:description . "From set.mm; compare Theorem *3.44 of Principia Mathematica p. 113."))
    (ass [→ → φ χ → → ψ χ → ∨ φ ψ χ])
    (proof
      [>andc [¬ χ] [¬ φ] [¬ ψ]]
      [con< [χ] [∧ ¬ φ ¬ ψ]]
      [!im!rp33 [→ ¬ χ ¬ φ] [→ ¬ χ ¬ ψ] [→ ¬ χ ∧ ¬ φ ¬ ψ] [→ ¬ ∧ ¬ φ ¬ ψ χ]]
      [!or!df.an φ ψ]
      [b2i !im!rp43 [→ ¬ χ ¬ φ] [→ ¬ χ ¬ ψ] [¬ ∧ ¬ φ ¬ ψ] [χ] [∨ φ ψ]]
      [con φ χ]
      [con ψ χ]
      [!im!rp32 [→ ¬ χ ¬ φ] [→ ¬ χ ¬ ψ] [→ ∨ φ ψ χ] [→ ψ χ]]
      [syl [→ φ χ] [→ ¬ χ ¬ φ] [→ → ψ χ → ∨ φ ψ χ]]))

(th "jaoi" (wff ![φ ψ χ])
    (meta (:comment . "Inference disjoining the antecedents of two implications.")
          (:description . "From set.mm."))
    (hypo [→ φ χ] [→ ψ χ])
    (ass [→ ∨ φ ψ χ])
    (proof
      [infer jao φ ψ χ]
      [ax-mp [→ ψ χ] [→ ∨ φ ψ χ]]))

(th "impexp" (wff ![φ ψ χ])
    (meta (:comment . "Import-export theorem.")
          (:description . "From set.mm; part of Theorem *4.87 of Principia Mathematica p. 122."))
    (ass [↔ → ∧ φ ψ χ → φ → ψ χ])
    (proof
      [df-an φ ψ]
      [infer !im!eqt< [∧ φ ψ] [¬ → φ ¬ ψ] [χ]]
      [!im!imp φ ψ χ]
      [!im!exp φ ψ χ]
      [>bii [→ ¬ → φ ¬ ψ χ] [→ φ → ψ χ]]
      [bitr [→ ∧ φ ψ χ] [→ ¬ → φ ¬ ψ χ] [→ φ → ψ χ]]))

(th "imp" (wff ![φ ψ χ])
    (meta (:comment . "Importation inference.")
          (:description . "From set.mm."))
    (hypo [→ φ → ψ χ])
    (ass [→ ∧ φ ψ χ])
    (proof
      [infer impexp φ ψ χ]))

(in-context !im
  (th "tr:im" (wff ![φ ψ χ])
    (meta (:comment . "Transitivity of implication.")
          (:description . "Theorem *3.33 (Syll) of Principia Mathematica p. 112; pm3.33 in set.mm."))
    (ass [→ ∧ → φ ψ → ψ χ → φ χ])
    (proof
      [imt< φ ψ χ]
      [!/prop!imp [→ φ ψ] [→ ψ χ] [→ φ χ]])))

(in-context !an
  (th "mpclosed" (wff ![φ ψ])
    (meta (:comment . "Conjunctive detachment.")
          (:description . "Theorem *3.35 of Principia Mathematica p. 112; pm3.35 in set.mm."))
    (ass [→ ∧ φ → φ ψ ψ])
    (proof
      [mpclosed φ ψ]
      [imp φ [→ φ ψ] ψ])))

(th "imp31" (wff ![φ ψ χ θ])
    (meta (:comment . "An importation inference.")
          (:description . "From set.mm."))
    (hypo [→ φ → ψ → χ θ])
    (ass [→ ∧ ∧ φ ψ χ θ])
    (proof
      [imp φ ψ [→ χ θ]]
      [imp [∧ φ ψ] χ θ]))

(th "imp32" (wff ![φ ψ χ θ])
    (meta (:comment . "An importation inference.")
          (:description . "From set.mm."))
    (hypo [→ φ → ψ → χ θ])
    (ass [→ ∧ φ ∧ ψ χ θ])
    (proof
      [ded impexp ψ χ θ φ]
      [imp φ [∧ ψ χ] θ]))

(th "imp4a" (wff ![φ ψ χ θ τ])
    (meta (:comment . "An importation inference.")
          (:description . "From set.mm."))
    (hypo [→ φ → ψ → χ → θ τ])
    (ass [→ φ → ψ → ∧ χ θ τ])
    (proof
      [impexp χ θ τ]
      [b2i !im!rp33 φ ψ [→ χ → θ τ] [→ ∧ χ θ τ]]))

(th "imp4b" (wff ![φ ψ χ θ τ])
    (meta (:comment . "An importation inference.")
          (:description . "From set.mm."))
    (hypo [→ φ → ψ → χ → θ τ])
    (ass [→ ∧ φ ψ → ∧ χ θ τ])
    (proof
      [imp4a φ ψ χ θ τ]
      [imp φ ψ [→ ∧ χ θ τ]]))

; todo infer<>
(th "exp" (wff ![φ ψ χ])
    (meta (:comment . "Exportation inference.")
          (:description . "From set.mm."))
    (hypo [→ ∧ φ ψ χ])
    (ass [→ φ → ψ χ])
    (proof
      [infer impexp φ ψ χ]))

(th "exp31" (wff ![φ ψ χ θ])
    (meta (:comment . "An exportation inference.")
          (:description . "From set.mm."))
    (hypo [→ ∧ ∧ φ ψ χ θ])
    (ass [→ φ → ψ → χ θ])
    (proof
      [exp [∧ φ ψ] χ θ]
      [exp φ ψ [→ χ θ]]))

(th "exp32" (wff ![φ ψ χ θ])
    (meta (:comment . "An exportation inference.")
          (:description . "From set.mm."))
    (hypo [→ ∧ φ ∧ ψ χ θ])
    (ass [→ φ → ψ → χ θ])
    (proof
      [exp φ [∧ ψ χ] θ]
      [ded impexp ψ χ θ φ]))

(th "exp4a" (wff ![φ ψ χ θ τ])
    (meta (:comment . "An exportation inference.")
          (:description . "From set.mm."))
    (hypo [→ φ → ψ → ∧ χ θ τ])
    (ass [→ φ → ψ → χ → θ τ])
    (proof
      [impexp χ θ τ]
      [b2i !im!rp33 φ ψ [→ ∧ χ θ τ] [→ χ → θ τ]]))

(in-context !an
  ; was adan<
  (th "add<" (wff ![φ ψ χ])
    (meta (:comment . "Theorem adding a conjunct to the left of an antecedent.")
          (:description . "Closed form of adantl in set.mm."))
    (ass [→ → φ ψ → ∧ χ φ ψ])
    (proof
      [sim> χ φ]
      [infer syl> [∧ χ φ] φ ψ]))

  ; was adan>
  (th "add>" (wff ![φ ψ χ])
    (meta (:comment . "Theorem adding a conjunct to the right of an antecedent.")
          (:description . "Closed form of adantr in set.mm."))
    (ass [→ → φ ψ → ∧ φ χ ψ])
    (proof
      [sim< φ χ]
      [infer syl> [∧ φ χ] φ ψ])) )

; todo: rename
(th "bi>an" (wff ![φ ψ χ])
    (meta (:comment . "Inference from a logical equivalence.")
          (:description . "biimpa in set.mm."))
    (hypo [→ φ ↔ ψ χ])
    (ass [→ ∧ φ ψ χ])
    (proof
      [ded bi> ψ χ φ]
      [imp φ ψ χ]))

(th "bi<an" (wff ![φ ψ χ])
    (meta (:comment . "Inference from a logical equivalence.")
          (:description . "biimpar in set.mm."))
    (hypo [→ φ ↔ ψ χ])
    (ass [→ ∧ φ χ ψ])
    (proof
      [ded bi< ψ χ φ]
      [imp φ χ ψ]))

(th "bi>anc" (wff ![φ ψ χ])
    (meta (:comment . "Inference from a logical equivalence.")
          (:description . "biimpac in set.mm."))
    (hypo [→ φ ↔ ψ χ])
    (ass [→ ∧ ψ φ χ])
    (proof
      [!bi!cd> φ ψ χ]
      [imp ψ φ χ]))

(th "bi<anc" (wff ![φ ψ χ])
    (meta (:comment . "Inference from a logical equivalence.")
          (:description . "biimparc in set.mm."))
    (hypo [→ φ ↔ ψ χ])
    (ass [→ ∧ χ φ ψ])
    (proof
      [!bi!cd< φ ψ χ]
      [imp χ φ ψ]))

(th "bjao" (wff ![φ ψ χ])
    (meta (:comment . "Disjunction of antecedents.")
          (:description . "Compare Theorem *4.77 of Principia Mathematica p. 121; jaob in set.mm."))
    (ass [↔ → ∨ φ ψ χ ∧ → φ χ → ψ χ])
    (proof
      [or< φ ψ]
      [infer syl> φ [∨ φ ψ] χ]
      [or> φ ψ]
      [infer syl> ψ [∨ φ ψ] χ]
      [jca [→ ∨ φ ψ χ] [→ φ χ] [→ ψ χ]]
      [jao φ ψ χ]
      [imp [→ φ χ] [→ ψ χ] [→ ∨ φ ψ χ]]
      [>bii [→ ∨ φ ψ χ] [∧ → φ χ → ψ χ]]))

(th "jaod" (wff ![φ ψ χ θ])
    (meta (:comment . "Deduction disjoining the antecedents of two implications.")
          (:description . "From set.mm."))
    (hypo [→ φ → ψ θ] [→ φ → χ θ])
    (ass [→ φ → ∨ ψ χ θ])
    (proof
      [jao ψ χ θ]
      [!im!sylc [→ ψ θ] [→ χ θ] [→ ∨ ψ χ θ] [φ]]))

(th "jaao" (wff ![φ ψ χ θ τ])
    (meta (:comment . "Inference conjoining and disjoining the antecedents of two implications.")
          (:description . "From set.mm."))
    (hypo [→ φ → χ τ] [→ ψ → θ τ])
    (ass [→ ∧ φ ψ → ∨ χ θ τ])
    (proof
      [infer !an!add> φ [→ χ τ] ψ]
      [infer !an!add< ψ [→ θ τ] φ]
      [jaod [∧ φ ψ] χ θ τ]))

(in-context !an
  (th "idm" (wff "φ")
    (meta (:comment . "Idempotent law for conjunction.")
          (:description . "Theorem *4.24 of Principia Mathematica; anidm in set.mm."))
    (ass [↔ ∧ φ φ φ])
    (proof
      [sim< φ φ]
      [id φ]
      [jca φ φ φ]
      [>bii [∧ φ φ] φ]))
  (th-pattern !idm)

  (th "com" (wff ![φ ψ])
    (meta (:comment . "Commutative law for conjunction.")
          (:description . "Theorem *4.3 of Principia Mathematica; ancom in set.mm."))
    (ass [↔ ∧ φ ψ ∧ ψ φ])
    (proof
      [sim< φ ψ]
      [sim> φ ψ]
      [jca [∧ φ ψ] ψ φ]
      [sim< ψ φ]
      [sim> ψ φ]
      [jca [∧ ψ φ] φ ψ]
      [>bii [∧ φ ψ] [∧ ψ φ]]))
  (th-pattern !com)

  (th "comsd" (wff ![φ ψ χ θ])
    (meta (:comment . "Deduction commuting conjunction in antecedent.")
          (:description . "ancomsd in set.mm."))
    (hypo [→ φ → ∧ ψ χ θ])
    (ass  [→ φ → ∧ χ ψ θ])
    (proof
      [com χ ψ]
      [b2i !im!rp32 φ [∧ ψ χ] θ [∧ χ ψ]]))

  (th "ass" (wff ![φ ψ χ])
    (meta (:comment . "Associative law for conjunction.")
          (:description . "Theorem *4.32 of Principia Mathematica p. 118; anass in set.mm."))
    (ass [↔ ∧ ∧ φ ψ χ ∧ φ ∧ ψ χ])
    (proof
      [impexp φ ψ [¬ χ]]
      [!im!df.nan ψ χ]
      [infer !im!eqt> [→ ψ ¬ χ] [¬ ∧ ψ χ] φ]
      [bitr [→ ∧ φ ψ ¬ χ] [→ φ → ψ ¬ χ] [→ φ ¬ ∧ ψ χ]]
      [infer neqt [→ ∧ φ ψ ¬ χ] [→ φ ¬ ∧ ψ χ]]
      [df-an [∧ φ ψ] χ]
      [df-an φ [∧ ψ χ]]
      [<bitr [¬ → ∧ φ ψ ¬ χ] [¬ → φ ¬ ∧ ψ χ]
             [∧ ∧ φ ψ χ]     [∧ φ ∧ ψ χ]])) )

(in-context !im
  ; was im.andi-x
  (th "distan" (wff ![φ ψ χ])
    (meta (:comment . "Distribution of implication with conjunction.")
          (:description . "imdistan in set.mm."))
    (ass [↔ → φ → ψ χ → ∧ φ ψ ∧ φ χ])
    (proof
      [!an!join2 φ ψ χ]
      [ded impexp φ ψ [∧ φ χ] [→ φ → ψ χ]]
      [!an!sim> φ χ]
      [infer syl< [∧ φ χ] χ [∧ φ ψ]]
      [ded impexp φ ψ χ [→ ∧ φ ψ ∧ φ χ]]
      [>bii [→ φ → ψ χ] [→ ∧ φ ψ ∧ φ χ]])) )

(in-context !an
  ; was sylan<
  (th "syl<" (wff ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference.")
          (:description . "sylan in set.mm."))
    (hypo [→ ∧ φ ψ χ] [→ θ φ])
    (ass [→ ∧ θ ψ χ])
    (proof
      [exp φ ψ χ]
      [syl θ φ [→ ψ χ]]
      [imp θ ψ χ]))

  ; was sylan>
  (th "syl>" (wff ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference.")
          (:description . "sylan2 in set.mm."))
    (hypo [→ ∧ φ ψ χ] [→ θ ψ])
    (ass [→ ∧ φ θ χ])
    (proof
      [exp φ ψ χ]
      [!im!rp32 φ ψ χ θ]
      [imp φ θ χ]))

  ; was sylan<>
  (th "syl2" (wff ![φ ψ χ θ τ])
    (meta (:comment . "A double syllogism inference.")
          (:description . "syl2an in set.mm."))
    (hypo [→ ∧ φ ψ χ] [→ θ φ] [→ τ ψ])
    (ass [→ ∧ θ τ χ])
    (proof
      [syl< φ ψ χ θ]
      [syl> θ ψ χ τ]))

  ; was sylan<d
  (th "syl<d" (wff ![φ ψ χ θ τ])
    (meta (:comment . "A syllogism deduction.")
          (:description . "syland in set.mm."))
    (hypo [→ φ → ∧ ψ χ θ] [→ φ → τ ψ])
    (ass [→ φ → ∧ τ χ θ])
    (proof
      [ded impexp ψ χ θ φ]
      [!im!trd φ τ ψ [→ χ θ]]
      [ded impexp τ χ θ φ]))

  ; was sylan>d
  (th "syl>d" (wff ![φ ψ χ θ τ])
    (meta (:comment . "A syllogism deduction.")
          (:description . "sylan2d in set.mm."))
    (hypo [→ φ → ∧ ψ χ θ] [→ φ → τ χ])
    (ass [→ φ → ∧ ψ τ θ])
    (proof
      [!an!comsd φ ψ χ θ]
      [syl<d φ χ ψ θ τ]
      [!an!comsd φ τ ψ θ]))

  ; was sylan<>d
  (th "syl2d" (wff ![φ ψ χ θ τ η])
    (meta (:comment . "A syllogism deduction.")
          (:description . "syl2and in set.mm."))
    (hypo [→ φ → ∧ ψ χ θ] [→ φ → τ ψ] [→ φ → η χ])
    (ass [→ φ → ∧ τ η θ])
    (proof
      [syl<d φ ψ χ θ τ]
      [syl>d φ τ χ θ η]))

  ; was sylan<i
  (th "syl<i" (wff ![φ ψ χ θ τ])
    (meta (:comment . "A syllogism inference.")
          (:description . "sylani in set.mm."))
    (hypo [→ φ → ∧ ψ χ θ] [→ τ ψ])
    (ass  [→ φ → ∧ τ χ θ])
    (proof
      [ded impexp ψ χ θ φ]
      [!im!rp32 φ ψ [→ χ θ] τ]
      [ded impexp τ χ θ φ]))

  ; was sylan>i
  (th "syl>i" (wff ![φ ψ χ θ τ])
    (meta (:commment . "A syllogism inference.")
          (:description . "sylan2i in set.mm."))
    (hypo [→ φ → ∧ ψ χ θ] [→ τ χ])
    (ass  [→ φ → ∧ ψ τ θ])
    (proof
      [ded impexp ψ χ θ φ]
      [!im!rp43 φ ψ χ θ τ]
      [ded impexp ψ τ θ φ]))

  ; was sylan<>i
  (th "syl2i" (wff ![φ ψ χ θ τ η])
    (meta (:comment . "A syllogism inference.")
          (:description . "syl2ani in set.mm."))
    (hypo [→ φ → ∧ ψ χ θ] [→ τ ψ] [→ η χ])
    (ass  [→ φ → ∧ τ η θ])
    (proof
      [syl<i φ ψ χ θ τ]
      [syl>i φ τ χ θ η]))

  ; was sylan9
  (th "syl9" (wff ![φ ψ χ θ τ])
    (meta (:comment . "Nested syllogism inference conjoining dissimilar antecedents.")
          (:description . "sylan9 in set.mm."))
    (hypo [→ φ → ψ χ] [→ θ → χ τ])
    (ass [→ ∧ φ θ → ψ τ])
    (proof
      [!im!syl9 φ ψ χ θ τ]
      [imp φ θ [→ ψ τ]]))

  (th "sylc" (wff ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference combined with contraction.")
          (:description . "sylanc in set.mm."))
    (hypo [→ ∧ φ ψ χ] [→ θ φ] [→ θ ψ])
    (ass [→ θ χ])
    (proof
      [jca θ φ ψ]
      [syl θ [∧ φ ψ] χ]))

  ; was casean<
  (th "case<" (wff ![φ ψ χ])
    (meta (:comment . "Elimination of an antecedent.")
          (:description . "pm2.61an1 in set.mm."))
    (hypo [→ ∧ φ ψ χ] [→ ∧ ¬ φ ψ χ])
    (ass [→ ψ χ])
    (proof
      [exp φ ψ χ]
      [exp [¬ φ] ψ χ]
      [casei φ [→ ψ χ]]))

  ; was casean>
  (th "case>" (wff ![φ ψ χ])
    (meta (:comment . "Elimination of an antecedent.")
          (:description . "pm2.61an2 in set.mm."))
    (hypo [→ ∧ φ ψ χ] [→ ∧ φ ¬ ψ χ])
    (ass [→ φ χ])
    (proof
      [exp φ ψ χ]
      [exp φ [¬ ψ] χ]
      [cased φ ψ χ])) )

; todo: rename
(th "abai" (wff ![φ ψ])
    (meta (:comment . "Introduce one conjunct as an antecedent to the other.")
          (:description . "From set.mm."))
    (ass [↔ ∧ φ ψ ∧ φ → φ ψ])
    (proof
      [!an!sim< φ ψ]
      [an->im φ ψ]
      [jca [∧ φ ψ] φ [→ φ ψ]]
      [!an!sim< φ [→ φ ψ]]
      [!an!mpclosed φ ψ]
      [jca [∧ φ → φ ψ] φ ψ]
      [>bii [∧ φ ψ] [∧ φ → φ ψ]]))

(in-context !an
  (th "eqt>" (wff ![φ ψ χ])
    (meta (:comment . "Introduce a left conjunct to both sides of a logical equivalence.")
          (:description . "Closed form of anbi2i in set.mm."))
    (ass [→ ↔ φ ψ ↔ ∧ χ φ ∧ χ ψ])
    (proof
      (suppose ([↔ φ ψ]) ([↔ ∧ χ φ ∧ χ ψ])
        [infer bi>  φ ψ]
        [infer imt> φ ψ χ]
        [infer bi<  φ ψ]
        [infer imt> ψ φ χ]
        [>bii [∧ χ φ] [∧ χ ψ]])))

  (th "eqt<" (wff ![φ ψ χ])
    (meta (:comment . "Introduce a right conjunct to both sides of a logical equivalence.")
          (:description . "Theorem *4.36 of Principia Mathematica p. 118; anbi1 in set.mm."))
    (ass [→ ↔ φ ψ ↔ ∧ φ χ ∧ ψ χ])
    (proof
      (suppose ([↔ φ ψ]) ([↔ ∧ φ χ ∧ ψ χ])
        [com φ χ]
        [infer eqt> φ ψ χ]
        [com χ ψ]
        [2bitr [∧ φ χ] [∧ χ φ] [∧ χ ψ] [∧ ψ χ]])))

  (th "eqti" (wff ![φ ψ χ θ])
    (meta (:comment . "Conjoin both sides of two equivalences.")
          (:description . "anbi12i in set.mm."))
    (hypo [↔ φ ψ] [↔ χ θ])
    (ass [↔ ∧ φ χ ∧ ψ θ])
    (proof
      [infer eqt< φ ψ χ]
      [infer eqt> χ θ ψ]
      [bitr [∧ φ χ] [∧ ψ χ] [∧ ψ θ]])) )

(in-context !an
  ; was an12ass
  (th "12ass" (wff ![φ ψ χ])
    (meta (:comment . "A rearrangement of conjuncts.")
          (:description . "an12 in set.mm."))
    (ass [↔ ∧ φ ∧ ψ χ ∧ ψ ∧ φ χ])
    (proof
      [com φ ψ]
      [infer eqt< [∧ φ ψ] [∧ ψ φ] χ]
      [ass φ ψ χ]
      [ass ψ φ χ]
      [>bitr [∧ ∧ φ ψ χ] [∧ ∧ ψ φ χ]
             [∧ φ ∧ ψ χ] [∧ ψ ∧ φ χ]]))

  ; was an23ass
  (th "23ass" (wff ![φ ψ χ])
    (meta (:comment . "A rearrangement of conjuncts.")
          (:description . "an23 in set.mm."))
    (ass [↔ ∧ ∧ φ ψ χ ∧ ∧ φ χ ψ])
    (proof
      [com ψ χ]
      [infer eqt> [∧ ψ χ] [∧ χ ψ] φ]
      [ass φ ψ χ]
      [ass φ χ ψ]
      [<bitr [∧ φ ∧ ψ χ] [∧ φ ∧ χ ψ]
             [∧ ∧ φ ψ χ] [∧ ∧ φ χ ψ]])) )

(in-context !an
  ; was anabs<
  (th "idm<" (wff ![φ ψ])
    (meta (:comment . "Absorption into embedded conjunct.")
          (:description . "anabs5 in set.mm."))
    (ass [↔ ∧ φ ∧ φ ψ ∧ φ ψ])
    (proof
      [sim< φ ψ]
      [id [∧ φ ψ]]
      [jca [∧ φ ψ] φ [∧ φ ψ]]
      [sim> φ [∧ φ ψ]]
      [>bii [∧ φ ∧ φ ψ] [∧ φ ψ]]))

  ; was anabs>
  (th "idm>" (wff ![φ ψ])
    (meta (:comment . "Absorption into embedded conjunct.")
          (:description . "anabs7 in set.mm."))
    (ass [↔ ∧ ψ ∧ φ ψ ∧ φ ψ])
    (proof
      [sim> φ ψ]
      [id [∧ φ ψ]]
      [jca [∧ φ ψ] ψ [∧ φ ψ]]
      [sim> ψ [∧ φ ψ]]
      [>bii [∧ ψ ∧ φ ψ] [∧ φ ψ]]))

  ; was 4anass
  (th "4ass" (wff ![φ ψ χ θ])
    (meta (:comment . "Rearrangement of 4 conjuncts.")
          (:description . "an4 in set.mm."))
    (ass [↔ ∧ ∧ φ ψ ∧ χ θ ∧ ∧ φ χ ∧ ψ θ])
    (proof
      [12ass ψ χ θ]
      [infer eqt> [∧ ψ ∧ χ θ] [∧ χ ∧ ψ θ] φ]
      [ass φ ψ [∧ χ θ]]
      [ass φ χ [∧ ψ θ]]
      [<bitr [∧ φ ∧ ψ ∧ χ θ] [∧ φ ∧ χ ∧ ψ θ]
             [∧ ∧ φ ψ ∧ χ θ] [∧ ∧ φ χ ∧ ψ θ]]))

  ; was 4anass2
  (th "4ass2" (wff ![φ ψ χ θ])
    (meta (:comment . "Rearrangement of 4 conjuncts.")
          (:description . "an42 in set.mm."))
    (ass [↔ ∧ ∧ φ ψ ∧ χ θ ∧ ∧ φ χ ∧ θ ψ])
    (proof
      [4ass φ ψ χ θ]
      [com ψ θ]
      [infer eqt> [∧ ψ θ] [∧ θ ψ] [∧ φ χ]]
      [bitr [∧ ∧ φ ψ ∧ χ θ] [∧ ∧ φ χ ∧ ψ θ] [∧ ∧ φ χ ∧ θ ψ]]))

  ; was an.andi<
  (th "di-an<" (wff ![φ ψ χ])
    (meta (:comment . "Distribution of conjunction over conjunction.")
          (:description . "anandi in set.mm."))
    (ass [↔ ∧ φ ∧ ψ χ ∧ ∧ φ ψ ∧ φ χ])
    (proof
      [idm φ]
      [!bi!comi [∧ φ φ] φ]
      [infer eqt< φ [∧ φ φ] [∧ ψ χ]]
      [4ass φ φ ψ χ]
      [bitr [∧ φ ∧ ψ χ] [∧ ∧ φ φ ∧ ψ χ] [∧ ∧ φ ψ ∧ φ χ]]))

  ; was an.andi>
  (th "di-an>" (wff ![φ ψ χ])
    (meta (:comment . "Distribution of conjunction over conjunction.")
          (:description . "anandir in set.mm."))
    (ass [↔ ∧ ∧ φ ψ χ ∧ ∧ φ χ ∧ ψ χ])
    (proof
      [idm χ]
      [!bi!comi [∧ χ χ] χ]
      [infer eqt> χ [∧ χ χ] [∧ φ ψ]]
      [4ass φ ψ χ χ]
      [bitr [∧ ∧ φ ψ χ] [∧ ∧ φ ψ ∧ χ χ] [∧ ∧ φ χ ∧ ψ χ]])) )

(in-context !bi
  (th "df.an" (wff ![φ ψ])
    (meta (:comment . "A theorem similar to the standard definition of the biconditional.")
          (:description . "Definition of [Margaris] p. 49; bi in set.mm."))
    (ass [↔ ↔ φ ψ ∧ → φ ψ → ψ φ])
    (proof
      [!bi!df2 φ ψ]
      [df-an [→ φ ψ] [→ ψ φ]]
      [!bi!tri< [↔ φ ψ] [¬ → → φ ψ ¬ → ψ φ] [∧ → φ ψ → ψ φ]]))
  (th-pattern !df.an))

(alias "dfbi" (wff ![φ ψ])
  [!bi!df.an φ ψ])

(th ">bid" (wff ![φ ψ χ])
    (meta (:comment . "Deduce an equivalence from two implications.")
          (:description . "impbid in set.mm."))
    (hypo [→ φ → ψ χ] [→ φ → χ ψ])
    (ass [→ φ ↔ ψ χ])
    (proof
      [jca φ [→ ψ χ] [→ χ ψ]]
      [dfbi ψ χ]
      [b2i syl φ [∧ → ψ χ → χ ψ] [↔ ψ χ]]))

(in-context !bi
  (th "com" (wff ![φ ψ])
    (meta (:comment . "Commutative law for equivalence.")
          (:description . "Theorem *4.21 of Principia Mathematica p. 117; bicom in set.mm."))
    (ass [↔ ↔ φ ψ ↔ ψ φ])
    (proof
      [!an!com [→ φ ψ] [→ ψ φ]]
      [df.an φ ψ]
      [df.an ψ φ]
      [<tr [∧ → φ ψ → ψ φ] [∧ → ψ φ → φ ψ]
           [↔ φ ψ]         [↔ ψ φ]]))
  (th-pattern !com))

(th "conb" (wff ![φ ψ])
    (meta (:comment . "Contraposition.")
          (:description . "Theorem *4.11 of Principia Mathematica p. 117; pm4.11 in set.mm."))
    (ass [↔ ↔ φ ψ ↔ ¬ φ ¬ ψ])
    (proof
      [bcon φ ψ]
      [bcon ψ φ]
      [!an!eqti [→ φ ψ] [→ ¬ ψ ¬ φ] [→ ψ φ] [→ ¬ φ ¬ ψ]]
      [dfbi φ ψ]
      [dfbi [¬ ψ] [¬ φ]]
      [<bitr [∧ → φ ψ → ψ φ] [∧ → ¬ ψ ¬ φ → ¬ φ ¬ ψ]
             [↔ φ ψ]         [↔ ¬ ψ ¬ φ]]
      [!bi!com [¬ ψ] [¬ φ]]
      [bitr [↔ φ ψ] [↔ ¬ ψ ¬ φ] [↔ ¬ φ ¬ ψ]]))
(th-pattern !conb)

(th "conb>" (wff ![φ ψ])
    (meta (:comment . "Contraposition.")
          (:description . "Theorem *4.12 of Principia Mathematica p. 117; con2bi in set.mm."))
    (ass [↔ ↔ φ ¬ ψ ↔ ψ ¬ φ])
    (proof
      [bcon> φ ψ]
      [bcon< ψ φ]
      [!an!eqti [→ φ ¬ ψ] [→ ψ ¬ φ] [→ ¬ ψ φ] [→ ¬ φ ψ]]
      [dfbi φ [¬ ψ]]
      [dfbi ψ [¬ φ]]
      [<bitr [∧ → φ ¬ ψ → ¬ ψ φ] [∧ → ψ ¬ φ → ¬ φ ψ]
             [↔ φ ¬ ψ]           [↔ ψ ¬ φ]]))
(th-pattern !conb>)

(th "conb<" (wff ![φ ψ])
    (meta (:comment . "Contraposition."))
    (ass [↔ ↔ ¬ φ ψ ↔ ¬ ψ φ])
    (proof
      [!bi!com [¬ φ] ψ]
      [conb> ψ φ]
      [!bi!com φ [¬ ψ]]
      [2bitr [↔ ¬ φ ψ] [↔ ψ ¬ φ] [↔ φ ¬ ψ] [↔ ¬ ψ φ]]))
(th-pattern !conb<)

(in-context !bi
  (th "trd" (wff ![φ ψ χ θ])
    (meta (:comment . "Deduction form of <a href=\"prop/bi/tri.html\">tri</a>")
          (:description . "bitrd in set.mm."))
    (hypo [→ φ ↔ ψ χ] [→ φ ↔ χ θ])
    (ass  [→ φ ↔ ψ θ])
    (proof
      [ded bi> ψ χ φ]
      [ded bi> χ θ φ]
      [!im!trd φ ψ χ θ]
      [ded bi< χ θ φ]
      [ded bi< ψ χ φ]
      [!im!trd φ θ χ ψ]
      [>bid φ ψ θ]))

  (th "trd-r" (wff ![φ ψ χ θ])
    (meta (:comment . "Deduction form of <a href=\"prop/bi/tri-r.html\">tri-r</a>")
          (:description . "bitr2d in set.mm."))
    (hypo [→ φ ↔ ψ χ] [→ φ ↔ χ θ])
    (ass  [→ φ ↔ θ ψ])
    (proof
      [trd φ ψ χ θ]
      [ded com ψ θ φ]))

  (th "trd>" (wff ![φ ψ χ θ])
    (meta (:comment . "Deduction form of <a href=\"prop/bi/tri.html\">tri&gt;</a>")
          (:description . "bitr3d in set.mm."))
    (hypo [→ φ ↔ χ ψ] [→ φ ↔ χ θ])
    (ass  [→ φ ↔ ψ θ])
    (proof
      [ded com χ ψ φ]
      [trd φ ψ χ θ]))

  (th "trd<" (wff ![φ ψ χ θ])
    (meta (:comment . "Deduction form of <a href=\"prop/bi/tri.html\">tri&lt;</a>")
          (:description . "bitr4d in set.mm."))
    (hypo [→ φ ↔ ψ χ] [→ φ ↔ θ χ])
    (ass  [→ φ ↔ ψ θ])
    (proof
      [ded com θ χ φ]
      [trd φ ψ χ θ])) )

(in-context !an
  (th "bsyl9" (wff ![φ ψ χ θ τ])
    (meta (:comment . "Nested syllogism inference conjoining dissimilar antecedents.")
          (:description . "sylan9bb in set.mm."))
    (hypo [→ φ ↔ ψ χ] [→ θ ↔ χ τ])
    (ass  [→ ∧ φ θ ↔ ψ τ])
    (proof
      [infer add> φ [↔ ψ χ] θ]
      [infer add< θ [↔ χ τ] φ]
      [!bi!trd [∧ φ θ] ψ χ τ]))

  (th "bsyl9-r" (wff ![φ ψ χ θ τ])
    (meta (:comment . "Nested syllogism inference conjoining dissimilar antecedents.")
          (:description . "sylan9bbr in set.mm."))
    (hypo [→ φ ↔ ψ χ] [→ θ ↔ χ τ])
    (ass  [→ ∧ θ φ ↔ ψ τ])
    (proof
      [bsyl9 φ ψ χ θ τ]
      [+syl [com θ φ] [↔ ψ τ]])) )

(in-context !im
  (th ">trd" (wff ![φ ψ χ θ τ])
    (meta (:comment . "More general version of 3imtr3.")
          (:description . "Useful for converting conditional definitions in a formula. 3imtr3d in set.mm."))
    (hypo [→ φ → ψ χ] [→ φ ↔ ψ θ] [→ φ ↔ χ τ])
    (ass  [→ φ → θ τ])
    (proof
      [ded bi< ψ θ φ]
      [trd φ θ ψ χ]
      [ded bi> χ τ φ]
      [trd φ θ χ τ]))

  (th "<trd" (wff ![φ ψ χ θ τ])
    (meta (:comment . "More general version of 3imtr4.")
          (:description . "Useful for converting conditional definitions in a formula. 3imtr4d in set.mm."))
    (hypo [→ φ → ψ χ] [→ φ ↔ θ ψ] [→ φ ↔ τ χ])
    (ass  [→ φ → θ τ])
    (proof
      [ded !bi!com θ ψ φ]
      [ded !bi!com τ χ φ]
      [>trd φ ψ χ θ τ])) )

(in-context !bi
  (th "2trd" (wff ![φ ψ χ θ τ])
    (meta (:comment . "Deduction from transitivity of biconditional.")
          (:description . "3bitrd in set.mm."))
    (hypo [→ φ ↔ ψ χ] [→ φ ↔ χ θ] [→ φ ↔ θ τ])
    (ass  [→ φ ↔ ψ τ])
    (proof
      [trd φ ψ χ θ]
      [trd φ ψ θ τ]))

(th ">trd" (wff ![φ ψ χ θ τ])
    (meta (:comment . "Deduction from transitivity of biconditional.")
          (:description . "Useful for converting conditional definitions in a formula. 3bitr3d in set.mm."))
    (hypo [→ φ ↔ ψ χ] [→ φ ↔ ψ θ] [→ φ ↔ χ τ])
    (ass  [→ φ ↔ θ τ])
    (proof
      [ded com ψ θ φ]
      [2trd φ θ ψ χ τ]))

  (th "<trd" (wff ![φ ψ χ θ τ])
    (meta (:comment . "Deduction from transitivity of biconditonal.")
          (:description . "Useful for converting conditional definitions in a formula. 3bitr4d in set.mm"))
    (hypo [→ φ ↔ ψ χ] [→ φ ↔ θ ψ] [→ φ ↔ τ χ])
    (ass  [→ φ ↔ θ τ])
    (proof
      [ded com τ χ φ]
      [2trd φ θ ψ χ τ])) )

(in-context !im
  (th ">trg" (wff ![φ ψ χ θ τ])
    (meta (:comment . "More general version of 3imtr3.")
          (:description . "Useful for converting definitions in a formula. 3imtr3g in set.mm."))
    (hypo [→ φ → ψ χ] [↔ ψ θ] [↔ χ τ])
    (ass  [→ φ → θ τ])
    (proof
      [infer ax1 [↔ ψ θ] φ]
      [infer ax1 [↔ χ τ] φ]
      [>trd φ ψ χ θ τ]))

  (th "<trg" (wff ![φ ψ χ θ τ])
    (meta (:comment . "More general version of 3imtr4.")
          (:description . "Useful for converting definitions in a formula. 3imtr4g in set.mm."))
    (hypo [→ φ → ψ χ] [↔ θ ψ] [↔ τ χ])
    (ass  [→ φ → θ τ])
    (proof
      [infer ax1 [↔ θ ψ] φ]
      [infer ax1 [↔ τ χ] φ]
      [<trd φ ψ χ θ τ])) )

(alias "<imtrg" (wff ![φ ψ χ θ τ])
  [!im!<trg φ ψ χ θ τ])
(alias ">imtrg" (wff ![φ ψ χ θ τ])
  [!im!>trg φ ψ χ θ τ])

(in-context !bi
  (th ">trg" (wff ![φ ψ χ θ τ])
    (meta (:comment . "More general version of 3bitr3.")
          (:description . "Useful for converting definitions in a formula. 3bitr3g in set.mm."))
    (hypo [→ φ ↔ ψ χ] [↔ ψ θ] [↔ χ τ])
    (ass  [→ φ ↔ θ τ])
    (proof
      [infer ax1 [↔ ψ θ] φ]
      [infer ax1 [↔ χ τ] φ]
      [>trd φ ψ χ θ τ]))

  (th "<trg" (wff ![φ ψ χ θ τ])
    (meta (:comment . "More general version of 3bitr4.")
          (:description . "Useful for converting definitions in a formula. 3bitr4g in set.mm."))
    (hypo [→ φ ↔ ψ χ] [↔ θ ψ] [↔ τ χ])
    (ass  [→ φ ↔ θ τ])
    (proof
      [infer ax1 [↔ θ ψ] φ]
      [infer ax1 [↔ τ χ] φ]
      [<trd φ ψ χ θ τ])) )

(alias "<bitrg" (wff ![φ ψ χ θ τ])
  [!bi!<trg φ ψ χ θ τ])
(alias ">bitrg" (wff ![φ ψ χ θ τ])
  [!bi!>trg φ ψ χ θ τ])

; was praeclarum
(in-context !an
  (th "imt" (wff ![φ ψ χ θ])
    (meta (:description . "Theorem *3.47 of Principia Mathematica p. 113; prth in set.mm. It was proved by Leibniz, and it evidently pleased him enough to call it 'praeclarum theorema.'"))
    (ass [→ ∧ → φ ψ → χ θ → ∧ φ χ ∧ ψ θ])
    (proof
      [>and ψ θ]
      [ded !im!tr> θ [∧ ψ θ] χ ψ]
      [infer !im!tr> ψ [→ → χ θ → χ ∧ ψ θ] φ]
      [!im!com23i [→ φ ψ] φ [→ χ θ] [→ χ ∧ ψ θ]]
      [imp4b [→ φ ψ] [→ χ θ] φ χ [∧ ψ θ]])) )

; was pm3.48
(in-context !or
  (th "imt" (wff ![φ ψ χ θ])
    (meta (:description . "Theorem *3.48 of Principia Mathematica p. 114; pm3.48 in set.mm."))
    (ass [→ ∧ → φ ψ → χ θ → ∨ φ χ ∨ ψ θ])
    (proof
      [!an!sim< [→ φ ψ] [→ χ θ]]
      [ded con φ ψ [∧ → φ ψ → χ θ]]
      [!an!sim> [→ φ ψ] [→ χ θ]]
      [!im!imtd [∧ → φ ψ → χ θ] [¬ ψ] [¬ φ] χ θ]
      [df-or φ χ]
      [df-or ψ θ]
      [<imtrg [∧ → φ ψ → χ θ] [→ ¬ φ χ] [→ ¬ ψ θ] [∨ φ χ] [∨ ψ θ]])) )

(in-context !an
  (th "imtd" (wff ![φ ψ χ θ τ])
    (meta (:comment . "Conjoin antecedents and consequents in a deduction.")
          (:description . "anim12d in set.mm."))
    (hypo [→ φ → ψ χ] [→ φ → θ τ])
    (ass  [→ φ → ∧ ψ θ ∧ χ τ])
    (proof
      [imt ψ χ θ τ]
      [jca φ [→ ψ χ] [→ θ τ]]
      [syl φ [∧ → ψ χ → θ τ] [→ ∧ ψ θ ∧ χ τ]])) )

(in-context !an
  ; was im.bldanan>
  (th "2imt" (wff ![φ ψ χ θ τ η])
    (meta (:comment . "Deduction joining nested implications to form implication of conjunctions.")
          (:description . "im2anan9 in set.mm."))
    (hypo [→ φ → ψ χ] [→ θ → τ η])
    (ass [→ ∧ φ θ → ∧ ψ τ ∧ χ η])
    (proof
      [infer add> φ [→ ψ χ] θ]
      [infer add< θ [→ τ η] φ]
      [imtd [∧ φ θ] ψ χ τ η])) )

(in-context !or
  (th "imtd" (wff ![φ ψ χ θ τ])
    (meta (:comment . "Disjoin antecedents and consequents in a deduction.")
          (:description . "orim12d in set.mm."))
    (hypo [→ φ → ψ χ] [→ φ → θ τ])
    (ass [→ φ → ∨ ψ θ ∨ χ τ])
    (proof
      [imt ψ χ θ τ]
      [jca φ [→ ψ χ] [→ θ τ]]
      [syl φ [∧ → ψ χ → θ τ] [→ ∨ ψ θ ∨ χ τ]])) )

; TODO: Move as lemma for or.bidi
(in-context !lemmas
  (th "pm2.85" (wff ![φ ψ χ])
    (meta (:description . "Theorem *2.85 of Principia Mathematica p. 108."))
    (ass [→ → ∨ φ ψ ∨ φ χ ∨ φ → ψ χ])

    (th "pm2.46" ()
        (meta (:description . "Theorem *2.46 of Principia Mathematica p. 106."))
        (ass [→ ¬ ∨ φ ψ ¬ ψ])
        (proof
          [or> φ ψ]
          [infer con ψ [∨ φ ψ]]))
    
    (th "pm2.48" ()
        (meta (:description . "Theorem *2.48 of Principia Mathematica p. 106."))
        (ass [→ ¬ ∨ φ ψ ∨ φ ¬ ψ])
        (proof
          [pm2.46]
          [ded ax1 [¬ ψ] [¬ φ] [¬ ∨ φ ψ]]
          [ded df-or φ [¬ ψ] [¬ ∨ φ ψ]]))

    (proof
      [!im!df.or [∨ φ ψ] [∨ φ χ]]
      [pm2.48]
      [infer !or!imt< [¬ ∨ φ ψ] [∨ φ ¬ ψ] [∨ φ χ]]
      [b2i syl [→ ∨ φ ψ ∨ φ χ] [∨ ¬ ∨ φ ψ ∨ φ χ] [∨ ∨ φ ¬ ψ ∨ φ χ]]
      [!im!df.or [ψ] [χ]]
      [infer !or!eqt> [→ ψ χ] [∨ ¬ ψ χ] [φ]]
      [!or!di-or< [φ] [¬ ψ] [χ]]
      [bitr [∨ φ → ψ χ] [∨ φ ∨ ¬ ψ χ] [∨ ∨ φ ¬ ψ ∨ φ χ]]
      [b2i syl [→ ∨ φ ψ ∨ φ χ] [∨ ∨ φ ¬ ψ ∨ φ χ] [∨ φ → ψ χ]])) )

(th ">nor" (wff ![φ ψ])
    (meta (:comment . "Infer negated disjunction of negated premises.")
          (:description . "pm3.2ni in set.mm."))
    (hypo [¬ φ] [¬ ψ])
    (ass [¬ ∨ φ ψ])
    (proof
      [>andi [¬ φ] [¬ ψ]]
      [infer !or!neg φ ψ]))

(in-context !or
  (th "abs" (wff ![φ ψ])
    (meta (:comment . "Absorption law for disjunction.")
          (:description . "One of the axioms for lattices: part of axiom L4 in
[Birkhoff]; theorem *4.44 of Principia Mathematica p. 119."))
    (ass [↔ φ ∨ φ ∧ φ ψ])
    (proof
      [or< φ [∧ φ ψ]]
      [id φ]
      [!an!sim< φ ψ]
      [jaoi φ [∧ φ ψ] φ]
      [>bii φ ∨ φ ∧ φ ψ])) )

;(in-context !or
 ; (th "abs" (wff ("φ" "ψ"))
  ;  (ass [↔ φ ∧ ∨ φ ψ φ])
   ; (proof
    ;  [or< φ ψ]
     ; [(infer !an!join-r) φ [∨ φ ψ]]
      ;[!an!sim> [∨ φ ψ] φ]
;      [>bii φ [∧ ∨ φ ψ φ]])) )

;(in-context !an
 ; (th "abs" (wff ![φ ψ])
  ;  (ass [↔ φ ∨ ∧ φ ψ φ])
   ; (proof
    ;  [or> [∧ φ ψ] φ]
     ; [sim< φ ψ]
      ;[id φ]
;      [jaoi [∧ φ ψ] φ φ]
 ;     [>bii φ [∨ ∧ φ ψ φ]])))

(in-context !im
  (th "di-bi<" (wff ![φ ψ χ])
    (meta (:comment . "Distribution of implication over biconditional.")
          (:description . "Theorem *5.74 of Principia Mathematica p. 126; pm5.74 in set.mm."))
    (ass [↔ → φ ↔ ψ χ ↔ → φ ψ → φ χ])
    (proof
      [bi> ψ χ]
      [infer syl< [↔ ψ χ] [→ ψ χ] φ]
      [ded ax2 φ ψ χ [→ φ ↔ ψ χ]]
      [bi< ψ χ]
      [infer syl< [↔ ψ χ] [→ χ ψ] φ]
      [ded ax2 φ χ ψ [→ φ ↔ ψ χ]]
      [>bid [→ φ ↔ ψ χ] [→ φ ψ] [→ φ χ]]
      [bi> [→ φ ψ] [→ φ χ]]
      [ded ax2-converse φ ψ χ [↔ → φ ψ → φ χ]]
      [bi< [→ φ ψ] [→ φ χ]]
      [ded ax2-converse φ χ ψ [↔ → φ ψ → φ χ]]
      [!an!imtd [↔ → φ ψ → φ χ] [φ] [→ ψ χ] [φ] [→ χ ψ]]
      [!an!idm φ]
      [!bi!comi [∧ φ φ] φ]
      [dfbi ψ χ]
      [<trg [↔ → φ ψ → φ χ] [∧ φ φ] [∧ → ψ χ → χ ψ]
            [φ] [↔ ψ χ]]
      [>bii [→ φ ↔ ψ χ] [↔ → φ ψ → φ χ]])) )

(th "im.imbi" (wff ![φ ψ])
    (meta (:comment . "Implication in terms of implication and biconditional.")
          (:description . "ibib in set.mm."))
    (ass [↔ → φ ψ → φ ↔ φ ψ])
    (proof
      [an->im φ ψ]
      [!an!sim< φ ψ]
      [ded ax1 φ ψ [∧ φ ψ]]
      [>bid [∧ φ ψ] φ ψ]
      [exp φ ψ [↔ φ ψ]]
      [bi> φ ψ]
      [com12i [↔ φ ψ] φ ψ]
      [>bid φ ψ [↔ φ ψ]]
      [infer !im!di-bi< φ ψ [↔ φ ψ]]))

(th "im.imbi-r" (wff ![φ ψ])
    (meta (:comment . "Implication in terms of implication and biconditional.")
          (:description . "ibibr in set.mm."))
    (ass [↔ → φ ψ → φ ↔ ψ φ])
    (proof
      [im.imbi φ ψ]
      [!bi!com φ ψ]
      [infer !im!eqt> [↔ φ ψ] [↔ ψ φ] φ]
      [bitr [→ φ ψ] [→ φ ↔ φ ψ] [→ φ ↔ ψ φ]]))

(in-context !bi
  (th "true<" (wff ![φ ψ])
    (meta (:comment . "Simplification of equivalence when the left side is true.")
          (:description . "Theorem *5.501 of Principia Mathematica p. 125; pm5.501 in set.mm."))
    (ass [→ φ ↔ ψ ↔ φ ψ])
    (proof
      [im.imbi φ ψ]
      [infer !im!di-bi< φ ψ [↔ φ ψ]]))
  (th-pattern !true<)

  (th "true>" (wff ![φ ψ])
    (meta (:comment . "Simlification of equivalence when the right side is true."))
    (ass [→ ψ ↔ φ ↔ φ ψ])
    (proof
      (suppose ([ψ]) ([↔ φ ↔ φ ψ])
        [infer true< ψ φ]
        [com ψ φ]
        [bitr φ [↔ ψ φ] [↔ φ ψ]])))
  (th-pattern !true>))

(in-context !or
  (th "di-an<" (wff ![φ ψ χ])
    (meta (:comment . "Distributive law for disjunction.")
          (:description . "Theorem *4.41 of Principia Mathematica p. 119; ordi in set.mm."))
    (ass [↔ ∨ φ ∧ ψ χ ∧ ∨ φ ψ ∨ φ χ])
    (proof
      [!an!sim< ψ χ]
      [infer imt> [∧ ψ χ] ψ φ]
      [!an!sim> ψ χ]
      [infer imt> [∧ ψ χ] χ φ]
      [jca [∨ φ ∧ ψ χ] [∨ φ ψ] [∨ φ χ]]
      [>andc [¬ φ] ψ χ]
      [df-or φ χ]
      [df-or φ [∧ ψ χ]]
      [<imtrg [→ ¬ φ ψ] [→ ¬ φ χ] [→ ¬ φ ∧ ψ χ]
              [∨ φ χ] [∨ φ ∧ ψ χ]]
      [df-or φ ψ]
      [b2i syl [∨ φ ψ] [→ ¬ φ ψ] [→ ∨ φ χ ∨ φ ∧ ψ χ]]
      [imp [∨ φ ψ] [∨ φ χ] [∨ φ ∧ ψ χ]]
      [>bii [∨ φ ∧ ψ χ] [∧ ∨ φ ψ ∨ φ χ]]))

  (th "di-an>" (wff ![φ ψ χ])
    (meta (:comment . "Distributive law for disjunction.")
          (:description . "ordir in set.mm."))
    (ass [↔ ∨ ∧ φ ψ χ ∧ ∨ φ χ ∨ ψ χ])
    (proof
      [com [∧ φ ψ] χ]
      [di-an< χ φ ψ]
      [bitr [∨ ∧ φ ψ χ] [∨ χ ∧ φ ψ] [∧ ∨ χ φ ∨ χ ψ]]
      [com χ φ]
      [com χ ψ]
      [!an!eqti [∨ χ φ] [∨ φ χ] [∨ χ ψ] [∨ ψ χ]]
      [bitr [∨ ∧ φ ψ χ] [∧ ∨ χ φ ∨ χ ψ] [∧ ∨ φ χ ∨ ψ χ]])) )

(in-context !im
  (th "di-an<" (wff ![φ ψ χ])
    (meta (:comment . "Distributive law for implication over conjunction.")
          (:description . "Compare Theorem *4.76 of Principia Mathematica p. 121; jcab in set.mm."))
    (ass [↔ → φ ∧ ψ χ ∧ → φ ψ → φ χ])
    (proof
      [!or!di-an< [¬ φ] ψ χ]
      [df.or φ [∧ ψ χ]]
      [df.or φ ψ]
      [df.or φ χ]
      [!an!eqti [→ φ ψ] [∨ ¬ φ ψ] [→ φ χ] [∨ ¬ φ χ]]
      [<bitr [∨ ¬ φ ∧ ψ χ] [∧ ∨ ¬ φ ψ ∨ ¬ φ χ]
             [→ φ ∧ ψ χ]   [∧ → φ ψ → φ χ]])) )

(th "jcad" (wff ![φ ψ χ θ])
    (meta (:comment . "Deduction conjoining the consequents of two implications."))
    (hypo [→ φ → ψ χ] [→ φ → ψ θ])
    (ass [→ φ → ψ ∧ χ θ])
    (proof
      [imp φ ψ χ]
      [imp φ ψ θ]
      [jca [∧ φ ψ] χ θ]
      [exp φ ψ [∧ χ θ]]))

(in-context !an
  (th "di-or<" (wff ![φ ψ χ])
    (meta (:comment . "Distributive law for conjunction.")
          (:description . "Theorem *4.4 of Principia Mathematica p. 118; andi in set.mm."))
    (ass [↔ ∧ φ ∨ ψ χ ∨ ∧ φ ψ ∧ φ χ])
    (proof
      [!or!di-an< [¬ φ] [¬ ψ] [¬ χ]]
      [!or!neg ψ χ]
      [infer !or!eqt> [¬ ∨ ψ χ] [∧ ¬ ψ ¬ χ] [¬ φ]]
      [neg φ ψ]
      [neg φ χ]
      [eqti [¬ ∧ φ ψ] [∨ ¬ φ ¬ ψ] [¬ ∧ φ χ] [∨ ¬ φ ¬ χ]]
      [<bitr [∨ ¬ φ ∧ ¬ ψ ¬ χ] [∧ ∨ ¬ φ ¬ ψ ∨ ¬ φ ¬ χ]
             [∨ ¬ φ ¬ ∨ ψ χ] [∧ ¬ ∧ φ ψ ¬ ∧ φ χ]]
      [infer neqt [∨ ¬ φ ¬ ∨ ψ χ] [∧ ¬ ∧ φ ψ ¬ ∧ φ χ]]
      [df.or φ [∨ ψ χ]]
      [!or!df.an [∧ φ ψ] [∧ φ χ]]
      [<bitr [¬ ∨ ¬ φ ¬ ∨ ψ χ] [¬ ∧ ¬ ∧ φ ψ ¬ ∧ φ χ]
             [∧ φ ∨ ψ χ] [∨ ∧ φ ψ ∧ φ χ]]))

  (th "di-or>" (wff ![φ ψ χ])
    (meta (:comment . "Distributive law for conjunction.")
          (:description . "andir in set.mm."))
    (ass [↔ ∧ ∨ φ ψ χ ∨ ∧ φ χ ∧ ψ χ])
    (proof
      [com [∨ φ ψ] χ]
      [di-or< χ φ ψ]
      [bitr [∧ ∨ φ ψ χ] [∧ χ ∨ φ ψ] [∨ ∧ χ φ ∧ χ ψ]]
      [com χ φ]
      [com χ ψ]
      [!or!eqti [∧ χ φ] [∧ φ χ] [∧ χ ψ] [∧ ψ χ]]
      [bitr [∧ ∨ φ ψ χ] [∨ ∧ χ φ ∧ χ ψ] [∨ ∧ φ χ ∧ ψ χ]])) )

(in-context !or
  (th "di-2an" (wff ![φ ψ χ θ])
    (meta (:comment . "Double distributive law for disjunction.")
          (:description . "orddi in set.mm."))
    (ass [↔ ∨ ∧ φ ψ ∧ χ θ ∧ ∧ ∨ φ χ ∨ φ θ ∧ ∨ ψ χ ∨ ψ θ])
    (proof
      [di-an> φ ψ [∧ χ θ]]
      [di-an< φ χ θ]
      [di-an< ψ χ θ]
      [!an!eqti [∨ φ ∧ χ θ] [∧ ∨ φ χ ∨ φ θ]
                [∨ ψ ∧ χ θ] [∧ ∨ ψ χ ∨ ψ θ]]
      [bitr [∨ ∧ φ ψ ∧ χ θ] [∧ ∨ φ ∧ χ θ ∨ ψ ∧ χ θ]
            [∧ ∧ ∨ φ χ ∨ φ θ ∧ ∨ ψ χ ∨ ψ θ]])) )

(in-context !an
  (th "di-2or" (wff ![φ ψ χ θ])
    (meta (:comment . "Double distributive law for conjunction.")
          (:description . "anddi in set.mm."))
    (ass [↔ ∧ ∨ φ ψ ∨ χ θ ∨ ∨ ∧ φ χ ∧ φ θ ∨ ∧ ψ χ ∧ ψ θ])
    (proof
      [di-or> φ ψ [∨ χ θ]]
      [di-or< φ χ θ]
      [di-or< ψ χ θ]
      [!or!eqti [∧ φ ∨ χ θ] [∨ ∧ φ χ ∧ φ θ]
                [∧ ψ ∨ χ θ] [∨ ∧ ψ χ ∧ ψ θ]]
      [bitr [∧ ∨ φ ψ ∨ χ θ] [∨ ∧ φ ∨ χ θ ∧ ψ ∨ χ θ]
            [∨ ∨ ∧ φ χ ∧ φ θ ∨ ∧ ψ χ ∧ ψ θ]])) )

(in-context !bi
  (th "eqt>" (wff ![φ ψ χ])
    (meta (:comment . "Theorem adding a biconditional to the left in an equivalence.")
          (:description . "Closed form of bibi2i in set.mm."))
    (ass [→ ↔ φ ψ ↔ ↔ χ φ ↔ χ ψ])
    (proof
    (suppose ([↔ φ ψ]) ([↔ ↔ χ φ ↔ χ ψ])
      [df.an χ φ]
      [infer !im!eqt< φ ψ χ]
      [infer !an!eqt> [→ φ χ] [→ ψ χ] [→ χ φ]]
      [bitr [↔ χ φ] [∧ → χ φ → φ χ] [∧ → χ φ → ψ χ]]
      [infer !im!eqt> φ ψ χ]
      [infer !an!eqt< [→ χ φ] [→ χ ψ] [→ ψ χ]]
      [bitr [↔ χ φ] [∧ → χ φ → ψ χ] [∧ → χ ψ → ψ χ]]
      [df.an χ ψ]
      [b2i bitr [↔ χ φ] [∧ → χ ψ → ψ χ] [↔ χ ψ]])))

  (th "eqt<" (wff ![φ ψ χ])
    (meta (:comment . "Theorem adding a biconditional to the right in an equivalence.")
          (:description . "Theorem *4.86 of Principia Mathematica p. 122; bibi1 in set.mm."))
    (ass [→ ↔ φ ψ ↔ ↔ φ χ ↔ ψ χ])
    (proof
      (suppose ([↔ φ ψ]) ([↔ ↔ φ χ ↔ ψ χ])
        [com φ χ]
        [infer eqt> φ ψ χ]
        [com χ ψ]
        [2tri [↔ φ χ] [↔ χ φ] [↔ χ ψ] [↔ ψ χ]])))

  (th "eqti" (wff ![φ ψ χ θ])
    (meta (:comment . "The equivalence of two equivalences.")
          (:description . "bibi12i in set.mm."))
    (hypo [↔ φ ψ] [↔ χ θ])
    (ass [↔ ↔ φ χ ↔ ψ θ])
    (proof
      [infer eqt< φ ψ χ]
      [infer eqt> χ θ ψ]
      [bitr [↔ φ χ] [↔ ψ χ] [↔ ψ θ]]))

  (th "tr:bi" (wff ![φ ψ χ])
    (meta (:comment . "Transitive law for biconditional.")
          (:description . "Theorem *4.22 of Principia Mathematica p. 117; pm4.22 in set.mm."))
    (ass [→ ∧ ↔ φ ψ ↔ ψ χ ↔ φ χ])
    (proof
      (suppose ([↔ φ ψ]) ([→ ↔ ψ χ ↔ φ χ])
        [infer !bi!eqt< φ ψ χ]
        [infer bi< [↔ φ χ] [↔ ψ χ]])
      [imp [↔ φ ψ] [↔ ψ χ] [↔ φ χ]]))
  (th-pattern !tr:bi))

(in-context !im
  (th "eqtd" (wff ![φ ψ χ θ τ])
    (meta (:comment . "Deduction joining two equivalences to form equivalence of implications.")
          (:description . "imbi12d in set.mm."))
    (hypo [→ φ ↔ ψ χ] [→ φ ↔ θ τ])
    (ass [→ φ ↔ → ψ θ → χ τ])
    (proof
      [ded eqt< ψ χ θ φ]
      [ded eqt> θ τ χ φ]
      [!bi!trd φ [→ ψ θ] [→ χ θ] [→ χ τ]])))

(in-context !or
  (th "eqtd" (wff ![φ ψ χ θ τ])
    (meta (:comment . "Deduction joining two equivalences to form equivalence of disjunctions.")
          (:description . "orbi12d in set.mm."))
    (hypo [→ φ ↔ ψ χ] [→ φ ↔ θ τ])
    (ass [→ φ ↔ ∨ ψ θ ∨ χ τ])
    (proof
      [ded eqt< ψ χ θ φ]
      [ded eqt> θ τ χ φ]
      [!bi!trd φ [∨ ψ θ] [∨ χ θ] [∨ χ τ]])))

(in-context !an
  (th "eqtd" (wff ![φ ψ χ θ τ])
    (meta (:comment . "Deduction joining two equivalences to form equivalence of conjunctions.")
          (:description  . "anbi12d in set.mm."))
    (hypo [→ φ ↔ ψ χ] [→ φ ↔ θ τ])
    (ass [→ φ ↔ ∧ ψ θ ∧ χ τ])
    (proof
      [ded eqt< ψ χ θ φ]
      [ded eqt> θ τ χ φ]
      [!bi!trd φ [∧ ψ θ] [∧ χ θ] [∧ χ τ]])))

(in-context !bi
  (th "eqtd" (wff ![φ ψ χ θ τ])
    (meta (:comment . "Deduction joining two equivalences to form equivalence of biconditionals.")
          (:description . "bibi12d in set.mm."))
    (hypo [→ φ ↔ ψ χ] [→ φ ↔ θ τ])
    (ass [→ φ ↔ ↔ ψ θ ↔ χ τ])
    (proof
      [ded eqt< ψ χ θ φ]
      [ded eqt> θ τ χ φ]
      [trd φ [↔ ψ θ] [↔ χ θ] [↔ χ τ]])))

(in-context !or
  (th "eqt" (wff ![φ ψ χ θ])
    (meta (:comment . "Theorem joining two equivalences to form equivalence of disjunctions.")
          (:description . "Theorem *4.39 of Principia Mathematica p. 118; pm4.39 in set.mm."))
    (ass [→ ∧ ↔ φ ψ ↔ χ θ ↔ ∨ φ χ ∨ ψ θ])
    (proof
      (suppose ([∧ ↔ φ ψ ↔ χ θ]) ([↔ ∨ φ χ ∨ ψ θ])
        [siman [↔ φ ψ] [↔ χ θ]]
        [eqti φ ψ χ θ]))))

(in-context !an
  (th "eqt" (wff ![φ ψ χ θ])
    (meta (:comment . "Theorem joining two equivalences to form equivalence of conjunctions.")
          (:description . "Theorem *4.38 of Principia Mathematica p. 118; pm4.38 in set.mm."))
    (ass [→ ∧ ↔ φ ψ ↔ χ θ ↔ ∧ φ χ ∧ ψ θ])
    (proof
      (suppose ([∧ ↔ φ ψ ↔ χ θ]) ([↔ ∧ φ χ ∧ ψ θ])
        [siman [↔ φ ψ] [↔ χ θ]]
        [eqti φ ψ χ θ]))))

(in-context !an
  (th "2eqt" (wff ![φ ψ χ θ τ η])
    (meta (:comment . "Deduction joining two equivalences to form equivalence of conjunctions.")
          (:description . "bi2anan9 in set.mm."))
    (hypo [→ φ ↔ ψ χ] [→ θ ↔ τ η])
    (ass [→ ∧ φ θ ↔ ∧ ψ τ ∧ χ η])
    (proof
      [ded eqt< ψ χ τ φ]
      [ded eqt> τ η χ θ]
      [bsyl9 φ [∧ ψ τ] [∧ χ τ] θ [∧ χ η]])))

(in-context !im
  ; was dfim.an
  (th "df.cap" (wff ![φ ψ])
    (meta (:comment . "Implication in terms of biconditional and conjunction.")
          (:description . "Implication is the partial order in the lattice of wffs.
Theorem *4.71 of Principia Mathematica p. 120; pm4.71 in set.mm."))
    (ass [↔ → φ ψ ↔ φ ∧ φ ψ])
    (proof
      [!an!join φ ψ]
      [!an!sim< φ ψ]
      [infer ax1 [→ ∧ φ ψ φ] [→ φ ψ]]
      [>bid [→ φ ψ] φ [∧ φ ψ]]
      [bi> φ [∧ φ ψ]]
      [!an!sim> φ ψ]
      [!im!rp33 [↔ φ ∧ φ ψ] φ [∧ φ ψ] ψ]
      [>bii [→ φ ψ] [↔ φ ∧ φ ψ]]))
  (th-pattern !df.cap)

  (th "df.cap-r" (wff ![φ ψ])
    (meta (:comment . "Implication in terms of biconditional and conjunction.")
          (:description . "Theorem *4.71 of Principia Mathematica p. 120 (with conjunct reversed); pm4.71r in set.mm."))
    (ass [↔ → φ ψ ↔ φ ∧ ψ φ])
    (proof
      [df.cap φ ψ]
      [!an!com φ ψ]
      [infer !bi!eqt> [∧ φ ψ] [∧ ψ φ] φ]
      [bitr [→ φ ψ] [↔ φ ∧ φ ψ] [↔ φ ∧ ψ φ]]))

  ; was dfim.or
  (th "df.cup" (wff ![φ ψ])
    (meta (:comment . "Implication in terms of biconditional and disjunction.")
          (:description . "Implication is the partial order in the lattice of wffs.
Theorem *4.72 of Principia Mathematica p. 121; pm4.72 in set.mm."))
    (ass [↔ → φ ψ ↔ ψ ∨ φ ψ])

    ; lemma
    (th "pm2.62" ()
        (meta (:description . "Theorem *2.62 of Principia Mathematica p. 107."))
        (ass [→ → φ ψ → ∨ φ ψ ψ])
        (proof
          [!or!df2 φ ψ]
          [b2i com12i [∨ φ ψ] [→ φ ψ] ψ]))

    (th "pm2.67" ()
        (meta (:description . "Theorem *2.67 of Principia Mathematica p. 107."))
        (ass [→ → ∨ φ ψ ψ → φ ψ])
        (proof
          [or< φ ψ]
          [infer syl> φ [∨ φ ψ] ψ]))
    
    (proof
      [or> φ ψ]
      [infer ax1 [→ ψ ∨ φ ψ] [→ φ ψ]]
      [pm2.62]
      [>bid [→ φ ψ] [ψ] [∨ φ ψ]]
      [bi< [ψ] [∨ φ ψ]]
      [pm2.67]
      [syl [↔ ψ ∨ φ ψ] [→ ∨ φ ψ ψ] [→ φ ψ]]
      [>bii [→ φ ψ] [↔ ψ ∨ φ ψ]])) 
  (th-pattern !df.cup))

(in-context !an
  (th "abs" (wff ![φ ψ])
    (meta (:comment . "Absorption law for conjunction.")
          (:description . "One of the axioms for lattices: part of axiom L4 in
[Birkhoff]; theorem *4.45 of Principia Mathematica p. 119; pm4.45 in set.mm."))
    (ass [↔ φ ∧ φ ∨ φ ψ])
    (proof
      [or< φ ψ]
      [infer !im!df.cap φ [∨ φ ψ]]))

  (th "true>" (wff ![ψ φ])
    (meta (:comment . "Simplification of conjunction when the right conjunct is true.")
          (:description . "Theorem *4.73 of Principia Mathematica p. 121; iba in set.mm."))
    (ass [→ φ ↔ ψ ∧ ψ φ])
    (proof
      [bjoin-r φ ψ]
      [infer !im!di-bi< φ ψ [∧ ψ φ]]))
  (th-pattern !true>)

  (th "true<" (wff ![φ ψ])
    (meta (:comment . "Simplification of conjunction when the left conjunct is true.")
          (:description . "ibar in set.mm."))
    (ass [→ φ ↔ ψ ∧ φ ψ])
    (proof
      [bjoin φ ψ]
      [infer !im!di-bi< φ ψ [∧ φ ψ]])) 
  (th-pattern !true<))

(th "im.bidian" (wff ![φ ψ χ])
    (meta (:comment . "Distribution of implication over biconditional.")
          (:description . "Theorem *5.32 of Principia Mathematica p. 125; pm5.32 in set.mm."))
    (ass [↔ → φ ↔ ψ χ ↔ ∧ φ ψ ∧ φ χ])
    (proof
      [conb ψ χ]
      [infer !im!eqt> [↔ ψ χ] [↔ ¬ ψ ¬ χ] φ]
      [!im!di-bi< φ [¬ ψ] [¬ χ]]
      [conb [→ φ ¬ ψ] [→ φ ¬ χ]]
      [2bitr [→ φ ↔ ψ χ] [→ φ ↔ ¬ ψ ¬ χ]
             [↔ → φ ¬ ψ → φ ¬ χ] [↔ ¬ → φ ¬ ψ ¬ → φ ¬ χ]]
      [df-an φ ψ]
      [df-an φ χ]
      [!bi!eqti [∧ φ ψ] [¬ → φ ¬ ψ] [∧ φ χ] [¬ → φ ¬ χ]]
      [b2i bitr [→ φ ↔ ψ χ] [↔ ¬ → φ ¬ ψ ¬ → φ ¬ χ] [↔ ∧ φ ψ ∧ φ χ]]))

(th "im.bidian-r" (wff ![φ ψ χ])
    (meta (:comment . "Distribution of implication over biconditional."))
    (ass [↔ → φ ↔ ψ χ ↔ ∧ ψ φ ∧ χ φ])
    (proof
      [im.bidian φ ψ χ]
      [!an!com φ ψ]
      [!an!com φ χ]
      [!bi!eqti [∧ φ ψ] [∧ ψ φ] [∧ φ χ] [∧ χ φ]]
      [bitr [→ φ ↔ ψ χ] [↔ ∧ φ ψ ∧ φ χ] [↔ ∧ ψ φ ∧ χ φ]]))

; use infer> im.bidian-r
;(th "im.bidian-ri" (wff ("φ" "ψ" "χ"))
;    (hypo [→ φ ↔ ψ χ])
;    (ass [↔ ∧ ψ φ ∧ χ φ])
;    (proof
;      [(infer> !im.bidian) [φ] [ψ] [χ]]
;      [!an!com [ψ] [φ]]
;      [!an!com [χ] [φ]]
;      [<bitr [∧ φ ψ] [∧ φ χ] [∧ ψ φ] [∧ χ φ]]))

(th "exmid" (wff "φ")
    (meta (:comment . "Law of excluded middle.")
          (:description . "Theorem *2.11 of Principia Mathematica p. 101. It says that
something is either true or not true; there are no in-between values of truth. This is
an essential distinction of our classical logic and is not a theorem of intuitionistic
logic."))
    (ass [∨ φ ¬ φ])
    (proof
      [id [¬ φ]]
      [infer df-or φ [¬ φ]]))

(th "nexmid" (wff "φ")
    (meta (:comment . "Law of contradiction.")
          (:description . "Theorem *3.24 of Principia Mathematica p. 111; pm3.24 in set.mm."))
    (ass [¬ ∧ φ ¬ φ])
    (proof
      [exmid [¬ φ]]
      [infer !an!neg φ [¬ φ]]))

(in-context !lemmas
  (th "pm2.5" (wff ![ψ φ])
    (meta (:description . "Theorem *2.5 of Principia Mathematica p. 107; pm2.5 in set.mm."))
    (ass [→ ¬ → ψ φ → ¬ ψ φ])
    (proof
      [<neg ψ φ]
      [infer con [¬ ψ] [→ ψ φ]]
      [ded <neg [¬ ψ] φ [¬ → ψ φ]]))

  (th "pm2.51" (wff ![φ ψ])
    (meta (:description . "Theorem *2.51 of Principia Mathematica p. 107; pm2.51 in set.mm."))
    (ass [→ ¬ → φ ψ → φ ¬ ψ])
    (proof
      [ax1 ψ φ]
      [infer con ψ [→ φ ψ]]
      [ded ax1 [¬ ψ] φ [¬ → φ ψ]])) )

(in-context !bi
  (th "df.xor" (wff ![φ ψ])
    (meta (:comment . "Equivalence in terms of exclusive or.")
          (:description . "Theorem *5.18 of Principia Mathematica p. 124; pm5.18 in set.mm."))
    (ass [↔ ↔ φ ψ ¬ ↔ φ ¬ ψ])

    (proof
      [ncase ψ φ]
      [con> φ ψ]
      [!im!rp32 [→ ψ φ] [→ ψ ¬ φ] [¬ ψ] [→ φ ¬ ψ]]
      [case ψ φ]
      [!an!imtd [→ ψ φ] [→ ¬ ψ φ] φ [→ φ ¬ ψ] [¬ ψ]]
      [df.an [¬ ψ] φ]
      [infer bi> [↔ ¬ ψ φ] [∧ → ¬ ψ φ → φ ¬ ψ]]
      [!im!rp32 [→ ψ φ] [∧ → ¬ ψ φ → φ ¬ ψ] [∧ φ ¬ ψ] [↔ ¬ ψ φ]]
      [!an!df.nim φ ψ]
      [b2i !im!rp33 [→ ψ φ] [↔ ¬ ψ φ] [∧ φ ¬ ψ] [¬ → φ ψ]]
      [com12i [→ ψ φ] [↔ ¬ ψ φ] [¬ → φ ψ]]
      [!im!df.nan [→ ψ φ] [→ φ ψ]]
      [b2i syl [↔ ¬ ψ φ] [→ → ψ φ ¬ → φ ψ] [¬ ∧ → ψ φ → φ ψ]]
      [dfbi ψ φ]
      [infer neqt [↔ ψ φ] [∧ → ψ φ → φ ψ]]
      [infer bi> [¬ ↔ ψ φ] [¬ ∧ → ψ φ → φ ψ]]
      [infer bi< [¬ ↔ ψ φ] [¬ ∧ → ψ φ → φ ψ]]
      [syl [↔ ¬ ψ φ] [¬ ∧ → ψ φ → φ ψ] [¬ ↔ ψ φ]]
      [!lemmas!pm2.5 ψ φ]
      [!an!df.nim ψ φ]
      [<neg φ [¬ ψ]]
      [infer !an!add< [¬ φ] [→ φ ¬ ψ] [ψ]]
      [b2i syl [¬ → ψ φ] [∧ ψ ¬ φ] [→ φ ¬ ψ]]
      [jca [¬ → ψ φ] [→ ¬ ψ φ] [→ φ ¬ ψ]]
      [ax1 φ [¬ ψ]]
      [infer !an!add> φ [→ ¬ ψ φ] [¬ ψ]]
      [!an!df.nim φ ψ]
      [b2i syl [¬ → φ ψ] [∧ φ ¬ ψ] [→ ¬ ψ φ]]
      [!lemmas!pm2.51 φ ψ]
      [jca [¬ → φ ψ] [→ ¬ ψ φ] [→ φ ¬ ψ]]
      [jaoi [¬ → ψ φ] [¬ → φ ψ] [∧ → ¬ ψ φ → φ ¬ ψ]]
      [!an!neg [→ ψ φ] [→ φ ψ]]
      [<imtr [∨ ¬ → ψ φ ¬ → φ ψ] [∧ → ¬ ψ φ → φ ¬ ψ]
             [¬ ∧ → ψ φ → φ ψ]   [↔ ¬ ψ φ]]
      [syl [¬ ↔ ψ φ] [¬ ∧ → ψ φ → φ ψ] [↔ ¬ ψ φ]]
      [>bii [↔ ¬ ψ φ] [¬ ↔ ψ φ]]
      [!bi!com φ [¬ ψ]]
      [bitr [↔ φ ¬ ψ] [↔ ¬ ψ φ] [¬ ↔ ψ φ]]
      [conb>i [↔ φ ¬ ψ] [↔ ψ φ]]
      [!bi!com φ ψ]
      [bitr [↔ φ ψ] [↔ ψ φ] [¬ ↔ φ ¬ ψ]])) 

  (th "neg<" (wff ![φ ψ])
    (meta (:comment . "Move negation outside of biconditional.")
          (:description . "nbbn in set.mm. Compare Theorem *5.18 of Principia Mathematica p. 124."))
    (ass [↔ ¬ ↔ φ ψ ↔ ¬ φ ψ])
    (proof
      [com [¬ φ] ψ]
      [com φ ψ]
      [df.xor ψ φ]
      [bitr [↔ φ ψ] [↔ ψ φ] [¬ ↔ ψ ¬ φ]]
      [conb>i [↔ φ ψ] [↔ ψ ¬ φ]]
      [bitr [↔ ¬ φ ψ] [↔ ψ ¬ φ] [¬ ↔ φ ψ]]
      [comi [↔ ¬ φ ψ] [¬ ↔ φ ψ]]))
  (th-pattern !neg<)

  (th "exmid" (wff ![φ ψ])
    (meta (:comment . "Law of excluded middle.")
          (:description . "Theorem *5.15 of Principia Mathematica p. 124; pm5.15 in set.mm."))
    (ass [∨ ↔ φ ψ ↔ φ ¬ ψ])
    (proof
      [df.xor φ ψ]
      [infer bi< [↔ φ ψ] [¬ ↔ φ ¬ ψ]]
      [infer con< [↔ φ ¬ ψ] [↔ φ ψ]]
      [infer df-or [↔ φ ψ] [↔ φ ¬ ψ]]))

  (th "nexmid" (wff ![φ ψ])
      (meta (:comment . "Law of contradiction.")
            (:description . "Theorem *5.16 of Principia Mathematica p. 124."))
      (ass [¬ ∧ ↔ φ ψ ↔ φ ¬ ψ])
      (proof
        [df.xor φ ψ]
        [→ ↔ φ ψ ¬ ↔ φ ¬ ψ]
        [infer !im!df.or [↔ φ ψ] [¬ ↔ φ ¬ ψ]]
        [infer !an!neg [↔ φ ψ] [↔ φ ¬ ψ]])))

(in-context !xor
  (th "refl" (wff "φ")
    (meta (:comment . "Exclusive or is irreflexive.")
          (:description . "Theorem *5.19 of Principia Mathematica p. 124; pm5.19 in set.mm."))
    (ass [¬ ↔ φ ¬ φ])
    (proof
      [!bi!refl φ]
      [infer !bi!df.xor φ φ])) )

(in-context !bi
  (th "df.or" (wff ![φ ψ])
    (meta (:comment . "An alternate definition of the biconditional.")
          (:description . "Theorem *5.23 of Principia Mathematica p. 124; dfbi in set.mm."))
    (ass [↔ ↔ φ ψ ∨ ∧ φ ψ ∧ ¬ φ ¬ ψ])
    (proof
      [df.xor φ ψ]
      [!im!df.nan φ ψ]
      [bcon< ψ φ]
      [!im!df.an [¬ φ] ψ]
      [bitr [→ ¬ ψ φ] [→ ¬ φ ψ] [¬ ∧ ¬ φ ¬ ψ]]
      [!an!eqti [→ φ ¬ ψ] [¬ ∧ φ ψ] [→ ¬ ψ φ] [¬ ∧ ¬ φ ¬ ψ]]
      [df.an φ [¬ ψ]]
      [!or!neg [∧ φ ψ] [∧ ¬ φ ¬ ψ]]
      [<bitr [∧ → φ ¬ ψ → ¬ ψ φ] [∧ ¬ ∧ φ ψ ¬ ∧ ¬ φ ¬ ψ]
             [↔ φ ¬ ψ] [¬ ∨ ∧ φ ψ ∧ ¬ φ ¬ ψ]]
      [comi [↔ φ ¬ ψ] [¬ ∨ ∧ φ ψ ∧ ¬ φ ¬ ψ]]
      [conb<i [∨ ∧ φ ψ ∧ ¬ φ ¬ ψ] [↔ φ ¬ ψ]]
      [bitr [↔ φ ψ] [¬ ↔ φ ¬ ψ] [∨ ∧ φ ψ ∧ ¬ φ ¬ ψ]])) )

(in-context !xor
  (th "df.or" (wff ![φ ψ])
    (meta (:comment . "Two ways to express \"exclusive or\".")
          (:description . "Theorem *5.22 of Principia Mathematica p. 124; xor in set.mm."))
    (ass [↔ ¬ ↔ φ ψ ∨ ∧ φ ¬ ψ ∧ ψ ¬ φ])
    (proof
      [!bi!df.or [¬ φ] ψ]
      [!bi!neg< φ ψ]
      [!bi!comi [¬ ↔ φ ψ] [↔ ¬ φ ψ]]
      [!an!com ψ [¬ φ]]
      [bneg> φ]
      [infer !an!eqt< φ [¬ ¬ φ] [¬ ψ]]
      [!or!eqti [∧ ψ ¬ φ] [∧ ¬ φ ψ] [∧ φ ¬ ψ] [∧ ¬ ¬ φ ¬ ψ]]
      [!or!com [∧ ψ ¬ φ] [∧ φ ¬ ψ]]
      [b2i bitr [∨ ∧ ¬ φ ψ ∧ ¬ ¬ φ ¬ ψ] [∨ ∧ ψ ¬ φ ∧ φ ¬ ψ] [∨ ∧ φ ¬ ψ ∧ ψ ¬ φ]]
      [>bitr [↔ ¬ φ ψ] [∨ ∧ ¬ φ ψ ∧ ¬ ¬ φ ¬ ψ]
             [¬ ↔ φ ψ] [∨ ∧ φ ¬ ψ ∧ ψ ¬ φ]]))

  (th "df.an" (wff ![φ ψ])
    (meta (:comment . "Two ways to express \"exclusive or\".")
          (:description . "xor2 in set.mm."))
    (ass [↔ ¬ ↔ φ ψ ∧ ∨ φ ψ ¬ ∧ φ ψ])

    ;lemma
    (th "pm5.24" ()
        (meta (:description . "Theorem *5.24 of Principia Mathematica p. 124."))
        (ass [↔ ¬ ∨ ∧ φ ψ ∧ ¬ φ ¬ ψ ∨ ∧ φ ¬ ψ ∧ ψ ¬ φ])
        (proof
          [!bi!df.or φ ψ]
          [infer neqt [↔ φ ψ] [∨ ∧ φ ψ ∧ ¬ φ ¬ ψ]]
          [df.or φ ψ]
          [b2i bitr [¬ ∨ ∧ φ ψ ∧ ¬ φ ¬ ψ] [¬ ↔ φ ψ] [∨ ∧ φ ¬ ψ ∧ ψ ¬ φ]]))

    (proof
      [!df.or φ ψ]
      [!or!neg [∧ φ ψ] [∧ ¬ φ ¬ ψ]]
      [pm5.24]
      [!or!df.an φ ψ]
      [infer !an!eqt> [∨ φ ψ] [¬ ∧ ¬ φ ¬ ψ] [¬ ∧ φ ψ]]
      [!an!com [¬ ∧ φ ψ] [∨ φ ψ]]
      [b2i bitr [∧ ¬ ∧ φ ψ ¬ ∧ ¬ φ ¬ ψ] [∧ ¬ ∧ φ ψ ∨ φ ψ] [∧ ∨ φ ψ ¬ ∧ φ ψ]]
      [>bitr [¬ ∨ ∧ φ ψ ∧ ¬ φ ¬ ψ] [∧ ¬ ∧ φ ψ ¬ ∧ ¬ φ ¬ ψ]
             [∨ ∧ φ ¬ ψ ∧ ψ ¬ φ] [∧ ∨ φ ψ ¬ ∧ φ ψ]]
      [bitr [¬ ↔ φ ψ] [∨ ∧ φ ¬ ψ ∧ ψ ¬ φ] [∧ ∨ φ ψ ¬ ∧ φ ψ]])) )

(in-context !bi
  (th "neg>" (wff ![φ ψ])
    (meta (:comment . "Move negation outside of biconditional.")
          (:description . "xor3 in set.mm."))
    (ass [↔ ¬ ↔ φ ψ ↔ φ ¬ ψ])
    (proof
      [df.xor φ ψ]
      [infer conb> [↔ φ ψ] [↔ φ ¬ ψ]]
      [comi [↔ φ ¬ ψ] [¬ ↔ φ ψ]])))

(th "true-eq" (wff ![φ ψ])
    (meta (:comment . "Two propositions are equivalent if they are both true.")
          (:description . "Theorem *5.1 of Principia Mathematica p. 123; pm5.1 in set.mm."))
    (ass [→ ∧ φ ψ ↔ φ ψ])
    (proof
      [!an!sim> φ ψ]
      [ded ax1 ψ φ [∧ φ ψ]]
      [!an!sim< φ ψ]
      [ded ax1 φ ψ [∧ φ ψ]]
      [>bid [∧ φ ψ] φ ψ]))

(th "false-eq" (wff ![φ ψ])
    (meta (:comment . "Two propositions are equivalent if they are both false.")
          (:description . "Theorem *5.21 of Principia Mathematica p. 124; pm5.21 in set.mm."))
    (ass [→ ∧ ¬ φ ¬ ψ ↔ φ ψ])
    (proof
      [true-eq [¬ φ] [¬ ψ]]
      [ded conb φ ψ [∧ ¬ φ ¬ ψ]]))

(in-context !bi
  (th "false<" (wff ![φ ψ])
    (meta (:comment . "Simplification of equivalence when the left side is false."))
    (ass [→ ¬ φ ↔ ¬ ψ ↔ φ ψ])
    (proof
      [false-eq φ ψ]
      [exp [¬ φ] [¬ ψ] [↔ φ ψ]]
      [neqt φ ψ]
      [ded bi> [¬ φ] [¬ ψ] [↔ φ ψ]]
      [com12i [↔ φ ψ] [¬ φ] [¬ ψ]]
      [>bid [¬ φ] [¬ ψ] [↔ φ ψ]]))
  (th-pattern !false<)

  (th "false>" (wff ![φ ψ])
    (meta (:comment . "Simplification of equivalence when the right side is false."))
    (ass [→ ¬ ψ ↔ ¬ φ ↔ φ ψ])
    (proof
      (suppose ([¬ ψ]) ([↔ ¬ φ ↔ φ ψ])
        [infer false< ψ φ]
        [com ψ φ]
        [bitr [¬ φ] [↔ ψ φ] [↔ φ ψ]])))
  (th-pattern !false>))

(th "false-eqi" (wff ![φ ψ χ])
    (meta (:comment . "Two propositions implying a false one are equivalent.")
          (:description . "pm5.21ni in set.mm."))
    (hypo [→ φ χ] [→ ψ χ])
    (ass [→ ¬ χ ↔ φ ψ])
    (proof
      [infer con φ χ]
      [infer con ψ χ]
      [jca [¬ χ] [¬ φ] [¬ ψ]]
      [false-eq φ ψ]
      [syl [¬ χ] [∧ ¬ φ ¬ ψ] [↔ φ ψ]]))

(th "false-eqii" (wff ![φ ψ χ])
    (meta (:comment . "Eliminate an antecedent implied by each side of a biconditional.")
          (:description . "pm5.21nii in set.mm."))
    (hypo [→ φ χ] [→ ψ χ] [→ χ ↔ φ ψ])
    (ass [↔ φ ψ])
    (proof
      [false-eqi φ ψ χ]
      [casei χ [↔ φ ψ]]))

(in-context !an
  ; was baib>
  (th "true<i" (wff ![φ ψ χ])
    (meta (:comment . "Move conjunction outside of biconditional.")
          (:description . "baib in set.mm."))
    (hypo [↔ φ ∧ ψ χ])
    (ass [→ ψ ↔ φ χ])
    (proof
      [infer ax1 [↔ φ ∧ ψ χ] ψ]
      (suppose ([ψ]) ([↔ φ χ])
        [infer true< ψ χ]
        [b2i bitr φ [∧ ψ χ] χ])))

  (th "true>i" (wff ![φ ψ χ])
    (meta (:comment . "Move conjunction outside of biconditional."))
    (hypo [↔ φ ∧ ψ χ])
    (ass [→ χ ↔ φ ψ])
    (proof
      [com ψ χ]
      [bitr φ [∧ ψ χ] [∧ χ ψ]]
      [true<i φ χ ψ]))

  ; was baib<
  (th "true<i-r" (wff ![φ ψ χ])
    (meta (:comment . "Move conjunction outside of biconditional.")
          (:description . "baibr in set.mm."))
    (hypo [↔ φ ∧ ψ χ])
    (ass [→ ψ ↔ χ φ])
    (proof
      [true<i φ ψ χ]
      [ded !bi!com φ χ ψ])) )

(in-context !or
  (th "false<" (wff ![φ ψ])
    (meta (:comment . "Simplification of disjunction when the left disjunct is false.")
          (:description . "Theorem *4.74 of Principia Mathematica p. 121; biorf in set.mm."))
    (ass [→ ¬ φ ↔ ψ ∨ φ ψ])
    (proof
      (suppose ([¬ φ]) ([↔ ψ ∨ φ ψ])
        [df-or φ ψ]
        [b2i mpi [∨ φ ψ] [¬ φ] ψ]
        [or> φ ψ]
        [>bii ψ [∨ φ ψ]])))
  (th-pattern !false<)

  (th "false>" (wff ![φ ψ])
    (meta (:comment . "Simplification of disjunction when the right disjunct is false."))
    (ass [→ ¬ ψ ↔ φ ∨ φ ψ])
    (proof
      (suppose ([¬ ψ]) ([↔ φ ∨ φ ψ])
        [infer false< ψ φ]
        [com ψ φ]
        [bitr φ [∨ ψ φ] [∨ φ ψ]])))
  (th-pattern !false>)

  (th "di-bi<" (wff ![φ ψ χ])
    (meta (:comment . "Disjunction distributes over the biconditional.")
          (:description . "An axiom of system DS in Vladimir Lifschitz, \"On calculational proofs\" (1998); orbidi in set.mm."))
    (ass [↔ ∨ φ ↔ ψ χ ↔ ∨ φ ψ ∨ φ χ])
    (proof
      (linear-subproof () ([→ ∨ φ ↔ ψ χ ↔ ∨ φ ψ ∨ φ χ])
        [or< φ ψ]
        [or< φ χ]
        [jca φ [∨ φ ψ] [∨ φ χ]]
        [ded true-eq [∨ φ ψ] [∨ φ χ] φ]
        [!or!eqt> ψ χ φ]
        [jaoi φ [↔ ψ χ] [↔ ∨ φ ψ ∨ φ χ]])
      ;; ---
      (suppose ([¬ φ]) ([→ ↔ ∨ φ ψ ∨ φ χ ∨ φ ↔ ψ χ])
        [infer false< φ ψ]
        [infer false< φ χ]
        [!bi!eqti ψ [∨ φ ψ] χ [∨ φ χ]]
        [b2i ded or> φ [↔ ψ χ] [↔ ∨ φ ψ ∨ φ χ]])
      [or< φ [↔ ψ χ]]
      [ded ax1 [∨ φ ↔ ψ χ] [↔ ∨ φ ψ ∨ φ χ] φ]
      [casei φ [→ ↔ ∨ φ ψ ∨ φ χ ∨ φ ↔ ψ χ]]
      ;; ---
      [>bii [∨ φ ↔ ψ χ] [↔ ∨ φ ψ ∨ φ χ]])) )

(in-context !bi
  (th "ass" (wff ![φ ψ χ])
    (meta (:comment . "Associative law for biconditional.")
          (:description . "An axiom of system DS in Vladimir Lifschitz, \"On calculational proofs\" (1998); biass in set.mm.
Noted by Jan Lukasiewicz c. 1923."))
    (ass [↔ [↔ ↔ φ ψ χ] [↔ φ ↔ ψ χ]])
    (proof
      (suppose ([φ]) ([↔ ↔ ↔ φ ψ χ ↔ φ ↔ ψ χ])
        [infer true< φ ψ]
        [infer  eqt< ψ [↔ φ ψ] χ]
        [infer true< φ [↔ ψ χ]]
        [b2i bitr [↔ ↔ φ ψ χ] [↔ ψ χ] [↔ φ ↔ ψ χ]])
      (suppose ([¬ φ]) ([↔ ↔ ↔ φ ψ χ ↔ φ ↔ ψ χ])
        [infer false< φ ψ]
        [infer   eqt< [¬ ψ] [↔ φ ψ] χ]
        [infer false< φ [↔ ψ χ]]
        [neg< ψ χ]
        [comi [¬ ↔ ψ χ] [↔ ¬ ψ χ]]
        [>tr [↔ ¬ ψ χ]   [¬ ↔ ψ χ]
             [↔ ↔ φ ψ χ] [↔ φ ↔ ψ χ]])
      [casei φ [↔ ↔ ↔ φ ψ χ ↔ φ ↔ ψ χ]])) )

(th "nan<" (wff ![φ ψ])
    (meta (:comment . "Introduction of conjunct inside a contradiction.")
          (:description . "Closed form of intnanr in set.mm."))
    (ass [→ ¬ φ ¬ ∧ φ ψ])
    (proof
      [!an!sim< φ ψ]
      [infer con [∧ φ ψ] φ]))

(th "nan>" (wff ![φ ψ])
    (meta (:comment . "Introduction of conjunct inside a contradiction.")
          (:description . "Closed form of intnan is set.mm."))
    (ass [→ ¬ ψ ¬ ∧ φ ψ])
    (proof
      [!an!sim> φ ψ]
      [infer con [∧ φ ψ] ψ]))

(in-context !an
  (th "mp<" (wff ![φ ψ χ])
    (meta (:comment . "An inference based on modus ponens.")
          (:description . "mpan in set.mm."))
    (hypo [φ] [→ ∧ φ ψ χ])
    (ass [→ ψ χ])
    (proof
      [exp φ ψ χ]
      [ax-mp φ [→ ψ χ]]))

  (th "mp>" (wff ![φ ψ χ])
    (meta (:comment . "An inference based on modus ponens.")
          (:description . "mpan2 in set.mm."))
    (hypo [ψ] [→ ∧ φ ψ χ])
    (ass [→ φ χ])
    (proof
      [exp φ ψ χ]
      [com12i φ ψ χ]
      [ax-mp ψ [→ φ χ]]))

  (th "mp2" (wff ![φ ψ χ])
    (meta (:comment . "An inference based on modus ponens.")
          (:description . "mp2an in set.mm."))
    (hypo [φ] [ψ] [→ ∧ φ ψ χ])
    (ass [χ])
    (proof
      [>andi φ ψ]
      [ax-mp [∧ φ ψ] χ]))

  (th "mpd<" (wff ![φ ψ χ])
    (meta (:comment . "An inference based on modus ponens.")
          (:description . "mpdan in set.mm."))
    (hypo [→ φ ψ] [→ ∧ φ ψ χ])
    (ass [→ φ χ])
    (proof
      [exp φ ψ χ]
      [mpd ψ χ φ]))

  (th "mpd>" (wff ![φ ψ χ])
    (meta (:comment . "An inference based on modus ponens with commutation of antecedents.")
          (:description . "mpancom in set.mm."))
    (hypo [→ ψ φ] [→ ∧ φ ψ χ])
    (ass [→ ψ χ])
    (proof
      [+syl com φ ψ χ]
      [mpd< ψ φ χ])) )

(th "2true" (wff ![φ ψ])
    (meta (:comment . "Two truths are equivalent.")
          (:description . "2th in set.mm."))
    (hypo [φ] [ψ])
    (ass [↔ φ ψ])
    (proof
      [infer ax1 ψ φ]
      [infer ax1 φ ψ]
      [>bii φ ψ]))

(th "2false" (wff ![φ ψ])
    (meta (:comment . "Two falsehoods are equivalent."))
    (hypo [¬ φ] [¬ ψ])
    (ass [↔ φ ψ])
    (proof
      [false-eq φ ψ]
      [!an!mp2 [¬ φ] [¬ ψ] [↔ φ ψ]]))

;(in-context !bi
  ; use (infer !bi!true>)
;  (th "true" (wff ("φ" "ψ"))

  ; use (infer !bi!false>)
;  (th "false" (wff ("φ" "ψ"))

; use (infer !bi!true>i)
;(th "banmp<" (wff ("φ" "ψ" "χ"))

; use (infer !bi!true<i)
;(th "banmp>" (wff ("φ" "ψ" "χ"))

(in-context !im
  (th "true<" (wff ![φ ψ])
    (meta (:comment . "A wff is equal to itself with true antecedent.")
          (:description . "biimt in set.mm."))
    (ass [→ φ ↔ ψ → φ ψ])
    (proof
      [ax1 ψ φ]
      [infer ax1 [→ ψ → φ ψ] φ]
      [→ φ → → φ ψ ψ]
      [>bid φ ψ [→ φ ψ]]))
  (th-pattern !true<))

(in-context !or
  (th "true<" (wff ![φ ψ])
    (meta (:comment . "A wff disjoined with truth is true.")
          (:description . "biort in set.mm."))
    (ass [→ φ ↔ φ ∨ φ ψ])
    (proof
      [or< φ ψ]
      [ded ax1 [∨ φ ψ] φ φ]
      [ax1 φ [∨ φ ψ]]
      [>bid φ φ [∨ φ ψ]]))
  (th-pattern !true<)

  (th "true>" (wff ![φ ψ])
    (meta (:comment . "Simplification of disjunction when the right disjunct is true."))
    (ass [→ ψ ↔ ψ ∨ φ ψ])
    (proof
      [or> φ ψ]
      [ded ax1 [∨ φ ψ] ψ ψ]
      [ax1 ψ [∨ φ ψ]]
      [>bid ψ ψ [∨ φ ψ]]))
  (th-pattern !true>))

(in-context !an
  (th "false<" (wff ![φ ψ])
    (meta (:comment . "Simplification of conjunction when the left conjunct is false."))
    (ass [→ ¬ φ ↔ φ ∧ φ ψ])
    (proof
      [<neg φ [∧ φ ψ]]
      [sim< φ ψ]
      [infer ax1 [→ ∧ φ ψ φ] [¬ φ]]
      [>bid [¬ φ] φ [∧ φ ψ]]))
  (th-pattern !false<)

  (th "false>" (wff ![ψ φ])
    (meta (:comment . "Simplification of conjunction when the right conjunct is false."))
    (ass [→ ¬ φ ↔ φ ∧ ψ φ])
    (proof
      [<neg φ [∧ ψ φ]]
      [sim> ψ φ]
      [infer ax1 [→ ∧ ψ φ φ] [¬ φ]]
      [>bid [¬ φ] φ [∧ ψ φ]])) 
  (th-pattern !false>))

(in-context !bi
  (th "tr<" (wff ![φ ψ χ])
    (meta (:comment . "A transitive law of equivalence.")
          (:description . "Compare Theorem *4.22 of Principia Mathematica p. 117; biantr in set.mm."))
    (ass [→ ∧ ↔ φ ψ ↔ χ ψ ↔ φ χ])
    (proof
      (suppose ([∧ ↔ φ ψ ↔ χ ψ]) ([↔ φ χ])
        [siman [↔ φ ψ] [↔ χ ψ]]
        [b2i bitr φ ψ χ]))) )

) ; end module prop
