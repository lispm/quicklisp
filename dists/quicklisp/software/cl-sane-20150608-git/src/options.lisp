(in-package :sane)

(defclass optionoid ()
  ((name        :accessor get-name        :initarg :name)
   (title       :accessor get-title       :initarg :title)
   (description :accessor get-description :initarg :description)
   (position    :accessor get-position    :initarg :position)
   (device      :accessor get-device      :initarg :device)))

(defclass option (optionoid)
  ((value        :initarg :value)
   (capabilities :reader  get-capabilities :initarg :capabilities)))

(defclass group (optionoid)
  ((group-end-pos :initform nil)
   (group-start-pos :initarg :group-start-pos)))

(defclass option-value ()
  ((type       :accessor get-type       :initarg  :type)
   (dirty      :accessor get-dirty      :initform t)
   (constraint :accessor get-constraint :initarg  :constraint)
   (unit       :initarg  :unit)
   (value      :initform nil)
   (c-size     :initarg  :c-size)))


;;; Constraint classes.
(defclass constraint ()
  ())

(defclass trivial-constraint (constraint)
())

(defclass range-constraint (constraint)
((min  :accessor get-min  :initarg :min)
 (step :accessor get-step :initarg :step)
 (max  :accessor get-max  :initarg :max)))

(defclass list-constraint (constraint)
((list :accessor get-list :initarg :list)))

(defun option-value-readable-p (capabilities)
"Predicate to test whether we can read the value of the option. This is
  possible if soft-detect and not inactive."
(and (find :soft-detect capabilities) (not (find :inactive capabilities))))

(defun recalculate-options-list (device)
  "Read the options list from the device. Groups get placed in a list of groups
and options in a list (and hash-table) of options. DEVICE is a sane::device
btw."

  ;; M is the number of non-group options read: important for making the
  ;; name-based hash table work.
  (let ((m 0) (desc))
    (with-slots (options options-hash groups handle) device

      ;; Blat any previous values.
      (setf options nil
            options-hash (make-hash-table :test #'equal))

      ;; The device has some # of option descriptors, starting at index 1. desc
      ;; will be a null pointer when we get past the end.
      (iterate
        (for n upfrom 1)
        (setf desc (sane-lowlevel::sane_get_option_descriptor handle n))

        (when (null-pointer-p desc) (terminate))

        (with-foreign-slots ((sane-lowlevel::name
                              sane-lowlevel::title
                              sane-lowlevel::desc
                              sane-lowlevel::type
                              sane-lowlevel::cap
                              sane-lowlevel::constraint_type
                              sane-lowlevel::constraint
                              sane-lowlevel::size
                              sane-lowlevel::unit) desc
                             (:struct sane-lowlevel::sane_option_descriptor))

          (cond
            ;; Push groups onto the list in slot GROUPS
            ((eq sane-lowlevel::type :sane_type_group)
             ;; If this isn't the first group, then tell the previous group that
             ;; our one starts here.
             (when groups
               (setf (slot-value (car groups) 'group-end-pos) (1- m)))

             (push (make-instance 'group
                                  :name sane-lowlevel::name
                                  :title sane-lowlevel::title
                                  :description sane-lowlevel::desc
                                  :device device
                                  :position n
                                  :group-start-pos m)
                   groups))

            ;; Non-groups are real honest-to-god options. Read in gobs of data
            ;; and push them onto slot OPTIONS.
            (t
             (let*
                 ((caps (lispify-sane-capabilities sane-lowlevel::cap))
                  (opt (make-instance 'option
                                      :name sane-lowlevel::name
                                      :title sane-lowlevel::title
                                      :description sane-lowlevel::desc
                                      :device device
                                      :position n
                                      :capabilities caps)))

               (setf (slot-value opt 'value)
                     (make-instance
                      'option-value
                      :type sane-lowlevel::type
                      :unit sane-lowlevel::unit
                      :constraint (lispify-constraint
                                   sane-lowlevel::constraint_type
                                   sane-lowlevel::constraint)
                      :c-size sane-lowlevel::size))

               (push opt options))

             ;; If the option has a real name, add it to the hash table too.
             (unless (string= "" sane-lowlevel::name)
               (setf (gethash sane-lowlevel::name options-hash) m))
             (incf m)))))

      ;; Set the last of the groups' end-pos to m.
      (setf (slot-value (car groups) 'group-end-pos) (1- m))

      ;; Reverse the two lists we built.
      (setf options (nreverse options)
            groups (nreverse groups)))))

(defun read-option-value (device position type size)
  "Actually read the value of an option from DEVICE. Read the option at
POSITION, which has TYPE and is length SIZE. This is an internal-ish function
called by recalculate-options-list."
  (case type
    (:sane_type_string
     (with-foreign-object (str :char size)

       (sane-lowlevel::sane_control_option
        (handle device) position :sane_action_get_value str (null-pointer))

       (convert-from-foreign str :string)))

    (:sane_type_int
     (let* ((len (ceiling (/ size (foreign-type-size :int))))
            (array (make-array len :element-type 'integer )))

       (with-foreign-object (ints :int len)

         (sane-lowlevel::sane_control_option
          (handle device) position :sane_action_get_value ints (null-pointer))

         (iterate (for i from 0 to (1- len))
                  (setf (aref array i)
                        (mem-aref ints :int i))))
       array))

    (:sane_type_fixed
     (let* ((len (ceiling (/ size (foreign-type-size :int))))
            (array (make-array len :initial-element 0/1 )))

       (with-foreign-object (ints :int len)

         (sane-lowlevel::sane_control_option
          (handle device) position :sane_action_get_value ints (null-pointer))

         (iterate (for i from 0 to (1- len))
                  (setf (aref array i)
                        (/ (mem-aref ints :int i) (ash 1 16)))))
       array))

    (:sane_type_bool
    (with-foreign-object (bool :int)
      (sane-lowlevel::sane_control_option
       (handle device) position :sane_action_get_value bool (null-pointer))

      (unless (= 0 (mem-ref bool :int)) t)))

    ;; Buttons don't actually have values.
    (:sane_type_button nil)

    (t
    (error "Unhandled option type: ~A~%" type))))

(defun lispify-sane-capabilities (caps)
  ;; This is a bit ugly, but I refuse to write the sane_caps_ nonsense!
  (iterate
    (for cap in
    (foreign-bitfield-symbols 'sane-lowlevel::capability-flags caps))
    (collecting
    (case cap
      (:sane_cap_soft_select :soft-select)
      (:sane_cap_hard_select :hard-select)
      (:sane_cap_soft_detect :soft-detect)
      (:sane_cap_emulated :emulated)
      (:sane_cap_automatic :automatic)
      (:sane_cap_inactive :inactive)
      (:sane_cap_advanced :advanced)
      (:sane_cap_always_settable :always-settable)))))

(defun foreign-strings-to-lisp (pointer &optional (length 0 lenp))
  "Suppose POINTER points to an array of pointers, each of which
points at a string. Evaluate to a list containing these strings. If
LENGTH is given, use that, else assume the array is null-terminated."

  (iterate
    (initially (setf i -1))
    (for i next (if (and lenp (= i (- length 1)))
                    (terminate)
                    (incf i)))

    (when (and (not lenp)
               (null-pointer-p
                (mem-aref pointer :pointer i)))
    (terminate))

    (collect (foreign-string-to-lisp (mem-aref pointer :pointer i)))))

(defun lispify-constraint (type constraint)
  "Parse a libsane constraint pointed to by CONSTRAINT of type TYPE into
something more pleasant: one of the sane::constraint heirarchy."
  (case type
    (:sane_constraint_none
     (make-instance 'trivial-constraint))

    (:sane_constraint_range
    (with-foreign-slots
        ((sane-lowlevel::min sane-lowlevel::max sane-lowlevel::quant)
         constraint (:struct sane-lowlevel::sane_range))

      (make-instance 'range-constraint
                     :min sane-lowlevel::min
                     :step sane-lowlevel::quant
                     :max sane-lowlevel::max)))

    (:sane_constraint_word_list
    (make-instance 'list-constraint
                   :list (iterate
                           (for n from 1 to (mem-ref constraint :int))
                           (collect (mem-aref constraint :int n)))))

    (:sane_constraint_string_list
    (foreign-strings-to-lisp constraint))))

(defgeneric device-option (optspec device)
  (:documentation
   "Read an option from a device. DEVICE should be the SANE device from which to
read the option and OPTSPEC should be either an integer index or a string name
for the option."))

(defmethod device-option ((n integer) (device device))
  "Access an option of DEVICE by position."
  (unless (slot-value device 'options)
    (recalculate-options-list device))

  (nth n (slot-value device 'options)))

(defmethod device-option ((name string) (device device))
  "Access an option of DEVICE by name."
  (unless (slot-value device 'options)
    (recalculate-options-list device))

  (let ((n (gethash name (slot-value device 'options-hash))))
    (when n (nth n (slot-value device 'options)))))

(defun update-option-value (option)
  "Read the value of an option from the device."
  (with-slots (value) option
    (setf (slot-value value 'value)
          (read-option-value (get-device option) (get-position option)
                             (slot-value value 'type)
                             (slot-value value 'c-size)))
    (setf (slot-value value 'dirty) nil)))

(defgeneric option-value (opt)
  (:documentation
   "Get a (possibly cached) value that was set for the option OPT."))

(defmethod option-value ((option option))
  (when (option-value-readable-p (get-capabilities option))
    (with-slots (value) option
      (when (get-dirty value) (update-option-value option))
      (slot-value value 'value))))

(defsetf option-value (option) (new-value)
  `(set-option-value (slot-value (slot-value ,option 'value) 'type)
                     ,option ,new-value))

(defgeneric set-option-value (type option value)
  (:documentation
   "Store a value in an option. Accessible via the OPTION-VALUE setf."))

(defmethod set-option-value ((type (eql :sane_type_string))
                             (option option) new-value)
  "Set the value of an option expecting a string."
  (unless (subtypep (type-of new-value) 'string)
    (error "Trying to set a string device option with a ~A"
           (type-of new-value)))
  (with-foreign-string (foreign-string new-value)
    (actually-set-option-value option foreign-string)))

(defmacro make-set-option-value-vector (foreign-type lisp-type txform)

  `(defmethod set-option-value ((type (eql ,foreign-type))
                                (option option) new-value)

     (let ((vals-to-store (if (subtypep (type-of new-value) ,lisp-type)
                              (list new-value)
                              (coerce new-value 'list)))
           (c-length (/ (slot-value (slot-value option 'value) 'c-size)
                        (foreign-type-size :int))))

       (unless (= (length vals-to-store) c-length)
         (error "Trying to fill a device option of length ~A with ~A elements"
                c-length (length vals-to-store)))

       (with-foreign-object (foreign-ints :int c-length)
         (iterate (for x in vals-to-store)
                  (for i from 0)
                  (setf (mem-aref foreign-ints :int i)
                        (funcall ,txform x)))
         (actually-set-option-value option foreign-ints)))))

(make-set-option-value-vector :sane_type_int 'integer
                              #'identity)
(make-set-option-value-vector :sane_type_fixed 'number
                              (lambda (x) (* x (ash 1 16))))

(defmethod set-option-value ((type (eql :sane_type_bool))
                             (option option) new-value)
  "Set the value of an option expecting a bool."
  (with-foreign-object (bool :int)
    (setf (mem-ref bool :int) (if new-value 1 0))
    (actually-set-option-value option bool)))

(defun actually-set-option-value (option memory)
  "Finally make the call to SANE to set the option value. Dirties parameters etc
if necessary and checks for errors."
  (with-slots (device position value) option
    (with-foreign-object (response :int)

      (setf (get-dirty value) t)

      (let ((ret (sane-lowlevel::sane_control_option
                  (handle device) position
                  :sane_action_set_value memory response)))
        (unless (eq ret :SANE_STATUS_GOOD)
          (error "Setting option failed. Error: ~A" ret)))
      
      (unless (= 0 (boole boole-and 2 (mem-ref response :int)))
        (recalculate-options-list device))

      (unless (= 0 (boole boole-and 4 (mem-ref response :int)))
        (format t "(Parameters may have changed)~%")))))

(defgeneric device-groups (device)
  (:documentation "Return a list of the groups for a device"))

(defmethod device-groups ((d device))
  (unless (slot-value d 'groups)
    (recalculate-options-list d))
  (slot-value d 'groups))

(defmethod print-object ((g group) stream)
  (format stream "=- Group: ~A (~A)~%"
          (get-title g)
          (1+ (- (slot-value g 'group-end-pos)
                 (slot-value g 'group-start-pos))))

  (iterate
    (for m
         from (slot-value g 'group-start-pos)
         to (slot-value g 'group-end-pos))
    (format stream "  ~A~%"
            (device-option m (get-device g)))))

(defmethod print-object ((o option) stream)
  (format stream
          "~A (~A)"
          (get-name o)
          (get-type (slot-value o 'value))))
