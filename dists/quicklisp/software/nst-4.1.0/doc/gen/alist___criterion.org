# DO NOT EDIT --- generated from source code.
*** Criterion =alist*=
The =:alist*= criterion evaluates the form under test, expecting to find an association list as a result.  Using the two given function specs to test the keys (during retrieval, via =assoc=) and the values, the criterion enforces that the lists contains equivalent keys, mapping to respective equivalent values.  Note that the list may contain additional key/value pairs; see also =:alist=.

Usage:
#+begin_example
(:alist* KEY-TEST-FN VALUE-TEST-FN (KEY VALUE) ... (KEY VALUE))
#+end_example

