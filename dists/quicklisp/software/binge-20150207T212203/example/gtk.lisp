;;;  gtk.lisp --- example gtk FFI generation module

;;;  Copyright (C) 2004 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-B793772F5F0AAB96B388A951A43B40E0.lisp,v 1.1 2004/04/15 22:54:17 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:
;;;
;;;  This works with gcc on FreeBSD.

#+cmu (ext:file-comment "$Id: F-B793772F5F0AAB96B388A951A43B40E0.lisp,v 1.1 2004/04/15 22:54:17 wcp Exp $")

(in-package :binge-examples)

(defvar *gtk-ffi-c-constants*
  nil)

(defun create-gtk-ffi ()
  (let ((namespace (make-instance 'c-namespace)))
    (binge-compiler:add-declaration *gcc-builtin-va_list* namespace)
    (populate-namespace namespace
			(extract-from-headers '("gtk/gtk.h")
					      :include-paths '("/usr/X11R6/include/gtk12"
							       "/usr/X11R6/include"
							       "/usr/local/include/glib12")
					      :defines *gcc-null-defines*
					      :constants *gtk-ffi-c-constants*))
    ;; leaving out the types we'll get only the necessary ones
    (make-ffi #P"gtk-ffi.lisp" namespace :variables t :functions t :constants t)))
