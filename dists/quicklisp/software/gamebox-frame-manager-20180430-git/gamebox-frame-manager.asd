(asdf:defsystem #:gamebox-frame-manager
  :description "A manager for frames within a game loop."
  :author "Michael Fiano <mail@michaelfiano.com>"
  :maintainer "Michael Fiano <mail@michaelfiano.com>"
  :license "MIT"
  :homepage "https://www.michaelfiano.com/projects/gamebox-frame-manager"
  :source-control (:git "git@github.com:mfiano/gamebox-frame-manager.git")
  :bug-tracker "https://github.com/mfiano/gamebox-frame-manager/issues"
  :version "1.0.0"
  :encoding :utf-8
  :long-description #.(uiop:read-file-string (uiop/pathname:subpathname *load-pathname* "README.md"))
  :depends-on (#:local-time
               #:simple-logger)
  :pathname "src"
  :serial t
  :components
  ((:file "package")
   (:file "log")
   (:file "frame-manager")))
