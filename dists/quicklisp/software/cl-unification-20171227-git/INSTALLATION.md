CL-UNIFICATION INSTALLATION
===========================

There are several ways to install CL-UNIFICATION.

Quicklisp
---------

CL-UNIFICATION is in quicklisp.  The easiest way to install it is by
issuing

	(ql:quickload "CL-UNIFICATION")

The package comes with a MK:DEFSYSTEM definition:
"cl-unification.system".

Issuing

	(mk:load-system "CL-UNIFICATION")

or

	(mk:compile-system "CL-UNIFICATION")

will make the UNIFY package available.


ASDF
----

There is also an ASDF system definition for those who use this system.

Issuing

	(asdf:oos 'asdf:load-op "CL-UNIFICATION")

should make the library available in your environment.


### ASDF-INSTALL

If your CL implementation is ASDF-INSTALL aware, you should also be
able to just say

        (asdf-install:install "CL-UNIFICATION")

provided that the package is unpacked in an ASDF-INSTALL known
directory.

Enjoy
