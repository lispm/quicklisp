;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: umlisp -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:    data-structures.lisp
;;;; Purpose:  Basic data objects for UMLisp
;;;; Author:   Kevin M. Rosenberg
;;;; Created:  Apr 2000
;;;;
;;;; $Id$
;;;;
;;;; This file, part of UMLisp, is
;;;;    Copyright (c) 2000-2004 by Kevin M. Rosenberg, M.D.
;;;;
;;;; UMLisp users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License.
;;;; *************************************************************************

(in-package #:umlisp-orf)

;;; Paths for files

(defvar *umls-path*
  (make-pathname :directory '(:absolute "data" "umls" "2003AC"))
  "Path for base of UMLS data files")

(defvar *meta-path*
    (merge-pathnames
     (make-pathname :directory '(:relative "META"))
     *umls-path*))

(defvar *lex-path*
    (merge-pathnames
     (make-pathname :directory '(:relative "LEX"))
     *umls-path*))

(defvar *net-path*
    (merge-pathnames
     (make-pathname :directory '(:relative "NET"))
     *umls-path*))

(defun umls-path! (p)
  (setq *umls-path* p))


;;; Structures for parsing UMLS text files

(defparameter *umls-files* nil
  "List of umls file structures. Used when parsing text files.")
(defparameter *umls-cols* nil
  "List of meta column structures. Used when parsing text files.")


;; Preliminary objects to replace structures

(defclass ufile ()
  ((fil :initarg :fil :accessor fil)
   (table :initarg :table :accessor table)
   (des :initarg :des :accessor des)
   (fmt :initarg :fmt :accessor fmt)
   (cls :initarg :cls :accessor cls)
   (rws :initarg :rws :accessor rws)
   (bts :initarg :bts :accessor bts)
   (fields :initarg :fields :accessor fields)
   (ucols :initarg :ucols :accessor ucols))
  (:default-initargs :fil nil :table nil :des nil :fmt nil :cls nil :rws nil :bts nil
                     :fields nil :ucols nil)
  (:documentation "UMLS File"))

(defclass ucol ()
  ((col :initarg :col :accessor col)
   (des :initarg :des :accessor des)
   (ref :initarg :ref :accessor ref)
   (min :initarg :min :accessor cmin)
   (av :initarg :av :accessor av)
   (max :initarg :max :accessor cmax)
   (fil :initarg :fil :accessor fil)
   (sqltype :initarg :sqltype :accessor sqltype)
   (dty :initarg :dty :accessor dty :documentation "new in 2002: suggested SQL datatype")
   (parse-fun :initarg :parse-fun :accessor parse-fun)
   (quote-str :initarg :quote-str :accessor quote-str)
   (datatype :initarg :datatype :accessor datatype)
   (custom-value-fun :initarg :custom-value-fun :accessor custom-value-fun))
  (:default-initargs :col nil :des nil :ref nil :min nil :av nil :max nil :fil nil
                     :sqltype nil :dty nil :parse-fun nil :datatype nil
                     :custom-value-fun nil)
  (:documentation "UMLS column"))


(defmethod print-object ((obj ufile) (s stream))
  (print-unreadable-object (obj s :type t)
    (format s "~A" (fil obj))))

(defmethod print-object ((obj ucol) (s stream))
  (print-unreadable-object (obj s :type t)
    (format s "~A" (col obj))))



