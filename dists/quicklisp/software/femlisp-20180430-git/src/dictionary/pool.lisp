;;; -*- mode: lisp; fill-column: 75; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; pool.lisp - Pools
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2018-
;;; Nicolas Neuss, Friedrich-Alexander-Universitaet Erlangen-Nuernberg
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
;;; IN NO EVENT SHALL THE AUTHOR, THE KARLSRUHE INSTITUTE OF TECHNOLOGY,
;;; OR OTHER CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.dictionary)

(file-documentation "Contains implementations of pools.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; A simple pool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass pool ()
  ((table :documentation "A table mapping keys to a list of free objects.")
   (test :initform 'equal :initarg :test))
  (:documentation "A pool implemented via a hash table."))

(defmethod initialize-instance :after ((pool pool) &key &allow-other-keys)
  (with-slots (table test) pool
    (setf table (make-hash-table :test test))))

(defmethod describe-object :after ((pool pool) stream)
  (with-slots (table) pool
    (format stream "~A contains the following:~%" pool)
    (maphash (lambda (key value)
               (format stream "~A ->~%~{ ~A~}~%" key value))
             table)))

(defgeneric put-in-pool (pool key object)
  (:documentation "Put an object into the pool.")
  (:method ((pool pool) key object)
    (with-slots (table entries) pool
      (dbg :dictionary "New in pool ~A: object ~A under key=~A"
           pool object key)
      (push object (gethash key table ())))))

(defgeneric get-from-pool (pool key)
  (:documentation "Get a suitable object from the pool.  If no such object
  is found, NIL is returned.")
  (:method ((pool pool) key)
    (with-slots (table) pool
      (whereas ((object (pop (gethash key table nil))))
        (dbg :dictionary "Getting from pool ~A: object ~A with key=~A"
             pool object key)
        object))))

(defgeneric pool-size (pool)
  (:documentation "Calculates number of entries in pool.")
  (:method (pool)
    (with-slots (table) pool
      (loop for entries being each hash-value of table
            sum (length entries)))))

(defclass thread-safe-pool (pool mutex-mixin)
  ()
  (:documentation "A thread-safe pool."))

(defmethod put-in-pool ((pool thread-safe-pool) key object)
  (with-mutual-exclusion (pool)
    (call-next-method)))

(defmethod get-from-pool ((pool thread-safe-pool) key)
  (with-mutual-exclusion (pool)
    (call-next-method)))

(defmethod pool-size ((pool thread-safe-pool))
  (with-mutual-exclusion (pool)
    (call-next-method)))

(defclass worker-local-pool (pool)
  ()
  (:documentation "A pool for each thread"))

(defmethod initialize-instance :after ((pool worker-local-pool) &key &allow-other-keys)
  (with-slots (table test) pool
    ;; we want a new test
    (setf test (ecase test
                 ((eq eql) 'equal)
                 ((equal equalp) 'equalp)))
    (setf table (make-hash-table :test test))))
  
(defmethod put-in-pool ((pool worker-local-pool) key object)
  (call-next-method pool (list fl.parallel::*worker-id* key) object))

(defmethod get-from-pool ((pool worker-local-pool) key)
  (call-next-method pool (list fl.parallel::*worker-id* key)))

(defmethod pool-size ((pool worker-local-pool))
  (with-mutual-exclusion (pool)
    (with-slots (table) pool
      (loop for entries being each hash-value of table
            sum (count fl.parallel::*worker-id* entries :key #'first)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Multithreaded pool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass parpool (mutex-mixin)
  ((table :documentation "A table mapping keys to a list of parpool entries")
   (ppes :type hash-table
         :documentation "A table mapping an object to its ppe")
   (test :initform 'eql :initarg :test)
   #+(or)(generator :initform (required-argument) :initarg :generator))
  (:documentation "A parallel pool implemented as a shared hash-table.  The
slot pool contains a hash-table, the test slot contains the hash-table
test, the generator contains a function which is called if the pool is
empty, i.e. all objects are used and generates a new object for the given
argument."))

(defstruct (parpool-entry (:conc-name ppe-))
  object refcount key)

(defmethod initialize-instance :after ((pool parpool) &key &allow-other-keys)
  (with-slots (table test ppes) pool
    (setf table (make-hash-table :test test))
    (setf ppes (make-hash-table :test 'eq))))

(defmethod describe-object :after ((ppool parpool) stream)
  (format stream "~A contains the following:~%" ppool)
  (maphash (lambda (key value)
             (format stream "~A ->~%~{ ~A~}~%" key value))
           (slot-value ppool 'table))
  (format stream "Furthermore, the following objects are registered:~%")
  (maphash (lambda (key value)
             (format stream "~A -> ~A~%" key value))
           (slot-value ppool 'ppes)))

(defun register-in-pool (pool key object)
  "Note that the object is only registered, but not put in the pool!"
  (with-mutual-exclusion (pool)
    (with-slots (table ppes) pool
      (let ((ppe (make-parpool-entry :object object :key key)))
        (let (*print-array*)
          (dbg :parpool "Registering new ppe ~A for object ~A under key=~A"
               ppe object key))
        (assert (not (gethash object ppes)))
        (setf (gethash object ppes) ppe)))))

(defmethod get-from-pool ((pool parpool) key)
  (with-mutual-exclusion (pool)
    (with-slots (table) pool
      (whereas ((ppe (pop (gethash key table nil))))
        (lret ((object (ppe-object ppe)))
          (let ((*print-array*))
            (dbg :parpool "Getting from pool ppe ~A for object ~A under key=~A"
                 ppe object key)))))))

(defun put-back-in-pool (pool object)
  (with-mutual-exclusion (pool)
    (with-slots (ppes table) pool
      (let ((ppe (gethash object ppes)))
        (assert ppe)
        (let ((key (ppe-key ppe)))
          (assert (not (member ppe (gethash key table) :test 'eq)))
          (let (*print-array*)
            (dbg :parpool "Putting back in pool ppe ~A for object ~A under key=~A"
               ppe object key))
          (push ppe (gethash key table)))))))

(defun set-refcount (pool object count)
  (with-mutual-exclusion (pool)
    (with-slots (ppes) pool
      (let ((ppe (gethash object ppes)))
        (setf (ppe-refcount ppe) count)))))

(defun decrease-refcount (pool object &optional (decrement 1))
  (with-mutual-exclusion (pool)
    (with-slots (ppes) pool
      (let ((ppe (gethash object ppes)))
        (when (zerop (decf (ppe-refcount ppe) decrement))
          (setf (ppe-refcount ppe) nil)
          (put-back-in-pool pool object))))))

;;;; Testing

(defun pool-test ()

  (let ((pool (make-instance 'worker-local-pool :test 'eql)))
    (p-map 'list (lambda (k)
                 (assert (null (get-from-pool pool k)))
                 (put-in-pool pool k (* k k))
                 (get-from-pool pool k))
           (list 1 2 3 4))
    (pool-size pool))

  (let ((pool (make-instance 'parpool)))
    (let ((o1 (vector 1))
          (o2 (vector 1 1)))
      (register-in-pool pool 1 o1)
      (register-in-pool pool 1 o2)
      (let ((o (get-from-pool pool 1)))
        (format t "~&Got ~A~%" o)
        (set-refcount pool o 4)
        (describe pool)
        (decrease-refcount pool o)
        (decrease-refcount pool o)
        (decrease-refcount pool o)
        (decrease-refcount pool o)
        (describe pool)
        )))
  )
