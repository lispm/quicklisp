(asdf:defsystem #:genie
  :description "A simple wrapper to generate portably seedable pseudo-random numbers."
  :author "Michael Fiano <mail@michaelfiano.com>"
  :maintainer "Michael Fiano <mail@michaelfiano.com>"
  :license "MIT"
  :homepage "https://www.michaelfiano.com/projects/genie"
  :source-control (:git "git@github.com:mfiano/genie.git")
  :bug-tracker "https://github.com/mfiano/genie/issues"
  :version "1.0.0"
  :encoding :utf-8
  :long-description #.(uiop:read-file-string (uiop/pathname:subpathname *load-pathname* "README.md"))
  :depends-on (#:cl-variates
               #:simple-logger)
  :pathname "src"
  :serial t
  :components
  ((:file "package")
   (:file "genie")))
