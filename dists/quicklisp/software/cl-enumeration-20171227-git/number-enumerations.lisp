;;;; -*- Mode: Lisp -*-

;;;; number-enumerations.lisp --
;;;;
;;;; See file COPYING for copyright and licensing information.

(in-package "CL.EXTENSIONS.DACF.ENUMERATIONS")


;;;;===========================================================================
;;;; Number Enumeration.

(defclass number-enumeration (bounded-enumeration
                              bi-directional-enumeration
                              functional-enumeration)
  ((cursor :type number)
   (by :type function :initarg :by :accessor number-enumeration-increment)
   (rev-by :type function :accessor number-enumeration-reverse-increment))
  )


(defgeneric number-enumeration-p (x)
  (:method ((x t)) nil)
  (:method ((x number-enumeration)) t))


(defmethod initialize-instance :after ((x number-enumeration) &key)
  (setf (enumeration-cursor x) (enumeration-start x))

  ;; We need to set up the "reverse" increment.  However, since we
  ;; have BY to be a function, we must be clever.
  (let ((pure-incr (funcall (number-enumeration-increment x) 0)))
    (setf (slot-value x 'rev-by)
          (lambda (x) (+ (- pure-incr) x)))))


(defmethod print-object ((ne number-enumeration) s)
  (with-accessors ((start enumeration-start)
                   (end enumeration-end)
                   (cursor enumeration-cursor)
                   (incr number-enumeration-increment)
                   )
      ne
    (print-unreadable-object (ne s :identity t :type nil)
      (format s "Number enumeration [~S ~:[...~;~:*~S~]) at ~S~:[~; by ~A~]"
              start
              end
              cursor
              (not (eq incr #'1+))
              (typecase incr
                (number incr)
                (symbol incr)
                (function "#<INCR Function>"))))
    ))


(defmethod enumerate ((x (eql 'number))
                      &key
                      (start 0)
                      end
                      (by #'1+)
                      &allow-other-keys) 
  "Calling ENUMERATE on a the symbol NUMBER returns a NUMBER-ENUMERATION instance.

NUMBER-ENUMERATIONs are not real enumerations per se, but they have a
nice interface and serve to render things like Python xrange type.

START must be a number, while END can be a number or NIL. The next (or
previous) element is obtained by changing the current element by BY.
BY can be a function (#'1+ is the default) or a number, in which case
it is always summed to the current element.

A NUMBER-ENUMERATION can also enumerate COMPLEX numbers, but in this
case the actual enumeration properties are completely determined by
the value of BY."
  (when (or (complexp start) (complexp end))
    (warn "Making an enumeration of complex numbers is not guaranteed~
           to have any \"correct\" mathematical properties."))
  (etypecase by
    (function (make-instance 'number-enumeration
                             :object x :start start :end end
                             :by by))
    (number   (make-instance 'number-enumeration
                             :object x :start start :end end
                             :by #'(lambda (x) (+ by x))))))


(defmethod has-more-elements-p ((x number-enumeration))
  (with-accessors ((end enumeration-end)
                   (start enumeration-start)
                   )
      x
    (or (null end)
        (if (<= start end)
            (< (enumeration-cursor x) end)
            (> (enumeration-cursor x) end)))))



(defmethod has-next-p ((x number-enumeration))
  (with-accessors ((end enumeration-end)
                   (start enumeration-start)
                   )
      x
    (or (null end)
        (if (<= start end)
            (< (enumeration-cursor x) end)
            (> (enumeration-cursor x) end)))))


(defmethod next ((x number-enumeration) &optional default)
  (declare (ignore default))
  (with-slots (cursor by)
      x
    (prog1 cursor
      (setf cursor (funcall by cursor)))))


(defmethod has-previous-p ((x number-enumeration))
  (has-next-p x))


(defmethod previous ((x number-enumeration) &optional default)
  (declare (ignore default))
  (with-slots (cursor rev-by)
      x
    (setf cursor (funcall rev-by cursor))))


(defmethod current ((x number-enumeration) &optional (errorp t) (default nil))
  (cond ((and errorp (not (has-more-elements-p x)))
         (error 'no-such-element :enumeration x))
        ((and (not errorp) (not (has-next-p x)))
         default)
        (t (enumeration-cursor x))))


(defmethod reset ((x number-enumeration))
  (setf (enumeration-cursor x) (enumeration-start x)))


(defun range (start end &optional (incr #'1+))
  "The function RANGE is a utility function the produce a \"stream\"
(quote mandatory) of numbers. It is almost equivalent to the APL/J
iota operator and to Python xrange type.

The main use of RANGE is in conjunction with FOREACH and other
iteration constructs.

Arguments and Values:

START : a NUMBER
END : a NUMBER or NIL
INCR : a NUMBER or a function of one argument (default is #'1+)
result : a NUMBER-ENUMERATION

Examples:

;;;; The two calls below are equivalent:

cl-prompt> (range 2 10 3)
#<Number enumeration [2 10) at 2 by #<FUNCTION> XXXXXX>

cl-prompt> (enumerate 'number :start 2 :end 10 :by 3)
#<Number enumeration [2 10) at 2 by #<FUNCTION> XXXXXX>
"
  (enumerate 'number :start start :end end :by incr))


;;;; end of file -- number-enumerations.lisp --
