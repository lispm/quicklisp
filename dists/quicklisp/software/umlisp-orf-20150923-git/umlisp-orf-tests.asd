;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          umlisp-tests.asd
;;;; Purpose:       ASDF system definitionf for umlisp testing package
;;;; Author:        Kevin M. Rosenberg
;;;; Date Started:  Apr 2003
;;;;
;;;; $Id$
;;;; *************************************************************************

(defpackage #:umlisp-tests-system
  (:use #:asdf #:cl))
(in-package #:umlisp-tests-system)

(defsystem umlisp-orf-tests
    :depends-on (:rt :umlisp)
    :components
    ((:module tests
	      :components
	      ((:file "package")
	       (:file "basic" :depends-on ("package"))
	       (:file "parse" :depends-on ("package"))))))

(defmethod perform ((o test-op) (c (eql (find-system 'umlisp-orf-tests))))
  (or (funcall (intern (symbol-name '#:do-tests)
		       (find-package '#:regression-test)))
      (error "test-op failed")))

