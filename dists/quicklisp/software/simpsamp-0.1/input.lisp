(in-package #:simpsamp)

(defmacro with-list-iterator ((next size exhausted? list-form) &body body)
  "Evaluates BODY with an iterator on a list.

LIST-FORM is evaluated once to produce the list to iterate upon.  The
resulting list is never modified (unless from within BODY), the
consequences of which are undefined).

NEXT is defined with MACROLET.  Each evaluation of (NEXT) yields the
next successive element of the list.

SIZE is defined with MACROLET.  Each evaluation of (SIZE) yields the
length of the list.  The length may or may not be calculated upon each
evaluation; it may (but need not be) be memoized.

EXHAUSTED? is defined with MACROLET?.  Each evaluation of (EXHAUSTED?)
yields a generalized boolean indicating whether the list has been
exhausted by evaluations of NEXT.

The effect of calling NEXT more times than the length of the list is
undefined."
  (jpl-util:with-gensyms (orig-list% remaining%)
    `(let* ((,orig-list% ,list-form)
	    (,remaining% ,orig-list%))
       (declare (type list ,orig-list% ,remaining%))
       (macrolet ((,next ()
		    '(pop ,remaining%))
		  (,size ()
		    '(length ,orig-list%))
		  (,exhausted? ()
		    '(endp ,remaining%)))
	 ,@body))))

(defmacro with-vector-iterator ((next size exhausted? vector-form) &body body)
  "Evaluates BODY with an iterator on a vector.

VECTOR-FORM is evaluated once to produce the vector to iterate upon.
The resulting vector is never modified (unless from within BODY, the
consequences of which are undefined).

NEXT is defined with MACROLET.  Each evaluation of (NEXT) yields the
next successive element of the vector.

SIZE is defined with MACROLET.  Each evaluation of (SIZE) yields the
length of the vector.

EXHAUSTED? is defined with MACROLET?.  Each evaluation of (EXHAUSTED?)
yields a generalized boolean indicating whether the vector has been
exhausted by evaluations of NEXT.

The effect of calling NEXT more times than the length of the vector is
undefined."
  (jpl-util:with-gensyms (vector% pos%)
    `(let ((,vector% ,vector-form)
	   (,pos% 0))
       (declare (type vector ,vector%)
		(type fixnum ,pos%))
       (macrolet ((,next ()
		    '(prog1 (aref ,vector% ,pos%)
		       (incf ,pos%)))
		  (,size ()
		    '(length ,vector%))
		  (,exhausted? ()
		    '(= ,pos% (length ,vector%))))
	 ,@body))))

(defmacro with-range-iterator ((next size exhausted? lower-form upper-form) &body body)
  "Evaluates BODY with an iterator on a range of integers.

LOWER-FORM and UPPER-FORM are evaluated once to produce the range of
integers, [LOWER,UPPER), to iterate upon.

NEXT is defined with MACROLET.  Each evaluation of (NEXT) yields the
next successive element of the range.

SIZE is defined with MACROLET.  Each evaluation of (SIZE) yields the
length of the range.

EXHAUSTED? is defined with MACROLET?.  Each evaluation of (EXHAUSTED?)
yields a generalized boolean indicating whether the range has been
exhausted by evaluations of NEXT.

The effect of calling NEXT more times than the length of the range is
undefined."
  (jpl-util:with-gensyms (counter% upper% size%)
    `(let ((,counter% ,lower-form)
	   (,upper% ,upper-form))
       (unless (and (typep ,counter% 'integer)
		    (typep ,upper% 'integer))
	 (error "LOWER-FORM and UPPER-FORM must have integer values, ~
                 but their actual values are ~S and ~S, respectively."
		,counter% ,upper%))
       (let ((,size% (- ,upper% ,counter%)))
	 (declare (type integer ,size%))
	 (when (minusp ,size%)
	   (error "LOWER-FORM must have a value lesser than or equal ~
                   to the value of UPPER-FORM, but their actual ~
                   values are ~D and ~D, respectively."
		  ,counter% ,upper%))
	 (macrolet ((,next ()
		      '(prog1 ,counter%
			 (incf ,counter%)))
		    (,size ()
		      ',size%)
		    (,exhausted? ()
		      '(= ,counter% ,upper%)))
	   ,@body)))))
