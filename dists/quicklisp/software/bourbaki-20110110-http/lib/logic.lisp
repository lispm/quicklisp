;; Combine propositional and predicate calculus,
;; Equality and Proper Substitution,
;; "exists unique" and "exists at most one" quantification

(module "logic"
  (export [!!logic-ax])
  (providing (("equ-ax" . !/logic-ax)) ()
    (export [!!equ])
    (export [!!unique]))
)
