(in-package #:simpsamp)

(defmacro with-vector-accumulator ((set-size/element-type accumulate)
				   &body body)
  "Evaluates BODY, accumulating elements to a vector, resulting in the
vector.  The order of the elements of the resulting vector is undefined.

SET-SIZE/ELEMENT-TYPE is defined with
MACROLET.  (SET-SIZE/ELEMENT-TYPE SIZE ELEMENT-TYPE) must be evaluated
once, before accumulation begins.  SIZE dictates the size of the
resulting vector.  ELEMENT-TYPE is a designator for the type of
elements to be accumulated.  The result of accumulating an element not
of type ELEMENT-TYPE is undefined.

ACCUMULATE is defined with MACROLET.  Each time (ACCUMULATE ELEMENT)
is evaluated, ELEMENT is accumulated to the vector.  It must be
evaluated exactly the number of times as given to the
SET-SIZE/ELEMENT-TYPE form before control leaves BODY in the normal
manner.  (It's acceptable for control to leave BODY due to non-local
exits, such as GO, THROW, or a signalled condition.)"
  (jpl-util:with-gensyms (out% out-pos%)
    `(let (,out%
	   (,out-pos% 0))
       (declare (type fixnum ,out-pos%))
       (macrolet ((,set-size/element-type (size type)
		    `(setf ,',out% (make-array ,size :element-type ,type)))
		  (,accumulate (x)
		    `(progn
		       (setf (aref ,',out% ,',out-pos%) ,x)
		       (incf ,',out-pos%))))
	 ,@body)
       ,out%)))

(defmacro with-list-accumulator ((accumulate) &body body)
  "Evaluates BODY, accumulating elements to a list, resulting in the
list.  The order of the elements of the resulting list is undefined.

ACCUMULATE is defined with MACROLET.  Each time (ACCUMULATE ELEMENT)
is evaluated, ELEMENT is accumulated to the list."
  (jpl-util:with-gensyms (out%)
    `(let ((,out% '()))
       (macrolet ((,accumulate (x)
		    `(push ,x ,',out%)))
	 ,@body)
       ,out%)))

(defmacro with-hash-table-accumulator ((set-test/size accumulate)
				       &body body)
  "Evaluates BODY, accumulating keys and values to a hash-table,
resulting in the hash-table.

SET-TEST/SIZE is defined with MACROLET.  (SET-TEST/SIZE TEST SIZE)
must be evaluated once, before accumulation begins.  TEST must be an
acceptable hash-table test function for MAKE-HASH-TABLE.  SIZE must be
a non-negative integer indicating the expected final size of the
hash-table (the number of times ACCUMULATE is evaluated with a unique
KEY), which need not be accurate, or NIL to make no guess as to the
final size.

ACCUMULATE is defined with MACROLET.  Each time (ACCUMULATE KEY VALUE)
is evaluated, KEY and TABLE are added as an entry to the hash-table,
replacing any previous entry with the same KEY."
  (jpl-util:with-gensyms (out%)
    `(let (,out%)
       (macrolet ((,set-test/size (test size)
		    `(setf ,',out% (make-hash-table :test ,test
						    ,@(unless (null size)
							`(:size ,size)))))
		  (,accumulate (key value)
		    `(setf (gethash ,key ,',out%) ,value)))
	 ,@body)
       ,out%)))
