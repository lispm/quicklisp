;; Copyright © 2016, cage
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:

;;     * Redistributions of source code must retain the above copyright
;; notice, this list of conditions and the following disclaimer.
;;     * Redistributions in binary form must reproduce the above copyright
;; notice, this list of conditions and the following disclaimer in the
;; documentation and/or other materials provided with the distribution.

;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
;; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
;; THE POSSIBILITY OF SUCH DAMAGE.

(cl:defpackage :cl-kanren-test
  (:use
   :cl
   :alexandria
   :clunit)
  (:export
   :tests-suite
   :define-kanren-test
   :run-tests))

(cl:defpackage :mu-kanren-test
  (:use
   :cl
   :alexandria
   :clunit
   :mu-kanren
   :cl-kanren-test)
  (:export))

(cl:defpackage :mu-kanren-goodies-test
  (:use
   :cl
   :alexandria
   :clunit
   :mu-kanren
   :mu-kanren-goodies
   :cl-kanren-test)
  (:export))

(cl:defpackage :mini-kanren-test
  (:use
   :cl
   :alexandria
   :clunit
   :mu-kanren
   :mu-kanren-goodies
   :mini-kanren
   :cl-kanren-test)
  (:export))

(cl:defpackage :mini-kanren-test-old
  (:use
   :cl
   :alexandria
   :clunit
   :cl-kanren
   :cl-kanren-test)
  (:export))
