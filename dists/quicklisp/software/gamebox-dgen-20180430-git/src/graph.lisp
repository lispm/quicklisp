(in-package :box.dgen)

(defstruct (graphs (:conc-name nil)
                   (:constructor %make-graphs))
  connectable
  mst
  final)

(defun get-graph (stage type)
  (funcall type (graphs stage)))

(defun connectable-nodes ()
  (loop :for x :from 1 :to *region*
        :collect x))

(defun connectable-edges (connections)
  (let ((edges))
    (maphash
     (lambda (k v)
       (declare (ignore v))
       (push (cons k 1) edges))
     connections)
    edges))

(defun make-connectable (connections)
  (graph:populate (make-instance 'graph:graph)
                  :nodes (connectable-nodes)
                  :edges-w-values (connectable-edges connections)))

(defun make-mst (stage connectable)
  (graph:minimum-spanning-tree
   connectable
   (graph:populate (make-instance 'graph:graph)
                   :nodes (list (genie:gen :elt (generator stage)
                                           :sequence (graph:nodes connectable))))))

(defun make-graphs (stage connections)
  (let* ((connectable (make-connectable connections))
         (mst (make-mst stage connectable)))
    (setf (%graphs stage) (%make-graphs :connectable connectable
                                        :mst mst
                                        :final (graph:copy mst)))))
