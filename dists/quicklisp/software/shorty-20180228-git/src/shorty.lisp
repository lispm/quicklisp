(in-package :shorty)

(defvar *input-buffer* nil)

(defun encode-data (data)
  "Encode DATA using the Base64 encoding scheme."
  (cl-base64:usb8-array-to-base64-string data))

(defun decode-data (data)
  "Decode DATA encoded in the Base64 encoding scheme."
  (cl-base64:base64-string-to-usb8-array data))

(defun compress-data (data)
  "Compress DATA using the DEFLATE compression scheme."
  (salza2:compress-data data 'salza2:deflate-compressor))

(defun uncompress-data (data)
  "Uncompress DATA encoded in the DEFLATE compression scheme."
  (chipz:decompress nil 'chipz:deflate data))

(defun get-path-component (url)
  "Get the path component of URL."
  (quri:uri-path (quri:uri url)))

(defun get-redirect (url)
  "Get the 301 redirection target from the header of URL."
  (handler-bind ((dex:http-request-not-found #'dex:ignore-and-continue))
    (nth-value 3 (dex:head url))))

(defun get-redirect-path (url)
  "Get the path component of the 301 redirection target of URL."
  (get-path-component (get-redirect url)))

(defun generate-chunk-size (service)
  "Generate a random chunk size within 25% of the maximum chunk size allowed for SERVICE."
  (let* ((max-size (getf (service-properties service) :max-chunk-size))
         (min-size (floor (* max-size 0.75))))
    (+ min-size (random (1+ (- max-size min-size))))))

(defgeneric generate-host (type))

(defmethod generate-host ((type (eql :random)))
  "Generate a URL prefix with a random 4-character domain name under the .com TLD, and a random
4-character sub-domain, which will prefix the encoded data in a URL shortening service query, in
order to mimic an real website link."
  (let ((chars "abcdefghijklmnopqrstuvwxyz0123456789")
        (host (make-string 8)))
    (dotimes (i 8)
      (setf (aref host i) (aref chars (random (length chars)))))
    (setf (aref host 3) #\.)
    (format nil "http://~A.com/" host)))

(defmethod generate-host ((type (eql :google)))
  "Generate a URL prefix using Google's domain name. This is a workaround for bugs present in both
drakma and dexador HTTP client libraries."
  "http://google.com/")

(defun build-query (service data)
  "Build a query URL string for SERVICE and the encoded DATA."
  (destructuring-bind (&key url path max-chunk-size encode-data) (service-properties service)
    (concatenate 'string
                 url
                 path
                 (generate-host :google)
                 (if encode-data
                     (quri:url-encode data)
                     data))))

(defun shorten-data (service data)
  "Query the specified SERVICE for a short URL that redirects to a long URL storing the encoded
DATA."
  (let ((url (dex:get (build-query service data))))
    (subseq (get-path-component url) 1)))

(defun read-chunk (service data)
  "Read a chunk of DATA as a string, with a size up to the maximum size supported by SERVICE."
  (let* ((max-size (generate-chunk-size service))
         (octets (fast-io:make-octet-vector
                  (min max-size
                       (- (length data)
                          (fast-io:buffer-position *input-buffer*))))))
    (fast-io:fast-read-sequence octets *input-buffer*)
    (babel:octets-to-string octets)))

(defun finalize-shorten (service chunks)
  "Shortens all shortened URL CHUNKS into a single short URL for the specified SERVICE."
  (let ((merged (format nil "~{~A~^/~}" chunks)))
    (format nil "~A/~A"
            (getf (service-properties service) :url)
            (shorten-data service merged))))

(defun shorten-chunks (service data)
  "Split DATA into chunks, collecting a list of shortened URL's of each chunk, for the specified
SERVICE."
  (fast-io:with-fast-input (*input-buffer* (babel:string-to-octets data))
    (loop :for chunk = (read-chunk service data)
          :until (zerop (length chunk))
          :collect (shorten-data service chunk) :into chunks
          :finally (return (finalize-shorten service chunks)))))

(defun shorten-file (file &key (service :tinyurl))
  "Shorten the file corresponding to the pathname FILE, producing a single short URL which encodes
all of its data."
  (with-open-file (in file :element-type '(unsigned-byte 8))
    (let ((data (fast-io:make-octet-vector (file-length in))))
      (read-sequence data in)
      (shorten-chunks service (encode-data (compress-data data))))))

(defun resolve-chunk-urls (url)
  "Resolve all the chunks contained in the short URL URL, returning a list of more short URL's."
  (let* ((url-object (quri:uri url))
         (prefix (format nil "~A://~A/"
                         (quri:uri-scheme url-object)
                         (quri:uri-host url-object))))
    (mapcar
     (lambda (x) (concatenate 'string prefix x))
     (split-sequence:split-sequence
      #\/
      (get-redirect-path url)
      :remove-empty-subseqs t))))

(defun rejoin-chunks (url)
  "Rejoin all chunks encoded in the short URL URL, returning a DEFLATE-compressed, Base64-encoded
string."
  (loop :for url :in (resolve-chunk-urls url)
        :for chunk = (subseq (get-redirect-path url) 1)
        :collect chunk :into chunks
        :finally (return (format nil "~{~A~}" chunks))))

(defun unshorten-chunks (url)
  "Unshorten the data stored in URL, returning an octet vector."
  (uncompress-data
   (decode-data
    (rejoin-chunks url))))

(defun unshorten-file (url &key file)
  "Unshorten the file stored in URL, storing the result to disk in the pathname corresponding to
FILE."
  (with-open-file (out file
                       :direction :output
                       :element-type '(unsigned-byte 8)
                       :if-does-not-exist :create
                       :if-exists :supersede)
    (write-sequence (unshorten-chunks url) out)
    (values)))
