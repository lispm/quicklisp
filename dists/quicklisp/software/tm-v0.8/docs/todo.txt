 
1. make tape inherited by tape machine.  A number of interface functions
   work on just tapes,  e.g.  a◧ (now epa).   It is tape which has an
   address space.  Would be good to introduce it sooner than status machine,
   though its appears to be optional, a tape transform.

   looks like we will need a second order tape-status so that tapes can
   be empty.  The tape machine will not make use of tape-status, but will
   implement its own status.  Or maybe it should make use of tape status
   .. hmm.

   when we have tapes as independent objects, we can create tape
   machines by 'mounting' them.  We can expand the :tape keyword
   in the current intialization list to accomodate this.  Perhaps
   replace #'mk with #'mount ?

2. access lang needs to be finished, also chapter needs to be added
to the manual.  .. not sure what we need access lang for, we already
have Lisp and it can do the same thing ;-)

4. need to write mn and m*, current m* as repeating m is very inefficient

7. right now tm-difference-engine is only linear, need to finish implementation
   tm-line -> tm-difference-engine .. will rename it tm-sums
   (really it sums a finite difference to recover the original sequence)

   need more generators

8. ... need tm on a stream, also the utf8 converters --> tm needs to re

10. should modify #'L, i.e. {},  to recognize repeated symbols and only evaluate
    them once.  This could be done by wrapping the current #'L with a let.

    wonder, .. square brackets to make machines  and curly for lists?
    or square brackets for vectors ...  quote ... really?

11. need to add name spaces for test-hook, and then have (test-all) run everything
independent of the names space, and (test-all 'space) to run only the given name
space. also if one runs load twice all the tests double up.  something should
be done so that only the most recent version plays.

13. affine transform

22. no-alloc continuations need to be implemented ** fairly serious, though
   we haven't pulled in the heap model from the C++ version yet, if ever for
   this version.

23. add something to the manual about functional programming (state machines),  state
variables,  and transactional behavior for tm functions (so that state doesn't change
when state transitions are not taken) .. about region and cell addresses, note
copy to new list doe snot have the problem and it is also transactional

29. some sort of issue with mount and subspaces ... should descend into subspaces (not leave any tms)

    ...add a manifold machine type, note the comments currently in tm-region.lisp.

    note doc on subspaces, pulled them out, need to be reimplemented

32.
  ;; behavior is not transactional, all machines update even when one or more cont-not-supported
  ;; need to check support first
  (defun ∀-entanglements-d◧-0 (tm cont-ok cont-not-supported)

  all api functions should either be transactional or return state for continuing
  .. I don't think we quite have that yet

33. change the quantifiers so that the first parms are optional, the first parm is
   the object, and the second is the machine .. there is something a little funny
   about passing the list into the predicate, as usually we already have the list,
   hence would just like to leave the predicate without parms ... but sometimes
   the list is created with an expression, so then it is useful to have it passed
   in.  Most predicates don't need the list, they just want the read object.
   key parms?

   difficulty being the lambda is where the optional keyword would be placed,
   and that is typed by the library user. We would have be able to check the
   lambda passed in to see how many parms it takes.

35. init should fail if there are unused keys. init functions that 'call-next-method'
  need to renove their keys, so that the more general init can detect unused
  keys.  (wrote remove key, it is in src0)

  actually this whole thing with key parameters is a bit muddled. It seems
  we need to settle on a set of parameters and remove the &allow-other-keys.
  functions that are specialized and have other parameters can include those
  in their more specific versions.  .. heck but the problem comes with functions
  that pass keyed parameter list through.

58. rename d◧ to epd (entangled copy, p, delete). 
     Then unambiguously, hp -> p h◧->◧ h◨->◨ 

     43. cue-leftmost / rightmost names updated to cue◧  and cue◨.   Perhaps c◧ and c◨.
         I don't think we will have 'copy' because that would be w* with a fill.  We
         will have move.  So c might be available.

44. add c-to works with two machines that are entangled, cues one to be at the same
    location as the other.

46. interesting situation in d◧ for ea-definitions.  We would like to enclose a call
   'with-lock-held', but we can't because we must release the lock before calling
   continuations within the 'a' for the spill, and there are multiple continuations.  So..
   instead we acquire the lock, and release it within each contiuation; however
   specializations might have their own continuations that we don't know about.
   We know about ➜ok  and ➜no-alloc, but the others are handled through the
   (o (remove-keys ...))  thing.  We don't know what they are (if they exist at all)
   and thus can not modify them by adding a lock release.  We might provide an
   standard 'always-before' continuation .. and release the lock there.  Anyway,
   need to fix this design.  For now, I'm only providing the continuations known
   for 'a' in src-list (➜ok and ➜no-alloc). 

   ... perhaps what we need is to be able to list multiple keys, followed by a single
   lambda.  keys may appear in more than one list, they execute in order.

   So for a continuation list, in the parameters list:
   {
     :a :b :c  (λ() (release lock)  .. executed when a b or c is present
     :b  (λ() .. executed after the above is executed
     :d (λ() .. runs with lock held
     }

   .. and need an else clause ..  and an any clause

   also need to build in exception handler options, because these are not going
   away in Lisp

   wonder, what of a loop with a continuations call:

      (c◧∀* (entanglements tm)
        (λ(es)
          (call-next-method (tg:weak-pointer-value (r es)) instance ➜) ...

   seems to work


49. need to turn #'cant-happen to carry a documentation string so we know
    why.

50.  ⟳ needs an optional max-iteration count continuation option, or an
  error continuation

51. tape length tests for status machines should be based on addresses as that is
faster.

55. if a test does not return in a short time, call the test a failure, perhaps
add a time to wait as part of test-hook (similar to #50 above)

56. ensemble needs something to help the programer know which machines hit
   rightmost.  Look at shallow copy for example.  After a quantifier we
   are left trying to make sense of what happened.  Perhaps an additional
   slot in ensemble that can be used to understand more about the
   ensemble.  We do have this, upon hiting rightmost, the members machine's
   head is left on the cell holding the machine that hit rightmost.

57. only have generic implementation for d*, which is rather inefficient,
    need to add implementation specific versions.  Might also give some
    consideration to the spill behavior - perhaps spill in the forward
    direction

59. see the_story_of_swap  in docs,  implement the swap trick add the
    update function to entanglement accounting, and remove the 'update-tape-after ..'
    functions.

60. would like some sort of access to the scoped context for entanglement
    copying.   Would like for (e tm)  to be an entangled copy or somee such.
    Perhaps with-entanglements defines such a scope, with a variable accessible
    in that scope that the function #'e above accesses.  Or perhaps it is
    time to implement our symbol tables,  (e st0 tm), or some such, st0 being
    the table to place the entangled copy on.

    It is safe to entangle tm-list and solo-list to a scoped non destructive machine
    within a single thread. Solo can delete cells, so the scoping assures there are no
    other heads on the tape by the time the thread gets back to the solo code.  We should
    define such entanglements so as to get our tm-print to work with all machines.

61. move tests to the export side instead of being in the package.  Give them
    a package of their own, so that they don't take up so much space in the lib

62. functions such as ◧  and p should allow more than one argument in then
    affect them all

63. In src-array need to add a bit fields type similar to tiled number, but each field can
    be a different width.  It should support random access against field index.
    We will later have a tiled-number type, so the current tiled-number type with
    an assumption of constant bit fields for a number up until the tailing zeros,
    should be renamed, and then the tiled-number should point to it until it is
    replaced. The current tiled-number starts at bit 0, with a field up to the tile
    length.  I suppose for the bit field machines they will not have to start
    at bit 0.  Or perhaps that is done with a region transform. .. yes the current
    tiled number should be somethign like 'by-digits'.  hex, octal,  and we
    need 'by-character'  ascii or unicode.  .. also need a simple array
