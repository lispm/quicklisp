
(in-package :bitfield-schema)

(defvar *ctx-pool* (make-hash-table :test 'eq))

(defclass gen-context ()
  ((prefix :type (or null symbol) :accessor prefix :initarg :prefix)
   (offset :type (or (member :skip :param) number) :accessor offset :initarg :offset :initform :skip)
   (gen-size :type (member :skip :macro :proc) :accessor gen-size :initarg :gen-size :initform :macro)
   (gen-make :type (member :skip :proc) :accessor gen-make :initarg :gen-make :initform :proc)
   (bit-field-var :type symbol :reader bit-field-var :initform (gensym "BIT-FIELD"))
   (index-var :type symbol :reader index-var :initform (gensym "INDEX"))
   (offset-var :type symbol :reader offset-var :initform (gensym "OFFSET"))
   (value-var :type symbol :reader value-var :initform (gensym "VALUE"))
   (decls :type list :accessor decls :initarg :decls :initform '())))

(defclass gen-context-decl ()
  ((name :type symbol :accessor name :initarg :name)
   (width :type (or number symbol list) :accessor width :initarg :width)
   (count :type (or null number symbol list) :accessor decl-count :initarg :count :initform nil)
   (gen-get :type (member :skip :macro :proc) :accessor gen-get :initarg :gen-get :initform :macro)
   (gen-set :type (member :skip :proc) :accessor gen-set :initarg :gen-set :initform :proc)))

(defmethod print-object ((obj gen-context) stream)
  (print-unreadable-object (obj stream :identity t :type t)
    (format stream "~a O:~a GS:~a GM:~a" (prefix obj) (offset obj) (gen-size obj) (gen-make obj))))

(defmethod print-object ((obj gen-context-decl) stream)
  (print-unreadable-object (obj stream :identity t :type t)
    (format stream "~a W:~a C:~a GG:~a GS:~a" (name obj) (width obj) (decl-count obj) (gen-get obj) (gen-set obj))))

(defun check-context-args (prefix &key offset gen-size gen-make)
  (assert (or (not offset) (numberp offset) (member offset '(:skip :param))) nil
          "OFFSET for schema~@[ ~a~] should be NIL, NUMBER, :SKIP or :PARAM" prefix)
  (assert (or (not gen-size) (member gen-size '(:skip :macro :proc))) nil
          "GEN-SIZE for schema~@[ ~a~] should be :SKIP, :MACRO or :PROC" prefix)
  (assert (or (not gen-make) (member gen-make '(:skip :proc))) nil
          "GEN-MAKE for schema~@[ ~a~] should be :SKIP or :PROC" prefix))

(defmethod const-field-p ((ctx gen-context) (field symbol) &optional rec-path)
  (assert (not (find (cons (prefix ctx) field) rec-path :test #'equal)) nil
          "Recursive reference found for field ~a schema~@[ ~a~]" field (prefix ctx))
  (and ctx
       (let ((decl (find field (decls ctx) :key #'name)))
         (and decl
              (not (decl-count decl))
              (etypecase (width decl)
                (number t)
                (symbol (const-field-p ctx (width decl) (cons (cons (prefix ctx) field) rec-path)))
                (cons (const-field-p (gethash (second (width decl)) *ctx-pool*)
                                     (third (width decl))
                                     (cons (cons (prefix ctx) field) rec-path))))))))

(defun check-number-or-ref-valid-p (prefix name field decls &key allow-nil)
  (assert (or (and allow-nil (not field))
              (numberp field)
              (symbolp field)
              (and (consp field)
                   (= (length field) 3)
                   (eq (first field) :ref)
                   (symbolp (second field))
                   (symbolp (third field))))
          nil
          "~a for field ~a in schema~@[ ~a~] should be~@[ NIL,~] NUMBER, SYMBOL or (:REF schema field)"
          field name prefix allow-nil)
  (assert (not (and (symbolp field) (getf (cdr (find field decls :key #'car)) :count)))
          nil
          "field ~a in schema~@[ ~a~] cannot use vector field ~a as value source"
          name prefix field)
  (assert (or (not (consp field))
              (let ((ext-ctx (gethash (second field) *ctx-pool*)))
                (and ext-ctx (let ((ext-field (find (third field) (decls ext-ctx) :key #'name)))
                               (and ext-field (const-field-p ext-ctx (third field)))))))
          nil
          "~a for field ~a in schema~@[ ~a~] cannot use external field ~a from scheme ~a as value source"
          field name prefix (third field) (second field)))

(defun check-context-decl-args (prefix decls name &key width count gen-get gen-set)
  (assert name nil "No field name given for schema~@[ ~a~]" prefix)
  (assert (or (not width) (numberp width) (symbolp width)) nil
          "WIDTH for field ~a in schema~@[ ~a~] should be NUMBER or SYMBOL" name prefix)
  (assert (or (not count) (numberp count) (symbolp count)) nil
          "COUNT for field ~a in schema~@[ ~a~] should be NIL, NUMBER or SYMBOL" name prefix)
  (assert (or (not gen-get) (member gen-get '(:skip :macro :proc))) nil
          "GEN-GET for field ~a in schema~@[ ~a~] should be :SKIP, :MACRO or :PROC" name prefix)
  (assert (or (not gen-set) (member gen-set '(:skip :macro :defun :proc))) nil
          "GEN-SET for field ~a in schema~@[ ~a~] should be :SKIP, or :PROC" name prefix)
  (assert (not (and (or (not gen-get) (eq gen-get :macro)) (not (numberp width)))) nil
          "GEN-GET for field ~a in schema~@[ ~a~] can be of :MACRO type only if WIDTH is a NUMBER, but not ~a" name prefix width)
  (assert (not (and (symbolp width) (eq width name))) nil
          "WIDTH for field ~a in schema~@[ ~a~] cannot use itself as value source" name prefix)
  (assert (not (and (symbolp count) (eq count name))) nil
          "COUNT for field ~a in schema~@[ ~a~] cannot use itself as value source" name prefix)
  (check-number-or-ref-valid-p prefix name width decls)
  (check-number-or-ref-valid-p prefix name count decls :allow-nil t))

(defun make-context (prefix context-keys declarations)
  (apply #'check-context-args prefix context-keys)
  (let ((ctx (apply #'make-instance 'gen-context :prefix prefix context-keys)))
    (setf (decls ctx)
          (iter (for (decl-name . decl-keys) in declarations)
                (apply #'check-context-decl-args prefix declarations decl-name decl-keys)
                (collect (apply #'make-instance 'gen-context-decl :name decl-name decl-keys))))
    ctx))


(defmethod gen-get-value-form ((ctx gen-context) (field symbol))
  (let ((decl (find field (decls ctx) :key #'name)))
    (assert decl nil "Cannot find field ~a within schema~@[ ~a~]" field (prefix ctx))
    (ecase (gen-get decl)
      (:skip (error "Cannot use field with getter type ~a as value source in schema~@[ ~a~]" field (prefix ctx)))
      ((:macro :proc) (append (gen-getter-name ctx decl)
                              (mapcar (lambda (var) (list :keep var)) (car (gen-getter-args ctx decl))))))))

(defmethod gen-set-value-form ((ctx gen-context) (field symbol))
  (let ((decl (find field (decls ctx) :key #'name)))
    (assert decl nil "Cannot find field ~a within schema~@[ ~a~]" field (prefix ctx))
    (ecase (gen-set decl)
      (:skip (error "Cannot use field with setter type ~a as value source in schema~@[ ~a~]" field (prefix ctx)))
      (:proc (append (gen-setter-name ctx decl)
                     (mapcar (lambda (var) (list :keep var)) (car (gen-setter-args ctx decl))))))))

(defmethod gen-field-shift ((ctx gen-context) (decl gen-context-decl) adjust-const adjust-form)
  (flet ((gen-by-ref (ref)
           (etypecase ref
             (symbol (gen-get-value-form ctx ref))
             (cons (gen-get-value-form (gethash (second ref) *ctx-pool*) (third ref))))))
    (etypecase (decl-count decl)
      (null (etypecase (width decl)
              (number (funcall adjust-const (width decl)))
              ((or symbol cons) (funcall adjust-form (gen-by-ref (width decl))))))
      (number (etypecase (width decl)
                (number (funcall adjust-const (* (decl-count decl) (width decl))))
                ((or symbol cons) (funcall adjust-form (gen-multiply (gen-by-ref (width decl)) (decl-count decl))))))
      ((or symbol cons) (etypecase (width decl)
                          (number (funcall adjust-form (gen-multiply (gen-by-ref (decl-count decl)) (width decl))))
                          ((or symbol cons) (funcall adjust-form (list '* (gen-by-ref (width decl)) (gen-by-ref (decl-count decl))))))))))

(defmethod gen-offset-form ((ctx gen-context) (decl gen-context-decl) &key skip-offset)
  (iter (for ctx-decl in (decls ctx))
        (when (eq (name ctx-decl) (name decl))
          (return-from gen-offset-form
            (apply #'gen-add
                   const-offset
                   (if skip-offset
                       0
                       (if (numberp (offset ctx))
                           (offset ctx)
                           (ecase (offset ctx)
                             (:skip 0)
                             (:param (list :keep (offset-var ctx))))))
                   form-offset)))
        (gen-field-shift ctx
                         ctx-decl
                         (lambda (const-inc)
                           (summing const-inc into const-offset))
                         (lambda (new-form)
                           (collect new-form into form-offset)))))

(defmethod gen-resolve-value ((ctx gen-context) (decl gen-context-decl) reader)
  (etypecase (funcall reader decl)
    (number (funcall reader decl))
    (symbol (gen-get-value-form ctx (funcall reader decl)))
    (cons (gen-get-value-form (gethash (second (funcall reader decl)) *ctx-pool*) (third (funcall reader decl))))))

(defmethod gen-offset-form-adjust ((ctx gen-context) (decl gen-context-decl))
  (if (decl-count decl)
      (gen-add (gen-offset-form ctx decl) (gen-multiply (list :keep (index-var ctx)) (gen-resolve-value ctx decl #'width)))
      (gen-offset-form ctx decl)))

(defmethod gen-getter-def ((ctx gen-context) (decl gen-context-decl))
  (ecase (gen-get decl)
    (:skip nil)
    (:macro '(defmacro))
    (:proc '(defun))))

(defmethod gen-getter-id ((ctx gen-context) (decl gen-context-decl))
  (format nil "~@[~a-~]~a" (prefix ctx) (name decl)))

(defmethod gen-getter-name ((ctx gen-context) (decl gen-context-decl))
  (let ((procname (intern (gen-getter-id ctx decl))))
    (ecase (gen-get decl)
      (:skip nil)
      (:macro (list procname))
      (:proc (list procname)))))

(defmethod gen-common-base-args ((ctx gen-context) &key gen-value)
  (let ((args '()))
    (when gen-value
      (push (value-var ctx) args))
    (push (bit-field-var ctx) args)
    (when (eq (offset ctx) :param)
      (push (offset-var ctx) args))
    (list (nreverse args))))

(defmethod gen-common-args ((ctx gen-context) (decl gen-context-decl) &key (gen-index t) gen-value)
  (let ((args (gen-common-base-args ctx :gen-value gen-value)))
    (list (append (car args)
                  (when (and (decl-count decl) gen-index)
                    (list (index-var ctx)))))))

(defmethod gen-getter-args ((ctx gen-context) (decl gen-context-decl))
  (unless (eq (gen-get decl) :skip)
    (gen-common-args ctx decl)))

(defun real-width (ctx field)
  (let ((decl (find field (decls ctx) :key #'name)))
    (assert decl nil "Cannot find value source ~a in schema~@[ ~a~]" field (prefix ctx))
    (etypecase (width decl)
      (number (width decl))
      (symbol (ash 1 (real-width ctx (width decl))))
      (cons (ash 1 (real-width (gethash (second (width decl)) *ctx-pool*) (third (width decl))))))))

(defmethod gen-param-type ((ctx gen-context) (decl gen-context-decl))
  `(integer 0 ,(1- (ash 1 (real-width ctx (name decl))))))

(defmethod gen-base-param-types ((ctx gen-context))
  (let ((decls `((type (simple-bit-vector *) ,(bit-field-var ctx)))))
    (when (eq (offset ctx) :param)
      (push `(type (integer 0 ,most-positive-fixnum) ,(offset-var ctx)) decls))
    (nreverse decls)))

(defmethod gen-param-types ((ctx gen-context) (decl gen-context-decl) &key set-value)
  (let ((decls (gen-base-param-types ctx)))
    (when set-value
      (push `(type ,(gen-param-type ctx decl) ,(value-var ctx)) decls))
    (append decls
            (etypecase (decl-count decl)
              (null nil)
              (number `((type (integer 0 ,(1- (decl-count decl))) ,(index-var ctx))))
              ((or symbol cons) `((type (integer 0 ,(1- (ash 1 (real-width ctx (decl-count decl))))) ,(index-var ctx))))))))
  
(defmethod gen-getter-body ((ctx gen-context) (decl gen-context-decl))
  (ecase (gen-get decl)
    (:skip nil)
    (:macro `((list 'get-integer/fixed ,(bit-field-var ctx) ,(gen-quasiquote (gen-offset-form-adjust ctx decl)) ,(width decl))))
    (:proc
     `((declare (optimize (speed 3) (safety 0)) ,@(gen-param-types ctx decl))
       (get-integer ,(bit-field-var ctx)
                    ,(gen-noquote (gen-offset-form-adjust ctx decl))
                    ,(gen-resolve-value ctx decl #'width))))))
    
(defun gen-getter (ctx decl)
  (append (gen-getter-name ctx decl)
          (gen-getter-args ctx decl)
          (gen-getter-body ctx decl)))

(defmethod gen-setter-def ((ctx gen-context) (decl gen-context-decl))
  (ecase (gen-set decl)
    (:skip nil)
    (:proc '(defun))))

(defmethod gen-setter-id ((ctx gen-context) (decl gen-context-decl))
  (format nil "SET-~@[~a-~]~a" (prefix ctx) (name decl)))
  
(defmethod gen-setter-name ((ctx gen-context) (decl gen-context-decl))
  (let ((procname (intern (gen-setter-id ctx decl))))
    (ecase (gen-set decl)
      (:skip nil)
      (:proc (list procname)))))

(defmethod gen-setter-args ((ctx gen-context) (decl gen-context-decl))
  (unless (eq (gen-set decl) :skip)
    (gen-common-args ctx decl :gen-value t)))

(defmethod gen-setter-body ((ctx gen-context) (decl gen-context-decl))
  (ecase (gen-set decl)
    (:skip nil)
    (:proc
     `((declare (optimize (speed 1) (safety 3)) ,@(gen-param-types ctx decl :set-value t))
       (put-integer/grow ,(value-var ctx)
                         ,(bit-field-var ctx)
                         ,(gen-noquote (gen-offset-form-adjust ctx decl))
                         ,(gen-resolve-value ctx decl #'width))))))

(defun gen-setter (ctx decl)
  (append (gen-setter-name ctx decl)
          (gen-setter-args ctx decl)
          (gen-setter-body ctx decl)))

(defmethod gen-size-id ((ctx gen-context))
  (format nil "~@[~a-~]SIZE" (prefix ctx)))

(defmethod gen-size-proc ((ctx gen-context))
  (let ((procname (intern (gen-size-id ctx))))
    (multiple-value-bind (prelude modificator)
        (ecase (gen-size ctx)
          (:skip nil)
          (:macro (values `(,procname (,(bit-field-var ctx))) #'gen-quasiquote))
          (:proc (values `(,procname (,(bit-field-var ctx))) #'gen-noquote)))
      (when prelude
        (append prelude
                `((declare (ignorable ,(bit-field-var ctx))))
                (list (funcall modificator (gen-offset-form ctx (car (last (decls ctx))) :skip-offset t))))))))

(defmethod gen-make-id ((ctx gen-context))
  (format nil "MAKE~@[-~a~]" (prefix ctx)))

(defmethod gen-make-proc ((ctx gen-context))
  (let* ((procname (intern (gen-make-id ctx)))
         (prelude (ecase (gen-make ctx)
                    (:skip nil)
                    (:proc (list procname))))
         (args (iter (for decl in (decls ctx))
                     (when (eq (gen-set decl) :proc)
                       (collect (list (name decl) decl (gen-param-type ctx decl)))))))
    (flet ((arg-type (decl elt-type)
             (append '(or null)
                     (etypecase (decl-count decl)
                       (null (list elt-type))
                       (number `((vector * ,(decl-count decl)) list (simple-array ,elt-type (,(decl-count decl)))))
                       ((or symbol cons) `(vector list (simple-array ,elt-type *)))))))
      (when prelude
        (append prelude `((,@(car (gen-common-base-args ctx)) &key ,@(mapcar #'first args)))
                `((declare (optimize (speed 1) (safety 3))
                           ,@(gen-base-param-types ctx)
                           ,@(iter (for (arg decl type) in args)
                                   (collect `(type ,(arg-type decl type) ,arg)))))
                (iter (for (arg decl type) in args)
                      (for set-form = (gen-set-value-form ctx arg))
                      (for full-form = (etypecase (decl-count decl)
                                         (null `(let ((,(value-var ctx) ,arg)) ,set-form))
                                         ((or number symbol cons)
                                          `(loop :for ,(index-var ctx)
                                              :from 0
                                              :below ,(typecase (decl-count decl) (number (decl-count decl)) ((or symbol cons) (gen-resolve-value ctx decl #'decl-count)))
                                              :for ,(value-var ctx) = (elt ,arg ,(index-var ctx))
                                              :do ,set-form))))
                      (collect (list `(when ,arg ,(gen-noquote full-form))
                                     (etypecase (decl-count decl) (null 0) ((or number symbol cons) 1)))
                        into forms)
                      (finally (return (mapcar #'first (sort forms #'< :key #'second)))))
                (list (bit-field-var ctx)))))))

(defun gen-toplevel (type form)
  (ecase type
    (:macro (cons 'defmacro form))
    (:proc (cons 'defun form))
    (:skip form)))

(defmacro defbitfield-schema (prefix (&rest schema-keys) &rest declarations)
  "Given the virtual markup of a simple-bit-vector bitfield generate effective
accessors for them.

Example:
\(defbitfield-schema tree-node \(:offset offt\)
  \(disabled-p :width 1\)
  \(values :width 16 :count 10\)
  \(left-child :width 24\)
  \(right-child :width 7\)\)

:OFFSET parameter could be a number constant, a variable or could be omited
at all.
"
  (let ((ctx (make-context prefix schema-keys declarations)))
    `(progn
       ,@(iter (for decl in (decls ctx))
               (collect (gen-toplevel (gen-get decl) (gen-getter ctx decl)))
               (collect (gen-toplevel (gen-set decl) (gen-setter ctx decl))))
       ,(gen-toplevel (gen-size ctx) (gen-size-proc ctx))
       ,(gen-toplevel (gen-make ctx) (gen-make-proc ctx)))))

(defmacro with-bitfield-schema (((&rest schema-keys) &rest declarations) &body body)
  (let ((ctx (make-context nil schema-keys declarations))
        (forms-macro '())
        (forms-proc '()))
    (flet ((collect-form (type form)
             (ecase type
               (:macro (push form forms-macro))
               (:proc (push form forms-proc))
               (:skip nil)))
           (wrap-macros (body-form)
             (if forms-macro
                 `(macrolet ,forms-macro ,body-form)
                 body-form))
           (wrap-procs (body-form)
             (if forms-proc
                 `(labels ,forms-proc ,body-form)
                 body-form)))
      (iter (for decl in (decls ctx))
            (collect-form (gen-get decl) (gen-getter ctx decl))
            (collect-form (gen-set decl) (gen-setter ctx decl)))
      (collect-form (gen-size ctx) (gen-size-proc ctx))
      (collect-form (gen-make ctx) (gen-make-proc ctx))
      (wrap-macros (wrap-procs `(progn ,@body))))))
    
;; (defbitfield-schema tree-node (:offset :param :gen-make :proc :gen-size :macro)
;;   (disabled-p :width 1 :gen-get :macro :gen-set :proc)
;;   (values :width 16 :count 10 :gen-get :macro :gen-set :proc)
;;   (left-child :width 24 :gen-get :macro :gen-set :proc)
;;   (right-child :width 7 :gen-get :macro :gen-set :proc))

;; (with-bitfield-schema ((:offset :param :gen-make :proc :gen-size :macro)
;;                        (disabled-p :width 1 :gen-get :macro :gen-set :proc)
;;                        (values :width 16 :count 10 :gen-get :macro :gen-set :proc)
;;                        (left-child :width 24 :gen-get :macro :gen-set :proc)
;;                        (right-child :width 7 :gen-get :macro :gen-set :proc))
;;   (+ 1 2))

