;;;  lispworks.lisp --- output module for LispWorks FFI

;;;  Copyright (C) 2004 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-624DC41BE4A49199EECBEC8A4F010349.lisp,v 1.13 2004/04/09 10:48:38 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-624DC41BE4A49199EECBEC8A4F010349.lisp,v 1.13 2004/04/09 10:48:38 wcp Exp $")

(in-package :binge-backend)

(defclass lispworks-backend (backend) ())

(defmethod use-packages ((backend lispworks-backend))
  (declare (ignore backend))
  '("COMMON-LISP" "FLI"))

(defmethod ffi-sizeof (thing (backend lispworks-backend))
  (format nil "(size-of '~A)" thing))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod c-object->ffi-notation ((object c-argument) (backend lispworks-backend))
  (c-object->ffi-notation (c-type object) backend))

(defmethod c-object->ffi-notation ((object c-slot) (backend lispworks-backend))
  (format nil "(~A ~A)"
	  (c-name object)
	  (c-object->ffi-notation (c-type object) backend)))

(defmethod c-object->ffi-notation ((object c-pointer) (backend lispworks-backend))
  (case (c-type object)
    ((:char :unsigned-char)
     ;; this magic spell is lifted from UFFI
     "(:reference-pass (:ef-mb-string :external-format (:latin-1 :eol-style :lf)) :allow-null t)")
    (t
     (format nil "(:pointer ~A)" (c-object->ffi-notation (c-type object) backend)))))

(defmethod c-object->ffi-notation ((object c-user-type-ref) (backend lispworks-backend))
  (c-object-lisp-name object backend))

(defmethod c-object->ffi-notation ((object c-struct-ref) (backend lispworks-backend))
  (format nil "(:struct ~A)" (c-object-lisp-name object backend)))

(defmethod c-object->ffi-notation ((object c-union-ref) (backend lispworks-backend))
  (format nil "(:union ~A)" (c-object-lisp-name object backend)))

(defmethod c-object->ffi-notation ((object c-enum-ref) (backend lispworks-backend))
  (format nil "(:enum ~A)" (c-object-lisp-name object backend)))

(defmethod c-object->ffi-notation ((object c-function) (backend lispworks-backend))
  (format nil "(:function ~A ~A)"
	  (mapcar #'(lambda (arg)
		      (c-object->ffi-notation arg backend))
		  (c-arguments object))
	  (c-object->ffi-notation (c-type object) backend)))

(defmethod c-object->ffi-notation ((object c-array) (backend lispworks-backend))
  (format nil "(:c-array ~A~{ ~A~})"
	  (c-object->ffi-notation (c-type object) backend)
	  (convert-array-dimensions (c-dimensions object) backend)))

(defmethod c-object->ffi-notation ((object symbol) (backend lispworks-backend))
  (case object
    (:void ":void")
    (:char ":char")
    (:short ":short")
    (:int ":int")
    (:long ":long")
    (:unsigned-char "(:unsigned :char)")
    (:unsigned-short "(:unsigned :short)")
    (:unsigned-int "(:unsigned :int)")
    (:unsigned-long "(:unsigned :long)")
    (:float ":float")
    (:double ":double")
    (t
     (error 'binge-compiler:compilation-unsupported-error
	    :item object))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod c-object->ffi-notation ((decl c-typedef-declaration) (backend lispworks-backend))
  (format nil "(define-c-typedef ~A ~A)"
	  (c-object-lisp-name decl backend)
	  (c-object->ffi-notation (c-type decl) backend)))

(defun lispworks-format-compound (type declaration backend)
  (format nil "(define-c-~A ~A~%  ~<~@{~A~^ ~_~}~:>)"
	  type
	  (c-object-lisp-name declaration backend)
	  (mapcar #'(lambda (slot)
		      (c-object->ffi-notation slot backend))
		  (c-slots declaration))))

(defmethod c-object->ffi-notation ((decl c-struct-declaration) (backend lispworks-backend))
  (lispworks-format-compound "struct" decl backend))

(defmethod c-object->ffi-notation ((decl c-union-declaration) (backend lispworks-backend))
  (lispworks-format-compound "union" decl backend))

(defmethod c-object->ffi-notation ((decl c-enum-declaration) (backend lispworks-backend))
  (format nil "(define-c-enum enum ~A~%  ~<~@{~A~^ ~:_~}~:>)"
	  (c-object-lisp-name decl backend)
	  (mapcar #'(lambda (c)
		      (if (consp c)
			  (format nil "(:~@[~A~]~A ~A)" (backend-prefix backend) (car c) (cadr c))
			  (format nil ":~@[~A~]~A" (backend-prefix backend) c)))
		  (c-constants decl))))

(defmethod c-object->ffi-notation ((decl c-function-declaration) (backend lispworks-backend))
  (format nil "(define-foreign-function (~A ~S)~@
		~3T~:<~@{~A~^ ~:_~}~:>~@
		~3T:result-type ~A~
		~@[~&~3T:module ~S~]~
		~@[~&~3T:documentation ~S~])"
	  (c-object-lisp-name decl backend)
	  (c-name decl)
	  (loop
	     for arg in (c-arguments decl)
	     for i from 1 by 1
	     collect (if (c-name arg)
			 (format nil "(~A ~A)" (c-name arg)
				 (c-object->ffi-notation (c-type arg) backend))
			 ;; the use of a dash in the name of arguments
			 ;; guarantees we won't have any name clash
			 (format nil "(arg-~A ~A)" i
				 (c-object->ffi-notation (c-type arg) backend))))
	  (c-object->ffi-notation (c-type decl) backend)
	  (or (c-library decl)
	      (backend-library backend))
	  (c-documentation decl)))

(defmethod c-object->ffi-notation ((decl c-variable-declaration) (backend lispworks-backend))
  (format nil "(define-foreign-variable (~A ~S) :type ~A~
		~@[~&~3T:module ~S~])"
	  (c-object-lisp-name decl backend)
	  (c-name decl)
	  (c-object->ffi-notation (c-type decl) backend)
	  (or (c-library decl)
	      (backend-library backend))))

(setf binge-compiler:*backend* (make-instance 'lispworks-backend))
(export 'lispworks-backend)		     ; so that we can use this backend
