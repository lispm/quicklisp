;;;  ods4cl.asd --- System description for the Open Document Spreadsheet library

;;;  Copyright (C) 2006, 2007 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: ods4cl

#+cmu (ext:file-comment "$Module: ods4cl.asd, Time-stamp: <2007-01-24 01:02:09 wcp> $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

(defpackage :ods4cl-system
  (:use :common-lisp :asdf #+asdfa :asdfa)
  (:export #:*base-directory*
	   #:*compilation-epoch*))

(in-package :ods4cl-system)

(defsystem ods4cl
    :name "ODS4CL"
    :author "Walter C. Pelissero <walter@pelissero.de>"
    :maintainer "Walter C. Pelissero <walter@pelissero.de>"
    :description "Open Document Spreadsheet library"
    :long-description
    "This is package provides some primitives to create
spreadsheets in Open Document format, the one digestible by
OpenOffice and other modern spreadsheet programmes."
    :licence "LGPL"
    :depends-on (:sclf)
    :components
    ((:file "ods4cl")))
