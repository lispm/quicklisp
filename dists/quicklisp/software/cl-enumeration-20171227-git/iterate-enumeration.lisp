;;;; -*- Mode: Lisp -*-

;;;; iterate-enumeration.lisp --
;;;; ITERATE extension for ENUMERATIONs.
;;;;
;;;; See file COPYING for copyright and licensing information.

(require :iterate)

(in-package "ITERATE")

(defmacro-driver (FOR var OVER enum)
  (let ((e (gensym "ENUM-"))
	(et (gensym "ENUM-"))
        (kwd (if generate 'generate 'for))
        )
    `(progn
       (with ,e = ,enum)
       (with ,e = (let ((,et ,enum))
		    (if (enum:enumerationp ,et)
			,et
			(enum:enumerate ,et))))
       (,kwd ,var next (if (enum:has-more-elements-p ,e)
                           (enum:next ,e)
                           (terminate))))))

;;;; end of file -- iterate-enumeration.lisp --
