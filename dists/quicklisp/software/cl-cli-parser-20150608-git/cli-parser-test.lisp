;;;; $Id: cli-parser-test.lisp,v 1.4 2005/03/19 23:08:23 dbueno Exp $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Denis Bueno
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Thorough test of cli-parser.lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :cli-parser)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (use-package :lunit))

(defparameter *option-conf*
  (list (make-instance 'cli-option
                       :abbr "t"
                       :full "use-threads"
                       :requires-arguments :optional
                       :description "Whether the application should using threads"
                       :example "--use-threads[=5]")
        (make-instance 'cli-option
                       :abbr nil
                       :full "root-dir"
                       :requires-arguments t
                       :description "The location of the root directory"
                       :example "--root-dir=/tmp")
        (make-instance 'cli-option
                       :abbr "e"
                       :full "extension-list"
                       :requires-arguments t
                       :description "The list of extensions to include from the root directory (see option root-dir)"
                       :example "--extension-list=txt[,jpg,jpeg,pdf]")))

(deftest test-opt-p ()
  (check (opt-p "-o"))
  (check (opt-p "--an-option"))
  (check (not (opt-p 'k))))

(deftest test-abbr-opt-p ()
  (check (abbr-opt-p "-o"))
  (check (abbr-opt-p "-j=123"))
  (check (abbr-opt-p "-j 123"))
  (check (abbr-opt-p "-k=opt1 opt2 opt3"))
  (check (abbr-opt-p "-k opt1 opt2 opt3"))
  (check (abbr-opt-p "-k=op1,op2"))
  (check (abbr-opt-p "-k op1,op2"))
  (check (not (abbr-opt-p "-crazy-cartoon")))
  (check (not (abbr-opt-p "--kilimanjaro")))
  (check (not (abbr-opt-p "--opt2=4"))))

(deftest test-full-opt-p ()
  (let ((opt1 "--obloquy")
        (opt2 "--jthingie=123")
        (opt3 "--thing=1 2 3")
        (opt4 "--k"))
    (check (full-opt-p opt1))
    (check (full-opt-p opt2))
    (check (full-opt-p opt3))
    (check (full-opt-p "--thing 1 2 3"))
    (check (full-opt-p "--thing 1,2,3"))
    (check (not (full-opt-p opt4)))
    (check (not (full-opt-p "-cookycrisp")))
    (check (not (full-opt-p "-k=3")))
    (check (not (full-opt-p "-k 3")))))

(deftest test-end-opt-name ()
  (check
    (= 7 (end-opt-name "--crazy"))
    (= 2 (end-opt-name "-c"))
    (= 7 (end-opt-name "--happy=25"))
    (= 7 (end-opt-name "--happy 25"))))

(deftest test-opt-name ()
  (check
    (string="crazy" (opt-name "--crazy"))
    (string= "o" (opt-name "-o"))
    (string= "crazy" (opt-name "--crazy=1299"))
    (string= "o" (opt-name "-o=678"))
    (string= "crazy" (opt-name "--crazy 1299"))
    (string= "o" (opt-name "-o 678"))))

(deftest test-opt-values ()
  (check
    (null (opt-values "--some-option"))
    (equalp '("x" "y" "z") (opt-values "--some-option=x y z "))
    (equalp '("x" "y" "z") (opt-values "--opt=x,y,z"))
    (equalp '("x" "y" "z") (opt-values "--opt x,y,z"))
    (equalp '("x" "y" "z") (opt-values "--opt x y z"))
    (equalp '("1" "2") (opt-values "-x=1 2"))
    (equalp '("1") (opt-values "-x=1"))
    (equalp '("1") (opt-values "-x 1"))
    (equalp '("1") (opt-values "--stupid 1"))))

(defparameter *test-cli-opts*
  (list
   (make-cli-option :abbr "o" :full "ornithology" :requires-arguments t
                    :description
                    "Pass 1 to this to enable ornithology, 2 to enable it twice"
                    :example "-o 1, --ornithology 1")
   (make-cli-option :abbr "a" :full "artaxerxes" :requires-arguments nil
                    :description "Here's some ancient king for you"
                    :example "-a, --artaxerxes")))

(deftest test-abbr->full-opt-name ()
  (check
    (string= "artaxerxes"
             (abbr->full-opt-name "a" *test-cli-opts*))
    (string= "ornithology"
             (abbr->full-opt-name "o" *test-cli-opts*))
    (string= "ornithology"
             (abbr->full-opt-name "ornithology" *test-cli-opts*))
    (string= "k" (abbr->full-opt-name "k" *test-cli-opts*))))

(deftest test-coalesce-options ()
  (check
    (equalp'("--files=1 2 3")
           (coalesce-options '("--files=1" "2" "3")))
    (equalp '("--files 1 2 3")
            (coalesce-options '("--files" "1" "2" "3")))
    (equalp '("--files 1, 2, 3")
            (coalesce-options '("--files" "1," "2," "3")))
    (equalp '("--genre=fatty.xml"
              "--files=file1 file2 file3"
              "--some-other-things=1 2 3")
            (coalesce-options '("--genre=fatty.xml" "--files=file1" "file2"
                                "file3" "--some-other-things=1" "2" "3")))))

(deftest test-cli-parse-hash ()
  (let ((test1 (list "--ornithology=x-value"  "-a y-value"))
        (resl1 (make-hash-table :test #'equal)))
    (setf (gethash "ornithology" resl1) (list"x-value")
          (gethash "artaxerxes"  resl1) (list "y-value"))

    (check (equalp resl1 (cli-parse-hash test1 *test-cli-opts*)))))

;;;; EOF