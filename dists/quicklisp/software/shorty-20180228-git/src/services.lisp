(in-package :shorty)

(defmethod service-properties ((service (eql :isgd)))
  '(:url "https://is.gd"
    :path "/create.php?format=simple&url="
    :max-chunk-size 4000
    :encode-data t))

(defmethod service-properties ((service (eql :tinyurl)))
  '(:url "http://tinyurl.com"
    :path "/api-create.php?url="
    :max-chunk-size 7000
    :encode-data nil))
