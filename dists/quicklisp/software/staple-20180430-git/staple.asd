#|
 This file is a part of Staple
 (c) 2014 Shirakumo http://tymoon.eu (shinmera@tymoon.eu)
 Author: Nicolas Hafner <shinmera@tymoon.eu>
|#


(asdf:defsystem staple
  :name "Staple Documentation Generator"
  :version "1.1.0"
  :license "Artistic"
  :author "Nicolas Hafner <shinmera@tymoon.eu>"
  :maintainer "Nicolas Hafner <shinmera@tymoon.eu>"
  :description "A tool to generate documentation about Lisp projects through an HTML template."
  :homepage "https://github.com/Shinmera/staple"
  :serial T
  :components ((:file "package")
               (:file "symbols")
               (:file "clip")
               (:file "fulltext")
               (:file "stapler")
               (:file "documentation"))
  :depends-on (:staple-package-recording
               :clip
               :closer-mop
               :cl-ppcre
               :trivial-arguments
               :3bmd
               :3bmd-ext-code-blocks
               :documentation-utils
               :do-urlencode
               (:feature :sbcl (:require :sb-cltl2))))
