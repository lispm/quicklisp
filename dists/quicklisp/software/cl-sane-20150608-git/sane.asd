;;; -*- Mode: asdf -*-

(defpackage :sane-asdf
  (:use :cl :asdf :asdf/run-program))
(in-package :sane-asdf)

(defclass swig-generated-source (cl-source-file)
  ())

(defmethod perform :before ((operation compile-op) (s swig-generated-source))
  (let ((asdf::*verbose-out* *standard-output*))
    (run-program (list "swig" "-cffi"
                       (namestring (merge-pathnames
                                    (make-pathname :type "i")
                                    (component-pathname s))))
                 :wait t)))

(defsystem sane
  :author "Rupert Swarbrick <rswarbrick@gmail.com>"
  :licence "GPLv3"
  :description "Lispy library bindings for sane."
  :components ((:file "package")

               (:module "swig"
                        :components ((:swig-generated-source "sane-lowlevel"))
                        :depends-on ("package"))

               (:module "src"
                        :components
                        ((:file "devices")
                         (:file "library" :depends-on ("utilities"))
                         (:file "options" :depends-on ("devices"))
                         (:file "parameters" :depends-on ("devices"))
                         (:file "read" :depends-on ("devices" "utilities"))
                         (:file "debug" :depends-on ("devices"))
                         (:file "utilities"))
                        
                        :depends-on ("package" "swig")))

  :depends-on (:iterate :cffi :trivial-gray-streams))
