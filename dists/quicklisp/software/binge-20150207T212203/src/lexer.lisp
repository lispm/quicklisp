;;;  lexer.lisp --- lexical analyzer

;;;  Copyright (C) 2004-2006, 2008 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-32BC5E2E5DC5A403C5D7C976D4C97C6D.lisp,v 1.13 2004/12/16 11:53:17 wcp Exp $

;;;  This program is free software; you can redistribute it and/or
;;;  modify it under the terms of the GNU General Public License as
;;;  published by the Free Software Foundation; either version 2, or
;;;  (at your option) any later version.
;;;  This program is distributed in the hope that it will be useful,
;;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;  General Public License for more details.
;;;  You should have received a copy of the GNU General Public License
;;;  along with this program; see the file COPYING.  If not, write to
;;;  the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;;  Boston, MA 02111-1307, USA.

;;;  Commentary:
;;;
;;;  Simple and incomplete lexical analyzer for C grammar.

#+cmu (ext:file-comment "$Id: F-32BC5E2E5DC5A403C5D7C976D4C97C6D.lisp,v 1.13 2004/12/16 11:53:17 wcp Exp $")

(in-package :binge-lexer)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defstruct (lexer-position
	       (:include file-offset)
	       (:conc-name fpos-))
    file-name
    token))

(defclass parsable-stream (stateful-stream)
  ((grammar :initarg :grammar
	    :type npg:grammar)
   (file-name :initform nil
	      :initarg :file-name)
   (token :initform 0
	  :type integer)))

(defmethod print-object ((obj lexer-position) stream)
  (format stream "line ~A col ~A (token ~A)~@[ of file ~A~]" (fpos-line obj)
	  (fpos-column obj) (fpos-token obj) (fpos-file-name obj)))

(defmethod later-position ((pos1 lexer-position) (pos2 lexer-position))
  (> (fpos-token pos1) (fpos-token pos2)))

(defun make-parsable-stream (stream grammar)
  (make-instance 'parsable-stream
		 :grammar grammar
		 :stream stream
		 ;; ignore stuff like string-input-streams
		 :file-name (ignore-errors (pathname stream))))

(defun make-parsable-file (pathname grammar)
  (make-parsable-stream (open pathname) grammar))

(defmethod get-position ((stream parsable-stream))
  (with-slots (file-name line column token) stream
    (make-lexer-position :file-name file-name
			 :line line
			 :column column
			 :token (incf token))))

(defmacro with-parsable-file ((var pathname grammar) &body body)
  `(let ((,var (make-parsable-file ,pathname ,grammar)))
     (unwind-protect (progn ,@body)
       (close ,var))))

(eval-when (:load-toplevel :compile-toplevel)
  (defun make-presence-table (chars)
    (declare (type string chars))
    (loop
       with arr = (make-array char-code-limit :element-type 'bit :initial-element 0)
       for c across chars
       do (setf (sbit arr (char-code c)) 1)
       finally (return arr)))

  (defconst +word-start-table+
    (make-presence-table "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_"))

  (defconst +word-component-table+
    (make-presence-table "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_0123456789")))

(declaim (inline word-start-p word-component-p))
(defun word-start-p (c)
  (declare (type character c))
  (= 1 (sbit +word-start-table+ (char-code c))))

(defun word-component-p (c)
  (declare (type character c))
  (= 1 (sbit +word-component-table+ (char-code c))))

(declaim (inline skip-whitespace skip-to-eol))
(defun skip-whitespace (stream)
  (loop
     for c = (peek-char nil stream)
     while (whitespace-p c)
     do (read-char stream)))

(defun skip-to-eol (stream)
  (peek-char #\newline stream nil))

(defun digit-value (char &optional (base 10))
  "Return the integer value of CHAR if it's a digit otherwise
NIL.  CHAR can be a letter to accomodate exadecimal numbers.
Actually the base can be up to 36."
  (declare (type character char))
  (position char "0123456789abcdefghijklmnopqrstuvwxyz" :test #'char-equal :end base))

(defun read-integer-number (stream &optional (base 10))
  "Try to read an integer number from STREAM according to BASE.
Return its integer value."
  (declare (type stream stream)
	   (type fixnum base))
  (loop for c = (peek-char nil stream)
	for val = (digit-value c base)
	with number of-type integer = 0
	when (not val)
	return (values number (concatenate 'string digits))
	do (setf number (+ (the integer (* base number)) val))
	collect (read-char stream) into digits))

(defun read-integer-from-string (str &key (start 0) end (base 10))
  "Try to interprete STR as an integer.  Return its value."
  (declare (type string str)
	   (type fixnum base))
  (loop
     for i from start below (or end (length str))
     for val = (digit-value (char str i) base)
     with number of-type integer = 0
     when (not val)			; if we see rubbish, we fail
     return nil
     do (setf number (+ (the integer (* base number)) val))
     finally (return number)))

(defun read-escape-sequence (stream)
  (declare (type parsable-stream stream))
  (let ((c (read-char stream)))
    (case c
      (#\0
       (read-integer-number stream 8))
      (#\n #\newline)
      (#\t #\tab)
      (#\r #\return)
      (#\\ #\\))))

(defun read-character-sequence (stream)
  (let ((position (get-position stream))
	(c (read-char stream)))
    (prog1
	(make-token :type 'binge-grammar:character
		    :value (if (char= c #\\)
			       (read-escape-sequence stream)
			       c)
		    :position position)
      (unless (char= #\' (read-char stream))
	(error "invalid character constant at ~A" position)))))

(defun read-string (stream delimiter)
  (declare (type parsable-stream stream))
  (let ((position (get-position stream)))
    (labels ((collect-string ()
	       (let ((string (loop
				for c of-type character = (read-char stream)
				when (char= delimiter c)
				return (concatenate 'string string)
				collect c into string)))
		 (if (eq delimiter (peek-char nil stream nil))
		     (concatenate 'string string (string (read-char stream)) (collect-string))
		     string))))
      (make-token :type 'binge-grammar:string
		  :value (collect-string)
		  :position position))))

(defun read-word (stream &rest first-chars)
  (declare (type parsable-stream stream)
	   (type list first-chars))
  (with-slots (grammar) stream
    (let* ((position (get-position stream))
	   (word (loop
		    for c = (peek-char nil stream)
		    while (word-component-p c)
		    collect (read-char stream) into word
		    finally (return (concatenate 'string first-chars word)))))
      (make-token :type (cond ((grammar-keyword-p word grammar)
			       'keyword)
			      (t 'binge-grammar:identifier))
		  :value word
		  :position position))))

(defun get-rid-of-optional-UL (stream)
  ;; this is to get rid of the U (unsigned) or L (long) suffix
  (let ((next (peek-char nil stream)))
    (when (or (eq #\L next)
	      (eq #\U next))
      (read-char stream))))

(defun read-number (stream first-digit)
  "Read any kind of syntactically correct C number.  Return a
token according to type and value."
  (declare (type parsable-stream stream)
	   (type character first-digit))
  (let ((position (get-position stream)))
    (cond ((and (char= #\0 first-digit)
		(eq #\x (peek-char nil stream)))
	   (read-char stream)
	   (prog1 (make-token :type 'binge-grammar:integer
			      :value (read-integer-number stream 16)
			      :position position)
	     (get-rid-of-optional-UL stream)))
	  ;; normally a leading zero starts an octal number
	  ((and (char= #\0 first-digit))
	   ;; we read it as decimal
	   (multiple-value-bind (value string) (read-integer-number stream)
	     (declare (ignore value))
	     (let ((next (peek-char nil stream)))
	       ;; maybe it wasn't octal
	       (cond ((eq next #\.)
		      ;; it was actually a real
		      (make-token :type 'binge-grammar:float
				  :value (read-from-string (concatenate 'string string (list (read-char stream))
									(cadr (multiple-value-list (read-integer-number stream)))))
				  :position position))
		     ;; it was a malformed octal
		     ((not (read-integer-from-string string :base 8))
		      (error "malformed octal number at ~A" position))
		     (t
		      (prog1 (make-token :type 'binge-grammar:integer
					 :value (read-integer-from-string string :base 8)
					 :position position)
			(get-rid-of-optional-UL stream)))))))
	  (t	   ; this is wrong (it could be a float!) -wcp27/2/04.
	   (unread-char first-digit stream)
	   (prog1 (make-token :type 'binge-grammar:integer
			      :value (read-integer-number stream)
			      :position position)
	     (get-rid-of-optional-UL stream))))))

(defun read-dots (stream)
  (let ((seen-so-far (if (eq #\. (read-char stream))
			 (let ((3rd-char (read-char stream nil)))
			   (if (eq 3rd-char #\.)
			       "..."
			     ".."))
		       ".")))
  (list
   (make-token :type 'keyword
	       :value seen-so-far
	       :position (get-position stream)))))

;; This will be used when generating parser errors, giving a better
;; idea where the parsing actually failed.
(defun read-pound-line (stream)
  "Read a preprocessor line (those starting with # on column 0)
and adjust the stream file name and line number accordingly."
  (with-input-from-string (line-stream (read-line stream))
    (let ((line-number (read line-stream)))
      ;; The "line" token may not be present in the output of older
      ;; compilers
      (unless (numberp line-number)
	(setf line-number (read line-stream)))
      (setf (slot-value stream 'line) line-number)
      ;; the file name may be missing
      (awhen (read line-stream nil)
	 (setf (slot-value stream 'file-name) it)))))

(defmethod read-next-tokens ((stream parsable-stream))
  (flet ((make-keyword (c)
	   (list
	    (make-token :type 'keyword
			:value (string c)
			:position (get-position stream)))))
    (with-slots (grammar) stream
      (loop
	 for bol of-type boolean = (ss-bol stream)
	 for c = (read-char stream nil)
	 do (cond ((not c)		; EOF
		   (return nil))
		  ;; preprocessor line
		  ((and bol (char= c #\#))
		   (read-pound-line stream))
		  (t (cond ((word-start-p c)
			    (let ((word (read-word stream c)))
			      (return (list word))))
			   ;; We must NOT handle negative numbers at
			   ;; lexer level!
			   ((digit-char-p c)
			    (return (list (read-number stream c))))
			   ((char= #\. c)
			    (return (read-dots stream)))
			   ((char= #\" c)
			    (return (list (read-string stream c))))
			   ((char= #\' c)
			    (return (list (read-character-sequence stream))))
			   ((position c "<>!=+-|&")
			    (return
			      (list
			       (make-token :type 'keyword
					   :value (if (or (eq #\= (peek-char nil stream))
							  (eq c (peek-char nil stream))
							  ;; pointer dereference ->
							  (and (eq c #\-)
							       (eq #\> (peek-char nil stream))))
						      (concatenate 'string (string c) (string (read-char stream)))
						      (string c))
					   :position (get-position stream)))))
			   ((not (whitespace-p c))
			    ;; anything else is considered a keyword even though it's
			    ;; just a single character
			    (return (make-keyword c))))))))))

(defun skip-block (stream)
  "This function is called upon seeing an open curly bracket, to
skip everything up to the matching close bracket."
  (declare (type parsable-stream stream))
  (let ((input-tokens '()))
    (labels ((next ()
	       (unless input-tokens
		 (setf input-tokens (read-next-tokens stream)))
	       (pop input-tokens))
	     (skip ()
	       (loop
		  for token = (next)
		  while token
		  when (eq 'keyword (token-type token))
		  do (cond ((equalp "{" (token-value token))
			    ;; one level deeper
			    (skip))
			   ((equalp "}" (token-value token))
			    (return))
			   (t		; just keep going
			    nil)))))
      (skip))))

;; for debugging purposes
(defun scan-file (pathname)
  (with-parsable-file (stream pathname (force binge-grammar:define-grammar))
    (loop
       for toks = (read-next-tokens stream)
       until (null toks)
       append toks)))
