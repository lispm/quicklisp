;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          package.cl
;;;; Purpose:       Package file for Lisp Markup Language
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  Aug 2002
;;;;
;;;; $Id$
;;;;
;;;; This file, part of LML, is Copyright (c) 2002 by Kevin M. Rosenberg
;;;;
;;;; LML users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License v2
;;;; (http://www.gnu.org/licenses/gpl.html)
;;;; *************************************************************************

(in-package #:cl-user)

(defpackage #:lisp-markup-language
  (:use #:common-lisp)
  (:nicknames #:lml)
  (:export

   ;; base.lisp
   #:*print-spaces*
   #:reset-indent
   #:with
   #:print-page
   #:page
   #:lml-format
   #:lml-print
   #:lml-princ
   #:lml-write-char
   #:lml-write-string
   #:lml-print-date
   #:*html-output*

   ;; files.lisp
   #:with-dir
   #:process-dir
   #:lml-load
   #:include-file

   ;; stdsite.lisp
   #:print-std-page
   #:std-page
   #:std-body
   #:std-head
   #:titled-pre-section

   ;; downloads.lisp
   #:std-dl-page
   #:full-dl-page

   ;; utils.lisp
   #:lml-quit
   #:lml-cwd
))
