;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: umlisp -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:     classes-support.lisp
;;;; Purpose:  Support for UMLisp classes
;;;; Author:   Kevin M. Rosenberg
;;;; Created:  Apr 2000
;;;;
;;;; $Id$
;;;;
;;;; This file, part of UMLisp, is
;;;;    Copyright (c) 2000-2004 by Kevin M. Rosenberg, M.D.
;;;;
;;;; UMLisp users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License.
;;;; *************************************************************************

(in-package #:umlisp-orf)

;;; Formatting routines

(defgeneric fmt-cui (c))
(defmethod fmt-cui ((c ucon))
  (fmt-cui (cui c)))

(defmethod fmt-cui ((c fixnum))
  (prefixed-fixnum-string c #\C 7))

(defmethod fmt-cui ((c string))
  (if (eql (aref c 0) #\C)
      c
      (fmt-cui (parse-integer c))))

(defmethod fmt-cui ((c null))
  (format nil "nil"))

(defgeneric fmt-lui (c))
(defmethod fmt-lui ((l uterm))
  (fmt-lui (lui l)))

(defmethod fmt-lui ((l fixnum))
  (prefixed-fixnum-string l #\L 7))

(defmethod fmt-lui ((l string))
  (if (eql (aref l 0) #\L)
      l
      (fmt-lui (parse-integer l))))

(defgeneric fmt-sui (s))
(defmethod fmt-sui ((s ustr))
  (fmt-sui (sui s)))

(defmethod fmt-sui ((s fixnum))
  (prefixed-fixnum-string s #\S 7))

(defmethod fmt-sui ((s string))
  (if (eql (aref s 0) #\S)
      s
      (fmt-sui (parse-integer s))))

(defgeneric fmt-tui (tui))
(defmethod fmt-tui ((tui fixnum))
  (prefixed-fixnum-string tui #\T 3))

(defmethod fmt-tui ((tui string))
  (if (eql (aref tui 0) #\T)
      tui
      (fmt-tui (parse-integer tui))))

(defgeneric fmt-eui (e))
(defmethod fmt-eui ((e fixnum))
  (prefixed-fixnum-string e #\E 7))

(defmethod fmt-eui ((e string))
  (if (eql (aref e 0) #\E)
      e
      (fmt-eui (parse-integer e))))

(defmethod fmt-eui ((e null))
  (format nil "nil"))

(defun cui-p (ui)
  "Check if a string is a CUI"
  (check-ui ui #\C 7))

(defun lui-p (ui)
  "Check if a string is a LUI"
  (check-ui ui #\L 7))

(defun sui-p (ui)
  "Check if a string is a SUI"
  (check-ui ui #\S 7))

(defun tui-p (ui)
  (check-ui ui #\T 3))

(defun eui-p (ui)
  (check-ui ui #\E 7))

(defun check-ui (ui start-char len)
  (when (and (stringp ui)
             (= (length ui) (1+ len))
             (char-equal start-char (schar ui 0))
             (ignore-errors (parse-integer ui :start 1)))
    t))


;;; Generic display functions

(eval-when (:compile-toplevel :load-toplevel :execute)
(defun english-term-p (obj)
  "Returns two values: T/NIL if term is english and T/NIL if obj is a TERM"
  (if (eq (hyperobject::class-name (hyperobject::class-of obj)) 'uterm)
      (values (string-equal (lat obj) "ENG") t)
    (values nil nil))))

(defun english-term-filter (obj)
  "Retrns NIL if object is a term and not english"
  (multiple-value-bind (is-english is-term) (english-term-p obj)
      (or (not is-term) is-english)))

(defun print-umlsclass (obj &key (stream *standard-output*)
                        (vid :compact-text)
                        (file-wrapper nil) (english-only t) (subobjects nil)
                        (refvars nil) (link-printer nil))
  (view obj :stream stream :vid vid :subobjects subobjects
        :file-wrapper file-wrapper
        :filter (if english-only nil #'english-term-filter)
        :link-printer link-printer
        :refvars refvars))

(defmacro define-lookup-display (newfuncname lookup-func)
  "Defines functions for looking up and displaying objects"
  `(defun ,newfuncname  (keyval &key (stream *standard-output*) (vid :compact-text)
                         (file-wrapper t) (english-only nil) (subobjects nil))
     (let ((obj (funcall ,lookup-func keyval)))
       (print-umlsclass obj :stream stream :vid vid
                        :file-wrapper file-wrapper :english-only english-only
                        :subobjects subobjects)
       obj)))

(define-lookup-display display-con #'find-ucon-cui)
(define-lookup-display display-term #'find-uterm-lui)
(define-lookup-display display-str #'find-ustr-sui)

(defun ucon-has-tui (ucon tui)
  "Returns T if UCON has a semantic type of TUI."
  (some #'(lambda (usty) (= tui (tui usty))) (s#sty ucon)))

(defgeneric suistr (lo))
(defmethod suistr ((lo ulo))
  "Return the string for a ulo object"
  (find-string-sui (sui lo)))

(defgeneric pf-ustr (obj))
(defmethod pf-ustr ((ucon ucon))
  "Return the preferred ustr for a ucon"
  (pf-ustr
   (find-if (lambda (uterm) (string= "P" (ts uterm))) (s#term ucon))))

(defmethod pf-ustr ((uterm uterm))
  "Return the preferred ustr for a uterm"
  (find-if (lambda (ustr) (string= "PF" (stt ustr))) (s#str uterm)))

(defgeneric mesh-number (obj))
(defmethod mesh-number ((con ucon))
  (mesh-number (pf-ustr con)))

(defmethod mesh-number ((ustr ustr))
  (let ((codes
         (map-and-remove-nils
          (lambda (sat)
            (when (and (string-equal "MSH" (sab sat))
                       (string-equal "MN" (atn sat)))
              (atv sat)))
          (s#sat ustr))))
    (if (= 1 (length codes))
        (car codes)
      codes)))

(defun ucon-ustrs (ucon)
  "Return lists of strings for a concept"
  (let (res)
    (dolist (term (s#term ucon) (nreverse res))
      (dolist (str (s#str term))
        (push str res)))))


(defmethod pfstr ((uterm uterm))
  "Return the preferred string for a uterm"
  (dolist (ustr (s#str uterm))
    (when (string= "PF" (stt ustr))
      (return-from pfstr (str ustr)))))

(defmethod pfstr ((ustr ustr))
  "Return the preferred string for a ustr, which is the string itself"
  (str ustr))

(defun remove-non-english-terms (uterms)
  (remove-if-not #'english-term-p uterms))

(defun remove-english-terms (uterms)
  (remove-if #'english-term-p uterms))


(defvar +relationship-abbreviations+
  '(("RB" "Broader" "has a broader relationship")
    ("RN" "Narrower" "has a narrower relationship")
    ("RO" "Other related" "has relationship other than synonymous, narrower, or broader")
    ("RL" "Like" "the two concepts are similar or 'alike'.  In the current edition of the Metathesaurus, most relationships with this attribute are mappings provided by a source")
    ("RQ" "Unspecified" "unspecified source asserted relatedness, possibly synonymous")
    ("SY" "Source Synonymy" "source asserted synonymy")
    ("PAR" "Parent" "has parent relationship in a Metathesaurus source vocabulary")
    ("CHD" "Child" "has child relationship in a Metathesaurus source vocabulary")
    ("SIB" "Sibling" "has sibling relationship in a Metathesaurus source vocabulary")
    ("AQ" "Allowed" "is an allowed qualifier for a concept in a Metathesaurus source vocabulary")
    ("QB" "Qualified" "can be qualified by a concept in a Metathesaurus source vocabulary")))

(defvar *rel-info-table* (make-hash-table :size 30 :test 'equal))
(defvar *is-rel-table-init* nil)
(unless *is-rel-table-init*
  (dolist (relinfo +relationship-abbreviations+)
    (setf (gethash (string-downcase (car relinfo)) *rel-info-table*)
      (cdr relinfo)))
  (setq *is-rel-table-init* t))

(defun rel-abbr-info (rel)
  (nth-value 0 (gethash (string-downcase rel) *rel-info-table*)))

(defun filter-urels-by-rel (urels rel)
  (remove-if-not (lambda (urel) (string-equal rel (rel urel))) urels))


(defvar +language-abbreviations+
  '(("BAQ" . "Basque")
    ("DAN" . "Danish")
    ("DUT" . "Dutch")
    ("ENG" . "English")
    ("FIN" . "Finnish")
    ("FRE" . "French")
    ("GER" . "German")
    ("HEB" . "Hebrew")
    ("HUN" . "Hungarian")
    ("ITA" . "Italian")
    ("NOR" . "Norwegian")
    ("POR" . "Portuguese")
    ("RUS" . "Russian")
    ("SPA" . "Spanish")
    ("SWE" . "Swedish")))

(defvar *lat-info-table* (make-hash-table :size 30 :test 'equal))
(defvar *is-lat-table-init* nil)
(unless *is-lat-table-init*
  (dolist (latinfo +language-abbreviations+)
    (setf (gethash (string-downcase (car latinfo)) *lat-info-table*)
      (cdr latinfo)))
  (setq *is-lat-table-init* t))

(defun lat-abbr-info (lat)
  (nth-value 0 (gethash (string-downcase lat) *lat-info-table*)))


(defun stt-abbr-info (stt)
  (when (string-equal "PF" stt)
    (return-from stt-abbr-info "Preferred"))
  (when (char-equal #\V (schar stt 0))
    (setq stt (subseq stt 1)))
  (loop for c across stt
      collect
        (cond
         ((char-equal #\C c)
          "Upper/lower case")
         ((char-equal #\W c)
          "Word order")
         ((char-equal #\S c)
          "Singular")
         ((char-equal #\P c)
          "Plural")
         ((char-equal #\O c)
          "Other"))))


(defun ucon-parents (con &optional sab)
  (ucon-ancestors con sab t))

(defun ucon-ancestors (ucon &optional sab single-level)
  "Returns a list of ancestor lists for a concept"
  (let* ((parent-rels (filter-urels-by-rel (s#rel ucon) "par"))
         (anc nil))
    (when sab
      (setq parent-rels (delete-if-not
                         (lambda (rel) (string-equal sab (sab rel)))
                         parent-rels)))
    (dolist (rel parent-rels (nreverse anc))
      (let ((parent (find-ucon-cui (cui2 rel))))
        (push
         (if single-level
             (list parent)
           (list* parent (car (ucon-ancestors parent (sab rel) nil))))
         anc)))))

(defgeneric cxt-ancestors (obj))
(defmethod cxt-ancestors ((con ucon))
  (loop for term in (s#term con)
      append (cxt-ancestors term)))


(defmethod cxt-ancestors ((term uterm))
  (loop for str in (s#str term)
      append (cxt-ancestors str)))

(defmethod cxt-ancestors ((str ustr))
  "Return the ancestory contexts of a ustr"
  (let* ((anc (remove-if-not
               (lambda (cxt) (string-equal "ANC" (cxl cxt)))
               (s#cxt str)))
         (num-contexts (if anc
                           (apply #'max (mapcar (lambda (cxt) (cxn cxt)) anc))
                         0))
         (anc-lists '()))
    (dotimes (i num-contexts (nreverse anc-lists))
      (let* ((anc-this-cxn (remove-if-not
                            (lambda (cxt) (= (1+ i) (cxn cxt))) anc)))
        (push
         (sort anc-this-cxn (lambda (a b) (< (rnk a) (rnk b))))
         anc-lists)))))


#+scl
(dolist (c '(urank udef usat uso ucxt ustr ulo uterm usty urel ucoc uatx ucon uxw uxnw uxns lexterm labr lagr lcmp lmod lnom lprn lprp lspl ltrm ltyp lwd sdef sstr sstre1 sstre2 usrl))
    (let ((cl (find-class c)))
      (clos:finalize-inheritance cl)))


