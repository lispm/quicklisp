;; -*- Lisp -*-

(defpackage #:text-query-system
  (:use #:common-lisp #:asdf))

(in-package #:text-query-system)

(defsystem text-query
  :description "A general text-based system for querying the user."
  :author "Mark Kantrowitz"
  :maintainer "Peter Scott <sketerpot@gmail.com>"
  :licence "Free use, modification, and distribution. Copyright 1993 by Mark Kantrowitz."
  :version "1.1"
  :components ((:doc-file "README")
	       (:file "text-query")))