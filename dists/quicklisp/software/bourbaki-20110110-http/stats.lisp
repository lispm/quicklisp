;;; Copyright (c) Juha Arpiainen 2005-06

;;; This file is part of Bourbaki.

;;; Bourbaki is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.

;;; Bourbaki is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Bourbaki; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; -------

;;; *****
;;; Theorem statistics (dependence on axioms, used by -list)
;;; *****

(in-package :bourbaki)

(defun collect-stats-thr (thr)
  (let ((dep nil) (defs nil) (pflen 0))
    (if (context-proof thr)
      (do-proof (l (context-proof thr))
        (let ((line (subproof-ref l)))
          (let ((ref (car line)))
            (case (context-class ref)
              (:axiom (setf dep (adjoin ref dep)) (incf pflen))
              (:definition (setf defs (adjoin ref defs)) (incf pflen))
              (:theorem
                (setf dep (union dep (gethash :depends-on (context-meta ref)))
                      defs (union defs (gethash :defs-used (context-meta ref)))
                      pflen (+ pflen (gethash :proof-len (context-meta ref))))))
            (setf (gethash :used-by (context-meta ref)) (adjoin thr (gethash :used-by (context-meta ref))))))))
    (setf (gethash :depends-on (context-meta thr)) dep
          (gethash :defs-used (context-meta thr)) defs
          (gethash :proof-len (context-meta thr)) pflen)))

(defun show-stats (ref)
  (let ((thr (mkcontext ref)))
    (format t "~&Depends on axioms: ")
    (dolist (dep (gethash :depends-on (context-meta thr)))
      (format t "~A " (context-name dep)))
    (format t "~&Uses definitions: ")
    (dolist (def (gethash :defs-used (context-meta thr)))
      (format t "~A " (context-name def)))
    (format t "~&Is used by ~d theorems: " (length (gethash :used-by (context-meta thr))))
    (dolist (th (gethash :used-by (context-meta thr)))
      (format t "~A " (context-name th)))
    (format t "~&Total proof length: ~d~%" (gethash :proof-len (context-meta thr)))))

(defun collect-stats (ref)
  (let ((thr (mkcontext ref)))
    (walk-proof-tree thr #'collect-stats-thr
                     (case (context-class thr)
                       ((:context :module :local) t)
                       (otherwise nil)))))

(defun show-unused (ref)
  (let ((ctx (mkcontext ref)))
    (loop for x being the hash-values in (context-syms ctx)
          do (if (and (symref-target x)
                      (null (gethash :used-by (context-meta (symref-target x)))))
               (format t "~A, " (context-name (symref-target x)))))
    (dolist (e (cdr (context-exports ctx)))
      (show-unused e))))
