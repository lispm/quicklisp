;;;  test.lisp --- output module for testing purposes

;;;  Copyright (C) 2004 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-F80F5C5C42B2CA21658D444171975A97.lisp,v 1.7 2004/04/05 15:56:24 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-F80F5C5C42B2CA21658D444171975A97.lisp,v 1.7 2004/04/05 15:56:24 wcp Exp $")

(in-package :binge-backend)

(defclass test-backend (backend) ())

(defmethod c-object->ffi-notation ((decl c-declaration) (backend test-backend))
  (format nil "~S~%" (binge-tests:sexp-of decl)))

;; Here we don't set the binge-compiler:*backend* because it doesn't
;; make sense to have the default backend to be something useful for
;; tests only.

(export 'test-backend)		     ; so that we can use this backend
