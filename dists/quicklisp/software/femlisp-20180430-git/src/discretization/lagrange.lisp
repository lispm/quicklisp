;;; -*- mode: lisp; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; lagrange.lisp - Lagrange Finite Elements
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2003-2006 Nicolas Neuss, University of Heidelberg.
;;; Copyright (C) 2007- Nicolas Neuss, University of Karlsruhe.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF HEIDELBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.discretization)

;;;; This module provides Lagrange dofs and Lagrange basis functions of
;;;; arbitrary order and for arbitrary product cells.

(defun lagrange-coords-1d (order type)
  (ecase type
    (:uniform
     (coerce (loop for i upto order collect (/ i order))
             'vector))
    ;; The following choice of nodal points does not work for simplices with
    ;; dim>=3 and order>=3 because nodal points on the sides do not fit.  But
    ;; it is much better suited for interpolation in the case of product-cell
    ;; elements.
    (:gauss-lobatto
     (coerce (gauss-lobatto-points-on-unit-interval (1- order)) 'vector))))

(defgeneric lagrange-inner-coords (cell order type)
  (:documentation "Returns a list of Lagrange coordinates on the cell @arg{cell}
for Lagrange finite elements of order @arg{order} and type @arg{type}.")
  (:method ((vtx <vertex>) order type)
    (declare (ignore order type))
    (list #()))
  (:method ((simplex <simplex>) order type)
    (let ((coords-1d (lagrange-coords-1d order type))
          (dim (dimension simplex))
          (result ()))
      (multi-for (x (make-fixnum-vec dim 1) (make-fixnum-vec dim (1- order)))
        (when (< (reduce #'+ x) order)
          (push (map 'vector (lambda (i) (aref coords-1d i)) x)
                result)))
      (reverse result)))
  (:method ((cell <product-cell>) order type)
    (apply #'map-product
           #'(lambda (&rest args)
               (apply #'concatenate 'vector args))
           (mapcar #'(lambda (simplex)
                       (lagrange-inner-coords simplex order type))
                   (factor-simplices cell)))))

(with-memoization (:id 'lagrange-dofs)
  (defun lagrange-dofs (cell order type)
    (memoizing-let ((cell cell) (order order) (type type))
      (let ((lagrange-coords (lagrange-coords-1d order type)))
	(coerce
	 (loop with dof-index = -1
	       for subcell across (subcells cell)
	       and i from 0
               nconcing
               (loop with subcell-coords = (lagrange-inner-coords subcell order type)
                     for precise-local in subcell-coords
                     and j from 0
                     collect
                     ;; we need below that the coordinates are eql to the
                     ;; 1d coordinates (uniform or Lobatto) without any
                     ;; rounding error which we ensure here.
                     (let* ((coercer (number-coercer 'double-float))
                            (local (funcall coercer precise-local))
                            (precise-global
                              (map 'vector
                                   (lambda (coord)
                                     (find-if (lambda (coord2) (< (abs (- coord coord2)) 1.0e-10))
                                              lagrange-coords))
                                   (l2g subcell local)))
                            (global (funcall coercer precise-global)))
                       (make-instance 'dof
                                      :index (incf dof-index)
                                      :subcell subcell :subcell-index i
                                      :in-vblock-index j
                                      :precise-coord precise-local :coord local
                                      :precise-gcoord precise-global :gcoord global
                                      :functional (lambda (shape)
                                                    (evaluate shape
                                                              (if *dof-precise-evaluation*
                                                                  precise-global
                                                                  global)))))))
	 'vector)))))

;;; (map 'vector #'dof-gcoord (lagrange-dofs (n-cube 2) 2 :uniform))

(defun lagrange-basis-simplex (cell order type)
  "Computes the Lagrange basis for a cell.  Should be applied only for
simplices, because for product-cells the basis can be computed as a tensor
product which is faster."
  (let ((*standard-matrix-default-element-type* 'number)
        (*dof-precise-evaluation* t))
    (compute-duals (Q-nomials-of-degree cell order '<=)
                   (lagrange-dofs cell order type)
                   #'evaluate)))

;;; (lagrange-basis-simplex (n-simplex 2) 3 :uniform)

(defun shapes-and-dof-coords (factor-simplices order type)
  "Computes simultaneously shapes and dof-coords for a simplex-product-cell
as products of the simplex factor shapes and dofs."
  (when (null factor-simplices)
    ;; No simplicial factors - This is a limit case for which it is unclear
    ;; to me how it should be handled in a clean way.  The number 1.0 is a
    ;; scalar which might be interpreted as a polynomial of zero variance.
    (return-from shapes-and-dof-coords
      (values (list #()) (list 1))))
  (let* ((factor (first factor-simplices))
         (f-dim (dimension factor))
         (f-shapes (lagrange-basis-simplex factor order type))
         (f-dofs (lagrange-dofs factor order type)))
    (if (single? factor-simplices)
        (values (map 'vector #'dof-gcoord f-dofs)
                (coerce f-shapes 'vector))
        (multiple-value-bind (coords shapes)
            (shapes-and-dof-coords (rest factor-simplices) order type)
          (let ((product
                  (loop+ ((coord coords) (shape shapes))
                    nconcing
                    (loop+ ((f-dof f-dofs) (f-shape f-shapes))
                      collecting
                      (let* ((f-coord (dof-gcoord f-dof))
                             (new-shape (poly-exterior-product f-shape shape)))
                        (assert (= (variance new-shape) (+ (variance shape) f-dim))
                                ()
                                "Problem: ~A * ~A = ~A" f-shape shape new-shape)
                        (cons (concatenate 'vector f-coord coord)
                              new-shape))))))
            (values (map 'vector #'car product) (map 'vector #'cdr product)))))))



(with-memoization (:id 'lagrange-basis)
  (defun lagrange-basis (cell order type)
    "Computes the Lagrange basis for a product-cell accelerated.  The idea is to
construct the shapes with their associated dof-coordinates as a product of
lower-dimensional shapes and coordinates."
    (memoizing-let ((cell cell) (order order) (type type))
      (multiple-value-bind (coords shapes)
          (shapes-and-dof-coords (factor-simplices cell) order type)
        (let ((table (make-hash-table :test #'equalp)))
          (loop+ ((coord coords) (shape shapes))
            do (setf (gethash coord table) shape))
          (map 'vector (lambda (dof) (gethash (dof-gcoord dof) table))
               (lagrange-dofs cell order type)))))))

;;; (lagrange-basis (n-cube 2) 5 :uniform)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; fe-class definitions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(with-memoization (:id 'cell-lagrange-fe :test 'equalp :debug t)
  (defun cell-lagrange-fe (cell order type &optional disc)
    "Returns a Lagrange fe depending on reference cell, an order \(which
can be number or vector\), and a type symbol."
    (memoizing-let ((refcell (reference-cell cell))
		    (order order) (type type) (disc disc))
      (etypecase order
	(number (make-instance
		 '<scalar-fe> :cell refcell :discretization disc
                              :dofs (lagrange-dofs refcell order type)
                              :basis (lagrange-basis refcell order type)
                              :order order))
	(vector (make-instance
		 '<vector-fe> :discretization disc :components
		 (map 'vector (lambda (order)
				(cell-lagrange-fe refcell order type))
		      order)))))))

#+(or)
(with-memoization (:id 'new-cell-lagrange-fe :test 'equalp :debug t)
  (defgeneric new-cell-lagrange-fe (cell order type &optional disc)
    (:documentation "Returns a Lagrange fe depending on reference cell, an
order \(which can be number or vector\), and a type symbol.")
    (:method :around (cell order type &optional disc)
      (memoizing-let ((refcell (reference-cell cell))
                      (order order) (type type) (disc disc))
        (call-next-method refcell order type disc)))
    (:method (refcell (order vector) type &optional disc)
      (make-instance
       '<vector-fe> :discretization disc :components
       (map 'vector (lambda (order)
                      (cell-lagrange-fe refcell order type))
            order)))
    (:method ((refcell <simplex>) (order number) type &optional disc)
      (make-instance
       '<scalar-fe> :cell refcell :discretization disc
                    :dofs (lagrange-dofs refcell order type)
                    :basis (lagrange-basis refcell order type)
                    :order order))
    (:method ((refcell <product-cell>) (order number) type &optional disc)
      (let ((factors (factor-simplices refcell)))
        (assert (> (length factors) 1))
        (let* ((rank (length factors))
               (fes (mapcar (lambda (factor)
                              (cell-lagrange-fe factor order type disc))
                            factors))
               (nvec (map 'vector #'nr-of-dofs fes))
               (n (reduce #'* nvec))
               (dofs (make-array n))
               (shapes (make-array n))
               (k 0))
          (multi-for (ivec (make-int-vec rank) nvec)
            (setf (aref shapes k)
                  (reduce #'poly-exterior-product
                          (map 'vector (lambda (fe i)
                                         (aref (fe-basis fe) i))
                               fes ivec)))
            (setf (aref dofs k)
                  (let* ((coercer (number-coercer 'double-float))
                         (local (funcall coercer precise-local))
                         (precise-global
                           (map 'vector
                                (lambda (coord)
                                  (find-if (lambda (coord2) (< (abs (- coord coord2)) 1.0e-10))
                                           lagrange-coords))
                                (l2g subcell local)))
                         (global (funcall coercer precise-global)))
                    (make-instance 'dof
                                   :index (incf dof-index)
                                   :subcell subcell :subcell-index i
                                   :in-vblock-index j
                                   :precise-coord precise-local :coord local
                                   :precise-gcoord precise-global :gcoord global
                                   :functional (lambda (shape)
                                                 (evaluate shape
                                                           (if *dof-precise-evaluation*
                                                               precise-global
                                                               global)))))))
      (make-instance
       '<scalar-fe> :cell refcell :discretization disc
                    :dofs dofs :basis shapes :order order))))))

(with-memoization (:id 'lagrange-fe :test 'equalp :debug t)
  (defun lagrange-fe (order &key (nr-comps 1) (type :uniform))
    "Constructor for Lagrange fe.  nr-comps=nil builds a scalar fe-discretization,
otherwise a vector-fe-discretization is built."
    (declare (notinline lagrange-fe))
    (when (vectorp order)
      (assert (= (length order) nr-comps)))
    (when (and nr-comps (numberp order))
      (setq order (make-array nr-comps :initial-element order)))
    ;; (vectorp order) is now indicator for vector-fe/scalar-fe
    (memoizing-let ((order order) (nr-comps nr-comps) (type type))
      (lret ((disc (make-instance
		    (if (vectorp order)
			'<vector-fe-discretization>
			'<scalar-fe-discretization>))))
	(with-slots (components cell->fe) disc
	  (setf (get-property disc :type) type
                (get-property disc :order) order
                (get-property disc :nr-comps) nr-comps)
	  (setf cell->fe
                (lambda (cell)
                  (cell-lagrange-fe cell order type disc)))
	  (if (vectorp order)
	      (setf components
		    (vector-map (rcurry #'lagrange-fe :nr-comps nil :type type)
				order))
	      (setf (slot-value disc 'order) order)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Isoparametric stuff
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun lagrange-basis-boundary (cell order type)
  (loop+ ((phi (lagrange-basis cell order type))
	  (dof (lagrange-dofs cell order type)))
    unless (interior-dof? dof) collect phi))

(defun lagrange-basis-inner (cell order type)
  (loop+ ((phi (lagrange-basis cell order type))
	  (dof (lagrange-dofs cell order type)))
    when (interior-dof? dof) collect phi))

(defun lagrange-boundary-dofs (cell order type)
  (loop+ ((dof (lagrange-dofs cell order type)))
    unless (interior-dof? dof) collect dof))

(defun lagrange-reference-parameters (refcell order type)
  "Computes an energy-minimizing extension to the interior for the
boundary lagrangian."
  (assert (reference-cell-p refcell))
  (let* ((fe-disc (lagrange-fe order :type type :nr-comps nil))
	 (fe (get-fe fe-disc refcell))
	 (nr-inner-dofs (nr-of-inner-dofs fe))
	 (result (make-hash-table)))
    (cond
      ((zerop nr-inner-dofs)
       (loop+ ((dof (fe-dofs fe)) (shape (fe-basis fe)))
         do (setf (gethash dof result) shape)))
      (t
       (let* ((nr-dofs (nr-of-dofs fe))
	      (energy-mat (make-real-matrix nr-dofs))
	      (inner-indices (range< 0 nr-inner-dofs))
	      (boundary-indices (range< nr-inner-dofs nr-dofs)))
	 (loop with qrule = (quadrature-rule fe)
	       for weight across (integration-weights qrule)
	       for gradients across (ip-gradients fe qrule) do
                 (gemm! weight gradients gradients 1.0 energy-mat :nt))
	 (let* ((A_II (submatrix energy-mat :row-indices inner-indices :col-indices inner-indices))
		(A_IB (submatrix energy-mat :row-indices inner-indices :col-indices boundary-indices))
		(corr-mat (m* (m/ A_II) A_IB))
		(inner-shapes (lagrange-basis-inner refcell order type)))
	   (loop+ (j (dof (lagrange-boundary-dofs refcell order type))
		     (phi (lagrange-basis-boundary refcell order type)))
	     do (setf (gethash dof result)
		      (loop+ (i (psi inner-shapes))
			do (setf phi (axpy (- (mref corr-mat i j)) psi phi))
			finally (return phi))))))))
    result))

(defun lagrange-polynomial-vector (cell order type)
  "Returns a vector of polynomials representing the isoparametric mapping."
  (loop with result = (copy (make-array (embedded-dimension cell)
					:initial-element (make-polynomial '(0))))
	with basis = (lagrange-reference-parameters (reference-cell cell) order type)
	with subcells = (subcells cell)
	for dof being the hash-keys of basis using (hash-value phi) do
          (loop for comp across (local->global (aref subcells (dof-subcell-index dof))
                                               (dof-coord dof))
                and i from 0 do
                  (axpy! comp phi (aref result i)))
	finally (return result)))

(defun lagrange-mapping (order &optional (type :uniform))
  "Returns a function which maps a cell by a polynomial which is obtained
by interpolating the boundary map via Lagrange interpolation."
  (lambda (cell)
    (let* ((poly-vec (lagrange-polynomial-vector cell order type))
           (embedded-dim (length poly-vec))
           (dim (dimension cell))
           (jacobian
             (make-array (list embedded-dim dim)
                         :initial-contents
                         (loop for poly across poly-vec
                               collect
                               (loop for i from 0 below dim
                                     collect                                                  
                                     (differentiate poly i))))))
      (dbg :lagrange "Lagrange mapping for cell ~A:~%Map=~A~%Jacobian=~A"
           cell poly-vec jacobian)
      (make-instance
       '<special-function>
       :domain-dimension (dimension cell)
       :image-dimension (length poly-vec)
       :evaluator
       (lambda (lcoord)
         (loop with result = (make-double-vec (length poly-vec))
               for poly across poly-vec
               and i from 0 do
                 (setf (aref result i) (evaluate poly lcoord))
               finally (return result)))
       :gradient
       (lambda (lcoord)
         (lret ((result (zeros embedded-dim dim)))
           (dotimes (i embedded-dim)
             (dotimes (j dim)
               (setf (mref result i j)
                     (evaluate (aref jacobian i j) lcoord))))))))))


;;; Testing
(defun test-lagrange ()
  (lagrange-coords-1d 2 :uniform)
  (lagrange-inner-coords (n-cube 2) 3 :uniform)
  (lagrange-basis *reference-vertex* 3 :uniform)
  (shapes-and-dof-coords (factor-simplices (n-cube 2)) 2 :uniform)
  (flet ((check-fe (fe-class refcell)
	   (let* ((fe (get-fe fe-class refcell))
		  (qrule (quadrature-rule fe)))
	     (integration-points qrule)
	     (list (ip-values fe qrule) (ip-gradients fe qrule)))))
    (check-fe (lagrange-fe 3 :nr-comps 2) *unit-cube*))
  (lagrange-basis *reference-vertex* 1 :uniform)
  (lagrange-basis *unit-interval* 1 :uniform)
  (loop
    for precise-p in '(nil t)
    for result
      = (loop with type = :uniform
              for order from 1 upto 10
              collect
              (let ((n (1+ order))
                    (basis (lagrange-basis *unit-interval* order type))
                    (dofs (lagrange-dofs  *unit-interval* order type))
                    (*dof-precise-evaluation* precise-p))
                (norm (m- (eye n)
                          (make-real-matrix
                           (loop for shape across basis
                                 collect
                                 (loop for dof across dofs
                                       collect
                                       (coerce (evaluate dof shape) 'double-float))))))))
    do (when precise-p
         (assert (every #'zerop result))))
  
  (let* ((dim 2)
         (order 5)
         (fe (cell-lagrange-fe (n-cube dim) order :uniform))
         (*dof-precise-evaluation* t))
    (let ((A (make-real-matrix
              (loop for dof across (fe-dofs fe)
                    collect
                    (loop for shape across (fe-basis fe)
                          collect (coerce (evaluate dof shape) 'double-float))))))
      (assert (mzerop (m- A (eye (nrows A)))))))
  
  (time
   (let* ((dim 3)
          (order 4)
          (fe (cell-lagrange-fe (n-cube dim) order :uniform))
          (qrule (quadrature-rule fe)))
     (ip-values fe qrule)
     nil))

  (lagrange-dofs *unit-cube* 5 :uniform)
  (vector-map #'dof-gcoord (lagrange-dofs *unit-cube* 1 :uniform))
  (vector-map #'variance (lagrange-basis *unit-cube* 1 :uniform))
  (assert (= (length (lagrange-dofs *unit-interval* 1 :uniform)) 2))
  (lagrange-dofs *unit-interval* 2 :uniform)
  (lagrange-basis *unit-triangle* 2 :uniform)
  (lagrange-dofs *unit-triangle* 2 :uniform)
  (lagrange-dofs *unit-triangle* 3 :uniform)
  (lagrange-dofs *unit-tetrahedron* 3 :uniform)
  (lagrange-basis *unit-triangle* 1 :uniform)
  ;; does not matter if :uniform or :gauss-lobatto
  (let ((coeffs (remove nil (flatten (coefficients (aref (lagrange-basis *unit-quadrangle* 7 :uniform) 0))))))
    (reduce #'max (mapcar #'abs coeffs)))
  (lagrange-dofs *unit-cube* 2 :uniform)
  (vector-map #'dof-gcoord (lagrange-dofs *unit-quadrangle* 1 :uniform))
  (vector-map #'variance (lagrange-basis *unit-quadrangle* 1 :uniform))
  (vector-map #'dof-gcoord (lagrange-dofs *unit-cube* 1 :uniform))
  (vector-map #'variance (lagrange-basis *unit-cube* 1 :uniform))
  (lagrange-basis-simplex (n-simplex 1) 1 :uniform)
  (shapes-and-dof-coords (list (n-simplex 1) (n-simplex 1) (n-simplex 1)) 1 :uniform)
  (shapes-and-dof-coords (list (n-simplex 1)) 1 :uniform)
  (lagrange-basis (n-simplex 1) 1 :uniform)
  (lagrange-basis *unit-interval* 1 :uniform)

  ;; the following works for order 15 with CMUCL, SBCL, Allegro.
  ;; however, it breaks for gcl (control stack overflow).
  (time (let ()
	  (lagrange-basis-simplex *unit-triangle* 5 :uniform)
	  nil))
  (lagrange-inner-coords *unit-quadrangle* 3 :uniform)
  ;; the following 
  (time (lagrange-reference-parameters *unit-quadrangle* 7 :uniform))

  ;; Isoparametric stuff
  (let ((center (make-vertex #(0.0 -4.0)))
	(east-vtx (make-vertex #(-1.0 1.0)))
	(north-vtx (make-vertex #(0.0 1.0))))
    ;; line segments
    (let ((seg-ce (make-line center east-vtx))
	  (seg-cn (make-line center north-vtx))
	  (seg-en (make-line east-vtx north-vtx)))
      (let ((triangle (make-simplex (vector seg-en seg-cn seg-ce)))
	    (lcoord #(0.2 0.3)))
	(princ (det (local->Dglobal triangle lcoord))) (terpri)
	(princ (local->global triangle lcoord))
	(evaluate (funcall (lagrange-mapping 4) triangle) lcoord))))

  (assert (eq (reference-cell (get-fe (lagrange-fe 1) *unit-interval*))
	      *unit-interval*))
  (setq *print-matrix* 7)
  (let ((fe (get-fe (lagrange-fe 1) *unit-interval*)))
    (fe-basis fe))
  (get-fe (lagrange-fe 3) *unit-interval*)
  (get-fe (lagrange-fe 3) (n-simplex 3))

  ;;
  (describe
   (make-instance
    '<vector-fe>
    :components (make-array 2 :initial-element (get-fe (lagrange-fe 1) *unit-interval*))))
  ;;
  (with-dbg (:lagrange)
    (let* ((domain (n-ball-domain 2))
           (mesh (make-mesh-from domain :parametric (lagrange-mapping 1)))
           (refined (refine mesh)))
      (check domain)
      ;; the following cannot work because mesh does not fit the boundary.
      ;; this may indicate a conceptual problem
      (ignore-errors (check refined))
      ))
  
  (shapes-and-dof-coords (list (n-simplex 2) (n-simplex 1)) 1 :uniform)
  
  #|
  (let ((s1 (second (lagrange-basis-simplex (n-simplex 2) 1 :uniform)))
        (s2 (first (lagrange-basis-simplex (n-simplex 1) 1 :uniform))))
    (coefficients (zero s2)))
  (mapc #'describe (Q-nomials-of-degree (n-simplex 2) 1 '<=))
  |#

  ;; Problem: Cells use double-float coordinates throughout the code.  This
  ;; should probably be generalized.  However, this might have a large
  ;; impact on performance, and must be done very carefully on a branch.
  (let* ((cell (n-simplex 1))
         (order 3)
         (basis (Q-nomials-of-degree cell order '<=))
         (dofs (lagrange-dofs cell order :uniform)))
    (evaluate (aref dofs 3) (second basis)))

  (let* ((disc (lagrange-fe 1 :nr-comps nil))
         (fe (get-fe disc (n-cube 2)))
         (ipvals (coerce (ip-values fe (quadrature-rule fe))
                         'list))
         (result (zeros (nrows (first ipvals)) (length ipvals))))
    (assert ipvals)
    (apply #'join-horizontal! result ipvals))
  )

;;; (fl.discretization::test-lagrange)
(fl.tests:adjoin-test 'test-lagrange)
