;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10; Package: photo -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          tables.lisp
;;;; Purpose:       Returns tables of values
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  April 2005
;;;;
;;;; $Id: dof.lisp 10436 2005-04-21 15:36:01Z kevin $
;;;;
;;;; This file, part of cl-photo, is Copyright (c) 2005 by Kevin M. Rosenberg
;;;;
;;;; cl-photo users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License v2
;;;; (http://www.gnu.org/licenses/gpl.html)
;;;;
;;;; *************************************************************************

(in-package #:photo)

(defparameter +f-stops+ '(1.4 2.0 2.8 4.0 5.6 8 11 16 22 32))
(defparameter +focal-lengths+ '(12 24 35 50 70 85 105 135 200 300 400 500 600))
(defparameter +distances-feet+ '(0.5 1 3 8 15 25 50 100 200))

(defun hyperfocal-table (focal-length coc &key (units :feet) (output *standard-output*))
  (loop for f-stop in +f-stops+
        do (format output "~4,1F ~,1F~%"
                   f-stop (hyperfocal focal-length f-stop coc :units units)))
  (values))

(defun aov-table (imager &key (output *standard-output*) (projection :rectilinear))
  (let ((imager-dim (etypecase imager
                      (cons imager)
                      (symbol (imager-dimensions imager)))))
    (format output "~5A ~5A ~5A ~5A~%" "FOCAL" "AOV-W" "AOV-H" "AOV-D")
    (loop for focal-length in +focal-lengths+
          do (let ((aov (multiple-value-list (aov focal-length (car imager-dim) (cdr imager-dim)
                                                  :projection projection))))
               (format output "~5D ~5,1F ~5,1F ~5,1F~%"
                       focal-length (nth 0 aov) (nth 1 aov) (nth 2 aov)))))
  (values))

(defun fov-table (imager focal-length
                         &key (output *standard-output*) (projection :rectilinear)
                         (units :feet))
  (let ((imager-dim (etypecase imager
                      (cons imager)
                      (symbol (imager-dimensions imager))))
        (distances (mapcar 'feet->mm +distances-feet+)))
    (format output "~8A  ~6A ~6A ~6A ~6A~%" "DISTANCE" "WIDTH" "HEIGHT" "DIAGON" "MAG")
    (loop for distance in distances
          do (let ((fov (multiple-value-list (fov focal-length
                                                  (car imager-dim) (cdr imager-dim)
                                                  distance
                                                  :projection projection))))
               (format output "~8F: ~6F ~6F ~6F ~6F~%"
                       (mm->length distance units)
                       (nth 0 fov) (nth 1 fov) (nth 2 fov) (nth 3 fov)))))
  (values))

(defun dof-table (focal-length coc &key (output *standard-output*) (units :feet))
  (let ((distances (mapcar (lambda (mm) (mm->length mm units))
                           (mapcar 'feet->mm +distances-feet+))))
    (format output "~&~5A  " "FStop")
    (dolist (distance distances)
      (format output " ~10F " distance))
    (format output "~%")
    (dolist (f-stop +f-stops+)
      (format output "~5,1F  " f-stop)
      (dolist (distance distances)
        (multiple-value-bind (near far dof mag blur) (dof focal-length f-stop distance coc
                                                          :units units)
          (declare (ignorable dof mag blur))
          (when (minusp far) (setq far "Inf  "))
          (format output "~5F/~5F " near far)))
      (format output "~%")))
  (values))
