(asdf:defsystem #:shorty
  :description "Shorten URLs using third-party services."
  :author ("Michael Fiano <mail@michaelfiano.com>")
  :maintainer "Michael Fiano <mail@michaelfiano.com>"
  :license "MIT"
  :homepage "https://www.michaelfiano.com/projects/shorty"
  :source-control (:git "git@github.com:mfiano/shorty.git")
  :bug-tracker "https://github.com/mfiano/shorty/issues"
  :version "1.0.0"
  :encoding :utf-8
  :long-description #.(uiop:read-file-string (uiop/pathname:subpathname *load-pathname* "README.md"))
  :depends-on (#:dexador
               #:quri
               #:split-sequence
               #:cl-base64
               #:babel
               #:salza2
               #:chipz
               #:fast-io)
  :pathname "src"
  :serial t
  :components
  ((:file "package")
   (:file "services")
   (:file "shorty")))
