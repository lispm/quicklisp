(defsystem :general-accumulator
  :description "A general-purpose, extensible value accumulator"
  :author "Teemu Likonen <tlikonen@iki.fi>"
  :licence "Public domain"
  :components ((:file "accumulator")))
