;;;; -*- Mode: Lisp -*-
;;;; ASDF definition for irc-logger
;;;; $Id: irc-logger.asd,v 1.1 2003/12/14 16:10:29 krosenberg Exp $

(in-package cl-user)
(defpackage irc-logger-system (:use :cl :asdf))
(in-package irc-logger-system)

(defsystem irc-logger
    :depends-on (cl-irc cl-ppcre)
    :components ((:file "package")
		 (:file "logger" :depends-on ("package"))))



