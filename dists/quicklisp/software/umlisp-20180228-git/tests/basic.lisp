;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: umlisp -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          basic.lisp
;;;; Purpose:       Basic tests for UMLisp
;;;; Author:        Kevin M. Rosenberg
;;;; Date Started:  May 2003
;;;;
;;;; $Id$
;;;;
;;;; This file, part of UMLisp, is
;;;;    Copyright (c) 2000-2002 by Kevin M. Rosenberg, M.D.
;;;;
;;;; UMLisp users are granted the rights to distribute AND use this software
;;;; as governed by the terms of the GNU General Public License.
;;;; *************************************************************************

(in-package #:umlisp-tests)

(setq *rt-basic*
  '(
    (deftest :qrystr/1 (umlisp::query-string mrconso (cui lui) nil nil nil)
      "SELECT CUI,LUI FROM MRCONSO")

    (deftest :qrystr/1e (umlisp::query-string-eval 'mrconso '(cui lui) nil nil nil)
      "SELECT CUI,LUI FROM MRCONSO")

    (deftest :qrystr/2 (umlisp::query-string mrconso (cui lui) 0 nil nil)
      "SELECT CUI,LUI FROM MRCONSO AND KCUILRL<=0")

    (deftest :qrystr/2e (umlisp::query-string-eval 'mrconso '(cui lui) 0 nil nil)
      "SELECT CUI,LUI FROM MRCONSO AND KCUILRL<=0")

    (deftest :qrystr/3 (umlisp::query-string mrconso (cui lui) nil cui 5)
      "SELECT CUI,LUI FROM MRCONSO WHERE CUI='5'")

    (deftest :qrystr/3e (umlisp::query-string-eval 'mrconso '(cui lui) nil 'cui 5)
      "SELECT CUI,LUI FROM MRCONSO WHERE CUI='5'")

    (deftest :qrystr/4 (umlisp::query-string mrconso (cui lui) nil kpfstr "Abc")
      "SELECT CUI,LUI FROM MRCONSO WHERE KPFSTR='Abc'")

    (deftest :qrystr/4e (umlisp::query-string-eval 'mrconso '(cui lui) nil 'kpfstr "Abc")
      "SELECT CUI,LUI FROM MRCONSO WHERE KPFSTR='Abc'")

    (deftest :qrystr/5 (umlisp::query-string mrconso (cui lui) 2 cui 5 :single t)
      "SELECT CUI,LUI FROM MRCONSO WHERE CUI='5' AND KCUILRL<=2 LIMIT 1")

    (deftest :qrystr/5e (umlisp::query-string-eval 'mrconso '(cui lui) 2 'cui 5 :single t)
      "SELECT CUI,LUI FROM MRCONSO WHERE CUI='5' AND KCUILRL<=2 LIMIT 1")

    (deftest :qrystr/6 (umlisp::query-string mrdef (sab def) 2 cui 39 :lrl ksrl :single t)
      "SELECT SAB,DEF FROM MRDEF WHERE CUI='39' AND KSRL<=2 LIMIT 1")

    (deftest :qrystr/6e (umlisp::query-string-eval 'mrdef '(sab def) 2 'cui 39 :lrl 'ksrl :single t)
      "SELECT SAB,DEF FROM MRDEF WHERE CUI='39' AND KSRL<=2 LIMIT 1")

    (deftest :qrystr/7 (umlisp::query-string mrdef (sab def) 2 cui 39 :lrl ksrl :order (cui asc))
      "SELECT SAB,DEF FROM MRDEF WHERE CUI='39' AND KSRL<=2 ORDER BY CUI asc")

    (deftest :qrystr/7e (umlisp::query-string-eval 'mrdef '(sab def) 2 'cui 39 :lrl 'ksrl :order '(cui asc))
      "SELECT SAB,DEF FROM MRDEF WHERE CUI='39' AND KSRL<=2 ORDER BY CUI asc")

    (deftest :qrystr/8 (umlisp::query-string mrdef (sab def) 2 cui 39 :lrl ksrl
                        :order (cui asc def desc))
      "SELECT SAB,DEF FROM MRDEF WHERE CUI='39' AND KSRL<=2 ORDER BY CUI asc,DEF desc")

    (deftest :qrystr/8e (umlisp::query-string-eval 'mrdef '(sab def) 2 'cui 39 :lrl 'ksrl
                         :order '(cui asc def desc))
      "SELECT SAB,DEF FROM MRDEF WHERE CUI='39' AND KSRL<=2 ORDER BY CUI asc,DEF desc")

    (deftest :ui/1 (umlisp::parse-cui "C0002341") 2341)
    (deftest :ui/2 (umlisp::parse-lui "L0002341") 2341)
    (deftest :ui/3 (umlisp::parse-sui "S0000000") 0)
    (deftest :ui/4 (umlisp::parse-tui "T123") 123)
    (deftest :ui/5 (fmt-cui 2341) "C0002341")
    (deftest :ui/6 (fmt-lui 2341) "L0002341")
    (deftest :ui/7 (fmt-sui 2341) "S0002341")
    (deftest :ui/8 (fmt-tui 231) "T231")
    (deftest :ui/9 (fmt-tui 231) "T231")
    (deftest :ui/10 (fmt-eui 231) "E0000231")
    (deftest :ui/11 (umlisp::make-cuisui 5 11) 50000011)
    (deftest :ui/12 (umlisp::decompose-cuisui 50000011) 5 11)
    (deftest :ui/13 (umlisp::parse-eui "E00002311") 2311)
    (deftest :ui/14 (umlisp::parse-lui "1234") 1234)
    (deftest :ui/15 (umlisp::parse-lui 1234) 1234)

    ))
