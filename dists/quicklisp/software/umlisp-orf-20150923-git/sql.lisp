;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: umlisp -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:     sql.lisp
;;;; Purpose:  Low-level SQL routines data for UMLisp
;;;; Author:   Kevin M. Rosenberg
;;;; Created:  Apr 2000
;;;;
;;;; $Id$
;;;;
;;;; This file, part of UMLisp, is
;;;;    Copyright (c) 2000-2004 by Kevin M. Rosenberg, M.D.
;;;;
;;;; UMLisp users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License.
;;;; *************************************************************************

(in-package #:umlisp-orf)

(defvar +umls-sql-map+
    '((:2002ad . "KUMLS2002AD") (:2003aa . "KUMLS2003AA")
      (:2003ab . "KUMLS2003AB") (:2003ac . "KUMLS2003AC")
      (:2004aa . "KUMLS2004AA")))
(defvar +default-umls-db+ :2003ac)


(defun lookup-db-name (db)
  (cdr (assoc (ensure-keyword db) +umls-sql-map+)))

(defvar *umls-sql-db* +default-umls-db+)
(defun umls-sql-db ()
  *umls-sql-db*)

(defun umls-sql-db! (db)
  (setq *umls-sql-db* db))

(defvar *umls-sql-user* "secret")
(defun umls-sql-user ()
  *umls-sql-user*)
(defun umls-sql-user! (u)
  (sql-disconnect-pooled)
  (setq *umls-sql-user* u))

(defvar *umls-sql-passwd* "secret")
(defun umls-sql-passwd ()
  *umls-sql-passwd*)
(defun umls-sql-passwd! (p)
  (sql-disconnect-pooled)
  (setq *umls-sql-passwd* p))

(defvar *umls-sql-host* "localhost")
(defun umls-sql-host ()
  *umls-sql-host*)
(defun umls-sql-host! (h)
  (sql-disconnect-pooled)
  (setq *umls-sql-host* h))

(defvar *umls-sql-type* :mysql)
(defun umls-sql-type ()
  *umls-sql-type*)
(defun umls-sql-type! (h)
  (sql-disconnect-pooled)
  (setq *umls-sql-type* h))

(defun sql-connect ()
  "Connect to UMLS database, automatically used pooled connections"
  (clsql:connect (list *umls-sql-host* (lookup-db-name *umls-sql-db*)
                       *umls-sql-user* *umls-sql-passwd*)
                 :database-type *umls-sql-type* :pool t))

(defun sql-disconnect (conn)
  "Disconnect from UMLS database, but put connection back into pool"
  (clsql:disconnect :database conn))

(defun sql-disconnect-pooled ()
  (clsql:disconnect-pooled))

(defmacro with-sql-connection ((conn) &body body)
  `(let ((,conn (sql-connect)))
     (unwind-protect
         (progn ,@body)
       (when ,conn (clsql:disconnect :database ,conn)))))

(defun sql (stmt conn)
  (if (string-equal "SELECT" (subseq stmt 0 6))
      (sql-query stmt conn)
    (sql-execute stmt conn)))

(defun sql-query (cmd conn &key (result-types :auto))
  (clsql:query cmd :database conn :result-types result-types :field-names nil))

(defun sql-execute (cmd conn)
  (clsql:execute-command cmd :database conn))

(defun umls-sql (stmt)
  (check-type stmt string)
  (with-sql-connection (conn)
    (sql stmt conn)))

;;; Pool of open connections

(defmacro with-mutex-sql ((conn) &body body)
  `(let ((,conn (sql-connect)))
     (unwind-protect
         (progn ,@body)
       (when ,conn (sql-disconnect ,conn)))))

(defun mutex-sql-execute (cmd)
  (with-mutex-sql (conn)
    (sql-execute cmd conn)))

(defun mutex-sql-query (cmd &key (result-types :auto))
  (with-mutex-sql (conn)
    (sql-query cmd conn :result-types result-types)))
