;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: umweb -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          modlisp.asd
;;;; Purpose:       ASDF system definition file for modlisp package
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  Dec 2002
;;;;
;;;; $Id$
;;;; *************************************************************************

(in-package #:cl-user)

(defpackage #:modlisp-system (:use #:cl #:asdf))
(in-package #:modlisp-system)

#+(or allegro cmu lispworks sbcl)
(defsystem modlisp
    :depends-on (:kmrcl)
    :components
    ((:file "package")
     (:file "variables" :depends-on ("package"))
     (:file "base" :depends-on ("variables"))
     (:file "utils" :depends-on ("base"))
     (:file "demo" :depends-on ("utils"))))
