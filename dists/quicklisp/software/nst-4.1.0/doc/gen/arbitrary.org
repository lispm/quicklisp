# DO NOT EDIT --- generated from source code.
#+LaTeX: \label{arbitrary:primary}
This function takes a single argument, which determines the
type of the value to be generated.  For simple types, the name of the type (or
the class object, such as returned by =find-class= by itself is a complete
specification.  For more complicated types, =arbitrary= can also take a list
argument, where the first element gives the type and the remaining elements are
keyword argument providing additional requirements for the generated value.

