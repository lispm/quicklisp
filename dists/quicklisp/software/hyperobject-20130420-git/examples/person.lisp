;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          hyperobject-example.lisp
;;;; Purpose:       Hyperobject Example file
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  Oct 2002
;;;;
;;;; A simple example file for hyperobjects
;;;;
;;;; $Id$
;;;;
;;;; This file is Copyright (c) 2000-2002 by Kevin M. Rosenberg
;;;;
;;;; *************************************************************************

(in-package :hyperobject-user)

(defclass person (hyperobject)
  ((first-name :value-type (varchar 20) :initarg :first-name :accessor first-name
               :value-constraint stringp :null-allowed nil)
   (last-name :value-type (varchar 30) :initarg :last-name :accessor last-name
              :value-constraint stringp
              :hyperlink find-person-by-last-name :null-allowed nil)
   (full-name :value-type string :stored nil)
   (dob :value-type integer :initarg :dob :accessor dob :print-formatter format-date
        :value-constraint integerp :input-filter convert-to-date)
   (resume :value-type string :initarg :resume :accessor resume
           :value-constraint stringp)
;;   (addresses :value-type (list-of subobject) :initarg :addresses :accessor addresses))
   (addresses :subobject t :initarg :addresses :accessor addresses))
  (:metaclass hyperobject-class)
  (:default-initargs :first-name "" :last-name "" :dob 0 :resume nil)
  (:default-print-slots first-name last-name dob resume)
  (:user-name "Person")
  (:user-name-plural "Persons")
  (:description "A Person")
  (:direct-rules
   (:rule-1 (:dependants (last-name first-name) :volatile full-name)
              (setf full-name (concatenate 'string first-name " " last-name)))))

(defun format-date (ut)
  (when (typep ut 'integer)
      (multiple-value-bind (sec min hr day mon year dow daylight-p zone)
          (decode-universal-time ut)
        (declare (ignore daylight-p zone))
        (format nil "~[Mon~;Tue~;Wed~;Thu~;Fri~;Sat~;Sun~], ~d ~[Jan~;Feb~;Mar~;Apr~;May~;Jun~;Jul~;Aug~;Sep~;Oct~;Nov~;Dec~] ~d ~2,'0d:~2,'0d:~2,'0d"
                dow
                day
                (1- mon)
                year
                hr min sec))))

(defclass address (hyperobject)
  ((title :value-type (varchar 20) :initarg :title :accessor title
          :value-constraint stringp)
   (street :value-type (varchar 30) :initarg :street :accessor street
           :value-constraint stringp)
   (phones :subobject t :initarg :phones :accessor phones))
  (:metaclass hyperobject-class)
  (:default-initargs :title nil :street nil)
  (:user-name "Address" "Addresses")
  (:default-print-slots title street)
  (:description "An address"))

(defclass phone (hyperobject)
  ((title :value-type (varchar 20) :initarg :title :accessor title
          :value-constraint stringp)
   (phone-number :value-type (varchar 16) :initarg :phone-number :accessor phone-number
                 :value-constraint stringp))
  (:metaclass hyperobject-class)
  (:user-name "Phone Number")
  (:user-name-plural "Phone Numbers")
  (:default-initargs :title nil :phone-number nil)
  (:default-print-slots title phone-number)
  (:description "A phone number"))


(defparameter home-phone-1 (make-instance 'phone :title "Voice" :phone-number "367-9812"))
(defparameter home-phone-2 (make-instance 'phone :title "Fax" :phone-number "367-9813"))

(defparameter office-phone-1 (make-instance 'phone :title "Main line" :phone-number "123-0001"))
(defparameter office-phone-2 (make-instance 'phone :title "Staff line" :phone-number "123-0002"))
(defparameter office-phone-3 (make-instance 'phone :title "Fax" :phone-number "123-0005"))

(defparameter home (make-instance 'address :title "Home" :street "321 Shady Lane"
                                  :phones (list home-phone-1 home-phone-2)))

(defparameter office (make-instance 'address :title "Office" :street "113 Main St."
                                    :phones (list office-phone-1 office-phone-2 office-phone-3)))


(defparameter mary (make-instance 'person :first-name "Mary" :last-name "Jackson"
                            :dob (get-universal-time)
                            :addresses (list home office)
                            :resume "Style & Grace"))


(format t "~&Text Format~%")
(view mary :subobjects t)

(format t "~&XML Format with field labels and hyperlinks~%")
(view mary :subobjects t :category :xml-link-labels)
