;;; Copyright (c) Juha Arpiainen 2005-06

;;; This file is part of Bourbaki.

;;; Bourbaki is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.

;;; Bourbaki is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Bourbaki; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; -------

;;; *****
;;; Structured proofs
;;; *****

(in-package :bourbaki)

(defun provenp (wff)
  (loop for lev in *current-proof*
        if (in-tree lev wff)
          return t))

;;; Traverse structured proof
;;; TODO: bind line number?
(defmacro do-proof ((var pf) &body body)
  (with-gensyms (rec)
    `(labels ((,rec (,var)
                (if (subproof-ref ,var)
                  (progn ,@body)
                  (dolist (s (subproof-sub ,var))
                    (,rec s)))))
       (,rec ,pf))))

;;; Perform operation fn for thr, each theorem referenced in thr's proof
;;; and so on, in topologically sorted order.
;;; If a cycle is found, signal error
(defun walk-proof-tree (thr fn &optional (nested nil))
  (let ((table (make-hash-table :test 'eq)))
    (labels ((walk (thr fn)
               (case (gethash thr table)
                 ;; TODO: print the whole cycle
                 (:active (error "Cyclic theorem reference: ~A" (context-name thr)))
                 (:done t)
                 (otherwise
                   (setf (gethash thr table) :active)
                   (if nested ;; TODO: walk exports also?
                     (loop for sym being the hash-values in (context-syms thr)
                           do (aif (symref-target sym)
                                (walk it fn))))
                   (aif (context-proof thr)
                     (do-proof (i it)
                       (walk (car (subproof-ref i)) fn)))
                   (prog1
                     (funcall fn thr)
                     (setf (gethash thr table) :done))))))
      (walk thr fn))))
