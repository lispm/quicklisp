;;;  ss.lisp --- stateful stream implementation

;;;  Copyright (C) 2005, 2008 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: cobstor

#+cmu (ext:file-comment "$Module: ss.lisp $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

;;; Commentary:
;;;
;;; This module requires Gray streams.  It extends the concept of a
;;; stream adding the possibility to get the position within a stream
;;; in terms of line and column.  A possible use for this kind of
;;; streams is file parsers, where it's desirable to report to the
;;; user errors positions in a human-readable way.
;;;
;;; "Stateful Stream" is a blatant demonstration of lack of
;;; imagination for names.

(in-package :cl-user)

#+cmu (ext:without-package-locks
	(unless (member :gray-streams *features*)
	  (load "library:subsystems/gray-streams-library")))

(in-package :cl-user)

(defpackage :stateful-streams
  (:use :common-lisp
	#+cmu :extensions
	#+sbcl :sb-gray)
  (:export #:open
	   #:stream-offset
	   #:stateful-stream
	   #:real-stream
	   #:line
	   #:column
	   #:off-line #:off-column
	   #:beginning-of-line-p
	   #:get-position)
  (:shadow #:open))

(in-package :stateful-streams)

(defclass stream-offset ()
  ((line :initarg :line
	 :reader off-line)
   (column :initarg :column
	   :reader off-column))
  (:documentation
   "Class for objects representing offset position within a
stateful stream. These are the kind of objects returned by
GET-POSITION method on a stateful stream."))

(defclass stateful-stream (fundamental-character-input-stream)
  ((column :type integer
	   :reader column
	   :initform 0)
   (line :type integer
	 :reader line
	 :initform 0)
   (real-stream :type stream
		:initarg :stream
		:reader real-stream
		:documentation
		"The stream this stateful stream encapsulates.")
   (bol :type boolean
	:initform t
	:reader beginning-of-line-p
	:documentation
	"True if stream is currently at the beginning of a line.")))

(defmethod print-object ((obj stateful-stream) stream)
  (with-slots (real-stream) obj
    (print-unreadable-object (obj stream :type t :identity t)
      (format stream "real-stream=~A" real-stream))))

(defmethod print-object ((obj stream-offset) stream)
  (cond (*print-pretty*
	 (format stream "line ~A col ~A"
		 (off-line obj) (off-column obj)))
	(t (print-unreadable-object (obj stream :type t)
	     (format stream "line=~A columns=~A"
		     (off-line obj) (off-column obj))))))

(defmethod stream-read-char ((stream stateful-stream))
  (with-slots (real-stream line column bol) stream
    (let ((c (read-char real-stream nil :eof)))
      (when bol
	(setf column 0)
	(incf line)
	(setf bol nil))
      (incf column)
      (when (eq c #\newline)
	(setf bol t))
      c)))

(defmethod close ((stream stateful-stream) &key abort)
  (with-slots (real-stream) stream
    (close real-stream :abort abort)))

(defun open-stateful-stream (pathname &rest open-options)
  (make-instance 'stateful-stream :stream (apply #'open pathname open-options)))

(defmacro with-open-stateful-stream ((stream pathname &rest open-options) &body body)
  `(let ((,stream (open-stateful-stream ,pathname ,@open-options)))
     (unwind-protect (progn ,@body)
       (close stream))))

;; Considering that it's forbidden to unread more than one character
;; in a row the BOL flag helps us to keep LINE and COLUMN in a
;; consistent state: LINE is increased only upon returning the first
;; character of the line (after the newline), too late to back up a
;; newline and no need to guess what the previous line's column was.
(defmethod stream-unread-char ((stream stateful-stream) char)
  (with-slots (real-stream line column bol) stream
    (decf column)
    (when (char= char #\newline)
      (setf bol nil))
    (unread-char char real-stream)))

(defmethod stream-peek-char ((stream stateful-stream))
  (peek-char nil (real-stream stream) nil :eof))

(defgeneric get-position (stream)
  (:documentation
   "Return current position of stream.  The position may be an
object of the class STREAM-OFFSET."))

(defmethod get-position ((stream stateful-stream))
  (with-slots (line column) stream
    (make-instance 'stream-offset :line line :column column)))

;; Override the Common Lisp OPEN.  This one accepts the :stateful
;; keyword.  The upshot is that even WITH-OPEN-FILE will accept the
;; :stateful keyword and use STATEFUL-STREAM objects instead of normal
;; streams.
(defun open (filename &rest options &key stateful &allow-other-keys)
  (if stateful
    (progn
      (remf options :stateful)
      (let ((stream (apply #'common-lisp:open filename options)))
	(make-instance 'stateful-stream :stream stream)))
    ;; if stateful wasn't specified, behave like normal OPEN
    (apply #'common-lisp:open filename options)))
