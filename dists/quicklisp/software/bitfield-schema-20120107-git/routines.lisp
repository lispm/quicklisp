
(in-package :bitfield-schema)

(defun put-integer (integer bit-field offset width)
  "Put WIDTH bits of given INTEGER into simple-bit-vector BIT-FIELD starting at OFFSET."
  (declare (optimize (speed 3) (safety 0))
           (type fixnum integer offset width)
           (type (simple-bit-vector *) bit-field))
  (iter (declare (declare-variables))
        (for (the fixnum field-index) from offset below (the fixnum (+ offset width)))
        (setf (elt bit-field field-index) (logand integer 1))
        (setf integer (ash integer -1)))
  (values bit-field (the fixnum (+ offset width))))

(defun put-integer/grow (integer bit-field offset width)
  "Put WIDTH bits of given INTEGER into BIT-FIELD simple-bit-vector at OFFSET.
Twices the BIT-FIELD size of it's current length less than (+ OFFSET WIDTH) by
creating a new simple-bit-vector and copying the old data into it."
  (declare (optimize (speed 3) (safety 0))
           (type fixnum integer offset width)
           (type (simple-bit-vector *) bit-field))
  (let ((expected-size (+ offset width)))
    (declare (type fixnum expected-size))
    (iter (while (> expected-size (length bit-field)))
          (let ((tmp-buffer (make-array (* (1+ (length bit-field)) 2) :element-type 'bit)))
            (replace tmp-buffer bit-field)
            (setf bit-field tmp-buffer))))
  (put-integer integer bit-field offset width))

(defun get-integer (bit-field offset width)
  "Get WIDTH bits of an integer from simple-bit-vector BIT-FIELD starting at OFFFSET."
  (declare (optimize (speed 3) (safety 0))
           (type fixnum offset width)
           (type (simple-bit-vector *) bit-field))
  (iter (declare (declare-variables))
        (with (the fixnum integer) = 0)
        (for (the fixnum field-index) from (the fixnum (1- (+ offset width))) downto offset)
        (setf integer (ash integer 1)
              integer (logior integer (the fixnum (elt bit-field field-index))))
        (finally (return (values integer (the fixnum (+ offset width)))))))

(defmacro get-integer/fixed (bit-field offset width)
  "Effectively get WIDTH bits of an integer from simple-bit-vector BIT-FIELD starting
at OFFFSET (only when WIDTH is a number)."
  (assert (numberp width))
  (let ((bit-field-var (gensym "BIT-FIELD"))
        (offset-var (gensym "OFFSET")))
    `(let ((,bit-field-var ,bit-field)
           ,@(unless (numberp offset) `((,offset-var ,offset))))
       (logior ,@(iter (for field-index from 0 below width)
                       (for elt-form = `(elt ,bit-field-var
                                             ,(if (numberp offset)
                                                  (+ offset field-index)
                                                  (if (zerop field-index)
                                                      offset-var
                                                      `(+ ,offset-var ,field-index)))))
                       (if (zerop field-index)
                           (collect elt-form)
                           (collect `(ash ,elt-form ,field-index))))))))

