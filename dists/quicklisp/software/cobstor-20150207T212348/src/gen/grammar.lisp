;;;  grammar.lisp --- cobol record description grammar

;;;  Copyright (C) 2005, 2009 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: cobstor

#+cmu (ext:file-comment "$Module: grammar.lisp $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

(in-package :cobstor-generator)

(deflazy define-grammar
  (let ((*package* #.*package*)
	(*compile-print* (and npg::*debug* t)))
    (reset-grammar)
    (format t "~&creating Cobol grammar...~%")
    (populate-grammar)
    (let ((grammar (generate-grammar)))
      (reset-grammar)
      (print-grammar-figures grammar)
      grammar)))

(defun populate-grammar ()

(defrule data-description-entry
    := "FD" cobol-identifier label-records-clause? "." :rest
    :reduce (list :fd cobol-identifier)
    := level-number name/filler? data-description-entry-clause* "." :rest
    :reduce (append (list level-number name/filler)
		    (alist->plist data-description-entry-clause))
    := :eof)

(defrule name/filler
    := data-name
    := "FILLER"
    :reduce (list))

(defrule data-description-entry-clause
    := picture-clause
    := redefines-clause
    := occurs-clause
    := sign-clause
    := usage-clause
    := indexed-clause)

(defrule redefines-clause
    := "REDEFINES" data-name
    :reduce `(:redefines ,data-name))

(defrule zeroes
    := "ZERO"
    := "ZEROS"
    := "ZEROES")

(defrule level-number
    := integer)

(defrule picture-clause
    := picture "IS"? picture-string
    :reduce (list :picture picture-string))

(defrule picture
    := "PICTURE"
    := "PIC")

(defrule sign-clause
    := sign-is? alt-leading-trailing separate-character?
    :reduce (list :separate-sign separate-character :sign-position alt-leading-trailing))

(defrule sign-is
    := "SIGN" "IS"?)

(defrule separate-character
    := "SEPARATE" "CHARACTER"?
    :reduce t)

(defrule alt-leading-trailing
    := "LEADING"
    :reduce :leading
    := "TRAILING"
    :reduce :trailing)

(defrule usage-clause
    := usage-is? usage
    :reduce (list :encoding usage))

(defrule usage-is
    := "USAGE" "IS"?)

(defrule usage
    := "BINARY"
    :reduce 'binary
    := "COMP"
    :reduce 'comp
    := "COMP-1"
    :reduce 'comp1
    := "COMP-2"
    :reduce 'comp2
    := "COMP-3"
    :reduce 'comp3
    := "COMP-4"
    :reduce 'comp4
    := "COMP-5"
    :reduce 'comp5
    := "COMP-6"
    :reduce 'comp6
    := "COMPUTATIONAL"
    :reduce 'comp
    := "COMPUTATIONAL-1"
    :reduce 'comp1
    := "COMPUTATIONAL-2"
    :reduce 'comp2
    := "COMPUTATIONAL-3"
    :reduce 'comp3
    := "COMPUTATIONAL-4"
    :reduce 'comp4
    := "COMPUTATIONAL-5"
    :reduce 'comp5
    := "COMPUTATIONAL-6"
    :reduce 'comp6
    := "PACKED-DECIMAL"
    :reduce 'packed
    := "DISPLAY"
    :reduce nil)

(defrule data-name
    := cobol-identifier)

(defrule are
    := "ARE"
    := "IS")

(defrule label-records-clause
    := "LABEL" records-are label-records-clause-tail
    :reduce (list :label-record label-records-clause-tail))

(defrule data-names
    := data-name+)

(defrule label-records-clause-tail
    := "STANDARD" :reduce :standard
    := "OMITTED" :reduce :omitted
    := data-names)

(defrule records-are
    := records are?)

(defrule records
    := "RECORD"
    := "RECORDS")

(defrule occurs-clause
    := "OCCURS" integer "TIMES"?
    :reduce (list :times integer))

(defrule indexed-clause
    := "INDEXED" "BY" cobol-identifier
    :reduce (list :indexed cobol-identifier))

(defrule cobol-identifier
    := identifier)

)					; end of POPULATE-GRAMMAR
