;;;; json-responses.lisp

(in-package #:ca.pinecode.json-responses)

(defun set-headers-from-alist (headers)
  (dolist (header headers)
    (setf (header-out (car header)) (cdr header))))

(defun json-response (&key (status +http-ok+) (headers nil) (error nil) (data nil))
  (setf (content-type*) "application/json")
  (setf (return-code*) status)
  (if headers
      (set-headers-from-alist headers))
  (if (or data error)
      (encode-json-plist-to-string
       (list "error" error "data" data))))

(defmacro define-json-response (name status &optional (error nil))
  `(defun ,name (&key (headers nil) (error ,error) (data nil))
     (json-response :status ,status
                    :headers headers
                    :error error
                    :data data)))

;; 1xx
(define-json-response continue-response +http-continue+)
(define-json-response switching-protocols-response +http-switching-protocols+)

;; 2xx
(define-json-response ok-response +http-ok+)
(define-json-response created-response +http-created+)
(define-json-response accepted-response +http-accepted+)
(define-json-response non-authoritative-information-response
    +http-non-authoritative-information+)
(define-json-response no-content-response +http-no-content+)
(define-json-response reset-content-response +http-reset-content+)
(define-json-response partial-content-response +http-partial-content+)

;; 3xx
(define-json-response multiple-choices-response +http-multiple-choices+)
(define-json-response moved-permanently-response +http-moved-permanently+)
(define-json-response moved-temporarily-response +http-moved-temporarily+)
(define-json-response see-other-response +http-see-other+)
(define-json-response not-modified-response +http-not-modified+)
(define-json-response use-proxy-response +http-use-proxy+)
(define-json-response temporary-redirect-response +http-temporary-redirect+)

;; 4xx
(define-json-response bad-request-response +http-bad-request+ "Bad request")
(define-json-response authorization-required-response
    +http-authorization-required+ "Authorization required")
(define-json-response forbidden-response +http-forbidden+ "Forbidden")
(define-json-response not-found-response +http-not-found+ "Not found")
(define-json-response method-not-allowed-response +http-method-not-allowed+
  "Method not allowed")
(define-json-response not-acceptable-response +http-not-acceptable+
  "Not acceptable")
(define-json-response proxy-authentication-required-response
    +http-proxy-authentication-required+ "Proxy authentication required")
(define-json-response request-timeout-response +http-request-time-out+
  "Request timeout")
(define-json-response conflict-response +http-conflict+ "Conflict")
(define-json-response gone-response +http-gone+ "Gone")
(define-json-response length-required-response +http-length-required+
  "Length required")
(define-json-response precondition-failed-response +http-precondition-failed+
  "Precondition failed")
(define-json-response request-entity-too-large-response
    +http-request-entity-too-large+ "Request entity too large")
(define-json-response request-uri-too-long-response
    +http-request-uri-too-large+ "Request URI too long")
(define-json-response unsupported-media-type-response
    +http-unsupported-media-type+ "Unsupported media type")
(define-json-response requested-range-not-satisfiable-response
    +http-requested-range-not-satisfiable+ "Requested range not satisfiable")
(define-json-response expectation-failed-response +http-expectation-failed+
  "Expectation failed")

;; 5xx
(define-json-response internal-server-error-response +http-internal-server-error+
  "Internal server error")
(define-json-response not-implemented-response +http-not-implemented+
  "Not implemented")
(define-json-response bad-gateway-response +http-bad-gateway+ "Bad gateway")
(define-json-response service-unavailable-response +http-service-unavailable+
  "Service unavailable")
(define-json-response gateway-timeout-response +http-gateway-time-out+
  "Gateway timeout")
(define-json-response http-version-not-supported-response
    +http-version-not-supported+ "HTTP version not supported")
