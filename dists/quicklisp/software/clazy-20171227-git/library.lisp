;;;; -*- Mode: Lisp -*-

;;;; library.lisp --
;;;; Some library functions useful with CLAZY.
;;;;
;;;; See COPYING file for copyright and licensing information.


(in-package "CLAZY")

;;;;===========================================================================
;;;; Usual stream things.

;;; lazy-stream, lazy-cons --
;;; Definitions and manipultion functions.

(defstruct (lazy-stream
            (:constructor %cons-lazy-stream (head &optional
                                                  (tail (make-thunk nil)))))
  "The Lazy Stream Structure.

The traditional implementation of SICP-like streams. The LAZY-STREAM
structure has a strict head and a lazy tail."
  (head () :read-only t) ; Maybe I'll revert to mutable later on. MA 20090219.
  (tail () :read-only t)
  )


(defstruct (lazy-cons
            (:include lazy-stream)
            (:constructor %cons-lazy-cons (head &optional
                                                (tail (make-thunk nil)))))
  "The Lazy Cons Structure.

The LAZY-CONS structure has both head and tail lazy.  It includes LAZY-STREAM.")


(defmacro cons-stream (head tail)
  "Constructs a LAZY-STREAM, DELAYing the TAIL argument."
  `(%cons-lazy-stream ,head (delay ,tail)))


;;; head, tail -- They operate on lists as well.

(defun head (s)
  "Returns the first element of a SEQUENCE or a LAZY-STREAM."
  (declare (type (or sequence lazy-stream) s))
  (etypecase s
    (list (first s))
    (vector (aref s 0))
    (lazy-cons (force (lazy-cons-head s)))
    (lazy-stream (lazy-stream-head s))))


(defun tail (s)
  "Returns the rest of a LIST or LAZY-STREAM."
  (declare (type (or list lazy-stream) s))
  (etypecase s
    (list (rest s))
    (lazy-stream (force (lazy-stream-tail s)))))


(defmethod print-object ((ls lazy-stream) s)
  (print-unreadable-object (ls s :type t :identity t)
    (format s "[~S ...]" (head ls))))


(defmethod print-object ((ls lazy-cons) s)
  (print-unreadable-object (ls s :type t :identity t)
    (if (thunk-p (lazy-cons-head ls))
        (write-string "[(LAMBDA () ...)" s)
        (format s "[~S" (head ls)))
    (if (lazy-cons-tail ls)
        (write-string " ...]" s)
        (write-string "]" s))))



;;;---------------------------------------------------------------------------
;;; Lazy versions of major functions.
;;;
;;; These are useful mostly within "slacking" blocks.
;;; The list is incomplete.  New ones can be added later.

;;; Note:
;;; We cannot define the lazy version of CONS and CONS-STREAM in the
;;; way I'd hoped, because of the way the thunking machinery works.

;;; cons-stream --
;;; Lazy version of the macro; which we don't actually define because
;;; it'd interfere with LAZILY.

#|
;;; Wrong version
(def-lazy-function cons-stream (head tail)
  (%cons-lazy-cons head (delay tail)))

;;; Working version
(def-lazy-function cons-stream (&rest head-tail)
  ;; (assert (= (list-length head-tail) 2))
  (%cons-lazy-cons (force (first head-tail)) (second head-tail)))
|#


;;; SBCL and CLISP fascist package locks are the reason for the
;;; hairiness following.

#+sbcl #| has-fascist-package-locks |#
(eval-when (:compile-toplevel :load-toplevel :execute)
  (sb-ext:unlock-package "CL"))

#+clisp #| has-fascist-package-locks |#
(eval-when (:compile-toplevel :load-toplevel :execute)
  (setf (ext:package-lock "CL") nil))



;;; lazy cons -- Let's (almost) put our money where our mouth is.

#| Nice version but...
(def-lazy-function cons (car cdr)
  ;; (assert (= (list-length car-cdr) 2))
  (%cons-lazy-cons (delay car) (delay cdr)))
|#

(def-lazy-function cons (&rest car-cdr)
  ;; (assert (= (list-length car-cdr) 2))
  (%cons-lazy-cons (first car-cdr) (second car-cdr)))


;;; lazy car, cdr --

(def-lazy-function car (cons)
  (head cons))


(def-lazy-function cdr (cons)
  (tail cons))


;;; list -- Same as for CONS
;;; This will become useful later as well.

(def-lazy-function list (&rest args)
  ;; ARGS is actually a list of THUNKS.  Since that's the case, I can
  ;; just fill up a nice set of lazy-cons'es.

  (labels ((lazy-cons (thunks)
             (if (null thunks)
                 () ; Rather arbitrary, but consistent with CL.
                 (%cons-lazy-cons (first thunks)
                                  (lazy-cons (rest thunks)))))
           )
    (lazy-cons args)))


;;; repeatedly --
;;; it is just a simple function.

(defun repeatedly (fn &rest args)
  "Returns a lazy list containing repeated applications of FN to ARGS."
  (lazy:call 'cons
             (apply fn args)
             (apply 'repeatedly fn args)))



;;;; szergling (Yong) ideas

;;; slacking, lazily --
;;; These macro are very useful, yet obviously not right.  Think of
;;; (let ((cons 1 2)) ...)
;;;
;;; The reason is that there is *NO* decent code walker out there and
;;; that it is impossible to implement one as the CLtL2 environment
;;; functions are not supported by all implementations (besides, it
;;; may be that these facilities are insufficient themselves.)

(defmacro slacking (&body body) ; with-laziness
  (labels ((transform (form)
             (if (atom form)
                 form
                 (destructuring-bind (name . args)
                     form
                   (if (lazy-function-name-p name)
                       `(call ',name ,@(mapcar #'transform args))
                       form)))))
    `(block slacking
       ,.(mapcar #'transform body))))


(defmacro lazily (&body body) ; with-laziness
  "Ensures that lazy functions are called lazily with LAZY:CALL.

The macro code-walks BODY and whenever it finds an application that
has a lazily defined operator, then it rewrites such application with
LAZY:CALL."
  (labels ((transform (form)
             (if (atom form)
                 form
                 (destructuring-bind (name . args)
                     form
                   (if (lazy-function-name-p name)
                       `(call ',name ,@(mapcar #'transform args))
                       form)))))
    `(block slacking
       ,.(mapcar #'transform body))))


#|  This is quite incorrect.  Need help to fix it.
(def-lazy-function map (result-type function &rest seqs)
  (ecase result-type
    (lazy-stream
     (lazily
       (cons (apply function (mapcar #'head seqs))
             (apply (lazy map)
                    result-type
                    function
                    (mapcar #'tail seqs)))))
    (lazy-stream
     (cons-stream (apply function (mapcar #'head seqs))
                  (apply (lazy map)
                         result-type
                         function
                         (mapcar #'rest seqs))))))
|#


#| More szergling ideas.  LETREC/LAZY is actually just LET/LAZY.

(defmacro letrec/lazy (bindings &body body)
  (let* ((bindings
          (mapcar (lambda (x)
                    (if (and (list x)
                             (= 2 (length x)))
                        x
                        (list x nil)))
                  bindings))
         (vars (mapcar #'car bindings))
         (val-forms (mapcar #'cadr bindings))
         (gvars (mapcar (lambda (x)
                          (gensym (symbol-name x)))
                        vars))
         (fun-names (mapcar (lambda (x)
                              (gensym (format nil "GET-~a" x)))
                            vars))
         (value (gensym "VALUE-"))
         )

    (flet ((make-getter (var fun-name)
             `(,fun-name () ,var))
           (make-setter (var fun-name)
             `((setf ,fun-name) (,value)
               (setf ,var ,value)))
           (make-symbol-macro (var fun-name)
             `(,var (,fun-name)))
           (make-initialiser (var val-form)
             `(setf ,var ,val-form)))
      `(let ,vars
         (flet (,@(mapcar #'make-getter vars fun-names)
                ,@(mapcar #'make-setter vars fun-names))
           (symbol-macrolet ,(mapcar #'make-symbol-macro
                                     vars fun-names)
             ,@(mapcar #'make-initialiser vars val-forms)
             ,@body))))))
|#

#|
(letrec/lazy ((x (call 'lcons 1 x)))
  (list-force 10 x))

(let/lazy ((x (call 'lcons 1 x)))
  (list-force 10 x))
|#


(defun diverge ()
  "A function that never returns.

It is equivalent to (LOOP).  It should not be called striclty: called
striclty: it will never terminate."
  (loop))

#+sbcl #| has-fascist-package-locks |#
(eval-when (:compile-toplevel :load-toplevel :execute)
  (sb-ext:lock-package "CL"))

#+clisp #| has-fascist-package-locks |#
(eval-when (:compile-toplevel :load-toplevel :execute)
  (setf (ext:package-lock "CL") t))



;;;;===========================================================================
;;;; Tests also counting as a library.
#|
(defun integers-starting-from (n)
  (lazy:call 'cons n (integers-starting-from (1+ n))))

(defparameter naturals (integers-starting-from 0))
|#
;;;; end of file -- library.lisp --
