;;; -*- mode: lisp; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; polynom.lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2003 Nicolas Neuss, University of Heidelberg.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF HEIDELBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.function)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; class of multivariate polynomials (over arbitrary rings)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass polynomial (<vector>)
  ((coeffs :reader coefficients :initarg :coeffs))
  (:documentation "Multivariate polynomial.  The coefficients are
represented as nested lists.  A special case are 0-variate polynomials
which are simply scalars."))

;;; polynomial constructor (from coefficient list)

;;; important, since redundancy may be introduced during arithmetic
;;; operations we have to strip trailing zeros from a coefficient list

(defun simplify (list)
  (labels ((empty? (list)
	     (or (null list)
		 (and (null (cdr list))
		      (listp (car list))
		      (empty? (car list))))))
    (cond
      ((numberp list) list)
      ((empty? list) list)
      (t (let ((rest (simplify (cdr list))))
           (if (empty? rest)
               (if (listp (car list))
                   (list (simplify (car list)))
                   (if (zero? (car list))
                       rest
                       (cons (car list) rest)))
               (if (listp (car list))
                   (cons (simplify (car list)) rest)
                   (cons (car list) rest))))))))

(defun check-and-calculate-depth-of (object)
  "Calculates the depth of an object -usually a list- and asserts that it
is uniform."
  (lret (global-depth)
    (flet ((set-and-check-global-depth (d)
             (if global-depth
                 (assert (= d global-depth))
                 (setf global-depth d))))
      (named-let rec ((object object) (depth 0))
        (cond ((null object)
               (set-and-check-global-depth (1+ depth)))
              ((atom object)
               (set-and-check-global-depth depth))
              (t (rec (first object) (1+ depth))
                 (awhen (rest object) (rec it depth))))))))

(defun make-polynomial (coeffs)
  "Constructor which simplifies the coefficient list."
  (check-and-calculate-depth-of coeffs)
  (let ((simplified (simplify coeffs)))
    (check-and-calculate-depth-of simplified)
    (make-instance 'polynomial :coeffs simplified)))

(defgeneric zero (f)
  (:documentation "Generates a zero of the same kind as @arg{F}.")
  (:method ((f number)) 0)
  (:method ((f list))
    (if (or (null f) (numberp (first f)))
        ()
        (list (zero (first f)))))
  (:method ((f polynomial))
    (make-polynomial
     (zero (coefficients f)))))

(defgeneric unit (f)
  (:documentation "Generates a unit of the same kind as @arg{F}.")
  (:method ((f number)) 1)
  (:method ((f list))
    (if (or (null f) (numberp (first f)))
        (list 1)
        (list (unit (first f)))))
  (:method ((f polynomial))
    (make-polynomial
     (unit (coefficients f)))))

;;; checks
(defgeneric zero? (x)
  (:method ((x number)) (zerop x))
  (:method ((list list)) (null list))
  (:method ((poly polynomial))
    (zero? (coefficients poly))))

(defgeneric unit? (x)
  (:method ((x number)) (= x 1))
  (:method ((list list)) (and (single? list) (unit? (car list))))
  (:method ((poly polynomial))
    (unit? (coefficients poly))))

;;; other functions

(defun nest (x n)
  "Nest x into n levels of parentheses."
  (if (zerop n)
      x
      (list (nest x (1- n)))))

(defun k-variate-zero (k)
  "A k-variate zero coefficient"
  (if (zerop k)
      0
      (nest nil (1- k))))

(defgeneric shift-polynomial (poly shift &optional from)
  (:documentation "Shifts a polynomial in dimension, i.e. variables
starting from index k>=@arg{from} get index k+shift.")
  (:method ((poly number) shift &optional (from 0))
    (assert (zerop from))
    (nest poly shift))
  (:method ((poly list) shift &optional (from 0))
    (when (null poly)
      (return-from shift-polynomial
        (k-variate-zero (+ shift from))))
    (if (zerop from)
        (nest poly shift)
        (mapcar (lambda (coeff)
                  (shift-polynomial coeff shift (1- from)))
                poly)))
  (:method ((poly polynomial) shift &optional (from 0))
    (make-polynomial
     (shift-polynomial (coefficients poly)
                       shift from))))

(defgeneric degree (poly)
  (:documentation "Degree of a polynomial")
  (:method ((poly polynomial))
    (- (length (coefficients poly)) 1)))

(defgeneric total-degree (poly)
  (:documentation "Degree of a multivariate polynomial")
  (:method ((poly polynomial))
    (labels ((tensorial-degree (coeff-list)
               (apply #'max
                      (mapcar #'(lambda (coeff deg)
                                  (if (listp coeff)
                                      (+ deg (tensorial-degree coeff))
                                      deg))
                              coeff-list
                              (loop for k below (length coeff-list) collect k)))))
      (tensorial-degree (coefficients poly)))))

(defgeneric partial-degree (poly index)
  (:documentation "Partial degree in variable INDEX of a multivariate polynomial.")
  (:method ((list list) (index integer))
    (if (zerop index)
        (- (length list) 1)
        (apply #'max -1
               (mapcar #'(lambda (term)
                           (partial-degree term (- index 1)))
                       list))))
  (:method ((poly polynomial) (index integer))
    (partial-degree (coefficients poly) index)))

(defgeneric variance (poly)
  (:documentation "Number of variables on which a polynomial depends.")
  (:method ((coeffs number)) 0)
  (:method ((coeffs list))
    "Nestedness of the list calculated by walking down depth-first."
    (named-let rec ((index 0)
                    (coeffs coeffs))
      (cond
        ((null coeffs) (+ index 1))
        ((listp coeffs) (rec (+ index 1) (car coeffs)))
        (t index))))
  (:method ((poly polynomial))
    (variance (coefficients poly))))

(defgeneric maximal-partial-degree (poly)
  (:documentation "Maximal partial degree of a polynomial.")
  (:method ((poly polynomial))
    (loop for k from 0 below (variance poly)
       maximize (partial-degree poly k))))

(defun eliminate-small-coefficients (poly &optional (threshold 1.0e-12))
  (make-polynomial
   (subst-if 0 #'(lambda (x) (and (numberp x) (< (abs x) threshold)))
	     (coefficients poly))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; write method
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; splits the coefficient list of an n-variate polynomial into
;;; monomials given by their exponents (1 1) = x1*x2
(defun split-into-monomials (coeffs)
  (if (numberp coeffs)
      (list (cons coeffs nil))
      (labels ((create-list (list mono)
                 (and list
                      (append
                       (if (listp (first list))
                           (create-list (first list) (cons 0 mono))
                           (list (cons (first list) mono)))
                       (create-list (rest list)
                                    (cons (1+ (car mono)) (cdr mono)))))))
        (create-list coeffs '(0)))))

(defun write-monomial (coeff&monomial stream)
  (let ((coeff (car coeff&monomial))
	(mono (cdr coeff&monomial)))
    
    ;; princ coefficient
    (if (not (unit? coeff))		; was: (and (number? coeff) (= 1 coeff)))
	(progn
	  (format stream " ~A " coeff)
	  (unless (every #'zerop mono)
	    (princ "*" stream)))
	(when (every #'zerop mono)
	  (princ "1" stream)))
    
    (unless (every #'zerop mono)
      (do ((i 1 (1+ i))
	   (mono (reverse mono) (cdr mono)))
	  ((null mono))
	(unless (zerop (car mono))
	  (princ "x" stream)
	  (princ i stream)
	  (when (> (car mono) 1)
	    (princ "^" stream)
	    (princ (car mono) stream))
	  (when (and (not (single? mono)) (not (every #'zerop (cdr mono))))
	    (princ "*" stream)))))))

(defun print-polynomial (poly &optional (stream *standard-output*) reversed)
  (let ((monomials (split-into-monomials (coefficients poly))))
    (loop with start-flag = t
       for coeff&mono in (if reversed (reverse monomials) monomials)
       unless (zerop (car coeff&mono)) do
         (if start-flag
             (setq start-flag nil)
             (princ " + " stream))
         (write-monomial coeff&mono stream))))

(defvar *print-polynomial-variance* nil
  "If T, the polynomial variance is shown when printing a polynomial.")

(defmethod print-object ((poly polynomial) stream)
  (let ((*print-circle* nil))
    (print-unreadable-object (poly stream :type t :identity t)
      (format stream "{")
      (ignore-errors (print-polynomial poly stream))
      (format stream " }~@[[~D]~]" (and *print-polynomial-variance* (variance poly))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; <function> methods
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; evaluation of a multivariate polynomial
(defun poly-eval (coeffs coords)
  (cond ((not (listp coeffs)) coeffs)
	((null coords) (or (car coeffs) 0))
	((null coeffs) 0)
	(t (+ (poly-eval (car coeffs) (cdr coords))
	      (* (car coords)
		 (poly-eval (cdr coeffs) coords))))))

(defmethod evaluate ((poly polynomial) (x list))
  (poly-eval (coefficients poly) x))
(defmethod evaluate ((poly polynomial) (x vector))
  (poly-eval (coefficients poly) (coerce x 'list)))
(defmethod evaluate ((poly polynomial) (x number))
  (poly-eval (coefficients poly) (list x)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Polynomial arithmetic
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric poly+ (p1 p2)
  (:documentation "Add two polynomials P1 and P2.")
  (:method ((x number) (y number)) (+ x y))
  ;; the following two methods are quite special for our use in polynomial
  ;; multiplication.  This is a hint that this should be done in a better
  ;; way.
  (:method ((x number) (y list))
    (if (null y) x (cons (poly+ (car y) x) (cdr y))))
  (:method ((y list) (x number))
    (if (null y) x (cons (poly+ (car y) x) (cdr y))))
  (:method ((x list) (y list))
    (cond ((null x) y)
          ((null y) x)
          (t (cons (poly+ (car x) (car y))
                   (poly+ (cdr x) (cdr y))))))
  (:method ((x polynomial) (y polynomial))
    (assert (= (variance x) (variance y)))
    (make-polynomial (poly+ (coefficients x) (coefficients y))))
  #+(or)
  (:method ((x polynomial) (y number))
    (make-polynomial (poly+ (coefficients x) (list y))))
  #+(or)
  (:method ((x number) (y polynomial))
    (make-polynomial (poly+ (list x) (coefficients y))))
  )

(defgeneric poly* (p1 p2)
  (:documentation "Multiplies two polynomials P1 and P2.")
  (:method ((f number) (g number)) (* f g))
  (:method ((f number) (g list)) (scal f g))
  (:method ((g list) (f number)) (scal f g))
  (:method ((f number) (g polynomial)) (scal f g))
  (:method ((g polynomial) (f number)) (scal f g))
  (:method ((f list) (g list))
    "The assumption is that f and g are coefficient lists with the same
variance."
    (assert (= (variance f) (variance g)))
    (let ((zero (zero f)))
      (cond
        ((or (zero? f) (zero? g)) zero)
      (t (poly+ (mapcar #'(lambda (x) (poly* (first f) x)) g)
                (cons (zero (first f))
                      (aand (rest f) 
                            (poly* it g))))))))
  (:method ((f polynomial) (g polynomial))
    (make-polynomial (poly* (coefficients f) (coefficients g)))))

(defun poly-exterior-product (poly1 poly2)
  "Multiply the polynomials poly1 and poly2 considered as polynomials in
separate variables."
  (lret* ((k1 (variance poly1))
          (k2 (variance poly2))
          (result (poly* (shift-polynomial poly1 k2 k1)
                         (shift-polynomial poly2 k1))))
    (assert (= (variance result) (+ k1 k2)))))

;;; exponentiation

(defgeneric poly-expt (p n)
  (:documentation "Raises the polynomial P to power N.")
  (:method ((list list) (n integer))
    (cond ((< n 0) (error "not implemented"))
          ((= n 0) (list 1))
          ((= n 1) list)
          ((evenp n) (poly-expt (poly* list list) (/ n 2)))
          (t (poly* list (poly-expt list (1- n))))))
  (:method ((poly polynomial) (n integer))
    (make-polynomial (poly-expt (coefficients poly) n))))

;;; matlisp methods

(defmethod copy ((poly polynomial))
  (make-polynomial (copy (coefficients poly))))

(defmethod scal! (val (poly polynomial))
  (make-polynomial (scal! val (coefficients poly))))

(defmethod m+ ((x polynomial) (y polynomial)) (poly+ x y))

(defmethod m+! ((x polynomial) (y polynomial))
  (setf (slot-value y 'coeffs)
	(poly+ (coefficients x) (coefficients y)))
  y)

#+(or)
(defmethod m+ ((x number) (y polynomial)) (poly+ x y))

#+(or)
(defmethod m+ ((x polynomial) (y number)) (poly+ x y))

(defmethod axpy! (alpha (x polynomial) (y polynomial))
  (setf (slot-value y 'coeffs)
	(poly+ (scal! alpha (coefficients x)) (coefficients y)))
  y)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; differentiation
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric differentiate (poly index)
  (:documentation "Differentiate a multivariate polynomial wrt the variable given by INDEX.")
  (:method ((list list) index)
    (cond ((null list) ())
          ((zerop index)
           (if (single? list)
               (zero list)
               (loop for coeff in (rest list)
                  and deg from 1
                  collect (scal deg coeff))))
          (t (mapcar #'(lambda (coeff)
                         (differentiate coeff (- index 1)))
                     list))))
  (:method ((poly number) index)
    (declare (ignore index))
    0)
  (:method ((poly polynomial) index)
    (make-polynomial (differentiate (coefficients poly) index))))

;;; warning: does not work for multidimensional polynomials (e.g. p(x1,x2)=x1)
(defmethod gradient ((poly polynomial))
  (loop for index from 0 below (variance poly)
     collect (differentiate poly index)))

(defmethod evaluate-gradient ((poly polynomial) (x vector))
  (let ((result (make-array (length x))))
    (dotimes (index (length result) result)
      (setf (aref result index)
	    (evaluate (differentiate poly index) x)))))

#|
(defmethod k-jet ((poly polynomial) (k integer) (dim integer))
  (loop for order upto k
     for Di = poly then
       (let ((Dnext (make-instance (full-tensor t)
                                   :dimensions (make-fixnum-vec order dim))))
         (dotimes (index dim Dnext)
           (if (= order 1)
               (setf (tensor-ref Dnext index)
                     (differentiate poly index))
               (copy! (tensor-map (full-tensor t) (rcurry #'differentiate index) Di)
                      (slice Dnext (list (cons (1- order) index)))))))
     collecting Di))

(defmethod evaluate-k-jet ((poly polynomial) (k integer) (x vector))
  (loop with k-jet = (k-jet poly k (length x))
     for i from 0
     and Di in k-jet collecting
       (if (zerop i)
           (evaluate poly x)
           (tensor-map (full-tensor 'double-float) (rcurry #'evaluate x) Di))))
|#

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Diversities
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun exponents->monomial-list (partition)
  "Converts a monomial given by a list of powers of its components into our
dense polynomial format.

Example: (exponents->monomial-list '(1 2)) -> (() (0 0 1))"
  (if (null partition)
      1
      (append (make-list (first partition)
                         :initial-element (k-variate-zero (1- (length partition))))
              (list (exponents->monomial-list (rest partition))))))

(defun n-variate-monomials-of-degree (n degree &optional (type '=))
  "Returns n-variate monomials of degree being equal or being lower or
equal than deg.
Examples:
 (n-variate-monomials-of-degree 2 2) -> (x2^2 x1*x2 x1^2)
 (n-variate-monomials-of-degree 2 2 '<=)  -> (1 x2 x1 x2^2 x1*x2 x1^2)"
  (ecase type
    (= (loop for partition in (n-partitions-of-k n degree)
          collect (make-polynomial (exponents->monomial-list partition))))
    (<= (loop for deg from 0 upto degree
           nconcing (n-variate-monomials-of-degree n deg)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Interpolation polynomials
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun lagrange-polynomials (points)
  (loop for xi in points
     for points-without-xi = (remove xi points)
     collect
       (scal
        (/ 1 (reduce #'* (mapcar #'(lambda (xj) (- xi xj)) points-without-xi)))
        (reduce #'poly* (mapcar #'(lambda (xj)
                                    (make-polynomial (list (- xj) 1)))
                                points-without-xi)
                :initial-value (make-polynomial '(1))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Simple 1d integration
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun integrate-simple-polynomial (poly)
  (make-polynomial
   (cons 0 (if (numberp poly)
	       (list poly)
	       (loop for coeff in (coefficients poly)
                  and n+1 from 1
                  collect (/ coeff n+1))))))


;;; Testing
(defun test-polynom ()
  (assert (equalp (simplify '(0 1 0 0)) '(0 1)))
  (assert (equalp (simplify '(() (0) (1) (0) (0))) '(nil nil (1))))
  (assert (equalp (simplify '(() (0 1 0 0) (1) (0) (0))) '(nil (0 1) (1))))
  
  (integrate-simple-polynomial (make-polynomial '(0 1)))
  (coefficients (make-polynomial ()))
  (coefficients (shift-polynomial (make-polynomial '(0 1)) 1))
  (let ((p (make-polynomial '((1 2) (3 4))))
	(p-x1 (make-polynomial '((0) (1))))
	(p-x2 (make-polynomial '((0 1)))))
    (poly+ p-x1 p-x2)
    (poly+ p p-x1)
    (poly+ p p-x2)
    (poly* p-x1 p-x2)
    (poly* p p-x1)
    (gradient p)
    (differentiate p-x1 1))
  (make-polynomial '(4))

  (let* ((poly1 (make-polynomial '(() (0 1))))
         (poly2 (make-polynomial '(1 -1)))
         (poly3 (shift-polynomial poly2 1)))
    (coefficients poly1)
    (variance poly1)
    (variance poly2)
    (variance poly3)
    (format t "~A~%" (coefficients poly1))
    (format t "~A~%" (coefficients poly2))
    (format t "~A~%" (coefficients poly3))
    (coefficients (poly* poly1 poly3))
    (poly-expt poly2 3)
    (poly-exterior-product poly2 poly2))
  
  (let* ((poly1 (make-polynomial '((0 1))))
         (poly2 (make-polynomial '(0 1)))
         (poly3 (shift-polynomial poly1 1 2))
         (poly4 (shift-polynomial poly2 2)))
    (list poly3 poly4 (poly* poly3 poly4)))

  (assert (equalp (poly* '((() (1))) '(((0 1))))
                  '((() (0 1)))))
  
  (let ((poly (make-polynomial '((NIL) (NIL (0 1) (0 1))))))
    (list poly (differentiate poly 1)))
  
  (gradient (make-polynomial '((NIL) (NIL (0.0 728) (0.0 -2733)))))

  (poly-exterior-product '(() (1)) '(0 1))

  (check-and-calculate-depth-of (shift-polynomial '(() (1)) 1 2))
  
  (let* ((poly1 (make-polynomial '(() (1))))
         (poly2 (make-polynomial '(0 1)))
         (poly3 (shift-polynomial poly1 1 2))
         (poly4 (shift-polynomial poly2 2 0)))
    (poly* poly3 poly4))

  (make-polynomial 1)
  (coefficients (shift-polynomial (make-polynomial '(1 2)) 1))
  
  (evaluate (make-polynomial '(3/2 -1/3)) 3/7)
  )

;;; (test-polynom)
(fl.tests:adjoin-test 'test-polynom)

