(cl:defpackage #:eager-future
  (:export #:pcall #:pexec #:plet #:future
           #:select #:ready-to-yield? #:yield
           #:execution-error #:execution-error-cause
           #:*thread-pool* #:thread-limit
           #:thread-pool #:fixed-fifo-thread-pool)
  (:use #:common-lisp #:bordeaux-threads))
