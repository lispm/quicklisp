;;;; -*- Mode: Lisp -*-

;;;; variables.lisp --

;;;; See file COPYING for copyright licensing information.

(in-package "CL.EXT.DACF.UNIFICATION") ; DACF = Data And Control Flow.


(defun make-var-name (&optional (s (gensym "UV_")) (package *package*))
  (declare (type (or string symbol character) s))
  (intern (concatenate 'string "?" (string s)) package))


(eval-when (:load-toplevel :execute)
  (setf (fdefinition 'new-var) #'make-var-name))


(defun variablep (x)
  (and (symbolp x)
       (or (char= (char (symbol-name x) 0) #\?)
           (string= x "_"))))

(defun variable-any-p (x)
  (and (symbolp x)
       (or (string= x "_")
           (string= x "?_"))))


;;; end of file -- variables.lisp --
