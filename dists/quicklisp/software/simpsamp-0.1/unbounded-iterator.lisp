(in-package #:simpsamp)

;;; VECTOR-SAMPLE-OF-UNBOUNDED-ITERABLE most naturally belongs in
;;; out-vector.lisp.  However, out-vector.lisp depends on out-do.lisp,
;;; and out-do.lisp depends on this function, so this function gets
;;; its own source file.
(defun vector-sample-of-unbounded-iterator (n next-fn exhausted?-fn)
  (declare (type jpl-util:array-dimension n)
	   (type jpl-util:function-designator next-fn exhausted?-fn)
	   (optimize speed))
  (vector-sample-of-unbounded-iterator-expr
   n (funcall next-fn) (funcall exhausted?-fn)))
