;;; -*- mode: Lisp; Syntax: Common-Lisp; -*-
;;;
;;; Copyright (c) 2011 by the authors.
;;;
;;; See LICENCE for details.

(in-package :hu.dwim.presentation)

(def constant +scroll-x-parameter-name+ "_sx")
(def constant +scroll-y-parameter-name+ "_sy")
(def constant +no-javascript-error-parameter-name+ "_njs")

(def constant +page-failed-to-load-id+ "page-failed-to-load")
(def constant +page-failed-to-load-grace-period-in-millisecs+ 10000)
