;;; rcfiles.lisp --- Unix-like RC files for Common Lisp systems

;; Copyright (C) 2010, 2011 Didier Verna

;; Author:        Didier Verna <didier@lrde.epita.fr>
;; Maintainer:    Didier Verna <didier@lrde.epita.fr>

;; This file is part of CL-RCFiles.

;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.

;; THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


;;; Commentary:

;; Contents management by FCM version 0.1.


;;; Code:

(in-package :com.dvlsoft.rcfiles)

(defvar *directory* (merge-pathnames "share/common-lisp/rc/"
				     (user-homedir-pathname))
  "Directory where initialization files should be found.")

(defvar *pre-directory* (merge-pathnames "pre/" *directory*)
  "Directory where pre-loading initialization files should be found.")

(defvar *post-directory* (merge-pathnames "post/" *directory*)
  "Directory where post-loading initialization files should be found.")


(defmethod asdf:perform :before
    ((operation asdf:load-op) (component asdf:system))
  (let ((rcfile
	  (probe-file (merge-pathnames
		       (concatenate 'string
			 (asdf:component-name component) ".lisp")
		       *pre-directory*))))
    (when rcfile (load rcfile))))

(defmethod asdf:perform :after
    ((operation asdf:load-op) (component asdf:system))
  (let* ((rcfile-name
	   (concatenate 'string	(asdf:component-name component) ".lisp"))
	 (rcfile
	   (or (probe-file (merge-pathnames rcfile-name *post-directory*))
	       ;; for backward-compatibility
	       (probe-file (merge-pathnames rcfile-name *directory*)))))
    (when rcfile (load rcfile))))


;;; rcfiles.lisp ends here
