(defpackage :iterate-clsql
  (:use :cl :iter :clsql))

(in-package :iterate-clsql)

(defmacro-driver (FOR vars IN-CLSQL-QUERY query-expression &optional ON-DATABASE (database '*default-database*))
  (let ((result-set (gensym "RESULT-SET-"))
	(columns (gensym "COLUMNS-"))
	(row (gensym "ROW-"))
	(db (gensym "DB-"))
	(query (gensym "QUERY-"))
	(kwd (if generate 'generate 'for)))
    `(progn
       (with ,db = ,database)
       (with ,query = ,query-expression)
       (with (,result-set ,columns) = (multiple-value-list (clsql-sys:database-query-result-set ,query ,db :full-set nil :result-types :auto)))
       (finally-protected (when ,result-set (clsql-sys:database-dump-result-set ,result-set ,db)))
       (with ,row = (make-list ,columns))
       (,kwd ,vars next (progn
			  (unless (clsql-sys:database-store-next-row ,result-set ,db ,row) (terminate))
			  ,row)))))