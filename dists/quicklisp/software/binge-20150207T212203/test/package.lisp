;;;  package.lisp --- package list for all the test suites

;;;  Copyright (C) 2004 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-D52E2063D2E488ED18C1F54A37856380.lisp,v 1.4 2004/03/25 15:00:31 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-D52E2063D2E488ED18C1F54A37856380.lisp,v 1.4 2004/03/25 15:00:31 wcp Exp $")

(defpackage :binge-tests
  (:use :common-lisp
	:rtest :binge :binge-decl)
  (:export #:efs
	   #:cfs
	   #:sexp-of))
