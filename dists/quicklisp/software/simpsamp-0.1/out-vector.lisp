(in-package #:simpsamp)

(defun vector-sample-of-list (n list)
  (declare (type jpl-util:array-dimension n)
	   (type list list)
	   (optimize speed))
  ;; Unlike the other functions in out-list.lisp, we choose not to
  ;; accumulate to a vector with DO-SAMPLE-OF-LIST.  It would work,
  ;; and wouldn't cons too much, and the time complexity would be the
  ;; same, but DO-SAMPLE-OF-LIST takes the LENGTH of list before work
  ;; begins.  Since the output we need is a VECTOR, there is no
  ;; disadvantage of using reservoir sampling via
  ;; VECTOR-SAMPLE-OF-UNBOUNDED-ITERATOR-EXPR.
  (with-list-iterator (next size exhausted? list)
    (vector-sample-of-unbounded-iterator-expr
     n (next) (exhausted?))))

(defun vector-sample-of-vector (n vector)
  (declare (type jpl-util:array-dimension n)
	   (type vector vector)
	   (optimize speed))
  (with-vector-accumulator (set-size/type accumulate)
    (do-sample-of-vector (x n vector
			  :effective-n-var n
			  :initial-form (set-size/type n (array-element-type vector)))
      (accumulate x))))

(defun vector-sample-of-hash-table-keys (n hash-table)
  (declare (type jpl-util:array-dimension n)
	   (type hash-table hash-table)
	   (optimize speed))
  (with-vector-accumulator (set-size/type accumulate)
    (do-sample-of-hash-table (key value n hash-table
			      :effective-n-var n
			      :initial-form (set-size/type n 't))
      (accumulate key))))

(defun vector-sample-of-hash-table-values (n hash-table)
  (declare (type jpl-util:array-dimension n)
	   (type hash-table hash-table)
	   (optimize speed))
  (with-vector-accumulator (set-size/type accumulate)
    (do-sample-of-hash-table (key value n hash-table
			      :effective-n-var n
			      :initial-form (set-size/type n 't))
      (accumulate value))))

(defun vector-sample-of-hash-table-pairs (n hash-table)
  (declare (type jpl-util:array-dimension n)
	   (type hash-table hash-table)
	   (optimize speed))
  ;; Got a style warning here due to this code expanding to form:
  ;; (make-array ... :element-type 'cons).  The warning was that the
  ;; initial-element 0 doesn't match the type, but no :INITIAL-ELEMENT
  ;; was given to MAKE-ARRAY.  I think I'm within the spec--CLHS
  ;; MAKE-ARRAY says that the result of *reading* an uninitialized
  ;; element from an array that and wasn't made with :INITIAL-ELEMENT,
  ;; :INITIAL-CONTENTS, or :DISPLACED-TO is undefined.  I can't find
  ;; any reason to believe that the mere fact that I have
  ;; uninitialized elements in an array with an ELEMENT-TYPE would be
  ;; in error or cause undefined/unspecified behavior.
  ;; 
  ;; This is just a STYLE-WARNING on SBCL.  In-case another compiler
  ;; dies on this, change WITH-VECTOR-ACCUMULATOR to accept an initial
  ;; element as well, and in this case, the initial element may be a
  ;; static cons (e.g. '(nil . nil)).
  (with-vector-accumulator (set-size/type accumulate)
    (do-sample-of-hash-table (key value n hash-table
			      :effective-n-var n
			      :initial-form (set-size/type n 'cons))
      (accumulate (cons key value)))))

(defun vector-sample-of-range (n lower upper)
  (declare (type jpl-util:array-dimension n)
	   (type integer lower upper)
	   (optimize speed))
  (with-vector-accumulator (set-size/type accumulate)
    (do-sample-of-range (x n lower upper
			 :effective-n-var n
			 :initial-form (set-size/type n 'integer))
      (accumulate x))))

(defun vector-sample-of-iterator (n size next-fn)
  (declare (type jpl-util:array-dimension n)
	   (type (integer 0) size)
	   (type jpl-util:function-designator next-fn)
	   (optimize speed))
  (with-vector-accumulator (set-size/type accumulate)
    (do-sample-of-iterator ((x) n size next-fn
			    :effective-n-var n
			    :initial-form (set-size/type n 't))
      (accumulate x))))

;;; VECTOR-SAMPLE-OF-UNBOUNDED-ITERATOR is defined in
;;; unbounded-iterator.lisp.
