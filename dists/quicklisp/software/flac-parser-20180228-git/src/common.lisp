(in-package :flac-parser)

(defun seek (file &optional offset (start :current))
  (with-slots (stream) (buffer file)
    (when offset
      (ecase start
        (:start (file-position stream offset))
        (:end (file-position stream (- (file-length stream) offset)))
        (:current (file-position stream (+ (file-position stream) offset)))))
    (file-position stream)))

(defun read-ubytes-le (file count)
  (let ((value 0))
    (loop :for i :below (* 8 count) :by 8
          :for byte = (fast-io:fast-read-byte (buffer file))
          :collect (setf (ldb (byte 8 i) value) byte))
    value))

(defun read-ubytes-be (file count)
  (let ((value 0))
    (loop :for i :from (* (1- count) 8) :downto 0 :by 8
          :for byte = (fast-io:fast-read-byte (buffer file))
          :collect (setf (ldb (byte 8 i) value) byte))
    value))

(defun sign-extend (value size)
  (logior (* (ldb (byte 1 (1- size)) value)
             (- (expt 2 size)))
          value))

(defmethod parse (file (node (eql :string)) &key (bytes 1) (encoding :ascii))
  (let* ((octets (parse file :octets :count bytes))
         (end (or (position 0 octets :test #'=) (length octets))))
    (babel:octets-to-string octets :end end :encoding encoding)))

(defmethod parse (file (node (eql :integer)) &key (bytes 1) (unsignedp t) (endian :big))
  (flet ((maybe-convert (reader)
           (let ((value (funcall reader file bytes)))
             (if unsignedp value (sign-extend value bytes)))))
    (ecase endian
      (:big (maybe-convert #'read-ubytes-be))
      (:little (maybe-convert #'read-ubytes-le)))))

(defmethod parse (file (node (eql :octets)) &key (count 1))
  (let ((octets (fast-io:make-octet-vector count)))
    (fast-io:fast-read-sequence octets (buffer file) 0 count)
    octets))

(defmethod parse (file (node (eql :bits)) &key octets (count 1) (offset 0) (endian :big))
  (flet ((bits->int (vec)
           (bit-smasher:bits->int
            (subseq (bit-smasher:octets->bits vec) offset (+ count offset)))))
    (ecase endian
      (:big (bits->int octets))
      (:little (bits->int (reverse octets))))))
