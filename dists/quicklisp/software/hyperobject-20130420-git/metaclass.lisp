;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          metaclass.lisp
;;;; Purpose:       Define options for hyperobject metaclass
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  Apr 2000
;;;;
;;;;
;;;; $Id$
;;;;
;;;; This file is Copyright (c) 2000-2006 by Kevin M. Rosenberg
;;;; *************************************************************************

(in-package #:hyperobject)

(defparameter *class-options*
  '(:user-name :default-print-slots :description :version :sql-name
    :guid :version :direct-functions :direct-views :direct-rules)
  "List of class options for hyperobjects.")

(defparameter *slot-options*
  '(:value-type :print-formatter :description :short-description :user-name
    :subobject :hyperlink :hyperlink-parameters :indexed :inverse :unique
    :sql-name :null-allowed :stored :input-filter :unbound-lookup
    :value-constraint :void-text :read-only-groups :hidden-groups :unit
    :compute-cached-value :disable-predicate :view-type :list-of-values)
  "Slot options that can appear as an initarg")

(defparameter *slot-options-no-initarg*
  '(:ho-type :sql-type :sql-length)
  "Slot options that do not have an initarg")

