;;;  file.lisp --- proxy layer for Cobol files

;;;  Copyright (C) 2005, 2006, 2007, 2008, 2009 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: cobstor

#+cmu (ext:file-comment "$Module: file.lisp $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

(in-package :cobstor)

(defvar *keep-agent-connections* t
  "If true agent connections are not severed upon exit of
WITH-CBL-FILE.")

(defvar *agent-source-type* "cob"
  "Pathname type of the source files of the Cobol agent.")

(defvar *agent-compiled-type* "acu"
  "Pathname type of the compiled files of the Cobol agent.")

(defvar *agents-library-name* NIL
  "Pathname of the Cobol library containing all the agents.  If
NIL cobstor will run the single agents using the
COMPILED-AGENT-FILE-PATHNAME function.")

(defvar *runcbl-pathname* #P"runcbl"
  "Pathname to the runcbl executable (or wrapper).  Although a
relative pathname would be resolved by the Unix shell search
path, it's a bad idea to rely on that.")

(defun agent-file-pathname (name)
  "Return the absolute pathname of the Cobol agent source file
named NAME."
  (make-pathname :defaults
		 (translate-logical-pathname "cobstor-dynamic:agent;")
		 :name (string-downcase (string name))
		 :type *agent-source-type*))

(defun compiled-agent-file-pathname (name)
  "Return the absolute pathname of the compiled Cobol agent
program named NAME."
  (make-pathname :defaults
		 (translate-logical-pathname "cobstor-dynamic:agent;")
		 :name (string-downcase (string name))
		 :type *agent-compiled-type*))

(defun data-file-pathname (name)
  "Return the absolute pathname of the Cobol data file named
NAME."
  (make-pathname :defaults (translate-logical-pathname "cobstor-data:")
		 :name (string-downcase (string name))
		 :type nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *cobol-files* '()
  "List of files declared with DECLARE-COBOL-DATA-FILE.")

(defmacro declare-cobol-data-file (name record keys)
  "Add a data file to the *AGENT-TABLE*."
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (setf (get ',name 'cbl-record) (list ',record ,keys)
	   (get ',record 'cbl-file) ',name)
     (pushnew ',name *cobol-files*)))

(defun cbl-file-metadata (file-name)
  "Find a data file in *DATA-FILES-TABLE*."
  (get file-name 'cbl-record))

(defgeneric cbl-record-file (record))

(defmethod cbl-record-file ((record-name symbol))
  (get record-name 'cbl-file))

(defmethod cbl-record-file ((record cbl-record))
  (cbl-record-file (type-of record)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; cobol run-time errors are a two bytes code, the first gives the
;; major error type, the seconds gives further details
(defstruct cbl-file-status
  major
  minor
  extended)

(defun file-error-p (status)
  (not (zerop (cbl-file-status-major status))))

(defun not-found-p (status)
  (and (= (cbl-file-status-major status) 2)
       (= (cbl-file-status-minor status) 3)))

(defun already-exists-p (status)
  (and (= (cbl-file-status-major status) 2)
       (= (cbl-file-status-minor status) 2)))

(defun no-current-record-p (status)
  (and (= (cbl-file-status-major status) 4)
       (= (cbl-file-status-minor status) 6)))

(defun eof-p (status)
  (and (= (cbl-file-status-major status) 1)
       (= (cbl-file-status-minor status) 0)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-condition cbl-file-condition (cbl-condition)
  ()
  (:documentation
   "Base abstract class for all the conditions related to the use
of a CBL-FILE."))

(defun print-cbl-file-error-condition (condition stream)
  (with-slots (description) condition
    (format stream "DB error \"~A\"" description)))

(define-condition cbl-file-error (cbl-file-condition)
  ((description :initarg :description))
  (:report print-cbl-file-error-condition))

(defun print-cbl-file-cobol-error-condition (condition stream)
  (format stream "DB error; file status=~A"
	  (cbl-error-status condition)))

(define-condition cbl-file-cobol-error (cbl-file-condition)
  ((status :initarg :status
	   :initform nil
	   :reader cbl-error-status))
  (:report print-cbl-file-cobol-error-condition))

(defun print-cbl-protocol-error-condition (condition stream)
  (with-slots (description) condition
    (format stream "DB protocol error \"~A\"" description)))

(define-condition cbl-protocol-error (cbl-file-condition)
  ((description :initarg :description))
  (:report print-cbl-protocol-error-condition))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass cbl-file ()
  ((name :type symbol
	 :initarg :name
	 :reader cbl-file-name
	 :documentation
	 "Name of the file and the Cobol agent.")
   (record-type :initarg :record-type
		:type (or class symbol)
		:reader cbl-file-record-type
		:documentation
		"The class of this file's records.")
   (keys :initarg :keys
	 :type list
	 :reader cbl-file-keys
	 :documentation
	 "List of file keys this file supports.")
   (process :initarg :process)
   (port-number :initarg :port
		:reader cbl-file-port
		:documentation
		"The port number this Cobol agent answers connections.")
   (socket :initarg :socket
	   :reader cbl-file-socket
	   :documentation
	   "The socket connected to the Cobol agent.")))

(defmethod print-object ((object cbl-file) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "~A" (cbl-file-name object))))

(defgeneric cbl-file-input (file))
(defgeneric cbl-file-output (file))
(defgeneric cbl-close (file))
(defgeneric cbl-exit (file))
(defgeneric cbl-disconnect (file &key abort))
(defgeneric cbl-read (file record &key key not-found-errorp not-found-value))
(defgeneric cbl-write (file record &key if-exists))
(defgeneric cbl-rewrite (file record &key if-not-found))
(defgeneric cbl-start (file record &key compare-op key))
(defgeneric cbl-next (file &optional record))
(defgeneric cbl-previous (file &optional record))
(defgeneric cbl-delete (file record &key inexistent-errorp))
(defgeneric cbl-delete-file (file))
(defgeneric cbl-set-pathname (file pathname))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod cbl-file-input ((file cbl-file))
  (net4cl:socket-stream (slot-value file 'socket)))

(defmethod cbl-file-output ((file cbl-file))
  (net4cl:socket-stream (slot-value file 'socket)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *agent-base-port* 4224
  "Portnumber at which the Cobol agent serves connections.")

(defvar *agent-handles-by-name* (make-hash-table)
  "The active connections to Cobol agents keyed by name.")

(defvar *agent-handles-by-port* (make-hash-table)
  "The active connections to Cobol agents keyed by socket port.")

(defun find-running-agent (name)
  (gethash name *agent-handles-by-name*))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; GET-RUNNING-AGENT and RELEASE-RUNNING-AGENT are the core of a
;; simple locking mechanism that avoids that nested client code will
;; use the same file handler yielding unexpected results (at best a
;; Cobol error 91).

(defun get-running-agent (name)
  "If it exists, return a running agent for NAME file.  In doing
so, remove it from the names hash table so the subsequent calls
to this function won't find the same agent."
  (be agent (find-running-agent name)
    (when agent
      ;; remove only from the names hash so that the next call to
      ;; this function won't find it
      (remhash name *agent-handles-by-name*)
      agent)))

(defun release-running-agent (file)
  "Opposite of GET-RUNNING-AGENT.  Return FILE to the names hash
table so that FIND-RUNNING-AGENT will find it."
  (setf (gethash (cbl-file-name file) *agent-handles-by-name*) file))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun find-running-agent-by-port (port-number)
  (gethash port-number *agent-handles-by-port*))

(defun free-port-p (port)
  "Check whether port is currently free to be used by a local
server."
  (be socket (ignore-errors (net4cl:make-server-socket port))
    (when socket
      (net4cl:close-socket socket)
      t)))

(defun next-agent-port (&optional (base *agent-base-port*))
  (loop
     for x from base by 1
     unless (or (find-running-agent-by-port x)
		(not (free-port-p x)))
     return x))

(defun add-running-agent (file)
  (setf (gethash (cbl-file-name file) *agent-handles-by-name*) file)
  (setf (gethash (cbl-file-port file) *agent-handles-by-port*) file))

(defun delete-running-agent (file)
  (remhash (cbl-file-name file) *agent-handles-by-name*)
  (remhash (cbl-file-port file) *agent-handles-by-port*))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun open-pipe (fmt &rest args)
  "Return a process object from which the command output/input
will be read/written."
  #+cmu (ext:run-program *bourne-shell*
			 (list "-c" (apply #'format nil fmt args))
			 :output :stream :pty nil
			 :error t :input :stream :wait nil)
  #+sbcl (sb-ext:run-program *bourne-shell*
			     (list "-c" (apply #'format nil fmt args))
			     :output :stream :pty nil
			     :error t :input :stream :wait nil)
  #-(or cmu sbcl)
  (error "OPEN-PIPE not implemented for target Lisp system"))

(defvar *debug* nil
  "If set to T, this library will produce more output.")

(defun run-async-program (fmt &rest args)
  "Run a shell program asynchronously (in background)."
  (be command (apply #'format nil fmt args)
    (format *trace-output* "running: ~A~%" command)
    #+cmu (ext:run-program *bourne-shell* (list "-c" command)
			   :wait nil
			   :pty nil
			   :input nil
			   :output (if *debug* t nil))
    #+sbcl (sb-ext:run-program *bourne-shell* (list "-c" command)
			       :wait nil
			       :pty nil
			       :input nil
			       :output (if *debug* t nil))))

(defun buffer (n)
  "Make an (UNSIGNED-BYTE 8) vector of length N."
  (make-sequence '(vector (unsigned-byte 8)) n))

(defun integer->byte-vector (integer length)
  (be sequence (buffer length)
    (loop
       for i from (1- length) downto 0
       do (setf (elt sequence i)
		(mod integer 256))
	 (setf integer (truncate integer 256)))
    sequence))

(defun byte-vector->integer (sequence)
  (be value 0
    (dotimes (i (length sequence))
      (setf value (+ (* value 256) (elt sequence i))))
    value))

(defun decode-file-status (status)
  "Read the file status from STREAM and return a CBL-FILE-STATUS
object."
  (flet ((digit-value (byte)
	   (position byte #.(string->byte-vector "0123456789"))))
    (let ((major (elt status 0))
	  (minor (elt status 1)))
      (acond ((digit-value major)
	      (make-cbl-file-status :major it
				    :minor (if (= it 9)
					       minor
					       (digit-value minor))
				    :extended (when (not (zerop it))
						(byte-vector->string status :start 2 :end 7))))
	     ((not (= major #.(char-code #\P)))
	      (error 'cbl-protocol-error :description
		     (format nil "illegal file status ~S.~S" major minor)))
	     (t (make-cbl-file-status :major major :minor minor))))))

;; Depending on the IPC mechanism we choose, streams can be byte or
;; character based.  We need byte streams, but if we use the pipes
;; mechanims we get only character streams, hence we have to insert
;; yet another conversion layer.

(defun rdseq (seq stream)
  (read-sequence seq stream))

(defun wrseq (seq stream)
  "Like write-sequence but considers the element type of the
stream."
  (write-sequence seq stream))

(defun receive-sequence (stream length)
  (be sequence (buffer length)
    (rdseq sequence stream)
    sequence))

(defun receive-message (file)
  "Receive a message from agent.  Anything coming from the agent
is preceded by a two bytes length of the following byte
sequence."
  (be stream (cbl-file-input file)
    (receive-sequence stream
		      (byte-vector->integer
		       (receive-sequence stream 2)))))

(defun send-message (file sequence)
  "Send a message to agent."
  (be stream (cbl-file-output file)
    (wrseq (integer->byte-vector (length sequence) 2) stream)
    (wrseq sequence stream)
    (finish-output stream)))

(defun send-command (file command &optional record key)
  (let ((stream (cbl-file-output file))
	(command-length (+ 3		; the command code
			   (if key 2 0)	; the key
			   (cond ((not record) 0)
				 ((typep record 'cbl-record)
				  (record-size record))
				 (t (length record))))))
    (wrseq (integer->byte-vector command-length 2) stream)
    (wrseq (string->byte-vector command) stream)
    (when record
      (if (typep record 'cbl-record)
	  (write-record record stream)
	  (wrseq record stream)))
    ;; the optional key is last
    (when key
      (wrseq (integer->byte-vector key 2) stream))
    (finish-output stream)))

(defun receive-answer (file &optional record)
  (let* ((message (receive-message file))
	 (status (decode-file-status message)))
    (when (eq (cbl-file-status-major status) #.(char-code #\P))
      (error 'cbl-protocol-error :description
	     (format nil "protocol error \"~S~S\" from agent"
		     (cbl-file-status-major status)
		     (cbl-file-status-minor status))))
    ;; if expecting also the record data, extract it
    (when (and record
	       (> (length message) 2))
      (replace (if (typep record 'cbl-record)
		   (record-storage record)
		   record)
	       message :start2 7))
    status))

(defun receive-ok (file &optional record)
  (be status (receive-answer file record)
    (when (file-error-p status)
      (error 'cbl-file-cobol-error :status status))
    status))

(defun simple-command (file command)
  (send-command file command)
  (receive-ok file))

(defun receive-string-message (file)
  (string-right-trim #.(s+ +whitespace+ (string #\null))
		     (byte-vector->string (receive-message file))))

(defun query-record-length (file)
  (send-command file "SIZ")
  (be buffer (buffer 16)
    (receive-ok file buffer)
    (parse-integer (byte-vector->string buffer) :junk-allowed t)))

(defun receive-name (handle)
  (receive-string-message handle))

(defun initial-handshake (handle)
  "Do some sanity checks on the cbl-file HANDLE, just in case the
agent doesn't correspond to the proxy stub."
  (let* ((message (receive-string-message handle))
	 (name (car (last (split-at +whitespace+ message)))))
    (unless (string-equal name (cbl-file-name handle))
      (error 'cbl-file-error :description
	     (format nil "File name mismatch; agent reports ~A while we expect ~A."
		     name (cbl-file-name handle)))))
  (let ((actual-length (query-record-length handle))
	(expected-length (record-size (cbl-file-record-type handle))))
    (unless (= actual-length expected-length)
      (error 'cbl-file-error :description
	     (format nil "File record size mismatch; agent reports ~D while we expect ~D."
		     actual-length expected-length)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun cbl-ensure-connection (file-name)
  "Return a handle of a connection to a Cobol agent.  If
*KEEP-AGENT-CONNECTIONS* is true the handle may already exist in
the cache, in this case no new connection is created."
  (if *keep-agent-connections*
      (or (get-running-agent file-name)
	  (be file (cbl-connect file-name)
	    (add-running-agent file)
	    (get-running-agent file-name)))
      (cbl-connect file-name)))

(defun cbl-release-connection (file &key abort)
  "Dispose of a connection that is no longer necessary.  If
*KEEP-AGENT-CONNECTIONS* is true at least a connection to a each
file is kept in cache so that the next time we require to access
the same file, the burden of starting a new agent is avoided."
  (declare (type cbl-file file))
  (cond ((or abort
	     (find-running-agent (cbl-file-name file)))
	 ;; there is already a spare connection to this file in the
	 ;; cache: we cut this one
	 (cbl-disconnect file :abort abort)
	 (remhash (cbl-file-port file) *agent-handles-by-port*))
	(*keep-agent-connections*
	 ;; this was the last connection to this file: we reinsert the
	 ;; handle in the cache
	 (release-running-agent file))
	(t
	 ;; we are not caching connections
	 (cbl-disconnect file :abort abort))))

(defmacro with-cbl-file ((handle file &key pathname (direction :input)) &body body)
  (with-gensyms (panic-p data-path)
    `(let* ((,data-path ,pathname)
	    (,handle (cbl-ensure-connection ,file))
	    (,panic-p nil))
       (unwind-protect
	    (handler-case
		(progn
		  (cbl-set-pathname ,handle (or ,data-path
						(data-file-pathname (cbl-file-name ,handle))))
		  (cbl-open ,handle :direction ,direction)
		  ,@body)
	      ;; if the protocol has been violated there is not much
	      ;; point in keep trying, thus disconnect without further
	      ;; ado and propagate the error
	      (cbl-protocol-error (condition)
		(setf ,panic-p t)
		;; propagate the condition
		(error condition)))
	 (unless ,panic-p
	   ;; the cbl-open itself may have gone wrong, so don't
	   ;; produce another unnecessary error trying to close what
	   ;; hasn't been opened
	   (ignore-errors
	     (cbl-close ,handle)))
	 (cbl-release-connection ,handle :abort ,panic-p)))))

(defun try-connect-to-agent (port)
  (ignore-errors
    (net4cl:open-socket :host "localhost"
			:port port
			:buffering :none)))

(defvar *default-term* "ansi"
  "Default value for TERM envvar in case it's not set already.
This is required by runcbl which requires it despite the -b \(batch)
flag.")

(defun ensure-term-envvar ()
  (unless (getenv "TERM")
    (warn "TERM environment variable is not set; we default to ~S."
	  *default-term*)
    (setf (getenv "TERM") *default-term*)))

(defun cbl-connect (name)
  "Connect to remote agent for file of type NAME."
  (be file-desc (cbl-file-metadata name)
    (assert file-desc)
    (ensure-term-envvar)
    (destructuring-bind (record keys) file-desc
      (be* port (next-agent-port)
	   ;; if *AGENTS-LIBRARY-NAME* is not NIL use it, otherwise
	   ;; call the fully qualified pathname of the agent
	   process (run-async-program "PORT=~A exec ~A -i /dev/null ~:[-o /dev/null~;~] -b ~:[~A~*~;-y ~:*~A~* ~(~A~)~]"
				      port *runcbl-pathname* *debug*
				      *agents-library-name*
				      (compiled-agent-file-pathname name) name)
	(loop
	   for delay = 0.1 then (* delay 2)
	   for socket = (progn
			  (sleep delay) ; give the agent time to start
			  (try-connect-to-agent port))
	   for i from 0 to 7
	   when socket
	   do (be handle (make-instance 'cbl-file
					:name name
					:record-type record
					:keys keys
					:port port
					:socket socket
					:process process)
		(on-error
		    (initial-handshake handle)
		  (cbl-disconnect handle :abort t))
		(return handle))
	   finally (error 'cbl-file-error :description
			  (format nil "Can't connect to agent on port ~A for file ~A"
				  port name)))))))

(defun cbl-open (file &key (direction :input))
  "Open a remote cobol file of type NAME.  The DIRECTION can
be :INPUT, :OUTPUT or :INPUT-OUTPUT."
  (simple-command file (ecase direction
			 (:input "OIN")
			 (:output "OOU")
			 ((:input-output :io :i-o :i/o) "OIO"))))

(defmethod cbl-close ((file cbl-file))
  "Close a Cobol file that has been opened with CBL-OPEN.  This
does _not_ close the connection to the Cobol agent."
  (simple-command file "CLS"))

(defmethod cbl-exit ((file cbl-file))
  "Stop the agent and don't wait for an answer that wouldn't
come."
  (send-command file "EXT"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod cbl-disconnect ((file cbl-file) &key abort)
  "Opposite of CBL-CONNECT."
  (unless abort
    (ignore-errors
      (cbl-exit file)))
  (net4cl:close-socket (slot-value file 'socket) :abort abort)
  (be process (slot-value file 'process)
    (when process
      (unless abort
	(sleep .5))
      (when (sysproc-alive-p process)
	(sysproc-kill process :terminate))
      (sleep 1)
      (when (sysproc-alive-p process)
	(sysproc-kill process :kill))
      (sysproc-wait process))))

(defmethod cbl-read ((file cbl-file) (record cbl-record)
		     &key key not-found-errorp not-found-value)
  "Read record RECORD.  Use KEY as the index in the file.  If
record is found without errors the RECORD is returned.  If record
is missing and NOT-FOUND-ERRORP is true, signal an error
condition; NOT-FOUND-ERRORP is false return NOT-FOUND-VALUE.  On
any other error signal an error condition."
  (be key-number (if key
		     (position key (cbl-file-keys file))
		     0)
    (assert key-number)
    (send-command file "REA" record key-number))
  (be status (receive-answer file record)
    (cond ((not (file-error-p status))
	   record)
	  ((not-found-p status)
	   (if not-found-errorp
	       (error 'cbl-file-cobol-error :status status)
	       not-found-value))
	  (t (error 'cbl-file-cobol-error :status status)))))

(defmethod cbl-write ((file cbl-file) (record cbl-record) &key (if-exists :error))
  "Write RECORD to FILE.  Raise an error condition if the record
is already present and IF-EXISTS is :ERROR.  Override any
existing record if IF-EXISTS is :REPLACE."
  (send-command file "WRT" record)
  (be status (receive-answer file)
    (cond ((already-exists-p status)
	   (case if-exists
	     (:error
	      (error 'cbl-file-cobol-error :status status))
	     (:replace
	      (cbl-rewrite file record))
	     (:status
	      status)
	     (t if-exists)))
	  ((file-error-p status)
	   (error 'cbl-file-cobol-error :status status)))))

(defmethod cbl-rewrite ((file cbl-file) (record cbl-record) &key (if-not-found :error))
  "Write RECORD to FILE.  Raise an error condition if record is
not already present and IF-NOT-FOUND is :ERROR.  Insert a new
record if IF-NOT-FOUND is :INSERT."
  (send-command file "RWR" record)
  (be status (receive-answer file)
    (cond ((not-found-p status)
	   (ecase if-not-found
	     (:error
	      (error 'cbl-file-cobol-error :status status))
	     ((:add :insert)
	      (cbl-write file record))
	     (:status
	      status)))
	  ((file-error-p status)
	   (error 'cbl-file-cobol-error :status status)))))

(defmethod cbl-start ((file cbl-file) (record cbl-record) &key (compare-op :not-less) key not-found-errorp)
  "Start a sequential READ \(see CBL-NEXT) of FILE using RECORD
as initial position.  The key is compared according to
COMPARE-OP, which can take the following values:

  :NOT-LESS \(or :GREATER-OR-EQUAL)
  :LESS
  :GREATER
  :NOT-GREATER \(or :LESS-OR-EQUAL)

The semantics are identical to the familiar Cobol START
statement.  Return true if the initial record was found.  If no
initial record was found and NOT-FOUND-ERRORP is not true return
false.  Otherwise signal an error."
  (be key-number (if key
		     (position key (cbl-file-keys file))
		     0)
    (assert key-number)
    (send-command file (ecase compare-op
			 ((:not-less :greater-or-equal) "S>=")
			 (:less "S<<")
			 (:greater "S>>")
			 ((:not-greater :less-or-equal) "S<="))
		  record key-number))
  (be status (receive-answer file)
    (cond ((not (file-error-p status))
	   t)
	  ((and (not-found-p status)
		(not not-found-errorp))
	   nil)
	  (t (error 'cbl-file-cobol-error :status status)))))

;; In cbl-next and cbl-previous we handle the not-found error as well.
;; This is because the start statement may succeed even though there
;; are no records matching the required key.  Put differently, it's a
;; bug workaround.

(defmethod cbl-next ((file cbl-file) &optional record)
  "Return next record from FILE or NIL if there are no more.
FILE must be first started with CBL-FILE-START."
  (send-command file "NXT")
  (unless record
    (setf record (make-instance (cbl-file-record-type file))))
  (be status (receive-answer file record)
    (cond ((or (eof-p status)
	       (not-found-p status))
	   (values nil status))
	  ((file-error-p status)
	   (error 'cbl-file-cobol-error :status status))
	  (t
	   (values record status)))))

(defmethod cbl-previous ((file cbl-file) &optional record)
  "Return previous record from FILE or NIL if at the beginning of
the file.  FILE must be first started with CBL-FILE-START."
  (send-command file "PRV")
  (unless record
    (setf record (make-instance (cbl-file-record-type file))))
  (be status (receive-answer file record)
    (cond ((or (eof-p status)
	       (not-found-p status))
	   (values nil status))
	  ((file-error-p status)
	   (error 'cbl-file-cobol-error :status status))
	  (t
	   (values record status)))))

(defmethod cbl-delete ((file cbl-file) record &key (inexistent-errorp t)
		       inexistent-value)
  "Delete RECORD from FILE."
  (send-command file "DEL" record)
  (be status (receive-answer file)
    (cond ((not-found-p status)
	   (if inexistent-errorp
	       (error 'cbl-file-cobol-error :status status)
	       inexistent-value))
	  ((file-error-p status)
	   (error 'cbl-file-cobol-error :status status)))))

(defun cbl-unlock (file)
  "Unlock all records of FILE."
  (declare (type cbl-file file))
  (simple-command file "ULK"))

(defmethod cbl-delete-file ((file cbl-file))
  "Delete FILE and return NIL.  Signal an error if operation
didn't succeed."
  (simple-command file "DLF"))

(defmethod cbl-set-pathname ((file cbl-file) (pathname pathname))
  (send-command file "NAM" (string->byte-vector (namestring pathname)))
  (receive-ok file))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro loop-cbl-file-query ((file record starts &key compare-op key multi-record pathname fields) &body body)
  "Execute BODY within a start and next loop on FILE.  Overwrite
RECORD at each iteration with the next record in FILE.  COMPARE-OP and
KEY are the same as for CBL-START.  STARTS is a list where each
element is a two elements list: the first is the RECORD accessor for a
key field and the second is its start value.  If MULTI-RECORD is true
allocate a new record at each iteration.  If RECORD is within
parenthesis, don't allocate a new record and use one in the lexical
context.  If FIELDS is not null bind local macrolets to access
RECORD's fields.  If FIELDS is a list bind only those listed."
  (be status (if multi-record
		 record
		 (gensym))
      record-name (if (consp record)
		      (car record)
		      record)
      record-class (car (cbl-file-metadata file))
    `(with-cbl-file (,file ',file :pathname ,pathname)
       (,@(if (consp record)
	      '(progn)
	      `(be ,record-name (make-instance ',record-class)))
	  ,@(loop
	       for (accessor value) in starts
	       collect `(setf (,accessor ,record-name) ,value))
	  (,@(cond ((not fields)
		    '(progn))
		   ((consp fields)
		    `(with-record-accessors ,fields ,record-name))
		   (t `(with-record ,record-name ,record-class)))
	   (when (cbl-start ,file ,record-name
			   ,@(when key `(:key ,key))
			   ,@(when compare-op
				   `(:compare-op ,compare-op)))
	    (loop
	       for ,status = (cbl-next ,file ,@(unless multi-record (list record-name)))
	       while ,status
	       ;; this empty DO is a trick to be able to start body
	       ;; with or without a loop keyword
	       do (progn)
		 ,@body)))))))

(defun previous-record (record &optional key)
  "Fetch the record that precedes RECORD in the file."
  (handler-case
      (with-cbl-file (file (cbl-record-file (type-of record)))
	(cbl-start file record :key key :compare-op :less)
	(cbl-previous file))
    (cbl-file-cobol-error (c)
      (if (no-current-record-p (cbl-error-status c))
	  nil
	  (error c)))))

(defun next-record (record &optional key)
  "Fetch the record that follows RECORD in the file."
  (handler-case
      (with-cbl-file (file (cbl-record-file (type-of record)))
	(cbl-start file record :key key :compare-op :greater)
	(cbl-next file))
    (cbl-file-cobol-error (c)
      (if (no-current-record-p (cbl-error-status c))
	  nil
	  (error c)))))

(defun record-key (record &optional key)
  "Return the value of the key of RECORD.  KEY, if specified is a
symbol.  If KEY is not specified, return the default key."
  (be key-name (or key
		   (car (cadr (cbl-file-metadata (cbl-record-file (type-of record))))))
    (funcall (intern (s+ (string key-name) "-STORAGE") (symbol-package key-name)) record)))

(defun previous-key (record &optional key)
  "Return the key of the record preceding RECORD in the file."
  (awhen (previous-record record key)
    (record-key it key)))

(defun next-key (record &optional key)
  "Return the key of the record following RECORD in the file."
  (awhen (next-record record key)
    (record-key it key)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun load-proxy (name)
  (if (pathnamep name)
      (load name)
      (load (make-pathname :defaults "cobstor-dynamic:proxy;"
			   :name (string-downcase (string name))
			   :type nil
			   :version :newest))))

(defun load-all-proxies ()
  (dolist (file (remove-duplicates (directory "cobstor-dynamic:proxy;*")
				   :test #'string-equal :key #'pathname-name))
    (load (make-pathname :defaults file
			 :type nil	; let LOAD decide what to load
			 :version :newest))))

(defun clear-agents-pool (&optional abort)
  "Severe all the existing connections to Cobol agents and remove
the entries from the *agent-handles-by-name* and
*agent-handles-by-port* hash tables."
  (loop
     for file being the hash-value of *agent-handles-by-port*
     do (cbl-disconnect file :abort abort))
  (clrhash *agent-handles-by-name*)
  (clrhash *agent-handles-by-port*))
