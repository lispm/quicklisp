(in-package :femlisp-ddo-test)

;;; When used interactively:
;;; (lfarm:end-kernel)
;;; (connect-to-mpi-workers)

;;; (ddo (load "~/CL-HOME/femlisp/src/femlisp-ddo/test-elahom-script.lisp"))
(defparameter *n* (mpi-size))
(sleep (* .1 (mpi-rank)))
(format t "Rank ~D CPU affinity: ~A~%" (mpi-rank) (princ-to-string (cl-cpu-affinity:get-cpu-affinity-mask)))
(force-output)

;;(progn #+(or) (fl.parallel::end-kernel) (fl.parallel::new-kernel) nil)

#+(or)
(pwork (_ (format t "CPU affinity: ~A~%" (princ-to-string (sb-cpu-affinity:get-cpu-affinity-mask)))
          (force-output)))
(format t "lparallel::*kernel*=~A~%" lparallel::*kernel*)

;; (dbg-off :communication :distribute)
(dbg-on :log-times :synchronization-time-in-approximation-step)
(setq *debug-show-data* nil)
(dbg-off :log-times :synchronization-time-in-approximation-step)

(fl.konwihr:initialize-konwihr-paper-calculation)

;;; actual calculation
(format t "~D- starting calculation~%" (mpi-rank)) (force-output)
(let ((ddo::*report-ranks* '(0)))
  (elahom-calculation 4 1))

(sb-ext:exit)
