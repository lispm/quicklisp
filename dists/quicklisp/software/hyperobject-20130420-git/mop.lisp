;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          mop.lisp
;;;; Purpose:       Metaobject Protocol Interface
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  Apr 2000
;;;;
;;;; This metaclass as functions to classes to allow display
;;;; in Text, HTML, and XML formats. This includes hyperlinking
;;;; capability and sub-objects.
;;;;
;;;; $Id$
;;;;
;;;; This file is Copyright (c) 2000-2006 by Kevin M. Rosenberg
;;;; *************************************************************************

(in-package #:hyperobject)

;; Main class

(defclass hyperobject-class (standard-class)
  ( ;; slots initialized in defclass
   (user-name :initarg :user-name :type string :initform nil
              :accessor user-name
              :documentation "User name for class")
   (user-name-plural :initarg :user-name-plural :type string :initform nil
                     :accessor user-name-plural
                     :documentation "Plural user name for class")
   (default-print-slots :initarg :default-print-slots :type list :initform nil
                        :accessor default-print-slots
                        :documentation "Defaults slots for a view")
   (description :initarg :description :initform nil
                :accessor description
                :documentation "Class description")
   (version :initarg :version :initform nil
            :accessor version
            :documentation "Version number for class")
   (closures :initarg :closures :initform nil
             :accessor closures
             :documentation "Closures to call on slot chnages")
   (sql-name :initarg :sql-name :accessor sql-name :initform nil
             :documentation "SQL Name for this class")
   (guid :initarg :guid :accessor guid :initform nil
         :documentation "ID string for this class")

   ;;; The remainder of these fields are calculated one time
   ;;; in finalize-inheritence.

   (subobjects :initform nil :accessor subobjects
               :documentation
               "List of fields that contain a list of subobjects objects.")
   (compute-cached-values :initform nil :accessor compute-cached-values
                         :documentation
                         "List of fields that contain a list of compute-cached-value objects.")
   (hyperlinks :type list :initform nil :accessor hyperlinks
               :documentation "List of fields that have hyperlinks")
   (direct-rules :type list :initform nil :initarg :direct-rules
                 :accessor direct-rules
                 :documentation "List of rules to fire on slot changes.")
   (direct-views :type list :initform nil :initarg :direct-views
                 :accessor direct-views
                 :documentation "List of views")
   (class-id :type integer :initform (+ (* 1000000 (get-universal-time)) (random 1000000))
             :accessor class-id
             :documentation "Unique ID for the class")
   (default-view :initform nil :initarg :default-view :accessor default-view
                 :documentation "The default view for a class")
   (documementation :initform nil :initarg :documentation
                    :documentation "Documentation string for hyperclass.")

   ;; SQL commands
   (create-table-cmd :initform nil :reader create-table-cmd)
   (create-indices-cmds :initform nil :reader create-index-cmds)
   (drop-table-cmd :initform nil :reader drop-table-cmd)

   (views :type list :initform nil :initarg :views :accessor views
          :documentation "List of views")
   (rules :type list :initform nil :initarg :rules :accessor rules
          :documentation "List of rules")
   )
  (:documentation "Metaclass for Markup Language classes."))

(defclass subobject ()
  ((name-class :type symbol :initarg :name-class :reader name-class)
   (name-slot :type symbol :initarg :name-slot :reader name-slot)
   (lazy-class :type symbol :initarg :lazy-class :reader lazy-class)
   (lookup :type (or function symbol) :initarg :lookup :reader lookup)
   (lookup-keys :type list :initarg :lookup-keys :reader lookup-keys))
  (:documentation "subobject information")
  (:default-initargs :name-class nil :name-slot nil :lazy-class nil
                     :lookup nil :lookup-keys nil))

(defclass compute-cached-value ()
  ((name-class :type symbol :initarg :name-class :reader name-class)
   (name-slot :type symbol :initarg :name-slot :reader name-slot)
   (lazy-class :type symbol :initarg :lazy-class
                              :reader lazy-class)
   (lookup :type (or function symbol) :initarg :lookup :reader lookup)
   (lookup-keys :type list :initarg :lookup-keys :reader lookup-keys))
  (:documentation "subobject information")
  (:default-initargs :name-class nil :name-slot nil :lazy-class nil
                     :lookup nil :lookup-keys nil))


(defmethod print-object ((obj subobject) s)
  (print-unreadable-object (obj s :type t)
    (format s "~S" (name-slot obj))))

(defclass hyperlink ()
  ((name :type symbol :initform nil :initarg :name :reader name)
   (lookup
    ;; The type specifier seems to break sbcl
    :type (or function symbol)
    ;;    :type t
    :initform nil :initarg :lookup :reader lookup)
   (link-parameters :type list :initform nil :initarg :link-parameters
                    :reader link-parameters)))

(defmethod print-object ((obj hyperlink) s)
  (print-unreadable-object (obj s :type t :identity t)
    (format s "~S" (name obj))))

(defmethod validate-superclass ((class hyperobject-class) (superclass standard-class))
  t)


(declaim (inline delistify))
(defun delistify (list)
  "Some MOPs, like openmcl 0.14.2, cons attribute values in a list."
  (if (listp list)
      (car list)
    list))

(defun remove-keyword-arg (arglist akey)
  (let ((mylist arglist)
        (newlist ()))
    (labels ((pop-arg (alist)
             (let ((arg (pop alist))
                   (val (pop alist)))
               (unless (equal arg akey)
                 (setf newlist (append (list arg val) newlist)))
               (when alist (pop-arg alist)))))
      (pop-arg mylist))
    newlist))

(defun remove-keyword-args (arglist akeys)
  (let ((mylist arglist)
        (newlist ()))
    (labels ((pop-arg (alist)
             (let ((arg (pop alist))
                   (val (pop alist)))
               (unless (find arg akeys)
                 (setf newlist (append (list arg val) newlist)))
               (when alist (pop-arg alist)))))
      (pop-arg mylist))
    newlist))


(defmethod shared-initialize :around ((class hyperobject-class) slot-names
                                        &rest initargs
                                        &key direct-superclasses
                                        user-name sql-name name description
                                        &allow-other-keys)
  ;(format t "ii ~S ~S ~S ~S ~S~%" initargs base-table direct-superclasses user-name sql-name)
  (let ((root-class (find-class 'hyperobject nil))
        (vmc 'hyperobject-class)
        user-name-plural user-name-str sql-name-str)
    ;; when does CLSQL pass :qualifier to initialize instance?
    (setq user-name-str
          (if user-name
              (delistify user-name)
            (and name (format nil "~:(~A~)" name))))

    (setq sql-name-str
          (if sql-name
              (delistify sql-name)
            (and name (lisp-name-to-sql-name name))))

    (if sql-name
        (delistify sql-name)
      (and name (lisp-name-to-sql-name name)))

    (setq description (delistify description))

    (setq user-name-plural
      (if (and (consp user-name) (second user-name))
          (second user-name)
        (and user-name-str (format nil "~A~P" user-name-str 2))))

    (flet ((do-call-next-method (direct-superclasses)
                                (let ((fn-args (list class slot-names :direct-superclasses direct-superclasses))
                                      (rm-args '(:direct-superclasses)))
                                  (when user-name-str
                                    (setq fn-args (nconc fn-args (list :user-name user-name-str)))
                                    (push :user-name rm-args))
                                  (when user-name-plural
                                    (setq fn-args (nconc fn-args (list :user-name-plural user-name-plural)))
                                    (push :user-name-plural rm-args))
                                  (when sql-name-str
                                    (setq fn-args (nconc fn-args (list :sql-name sql-name-str)))
                                    (push :sql-name rm-args))
                                  (when description
                                    (setq fn-args (nconc fn-args (list :description description)))
                                    (push :description rm-args))
                                  (setq fn-args (nconc fn-args (remove-keyword-args initargs rm-args)))
                                  (apply #'call-next-method fn-args))))
      (if root-class
          (if (some #'(lambda (super) (typep super vmc))
                    direct-superclasses)
              (do-call-next-method direct-superclasses)
            (do-call-next-method          direct-superclasses #+nil (append (list root-class)
                                                                            direct-superclasses)))
        (do-call-next-method direct-superclasses)))))


(defmethod finalize-inheritance :after ((cl hyperobject-class))
  "Initialize a hyperobject class. Calculates all class slots"
  (finalize-subobjects cl)
  (finalize-compute-cached cl)
  (init-hyperobject-class cl))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (when (>= (length (generic-function-lambda-list
                     (ensure-generic-function
                      'compute-effective-slot-definition)))
            3)
    (pushnew :ho-normal-cesd cl:*features*))

    (when (>= (length (generic-function-lambda-list
                       (ensure-generic-function
                        'direct-slot-definition-class)))
            3)
      (pushnew :ho-normal-dsdc cl:*features*))

    (when (>= (length (generic-function-lambda-list
                       (ensure-generic-function
                        'effective-slot-definition-class)))
              3)
      (pushnew :ho-normal-esdc cl:*features*)))

(defmethod direct-slot-definition-class ((cl hyperobject-class)
                                         #+ho-normal-dsdc &rest iargs)
  (declare (ignore iargs))
  (find-class 'hyperobject-dsd))

(defmethod effective-slot-definition-class ((cl hyperobject-class)
                                            #+ho-normal-esdc &rest iargs)
  (declare (ignore iargs))
  (find-class 'hyperobject-esd))

;;; Slot definitions

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defmacro process-class-option (slot-name &optional required)
    #+lispworks
    `(defmethod clos:process-a-class-option ((class hyperobject-class)
                                             (name (eql ,slot-name))
                                             value)
      (when (and ,required (null value))
        (error "hyperobject class slot ~A must have a value" name))
      (list name `',value))
    #+(or allegro sbcl cmu scl openmcl)
    (declare (ignore slot-name required))
    )

  (defmacro process-slot-option (slot-name)
    #+lispworks
    `(defmethod clos:process-a-slot-option ((class hyperobject-class)
                                            (option (eql ,slot-name))
                                            value
                                            already-processed-options
                                            slot)
      (list* option `',value already-processed-options))
    #-lispworks
    (declare (ignore slot-name))
    )

  (dolist (option *class-options*)
    (eval `(process-class-option ,option)))
  (dolist (option *slot-options*)
    (eval `(process-slot-option ,option)))

  (eval
   `(defclass hyperobject-dsd (standard-direct-slot-definition)
     (,@(mapcar #'(lambda (x)
                    `(,(intern (symbol-name x))
                      :initform nil))
                *slot-options-no-initarg*)
      ,@(mapcar #'(lambda (x)
                    `(,(intern (symbol-name x))
                      :initarg
                      ,(intern (symbol-name x) (symbol-name :keyword))
                      :initform nil
                      :accessor
                      ,(intern (concatenate 'string
                                            (symbol-name :dsd-)
                                            (symbol-name x)))))
                *slot-options*))))
  (eval
   `(defclass hyperobject-esd (standard-effective-slot-definition)
     (,@(mapcar #'(lambda (x)
                    `(,(intern (symbol-name x))
                      :initarg
                      ,(intern (symbol-name x) (symbol-name :keyword))
                      :initform nil
                      :accessor
                      ,(intern (concatenate 'string
                                            (symbol-name :esd-)
                                            (symbol-name x)))))
                (append *slot-options* *slot-options-no-initarg*)))))
  ) ;; eval-when

(defun intern-in-keyword (obj)
  (cond
    ((null obj)
     nil)
    ((eq t obj)
     t)
    ((atom obj)
     (intern (symbol-name obj) (find-package 'keyword)))
    ((consp obj)
     (cons (intern-in-keyword (car obj) ) (intern-in-keyword (cdr obj))))
    (t
     obj)))

(defun canonicalize-value-type (vt)
  (typecase vt
    (atom
     (ensure-keyword vt))
    (cons
     (list (ensure-keyword (car vt)) (cadr vt)))
    (t
     t)))


(defmethod compute-effective-slot-definition :around ((cl hyperobject-class)
                                                      #+ho-normal-cesd name
                                                      dsds)
  (declare (ignore #+ho-normal-cesd name))
  (let ((esd (call-next-method)))
    (if (typep esd 'hyperobject-esd)
        (compute-hyperobject-esd esd dsds)
        esd)))

(defun compute-hyperobject-esd (esd dsds)
  (let* ((dsd (car dsds)))
    (multiple-value-bind (sql-type sql-length)
        (value-type-to-sql-type (dsd-value-type dsd))
      (setf (esd-sql-type esd) sql-type)
      (setf (esd-sql-length esd) sql-length))
    (setf (esd-user-name esd)
          (aif (dsd-user-name dsd)
               it
               (string-downcase (symbol-name (slot-definition-name dsd)))))
    (setf (esd-sql-name esd)
          (aif (dsd-sql-name dsd)
               it
               (lisp-name-to-sql-name (slot-definition-name dsd))))
    (setf (esd-sql-name esd)
          (aif (dsd-sql-name dsd)
               it
               (lisp-name-to-sql-name (slot-definition-name dsd))))
    (dolist (name '(value-type print-formatter subobject hyperlink
                    hyperlink-parameters unbound-lookup
                    description value-constraint indexed null-allowed
                    unique short-description void-text read-only-groups
                    hidden-groups unit disable-predicate view-type
                    list-of-values compute-cached-value stored))
      (setf (slot-value esd name) (slot-value dsd name)))
    esd))

(defun lisp-name-to-sql-name (lisp)
  "Convert a lisp name (atom or list, string or symbol) into a canonical
SQL name"
  (unless (stringp lisp)
    (setq lisp
          (typecase lisp
            (symbol (symbol-name lisp))
            (t (write-to-string lisp)))))
  (do* ((len (length lisp))
        (sql (make-string len))
        (i 0 (1+ i)))
      ((= i len) (string-upcase sql))
    (declare (fixnum i)
             (simple-string sql))
    (setf (schar sql i)
          (let ((c (char lisp i)))
            (case c
              ((#\- #\$ #\+ #\#) #\_)
              (otherwise c))))))

#+ho-normal-cesd
(setq cl:*features* (delete :ho-normal-cesd cl:*features*))
#+ho-normal-dsdc
(setq cl:*features* (delete :ho-normal-dsdc cl:*features*))
#+ho-normal-esdc
(setq cl:*features* (delete :ho-normal-esdc cl:*features*))

(defun lisp-type-is-a-string (type)
  (or (eq type 'string)
      (and (listp type) (some #'(lambda (x) (eq x 'string)) type))))

(defun value-type-is-a-string (type)
  (or (eq type 'string)
      (eq type 'cdata)
      (and (listp type) (some #'(lambda (x) (or (eq x 'string)
                                                (eq x 'cdata)))
                              type))))

(defun base-value-type (value-type)
  (if (atom value-type)
      value-type
    (car value-type)))

(defun value-type-to-lisp-type (value-type)
  (case (base-value-type value-type)
    ((:string :cdata :varchar :char)
     '(or null string))
    (:datetime
     '(or null integer))
    (:character
     '(or null character))
    (:fixnum
     '(or null fixnum))
    (:boolean
     '(or null boolean))
    ((:integer :long-integer)
     '(or null integer))
    ((:float :single-float)
     '(or null single-float))
    (:double-float
     '(or null double-float))
    (otherwise
     t)))

(defun value-type-to-sql-type (value-type)
  "Return two values, the sql type and field length."
  (let ((type (base-value-type value-type))
        (length (when (consp value-type)
                  (cadr value-type))))
    (values
     (case type
       ((:char :character)
        :char)
       (:varchar
        :varchar)
       ((:fixnum :integer)
        :integer)
       (:long-integer
        :long-integer)
       (:boolean
        :boolean)
       ((:float :single-float)
        :single-float)
       (:double-float
        :double-float)
       (:datetime
        :long-integer)
       (otherwise
        :text))
     length)))

;;;; Class initialization function

;; One entry for each class with lazy readers defined.  The value is a plist mapping
;; slot-name to a lazy reader, each of which is a list of a function and slot-names.
(defvar *lazy-readers* (make-hash-table))

(defmethod slot-unbound ((class hyperobject-class) instance slot-name)
  (let ((lazy-reader
         (loop for super in (class-precedence-list class)
               as lazy-reader = (getf (gethash super *lazy-readers*) slot-name)
               when lazy-reader return it)))
    (if lazy-reader
        (setf (slot-value instance slot-name)
              (if (atom lazy-reader)
                  (make-instance lazy-reader)
                  (apply (car lazy-reader)
                         (loop for arg-slot-name in (cdr lazy-reader)
                               collect (slot-value instance arg-slot-name)))))
        ;; No lazy reader -- defer to regular slot-unbound handling.
        (call-next-method))))

;; The reader is a function and the reader-keys are slot names.  The slot is lazily set to
;; the result of applying the function to the slot-values of those slots, and that value
;; is also returned.
(defun ensure-lazy-reader (cl class-name slot-name lazy-class reader
                           &rest reader-keys)
  (declare (ignore class-name))
  (setf (getf (gethash cl *lazy-readers*) slot-name)
    (aif lazy-class
         it
         (list* reader (copy-list reader-keys)))))

(defun remove-lazy-reader (class-name slot-name)
  (setf (getf (gethash (find-class class-name) *lazy-readers*) slot-name)
    nil))


(defun store-lazily-computed-objects (cl slot esd-accessor obj-class)
  (setf (slot-value cl slot)
        (let ((objs '()))
          (dolist (slot (class-slots cl))
            (let-when
             (def (funcall esd-accessor slot))
             (let ((obj (make-instance obj-class
                                       :name-class (class-name cl)
                                       :name-slot (slot-definition-name slot)
                                       :lazy-class (when (atom def)
                                                     def)
                                       :lookup (when (listp def)
                                                 (car def))
                                       :lookup-keys (when (listp def)
                                                      (cdr def)))))
               (unless (eq (lookup obj) t)
                 (apply #'ensure-lazy-reader
                  cl
                  (name-class obj) (name-slot obj)
                  (lazy-class obj)
                  (lookup obj) (lookup-keys obj))
                 (push obj objs)))))
          ;; sbcl/cmu reverse class-slots compared to the defclass form
          ;; so re-reverse on cmu/sbcl
          #+(or cmu sbcl) objs
          #-(or cmu sbcl) (nreverse objs)
          )))

(defun finalize-subobjects (cl)
  (store-lazily-computed-objects cl 'subobjects 'esd-subobject 'subobject))

(defun finalize-compute-cached (cl)
  (store-lazily-computed-objects cl 'compute-cached-values
                                 'esd-compute-cached-value 'compute-cached-value))


(defun finalize-documentation (cl)
  "Calculate class documentation slot"
  (let ((*print-circle* nil))
    (setf (documentation cl 'type)
          (format nil "Hyperobject~A~A~A~A"
                  (aif (user-name cl)
                       (format nil ": ~A" it ""))
                  (aif (description cl)
                       (format nil "~%Class description: ~A" it) "")
                  (aif (subobjects cl)
                       (format nil "~%Subobjects:~{ ~A~}" (mapcar #'name-slot it)) "")
                  (aif (default-print-slots cl)
                       (format nil "~%Default print slots:~{ ~A~}" it) "")
                  ))))

(defun finalize-hyperlinks (cl)
  (let ((hyperlinks '()))
    (dolist (esd (class-slots cl))
      (awhen (slot-value esd 'hyperlink)
             (push
              (make-instance 'hyperlink
                             :name (slot-definition-name esd)
                             :lookup it
                             :link-parameters (slot-value esd 'hyperlink-parameters))
              hyperlinks)))
    ;; cmu/sbcl reverse class-slots compared to the defclass form
    ;; hyperlinks is already reversed from the dolist/push loop, so re-reverse on sbcl/cmu
    #-(or cmu sbcl) (setq hyperlinks (nreverse hyperlinks))
    (setf (slot-value cl 'hyperlinks) hyperlinks)))

(defun init-hyperobject-class (cl)
  "Initialize a hyperobject class. Calculates all class slots"
  (finalize-views cl)
  (finalize-hyperlinks cl)
  (finalize-sql cl)
  (finalize-rules cl)
  (finalize-documentation cl))




;;;; *************************************************************************
;;;;  Metaclass Slot Accessors
;;;; *************************************************************************

(defun find-slot-by-name (cl name)
  (find name (class-slots cl) :key #'slot-definition-name))

(defun hyperobject-class-user-name (obj)
  (user-name (class-of obj)))

(defun hyperobject-class-user-name-plural (obj)
  (user-name-plural (class-of obj)))

(defun hyperobject-class-subobjects (obj)
  (subobjects (class-of obj)))

(defun hyperobject-class-hyperlinks (obj)
  (hyperlinks (class-of obj)))

(defun hyperobject-class-slots (obj)
  ;; cmucl/sbcl reverse class-slots
  #+(or cmu sbcl) (reverse (class-slots (class-of obj)))
  #-(or cmu sbcl) (class-slots (class-of obj)))

(defun all-subobjects (obj)
  "Returns a list of all subobjects in an object"
  (let ((so-list '()))
    (dolist (subobj-obj (subobjects (class-of obj)) (nreverse so-list))
      (dolist (so (funcall (name-slot subobj-obj) obj))
        (push so so-list)))))
