;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defpackage #:km
  (:use :cl :asdf))

(in-package :km)

(defsystem km
  :name "km"
  :version "2.5.33"
  :author "Peter Clark"
  :author "Bruce Porter"
  :licence "GNU Lesser General Public Licence"
  :description "THE KNOWLEDGE MACHINE - INFERENCE ENGINE"
  :long-description "KM knowledge representation language. KM is a powerful, frame-based language with clear first-order logic semantics. It contains sophisticated machinery for reasoning, including selection by description, unification, classification, and reasoning about actions using a situations mechanism."
  :serial t
  :components ((:file "header")
               (:file "htextify")
               (:file "case")
               (:file "interpreter")
               (:file "get-slotvals")
               (:file "frame-io")
               (:file "trace")
               (:file "lazy-unify")
               (:file "constraints")
               (:file "explain")
               (:file "kbutils")
               (:file "stack")
               (:file "stats")
               (:file "sadl")
               (:file "anglify")
               (:file "writer")
               (:file "taxonomy")
               (:file "subsumes")
               (:file "prototypes")
               (:file "loadkb")
               (:file "minimatch")
               (:file "utils")
               (:file "strings")
               (:file "compiler")
               (:file "compiled-handlers")
               (:file "licence")
               (:file "initkb")))
