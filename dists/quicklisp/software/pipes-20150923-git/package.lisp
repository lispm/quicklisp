;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          package.lisp
;;;; Purpose:       Package definition for pipes package
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  Apr 2000
;;;;
;;;; $Id$
;;;;
;;;; *************************************************************************

(in-package #:cl-user)

(defpackage #:pipes
  (:use #:common-lisp)
  (:export
   #:+empty-pipe+
   #:make-pipe
   #:pipe-tail
   #:pipe-head
   #:pipe-elt
   #:pipe-enumerate
   #:pipe-values
   #:pipe-force
   #:pipe-filter
   #:pipe-map
   #:pipe-map-filtering
   #:pipe-append
   #:pipe-mappend
   #:pipe-mappend-filtering
   ))


(defpackage #:pipes-user
  (:use #:common-lisp #:pipes)
  )
