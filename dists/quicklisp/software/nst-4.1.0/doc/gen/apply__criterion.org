# DO NOT EDIT --- generated from source code.
*** Criterion =apply=
The =:apply= criterion first evaluates the forms under test, applying =function= to them.  The overall criterion passes or fails exactly when the subordinate =criterion= with the application's multiple result values.

Usage:
#+begin_example
(:apply FUNCTION CRITERION)
#+end_example
Example:
#+begin_example
(def-test applycheck (:apply cadr (:eql 10)) '(0 10 20))
#+end_example

