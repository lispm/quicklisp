;;; Copyright (c) Juha Arpiainen 2005-06

;;; This file is part of Bourbaki.

;;; Bourbaki is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.

;;; Bourbaki is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Bourbaki; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; -------

;;; Sound definitions

(in-package :bourbaki)

;;; Define a new symbol in terms of previous ones
;;; 
;;;    (def "df-foo" type "foo" (vars) (dummy) [rhs])
;;;    
;;; is roughly equivalent to
;;; 
;;;    (prim type "foo" (vars))
;;;    (ax "df-foo" (vars dummy)
;;;      (ass [== foo vars [rhs]]))
;;;
;;; where == is the equality operator for `type'. The rhs may contain
;;; `dummy' variables. These will implicitly be marked distinct from
;;; each other and the normal variables. Vars marked :bound must not
;;; potentially occur free in the rhs.
(defmacro def (df-name sym-type sym-name vars dummy rhs &body body)
  (let ((df-sym (gensym)) (the-sym (gensym)) (vars-sym (gensym)) (type-sym (gensym)))
    ;; Create contexts for the definition and the symbol being defined
    `(let* ((,df-sym (create-context :class :definition :name ,df-name :parent *current-context*))
            (,type-sym (get-symkind ',sym-type))
            (,the-sym (create-context :class :sym :type ,type-sym :name ,sym-name :parent *current-context*))
            (nsym 0) (ndummy 0)
            (the-rhs nil))
       ;; Link the symbol to its definition
       (setf (gethash :def (context-meta ,the-sym)) ,df-sym)
       ;; XXX a definition never inherits hypotheses
       (setf (context-hypo ,df-sym) nil)
       
       (let ((parent-ctx *current-context*)
             (*current-context* ,df-sym))
         ;; Parse normal variables of the definition
         (setf nsym (parse-vars ,vars))
         (let ((,vars-sym (context-vars ,df-sym)))
           ;; Copy variables to the symbol
           (setf (context-vars ,the-sym) ,vars-sym)
           (insert-sym (wrap-ctx ,the-sym nsym) parent-ctx)
           ;; Parse dummy variables
           (setf ndummy (parse-vars ,dummy))
           (do ;; Make the dummy variables distinct from each other and the "normal" variables
             ((i 0 (1+ i))
              (dv (context-vars *current-context*) (cdr dv)))
             ((>= i ndummy))
             (dvc-var (car dv) *current-context*))
           ;; Construct the assertion
           (setf the-rhs ,rhs)
           (ass (list (gethash :eq-op (context-meta ,type-sym))
                      (symtree-parse (cons (seek-sym :rel ,sym-name)
                                           (makevarlist ,the-sym)))
                      the-rhs))
           (progn ,@body)))
;       (format t "Def ~a: potential free vars on rhs: ~{~a~}~%" ,df-name
;               (potential-vars (context-vars ,df-sym) (context-distinct ,df-sym) the-rhs))
       (insert-sym (wrap-ctx ,df-sym (+ nsym ndummy))))))

;;; Move to context.lisp?
;;; Create a pattern for argument transformations
(defmacro pattern (vars rhs)
  `(let ((dummy-ctx (create-context :class :context :name "" :parent *current-context*))
         (nvars 0)
         (str nil))
     (let ((*current-context* dummy-ctx))
       (setf nvars (parse-vars ,vars))
       (setf str ,rhs))
     (let ((ref (typecase str
                  (subproof (subproof-ref str))
                  (list str))))
       (make-symref
         :target (car ref)
         :class :alias
         :args-needed nvars
         :fn #'(lambda (&rest args)
                 (let ((smap (pairlis (context-vars dummy-ctx) args)))
                   (nreverse (mapcar #'(lambda (x) (replace-vars x smap))
                                     (cdr ref)))))))))

;;; Insert a named pattern into current context
(defmacro alias (alias-name vars rhs)
  `(setf (gethash ,alias-name (context-syms *current-context*))
         (pattern ,vars ,rhs)))

;;; List of variables that potentially occur in a symtree
;(defun potential-vars (lst dist expr)
;  (format t "(potent ~a ~a ~a)~%" lst dist expr)
;  (let ((sym (car expr)))
;    (if (eq (context-class sym) :arg)
;      (let ((res (remove-if #'(lambda (x) (member (cons x sym) dist :test #'pair-eq)) (copy-list lst)))) res)
;        (format t "var => ~a~%" res) res)
;      (let* ((bound (loop for i in (reverse (context-vars sym))
;                          for j in (cdr expr)
;                          if (gethash :bound (context-meta i))
;                            collect (car j)))
;             (free? (set-difference lst bound :test #'eq)))
;        (remove-duplicates (mapcan #'(lambda (x) (potential-vars free? dist x))
;                                   (cdr expr))
;                           :test #'eq)))))
