;;;  class.lisp --- classes that used to be in src/proxy/field.lisp

;;;  Copyright (C) 2009 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: cobstor

#+cmu (ext:file-comment "$Module: class.lisp $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

(in-package :cobstor-tests)


(defclass cbl-field ()
  ((%storage :type cobstor::field-storage
	     :reader field-storage
	     :documentation
	     "Vector holding the binay representation of the
value of this field.  It may be a displaced vector when this
field shares the storage with another field/record."))
  (:documentation "Abstract class."))

(defgeneric field-size (cbl-field)
  (:documentation
   "Return the size in bytes accupied by a Cobol field."))

(defgeneric field-length (cbl-field)
  (:documentation
   "Return the length in characters or digits of a Cobol field.
For numeric fields, this length doesn't consider the sign, even
if separated, and the decimal point, if the field can store
decimals."))

(defgeneric field-size-using-class (class &rest initargs)
  (:documentation
   "Return the size in bytes of a cbl-field of class CLASS if it
were to be initialised with initargs."))

(defgeneric field-value (cbl-field)
  (:documentation
   "Return the value of a Cobol field (decoded from its storage)."))

(defgeneric (setf field-value) (value cbl-field)
  (:documentation
   "Set a Cobol field value encoding it into its storage."))

(defgeneric cbl-field-p (object)
  (:documentation
   "Return true if OBJECT is a CBL-FIELD derived object."))

(defgeneric allocate-storage (cbl-field source offset size)
  (:documentation
   "Allocate storage for a CBL-FIELD taking it from SOURCE, if
not NIL, otherwise allocate a new array for it."))

(defgeneric read-field (field stream)
  (:documentation
   "Read from STREAM enough data to fill FIELD's storage."))

(defgeneric write-field (field stream)
  (:documentation
   "Write the storage of FIELD to STREAM."))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; some method defaults
;;

(defmethod field-value ((field cbl-field))
  (field-storage field))

(defmethod (setf field-value) ((value cbl-field) (field cbl-field))
  "Set a Cobol field using another Cobol field."
  ;;  get the value of the source field and recur
  (setf (field-value field) (field-value value)))

(defmethod (setf field-value) ((value null) (field cbl-field))
  "Special case.  By default any cbl-field we set to NIL will
have its entire storage area set to zero."
  (fill (field-storage field) 0)
  field)

(defmethod field-size ((field cbl-field))
  (length (field-storage field)))

(defmethod field-length ((field cbl-field))
  (field-size field))

(defmethod cbl-field-p ((field cbl-field))
  t)

(defmethod cbl-field-p (object)
  nil)

(defmethod allocate-storage ((field cbl-field) (source cbl-field) offset size)
  "Take the storage from another CBL-FIELD.  Offset must be
specified."
  (allocate-storage field (field-storage source) offset size))

(defmethod allocate-storage ((field cbl-field) (source vector) offset size)
  "Take the storage from a vector.  Offset must be specified."
  (setf (slot-value field '%storage)
	(make-array size
		    :element-type 'cobstor::ubyte
		    :displaced-to source
		    :displaced-index-offset offset)))

(defmethod allocate-storage ((field cbl-field) (source null) offset size)
  "Allocate a brand new string as storage for FIELD.  Source is
not specified, and offset is disregarded."
  (setf (slot-value field '%storage)
	(make-sequence 'cobstor::field-storage size)))

(defmethod initialize-instance :before ((field cbl-field) &rest args
					&key storage (offset 0) &allow-other-keys)
  "On top of all the usual initialisations this method allocates
the storage used by the field."
  (declare (ignorable args))
  (allocate-storage field storage offset
		    (apply #'field-size-using-class (class-name (class-of field)) args)))

(defun field-start (field)
  "Return the displacement of field within the array it borrows
the storage from.  This is zero if field has its own storage."
  (cadr (multiple-value-list (array-displacement (field-storage field)))))

(defun field-end (field)
  "Return the end of the field in the array it borrows the
storage from.  This is = to FIELD-SIZE if field has its own
storage."
  (+ (field-start field)
     (field-size field)))

(defmethod read-field ((field cbl-field) stream)
  (read-sequence (field-storage field) stream))

(defmethod write-field((field cbl-field) stream)
  (write-sequence (field-storage field) stream))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass cbl-sizable-field (cbl-field)
  ((length :type integer
	   :initarg :length
	   :accessor field-length
	   :documentation
	   "Total length in charcaters or digits of field.  In
case of numerical fields this does not include decimal point or
sign characters, but includes the decimals (if present).
Usually, this length does not correspond to the actual size in
bytes accupied by the field."))
  (:documentation
   "Abstract class for fields that may have arbitrary lengths."))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass cbl-string-field (cbl-sizable-field)
  ()
  (:documentation
   "Cobol field that can hold alphanumeric values."))

(defmethod field-size-using-class ((class (eql 'cbl-string-field)) &key length &allow-other-keys)
  (declare (ignore class))
  length)

(defmethod field-value ((field cbl-string-field))
  (be storage (field-storage field)
    (cobstor::get-string-field-value storage 0 (length storage))))

(defmethod (setf field-value) ((value string) (field cbl-string-field))
  (be storage (field-storage field)
    (cobstor::set-string-field-value value storage 0 (length storage))))

(defmethod (setf field-value) (value (field cbl-string-field))
  (setf (field-value field) (format nil "~A" value))
  field)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass cbl-field-decimals-mixin ()
  ((decimals :type integer
	     :initform 0
	     :initarg :decimals
	     :accessor field-decimals))
  (:documentation
   "Abstract mixin class for numeric fields that may have decimals."))

(defclass cbl-numeric-field (cbl-sizable-field cbl-field-decimals-mixin)
  ((signed :initarg :signed
	   :initform nil
	   :accessor field-signed
	   :type boolean
	   :documentation
	   "True if field accept an algebraic sign.")
   (sign-separated :initarg :sign-separated
		   :initform nil
		   :accessor field-sign-separated-p
		   :type boolean
		   :documentation
		   "True if sign should accupy an extra character
instead of being encoded with one of the digits.")
   (sign-position :initarg :sign-position
		  :initform *default-sign-position*
		  :accessor field-sign-position
		  :type (member :leading :trailing)
		  :documentation
		  "Position of the sign."))
  (:documentation
   "Standard uncompressed numeric field.  What is commonly
referred to as \"usage display\" in Cobol circles."))

(defmethod field-size-using-class ((class (eql 'cbl-numeric-field))
				   &key length signed sign-separated
				   &allow-other-keys)
  (declare (ignore class))
  (+ length (if (and signed sign-separated) 1 0)))

(defmethod field-value ((field cbl-numeric-field))
  (be storage (field-storage field)
    (cobstor::get-numeric-field-value storage 0 (length storage)
				      (field-signed field)
				      (field-sign-separated-p field)
				      (field-sign-position field)
				      (field-decimals field))))

(defmethod (setf field-value) ((value number) (field cbl-numeric-field))
  (be storage (field-storage field)
    (cobstor::set-numeric-field-value value storage 0 (length storage)
				      (field-signed field)
				      (field-sign-separated-p field)
				      (field-sign-position field)
				      (field-decimals field))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass cbl-comp1-field (cbl-field)
  ()
  (:documentation
   "The format of a COMP-1 data item is 16-bit signed binary. The
legal values range from -32767 to 32767.  The size of the data
item is always two bytes, and the high-order half of the data is
stored in the leftmost byte.  The PICTURE string that describes
the item is irrelevant.  Unlike other numeric data types, a size
error will occur on a COMP-1, COMP-X, or COMP-N data item only
when the value exceeds the physical storage of the item (in other
words, the number of \"9\"s in the item's PICTURE is ignored when
size error is determined)."))

(defmethod field-size-using-class ((class (eql 'cbl-comp1-field)) &rest args)
  (declare (ignore class args))
  2)

(defmethod field-value ((field cbl-comp1-field))
  (cobstor::get-comp1-field-value (field-storage field) 0))

(defmethod (setf field-value) ((value number) (field cbl-comp1-field))
  (cobstor::set-comp1-field-value value (field-storage field) 0))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass cbl-comp2-field (cbl-sizable-field cbl-field-decimals-mixin)
  ((signed :initarg :signed
	   :initform nil
	   :accessor field-signed
	   :type boolean))
  (:documentation
   "For COMP-2 (decimal storage), each digit is stored in one
byte in decimal format. If the value is signed, then an
additional trailing byte is allocated for the sign. The storage
of COMP-2 is identical with USAGE DISPLAY with the high-order
four bits stripped from each byte."))

(defmethod field-size-using-class ((class (eql 'cbl-comp2-field))
				   &key length signed
				   &allow-other-keys)
  (declare (ignore class))
  (+ length (if signed 1 0)))

(defmethod field-value ((field cbl-comp2-field))
  (cobstor::get-comp2-field-value (field-storage field) 0 (field-length field)
				  (field-signed field) (field-decimals field)))

(defmethod (setf field-value) ((value number) (field cbl-comp2-field))
  (cobstor::set-comp2-field-value value (field-storage field) 0 (field-length field)
				  (field-signed field) (field-decimals field)))

(defclass cbl-comp3-field (cbl-sizable-field cbl-field-decimals-mixin)
  ((signed :initarg :signed
	   :initform nil
	   :accessor field-signed
	   :type boolean))
  (:documentation
   "For COMP-3 (packed-decimal storage), two digits are stored in
each byte.  An additional half byte is allocated for the sign,
even if the value is unsigned. The sign is placed in the
rightmost position, and its value is 0x0D for negative; all other
values are treated as positive.  The size of an item (including
one for the implied sign) is divided by two to arrive at its
actual size (rounding fractions up)."))

(defmethod field-size-using-class ((class (eql 'cbl-comp3-field))
				   &key length &allow-other-keys)
  (declare (ignore class))
  (ceiling (1+ length) 2))

(defmethod field-value ((field cbl-comp3-field))
  (be storage (field-storage field)
    (cobstor::get-comp3-field-value storage 0 (length storage)
				    (field-signed field)
				    (field-decimals field))))

(defmethod (setf field-value) ((value number) (field cbl-comp3-field))
  (be storage (field-storage field)
    (cobstor::set-comp3-field-value value storage 0 (length storage)
				    (field-signed field) (field-decimals field))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass cbl-comp4-field (cbl-sizable-field cbl-field-decimals-mixin)
  ((signed :initarg :signed
	   :initform nil
	   :accessor field-signed
	   :type boolean))
  (:documentation
   "The format of a COMP-4 item is two's-complement binary (the
value without its decimal point). COMP-4 values are stored in a
machine-independent format. This format places the highest-order
part of the value in the leftmost position and follows down to
the low-order part in the rightmost position. The number of bytes
a data item occupies depends on the number of \"9\"s in its
PICTURE and on the presence of various compile-time options. For
example, you may include more than eighteen \"9\"s only if your
program has been compiled for 31-digit support."))

(defmethod field-size-using-class ((class (eql 'cbl-comp4-field))
				   &key length signed &allow-other-keys)
  (declare (ignore class))
  (cobstor::size-of-comp4/5 length signed))

(defun get-value-comp4/5-big-endian (field)
  (be storage (field-storage field)
    (cobstor::get-field-value-comp4/5-be storage 0 (length storage)
					 (field-decimals field))))

(defun set-value-comp4/5-big-endian (value field)
  (be storage (field-storage field)
    (cobstor::set-field-value-comp4/5-be value storage 0 (length storage)
					 (field-decimals field))))

(defun get-value-comp4/5-little-endian (field)
  (be storage (field-storage field)
    (cobstor::get-field-value-comp4/5-le storage 0 (length storage)
					 (field-decimals field))))

(defun set-value-comp4/5-little-endian (value field)
  (be storage (field-storage field)
    (cobstor::set-field-value-comp4/5-le value storage 0 (length storage)
					 (field-decimals field))))

(defmethod field-value ((field cbl-comp4-field))
  (get-value-comp4/5-big-endian field))

(defmethod (setf field-value) ((value number) (field cbl-comp4-field))
  (set-value-comp4/5-big-endian value field))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass cbl-comp5-field (cbl-sizable-field cbl-field-decimals-mixin)
  ((signed :initarg :signed
	   :initform nil
	   :accessor field-signed
	   :type boolean)
   (endianness :initarg :endianness
	       :initform *default-endianness*
	       :accessor field-endianness
	       :type (member :big-endian :little-endian)))
  (:documentation
   "COMP-5 is primarily used to communicate with external
programs that expect native data storage.  The format of a COMP-5
data item is identical to a COMP-4 data item, except that the
data is stored in a machine-dependent format. It is stored in an
order that is natural to the host machine. For example, a PIC
S9(9) COMP-5 data item is equivalent to a 32-bit binary word on
the host machine, and a PIC S9(20) COMP-5 item is equivalent to a
64-bit word."))

(defmethod field-size-using-class ((class (eql 'cbl-comp5-field))
				   &key length signed &allow-other-keys)
  (declare (ignore class))
  (cobstor::size-of-comp4/5 length signed))

(defmethod field-value ((field cbl-comp5-field))
  (ecase (field-endianness field)
    (:big-endian
     (get-value-comp4/5-big-endian field))
    (:little-endian
     (get-value-comp4/5-little-endian field))))

(defmethod (setf field-value) ((value number) (field cbl-comp5-field))
  (ecase (field-endianness field)
    (:big-endian
     (set-value-comp4/5-big-endian value field))
    (:little-endian
     (set-value-comp4/5-little-endian value field))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass cbl-comp6-field (cbl-sizable-field cbl-field-decimals-mixin)
  ()
  (:documentation
   "The format of a COMP-6 item is identical to a COMP-3 item
except that it is unsigned and no space is allocated for the
sign. If the number of digits is odd, a zero is added to the left
end of the number before it is packed. Thus there are two decimal
digits per byte, and the actual size of the item is determined by
dividing its PICTURE size by two and rounding up."))

(defmethod field-size-using-class ((class (eql 'cbl-comp6-field))
				   &key length &allow-other-keys)
  (declare (ignore class))
  (ceiling length 2))

(defmethod field-value ((field cbl-comp6-field))
  (be storage (field-storage field)
    (cobstor::get-comp6-field-value storage 0 (length storage) (field-decimals field))))

(defmethod (setf field-value) ((value number) (field cbl-comp6-field))
  (be storage (field-storage field)
    (cobstor::set-comp6-field-value value storage 0 (length storage) (field-decimals field))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric copy-field (field)
  (:documentation
   "Return a duplicate of FIELD.  That is a disting object of the
same content."))

(defmethod copy-field ((field cbl-field))
  (be new (make-instance (class-of field))
    (setf (field-value new) field)
    new))

(defmethod copy-field ((field cbl-string-field))
  (be new (make-instance (class-of field) :length (field-length field))
    (setf (field-value new) field)
    new))


(defmethod copy-field ((field cbl-numeric-field))
  (be new (make-instance (class-of field)
			 :length (field-length field)
			 :signed (field-signed field)
			 :sign-separated (field-sign-separated-p field)
			 :decimals (field-decimals field))
    (setf (field-value new) field)
    new))


(defmethod copy-field ((field cbl-comp2-field))
  (be new (make-instance (class-of field)
			 :length (field-length field)
			 :signed (field-signed field)
			 :decimals (field-decimals field))
    (setf (field-value new) field)
    new))

(defmethod copy-field ((field cbl-comp3-field))
  (be new (make-instance (class-of field)
			 :length (field-length field)
			 :signed (field-signed field)
			 :decimals (field-decimals field))
    (setf (field-value new) field)
    new))


(defmethod copy-field ((field cbl-comp4-field))
  (be new (make-instance (class-of field)
			 :length (field-length field)
			 :signed (field-signed field)
			 :decimals (field-decimals field))
    (setf (field-value new) field)
    new))


(defmethod copy-field ((field cbl-comp5-field))
  (be new (make-instance (class-of field)
			 :length (field-length field)
			 :signed (field-signed field)
			 :decimals (field-decimals field)
			 :endianness (field-endianness field))
    (setf (field-value new) field)
    new))


(defmethod copy-field ((field cbl-comp6-field))
  (be new (make-instance (class-of field)
			 :length (field-length field)
			 :decimals (field-decimals field))
    (setf (field-value new) field)
    new))
