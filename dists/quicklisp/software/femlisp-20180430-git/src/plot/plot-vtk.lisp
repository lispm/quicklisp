;;; -*- mode: lisp; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; plot-vtk.lisp - VTK plotting of meshes and finite element functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2010 Nicolas Neuss, Karlsruhe Institute of Technology.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
;;; IN NO EVENT SHALL THE AUTHOR, THE KARLSRUHE INSTITUTE OF TECHNOLOGY
;;; OR OTHER CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.plot)

(defun write-vtk-positions (stream position-array)
  (format stream "POINTS ~D float~%" (length position-array))
  (loop for position across position-array do
	 (loop for i below 3 do
	      (format stream "~G "
                      (if (< i (length position))
                          (float (aref position i) 1.0)
                          0.0))
	    finally (terpri stream))))

(defmethod graphic-write-data (stream object (program (eql :vtk))
			       &key cells cell->values component
			       (depth 0) transformation
                               &allow-other-keys)
  (declare (optimize debug))
  (unless cells (return-from graphic-write-data))
  
  ;; write datafile header
  (format stream "# vtk DataFile Version 2.0~%femlisp-vtk~%ASCII~%DATASET UNSTRUCTURED_GRID~%~%")
  
  (let* ((position-indices (compute-position-indices cells depth))
	 (position-array (position-array cells position-indices depth transformation)))
    
    ;; write point data
    (write-vtk-positions stream position-array)
    (terpri stream)

    (let ((connections (connections cells position-indices depth)))
      ;; corner info header
      (format stream "CELLS ~D ~D~%"
              (length connections)
              (reduce #'+ connections :key #'length))
      ;; corner info
      (loop for (cell . conn) in connections do
           (typecase cell
             (fl.mesh::<1-1-product-cell>
              (destructuring-bind (a b c d) conn
                (format stream "4 ~D ~D ~D ~D~%" a b d c)))
             (fl.mesh::<2-1-product-cell>
              (destructuring-bind (a b c d e f) conn
                (format stream "6 ~D ~D ~D ~D ~D ~D~%" a e b c f d)))
             (fl.mesh::<1-1-1-product-cell>
              (destructuring-bind (a b c d e f g h) conn
                (format stream "8 ~D ~D ~D ~D ~D ~D ~D ~D~%" a b d c e f h g)))
             (t (format stream "~D ~{~D~^ ~}~%" (length conn) conn))))
      
      ;; write cell type info
      (format stream "~%CELL_TYPES ~D~%" (length connections))
      (loop for conn in connections do
           (format stream "~D~%"
                   (etypecase (first conn)
                     (fl.mesh::<1-simplex> 4)           ; VTK_POLY_LINE
                     (fl.mesh::<2-simplex> 5)           ; VTK_TRIANGLE
                     (fl.mesh::<1-1-product-cell> 9)    ; VTK_QUAD
                     (fl.mesh::<3-simplex> 10)          ; VTK_TETRA
                     (fl.mesh::<1-1-1-product-cell> 12) ; VTK_HEXAHEDRON
                     (fl.mesh::<1-2-product-cell> 13)   ; VTK_WEDGE
                     (fl.mesh::<2-1-product-cell> 13)   ; VTK_WEDGE
                     ))))
    ;; write data
    (when cell->values
      (let ((values
              (if (numberp component)
                  (let ((vals (compute-all-position-values
                               cells position-indices depth cell->values)))
                    (make-array (list 1 (length vals))
                                :displaced-to vals))
                  (compute-all-position-component-values
                   cells position-indices depth cell->values
                   (mklist component))))
            (all-components (components (problem object))))
        (let ((n (length position-array)))
          (format stream "POINT_DATA ~A~%" n)
          (loop for comp in (mklist component)
                and c from 0
                for size = (nth-value 1 (component-position all-components comp))
                do
                   (cond
                     ((= size 1)
                      (format stream "SCALARS ~A float ~A~%LOOKUP_TABLE default~%" comp 1)
                      (dotimes (j n)
                        (dovec (num (aref values c j))
                          (format stream "~10,8,,,,,'eG " (coerce num 'single-float)))
                        (terpri stream)))
                     ((<= 2 size 3)
                      (format stream "VECTORS ~A float~%" comp)
                      (dotimes (j n)
                        (let ((value (aref values c j)))
                          (dotimes (k 3)
                            (format stream "~10,8,,,,,'eG "
                                  (coerce (if (< k (nrows value))
                                              (vref value k)
                                              0.0)
                                          'single-float))))
                        (terpri stream))))))))
    ))

