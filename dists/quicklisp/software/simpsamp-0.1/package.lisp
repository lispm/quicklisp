(in-package #:common-lisp-user)

(defpackage #:simpsamp
  (:export ;; Core algorithms.
           #:do-sample-of-iterator-expr
	   #:vector-sample-of-unbounded-iterator-expr
	   
	   ;; Output via looping macros.
	   #:do-sample-of-list
	   #:do-sample-of-vector
	   #:do-sample-of-hash-table
	   #:do-sample-of-range
	   #:do-sample-of-iterator
	   #:do-sample-of-unbounded-iterator
	   
	   ;; Output via mapping (for effect).
	   #:map-sample-of-list
	   #:map-sample-of-vector
	   #:map-sample-of-hash-table-keys
	   #:map-sample-of-hash-table-values
	   #:map-sample-of-hash-table
	   #:map-sample-of-range
	   #:map-sample-of-iterator
	   #:map-sample-of-unbounded-iterator
	   
	   ;; Output as lists.
	   #:list-sample-of-list
	   #:list-sample-of-vector
	   #:list-sample-of-hash-table-keys
	   #:list-sample-of-hash-table-values
	   #:list-sample-of-hash-table-pairs
	   #:list-sample-of-range
	   #:list-sample-of-iterator
	   #:list-sample-of-unbounded-iterator
	   
	   ;; Output as vectors.
	   #:vector-sample-of-list
	   #:vector-sample-of-vector
	   #:vector-sample-of-hash-table-keys
	   #:vector-sample-of-hash-table-values
	   #:vector-sample-of-hash-table-pairs
	   #:vector-sample-of-range
	   #:vector-sample-of-iterator
	   #:vector-sample-of-unbounded-iterator
	   
	   ;; Output as hash-tables.
	   #:hash-table-sample-of-hash-table
	   
	   ;; Generic functions.
	   #:sample
	   #:list-sample
	   #:vector-sample
	   #:map-sample
	   
	   ;; Testing.
	   #:test)
  (:use #:common-lisp))
