(asdf:defsystem #:simple-logger
  :description "A simple message logging system."
  :author "Michael Fiano <mail@michaelfiano.com>"
  :maintainer "Michael Fiano <mail@michaelfiano.com>"
  :license "MIT"
  :homepage "https://www.michaelfiano.com/projects/simple-logger"
  :source-control (:git "git@github.com:mfiano/simple-logger.git")
  :bug-tracker "https://github.com/mfiano/simple-logger/issues"
  :version "1.0.0"
  :encoding :utf-8
  :long-description #.(uiop:read-file-string (uiop/pathname:subpathname *load-pathname* "README.md"))
  :depends-on (#:alexandria
               #:local-time)
  :pathname "src"
  :serial t
  :components
  ((:file "package")
   (:file "logger")))
