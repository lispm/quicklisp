;;;  metafs.lisp --- Handle files metadata

;;;  Copyright (C) 2006, 2007, 2008, 2010 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: metafs

#+cmu (ext:file-comment "$Module: metafs.lisp $")

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

(cl:in-package :cl-user)

(defpackage :meta-file-system
  (:nicknames :metafs)
  (:use :common-lisp :sclf)
  (:shadow #:rename-file
	   #:delete-file
	   #:copy-file
	   #:list-directory
	   #:pathname
	   #:move-file)
  (:export #:list-directory
	   #:pathname
	   #:metadata-pathnames
	   #:rename-file
	   #:copy-file
	   #:delete-file
	   #:move-file
	   #:metadata
	   #:property
	   #:file-system-object #:system-file #:system-directory
	   #:fs-object
	   #:define-file-system-object
	   #:*auto-save*
	   #:*metadata-directory-name*
	   #:*default-file-class*
	   #:metadata-directory-p
	   #:remove-metadata
	   #:remove-property
	   #:save-metadata))

(in-package :meta-file-system)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric pathname (thing)
  (:documentation
   "Return a pathname object from THING."))



;; FILE-SYSTEM-OBJECT is meant to be extended for different file types which
;; are not of interest for this library.

(defclass file-system-object ()
  ((pathname :initarg :pathname
	     :reader pathname)
   (metadata :documentation "Alist of keywords and values")))

(defmethod initialize-instance ((file file-system-object) &key pathname &allow-other-keys)
  (unless pathname
    (error "PATHNAME is a required keyword argument to instantiate a ~A object" (type-of file)))
  (unless (pathnamep pathname)
    (error "PATHNAME has to be a PATHNAME object not a ~A" pathname))
  (call-next-method))

(defmethod print-object ((file file-system-object) stream)
  "Print FILE showing the pathname instead of the usual memory
address."
  (print-unreadable-object (file stream)
    (format stream "~A ~A" (type-of file) (pathname file))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric metadata-directory (file)
  (:documentation
   "Return the pathname of the directory containing the metadata for FILE."))

(defgeneric metadata-pathname (file)
  (:documentation
   "Return the pathname of the metadata file associated with FILE."))

(defgeneric metadata-pathnames (file)
  (:documentation
   "Return the list of pathnames of the metadata files associated with
FILE."))

(defgeneric remove-metadata (file)
  (:documentation
   "Permanently remove the metadata from FILE object."))

(defgeneric delete-metadata-file (file-system-object)
  (:documentation
   "Delete just the file containing the metadata.  Don't do anything
if metadata file doesn't exist.  Return true if the metadata file was
deleted otherwise false."))

(defgeneric rename-file (file new-path)
  (:documentation
   "Like CL:RENAME-FILE but polymorphically handle
METAFS:FILE-SYSTEM-OBJECTs as well."))

(defgeneric copy-file (file-system-object new-path &key if-exists)
  (:documentation
   "Copy FILE to NEW-PATH preserving its metadata in the copy.  Return
a new FILE-SYSTEM-OBJECT object."))

(defgeneric delete-file (file)
  (:documentation
   "Like CL:DELETE-FILE but takes care of removing the metadata as well."))

(defgeneric move-file (file new-path &key if-exists)
  (:documentation
   "Like RENAME-FILE but handle cross-filesystem relocations."))

(defgeneric fs-object (thing)
  (:documentation
   "Return a FILE-SYSTEM-OBJECT object of THING."))

(defgeneric list-directory (directory)
  (:documentation
   "Just like SCLF:LIST-DIRECTORY but this one returns a list of FILE-SYSTEM-OBJECT
objects.  Care is taken to skip metadata directories and their
contents."))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *metadata-directory-name* ".metadata"
  "Name of the directory that contains the metadata files.")

(defmethod metadata-directory ((file cl:pathname))
  (make-pathname :defaults file
		 :directory (append (pathname-directory file)
				    (list *metadata-directory-name*))
		 :name nil :type nil :version nil))

(defmethod metadata-directory ((file file-system-object))
  (metadata-directory (pathname file)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun metadata-directory-p (file)
   "Return true if PATHNAME is a metadata directory.  This function
may be used by programs that search through a directory tree, to be
able to skip metadata directories.  The DIRECTORY function in this
package does already this."
  (string= *metadata-directory-name* (file-namestring (pathname-as-file file))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod metadata-pathname ((file cl:pathname))
  (be pathname (pathname-as-file file)
    (make-pathname :defaults pathname
		   :directory (append (pathname-directory pathname)
				      (list *metadata-directory-name*)))))

(defmethod metadata-pathname ((file file-system-object))
  (metadata-pathname (pathname file)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod metadata-pathnames ((file file-system-object))
  (list (metadata-pathname file)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *auto-save* t
  "Whether setting a property or changing any metadata will
automatically make the change permanent on the filesystem.")

(defmethod load-metadata ((file file-system-object))
  "Load the metadata of FILE-SYSTEM-OBJECT from its file."
  (setf (slot-value file 'metadata)
	(read-metadata (pathname file))))

(defmethod save-metadata ((file file-system-object))
  "Save the metadata of FILE-SYSTEM-OBJECT into its file."
  (be pathname (metadata-pathname file)
    (ensure-directory pathname)
    (with-open-file (stream pathname :direction :output :if-exists :supersede)
      (write (metadata file) :stream stream))))

(defmethod metadata ((file file-system-object))
  (unless (slot-boundp file 'metadata)
    (load-metadata file))
  (slot-value file 'metadata))

(defmethod (setf metadata) (value (file file-system-object))
  "Setter method of the metadata slot.  Automatically make the
new value permanent on disk.  Return VALUE."
  (prog1 (setf (slot-value file 'metadata)
	       value)
    (when *auto-save*
      (save-metadata file))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod remove-metadata ((file file-system-object))
  (delete-metadata-file file)
  (setf (metadata file) nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod property ((file file-system-object) keyword)
  "Get a metadata property."
  (cdr (assoc keyword (metadata file))))

(defmethod (setf property) (value (file file-system-object) keyword)
  "Set or add a metadata property.  Return VALUE."
  (be property (assoc keyword (metadata file))
    (if property
	(prog1 (setf (cdr property) value)
	  (when *auto-save*
	    (save-metadata file)))
	(progn
	  (push (cons keyword value)
		(metadata file))
	  value))))

(defmethod remove-property ((file file-system-object) keyword)
  (setf (metadata file) (remove keyword (metadata file) :key #'car)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun read-metadata (pathname)
  "Read the metadata information belonging to PATHNAME file.
Return NIL if not available."
  (be path (metadata-pathname pathname)
    (when (probe-file path)
      (ignore-errors		      ; if data is corrupted ignore it
	(with-open-file (stream path)
	  (read stream nil))))))

(defvar *file-types-table* '()
  "Lookup table for the automatic instantiation of file-system-object
class objects.  This variable is normally updated by the
DEFINE-FILE-SYSTEM-OBJECT macro when defining new types derived from
FILE-SYSTEM-OBJECT.  This is an alist of strings or function and types.
See DEFINE-FILE-SYSTEM-OBJECT for further details.")

(defmacro define-file-system-object (name super-classes filename-types &rest class-declarations)
  "This is the macro that should be used to declared new classes
derived from FILE-SYSTEM-OBJECT.  The syntax is similar to defclass with few
differences: if no superclass is mentioned it defaults to FILE-SYSTEM-OBJECT;
FILENAME-TYPES is the list of file types this class represents.  This
list is used by MAKE-FILE-SYSTEM-OBJECT-FROM-FILE to automatically
instantiate the right object from a pathname.  FILENAME-TYPES is a
list of strings, pathnames, or functions: strings are matched against
pathname-type of the files, pathnames are matched against the whole
pathname of files, while the function is called passing the pathname
of files.  These functions should return true if they recognise the
file as of their type."
  (with-gensyms (type)
    `(progn
       (defclass ,name ,(if super-classes super-classes '(file-system-object))
	 ,@class-declarations)
       (dolist (,type ',filename-types)
	 (push (cons ,type ',name) *file-types-table*)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod pathname (thing)
  "Fall back method: use standard CL function."
  (cl:pathname thing))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun remove-empty-metadata-directory (file)
  "Attempt to remove the metadata directory if empty."
  ;; If the directory is not empty, this will signal an error that we
  ;; can ignore.
  (ignore-errors (delete-directory (metadata-directory file))))

(defmethod delete-metadata-file ((file file-system-object))
  (be pathname (metadata-pathname file)
    (prog1
	(when (probe-file pathname)
	  (cl:delete-file pathname)
	  t)
      (remove-empty-metadata-directory file))))

(defmethod delete-metadata-file ((file cl:pathname))
  (delete-metadata-file (fs-object file)))

(defmethod rename-file :around ((file file-system-object) new-path)
  (be pathname (metafs:pathname file)
    (call-next-method)
    (remove-empty-metadata-directory pathname)))

(defmethod rename-file ((file file-system-object) new-path)
  (when (probe-file new-path)
    (error "~A exists; can't overwrite it" new-path))
  (cl:rename-file (pathname file) (pathname-as-file new-path))
  ;; I have to be careful to either move everything or nothing
  (handler-case
      (be old-metadata-pathname (metadata-pathname file)
	  new-metadata-pathname (metadata-pathname new-path)
	(when (delete-metadata-file new-path)
	  (warn "~A looks like a stray metadata file; deleted." new-metadata-pathname))
	(when (probe-file old-metadata-pathname)
	  (ensure-directory new-metadata-pathname)
	  (cl:rename-file old-metadata-pathname new-metadata-pathname)))
    (serious-condition (condition)
      (cl:rename-file new-path (pathname file))
      ;; continue with the normal processing of condition
      (error condition)))
  ;; finally reflect the name change in the object itself
  (setf (slot-value file 'pathname) new-path)
  file)

(defmethod rename-file ((file cl:pathname) new-path)
  (rename-file (fs-object file) new-path))

(defmethod copy-file ((file file-system-object) new-path &key (if-exists :error))
  (sclf:copy-file (pathname file) new-path :if-exists if-exists)
  (ensure-directory new-path)
  (be md-path (metadata-pathname file)
    (when (probe-file md-path)
      (be md-new-path (metadata-pathname new-path)
	(ensure-directory md-new-path)
	(sclf:copy-file md-path md-new-path :if-exists if-exists))))
  (fs-object new-path))

(defmethod copy-file ((file cl:pathname) new-path &key (if-exists :error))
  (copy-file (fs-object file) new-path :if-exists if-exists))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod delete-file ((file file-system-object))
  "This method deletes only the metadata.  It's responsability of the
specialisers to delete the FILE itself."
  (delete-metadata-file file))

(defmethod delete-file ((file cl:pathname))
  (delete-file (fs-object file)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod move-file ((file file-system-object) new-path &key (if-exists :error))
  ;; If a simple rename can't be done (for instance when the rename is
  ;; cross-filesystem), copy the file and remove the original.
  (handler-case
      (rename-file file new-path)
    (file-error ()
      (copy-file file new-path :if-exists if-exists)
      (delete-file file)
      ;; finally reflect the name change in the object itself
      (setf (slot-value file 'pathname) new-path))))

(defmethod move-file ((file cl:pathname) new-path &key (if-exists :error))
  (move-file (fs-object file) new-path :if-exists if-exists))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun match-property-p (file keyword match-value)
  (be value (property file keyword)
    (when value
      (etypecase match-value
	(string (search match-value value))
	(function (funcall match-value value))))))

(defun find-metadata (search-paths metadata-matchers)
  (loop
     for dir in search-paths
     append (find-files dir #'(lambda (file)
				(etypecase metadata-matchers
				  (list
				   (loop
				      for (keyword . value-match) in metadata-matchers
				      always (match-property-p (fs-object file) keyword value-match)))
				  (function
				   (funcall metadata-matchers (metadata file))))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass system-file (file-system-object)
  ()
  (:documentation
   "Class that models standard files."))

(defmethod delete-file ((file system-file))
  (cl:delete-file (pathname file))
  (call-next-method))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-file-system-object system-directory ()
  (directory-p)
  ()
  (:documentation
   "Class that models file system directories."))

(defmethod delete-file ((directory system-directory))
  (call-next-method)
  (be pathname (metafs:pathname directory)
    (when (probe-file pathname)
      (delete-directory pathname))))

(defmethod move-file ((directory system-directory) new-path &key (if-exists :error))
  (if (probe-file new-path)
      (when (eq if-exists :error)
	(error "Can't rename directory because ~S already exists." new-path))
      (metafs:rename-file directory new-path)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *default-file-class* 'system-file
  "The class that is instantiated by default for an unknown file type.
See FILE-CLASS-BY-PATHNAME and MAKE-FILE-SYSTEM-OBJECT-FROM-FILE.")

(defun file-class-by-pathname (pathname)
  "Given a pathname return the class that best represents the kind of
file.  By default return *DEFAULT-FILE-CLASS*."
  (flet ((match (matcher)
	   (etypecase matcher
	     ((or function symbol) (funcall matcher pathname))
	     (cl:pathname (pathname-match-p pathname matcher))
	     (string (string-equal matcher (pathname-type pathname))))))
    (loop
       for (matcher . class) in *file-types-table*
       when (match matcher)
       return class
       finally (return *default-file-class*))))

(defun make-file-system-object-from-file (pathname)
  (make-instance (file-class-by-pathname pathname)
		 :pathname pathname))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod fs-object ((thing cl:pathname))
  (make-file-system-object-from-file thing))

(defmethod fs-object ((thing string))
  (fs-object (cl:pathname thing)))

(defmethod fs-object ((thing file-system-object))
  thing)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod list-directory ((pathname cl:pathname))
  (mapcar #'fs-object
	  (remove-if #'metadata-directory-p
		     (sclf:list-directory pathname :truenamep nil))))

(defmethod list-directory (thing)
  (list-directory (pathname thing)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun check-metadata-consistency (pathname)
  "Remove stray metadata files from directory PATHNAME."
  (setf pathname (pathname-as-directory pathname))
  (dolist (mdf (cl:directory (make-pathname :defaults pathname
					    :directory (append (pathname-directory pathname)
							       (list *metadata-directory-name*)))))
    (unless (probe-file (make-pathname :defaults mdf
				       :directory (pathname-directory pathname)))
      (cl:delete-file mdf)
      (warn "Stray metadata file ~A; removed.~%" mdf))))
