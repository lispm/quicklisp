;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: umlisp -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          umlisp.asd
;;;; Purpose:       ASDF system definition file for UMLisp
;;;; Author:        Kevin M. Rosenberg
;;;; Date Started:  Apr 2000
;;;;
;;;; $Id$
;;;;
;;;; This file, part of UMLisp, is
;;;;    Copyright (c) 2000-2002 by Kevin M. Rosenberg, M.D.
;;;;
;;;; UMLisp users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License.
;;;; *************************************************************************

(defpackage #:umlisp-system (:use #:asdf #:cl))
(in-package #:umlisp-system)

#+(or allegro lispworks cmu sbcl openmcl scl)
(defsystem umlisp-orf
    :components 
  ((:file "package")
   (:file "data-structures" :depends-on ("package"))
   (:file "utils" :depends-on ("data-structures"))
   (:file "sql" :depends-on ("utils"))
   (:file "parse-macros"  :depends-on ("sql"))
   (:file "parse-2002"  :depends-on ("parse-macros"))
   (:file "parse-common"  :depends-on ("parse-2002"))
   (:file "create-sql" :depends-on ("parse-common"))
   (:file "sql-classes" :depends-on ("sql"))
   (:file "classes" :depends-on ("sql-classes"))
   (:file "class-support" :depends-on ("classes"))
   (:file "composite" :depends-on ("sql-classes")))
  :depends-on (clsql clsql-postgresql-socket kmrcl hyperobject))

#+(or allegro lispworks cmu sbcl openmcl scl)
(defmethod perform ((o test-op) (c (eql (find-system 'umlisp-orf))))
  (oos 'load-op 'umlisp-orf-tests)
  (oos 'test-op 'umlisp-orf-tests))
