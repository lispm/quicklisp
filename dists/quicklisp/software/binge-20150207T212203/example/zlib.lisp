;;;  zlib.lisp --- example zlib FFI generation module

;;;  Copyright (C) 2004 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-274BC9CD44DCA4D1E2D8C15813CB1D8C.lisp,v 1.7 2004/03/24 16:06:36 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:
;;;
;;;  This works with gcc on FreeBSD.

#+cmu (ext:file-comment "$Id: F-274BC9CD44DCA4D1E2D8C15813CB1D8C.lisp,v 1.7 2004/03/24 16:06:36 wcp Exp $")

(in-package :binge-examples)

(defvar *zlib-ffi-c-constants*
  '("ZLIB_VERSION"
    "Z_NO_FLUSH"
    "Z_PARTIAL_FLUSH"
    "Z_SYNC_FLUSH"
    "Z_FULL_FLUSH"
    "Z_FINISH"
    "Z_OK"
    "Z_STREAM_END"
    "Z_NEED_DICT"
    "Z_ERRNO"
    "Z_STREAM_ERROR"
    "Z_DATA_ERROR"
    "Z_MEM_ERROR"
    "Z_BUF_ERROR"
    "Z_VERSION_ERROR"
    "Z_NO_COMPRESSION"
    "Z_BEST_SPEED"
    "Z_BEST_COMPRESSION"
    "Z_DEFAULT_COMPRESSION"
    "Z_FILTERED"
    "Z_HUFFMAN_ONLY"
    "Z_DEFAULT_STRATEGY"
    "Z_BINARY"
    "Z_ASCII"
    "Z_UNKNOWN"
    "Z_DEFLATED"
    "Z_NULL"))

(defun create-zlib-ffi ()
  (let ((namespace (make-instance 'c-namespace)))
    (binge-compiler:add-declaration *gcc-builtin-va_list* namespace)
    (populate-namespace namespace
			(extract-from-headers '("zlib.h")
					      :defines *gcc-null-defines*
					      :constants *zlib-ffi-c-constants*))
    ;; leaving out the types we'll get only the necessary ones
    (make-ffi #P"zlib-ffi.lisp" namespace :variables t :functions t :constants t)))
