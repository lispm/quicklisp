% Created 2016-02-26 Fri 11:43
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\author{John Maraist}
\date{\today}
\title{}
\hypersetup{
 pdfauthor={John Maraist},
 pdftitle={},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 24.3.1 (Org mode 8.3.3)}, 
 pdflang={English}}
\begin{document}


\section{Use Org-mode in your Common Lisp docstrings}
\label{sec:orgheadline15}
To more easily keep your documentation synchronized with the source
code, write your Common Lisp documentation strings in Org-mode format
and use this system to extract each symbol usage into its own separate
Org file. Then your user manual and other documentation can just
include these sampled extracts.  Updates to the manual can be
automated from updates to the docstrings - which we're more likely to
actually remember to do.

Requires \texttt{iterate}.
\subsection{Main routines}
\label{sec:orgheadline4}
\subsubsection{Function \texttt{write-package-files}}
\label{sec:orgheadline1}
Documents a package by writing an Org file for each defined symbol.
\begin{verbatim}
(write-package-files PACKAGE
                     [ :all FLAG ]
                     [ :system ASDF-SYSTEM-NAME ]
                     [ :path DIR ]
                     [ :index FLAG ]
                     [ :index-acc HASH-OR-NIL ]
                     [ :show-package FLAG ]
                     [ :hoist-exported FLAG ]
                     [ :page-title STRNIG ]
                     [ :always-disambiguate FLAG ]
                     [ :section-level NUMBER-OR-NIL ]
                     [ :package-headers FLAG ]
                     [ :usage-headers FLAG ]
                     [ :show-title FLAG ] )
\end{verbatim}
\begin{itemize}
\item The \texttt{package} argument should be a package specifier.
\item If the \texttt{all} keyword argument is given and is non-nil, then all symbols in
the package should be documented, instead of just exported symbols.
\item The \texttt{path} argument gives the directory where the files should be written.
This directory will be created if it does not exist.
\item A relative \texttt{path} is resolved relative to the location of the ASDF file
defining the given \texttt{system}.
\item When this function is called by another function in this package, \texttt{index-acc}
will be a hash-table used to accumulate symbol references for the index that
page will create.  When this function is called as an API from outside of this
system, or if no index will be needed, then the argument should be left \texttt{nil}.
\item The \texttt{show-package}, \texttt{hoist-exported}, and \texttt{page-title} keyword arguments
direct the formatting of the index page which this routine should create when
it is the entry call to this package.
\begin{itemize}
\item If \texttt{show-package} is non-nil, then the symbol's package name will be
dsplayed on any index page.
\item If \texttt{hoist-exported} in non-nil, then the list of symbols will be divided
according to the package which exports each symbol, or by internal symbols
of all packages.
\item If non-nil, \texttt{page-title} should be a string to be used as the page name.
\end{itemize}
\end{itemize}

\subsubsection{Function \texttt{write-packages}}
\label{sec:orgheadline2}
Document several packages by making a call to \texttt{write-package-files} for each.
\begin{verbatim}
(write-packages PACKAGES
                [ :default-all FLAG ]
                [ :default-system ASDF-SYSTEM-NAME ]
                [ :default-path DIR ]
                [ :package-extension FLAG ]
                [ :extension-downcase FLAG ]
                [ :index FLAG ]
                [ :index-system ASDF-SYSTEM-NAME ]
                [ :index-acc HASH-TABLE ]
                [ :show-package FLAG ]
                [ :hoist-exported page-title FLAG ]
                [ :show-title FLAG ]
                [ :package-headers FLAG ]
                [ :usage-headers FLAG ] )
\end{verbatim}
\begin{itemize}
\item The \texttt{packages} argument is a list giving a specification of the packages to be
documented.  Each element can be either a package designator or a list whose
first element is a package designator and other elements are keyword arguments
accepted by \texttt{write-package-files}.  These keywords will be used for the call
to \texttt{write-package-files} for that package.
\item The \texttt{default-all}, \texttt{default-system}, and \texttt{default-path} arguments give the
default arguments for the calls to \texttt{write-package-files}.
\item If \texttt{package-extension} is non-nil (its default is \texttt{t}), then whenever a
package spec does not give an explicit path, it should use a subdirectory of
the default path whose name is taken from the package.  If
\texttt{extension-downcase} is non-nil (its default is \texttt{t}), then the package name
is converted to lower-case for this extension.
\item The \texttt{index-acc} is a hash-table used to accumulate symbol references for an
index page, or \texttt{nil} if no index data should be saved.
\item The \texttt{show-package}, \texttt{hoist-exported}, and \texttt{page-title} keyword arguments
direct the formatting of the index page which this routine should create when
it is the entry call to this package.
\begin{itemize}
\item If \texttt{show-package} is non-nil, then the symbol's package name will be
dsplayed on any index page.
\item If \texttt{hoist-exported} in non-nil, then the list of symbols will be divided
according to the package which exports each symbol, or by internal symbols
of all packages.
\item If non-nil, \texttt{page-title} should be a string to be used as the page name.
\end{itemize}
\end{itemize}

\subsubsection{Function \texttt{write-symbol-files}}
\label{sec:orgheadline3}
Writes Org-mode files (in the directory named by \texttt{directory-path}) documenting the uses of the given \texttt{symbol}.
\begin{verbatim}
(write-symbol-files SYMBOL DIRECTORY-PATH
  [ :index-acc HASH ] [ :always-disambiguate FLAG ] )
\end{verbatim}
\begin{itemize}
\item The \texttt{index-acc} is a hash-table used to accumulate symbol references for an index page, or \texttt{nil} if no index data should be saved.
\item This function will write a separate file for each \emph{use} of the symbol, disambiguating the file name where necessary with \texttt{\_\_fn}, \texttt{\_\_var} and so forth.  If \texttt{always-disambiguate} is non-nil, then these suffixes will \emph{always} be added to the names of the generated files, even when a symbol has only one usage.
\end{itemize}
\subsection{Nonstandard documentation targets}
\label{sec:orgheadline6}
\subsubsection{Function \texttt{documentation*}}
\label{sec:orgheadline5}
Sometimes you may want to maintain documentation attached to
non-standard targets in the source code, and pull those strings into
the manual (for example, command options or available quoted forms).
Some Lisp implementations make it easy to use non-standard targets to
\texttt{documentation}; others essentially forbid it.  The \texttt{documentation*}
and \texttt{(setf documentation*)} functions manage the nonstandard documentation
types on platforms which do not allow it.
\begin{verbatim}
(documentation* X DOC-TYPE)
(setf (documentation* X DOC-TYPE) DOC-STRING)
\end{verbatim}
At this time, on Allegro Lisp these functions are inlined wrappers for the
standard \texttt{documentation} functions, and on other platforms it filters out the
non-standard types and stores them in the \texttt{+nonstandard-doc-type+} hash.

Note that when using \texttt{(setf documentation*)} to install documentation strings
of non-standard doc-type it is necessary to load the \texttt{org-sampler} ASDF system
as a prerequisite of the documented system.  This is in contrast to the use
case of merely assembling documentation separately from loading the system,
in which case there is no reason to \texttt{:depends-on} the Org-Sampler system.
\subsection{Global switches}
\label{sec:orgheadline12}
\subsubsection{Variable \texttt{*section-level*}}
\label{sec:orgheadline7}
If non-nil, then generated Org mode with begin with the indicated level of section header giving the name and use of the definition. If \texttt{nil}, no section header is generated.

\subsubsection{Variable \texttt{*show-package-header*}}
\label{sec:orgheadline8}
Whether a header line for the package should be written.

\subsubsection{Variable \texttt{*show-title*}}
\label{sec:orgheadline9}
Whether an initial comment with the title should be written.

\subsubsection{Variable \texttt{*show-usage-header*}}
\label{sec:orgheadline10}
Whether a header line for the usage should be written.

\subsubsection{Variable \texttt{*generate-html*}}
\label{sec:orgheadline11}
If non-nil, then an HTML file should be generated from each Org file.
\subsection{Self-documentation}
\label{sec:orgheadline14}
\subsubsection{Function \texttt{self-document}}
\label{sec:orgheadline13}
Applies \texttt{Org-Sampler} to itself in its own directory.
\end{document}
