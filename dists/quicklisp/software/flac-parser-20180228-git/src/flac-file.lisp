(in-package :flac-parser)

(defmacro with-flac-buffer ((buffer file-path) &body body)
  `(with-open-file (stream ,file-path :element-type '(unsigned-byte 8))
     (fast-io:with-fast-input (,buffer nil stream)
       ,@body)))

(defun get-file-size (file)
  (file-length (fast-io::input-buffer-stream (buffer file))))

(defun open-flac (file-path)
  (with-flac-buffer (buffer file-path)
    (let ((file (make-instance 'flac-file :path file-path :buffer buffer)))
      (setf (size file) (get-file-size file)
            (parse-tree file) (parse file :parse-tree))
      (type-check file (parse-tree file))
      file)))
