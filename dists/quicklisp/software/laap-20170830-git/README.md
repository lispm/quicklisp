# laap

[![build status](https://gitlab.com/ralt/laap/badges/master/build.svg)](https://gitlab.com/ralt/laap/commits/master)

A Common Lisp multi-threaded event loop.

From micro-benchmarks run on a random laptop (i.e. ballpark numbers to
give a rough idea), a small HTTP server could handle:

- laap: ~35k requests/sec
- cl-async: ~3k requests/sec
- Golang: ~125k requests/sec
- Aerys: ~30k requests/sec
- Node.js: ~3k requests/sec

Tested on linux/sbcl x86-64 only. (But definitely open to
compatibility fixes.)

What motivated the creation of this library:

- Lack of filesystem I/O support on the cl-async library.
- Lack of native Common Lisp event loop library. (To the author's
  knowledge.)

## Getting started

```lisp
(laap:with-event-loop
  (let ((socket (make-instance 'laap/socket:ipv4-tcp-socket)))
	(laap/socket:connect socket
						 (lambda (err res)
						   (when err (error err))
						   (laap/socket:send socket
											 :data (babel:string-to-octets "foo")))
											 :ip "127.0.0.1"
											 :port 80)))
```

This snippet creates a TCP socket to `127.0.0.1:80` and send the
string `foo` to it.

More examples in the `t/` folder.

## Features

Currently, laap supports:

- Sleeping
- Most operations for TCP IPv4 sockets.
- Most filesystem I/O operations.

laap plans on supporting the following features:

- All filesystem I/O operations.
- A built-in DNS resolver.
- Support for TCP IPv4 sockets options.
- Support for UDP/IPv6/Unix sockets.

## API

The API is based on callbacks. All the callbacks must accept 2
arguments: an error (possibly `nil`) and a result (possibly `nil`
too). This allows for easy wrapping around the callbacks.

### Core

TODO.

### Sockets

TODO.

### Filesystem

TODO.

### Internal

TODO.

## License

This library is provided under the MIT license. See [LICENSE](LICENSE).
