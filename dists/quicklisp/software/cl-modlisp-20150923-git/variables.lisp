;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: modlisp -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          base.lisp
;;;; Purpose:       Base data and functions for modlisp package
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  Dec 2002
;;;;
;;;; $Id$
;;;; *************************************************************************

(in-package #:modlisp)

(defconstant +default-modlisp-port+ 20123
  "Default port for listen")

(defvar *modlisp-socket* nil
  "the socket stream to modlisp")

(defvar *number-server-requests* 0
  "number of requests for the server")

(defvar *number-worker-requests* 0
  "number of requests for this worker process")

(defvar *close-modlisp-socket* t
  "whether to close the modlisp socket at the end of this request")


(defvar *ml-server* nil "Current ml-server instance")

(defclass ml-server ()
  ((listener :initarg :listener :initform nil :accessor listener)
   (port :initarg :port :initform nil :accessor port)
   (processor :initarg :processor :initform nil :accessor processor)
   (processor-args :initarg :processor-args :initform nil
                   :accessor processor-args)))




