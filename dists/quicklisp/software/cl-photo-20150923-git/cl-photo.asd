;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          cl-photo.asd
;;;; Purpose:       ASDF definition file for Lisp Markup Language Version 2
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  April 2005
;;;;
;;;; $Id$
;;;;
;;;; This file, part of cl-photo, is Copyright (c) 2005 by Kevin M. Rosenberg
;;;;
;;;; cl-photo users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License v2
;;;; (http://www.gnu.org/licenses/gpl.html)
;;;; *************************************************************************

(in-package #:cl-user)
(defpackage #:cl-photo-system (:use #:asdf #:cl))
(in-package #:cl-photo-system)

(defsystem cl-photo
  :name "cl-photo"
  :author "Kevin M. Rosenberg <kevin@rosenberg.net>"
  :version "1.0"
  :maintainer "Kevin M. Rosenberg <kmr@debian.org>"
  :licence "GNU General Public License"
  :description "Lisp Markup Language"
  :long-description "cl-photo calculates photography values."

  :depends-on (kmrcl)
  :components
  ((:file "package")
   (:file "convert" :depends-on ("package"))
   (:file "cameras" :depends-on ("convert"))
   (:file "fov" :depends-on ("cameras"))
   (:file "dof" :depends-on ("fov"))
   (:file "tables" :depends-on ("dof" "fov"))))

(defmethod perform ((o test-op) (c (eql (find-system 'cl-photo))))
  (operate 'load-op 'cl-photo-tests)
  (operate 'test-op 'cl-photo-tests))
