;;;; json-responses-test.lisp

(in-package #:ca.pinecode.json-responses.test)

(defparameter *reply* (make-instance 'reply))

(def-suite json-responses-test-suite)

;; generic JSON response function
(def-test json-response-test (:suite json-responses-test-suite)
  (let ((response (json-response :data "Hello, world!" :status +http-ok+
                                 :headers '(("X-HELLO" . "world")) :error nil)))
    (is (= (return-code*) 200))
    (is (equal (content-type*) "application/json"))
    (is (equal (assoc ':x-hello (headers-out*)) '(:x-hello . "world")))
    (is (equal response "{\"error\":null,\"data\":\"Hello, world!\"}"))))

;; generic JSON response function encoding a list in :data
(def-test json-response-with-list-test (:suite json-responses-test-suite)
  (let ((response (json-response :data (list "Hello" "World"))))
    (is (= (return-code*) 200))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":null,\"data\":[\"Hello\",\"World\"]}"))))

;; generic JSON response function encoding a list of alists in :data
(def-test json-response-with-alists (:suite json-responses-test-suite)
  (let ((response (json-response :data '(("Hello" . "World") ("Goodbye" . "Now")))))
    (is (= (return-code*) 200))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":null,\"data\":{\"Hello\":\"World\",\"Goodbye\":\"Now\"}}"))))

;; generic JSON response function encoding nested alists in :data
(def-test json-response-with-nested-alists (:suite json-responses-test-suite)
  (let ((response (json-response :data '((("Hello" . "World") ("Goodbye" . "Now"))))))
    (is (= (return-code*) 200))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":null,\"data\":[{\"Hello\":\"World\",\"Goodbye\":\"Now\"]}"))))

;; generic JSON response function encoding deeply nested alists in :data
(def-test json-response-with-nested-alists (:suite json-responses-test-suite)
  (let ((response (json-response :data '((("Hello" . "World")) (("Goodbye" . "Now"))))))
    (is (= (return-code*) 200))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":null,\"data\":[{\"Hello\":\"World\"},{\"Goodbye\":\"Now\"}]}"))))

;; macro-defined response function with an overridden error message
(def-test overridden-bad-response-test (:suite json-responses-test-suite)
  (let ((response (bad-request-response :error "Nope!")))
    (is (= (return-code*) 400))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Nope!\",\"data\":null}"))))

;; 1xx
(def-test continue-response-test (:suite json-responses-test-suite)
  (let ((response (continue-response)))
    (is (= (return-code*) 100))
    (is (equal (content-type*) "application/json"))
    (is (equal response nil))))

(def-test switching-protocols-response-test (:suite json-responses-test-suite)
  (let ((response (switching-protocols-response)))
    (is (= (return-code*) 101))
    (is (equal (content-type*) "application/json"))
    (is (equal response nil))))

;; 2xx
(def-test ok-response-test (:suite json-responses-test-suite)
  (let ((response (ok-response)))
    (is (= (return-code*) 200))
    (is (equal (content-type*) "application/json"))
    (is (equal response nil))))

(def-test created-response-test (:suite json-responses-test-suite)
  (let ((response (created-response)))
    (is (= (return-code*) 201))
    (is (equal (content-type*) "application/json"))
    (is (equal response nil))))

(def-test accepted-response-test (:suite json-responses-test-suite)
  (let ((response (accepted-response)))
    (is (= (return-code*) 202))
    (is (equal (content-type*) "application/json"))
    (is (equal response nil))))

(def-test non-authoritative-information-response-test (:suite json-responses-test-suite)
  (let ((response (non-authoritative-information-response)))
    (is (= (return-code*) 203))
    (is (equal (content-type*) "application/json"))
    (is (equal response nil))))

(def-test no-content-response-test (:suite json-responses-test-suite)
  (let ((response (no-content-response)))
    (is (= (return-code*) 204))
    (is (equal (content-type*) "application/json"))
    (is (equal response nil))))

(def-test reset-content-response-test (:suite json-responses-test-suite)
  (let ((response (reset-content-response)))
    (is (= (return-code*) 205))
    (is (equal (content-type*) "application/json"))
    (is (equal response nil))))

(def-test partial-content-response-test (:suite json-responses-test-suite)
  (let ((response (partial-content-response)))
    (is (= (return-code*) 206))
    (is (equal (content-type*) "application/json"))
    (is (equal response nil))))

;; 3xx
(def-test multiple-choices-response-test (:suite json-responses-test-suite)
  (let ((response (multiple-choices-response)))
    (is (= (return-code*) 300))
    (is (equal (content-type*) "application/json"))
    (is (equal response nil))))

(def-test moved-permanently-response-test (:suite json-responses-test-suite)
  (let ((response (moved-permanently-response)))
    (is (= (return-code*) 301))
    (is (equal (content-type*) "application/json"))
    (is (equal response nil))))

(def-test moved-temporarily-response-test (:suite json-responses-test-suite)
  (let ((response (moved-temporarily-response)))
    (is (= (return-code*) 302))
    (is (equal (content-type*) "application/json"))
    (is (equal response nil))))

(def-test see-other-response-test (:suite json-responses-test-suite)
  (let ((response (see-other-response)))
    (is (= (return-code*) 303))
    (is (equal (content-type*) "application/json"))
    (is (equal response nil))))

(def-test not-modified-response-test (:suite json-responses-test-suite)
  (let ((response (not-modified-response)))
    (is (= (return-code*) 304))
    (is (equal (content-type*) "application/json"))
    (is (equal response nil))))

(def-test use-proxy-response-test (:suite json-responses-test-suite)
  (let ((response (use-proxy-response)))
    (is (= (return-code*) 305))
    (is (equal (content-type*) "application/json"))
    (is (equal response nil))))

(def-test temporary-redirect-response-test (:suite json-responses-test-suite)
  (let ((response (temporary-redirect-response)))
    (is (= (return-code*) 307))
    (is (equal (content-type*) "application/json"))
    (is (equal response nil))))

;; 4xx
(def-test bad-request-response-test (:suite json-responses-test-suite)
  (let ((response (bad-request-response)))
    (is (= (return-code*) 400))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Bad request\",\"data\":null}"))))

(def-test authorization-required-response-test (:suite json-responses-test-suite)
  (let ((response (authorization-required-response)))
    (is (= (return-code*) 401))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Authorization required\",\"data\":null}"))))

(def-test forbidden-response-test (:suite json-responses-test-suite)
  (let ((response (forbidden-response)))
    (is (= (return-code*) 403))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Forbidden\",\"data\":null}"))))

(def-test not-found-response-test (:suite json-responses-test-suite)
  (let ((response (not-found-response)))
    (is (= (return-code*) 404))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Not found\",\"data\":null}"))))

(def-test method-not-allowed-response-test (:suite json-responses-test-suite)
  (let ((response (method-not-allowed-response)))
    (is (= (return-code*) 405))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Method not allowed\",\"data\":null}"))))

(def-test not-acceptable-response-test (:suite json-responses-test-suite)
  (let ((response (not-acceptable-response)))
    (is (= (return-code*) 406))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Not acceptable\",\"data\":null}"))))

(def-test proxy-authentication-required-response-test (:suite json-responses-test-suite)
  (let ((response (proxy-authentication-required-response)))
    (is (= (return-code*) 407))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Proxy authentication required\",\"data\":null}"))))

(def-test request-timeout-response-test (:suite json-responses-test-suite)
  (let ((response (request-timeout-response)))
    (is (= (return-code*) 408))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Request timeout\",\"data\":null}"))))

(def-test conflict-response-test (:suite json-responses-test-suite)
  (let ((response (conflict-response)))
    (is (= (return-code*) 409))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Conflict\",\"data\":null}"))))

(def-test gone-response-test (:suite json-responses-test-suite)
  (let ((response (gone-response)))
    (is (= (return-code*) 410))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Gone\",\"data\":null}"))))

(def-test length-required-response-test (:suite json-responses-test-suite)
  (let ((response (length-required-response)))
    (is (= (return-code*) 411))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Length required\",\"data\":null}"))))

(def-test precondition-failed-response-test (:suite json-responses-test-suite)
  (let ((response (precondition-failed-response)))
    (is (= (return-code*) 412))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Precondition failed\",\"data\":null}"))))

(def-test request-entity-too-large-response-test (:suite json-responses-test-suite)
  (let ((response (request-entity-too-large-response)))
    (is (= (return-code*) 413))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Request entity too large\",\"data\":null}"))))

(def-test request-uri-too-long-response-test (:suite json-responses-test-suite)
  (let ((response (request-uri-too-long-response)))
    (is (= (return-code*) 414))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Request URI too long\",\"data\":null}"))))

(def-test unsupported-media-type-response-test (:suite json-responses-test-suite)
  (let ((response (unsupported-media-type-response)))
    (is (= (return-code*) 415))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Unsupported media type\",\"data\":null}"))))

(def-test requested-range-not-satisfiable-response-test (:suite json-responses-test-suite)
  (let ((response (requested-range-not-satisfiable-response)))
    (is (= (return-code*) 416))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Requested range not satisfiable\",\"data\":null}"))))

(def-test expectation-failed-response-test (:suite json-responses-test-suite)
  (let ((response (expectation-failed-response)))
    (is (= (return-code*) 417))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Expectation failed\",\"data\":null}"))))

;; 5xx
(def-test internal-server-error-response-test (:suite json-responses-test-suite)
  (let ((response (internal-server-error-response)))
    (is (= (return-code*) 500))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Internal server error\",\"data\":null}"))))

(def-test not-implemented-response-test (:suite json-responses-test-suite)
  (let ((response (not-implemented-response)))
    (is (= (return-code*) 501))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Not implemented\",\"data\":null}"))))

(def-test bad-gateway-response-test (:suite json-responses-test-suite)
  (let ((response (bad-gateway-response)))
    (is (= (return-code*) 502))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Bad gateway\",\"data\":null}"))))

(def-test service-unavailable-response-test (:suite json-responses-test-suite)
  (let ((response (service-unavailable-response)))
    (is (= (return-code*) 503))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Service unavailable\",\"data\":null}"))))

(def-test gateway-timeout-response-test (:suite json-responses-test-suite)
  (let ((response (gateway-timeout-response)))
    (is (= (return-code*) 504))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"Gateway timeout\",\"data\":null}"))))

(def-test http-version-not-supported-response-test (:suite json-responses-test-suite)
  (let ((response (http-version-not-supported-response)))
    (is (= (return-code*) 505))
    (is (equal (content-type*) "application/json"))
    (is (equal response "{\"error\":\"HTTP version not supported\",\"data\":null}"))))
