# golden-utils

Auxiliary Utilities (AU).

## Overview

A collection of personal utilities that have been found useful.

## Install

```lisp
(ql:quickload :golden-utils)
```

## Usage

TODO

## License

Copyright © 2017-2018 [Michael Fiano](mailto:mail@michaelfiano.com).

Licensed under the MIT License.
