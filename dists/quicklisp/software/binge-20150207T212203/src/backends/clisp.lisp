;;;  clisp.lisp --- output module for CLISP

;;;  Copyright (C) 2004 by Walter C. Pelissero

;;;  Author: Aurelio Bignoli <aurelio@bignoli.it>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-FCC3FB77D4A37095C3A7B70FCF6D4D1E.lisp,v 1.11 2004/04/09 12:10:44 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-FCC3FB77D4A37095C3A7B70FCF6D4D1E.lisp,v 1.11 2004/04/09 12:10:44 wcp Exp $")

(in-package :binge-backend)

(defclass clisp-backend (backend) ())

(defmethod use-packages ((backend clisp-backend))
  (declare (ignore backend))
  '("COMMON-LISP" "FFI"))

(defmethod ffi-sizeof (thing (backend clisp-backend))
  (format nil "(sizeof '~A)" thing))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Warning: c-object->ffi-notation on a c-argument is called only in
;; the case of a sizeof because in all the other cases we format the
;; object in a different way.
;;
;; FIXME: sizeofs are currently bad implemented, hence this modelling
;; nonsense -wcp7/4/04.
(defmethod c-object->ffi-notation ((object c-argument) (backend clisp-backend))
  (c-object->ffi-notation (c-type object) backend))

(defmethod c-object->ffi-notation ((object c-slot) (backend clisp-backend))
  (format nil "(~A ~A)"
	  (c-name object)
	  (c-object->ffi-notation (c-type object) backend)))

(defmethod c-object->ffi-notation ((object c-pointer) (backend clisp-backend))
  (case (c-type object)
    (:void
     ;; pointer to anything
     "c-pointer")
    ((:char :unsigned-char)
     "c-string")
    (t
     (format nil "(c-ptr-null ~A)" 
	     (c-object->ffi-notation (c-type object) backend)))))

(defmethod c-object->ffi-notation ((object c-user-type-ref) (backend clisp-backend))
  (c-object-lisp-name object backend))

(defmethod c-object->ffi-notation ((object c-struct-ref) (backend clisp-backend))
  (c-object-lisp-name object backend))

(defmethod c-object->ffi-notation ((object c-union-ref) (backend clisp-backend))
  (c-object-lisp-name object backend))

(defmethod c-object->ffi-notation ((object c-enum-ref) (backend clisp-backend))
  (c-object-lisp-name object backend))

(defmethod c-object->ffi-notation ((object c-function) (backend clisp-backend))
  (format nil "(c-function (:arguments~{ ~A~}) (:return-type ~A) (:language :stdc))"
	  (loop
	     for arg in (c-arguments object)
	     for i from 1 by 1
	     collect (if (c-name arg)
			 (format nil "(~A ~A)" (c-name arg)
				 (c-object->ffi-notation (c-type arg) backend))
			 ;; the use of a dash in the name of arguments
			 ;; guarantees we won't have any name clash
			 (format nil "(arg-~A ~A)" i
				 (c-object->ffi-notation (c-type arg) backend))))
	  (c-object->ffi-notation (c-type object) backend)))

(defmethod c-object->ffi-notation ((object c-array) (backend clisp-backend))
  (format nil "(c-array ~A~{ ~A~})" 
	  (c-object->ffi-notation (c-type object) backend)
	  (convert-array-dimensions (c-dimensions object) backend)))

(defmethod c-object->ffi-notation ((object symbol) (backend clisp-backend))
  (case object
    (:void "nil")
    (:char "char")
    (:short "short")
    (:int "int")
    (:long "long")
    (:unsigned-char "uchar")
    (:unsigned-short "ushort")
    (:unsigned-int "uint")
    (:unsigned-long "ulong")
    (:float "float")
    (:double "double")
    (t
     (error 'binge-compiler:compilation-unsupported-error
	    :item object))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod c-object->ffi-notation ((decl c-typedef-declaration) (backend clisp-backend))
  (format nil "(def-c-type ~A ~A)"
	  (c-object-lisp-name decl backend) 
	  (c-object->ffi-notation (c-type decl) backend)))

(defmethod c-object->ffi-notation ((decl c-struct-declaration) (backend clisp-backend))
  (format nil "(def-c-struct ~A~@[~%  ~<~@{~A~^ ~_~}~:>~])"
	  (c-object-lisp-name decl backend)
	  (mapcar #'(lambda (slot)
		      (c-object->ffi-notation slot backend))
		  (c-slots decl))))

;; This is not an error.  CLISP provides a means only to define
;; structures but not unions.  This form is a suitable surrogate.
(defmethod c-object->ffi-notation ((decl c-union-declaration) (backend clisp-backend))
  (format nil "(def-c-type ~A (c-union ~<~@{~A~^ ~_~}~:>))"
	  (c-object-lisp-name decl backend)
	  (mapcar #'(lambda (slot)
		      (c-object->ffi-notation slot backend))
		  (c-slots decl))))

(defmethod c-object->ffi-notation ((decl c-enum-declaration) (backend clisp-backend))
  (format nil "(def-c-enum ~A ~<~@{~A~^ ~:_~}~:>)"
	  (c-object-lisp-name decl backend)
	  (mapcar #'(lambda (c)
		      (if (consp c)
			  (format nil "(~@[~A~]~A ~A)" (backend-prefix backend) (car c) (cadr c))
			  (format nil "~@[~A~]~A" (backend-prefix backend) c)))
		  (c-constants decl))))

(defmethod c-object->ffi-notation ((decl c-function-declaration) (backend clisp-backend))
  (format nil "(def-call-out ~A~@
		~2T(:name ~S)~@
		~2T(:arguments ~<~@{~A~^ ~:_~}~:>)~@
		~2T(:return-type ~A)~@
		~2T(:language :stdc)~@[~&  (:library ~S)~])"
	  (c-object-lisp-name decl backend)
	  (c-name decl)
	  (loop
	     for arg in (c-arguments decl)
	     for i from 1 by 1
	     collect (if (c-name arg)
			 (format nil "(~A ~A)" (c-name arg)
				 (c-object->ffi-notation (c-type arg) backend))
			 ;; the use of a dash in the name of arguments
			 ;; guarantees we won't have any name clash
			 (format nil "(arg-~A ~A)" i
				 (c-object->ffi-notation (c-type arg) backend))))
	  (c-object->ffi-notation (c-type decl) backend)
	  (or (c-library decl)
	      (backend-library backend))))

(defmethod c-object->ffi-notation ((decl c-variable-declaration) (backend clisp-backend))
  (format nil "(def-c-var ~A (:name ~S) (:type ~A)~@[ (:library ~S)~])"
	  (c-object-lisp-name decl backend)
	  (c-name decl)
	  (c-object->ffi-notation (c-type decl) backend)
	  (or (c-library decl)
	      (backend-library backend))))

(setf binge-compiler:*backend* (make-instance 'clisp-backend))
(export 'clisp-backend)		     ; so that we can use this backend

