(cl:defpackage :simple-flow-dispatcher
  (:use :cl :alexandria)
  (:export make-simple-dispatcher
           free-simple-dispatcher))
