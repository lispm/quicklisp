;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10; Package: photo -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          convert.lisp
;;;; Purpose:       Conversions functions for cl-photo
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  April 2005
;;;;
;;;; $Id$
;;;;
;;;; This file, part of cl-photo, is Copyright (c) 2005 by Kevin M. Rosenberg
;;;;
;;;; cl-photo users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License v2
;;;; (http://www.gnu.org/licenses/gpl.html)
;;;;
;;;; *************************************************************************

(in-package #:photo)

(defconstant +radian->degrees+ (/ 360d0 pi 2))
(defconstant +inches->mm+ 25.4d0)

(declaim (inline diagonal))
(defun diagonal (x y)
  (sqrt (+ (* x x) (* y y))))

(declaim (inline radians->degrees))
(defun radians->degrees (r)
  (* +radian->degrees+ r))

(declaim (inline degrees->radians))
(defun degrees->radians (r)
  (/ r +radian->degrees+))

(declaim (inline mm->feet))
(defun mm->feet (d)
  (/ d +inches->mm+ 12))

(declaim (inline feet->mm))
(defun feet->mm (d)
  (* d 12 +inches->mm+))

(declaim (inline inches->mm))
(defun inches->mm (d)
  (* d +inches->mm+))

(declaim (inline mm->inches))
(defun mm->inches (d)
  (/ d +inches->mm+))

(defun length->mm (d units)
  "Convert a length in units to mm."
  (ecase units
    (:mm d)
    (:inches (inches->mm d))
    (:feet (inches->mm (* d 12)))
    (:yards (inches->mm (* d 36)))
    (:meters (* 1000 d))))

(defun mm->length (d units)
  "Convert a number of mm to units."
  (ecase units
    (:mm d)
    (:inches (mm->inches d))
    (:feet (/ (mm->inches d) 12))
    (:yards (/ (mm->inches d) 36))
    (:meters (/ d 1000))))


