(in-package :cl-user)

(defpackage #:simple-logger
  (:use #:cl)
  (:export #:*current-level*
           #:define-message
           #:emit))
