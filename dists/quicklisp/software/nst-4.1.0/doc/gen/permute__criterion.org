# DO NOT EDIT --- generated from source code.
*** Criterion =permute=
The =:permute= criterion evaluates the form under test, expecting to find a list as a result.  The criterion expects to find that some permutation of this list will satisfy the subordinate criterion.

Usage:
#+begin_example
(:permute CRITERION)
#+end_example
Example:
#+begin_example
(def-test permute1 (:permute (:each (:eq 'a))) '(a a))
(def-check permute2
    (:permute (:seq (:symbol b)
                    (:predicate symbolp)
                    (:predicate numberp)))
  '(1 a b))
#+end_example

