;;;  cobstor-tests.asd --- system description for the regression tests

;;;  Copyright (C) 2005, 2008, 2009 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: cobstor

#+cmu (ext:file-comment "$Module: cobstor-tests.asd, Time-stamp: <2009-01-09 10:30:26 wcp> $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

#+nil
(error "The feature NIL has been defined.  This has been reserved to comment out sexps.")

#-cmu
(warn "This code hasn't been tested on your Lisp system.")

(defpackage :cobstor-tests-system
  (:use :common-lisp :asdf #+asdfa :asdfa)
  (:export #:*base-directory*
	   #:*compilation-epoch*))

(in-package :cobstor-tests-system)

(defsystem cobstor-tests
    :name "COBSTOR-tests"
    :author "Walter C. Pelissero <walter@pelissero.de>"
    :maintainer "Walter C. Pelissero <walter@pelissero.de>"
    :description "Test suite for the COBSTOR library"
    :licence "LGPL"
    :depends-on (:rt :cobstor :sclf)
    :components
    ((:module test
	      :components
	      ((:file "package")
	       (:file "class" :depends-on ("package"))
	       (:file "field" :depends-on ("package" "class"))
	       (:file "composite" :depends-on ("package"))))))

;; when loading this form the regression-test, the package is yet to
;; be loaded so we cannot use rt:do-tests directly or we would get a
;; reader error (unknown package)
(defmethod perform ((o test-op) (c (eql (find-system :cobstor-tests))))
  (or (funcall (intern "DO-TESTS" "REGRESSION-TEST"))
      (error "test-op failed")))
