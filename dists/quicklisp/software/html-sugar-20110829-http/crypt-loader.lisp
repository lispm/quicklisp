;;;  crypt-loader.lisp --- load libcrypt

;;;  Copyright (C) 2007, 2008 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: html sugar

#+cmu (ext:file-comment "$Module: crypt-loader.lisp $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

;;; After having CMUCL refusing to execute this eval-when in
;;; sugar.lisp for a day, I ended up having the loading of the foreign
;;; library in a different lisp module.  It just works and I don't
;;; bloody care why.

(in-package :html-sugar)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *crypt-library-pathname*
    (uffi:find-foreign-library "libcrypt"
			       ;; cmucl is a 32 bit ELF even on amd64
			       '(#+cmu "/usr/lib32/"
				 "/usr/local/lib/" "/usr/lib/" "/lib/")))
  (defvar *crypt-library-loaded* nil)
  (unless *crypt-library-loaded*
    (format t "~&; loading crypt shared library ~A~%" *crypt-library-pathname*)
    (uffi:load-foreign-library *crypt-library-pathname*
			       :supporting-libraries '("c")))
  (setq *crypt-library-loaded* t))
