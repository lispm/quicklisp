;;; -*- mode: Lisp; Syntax: Common-Lisp; -*-
;;;
;;; Copyright (c) 2009 by the authors.
;;;
;;; See LICENCE for details.

(in-package :hu.dwim.presentation)

;;;;;;
;;; flow/chart

(def (component e) flow/chart (standard/chart)
  ())

(def render-xhtml flow/chart
  (not-yet-implemented))
