;;; -*- Mode: Lisp; Package: CL-User -*-
;;;
;;; Copyright (c) 2009 - 2015 Paul Onions
;;; See LICENCE file for licence information.
;;;
;;; Package definitions for the CL-GRACE system
;;;
(in-package :cl-user)

(defpackage :cl-grace
  (:use :common-lisp)
  (:nicknames :grace)
  (:import-from :cl-fad
   #:with-output-to-temporary-file)
  (:export
   #:*grace-program*
   #:*grace-args*
   ;; Low-level API
   #:grace-plot #:grace-program #:grace-args #:grace-process
   #:new-plot #:open-plot #:connect-plot
   #:save-plot #:close-plot #:disconnect-plot
   #:active-plot-p #:send-command #:refresh-plot
   ;; Mid-level API
   #:*refresh-plot* #:without-refresh
   #:create-graph #:delete-graph #:arrange-graphs
   #:set-graph-type #:set-graph-title #:set-graph-subtitle
   #:set-graph-view #:set-graph-axes #:set-graph-legend
   #:autoscale-graph #:autotick-graph
   #:create-dataset #:delete-dataset
   #:flatten-datum #:encode-datum #:send-data #:send-dataset
   #:set-dataset-line #:set-dataset-fill #:set-dataset-symbol
   #:set-dataset-legend #:set-dataset-annotation
   ;; High-level API -- easy plotting
   #:*plot* #:*graph* #:*dataset* #:*autoscale-graph* #:*autotick-graph*
   #:ensure-active-plot #:ensure-clear-graph #:layout #:graph #:cmd
   #:plot #:replot
   ;; Utilities
   #:hsv->rgb))
