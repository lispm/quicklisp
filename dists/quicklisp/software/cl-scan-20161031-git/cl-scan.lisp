(in-package #:cl-scan)

(defvar *ip-vec*)

(defun n-processors ()
  "queries system for number of cores to set the worker size"
  (parse-integer (run/s "getconf _NPROCESSORS_ONLN")))

(defun make-ip-vector (hostname port-min port-max)
  (progn
    (setf *ip-vec* (make-array port-max :fill-pointer 0))
    (build-ip-vector hostname port-min port-max)))

(defun build-ip-vector (hostname port-min port-max)
  (loop for i from port-min to port-max
	do (vector-push (vector hostname i) *ip-vec*))) 

(defun scan (source timeout)
  (ignore-errors
   (let* ((host (aref source 0))
	  (port (aref source 1))
	  (sock (socket-connect host port :timeout timeout)))
     (progn
       (socket-close sock)
       (format t "~d is open~%" port)
       (force-output)))))

(defun main (hostname port-min port-max timeout)
  (progn
    (setf *kernel* (make-kernel (n-processors)))
    (make-ip-vector hostname port-min port-max)
    (pmap nil #'(lambda (x) (scan x timeout)) *ip-vec*)))
