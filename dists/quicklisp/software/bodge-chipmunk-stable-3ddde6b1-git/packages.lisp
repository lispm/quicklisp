(cl:defpackage :%chipmunk
  (:nicknames :%cp)
  (:use))

(cl:defpackage :chipmunk
  (:nicknames :cp)
  (:use :cl :claw :alexandria)
  (:export libchipmunk
           +vzero+
           v))
