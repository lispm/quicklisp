;;;  decl.lisp --- C declarations types

;;;  Copyright (C) 2004, 2005 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-DC764BA2B0A6F299C77B06F8BBF51E66.lisp,v 1.12 2004/12/16 12:02:25 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-DC764BA2B0A6F299C77B06F8BBF51E66.lisp,v 1.12 2004/12/16 12:02:25 wcp Exp $")

(in-package :binge-decl)


(defclass c-object ()
  ()
  (:documentation
   "Root class for all C language related objects: declarations,
types and so on."))

(defclass c-named-object (c-object)
  ((name :accessor c-name
	 :initarg :name
	 :initform (gensym)
	 :type (or string symbol)))
  (:documentation
   "Mixin class for C objects (declarations/types/variable/slots/
whatever) bearing a name."))

(defclass c-typed-object (c-object)
  ((type :accessor c-type
	 :initarg :type
	 :initform nil
	 :type (or c-type keyword)))
  (:documentation
   "Mixin class for C objects having a type."))

(defclass c-declaration (c-named-object)
  ((documentation :accessor c-documentation
		  :initarg :documentation
		  :initform nil
		  :type (or string null))
   (alternate-name :accessor c-alternate-name
		   :initarg :alternate-name
		   :initform nil
		   :type (or string symbol null))
   ;; whether this declaration has been required by the user
   ;; him/her-self
   (required :accessor c-required
	     :initarg :required
	     :initform nil
	     :type (or symbol null))
   ;; if this object is to be found in a particular shared library
   ;; (some backends require this)
   (library :accessor c-library
	    :initarg :library
	    :initform nil
	    :type (or string null)))
  (:documentation
   "Base class modelling the C declarations."))

(defclass c-type (c-object)
  ()
  (:documentation
   "Base class modelling the C types.  Those which are not declarations."))

(defclass c-function (c-type c-typed-object)
  ((arguments :accessor c-arguments
	      :initarg :arguments
	      :type list))
  (:documentation
   "Class modelling the C function types."))

(defclass c-function-declaration (c-declaration c-function)
  ()
  (:documentation
   "Class modelling the C function declarations."))

(defclass c-container (c-type)
  ((slots :accessor c-slots
	  :initarg :slots
	  :type list))
  (:documentation
   "Base class modelling C structured types; those containing a list
of other objects."))

(defclass c-type-ref (c-type)
  ((ref-name :accessor c-ref-name
	     :initarg :ref-name
	     :type (or string symbol)))
  (:documentation
   "Base class modelling references to C types.  Whether they are
typedef, struct, union or enum."))

(defclass c-user-type-ref (c-type-ref)
  ()
  (:documentation
   "Class modelling references to user defined C types (those
created with typedefs) ."))

(defclass c-typedef-declaration (c-declaration c-typed-object)
  ()
  (:documentation
   "Class modelling C user type definition (aka typedef) ."))

(defclass c-struct-ref (c-type-ref)
  ()
  (:documentation
   "Class modeliing references to C struct."))

(defclass c-struct-declaration (c-declaration c-container)
  ()
  (:documentation
   "Class modelling C struct declarations."))

(defclass c-union-ref (c-type-ref)
  ()
  (:documentation
   "Class modeliing references to C unions."))

(defclass c-union-declaration (c-declaration c-container)
  ()
  (:documentation
   "Class modelling C union declarations."))

(defclass c-enum-ref (c-type-ref)
  ()
  (:documentation
   "Class modelling references to C enums"))

(defclass c-enum-declaration (c-declaration c-type)
  ((constants :accessor c-constants
	      :initarg :constants
	      :type list))
  (:documentation
   "Class modelling C enumerated declarations."))

(defclass c-variable-declaration (c-declaration c-typed-object)
  ()
  (:documentation
   "Class modelling C variable declarations."))

(defclass c-typed-name (c-named-object c-typed-object)
  ()
  (:documentation
   "Base class for objects having a name and a type."))

(defclass c-slot (c-typed-name)
  ()
  (:documentation
   "Class modelling C struct/union slots."))

(defclass c-argument (c-typed-name)
  ()
  (:documentation
   "Class modelling C function arguments."))

(defclass c-constant-declaration (c-declaration)
  ((value :accessor c-value
	  :initarg :value))
  (:documentation
   "Class modelling C constant declarations."))

(defclass c-bitfield (c-type c-typed-object)
  ((bits :accessor c-bits
	 :initarg :bits
	 :type integer))
  (:documentation
   "Class modelling C bitfields."))

(defclass c-pointer (c-type c-typed-object)
  ()
  (:documentation
   "Class modelling C pointers."))

(defclass c-array (c-type c-typed-object)
  ((dimensions :accessor c-dimensions
	       :initarg :dimensions
	       :type list))
  (:documentation
   "Class modelling C arrays."))


(defmethod print-object ((object c-named-object) stream)
  (format stream "#<~A ~S>" (type-of object) (c-name object)))

(defun primitive-type-p (object)
  (symbolp object))

(defun anonymous-declaration-p (decl)
  (declare (type c-declaration decl))
  (or (not (c-name decl))
      (symbolp (c-name decl))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric make-c-reference (type)
  (:documentation
   "Make a reference to TYPE if TYPE is among those
referenciables (struct, union, enum), otherwise return NIL."))

(defmethod make-c-reference (type)
  nil)

(defmethod make-c-reference ((type c-struct-declaration))
  (make-instance 'c-struct-ref :ref-name (c-name type)))

(defmethod make-c-reference ((type c-union-declaration))
  (make-instance 'c-union-ref :ref-name (c-name type)))

(defmethod make-c-reference ((type c-enum-declaration))
  (make-instance 'c-enum-ref :ref-name (c-name type)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
