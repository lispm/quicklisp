;;;  stub.lisp --- creation of Lisp proxy stubs

;;;  Copyright (C) 2005, 2007, 2009 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: cobstor

#+cmu (ext:file-comment "$Module: stub.lisp $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

(in-package :cobstor-generator)

(defun spc (stream)
  (write-char #\space stream))

(defun case-string (string)
  "Up/down-case STRING according to the value of *PRINT-CASE*."
  (case *print-case*
    (:downcase (string-downcase string))
    (:upcase (string-downcase string))
    (:capitalize (string-capitalize string))
    ;; what the hell...
    (t string)))

(defun pprint-cobol-subfields (stream subfields)
  (pprint-logical-block (stream subfields :prefix "(" :suffix ")")
    (pprint-exit-if-list-exhausted)
    (loop
       (pprint-cobol-field stream (pprint-pop))
       (pprint-exit-if-list-exhausted)
       (spc stream)
       (pprint-newline :mandatory stream))))

(defun pprint-plist (stream properties)
  (pprint-logical-block (stream properties)
    (loop
       (pprint-logical-block (stream (list (pprint-pop) (pprint-pop)))
	 (be key (pprint-pop)
	   (write key :stream stream)
	   (spc stream)
	   (pprint-indent :block 2 stream)
	   (if (eq key :alternates)
	       (progn
		 (pprint-newline :fill stream)
		 (pprint-cobol-subfields stream (pprint-pop)))
	       (write (pprint-pop) :stream stream))))
       (pprint-exit-if-list-exhausted)
       (spc stream)
       (pprint-newline :linear stream))))

(defun pprint-cobol-field (stream definition)
  "Pretty print on *STANDARD-OUTPUT* a cobol field as Lisp code."
  (pprint-logical-block (stream definition :prefix "(" :suffix ")")
    ;; the field name (it could be a symbol)
    (princ (case-string (string (pprint-pop))) stream)
    (pprint-exit-if-list-exhausted)
    (spc stream)
    (pprint-indent :block 2 stream)
    ;; the type
    (be type (pprint-pop)
      (if (consp type)
	  ;; it's a collection of subfield, not a type
	  (progn
	    (pprint-newline :mandatory stream)
	    (pprint-cobol-subfields stream type))
	  ;; it's indeed a type
	  (write type :stream stream)))
    (pprint-exit-if-list-exhausted)
    (spc stream)
    (pprint-newline :fill stream)
    ;; the properties
    (pprint-plist stream (cddr definition))))

(defun pprint-define-cobol-record (stream name fields &key alternates)
  "Pretty print on *STANDARD-OUTPUT* a Cobol record definition in
Lisp language.  FIELDS are composing the record."
  (pprint-logical-block (stream (mapcar #'field-declaration->sexp fields) :prefix "(" :suffix ")")
    (princ "define-cobol-record" stream)
    (spc stream)
    (pprint-newline :miser stream)
    (pprint-indent :current 0 stream)
    ;; the name of the record
    (pprint-logical-block (stream nil :prefix "(" :suffix ")")
      (princ (case-string name) stream)
      (when alternates
	(spc stream)
	(prin1 :alternates stream)
	(spc stream)
	(prin1 t stream)))
    (pprint-indent :block 1 stream)
    ;; the body of the record (that is, the fields)
    (loop
       (pprint-exit-if-list-exhausted)
       (spc stream)
       (pprint-newline :linear stream)
       (pprint-cobol-field stream (pprint-pop)))))

(defun proxy-stub-file-pathname (name)
  (make-pathname :defaults
		 (translate-logical-pathname "cobstor-dynamic:proxy;*.lisp")
		 :name name))

(defgeneric field-names (field-declaration)
  (:documentation
   "Return a list of all field names at all level of nesting."))

(defmethod field-names ((field field-declaration))
  (be alternates (mapcan #'field-names (field-alternates field))
    (aif (field-name field)
	 (cons it alternates)
	 alternates)))

(defmethod field-names ((field container-field-declaration))
  (append (call-next-method)
	  (mapcan #'field-names (field-subfields field))))

(defun compile-proxy-stub (pathname)
  "Compile a proxy stub.  Thi is at the moment just a wrapper for
compile-file."
  (compile-file pathname :verbose nil :print nil))

(defun make-cobol-proxy-stub (output-file records keys &key package record-name)
  "Compile a Cobol record declaration from records into a
Lisp record declaration written into OUTPUT-FILE.  If PACKAGE is
specified the Lisp code will be populated in that package.  If
RECORD-NAME is not specified, the output record name will be the
name of the first INPUT-FILE with \"-RECORD\" appended to it."
  (be file-name (pathname-name output-file)
    (unless record-name
      (setf record-name (s+ file-name "-record")))
    (with-open-file (out output-file :direction :output :if-exists :supersede)
      (format out ";;; -*- Common-Lisp -*-~%")
      (format out ";;; File automatically generated by COBSTOR on ~A.~2%"
	      (time-string (get-universal-time)))
      (when package
	(format out "(in-package :~A)~2%" package))
      (be *print-case* :downcase
	(pprint-define-cobol-record out record-name records :alternates t)
	(format out "~2&(declare-cobol-data-file ~A ~A '~A)~%"
		file-name record-name
		(mapcar #'(lambda (key)
			    (if (consp key)
				(car key)
				key))
			keys))
	(format out "~2&(export '~A)~%"
		(be fields (mapcar #'string-downcase
				   (mapcan #'field-names records))
		  (append (list file-name record-name)
			  fields
			  (mapcar #'(lambda (f)
				      (string-downcase (s+  (string f) "-storage")))
				  fields)
			  (mapcar #'(lambda (f)
				      (string-downcase (s+ (string *setter-prefix*) (string f))))
				  fields)))))
      (terpri out))))

