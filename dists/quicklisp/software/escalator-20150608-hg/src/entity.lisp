;;;; Escalator -- Entity System for Common Lisp
;;;;   High Performance Object System for Games
;;;;
;;;; Copyright (c) 2010, Elliott Slaughter <elliottslaughter@gmail.com>
;;;;
;;;; Permission is hereby granted, free of charge, to any person
;;;; obtaining a copy of this software and associated documentation
;;;; files (the "Software"), to deal in the Software without
;;;; restriction, including without limitation the rights to use, copy,
;;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;;; of the Software, and to permit persons to whom the Software is
;;;; furnished to do so, subject to the following conditions:
;;;;
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.
;;;;

(in-package :escalator)

(defun print-table (table)
  (format t "#<HASH-TABLE~%")
  (iter (for (k v) in-hashtable table) (format t "~s: ~s~%" k v))
  (format t ">~%"))

(defvar *component-meta* (make-hash-table))
(defvar *component-data* (make-hash-table))
(defvar *system-code* (make-hash-table))

(defun component-meta (component)
  (gethash component *component-meta*))
(defun (setf component-meta) (value component)
  (setf (gethash component *component-meta*) value))

(defun component-data (component)
  (gethash component *component-data*))
(defun (setf component-data) (value component)
  (setf (gethash component *component-data*) value))

(defun system-code (component)
  (gethash component *system-code*))
(defun (setf system-code) (value component)
  (setf (gethash component *system-code*) value))

(defun dependency-index (component dependency)
  (let ((meta (component-meta component)))
    (position dependency (first meta))))

(defun field-index (component field)
  (let ((meta (component-meta component)))
    (+ (length (first meta)) (position field (second meta)))))

(defun contains-circularity-p (graph)
  (let ((seen (make-hash-table))
        (path (make-hash-table)))
    (labels ((circle (c)
               (if (gethash c path)
                   t
                   (progn
                     (setf (gethash c seen) t (gethash c path) t)
                     (unwind-protect
                          (iter (for d in (gethash c graph))
                                (thereis (circle d)))
                       (setf (gethash c path) nil))))))
      (iter (for (c nil) in-hashtable graph)
            (unless (gethash c seen) (thereis (circle c)))))))

(defun total-order (graph)
  (let ((seen (make-hash-table))
        (ordered nil))
    (labels ((order (c)
               (unless (gethash c seen)
                 (setf (gethash c seen) t)
                 (push c ordered)
                 (iter (for d in (gethash c graph))
                       (order c)))))
      (iter (for (c nil) in-hashtable graph)
            (order c))
      (reverse ordered))))

(defmacro defcomponent (name (&rest dependencies) (&rest fields))
  "Defines a new component with the given dependencies and fields.

   Internal:
    * Error if name isn't a non-nil symbol.
    * Error if dependencies contains a non-symbol or nil.
    * Error if fields contains a non-symbol or nil.
    * Error if dependency doesn't exist.
    * Error if dependencies include self.
    * Error if dependencies cause cycle in component precendence list.
    * Warn on redefinition of component.
    * Warn if field hides a previously defined field for any component.
    * Adds component entry to *component-meta*
      (name -> deps * fields * initargs * size).
    * Defines macros for each field accessor.
    * Initializes global hash-table for component in *component-data*."

  ;; Error checks:
  (when (or (not name) (not (symbolp name)))
    (error "Component name ~a must be a non-nil symbol." name))
  (when (iter (for d in dependencies) (thereis (or (not d) (not (symbolp d)))))
    (error "Component ~a dependencies must be non-nil symbols." name))
  (when (iter (for f in fields) (thereis (or (not f) (not (symbolp f)))))
    (error "Component ~a fields must be non-nil symbols." name))
  (iter (for d in dependencies)
        (when (not (component-meta d))
          (error "Undefined dependency ~a." d)))
  (when (member name dependencies)
    (error "Component ~a lists itself as a dependency." name))
  (let ((graph (make-hash-table)))
    (iter (for (c m) in-hashtable *component-meta*)
          (setf (gethash c graph) (first m)))
    (setf (gethash name graph) dependencies)
    (when (contains-circularity-p graph)
      (error "Component ~a would cause a circularity in dependencies." name)))
  (when (component-meta name)
    (warn "Duplicate definition of component ~a." name))
  (iter (for (c m) in-hashtable *component-meta*)
        (iter (for f in (intersection fields (second m)))
              (warn "Field ~a was already defined in component ~a." f c)))


  `(eval-when (:compile-toplevel :load-toplevel :execute)
     ;; Global state:
     (setf (component-meta ',name)
           '(,dependencies
             ,fields
             ,(iter (for f in fields)
                    (collect (intern (symbol-name f) :keyword)))
             ,(+ (length dependencies) (length fields)))
           (component-data ',name) (make-hash-table))

     ;; Code gen:
     ,@(iter (for f in fields)
             (collect
                 (let ((g (gensym)))
                   `(defmacro ,f (,g)
                      `(aref ,,g ,',(+ (length dependencies)
                                       (position f fields)))))))
     nil))

(defmacro defsystem (name (entity component &rest dependencies) &body body)
  "Defines a system for the given component.

   Internal:
    * Error if name, entity, and component aren't non-nil symbols.
    * Error if dependencies aren't symbols (nil allowed).
    * Error if name doesn't refer to a defined component.
    * Error if number of dependencies doesn't match component.
    * Warns on redefinition of system.
    * Stores code to operate on component data."
  (when (or (not name) (not (symbolp name)))
    (error "System name ~a must be a non-nil symbol." name))
  (when (or (not entity) (not (symbolp entity)))
    (error "System entity param ~a must be a non-nil symbol." entity))
  (when (or (not component) (not (symbolp component)))
    (error "System component param ~a must be a non-nil symbol." component))
  (when (iter (for d in dependencies) (thereis (not (symbolp d))))
    (error "System ~a dependencies must be symbols." name))
  (unless (component-meta name)
    (error "Undefined component ~a." name))
  (unless (= (length dependencies) (length (first (component-meta name))))
    (error "Dependencies for system ~a don't match component ~a." name name))
  (when (system-code name)
    (warn "Redefinition of system ~a." name))

  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (setf (system-code ',name)
           '(iter (for (,entity ,component) in-hashtable (component-data ',name))
                  (let (,@(iter (for d in dependencies)
                                (for i from 0)
                                (when d
                                  (collect `(,d (aref ,component ,i))))))
                    ,@body)))
     nil))

(defmacro system-loop ()
  (let ((graph (make-hash-table)))
    (iter (for (c m) in-hashtable *component-meta*)
          (setf (gethash c graph) (first m)))
    (let ((order (total-order graph)))
      `(progn
         ,@(iter (for c in order)
                 (let ((s (system-code c)))
                   (collect s)))))))

(defun entity-component (entity component)
  "Returns the component instance for entity or nil."
  (gethash entity (component-data component)))

(defun (setf entity-component) (value entity component)
  (setf (gethash entity (component-data component)) value))

(let ((next 0))
  (defun next-uuid ()
    (incf next)))

(defun make-entity (prototype components &rest initargs)
  "Instantiates an entity from the given prototype (shallow copy) and
   additionally associated with the specified components.

   Internal:
    * Error if any component doesn't exist.
    * Enforce uniqueness of components by pushing into a new list.
    * Shallow copy prototype components.
    * Instantiate new components (overwrite if they already exist).
    * Copy references to dependencies (error if it doesn't exist)."

  ;; Error checks:
  (iter (for c in components)
        (when (not (component-meta c)) (error "Undefined component ~a." c)))

  (let ((uuid (next-uuid)))

    ;; Copy prototype:
    (iter (for (c ds) in-hashtable *component-data*)
          (let ((d (entity-component prototype c)))
            ;; TODO: is copy-seq standard?
            (when d (setf (entity-component uuid c) (copy-seq d)))))

    ;; Copy initargs:
    (let ((unspecified (gensym)))
      (iter (for c in components)
            (let ((m (component-meta c))
                  (d (entity-component uuid c)))
              (unless d
                (setf d (make-array (fourth m))
                      (entity-component uuid c) d))
              (iter (for f in (second m))
                    (for i in (third m))
                    (let ((v (getf initargs i unspecified)))
                      (unless (eql v unspecified)
                        (setf (aref d (field-index c f)) v)))))))

    ;; Copy dependency references:
    (iter (for c in components)
          (let ((cr (entity-component uuid c)))
            (iter (for d in (first (component-meta c)))
                  (let ((dr (entity-component uuid d)))
                    (if dr
                        (setf (aref cr (dependency-index c d)) dr)
                        (error "Component ~a requires dependency ~a." c d))))))

    uuid))
