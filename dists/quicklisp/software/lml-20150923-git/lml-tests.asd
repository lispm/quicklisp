;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          lml-tests.asd
;;;; Purpose:       ASDF system definitionf for lml testing package
;;;; Author:        Kevin M. Rosenberg
;;;; Date Started:  Apr 2003
;;;;
;;;; $Id$
;;;; *************************************************************************

(defpackage #:lml-tests-system
  (:use #:asdf #:cl))
(in-package #:lml-tests-system)

(defsystem lml-tests
    :depends-on (:rt :lml)
    :components
    ((:file "tests")))

(defmethod perform ((o test-op) (c (eql (find-system 'lml-tests))))
  (or (funcall (intern (symbol-name '#:do-tests)
		       (find-package '#:regression-test)))
      (error "test-op failed")))

