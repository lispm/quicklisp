(in-package :mini-cas)

;; only handle 2D matrices for now

(defmethod full-derivative ((form array) var)
  (let ((result (make-array (array-dimensions form))))
    (dotimes (i (array-dimension form 0))
      (dotimes (j (array-dimension form 1))
        (setf (aref result i j)
              (full-derivative (aref form i j) var))))
    result))

(defmethod simplify ((form array))
  (let ((result (make-array (array-dimensions form))))
    (dotimes (i (array-dimension form 0))
      (dotimes (j (array-dimension form 1))
        (setf (aref result i j) (simplify (aref form i j)))))
    result))

(defmethod +reduce ((a array) (b array))
  (let* ((d0 (array-dimension a 0))
         (d1 (array-dimension a 1))
         (result (make-array (list d0 d1))))
    (assert (and (= d0 (array-dimension b 0))
                 (= d1 (array-dimension b 1))))
    (dotimes (i d0)
      (dotimes (j d1)
        (setf (aref result i j) (list '+
                                      (aref a i j)
                                      (aref b i j)))))
    result))

(defmethod *reduce ((a array) (b array))
  (let* ((d0 (array-dimension a 0))
         (d1 (array-dimension a 1))
         (d2 (array-dimension b 1))
         (result (make-array (list d0 d2))))
    (assert (= d1 (array-dimension b 0)))
    (dotimes (i d0)
      (dotimes (j d2)
        (setf (aref result i j)
              (cons '+ (loop for k from 0 below d1
                            collecting (list '* 
                                             (aref a i k)
                                             (aref b k j)))))))
    ;; reduce 1x1 arrays to scalar values
    (if (= 1 d0 d2)
        (aref result 0 0)
        result)))

;; questionable as a reduction, but useful for a case at hand...
(defmethod *reduce (a (b array))
  (when a
    (let* ((d0 (array-dimension b 0))
           (d1 (array-dimension b 1))
           (result (make-array (list d0 d1))))
      (dotimes (i d0)
        (dotimes (j d1)
          (setf (aref result i j)
                (list '* a (aref b i j)))))
      result)))

(defun transpose (array)
  (let* ((d0 (array-dimension array 0))
         (d1 (array-dimension array 1))
         (result (make-array (list d1 d0))))
    (dotimes (i d0)
      (dotimes (j d1)
        (setf (aref result j i) (aref array i j))))
    result))
