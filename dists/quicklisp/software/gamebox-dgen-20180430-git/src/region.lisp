(in-package :box.dgen)

(defvar *region* 0)

(defstruct (region (:conc-name nil)
                   (:constructor %make-region (id)))
  id cells)

(defun make-region (stage)
  (let ((id (incf *region*)))
    (setf (gethash id (regions stage)) (%make-region id))
    id))

(defun get-region (stage id)
  (gethash id (regions stage)))

(defun regions-distinct-p (n1 n2)
  (let ((items (mapcar #'region-id (list n1 n2))))
    (and (not (some #'null items))
         (not (apply #'= items)))))
