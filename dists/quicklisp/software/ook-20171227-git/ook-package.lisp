;;;; -*- Mode: Lisp -*-

;;;; ook-package.lisp --

(defpackage "OOK" (:use "CL")
  (:export
   "OOK"
   "OOK."
   "OOK!"
   "OOK?"
   "MONKEY?!?"
   "GENTLY-ASK-THE-LIBRARIAN"
   "UU"
   "KOO"))

;;;; end of file -- ook-package.lisp --
