(defpackage :mini-cas
  (:use :cl)
  (:export
   #:cas+
   #:cas-
   #:cas*
   #:cas/
   #:cas-sin
   #:cas-cos

   #:full-derivative))
