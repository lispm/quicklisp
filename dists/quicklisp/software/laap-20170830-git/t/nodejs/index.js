const net = require('net');
const cluster = require('cluster');

if (cluster.isMaster) {
    for (let i = 0; i < 8; i++) {
	cluster.fork();
    }
}
else {
    net.createServer((socket) => {
	socket.end('HTTP/1.0 200 OK\r\nContent-Length: 5\r\n\r\nPong\n\r\n');
    }).on('error', (err) => {
    }).listen(9876, '127.0.0.1');
}
