(in-package :flac-parser)

;;; Parse tree

(defmethod parse (file (node (eql :parse-tree)) &key)
  (let ((node (make-instance 'parse-tree)))
    (with-slots (marker stream-info blocks frames) node
      (setf marker (parse file :parse-tree/marker)
            stream-info (parse file :parse-tree/stream-info)
            blocks (parse file :parse-tree/blocks :stream-info stream-info)
            frames (parse file :parse-tree/frames)))
    node))

(defmethod parse (file (node (eql :parse-tree/marker)) &key)
  (let ((marker (parse file :string :bytes 4)))
    (if (string= marker "fLaC")
        marker
        (error 'invalid-file :file file))))

(defmethod parse (file (node (eql :parse-tree/stream-info)) &key)
  (parse file :metadata-block))

(defmethod parse (file (node (eql :parse-tree/blocks)) &key stream-info)
  (loop :with stream-info-last-block-p = (last-block-p stream-info)
        :until (or stream-info-last-block-p current-last-block-p)
        :for block = (parse file :metadata-block)
        :for current-last-block-p = (last-block-p block)
        :collect block))

(defmethod parse (file (node (eql :parse-tree/frames)) &key)
  nil)

;;; Metadata blocks

(defmethod parse (file (node (eql :metadata-block)) &key)
  (let ((node (make-instance 'metadata-block)))
    (with-slots (header data) node
      (setf header (parse file :metadata-block-header)
            data (parse file (block-type-name header) :header header)))
    node))

(defmethod parse (file (node (eql :metadata-block-header)) &key)
  (let ((node (make-instance 'metadata-block-header))
        (header-data (parse file :octets)))
    (with-slots (last-flag type length) node
      (setf last-flag (parse file :metadata-block-header/last-flag :octets header-data)
            type (parse file :metadata-block-header/type :octets header-data)
            length (parse file :metadata-block-header/length)))
    node))

(defmethod parse (file (node (eql :metadata-block-header/last-flag)) &key octets)
  (parse file :bits :octets octets))

(defmethod parse (file (node (eql :metadata-block-header/type)) &key octets)
  (parse file :bits :count 7 :offset 1 :octets octets))

(defmethod parse (file (node (eql :metadata-block-header/length)) &key)
  (parse file :integer :bytes 3))

(defmethod parse (file (node (eql :metadata-block-unknown)) &key header)
  (seek file (block-length header))
  (warn 'metadata-block-type-unknown :file file :block-type (block-type header)))

(defmethod parse (file (node (eql :metadata-block-invalid)) &key header)
  (seek file (block-length header))
  (error 'metadata-block-type-invalid :file file :block-type (block-type header)))

;;; Stream Information Metadata Block

(defmethod parse (file (node (eql :stream-info)) &key)
  (let ((node (make-instance 'metadata-block-stream-info))
        (temp))
    (with-slots (minimum-block-size maximum-block-size minimum-frame-size maximum-frame-size
                 sample-rate channel-count bits-per-sample sample-count md5) node
      (setf minimum-block-size (parse file :stream-info/minimum-block-size)
            maximum-block-size (parse file :stream-info/maximum-block-size)
            minimum-frame-size (parse file :stream-info/minimum-frame-size)
            maximum-frame-size (parse file :stream-info/maximum-frame-size)
            temp (parse file :octets :count 8)
            sample-rate (parse file :stream-info/sample-rate :octets temp)
            channel-count (parse file :stream-info/channel-count :octets temp)
            bits-per-sample (parse file :stream-info/bits-per-sample :octets temp)
            sample-count (parse file :stream-info/sample-count :octets temp)
            md5 (parse file :stream-info/md5)))
    node))

(defmethod parse (file (node (eql :stream-info/minimum-block-size)) &key)
  (parse file :integer :bytes 2))

(defmethod parse (file (node (eql :stream-info/maximum-block-size)) &key)
  (parse file :integer :bytes 2))

(defmethod parse (file (node (eql :stream-info/minimum-frame-size)) &key)
  (parse file :integer :bytes 3))

(defmethod parse (file (node (eql :stream-info/maximum-frame-size)) &key)
  (parse file :integer :bytes 3))

(defmethod parse (file (node (eql :stream-info/sample-rate)) &key octets)
  (parse file :bits :count 20 :octets octets))

(defmethod parse (file (node (eql :stream-info/channel-count)) &key octets)
  (parse file :bits :count 3 :offset 20 :octets octets))

(defmethod parse (file (node (eql :stream-info/bits-per-sample)) &key octets)
  (parse file :bits :count 5 :offset 23 :octets octets))

(defmethod parse (file (node (eql :stream-info/sample-count)) &key octets)
  (parse file :bits :count 36 :offset 28 :octets octets))

(defmethod parse (file (node (eql :stream-info/md5)) &key)
  (parse file :octets :count 16))

;;; Padding Metadata Block

(defmethod parse (file (node (eql :padding)) &key header)
  (let ((node (make-instance 'metadata-block-padding)))
    (with-slots (data) node
      (setf data (parse file :padding/data :count (block-length header))))
    node))

(defmethod parse (file (node (eql :padding/data)) &key count)
  (parse file :octets :count count))

;;; Application Metadata Block

(defmethod parse (file (node (eql :application)) &key header)
  (let ((node (make-instance 'metadata-block-application)))
    (with-slots (id) node
      (setf id (parse file :application/id))
      (seek file (- (block-length header) 4)))
    node))

(defmethod parse (file (node (eql :application/id)) &key)
  (parse file :string :bytes 4))

(defmethod parse (file (node (eql :application/data)) &key header)
  (parse file :octets :count (- (block-length header) 4)))

;;; Seek Table Metadata Block

(defmethod parse (file (node (eql :seek-table)) &key header)
  (let ((node (make-instance 'metadata-block-seek-table)))
    (with-slots (seek-points) node
      (setf seek-points (parse file :seek-table/seek-points :count (/ (block-length header) 18))))
    node))

(defmethod parse (file (node (eql :seek-table/seek-points)) &key count)
  (loop :repeat count
        :collect (parse file :seek-point)))

(defmethod parse (file (node (eql :seek-point)) &key)
  (make-instance 'metadata-block-seek-table/seek-point
                 :sample (parse file :seek-point/sample)
                 :offset (parse file :seek-point/offset)
                 :sample-count (parse file :seek-point/sample-count)))

(defmethod parse (file (node (eql :seek-point/sample)) &key)
  (parse file :integer :bytes 8))

(defmethod parse (file (node (eql :seek-point/offset)) &key)
  (parse file :integer :bytes 8))

(defmethod parse (file (node (eql :seek-point/sample-count)) &key)
  (parse file :integer :bytes 2))

;;; Vorbis Comment Metadata Block

(defmethod parse (file (node (eql :vorbis-comment)) &key)
  (let ((node (make-instance 'metadata-block-vorbis-comment)))
    (with-slots (vendor-length vendor user-comments-count user-comments) node
      (setf vendor-length (parse file :vorbis-comment/vendor-length)
            vendor (parse file :vorbis-comment/vendor :length vendor-length)
            user-comments-count (parse file :vorbis-comment/user-comments-count)
            user-comments (parse file :vorbis-comment/user-comments :count user-comments-count)))
    node))

(defmethod parse (file (node (eql :vorbis-comment/vendor-length)) &key)
  (parse file :integer :bytes 4 :endian :little))

(defmethod parse (file (node (eql :vorbis-comment/vendor)) &key length)
  (parse file :string :bytes length :encoding :utf-8))

(defmethod parse (file (node (eql :vorbis-comment/user-comments-count)) &key)
  (parse file :integer :bytes 4 :endian :little))

(defmethod parse (file (node (eql :vorbis-comment/user-comments)) &key count)
  (loop :with table = (make-hash-table :test #'equal)
        :repeat count
        :for length = (parse file :vorbis-user-comment/length)
        :for comment = (parse file :vorbis-user-comment/data :length length)
        :for split = (position #\= comment)
        :for key = (subseq comment 0 split)
        :for value = (subseq comment (1+ split))
        :do (push value (gethash key table))
        :finally (return table)))

(defmethod parse (file (node (eql :vorbis-user-comment/length)) &key)
  (parse file :integer :bytes 4 :endian :little))

(defmethod parse (file (node (eql :vorbis-user-comment/data)) &key length)
  (parse file :string :bytes length :encoding :utf-8))

;;; Cue Sheet Metadata Block

(defmethod parse (file (node (eql :cue-sheet)) &key)
  (let ((node (make-instance 'metadata-block-cue-sheet)))
    (with-slots (media-catalog-id lead-in-sample-count cd-flag track-count tracks) node
      (setf media-catalog-id (parse file :cue-sheet/media-catalog-id)
            lead-in-sample-count (parse file :cue-sheet/lead-in-sample-count)
            cd-flag (parse file :cue-sheet/cd-flag)
            track-count (parse file :cue-sheet/track-count)
            tracks (parse file :cue-sheet/tracks :count track-count)))
    node))

(defmethod parse (file (node (eql :cue-sheet/media-catalog-id)) &key)
  (parse file :string :bytes 128))

(defmethod parse (file (node (eql :cue-sheet/lead-in-sample-count)) &key)
  (parse file :integer :bytes 8))

(defmethod parse (file (node (eql :cue-sheet/cd-flag)) &key)
  (let ((octets (parse file :octets :count 259)))
    (parse file :bits :octets octets)))

(defmethod parse (file (node (eql :cue-sheet/track-count)) &key)
  (parse file :integer))

(defmethod parse (file (node (eql :cue-sheet/tracks)) &key count)
  (loop :repeat count
        :collect (parse file :cue-sheet-track)))

(defmethod parse (file (node (eql :cue-sheet-track)) &key)
  (let ((node (make-instance 'metadata-block-cue-sheet/track))
        (temp))
    (with-slots (offset number isrc type pre-emphasis-flag index-count indices) node
      (setf offset (parse file :cue-sheet-track/offset)
            number (parse file :cue-sheet-track/number)
            isrc (parse file :cue-sheet-track/isrc)
            temp (parse file :octets :count 14)
            type (parse file :cue-sheet-track/type :octets temp)
            pre-emphasis-flag (parse file :cue-sheet-track/pre-emphasis-flag :octets temp)
            index-count (parse file :cue-sheet-track/index-count)
            indices (parse file :cue-sheet-track/indices :count index-count)))
    node))

(defmethod parse (file (node (eql :cue-sheet-track/offset)) &key)
  (parse file :integer :bytes 8))

(defmethod parse (file (node (eql :cue-sheet-track/number)) &key)
  (parse file :integer))

(defmethod parse (file (node (eql :cue-sheet-track/isrc)) &key)
  (parse file :string :bytes 12))

(defmethod parse (file (node (eql :cue-sheet-track/type)) &key octets)
  (parse file :bits :octets octets))

(defmethod parse (file (node (eql :cue-sheet-track/pre-emphasis-flag)) &key octets)
  (parse file :bits :offset 1 :octets octets))

(defmethod parse (file (node (eql :cue-sheet-track/index-count)) &key)
  (parse file :integer))

(defmethod parse (file (node (eql :cue-sheet-track/indices)) &key count)
  (loop :repeat count
        :collect (parse file :cue-sheet-track-index)))

(defmethod parse (file (node (eql :cue-sheet-track-index)) &key)
  (prog1 (make-instance 'metadata-block-cue-sheet/track-index
                        :offset (parse file :cue-sheet-track-index/offset)
                        :number (parse file :cue-sheet-track-index/number))
    (parse file :cue-sheet-track-index/reserved)))

(defmethod parse (file (node (eql :cue-sheet-track-index/offset)) &key)
  (parse file :integer :bytes 8))

(defmethod parse (file (node (eql :cue-sheet-track-index/number)) &key)
  (parse file :integer))

(defmethod parse (file (node (eql :cue-sheet-track-index/reserved)) &key)
  (parse file :octets :count 3))

;;; Picture Metadata Block

(defmethod parse (file (node (eql :picture)) &key)
  (let ((node (make-instance 'metadata-block-picture)))
    (with-slots (type mime-type-length mime-type description-length description width height
                 bits-per-pixel indexed-color-count size data) node
      (setf type (parse file :picture/type)
            mime-type-length (parse file :picture/mime-type-length)
            mime-type (parse file :picture/mime-type :count mime-type-length)
            description-length (parse file :picture/description-length)
            description (parse file :picture/description :count description-length)
            width (parse file :picture/width)
            height (parse file :picture/height)
            bits-per-pixel (parse file :picture/bits-per-pixel)
            indexed-color-count (parse file :picture/indexed-color-count)
            size (parse file :picture/size)
            data (parse file :picture/data :count size)))
    node))

(defmethod parse (file (node (eql :picture/type)) &key)
  (parse file :integer :bytes 4))

(defmethod parse (file (node (eql :picture/mime-type-length)) &key)
  (parse file :integer :bytes 4))

(defmethod parse (file (node (eql :picture/mime-type)) &key count)
  (parse file :string :bytes count))

(defmethod parse (file (node (eql :picture/description-length)) &key)
  (parse file :integer :bytes 4))

(defmethod parse (file (node (eql :picture/description)) &key count)
  (parse file :string :bytes count :encoding :utf-8))

(defmethod parse (file (node (eql :picture/width)) &key)
  (parse file :integer :bytes 4))

(defmethod parse (file (node (eql :picture/height)) &key)
  (parse file :integer :bytes 4))

(defmethod parse (file (node (eql :picture/bits-per-pixel)) &key)
  (parse file :integer :bytes 4))

(defmethod parse (file (node (eql :picture/indexed-color-count)) &key)
  (parse file :integer :bytes 4))

(defmethod parse (file (node (eql :picture/size)) &key)
  (parse file :integer :bytes 4))

(defmethod parse (file (node (eql :picture/data)) &key count)
  (parse file :octets :count count))
