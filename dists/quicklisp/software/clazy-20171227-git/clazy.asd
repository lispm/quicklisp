;;;; -*- Mode: Lisp -*-

;;;; clazy.asd --
;;;; Lazy evaluation for Common Lisp.
;;;;
;;;; See the file COPYING for copyright and licensing information.

(asdf:defsystem :clazy
  :components ((:file "clazy-package")
               (:file "lambda-list-parsing"
                :depends-on ("clazy-package"))
               (:file "clazy"
                :depends-on ("clazy-package" "lambda-list-parsing"))
               (:module "impl-dependent"
			:components ((:file
				      #+lispworks
				      "lispworks"
                             
				      #+allegro
				      "acl"

				      #+cmucl
				      "cmucl"

				      #+sbcl
				      "sbcl"

				      #+clisp
				      "clisp"

                                      #+ecl
                                      "ecl"

                                      #+gcl
                                      "gcl"
                             
                                      #+abcl
                                      "abcl"

                                      #+(and clozure-common-lisp ccl)
                                      "clozure-cl"

                                      #+(and ccl (not clozure-common-lisp))
                                      "ccl"
				      ))
			:depends-on ("clazy-package"))
               (:file "library" :depends-on ("clazy"))
               (:file "seq-funs-package" :depends-on ("clazy" "library"))
               (:file "seq-funs" :depends-on ("seq-funs-package"))
               )

  :author "Marco Antoniotti"
  :description "The CLAZY System."
  :long-description "The CLAZY System.

The CLAZY system (as in 'I must be CLAZY!') contains a set of
definitions useful for introducing lazy evaluation via lazy calls in
Common Lisp; i.e., CLAZY turns parts of Common Lisp into a non-strict
functional language."
)

;;;; end of file -- clazy.asd --
