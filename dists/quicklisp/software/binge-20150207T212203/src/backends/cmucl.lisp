;;;  cmucl.lisp --- output module for CMU-CL FFI

;;;  Copyright (C) 2004 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-A5050645A7CC19EB918BB0B73CA45ECE.lisp,v 1.15 2004/04/09 10:48:26 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:
;;;
;;;  This backend is based on the SBCL backend.  Here we redefine only
;;;  what changes.

#+cmu (ext:file-comment "$Id: F-A5050645A7CC19EB918BB0B73CA45ECE.lisp,v 1.15 2004/04/09 10:48:26 wcp Exp $")

(in-package :binge-backend)

(defclass cmucl-backend (sbcl-backend) ())

(defmethod use-packages ((backend cmucl-backend))
  '("COMMON-LISP" "ALIEN" "C-CALL"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun cmucl-format-compound (type declaration backend)
  (format nil "(def-alien-type nil~%  (~A ~I~A~@[ ~<~@{~A~^ ~_~}~:>~]))"
	  type
	  (c-object-lisp-name declaration backend)
	  (mapcar #'(lambda (slot)
		      (c-object->ffi-notation slot backend))
		  (c-slots declaration))))

(defmethod c-object->ffi-notation ((decl c-typedef-declaration) (backend cmucl-backend))
  (format nil "(def-alien-type ~A ~A)"
	  (c-object-lisp-name decl backend)
	  (c-object->ffi-notation (c-type decl) backend)))

(defmethod c-object->ffi-notation ((decl c-struct-declaration) (backend cmucl-backend))
  (cmucl-format-compound "struct" decl backend))

(defmethod c-object->ffi-notation ((decl c-union-declaration) (backend cmucl-backend))
  (cmucl-format-compound "union" decl backend))

(defmethod c-object->ffi-notation ((decl c-enum-declaration) (backend cmucl-backend))
  (format nil "(def-alien-type nil~%  (enum ~A~{ ~A~}))"
	  (c-object-lisp-name decl backend)
	  (mapcar #'(lambda (c)
		      (if (consp c)
			  (format nil "(:~@[~A~]~A ~A)" (backend-prefix backend) (car c) (cadr c))
			  (format nil ":~@[~A~]~A" (backend-prefix backend) c)))
		  (c-constants decl))))

(defmethod c-object->ffi-notation ((decl c-function-declaration) (backend cmucl-backend))
  (format nil "(def-alien-routine (~S ~A) ~A~@[~&  ~<~@{~A~^ ~:_~}~:>~])"
	  (c-name decl)
	  (c-object-lisp-name decl backend)
	  (c-object->ffi-notation (c-type decl) backend)
	  (loop
	     for arg in (c-arguments decl)
	     for i from 1 by 1
	     collect (if (c-name arg)
			 (format nil "(~A ~A)" (c-name arg)
				 (c-object->ffi-notation (c-type arg) backend))
			 ;; the use of a dash in the name of arguments
			 ;; guarantees we won't have any name clash
			 (format nil "(arg-~A ~A)" i
				 (c-object->ffi-notation (c-type arg) backend))))))

(defmethod c-object->ffi-notation ((decl c-variable-declaration) (backend cmucl-backend))
  (format nil "(def-alien-variable (~S ~A) ~A)"
	  (c-name decl)
	  (c-object-lisp-name decl backend)
	  (c-object->ffi-notation (c-type decl)  backend)))

(setf binge-compiler:*backend* (make-instance 'cmucl-backend))
(export 'cmucl-backend)		     ; so that we can use this backend
