CLAZY
=====

This is the CLAZY package, a library that introduces lazy calling in
Common Lisp.

In order to load in your environment you can use either the .asd or
the .system file that come with the distribution.

Documentation in HTML is in the 'docs/html' subfolder.

The library is relased under the terms contained in the COPYING file.


A NOTE ON FORKING
-----------------

Of course you are free to fork the project subject to the current
licensing scheme.  However, before you do so, I ask you to consider
plain old "cooperation" by asking me to become a developer.
It helps keeping the entropy level at an acceptable level.



Enjoy

Marco Antoniotti, Milan, Italy, (c) 2008-2018
