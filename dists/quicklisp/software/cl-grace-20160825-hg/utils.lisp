;;; -*- Mode: Lisp; Package: CL-Grace -*-
;;;
;;; Copyright (c) 2013 Paul Onions
;;; See LICENCE file for licence information.
;;;
;;; Utilities for the CL-GRACE system
;;;
(in-package :cl-grace)

(defun remove-kw-args (kws arglist)
  "Remove keywords and their values from ARGLIST."
  (loop :for args := arglist :then (rest (rest args))
        :while args
        :unless (member (first args) kws)
          :append (list (first args) (second args))))

(defun hsv->rgb (hsv)
  "Convert an HSV triple to an RGB triple.
Where HSV should be a 3-element list of reals in range [0,1].  Returns
a 3-element list of RGB values in range [0,1]."
  (destructuring-bind (h s v) hsv
    (let* ((c (* s v))
           (k (* 6 h))
           (x (* c (- 1 (abs (- (mod k 2) 1))))))
      (mapcar (lambda (z) (+ z (- v c)))
              (cond ((and (<= 0 k) (< k 1))
                     (list c x 0))
                    ((and (<= 1 k) (< k 2))
                     (list x c 0))
                    ((and (<= 2 k) (< k 3))
                     (list 0 c x))
                    ((and (<= 3 k) (< k 4))
                     (list 0 x c))
                    ((and (<= 4 k) (< k 5))
                     (list x 0 c))
                    ((and (<= 5 k) (< k 6))
                     (list c 0 x))
                    (t
                     (list 0 0 0)))))))
