;;;; -*- Mode: Lisp -*-

;;;; xhtmlambda.asd --
;;;;
;;;; See file COPYING for copyright and license information.

(eval-when (:load-toplevel :execute)
  (pushnew :xmlambda-parser *features*))


(asdf:defsystem "xhtmlambda"
  :description "THe XHTMLambda System.

Another HTML/XHTML/XML generation (and parsing) system in Common Lisp."

  :author "Marco Antoniotti"

  :license "BSD"

  :components ((:file "xhtmlambda-pkg")
               (:file "xhtmlambda"
                :depends-on ("xhtmlambda-pkg"))
               (:module "utilities"
                :depends-on ("xhtmlambda-pkg")
                :components ((:file "streams-utilities")))
               (:file "xml-parser"
                :depends-on ("utilities"))
	       (:module "impl-dependent"
		:depends-on ("xhtmlambda-pkg")
		:components (
			     #+lispworks 
                             (:file "lispworks")
                             ))
               )
  :depends-on (#+xmlambda-parser "cl-unicode")
  )

;;;; end of file -- xhtmlambda.asd --
