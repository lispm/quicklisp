
(asdf:defsystem #:pfft
    :author "Patrick Stein <pat@nklein.com>"
    :maintainer "Patrick Stein <pat@nklein.com>"
    :description "N-Dimensional Fourier Transforms with some parallel processing."
    :license "Public Domain"
    :version "0.1.20110324"
    :depends-on (:fft :pcall)
    :components ((:file "pfft")))
