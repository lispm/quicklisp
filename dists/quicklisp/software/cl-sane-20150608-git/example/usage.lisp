;;;;;;;;;;;;
;; This is a sort of elephants' graveyard of usage examples/snippets. Will be
;; removed/replaced when the library is fully bound and I can write some proper
;; documentation!
;;;;;;;;;;;;;

(defpackage :sane-user
  (:use :sane :cl :trivial-gray-streams :iterate))

(in-package :sane-user)

;; With-init runs forms with the library initialised. It can bind a
;; variable to the version - in this case, V.
(with-init v
  (format t "~A~%" v)
  42)

;; devices lists the devices sane can find.
(with-init nil
  (mapcar #'name (devices t)))

;; List all the options of the device
(with-init nil
  (with-device d "test:0"
    (dump-option-list d)))

;; Read from the device.
(with-init nil
  (with-device d "test:0"
    (setf (option-value (device-option "mode" d)) "Color")
    (setf (option-value (device-option "test-picture" d)) "Color pattern")

    (with-open-file (f "out.raw"
                       :direction :output
                       :element-type '(unsigned-byte 8)
                       :if-exists :supersede :if-does-not-exist :create)
      (with-open-scanner-stream s d
        (let (byte)
          (iterate
            (setf byte (stream-read-byte s))
            (when (eq byte :eof) (terminate))
            (write-byte byte f)))))))
