;;;  smtp4cl.asd --- A client SMTP library for CL

;;;  Copyright (C) 2005, 2006 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.org>
;;;  Project: smtp4cl

#+cmu (ext:file-comment "$Module: smtp4cl.asd, Time-stamp: <2006-10-05 15:52:54 wcp> $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

(defpackage :smtp4cl-system
  (:use :common-lisp :asdf))

(in-package :smtp4cl-system)

(defsystem smtp4cl
    :name "SMTP4CL"
    :author "Walter C. Pelissero <walter@pelissero.de>"
    :maintainer "Walter C. Pelissero <walter@pelissero.de>"
    :licence "Lesser General Public License"
    :description "SMTP4CL an SMTP library for Common Lisp"
    :long-description
    "SMTP4CL is a library for Common Lisp to send e-mail using
the SMTP protocol."
    :depends-on (:mime4cl :net4cl)
    :components
    ((:file "smtp")))
