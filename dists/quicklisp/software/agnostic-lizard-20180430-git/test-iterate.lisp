(require :iterate)
(use-package :iterate)

(iterate
  (for x :from 1 :to 3)
  (macrolet
    ((f (y) `(collect ,y)))
    (f x)))
