;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          lml.asd
;;;; Purpose:       ASDF definition file for Lisp Markup Language
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  Aug 2002
;;;;
;;;; $Id$
;;;;
;;;; This file, part of LML, is Copyright (c) 2002 by Kevin M. Rosenberg
;;;;
;;;; LML users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License v2
;;;; (http://www.gnu.org/licenses/gpl.html)
;;;; *************************************************************************

(in-package #:cl-user)
(defpackage #:lml-system (:use #:asdf #:cl))
(in-package #:lml-system)

(defsystem lml
  :name "lml"
  :author "Kevin M. Rosenberg <kevin@rosenberg.net>"
  :version "2.4"
  :maintainer "Kevin M. Rosenberg <kmr@debian.org>"
  :licence "GNU General Public License"
  :description "Lisp Markup Language"
  :long-description "LML provides creation of XHTML for Lisp programs."
  
  :components
  ((:file "package")
   (:file "utils" :depends-on ("package"))
   (:file "files" :depends-on ("utils"))
   (:file "base" :depends-on ("files"))
   (:file "read-macro" :depends-on ("base"))
   (:file "stdsite" :depends-on ("base"))
   (:file "downloads" :depends-on ("base"))
   ))

(defmethod perform ((o test-op) (c (eql (find-system 'lml))))
  (operate 'load-op 'lml-tests)
  (operate 'test-op 'lml-tests))
