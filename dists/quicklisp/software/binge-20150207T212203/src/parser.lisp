;;;  parser.lisp --- C declaration extractor main program

;;;  Copyright (C) 2004-2006, 2008, 2009, 2010 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-F178F4F9CA53172B5D9DF63342C39549.lisp,v 1.21 2004/12/16 12:04:29 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-F178F4F9CA53172B5D9DF63342C39549.lisp,v 1.21 2004/12/16 12:04:29 wcp Exp $")

(in-package :binge-parser)

(defvar *c-preprocessor* "cpp"
  "Pathname or just the name of the C preprocessor program.")

(defvar *c-compiler* "cc"
  "Pathname or just the name of the C compiler program.")

(defvar *extract-constants-with-cc* t
  "Whether or not use the C compiler to extract symbolic
constants.  If this is NIL then the internal parser is used
instead.")

(defvar *gcc-null-defines* '(("__attribute__(x)" "")
			     ("__extension__(x)" "0")
			     ("__extension__" "")
			     ("__inline" "")
			     ("inline" "")
			     ("__asm(x)" "")
			     ("__restrict" "")
			     ("__restrict__" "")
			     ("__const" ""))
  "List of preprocessor defines necessary to remove some special
GCC features that would interfere with the parser.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun write-c-source-for-parser (out preamble defines includes constants)
  "Write a C source file on OUT stream suitable for preprocessing
and, later on, parsing by BINGE.  This means inserting the
necessary inclusion statements and the DEFINES to clear out all
the fancy cc extensions that can be found.  CONSTANTS are written
in a special syntax only BINGE can parse."
  (format out "~A~2%~:{#define ~A ~A~%~}~@
	       ~{#include <~A>~%~}~%"
	  preamble defines includes)
  (format out "~{#ifdef ~A~:*~%const \"~A\" = ~:*~A;~%#endif~%~}~%"
	  (mapcar #'(lambda (c)
		      ;; remove the type specifier if present
		      (if (consp c) (car c) c))
		  constants)))

(defun write-c-source-for-compilation (out preamble defines includes constants)
  "Write a C source file on OUT stream suitable for compilation
and execution.  This means inserting the necessary inclusion
statements and the DEFINES to clear out all the fancy cc
extensions that can be found.  A main function is written
including printf()s of all the CONSTANTS values."
  (write-c-source-for-parser out preamble defines includes nil)
  (format out "#include <stdio.h>~%~@
	       int main () {~%~@
		   ~:{#ifdef ~A~:*~@
			~2Tprintf(\"(\\\"~A\\\" ~A)\\n\", ~2:*~A);~@
		      #endif~2%~}~
		        ~2Treturn 0;~%}~%"
	  (mapcar #'(lambda (c)
		      (destructuring-bind (name &key type doc) (if (listp c) c (list c))
			(declare (ignore doc))
			(list name
			      (case type
				((:integer :int :short) "%d")
				(:long "%ld")
				(:unsigned "%u")
				(:unsigned-long "%lu")
				((:float :double) "%f")
				((:character :char) "#\%c")
				(:string "\\\"%s\\\"")
				(t "%d")))))
		  constants)))

(defun extract-constants (constants includes include-paths)
  "Extract C symbolic constants writing and compiling a special C
source files that produces an alist of constants and values.
Read its output and return the alist."
  (with-temp-file (output-stream (make-pathname :defaults *tmp-file-defaults* :type "c"))
    (write-c-source-for-compilation output-stream "" nil includes constants)
    (close output-stream)
    (let* ((c-source (pathname output-stream))
	   (exec-file (make-pathname :type "exe" :defaults c-source)))
      (unwind-protect
	   (progn
	     (apply #'run-program *c-compiler*
		    (list "-o" (namestring exec-file)
			  (append (mapcan #'(lambda (path)
					      (list "-I" path))
					  include-paths)
				  (list (namestring c-source)))))
	     (with-open-pipe (in nil (namestring exec-file) '())
	       (loop
		  for sexp = (read in nil)
		  while sexp
		  collect (make-instance 'c-constant-declaration
					 :name (car sexp)
					 :value (cadr sexp)))))
	(ignore-errors
	  (delete-file exec-file))))))

(defgeneric extract (input)
  (:documentation
   "Extract a list of all the definitions from INPUT.
Return a tagged list."))

(defmethod extract ((input stream))
  (let* ((*compile-print* nil)
	 (grammar (force binge-grammar:define-grammar)))
    (with-open-stream (input-stream (binge-lexer:make-parsable-stream input grammar))
      (loop
	 for decls = (npg:parse grammar 'binge-grammar:top-level-statement input-stream)
	 until (eq decls :eof)
	 when (member :skip-block decls)
	   do (binge-lexer:skip-block input-stream)
	 append (remove-if #'symbolp decls)))))

(defmethod extract ((input pathname))
  (with-open-file (stream input)
    (extract stream)))

(defmethod extract ((input string))
  (with-input-from-string (stream input)
    (extract stream)))

(defun extract-from-headers (includes &key (preamble "") defines constants include-paths)
  "Extract all the definitions from INCLUDES files."
  (let ((declarations
	 (be *run-verbose* t
	   (with-temp-file (output-stream (make-pathname :defaults *tmp-file-defaults* :type "c"))
	     (write-c-source-for-parser output-stream preamble defines includes
					(unless *extract-constants-with-cc*
					  constants))
	     (close output-stream)
	     (with-open-pipe (in nil *c-preprocessor*
				 (append (mapcan #'(lambda (path)
						     (list "-I" path))
						 include-paths)
					 (list (namestring (pathname output-stream)))))
	       (extract in))))))
    (if *extract-constants-with-cc*
	(append (extract-constants constants includes include-paths) declarations)
	declarations)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; The explain* stuff is mostly for fun
;;;

(defgeneric explain-c-object (obj)
  (:documentation
   "Explain in plain English what the C object OBJ stands for."))

(defmethod explain-c-object ((obj c-named-object))
  (format t "~A is" (c-name obj)))

(defmethod explain-c-object ((obj c-function))
  (when (next-method-p)
    (call-next-method))
  (format t "function accepting ~R argument~:*~P and returning a "
	  (length (c-arguments obj)))
  (explain-c-object (c-type obj)))

(defmethod explain-c-object ((obj c-struct-declaration))
  (format t "structure ~A of ~R slot~:*~P "
	  (c-name obj) (length (c-slots obj)))
  (dolist (slot (c-slots obj))
    (explain-c-object slot)))

(defmethod explain-c-object ((obj c-union-declaration))
  (format t "union ~A of ~R slot~:*~P "
	  (c-name obj) (length (c-slots obj)))
  (dolist (slot (c-slots obj))
    (explain-c-object slot)))

(defmethod explain-c-object ((obj c-enum-declaration))
  (format t "enum ~A of ~R value~:*~P "
	  (c-name obj) (length (c-constants obj))))

(defmethod explain-c-object ((obj c-user-type-ref))
  (format t "user defined type ~A" (c-ref-name obj)))

(defmethod explain-c-object ((obj c-array))
  (format t "array with ~R dimension~:*~P of elements of type " (length (c-dimensions obj)))
  (explain-c-object (c-type obj)))

(defmethod explain-c-object ((obj c-pointer))
  (format t "pointer to ")
  (explain-c-object (c-type obj)))

(defmethod explain-c-object ((obj c-variable-declaration))
  (format t "~A is a variable of type " (c-name obj))
  (explain-c-object (c-type obj)))

(defmethod explain-c-object ((obj symbol))
  (ecase obj
    (:char
     (format t "character"))
    (:int
     (format t "integer"))
    (:short
     (format t "short integer"))
    (:long
     (format t "long integer"))
    (:long-long
     (format t "very long integer"))
    (:unsigned-char
     (format t "unsigned character"))
    (:unsigned-int
     (format t "unsigned integer"))
    (:unsigned-short
     (format t "unsigned short integer"))
    (:unsigned-long
     (format t "unsigned long integer"))
    (:unsigned-long-long
     (format t "unsigned very long integer"))
    (:float
     (format t "floating point number"))
    (:double
     (format t "double precision floating point number"))
    (:void
     (format t "any or nothing"))))

(defun explain (string)
  (dolist (obj (extract string))
    (explain-c-object obj)
    (values)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
