(in-package #:simpsamp)

;;; These functions test and report the rates that elements are
;;; selected for a sample, averaged over thousands of iterations.
;;; They're surprisingly effective for identifying off-by-one errors.

(defun test-core (&key (iterations 100000) (set-lower -100) (set-upper 100)
		  (sample-size 15) (stream *standard-output*) (verbose-p t))
  (multiple-value-bind (expected-rate actual-rates)
      (measure-selection-rates iterations set-lower set-upper sample-size
			       (lambda (sample-size set-size
					set-next set-exhausted?
					visit)
				 (declare (ignore set-exhausted?))
				 (do-sample-of-iterator-expr
				     ((x) sample-size set-size
				      (funcall set-next))
				   (funcall visit x))))
    (report-selection-rates 'do-sample-of-iterator-expr
			    expected-rate actual-rates verbose-p stream))
  (multiple-value-bind (expected-rate actual-rates)
      (measure-selection-rates iterations set-lower set-upper sample-size
			       (lambda (sample-size set-size
					set-next set-exhausted?
					visit)
				 (declare (ignore set-size))
				 (let ((out (vector-sample-of-unbounded-iterator-expr
					     sample-size
					     (funcall set-next)
					     (funcall set-exhausted?))))
				   (map nil visit out))))
    (report-selection-rates 'vector-sample-of-unbounded-iterator-expr
			    expected-rate actual-rates verbose-p stream)))

(defun measure-selection-rates (iterations set-lower set-upper sample-size sample-fn)
  "Given the set of integers [SET-LOWER,SET-UPPER), measures the
selection rates of each element when repeatedly given to SAMPLE-FN,
which takes a simple random sample, without replacement, of the set.
SAMPLE-SIZE is the size of each sample.

The function SAMPLE-FN is called ITERATIONS times.  Its arguments are:

  SAMPLE-SIZE
    The requested size of the sample to take of the set.

  SET-SIZE
    The size of the set.

  SET-NEXT
    A function of no arguments returning the next element of the set.

  SET-EXHAUSTED?
    A function of no arguments returning a generalized boolean
    indicating whether the set has been exhausted.
  
  VISIT-SAMPLE-ELEMENT
    A function which must be called once for every element of the
    chosen sample.

Returns two values: the expected rate of selection for all elements,
based on the set size and sample size, and a vector of the actual
measured rate of selection for each element of the set; each element
of the vector corresponds to [SET-LOWER,SET-UPPER) (in the same
order).

The more any actual rate varies from the expected rate, the more
likely there was some bias or error in the sample taken by SAMPLE-FN.
A lower number of iterations can also cause this problem; 1,000,000 is
a good number of iterations to use to get a clear reading."
  (check-type iterations (integer 0) "a positive integer")
  (check-type set-lower integer)
  (check-type set-upper integer)
  (unless (<= set-lower set-upper)
    (error "SET-LOWER must be less than SET-UPPER."))
  (when (= set-lower set-upper)
    (error "The set must be non-empty.  (SET-LOWER must be less than SET-UPPER.)"))
  (let* ((set-size (- set-upper set-lower))
	 ;; The actual sample size we should be getting.
	 (expected-sample-size (min set-size sample-size))
	 ;; Maps elements to the number of times they have been
	 ;; returned in a sample.
	 (sample-element-counts (make-hash-table :test #'eql :size set-size))
	 ;; Used to track which elements have been selected for per
	 ;; sample (i.e. per iteration).
	 (elements-selected-this-sample (make-array set-size :element-type 'bit)))
    (dotimes (iter-num iterations)
      (with-range-iterator (next set-size exhausted? set-lower set-upper)
	(let ((actual-sample-size 0))
	  (fill elements-selected-this-sample 0)
	  (funcall sample-fn sample-size (set-size)
		   (lambda () (next)) (lambda () (exhausted?))
		   (lambda (x)
		     (unless (zerop (shiftf (aref elements-selected-this-sample
						  (- x set-lower))
					    1))
		       (error "Element was already selected in this same sample: ~S" x))
		     (incf (gethash x sample-element-counts 0))
		     (incf actual-sample-size)))
	  (unless (= expected-sample-size actual-sample-size)
	    (error "Expected to receive ~D sample~:P, but really got ~D."
		   expected-sample-size actual-sample-size)))))
    (maphash (lambda (element count)
	       (declare (ignore count))
	       (unless (and (typep element 'integer)
			    (<= set-lower element)
			    (< element set-upper))
		 (error "Got bogus element in sample: ~D" element)))
	     sample-element-counts)
    (values (/ expected-sample-size set-size)
	    (with-vector-accumulator (set-size/type accumulate)
	      (set-size/type set-size 'rational)
	      (loop for elt from set-lower below set-upper
		    for elt-count = (gethash elt sample-element-counts 0)
		    doing (accumulate (/ elt-count iterations)))))))

(defun report-selection-rates (label expected-rate actual-rates verbose-p stream)
  "Reports EXPECTED-RATE and some trivial statistics on ACTUAL-RATES,
as returned by MEASURE-SELECTION-RATES.  Prints the analysis to
STREAM.

When VERBOSE-P is true, prints each element of ACTUAL-RATES, along
with its error from EXPECTED-RATE and a graph of its error."
  ;; FIXME: I need a stats nerd to tell me if there's some reasonable
  ;; way we can return a true/false *estimate* of whether the results
  ;; indicate unbiased random samples, perhaps also based on number of
  ;; iterations, set size, and sample size.
  (flet ((rate-error (rate)
	   (- rate expected-rate)))
    (let ((mean (jpl-util:mean actual-rates))
	  (std-dev (jpl-util:standard-deviation actual-rates))
	  (max-error (rate-error (jpl-util:best actual-rates #'>
						:key (jpl-util:compose
						      #'abs #'rate-error)))))
      (format stream "~&~A:~&  expected rate ~8,6F; actual rates: ~
                      mean ~8,6F, std. dev. ~12,10F, max. error ~9,6F~&"
	      label expected-rate
	      mean std-dev max-error)
      (when verbose-p
	(format stream "  elt. # | act. rate  | error*10^5 | graph of error scaled to [-1,+1] |~&")
	(loop with error-stars-max = 16
	      for i below (length actual-rates)
	      for rate = (aref actual-rates i)
	      for error = (rate-error rate)
	      for error-scaled = (if (null max-error) ; Seems unlikely.
				     0
				     (/ error (abs max-error)))
	      ;; Little did I know I'd be plotting a graph.  But this
	      ;; is the clearest way of finding anomalies.
	      for error-stars-neg = (if (minusp error-scaled)
					(round (* error-stars-max (abs error-scaled)))
					0)
	      for error-stars-pos = (if (plusp error-scaled)
					(round (* error-stars-max error-scaled))
					0)
	      doing (format stream "  ~6D | ~10,8F | ~10,2,5F | ~
                                    ~VA~V,,,'*A~V,,,'*A~VA |~&"
			    i (coerce rate 'double-float)
			    (coerce error 'double-float)
			    (- error-stars-max error-stars-neg) ""
			    error-stars-neg ""
			    error-stars-pos ""
			    (- error-stars-max error-stars-pos) ""))
	(format stream "  (The more noise in the graph, the better.  ~
                          Randomly-distributed~&  spikes are expected.  ~
                          Clumps, slopes, or curves may indicate failure.)~&"))))
  (values))
