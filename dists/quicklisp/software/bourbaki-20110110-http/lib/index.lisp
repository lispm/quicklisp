(module "index"

  (meta (:description . "The database covers currently a greater part
of the first 1400 theorems of the
<a href=\"http://us2.metamath.org:8888/mpegif/mmtheorems.html\">set.mm</a>
file by Norman Megill, splitted into several 'modules'. The following diagram
lists the dependencies between these modules; solid line means the axioms
and theorems of the referred module are assumed ('imported'); dotted line
means that an interpretation for the axioms is provided and the theorems
are used ('exported'). For example,
the general transitivity theorems in <a href=\"equiv.html\">equiv</a>
are used in <a href=\"equ.html\">equ</a> for the equality relation
between sets.</p><hr><img src=\"images/modules-depend.png\"><hr><p>")
        (:index . '![prop-ax prop prop-extra pred-ax pred equ-ax equ unique
                     equiv-ax equiv order-ax order extent-ax extent
                     set-extent-ax set-extent descr-ax descr adjoin-ax adjoin]))
) ; end module "index
