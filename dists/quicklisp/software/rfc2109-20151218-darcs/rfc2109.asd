;-*- mode: lisp -*-

(cl:defpackage #:rfc2109-system
  (:use #:cl #:asdf))

(cl:in-package #:rfc2109-system)

(defsystem "rfc2109"
  :depends-on (:split-sequence)
  :version "0.4"
  :components ((:file "rfc2109")))

;;;;;;
;;; The test system

;; It loads the same file with :test in *features*. We need to use a different
;; name for the fasl file to avoid clashes (different packages are needed to
;; load the two different systems/fasl files).

(defclass load-file-with-tests (cl-source-file)
  ())

(defmethod perform :around (op (component load-file-with-tests))
  (let ((*features* *features*))
    (pushnew :test *features*)
    (call-next-method)))

(defmethod output-files :around ((op compile-op) (c load-file-with-tests))
  (multiple-value-bind
        (files fixedp)
      (call-next-method)
    (values
     (loop
       :for file :in files
       :collect (make-pathname :name (concatenate 'string
                                                  (pathname-name file)
                                                  "-with-tests")
                               :defaults file))
     fixedp)))

(defsystem "rfc2109/test"
  :depends-on (:split-sequence :fiveam)
  :components ((:load-file-with-tests "rfc2109")))
