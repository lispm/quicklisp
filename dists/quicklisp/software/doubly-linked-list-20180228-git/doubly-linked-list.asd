(asdf:defsystem #:doubly-linked-list
  :description "An implementation of the doubly linked list data structure."
  :author "Michael Fiano <mail@michaelfiano.com>"
  :maintainer "Michael Fiano <mail@michaelfiano.com>"
  :license "MIT"
  :homepage "https://www.michaelfiano.com/projects/doubly-linked-list"
  :source-control (:git "git@github.com:mfiano/doubly-linked-list.git")
  :bug-tracker "https://github.com/mfiano/doubly-linked-list/issues"
  :version "1.0.0"
  :encoding :utf-8
  :long-description #.(uiop:read-file-string (uiop/pathname:subpathname *load-pathname* "README.md"))
  :depends-on (#:alexandria)
  :pathname "src"
  :serial t
  :components
  ((:file "package")
   (:file "doubly-linked-list")))
