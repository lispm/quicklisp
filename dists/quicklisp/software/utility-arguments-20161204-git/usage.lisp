;;;; usage.lisp

;;; Copyright (C) 2016, Frank A. U. <fau@riseup.net>
;;;
;;; Permission to use, copy, modify, and/or distribute this software for any
;;; purpose with or without fee is hereby granted, provided that the above
;;; copyright notice and this permission notice appear in all copies.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


(in-package :utility-arguments)


(defmacro defusage (name (operand-lambda-list option-lambda-list) &body body)
  "Define a new usage for operands and options.

NAME is a symbol and denotes the name of the usage.

OPERAND-LAMBDA-LIST describes operands.  It resembles a lambda list
with required, optional, or rest parameter specifiers as the only
elements allowed.  The parameters receive the operands' values.

OPTION-LAMBDA-LIST describes options.  It resembles a lambda list with
keyword and rest parameter specifiers as the only elements allowed.
The parameters receive the options' values.

Optional and keyword parameter that specify an init-form make it
optional in this usage.

The forms of BODY are executed when the usage is applied."
  (multiple-value-bind (forms declarations documentation)
      (parse-body body :documentation t)

    `(progn
       (setf (get ',name 'operand-lambda-list) ',operand-lambda-list
             (get ',name 'option-lambda-list) ',option-lambda-list)

       ,(with-gensyms (operands options)
          `(defun ,name (,operands ,options)
             ,documentation
             (apply (lambda ,operand-lambda-list
                      (apply (lambda ,option-lambda-list
                               ,@declarations
                               ,@forms)
                             ,options))
                    ,operands))))))
(export 'defusage)


(declaim (inline operand-lambda-list))
(defun operand-lambda-list (usage)
  (get usage 'operand-lambda-list))


(declaim (inline option-lambda-list))
(defun option-lambda-list (usage)
  (get usage 'option-lambda-list))


(defun complete-usage (arity keys usages)
  (let (l)
    (dolist (u usages l)
      (multiple-value-bind (operands optionals rest?)
          (lambda-arity (operand-lambda-list u))

        (let ((options (lambda-key-parameters (option-lambda-list u))))
          (when (and (or rest?
                         (<= arity (+ operands optionals)))

                     (or (allow-other-keys-lambda? (option-lambda-list u))
                         (null (set-difference keys (plist-keys options)))))

            (push (list u (- operands arity)
                        (set-difference (plist-keys-if #'not options) keys)) l)))))))


(defun find-all-usages (arity keys usages)
  (mapcan (lambda (list)
            (destructuring-bind (usage arity lack) list
              (when (and (<= arity 0)
                         (zerop (length lack)))
                (list usage))))
          (complete-usage arity keys usages)))


(defun parse-operands (operands arguments usage)
  (when arguments
    (let ((keys (lambda-variables (operand-lambda-list usage)))
          rest)
      (multiple-value-setq (arguments rest)
        (split-at arguments (min (length arguments) (1- (length keys)))))

      (nconc (mapcar (lambda (k v)
                       (parse-operand-argument v (find-operand k operands)))
                     keys arguments)
             (mapcar (rcurry #'parse-operand-argument
                             (find-operand (car (last keys)) operands))
                     rest)))))


(defun apply-usage (usages operands options utility-arguments)
  "Apply a usage to utility arguments.

USAGES is a list of symbols naming usages defined with defusage.

The lists OPERANDS and OPTIONS hold objects that complement the
parameters on usage lambda lists in providing additional type and
syntactic information.  Correlation is established by object key and
parameter name.

UTILITY-ARGUMENTS is a list of strings that is parsed into operands
and options.  The operand and options are matched against the usage
lambda lists to find a suitable usage which is then applied to the
operand's and option's values.

An error condition of utility-argument-error, no-such-usage, or
usage-conflict may be signaled."
  (multiple-value-bind (arguments plist)
      (parse-utility-arguments utility-arguments options)

    (let ((l (find-all-usages (length arguments) (plist-keys plist) usages)))
      (case (length l)
        (0 (no-such-usage utility-arguments))

        (1 (let ((u (car l)))
             (funcall u (parse-operands operands arguments u) plist)))

        (t (usage-conflict l))))))
(export 'apply-usage)


;;;; usage.lisp ends here
