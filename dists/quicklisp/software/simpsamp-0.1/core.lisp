(in-package #:simpsamp)

;;; See doc.html#sec-low_level for the rationale of using macros to
;;; implement these core algorithms.

(defmacro do-sample-of-iterator-expr ((vars n-form size-form next-form
				       &key (effective-n-var nil effective-n-var-p)
				       (initial-form nil initial-form-p)
				       (result-form nil result-form-p))
				      &body body)
  "Iterates over a simple random sample of N-FORM elements, without
replacement, from a set expressed by SIZE-FORM and NEXT-FORM.

N-FORM is evaluated once, before any other arguments are evaluated.
If the value of N-FORM is larger than the size of the input set, then
the resulting sample will just contain all the elements of the
set--that is, the value of N-FORM is automatically lowered to the size
of the input set when necessary.

SIZE-FORM is then evaluated once to produce the number of elements in
the set.

When INITIAL-FORM is given, it is evaluated before iteration begins.
When RESULT-FORM is given, it is evaluated after iteration ends, and
its resulting values become the result of the iteration form; if
RESULT-FORM is not given, the result is NIL.  If EFFECTIVE-N-VAR is
given, it is bound with the effective number of elements to
process (the result of N-FORM limited by the actual size of the input
set) for the evaluation of INITIAL-FORM, NEXT-FORM, RESULT-FORM, and
BODY.

NEXT-FORM is evaluated to produce the next available element,
advancing through the set.  NEXT-FORM may produce multiple values;
these values taken together as a tuple constitute a single element of
the set.  NEXT-FORM will never be evaluated more times than the value
of SIZE-FORM.

For each element selected for the random sample, BODY is evaluated
with the symbol specified by VARS bound to a list of each value of the
element (the values yielded by NEXT-FORM).  Alternatively, VARS may be
a list of symbols, each of which is bound to each successive value of
the element, as if by MULTIPLE-VALUE-BIND.  In either case, the
bindings may be shared between different iterations, or the binding
may be fresh each time.

An implicit block named NIL is established around the loop.  RETURN
may be used to terminate the loop prematurely, overriding the default
resulting values of the iteration form (controlled by RESULT-FORM).

The order in which BODY is evaluated on the sampled elements is not
assured to be random.  The only thing that is random is which
selection is made of all the possible subsets of the input set with
the size expressed by N-FORM."
  ;; Implements the Selection Sampling Technique described in The Art
  ;; of Computer Programming, vol. 2, sec. 3.4.2 (algo. S).  Knuth
  ;; references a paper by C. T. Fan, Mervin E. Muller, and Ivan
  ;; Rezucha, J. Amer. Stat. Assoc.  57 (1962), 387-402.  Knuth also
  ;; attributes independent discovery of the algorithm to T. G. Jones,
  ;; CACM 5 (1962), 343.
  (jpl-util:with-gensyms (n% size% remaining-to-write% remaining-to-read%)
    `(let ((,n% ,n-form)
	   (,size% ,size-form))
       (unless (typep ,n% '(integer 0))
	 (error "N-FORM must have a non-negative integer value, but ~
                 its actual value is ~S."
		,n%))
       (unless (typep ,size% '(integer 0))
	 (error "SIZE-FORM must have a non-negative integer value, ~
                 but its actual value is ~S."
		,size%))
       (loop with ,remaining-to-write% = (min ,n% ,size%)
	     ,@(when effective-n-var-p
		 `(with ,effective-n-var = ,remaining-to-write%))
	     ,@(when initial-form-p
		 `(initially ,initial-form))
	     for ,remaining-to-read% downfrom ,size%
	     until (zerop ,remaining-to-write%)
	     doing ,(let ((body-code
			   `(when (< (random ,remaining-to-read%)
				     ,remaining-to-write%)
			      (decf ,remaining-to-write%)
			      (progn ,@body))))
		      (etypecase vars
			(list
			 (dolist (var vars)
			   (assert (symbolp var)))
			 `(multiple-value-bind (,@vars) ,next-form
			    (declare (ignorable ,@vars))
			    ,body-code))
			(symbol
			 `(let ((,vars (multiple-value-list ,next-form)))
			    (declare (ignorable ,vars))
			    ,body-code))))
	     finally (return ,(if result-form-p
				  result-form
				  '(values)))))))

(defmacro vector-sample-of-unbounded-iterator-expr (n-form next-form exhausted?-form)
  "Evaluates to a simple random sample of size N-FORM, without
replacement, from a set of unknown size expressed by NEXT-FORM and
EXHAUSTED?-FORM.  The resulting sample is expressed as a vector.

N-FORM is evaluated once, before any other arguments are evaluated.
If the value of N-FORM is larger than the size of the input set, then
the resulting sample will just contain all the elements of the
set--that is, the value of N-FORM is automatically lowered to the size
of the input set when necessary.  The value of N-FORM (before being
limited by the size of the input set) must be within the host
implementation's ARRAY-DIMENSION-LIMIT.

NEXT-FORM is evaluated to produce the next available element,
advancing through the input set.  (Its first return value is used as
the element, or NIL if NEXT-FORM produced no values.)  EXHAUSTED?-FORM
is evaluated to produce a generalized boolean indicating whether the
input set has been exhausted.  NEXT-FORM will never be evaluated
unless EXHAUSTED?-FORM has been evaluated since the last evaluation of
NEXT-FORM, and the result was false.

The order of the returned elements is not assured to be random.  The
only thing that is random is which selection is made of all the
possible subsets of the input set with the size expressed by N-FORM."
  ;; Implements the Reservoir Sampling algorithm described at
  ;; <http://gregable.com/2007/10/reservoir-sampling.html> and in The
  ;; Art of Computer Programming, vol. 2, sec. 3.4.2 (algo. R).  Knuth
  ;; attributes this algorithm to Alan G. Waterman.
  (jpl-util:with-gensyms (n% out% i% loop% known-input-size%)
    `(let ((,n% ,n-form))
       (unless (typep ,n% 'jpl-util:array-dimension)
	 (error "N-FORM must have a value that is usable as an array ~
                 dimension, but its actual value is ~S."
		,n%))
       (loop named ,loop%
	     with ,out% = (make-array ,n% :fill-pointer ,n%)
	     initially (when (zerop ,n%)
			 (return-from ,loop% ,out%))
	     initially (dotimes (,i% ,n%)
			 (declare (type fixnum ,i%))
			 (when ,exhausted?-form
			   (setf (fill-pointer ,out%) ,i%)
			   (return-from ,loop% ,out%))
			 (setf (aref ,out% ,i%) ,next-form))
	     for ,known-input-size% from (1+ ,n%)
	     until ,exhausted?-form
	     doing (let ((elt ,next-form))
		     (when (< (random ,known-input-size%) ,n%)
		       (setf (aref ,out% (random ,n%)) elt)))
	     finally (return-from ,loop% ,out%)))))
