(in-package :flac-parser)

(defmacro defdoc ((object type) &body body)
  `(setf (documentation ',object ',type)
         (format nil ,@body)))

;; Classes

(defdoc (flac-file type)
  "An object representing a FLAC audio file.~@
A FLAC-FILE object, with the full parse tree and other information about the data on disk, is ~
returned from the function, OPEN-FLAC.")

(defdoc (parse-tree type)
  "The parse tree compiled from a FLAC audio file on disk, conforming to the FLAC specification.")

(defdoc (metadata-block type)
  "A FLAC metadata block object, which holds the header and data portion of metadata block, as ~
described in the FLAC specification.")

(defdoc (metadata-block-header type)
  "An object describing the header of a FLAC metadata block.~@
This describes how the following data portion of the block is to be parsed, and how large it is.")

(defdoc (metadata-block-data type)
  "An object describing the data portion of a FLAC metadata block.~@
This is simple a named type that is inherited by more specific data classes.")

(defdoc (metadata-block-stream-info type)
  "A stream information metadata block, that contains information about the audio data.~@
This block has information about the whole stream, like sample rate, number of channels, total ~
number of samples, etc. It must be present as the first metadata block in the stream. Other ~
metadata blocks may follow, and ones that the decoder doesn't understand, it will skip.")

(defdoc (metadata-block-padding type)
  "A padding metadata block.~@
This block allows for an arbitrary amount of padding. The contents of a padding block have no ~
meaning.~@
This block is useful when it is known that metadata will be edited after encoding; the user can ~
instruct the encoder to reserve a padding block of sufficient size so that when metadata is added, ~
it will simply overwrite the padding (which is relatively quick) instead of having to insert it ~
into the right place in the existing file (which would normally require rewriting the entire file).")

(defdoc (metadata-block-application type)
  "An application metadata block.~@
This block is for use by third-party applications. The only mandatory field is a 32-bit identifier. ~
This ID is granted upon request to an application by the FLAC maintainers. The remainder is of the ~
block is defined by the registered application.")

(defdoc (metadata-block-seek-table type)
  "A seek table metadata block.~@
This is an optional block for storing seek points. It is possible to seek to any given sample in a ~
FLAC stream without a seek table, but the delay can be unpredictable since the bitrate may vary ~
widely within a stream. By adding seek points to a stream, this delay can be significantly reduced. ~
Each seek point takes 18 bytes, so 1% resolution within a stream adds less than 2k. There can be ~
only one seek table in a stream, but the table can have any number of seek points. There is also a ~
special 'placeholder' seekpoint which will be ignored by decoders but which can be used to reserve ~
space for future seek point insertion.")

(defdoc (metadata-block-seek-table/seek-point type)
  "An individual seek point within a seek table metadata block.")

(defdoc (metadata-block-vorbis-comment type)
  "A Vorbis comment metadata block.~@
This block is for storing a list of human-readable name/value pairs. Values are encoded using ~
UTF-8. It is an implementation of the Vorbis comment specification (without the framing bit). ~
This is the only officially supported tagging mechanism in FLAC. There may be only one Vorbis ~
comment block in a stream. In some external documentation, Vorbis comments are called FLAC tags to ~
lessen confusion.")

(defdoc (metadata-block-cue-sheet type)
  "A cue sheet metadata block.~@
This block is for storing various information that can be used in a cue sheet. It supports track ~
and index points, compatible with Red Book CD digital audio discs, as well as other CD-DA metadata ~
such as media catalog number and track ISRCs. The cue sheet block is especially useful for backing ~
up CD-DA discs, but it can be used as a general purpose cueing mechanism for playback.")

(defdoc (metadata-block-cue-sheet/track type)
  "An individual track within a cue sheet metadata block.")

(defdoc (metadata-block-cue-sheet/track-index type)
  "An individual index point for a track within a cue sheet metadata block.")

(defdoc (metadata-block-picture type)
  "A picture metadata block.~@
This block is for storing pictures associated with the file, most commonly cover art from CDs. ~
There may be more than one PICTURE block in a file. The picture format is similar to the APIC frame ~
in ID3v2. The picture block has a type, MIME type, and UTF-8 description like ID3v2, and supports ~
external linking via URL (though this is discouraged). The differences are that there is no ~
uniqueness constraint on the description field, and the MIME type is mandatory. The picture block ~
also includes the resolution, color depth, and palette size so that the client can search for a ~
suitable picture without having to scan them all.")

;;; Functions

(defdoc (parse function)
  "The recursive descent parser, responsible for parsing the whole of the FLAC specification into a ~
tree structure.~@
Returns a PARSE-TREE object.")

(defdoc (report-type function)
  "Report a type check problem so that it can later be handled by the repair stage.")

(defdoc (type-check-rule function)
  "Performs a type validity check, emitting a report if there is a problem.")

(defdoc (type-check function)
  "Recursively performs type validity checking on a parse tree object.~@
This is called after parsing, and after modification to the in-memory tree before writing a file ~
back to disk.")

(defdoc (with-flac-buffer function)
  "A convenience interface for opening a FLAC file on disk, and initializing a buffer for accessing ~
its stream of binary data.")

(defdoc (get-file-size function)
  "Calculates the size in bytes of a FLAC file on disk, given a FLAC-FILE object.")

(defdoc (open-flac function)
  "Opens a FLAC file given a filesystem path, and attempts to parse its data according to the ~
FLAC specification.~@
This is the entry point of the library.")

(defdoc (block-type-name function)
  "Converts the type of a metadata block to a human-readable keyword symbol.")

(defdoc (last-block-p function)
  "Checks whether a given metadata block is the last occurence in the file.")

(defdoc (list-blocks-by-type function)
  "Returns a list of metadata blocks of a particular type for a file.")

(defdoc (list-pictures-by-type function)
  "Returns a list of picture metadata blocks with the specified picture-type.")

(defdoc (seek-point-placeholder-p function)
  "Check if a seek table's seek point is a placeholder.")

(defdoc (seek function)
  "Seeks a FLAC-FILE object's stream, relative to the start, end, or current position.~@
If OFFSET is not supplied, return the current file position.")

(defdoc (read-ubytes-le function)
  "Read an integer from a file's stream, according to the specified number of unsigned bytes in ~
little endian order.")

(defdoc (read-ubytes-be function)
  "Read an integer from a file's stream, according to the specified number of unsigned bytes in ~
big endian order.")

(defdoc (sign-extend function)
  "Convert an unsigned integer to a signed integer.")

;;; Conditions

(defdoc (flac-error type)
  "A general error condition.~@
Other error conditions are to inherit this condition.")

(defdoc (invalid-file type)
  "An error condition for when a valid marker at the start of a file is not found.~@
The FLAC specification mandates that a FLAC file starts with ASCII sequence \"fLaC\".~@
This marker must be present for the parser to proceed with the file.")
