(cl:in-package #:fucc-generator)

(defun make-lr-lexer (tokens)
  (lambda ()
    (let ((val (pop tokens)))
      (format t "LEXER: ~S~%" val)
      (values val val))))

(fucc:defparser *test-exp-lr*
    s
  (:a :b :c)
  ((s -> :a (:do (format t "Hello ")) b :b :c
      (:call #'list))
   (b -> (:do (format t "world!~%")) :b))
  :type :lr0)

(fucc:defparser *test-err-lalr*
        s
      (:a :b :c)
      ((s -> :a b :b :c
          (:call #'list))
       (b -> c2 :c)
       (c2 -> (:call (lambda () (format t "Hello ")))))
      :type :lalr)

(defun run-simple-lr-test ()
  (fucc:parser-lr (make-lr-lexer '(:a :b :b :c)) *test-exp-lr*))

(defun run-err-lalr-test ()
  (fucc:parser-lr (make-lr-lexer '(:a :c :b :c)) *test-err-lalr*))
