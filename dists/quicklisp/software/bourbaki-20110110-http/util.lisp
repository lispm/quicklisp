;;; Copyright (c) Juha Arpiainen 2005-06

;;; This file is part of Bourbaki.

;;; Bourbaki is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.

;;; Bourbaki is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Bourbaki; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; *****
;;; Utility functions
;;; *****

(in-package :bourbaki)

;;; unordered pairs (a . b) and (c . d) are equal if they contain the same elements
;;; ie., [a = c & b = d] or [a = d & b = c]
(defun pair-eq (lhs rhs)
  "Equality of unordered pairs"
  (or (and (eq (car lhs) (car rhs)) (eq (cdr lhs) (cdr rhs)))
      (and (eq (car lhs) (cdr rhs)) (eq (cdr lhs) (car rhs)))))

;;; cartesian product of vars1 and vars2
;;; ie., the list of all (a . b) such that (member a vars1) and (member b vars2)
(defun cartesian-product (vars1 vars2)
  "Cartesian product of two lists"
  (cond ((null vars1) nil)
    ((typep vars1 'context)
     (cond ((null vars2) nil)
           (t (cons (cons vars1 (car vars2)) (cartesian-product vars1 (cdr vars2))))))
    (t (union (cartesian-product (car vars1) vars2) (cartesian-product (cdr vars1) vars2) :test #'eq))))

;;; Push x into second position of lst
(defun push-second (x lst)
  (setf (cdr lst) (cons x (cdr lst))))

;;; Flatten a tree. From "On Lisp", p. 49
(defun flatten (lst)
  (labels ((rec (x acc)
             (cond ((null x) acc)
                   ((atom x) (cons x acc))
                   (t (rec (car x) (rec (cdr x) acc))))))
    (rec lst nil)))


;;; Ensure argument is a list
(defun mklist (x)
  (if (listp x)
      x
      (cons x nil)))

;;; Copy first len members of lst; apply fn to rest
;;; == splice with identity as fn1
;;; rename lhs-splice?
(defun simple-splice (lst len fn)
  (if (= len 0)
    (apply fn lst)
    (let ((temp lst))
      (dotimes (i (1- len))
        (setq temp (cdr temp)))
      (setf (cdr temp) (apply fn (cdr temp)))
      lst)))

;;; Like simple-splice, but apply fn to len last
;;; members of lst
;;; rename rhs-splice?
(defun splice (lst len fn)
  (let ((total (length lst))
        (temp lst))
    (cond
      ((= len total) (apply fn lst))
      ((= len 0) (nconc lst (funcall fn)))
      (t (dotimes (i (- total len 1))
           (setf temp (cdr temp)))
         (setf (cdr temp) (apply fn (cdr temp)))
         lst))))

(defun lib-path-name (filename &optional type)
  (make-pathname :directory `(:relative ,*bourbaki-library-path*)
                 :name filename
                 :type type))

;;; load a file from bourbaki lib directory
(defun load-aux (filename &optional type)
  (load (lib-path-name filename type)))

;(defun string-case-expander (sym cases)
;  (if (null cases)
;    nil
;    (destructuring-bind (str . body) (car cases)
;      `(if (string= ,sym ,str)
;         (progn . ,body)
;         ,(string-case-expander sym (cdr cases))))))

;(defmacro string-case (form &rest cases)
;  (let ((sym (gensym)))
;    `(let ((,sym ,form))
;       ,(string-case-expander sym cases))))

;; Anaphoric if
(defmacro aif (test then &optional else)
  `(let ((it ,test))
     (if it ,then ,else)))

(defmacro acond (&body conds)
  (labels ((rec (cnds)
             (if cnds
               `(aif ,(caar cnds)
                   (progn ,@(cdar cnds))
                   ,(rec (cdr cnds)))
               nil)))
    (rec conds)))

;; By Tim Bradshaw, http://www.tfeb.org/lisp/hax.html
(defmacro collecting (&body forms)
  ;; Collect some random stuff into a list by keeping a tail-pointer
  ;; to it, return the collected list.  No real point in using
  ;; gensyms, although one probably should on principle.
  "Collect things into a list forwards.  Within the body of this macro
   The form `(COLLECT THING)' will collect THING into the list returned by 
   COLLECTING.  Uses a tail pointer -> efficient."
  (let (($resnam$ (gensym)) ($tail$ (gensym)) ($thing$ (gensym)))
    `(let
       (,$resnam$ ,$tail$)
       (macrolet
	 ((collect
	     (thing)
	    ;; Collect returns the thing it's collecting
	    `(let ((,',$thing$ ,thing))
	       (if ,',$resnam$
		   (setf (cdr ,',$tail$)
			   (setf ,',$tail$ (list ,',$thing$)))
		   (setf ,',$resnam$
			 (setf ,',$tail$ (list ,',$thing$))))
	       ,',$thing$)))
	 ,@forms)
       ,$resnam$)))

(defmacro with-gensyms (syms &body body)
  `(let ,(mapcar #'(lambda (s)
                     `(,s (gensym)))
                 syms)
     ,@body))

;; Integers 1, 2, ..., n-1 in ascending order
(defun int-list (n)
  (loop for i from 0 below n collect i))

;; Integers 1, 2, ..., n-1 in descending order
(defun r-int-list (n)
  (let ((lst nil))
    (dotimes (i n)
      (push i lst))
    lst))

(defun highest-number-in (tree)
  (apply #'max -1 (remove-if-not #'integerp (flatten tree))))

;; Use for symtrees
(defun op (x)
  (car x))
(defun args (x)
  (cdr x))

;; Flatten the car of argument one level,
;; return the first symbol in tree and
;; rest of the flattened tree.
;; Used in pattern.lisp
;; TODO: should nconc instead of append?
(defun flatten-1 (wff)
  (if (listp (car wff))
    (values (caar wff)
            (append (cdar wff) (cdr wff)))
    (values (car wff)
            (cdr wff))))
