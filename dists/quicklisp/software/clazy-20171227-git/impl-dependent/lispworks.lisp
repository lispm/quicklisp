;;;; -*- Mode: Lisp -*-

;;;; lispworks.lisp --
;;;; CLAZY implementation dependency file.

(in-package "CLAZY")

(defun arglist (function-designator)
  (lw:function-lambda-list function-designator))

;;;; end of file -- lispworks.lisp --
