;;;; -*- Mode: lisp; indent-tabs-mode: nil -*-

(defpackage #:abstract-classes-system
	(:use #:cl #:asdf))

(in-package #:abstract-classes-system)

(defsystem abstract-classes
    :description "Extends the MOP to allow `abstract` and `final` classes."
    :author "Tim Bradshaw (tfb at lostwithiel)"
    :version "1.7"
    :components
    ((:file "abstract-classes"))
    :depends-on
    ("closer-mop"))




              
