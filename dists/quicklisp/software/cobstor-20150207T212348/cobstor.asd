;;;  cobstor.asd --- System description for the Cobol Storage library

;;;  Copyright (C) 2005, 2006, 2008, 2009 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: cobstor

#+cmu (ext:file-comment "$Module: cobstor.asd, Time-stamp: <2009-01-09 10:16:57 wcp> $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

#-(or cmu sbcl)
(warn "This code hasn't been tested on your Lisp system.")

(eval-when (:compile-toplevel :load-toplevel :execute)
  ;; CMUCL 19d has a broken WITHOUT-PACKAGE-LOCKS (used somewhere in cobstor)
  #+cmu19d (ext:unlock-all-packages))

(defpackage :cobstor-system
  (:use :common-lisp :asdf #+asdfa :asdfa)
  (:export #:*base-directory*
	   #:*compilation-epoch*))

(in-package :cobstor-system)

(defvar *base-directory*
  (make-pathname :name nil :type nil :version nil
                 :defaults (parse-namestring *load-truename*)))

(defparameter *compilation-epoch* (get-universal-time))

(setf (logical-pathname-translations "cobstor")
      `(("**;*.*.*" ,(make-pathname :defaults *base-directory*
				    :directory (append (pathname-directory *base-directory*)
						       '(:wild-inferiors))
				    :name :wild
				    :type :wild
				    :version :wild))))

;; These are almost certainly not what you want.  Redefine them
;; according to where your projects keeps source files.
(setf (logical-pathname-translations "cobstor-dynamic")
      `(("**;*.*.*" ,(logical-pathname "cobstor:dynamic;**;*.*.*"))))

(setf (logical-pathname-translations "cobstor-copys")
      `(("**;*.*.*" ,(logical-pathname "cobstor-dynamic:copys;**;*.*.*"))))

(setf (logical-pathname-translations "cobstor-data")
      `(("**;*.*.*" ,(logical-pathname "cobstor-dynamic:data;**;*.*.*"))))


(defsystem cobstor
    :name "COBSTOR"
    :author "Walter C. Pelissero <walter@pelissero.de>"
    :maintainer "Walter C. Pelissero <walter@pelissero.de>"
    :description "Cobol Storage library"
    :long-description
    "This is a package that provides access to Cobol data files
from within Common Lisp.  Its design is of the proxy/agent type,
where the lisp program communicates through a proxy library to a
cobol program (the agent) which ultimately accesses the data
storage.  An generator is included which automates the extraction
of the necessary meta-data from Cobol source files."
    :licence "LGPL"
    :depends-on (:sclf :npg :net4cl)
    :components
    ((:module "src"
	      :components
	      ((:module "proxy"
			:components
			((:file "package")
			 (:file "field" :depends-on ("package"))
			 (:file "record" :depends-on ("package" "field"))
			 (:file "file" :depends-on ("package" "record"))))
	       (:module "gen"
			:depends-on ("proxy")
			:components
			((:file "ss" :depends-on (#+cmu19b "cmupatch"))
			 #+cmu19b (:file "cmupatch")
			 (:file "package" :depends-on ("ss"))
			 (:file "lexer" :depends-on ("package" "ss"))
			 (:file "grammar" :depends-on ("package"))
			 (:file "agent" :depends-on ("package" "parser"))
			 (:file "stub" :depends-on ("package"))
			 (:file "parser" :depends-on ("package" "grammar" "lexer"))
			 (:file "generator" :depends-on ("package" "parser" "agent" "stub"))))))))

(defmethod perform ((o test-op) (c (eql (find-system 'cobstor))))
  (oos 'load-op 'cobstor-tests)
  (oos 'test-op 'cobstor-tests :force t))
