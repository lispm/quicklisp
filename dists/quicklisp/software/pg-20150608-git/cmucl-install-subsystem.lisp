;;; cmucl-install-subsystem.lisp
;;
;;
;; Loading this into CMUCL will install pg.lisp as a CMUCL subsystem,
;; that can be loaded with
;;
;;   (require :pg)
;;
;; Note that the user running CMUCL needs to have write access to the
;; subsystems directory (so you may need to run this as root).


(let ((fasl-1 (compile-file "defpackage" :load t))
      (fasl-2 (compile-file "sysdep" :load t))
      (fasl-3 (compile-file "pg")))
  (with-open-file (out (compile-file-pathname "library:subsystems/pg-library")
                       :direction :output
                       :element-type '(unsigned-byte 8)
                       :if-exists :supersede)
    (dolist (f (list fasl-1 fasl-2 fasl-3))
      (with-open-file (in f
                          :direction :input
                          :element-type '(unsigned-byte 8))
        (loop
         :with buffer = (make-array 1024 :element-type '(unsigned-byte 8))
         :for n = (read-sequence buffer in)
         :until (= n 0)
         :do (write-sequence buffer out :end n))))))


;; EOF
