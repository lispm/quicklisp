;;; ************
;;; Metatheorems
;;; ************

;; hack -- to avoid loading this file multiple times
(module "equ-meta"
  (import [!!equ])

; Theorem with distinct variable conditions
; For all hypotheses [→ φ ∀ x φ] in thr,
; add distinct variable condition [x φ]
(metath "distv" (ref)
  (let ((pfl (parse-subproof ref)))
    (subproof (:assert (subproof-assert pfl))
      (dolist (h (subproof-hypo pfl))
        (if-match [Bvp ?x ?ph] h
          (line [ax17-bv ,?x ,?ph])
          (hypo h)))
      (line pfl))))

); end module equ-meta
;  (in-context !/equ-meta
;    (copy-th thr "v" (assert vars distinct)
;      (proof `(,thr ,@(makevarlist (context-vars thr))))
;      (loop for h in (context-hypo thr)
;            do (if-match ("Bvp" x ph) h
;                 (progn
;                   (push (cons (car x) (car ph)) (context-distinct *current-context*)) ;; TODO: general case
;                   (push [ax17-bv ,x ,ph] (context-proof *current-context*)))
;                 (hypo h))))))

;(defun string-case-expander (sym cases)
 ; (if (null cases)
  ;  nil
   ; (destructuring-bind (str . body) (car cases)
    ;  `(if (string= ,sym ,str)
     ;    (progn . ,body)
      ;   ,(string-case-expander sym (cdr cases))))))

;(defmacro string-case (form &rest cases)
 ; (let ((sym (gensym)))
  ;  `(let ((,sym ,form))
   ;    ,(string-case-expander sym cases))))
#|
(memoized bv-theorem (sym)
  (in-context !/equ-meta
    (mkcontext
      (string-case (context-name sym)
        ("→" !bv-→)
        ("↔" !bv-↔)
        ("¬" !bv-¬)
        ("∧" !bv-∧)
        ("∨" !bv-∨)
        ("∀" !bv-∀)
        ("∃" !bv-∃)))))

(memoized bind-theorem (sym)
  (in-context !/equ-meta
    (string-case (context-name sym)
      ("sb" (make-array 2 :initial-contents (list nil (mkcontext !bv-sbb))))
      ("∀"  (make-array 2 :initial-contents (list nil (mkcontext !bv-∀b))))
      ("∃"  (make-array 2 :initial-contents (list nil (mkcontext !bv-∃b)))))))

(defun ch-stree-op (sym tree)
  (cons sym (cdr tree)))

(defun ch-stree-op-var (sym var tree)
  (cons sym (cons var (cdr tree))))

(defun find-binder (var tree)
  (let ((op (car tree)))
    (do
      ((i 0 (1+ i))
       (pos (cdr tree) (cdr pos))
       (arg (context-vars op) (cdr arg)))
      ((or (null pos) (and (eq var (caar pos)) (bsymtype-is-bound (context-type (car arg)))))
         (if (null pos) -1 i)))))

(defun sb-assert (line)
  (replace-vars (context-assert (car line))
                (pairlis (context-vars (car line))
                         (cdr line))))

(defun proof-seek (tr)
  (let ((res (find tr *current-proof* :key #'sb-assert :test #'wff-equal)))
    (if res
      (progn
        (terpri) (print-symtree t tr) (format t " found!"))
      (progn
        (terpri) (print-symtree t tr) (format t " not found.")))
    res))

; Try to construct a proof for [Bv x φ] or [Bvt x t] or [Bvc x A]
; Where φ,t,A = [op ...]
; 1. if var is bound by op, use the appropriate bind-theorem
; 2. TODO: if (x . φ) is in dvc, use ax-17
; 3. TODO: see if φ is in *current-proof* and use bv-theorem if possible
; 4. use bv-theorem and call pline recursively for the subexpressions
(defun hbuilder (vref tree)
  (in-context !/equ-meta
    (labels ((pline (tr)
               (if (proof-seek (list (mkcontext !Bvp) tr (cons var nil))) nil
                 (let ((idx (find-binder var tr)))
                   (format t "~&Index ~d, tree " idx)
                   (print-symtree t tr)
                   (if (>= idx 0)
                     (push (ch-stree-op (aref (bind-theorem (car tr)) idx) tr) *current-proof*)
                     (if (proof-seek tr)
                       (push [bv-theor ,var ,tr] *current-proof*)
                       (progn
                         (dolist (term (cdr tr))
                           (pline term))
                         (push (ch-stree-op-var (bv-theorem (car tr)) var tr) *current-proof*))))))))
      (pline tree)
      nil)))
|#
;; TODO: same for equality theorems, substitution theorems
