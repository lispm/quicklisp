#|
  This file is a part of cl-sat.minisat project.
  Copyright (c) 2016 Masataro Asai (guicho2.71828@gmail.com)
|#

(in-package :cl-user)
(defpackage cl-sat.minisat
  (:use :cl :trivia :alexandria :iterate :cl-sat)
  (:export
   #:*minisat-home*))
(in-package :cl-sat.minisat)

;; blah blah blah.

(defvar *minisat-home* (asdf:system-relative-pathname :cl-sat.minisat "minisat/"))

(defun minisat-binary (&optional (*minisat-home* *minisat-home*))
  (if (trivial-package-manager:which "minisat")
      "minisat"
      (merge-pathnames "build/release/bin/minisat" *minisat-home*)))

(defmethod solve ((input pathname) (solver (eql :minisat)) &rest options)
  (with-temp (dir :directory t :template "minisat.XXXXXXXX")
    (let* ((command (format nil "cd ~a; ~a ~{~A~^ ~} ~a ~a"
                            (namestring dir)
                            (namestring (minisat-binary))
                            options (namestring input) "result")))
      (format t "~&; ~a" command)
      (multiple-value-match (uiop:run-program command :output *standard-output* :error-output *error-output* :ignore-error-status t)
        ((_ _ 0)
         ;; indeterminite
         (values nil nil nil))
        ((_ _ 10)
         ;; sat
         (ematch (iter (for token in-file (format nil "~a/result" dir))
                       (collect token))
           ((list* _ assignments)
            (values
             (iter (for v in (sat-instance-variables *instance*))
                   (for a in assignments)
                   (when (plusp a) (collect v)))
             t t))))
        ((_ _ 20)
         ;; unsat
         (values nil nil t))))))


