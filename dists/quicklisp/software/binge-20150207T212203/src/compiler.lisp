;;;  compiler.lisp --- generic FFI generation code

;;;  Copyright (C) 2004-2006 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-FB002F8C6D005F4536C551E629BF0CF8.lisp,v 1.25 2004/04/15 22:49:24 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-FB002F8C6D005F4536C551E629BF0CF8.lisp,v 1.25 2004/04/15 22:49:24 wcp Exp $")

(in-package :binge-compiler)

(defvar *backend* nil
  "Should hold the current backend to be used by the compiler.")

(define-condition compilation-error (error)
  ((declaration :accessor comp-error-declaration :initarg :declaration))
  (:report (lambda (condition stream)
	     (format stream "unspecified compilation error in declaration ~A."
		     (comp-error-declaration condition)))))

(define-condition compilation-unsupported-error (compilation-error)
  ((item :reader comp-error-item :initarg :item))
  (:report (lambda (condition stream)
	     (format stream "~A in ~A is not supported by ~A."
		     (comp-error-item condition)
		     (comp-error-declaration condition)
		     *backend*))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; This stuff is silly, but I couldn't resist.
;;;

(defgeneric convert-to-C-syntax (c-object)
  (:documentation
   "Convert C-OBJECT to C syntax.  More or less the opposite of
what the BINGE parser does." ))

(defgeneric C-expression (c-object name)
  (:documentation
   "Convert C-OBJECT type to C syntax and attach name." ))

(defmethod C-expression ((object symbol) name)
  (format nil "~A~@[ ~A~]"
	  (ecase object
	    (:void "void")
	    (:char "char")
	    (:short "short")
	    (:int "int")
	    (:long "long")
	    (:unsigned-char "unsigned char")
	    (:unsigned-short "unsigned short")
	    (:unsigned-int "unsigned")
	    (:unsigned-long "unsigned long")
	    (:float "float")
	    (:double "double")
	    (:vararg "..."))
	  name))

(defmethod C-expression ((object c-user-type-ref) name)
  (format nil "~A~@[ ~A~]" (c-ref-name object) name))

(defmethod C-expression ((object c-struct-ref) name)
  (format nil "struct ~A~@[ ~A~]" (c-ref-name object) name))

(defmethod C-expression ((object c-union-ref) name)
  (format nil "union ~A~@[ ~A~]" (c-ref-name object) name))

(defmethod C-expression ((object c-enum-ref) name)
  (format nil "enum ~A~@[ ~A~]" (c-ref-name object) name))

(defmethod C-expression ((object c-bitfield) name)
  (C-expression (c-type object) (format nil "~A : ~A" name (c-bits object))))

(defmethod C-expression ((object c-pointer) name)
  (C-expression (c-type object) (concatenate 'string "*" name)))

(defmethod C-expression ((object c-array) name)
  (C-expression (c-type object)
		(format nil "~A~{[~@[~A~]]~}" name
			(mapcar #'(lambda (dim)
				    ;; if it's not a number, I can't
				    ;; be bothered to translate the
				    ;; expression
				    (if (numberp dim)
					dim
					"#"))
				(c-dimensions object)))))

(defmethod C-expression ((object c-function) name)
  (format nil "~A (~A)(~{~A~^, ~})"
	  (C-expression (c-type object) nil)
	  name
	  (mapcar #'convert-to-C-syntax (c-arguments object))))

(defmethod convert-to-C-syntax ((object c-typed-name))
  (C-expression (c-type object) (c-name object)))

(defmethod convert-to-C-syntax ((declaration c-function-declaration))
  (format nil "~&~<;; ~@;~A~:<~@{~A~^, ~:_~}~:>~:>"
	  (list (C-expression (c-type declaration) (c-name declaration))
		(mapcar #'convert-to-C-syntax (c-arguments declaration)))))

(defmethod convert-to-C-syntax ((declaration c-variable-declaration))
  (format nil "~&;; ~A"
	  (C-expression (c-type declaration) (c-name declaration))))

(defun convert-compound-to-C-syntax (type declaration)
  (format nil "~&~<;; ~@;~A ~@[~A ~]~@[{~&  ~<~;~@{~A; ~_~}~;}~:>~]~:>"
	  (list type (c-name declaration)
		(mapcar #'convert-to-C-syntax (c-slots declaration)))))

(defmethod convert-to-C-syntax ((declaration c-struct-declaration))
  (convert-compound-to-C-syntax "struct" declaration))

(defmethod convert-to-C-syntax ((declaration c-union-declaration))
  (convert-compound-to-C-syntax "union" declaration))

(defmethod convert-to-C-syntax ((declaration c-declaration))
  ;; for the declarations which don't deserve extra explanations,
  ;; don't output anything
  nil)

(defmethod c-object->ffi-notation :around ((declaration c-declaration) backend)
  (format nil "~@[~&;;~%~A~%;;~%~]~@[;; ~A~%~]~A" (convert-to-C-syntax declaration)
	  (c-documentation declaration)
	  (call-next-method)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun write-package (output-stream declarations)
  "Write the package declaration to OUTPUT-STREAM.  The package
declaration is taken from DECLARATIONS.  The package name is
taken from BACKEND.  If that slot is not set the whole
declaration is skipped."
  (declare (type stream output-stream)
	   (type list declarations))
  (cond ((and (backend-package *backend*)
	      (backend-define-package *backend*))
	 (format output-stream "(cl:in-package :cl-user)~3%")
	 (format output-stream "(defpackage ~S~%~3T(:use~<~@{~:_ ~S~}~:>)~%~3T(:export~<~@{~:_ ~S~}~:>))~3%"
		 (backend-package *backend*)
		 (use-packages *backend*)
		 ;; Some declarations are homonymous (a struct with the
		 ;; same name of a typedef, for instance).  This loop
		 ;; collects only the unique names.
		 (loop
		    for decl in declarations
		    with names = '()
		    ;; skip the nameless declarations
		    unless (anonymous-declaration-p decl)
		    do (pushnew (c-object-lisp-name decl *backend*) names)
		    ;; order is uninfluential, don't bother to reverse
		    finally (return names)))
	 (format output-stream "(in-package ~S)~2%" (backend-package *backend*)))
	((backend-package *backend*)
	 (format output-stream "(cl:in-package ~S)~3%" (backend-package *backend*)))))

(defun write-declarations (output-stream declarations-list)
  "Write a list of declarations to OUTPUT-STREAM."
  (dolist (decl declarations-list)
    (handler-case
	(progn
	  (princ (c-object->ffi-notation decl *backend*)
		 output-stream)
	  (terpri output-stream)
	  (terpri output-stream))
      (compilation-error (condition)
	;; fill in the blanks...
	(setf (comp-error-declaration condition) decl)
	;; ...and report the problem, but don't stop running
	(warn "compiling FFI: ~A" condition)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *compress-types* nil
  "Whether or not the compiler should try to compress the type
hierarchy to avoid writing unnecessary type declarations in the
generated FFI.  This variable can have one of the following
values:

  - NIL, don't compress
  - :TYPEDEFS, remove all the typedefs not required by user
  - :ALL, remove all the types that are not strictly required or
    requested by user.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass fast-searchable-list ()
  ((table :initform (make-hash-table))
   (list :initform '()
	 :reader get-list)))

(defun member-of-fslist (obj hash)
  (declare (type fast-searchable-list hash))
  (gethash obj (slot-value hash 'table)))

(defun push-into-fslist (obj hash)
  (declare (type fast-searchable-list hash))
  (setf (gethash obj (slot-value hash 'table))
	(push obj (slot-value hash 'list)))
  hash)

(defun add-presence-into-fslist (obj hash)
  "This will add only a placeholder in the hashtable of a
FAST-SEARCHABLE-LIST, but not in its list.  This is handy to
pretend the object is already in the list when actualy it isn't."
  (declare (type fast-searchable-list hash))
  (setf (gethash obj (slot-value hash 'table)) t)
  hash)

(defmethod print-object ((obj fast-searchable-list) stream)
  (format stream "~S" (slot-value obj 'list)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric unalias-reference (object namespace)
  (:documentation
   "Return the primitive type or a reference to the first
user-required typedef or compound object at the basis of object.
Look it up in NAMESPACE."))

(defmethod unalias-reference (object namespace)
  ;; Anything that is not a user-type reference is returned unchanged.
  ;; That is, primitive types or references to compound objects are
  ;; kept unchanged.
  object)

(defmethod unalias-reference ((object c-user-type-ref) namespace)
  (let ((typedef (find-declaration object namespace)))
    (if (c-required typedef)
	;; if it refers to a required typedef, than keep the reference
	object
	;; otherwise search deeper
	(unalias-reference (c-type typedef) namespace))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric collect-declarations (declaration declarations-list namespace)
  (:documentation
   "Add to DECLARATIONS-LIST any missing declarations to fully
define DECLARATION itself.  All the additional declarations are
taken from NAMESPACE.  DECLARATIONS-LIST is sorted with most
basic declarations last and independed delcarations first in the
list."))

(defmethod collect-declarations ((declaration symbol) declarations-list namespace)
  ;; a base type doesn't need to be added
  declarations-list)

(defmethod collect-declarations ((declaration c-declaration) declarations-list namespace)
  ;; First push the declarations this declaration depends on, and then
  ;; this declaration itself.  That's according to the order of
  ;; declarations-list: most basic first.
  (when (next-method-p)
    (call-next-method))
  (unless (member-of-fslist declaration declarations-list)
    (push-into-fslist declaration declarations-list)))

(defmethod collect-declarations ((declaration c-struct-declaration) declarations-list namespace)
  ;; A struct can refer to itself, so we first reserve a place for the
  ;; struct, we add it's its depended declarations, and then we
  ;; finally add the struct declaration object itself
  (unless (member-of-fslist declaration declarations-list)
    (add-presence-into-fslist declaration declarations-list)
    (dolist (slot (c-slots declaration))
      (collect-declarations slot declarations-list namespace))
    (push-into-fslist declaration declarations-list)))

(defmethod collect-declarations ((obj c-pointer) declarations-list namespace)
  (if (and (eq *compress-types* :all)
	   (subtypep (type-of (c-type obj)) 'c-type-ref))
      (let ((base (unalias-reference (c-type obj) namespace)))
	(unless (primitive-type-p base)
	  (let ((decl (find-declaration base namespace)))
	    (if (c-required decl)
		(progn			; user-required type
		  (setf (c-type obj) base)
		  (collect-declarations (c-type decl) declarations-list namespace))
		(setf (c-type obj) :void)))))
      (call-next-method)))

(defmethod collect-declarations ((obj c-typed-object) declarations-list namespace)
  (when *compress-types*
    (let ((base (unalias-reference (c-type obj) namespace)))
      (setf (c-type obj) base)
      (when (and (eq *compress-types* :all)
		 (subtypep (type-of base) 'c-type-ref))
	(let ((decl (find-declaration base namespace)))
	  (if (and (not (c-required decl))
		   (subtypep (type-of decl) 'c-enum-declaration))
	      (setf (c-type obj) :int))))))
  (collect-declarations (c-type obj) declarations-list namespace)
  (when (next-method-p)
    (call-next-method)))

(defmethod collect-declarations ((declaration c-function) declarations-list namespace)
  (dolist (arg (c-arguments declaration))
    (collect-declarations arg declarations-list namespace))
  (call-next-method))

(defmethod collect-declarations ((declaration c-container) declarations-list namespace)
  (dolist (slot (c-slots declaration))
    (collect-declarations slot declarations-list namespace))
  (when (next-method-p)
    (call-next-method)))

(defmethod collect-declarations ((declaration c-type-ref) declarations-list namespace)
  (aif (find-declaration declaration namespace)
       (collect-declarations it declarations-list namespace)
       (warn "~S not found: C declarations may be incomplete" (c-ref-name declaration))))

(defun complete-declarations (declaration-list namespace)
  (loop
     for decl in declaration-list
     with depended = (make-instance 'fast-searchable-list)
     do (collect-declarations decl depended namespace)
     finally (return (nreverse (get-list depended)))))
