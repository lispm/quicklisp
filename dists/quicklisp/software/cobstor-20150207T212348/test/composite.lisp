;;;  fields.lisp --- complex fields tests

;;;  Copyright (C) 2006, 2008, 2009 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: cobstor

#+cmu (ext:file-comment "$Module: fields.lisp $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

(in-package :cobstor-tests)

(define-cobol-record (sample-record1)
    (field1
     ((field1pairs
	 ((f1p1 :numeric :length 5)
	  (f1p2 :string :length 7))
	 :times 3)
      (field1a
       ((field1a1 :string
		  :length 1)
	(field1a2 :string
		  :length 6
		  :alternates ((field1a2a :string
					  :length 6)
			       (field1a2b :numeric
					  :length 6))))
       :alternates ((field1b ((field1b1 :string
					:length 4)
			      (field1b2 :string
					:length 3))
			     :alternates ((nil
					   ((field1b2a :string
						       :length 3)
					    (field1b2b :string
						       :length 4))))))))))

(defmacro with-sample-rec1 (record &body forms)
  `(let ((,record (make-instance 'sample-record1)))
     (setf (f1p1 ,record 0) 12345
	   (f1p2 ,record 1) 67890
	   (f1p2 ,record 0) "foo"
	   (f1p2 ,record 1) "bar")
     ,@forms))

(deftest composite.1
    (with-sample-rec1 r
      (field1a2 r))
  "")

(deftest composite.2
    (with-sample-rec1 r
      (field1a2b r))
  0)

(deftest composite.3
    (with-sample-rec1 r
      (setf (field1b1 r) "12345")
      (field1a2 r))
  "234")

(deftest composite.4
    (with-sample-rec1 r
      (setf (field1b2 r) "12345")
      (field1a2b r))
  123)

(deftest composite.5
    (with-sample-rec1 r
      (setf (field1b2b r) "12345")
      (field1b2 r))
  "234")

(deftest composite.6
    (with-sample-rec1 r
      (setf (field1a2b r) 123456)
      (field1b2 r))
  "456")

