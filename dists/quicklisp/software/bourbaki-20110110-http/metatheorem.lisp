;;; Copyright (c) Juha Arpiainen 2005-06

;;; This file is part of Bourbaki.

;;; Bourbaki is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.

;;; Bourbaki is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Bourbaki; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; TODO: "memoized" pattern-matching: matcher function is constructed the
;;; first time a metatheorem is called

(in-package :bourbaki)

;;; Metatheorem providing a "virtual namespace"
;;; The defined function takes and returns contexts.
;;; Results are memoized in a hash-table.
;;; When the function is called, *meta-context* will be bound to
;;; the value of *current-context* at the time of function definition
(defmacro virtual-metath (name args &body body)
  (with-gensyms (cache curr fn namesym val found? my-full-name)
    `(let ((,cache (make-hash-table :test 'eq))
           (,namesym ,name)
           (,curr *current-context*))
       (let* ((,my-full-name (cons ,namesym (gethash :full-name (context-meta ,curr))))
              (,fn (labels ((,namesym ,args ,@body))
                     #'(lambda (ctx)
                         (multiple-value-bind (,val ,found?) (gethash ctx ,cache)
                           (if ,found?
                             ,val
                             (let ((*meta-context* ,curr))
                               (let ((res (funcall #',namesym ctx)))
                                 (setf (gethash :full-name (context-meta res))
                                       (cons (context-name res) ,my-full-name)
                                       (gethash ctx ,cache)
                                       res))))))))
              (ref (make-symref :target nil
                                :fn ,fn
                                :args-needed 1
                                :class :simple-meta)))
         (insert-sym ref ,curr ,namesym)))))

;;; General metatheorem or metasymbol accepting any number of arguments
;;; (including &optional, &rest)
;;; *meta-context* will be bound like for simple-metath, but the result
;;; will not be memoized
(defmacro metath (name args &body body)
  (with-gensyms (namesym curr fn)
    `(let ((,namesym ,name)
           (,curr *current-context*))
       (let* ((,fn (labels ((,namesym ,args ,@body))
                     #'(lambda (&rest args)
                         (let ((*meta-context* ,curr))
                           (apply #',namesym (nreverse args))))))
              (ref (make-symref :target nil
                                :fn ,fn
                                :args-needed -1
                                :class :alias ; TODO: change to :meta ?
                                :proof nil)))
         (insert-sym ref ,curr ,namesym)))))

;;; Extend a metatheorem
(defmacro extend-metath (ref args &body body)
  (with-gensyms (curr fn prev temp)
    `(let ((,curr *current-context*)
           (,prev (seek-sym :rel ,ref)))
       (let* ((,fn (labels ((,temp ,args ,@body))
                     #'(lambda (&rest args)
                         (let ((*meta-context* ,curr))
                           ;; try more special version first
                           (aif (ignore-errors (apply #',temp (nreverse args)))
                             it
                             (apply-ref ,prev args))))))
              (ref (make-symref :target nil
                                :fn ,fn
                                :args-needed -1
                                :class :alias
                                :proof nil)))
         (insert-sym ref ,curr ,ref)))))
