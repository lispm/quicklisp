;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: cl-user -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:     package.lisp
;;;; Purpose:  Package definition for UMLisp
;;;; Author:   Kevin M. Rosenberg
;;;; Created:  Apr 2000
;;;;
;;;; This file, part of UMLisp, is
;;;;    Copyright (c) 2000-2004 by Kevin M. Rosenberg, M.D.
;;;;
;;;; UMLisp users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License.
;;;; *************************************************************************

(in-package #:cl-user)

(eval-when (:compile-toplevel :load-toplevel :execute) ;; enclose reader macro
  (defpackage #:umlisp-orf
  (:nicknames #:u1)
  (:use #:kmrcl #:common-lisp #:hyperobject)
  (:export
   #:dummy
   .
   ;; From classes.lisp
   #1=(#:umlsclass
       #:ucon #:uterm #:ustr #:usrl #:uso #:ucxt #:urank #:udef #:usat #:usab #:ulo
       #:urel #:ucoc #:usty #:uatx #:uxw #:uxnw  #:uxns
       #:lexterm #:labr #:lagr #:lcmp #:lmod #:lnom #:lprn #:lprp #:lspl #:ltrm
       #:ltyp #:lwd #:sdef #:sstr #:sstre1 #:sstre2
       #:sty #:tui #:def #:sab #:srl #:tty #:rank #:supres #:atn #:atv #:vcui
       #:rcui #:vsab
       #:rl #:sty2 #:ui #:ui2 #:ui3 #:eui #:bas #:eui2 #:bas2
       #:cui #:lui #:sui #:wd #:lat #:nstr :cuilist
       #:rsab #:lat
       #:s#def #:s#sty #:s#term #:s#str #:s#atx #:s#lo #:s#sat #:s#rel #:s#coc
       #:s#so #:s#cxt
       #:pfstr #:pfstr2 #:lrl #:def #:ts #:cui1 #:cui2 #:rela #:sl #:mg #:rel
       #:soc #:cot #:cof #:coa #:isn #:fr #:un #:sna #:soui #:hcd #:stt #:str

   ;; From class-support.lisp
   #:ucon-has-tui
   #:english-term-p #:remove-non-english-terms #:remove-english-terms
   #:fmt-cui #:fmt-tui #:fmt-sui #:fmt-eui #:fmt-tui
   #:display-con #:display-term #:display-str
   #:pfstr #:pf-ustr
   #:cui-p #:lui-p #:sui-p #:tui-p #:eui-p
   #:rel-abbr-info #:filter-urels-by-rel
   #:ucon-ancestors #:ucon-parents
   #:mesh-number #:cxt-ancestors #:ucon-ustrs
   #:lat-abbr-info #:stt-abbr-info

   ;; From sql.lisp
   #:*umls-sql-db*
   #:umls-sql-user!
   #:umls-sql-passwd!
   #:umls-sql-db!
   #:umls-sql-host!
   #:umls-sql-type!
   #:with-sql-connection
   #:mutex-sql-execute
   #:mutex-sql-query
   #:with-mutex-sql
   #:sql-query
   #:sql-execute

   ;; From utils.lisp
   #:fmt-cui
   #:fmt-lui
   #:fmt-sui
   #:fmt-tui
   #:find-uterm-in-ucon
   #:find-ustr-in-uterm
   #:find-ustr-in-ucon
   #:*current-srl*
   #:parse-cui #:parse-lui #:parse-sui #:parse-tui #:parse-eui

   ;; From sql-classes.lisp

   #:find-udef-cui
   #:find-usty-cui
   #:find-usty-word
   #:find-urel-cui
   #:find-cui2-urel-cui
   #:find-urel-cui2
   #:find-ucon-rel-cui2
   #:find-ucoc-cui
   #:find-ucoc-cui2
   #:find-ucon-coc-cui2
   #:find-usty-sty
   #:find-ulo-cui
   #:suistr
   #:find-uatx-cui
   #:print-umlsclass
   #:find-ucon-cui
   #:find-ucon-cui-sans-pfstr
   #:find-ucon-lui
   #:find-ucon-sui
   #:find-ucon-cuisui
   #:find-ucon-str
   #:find-ucon-all
   #:find-cui-ucon-all
   #:map-ucon-all
   #:find-uterm-cui
   #:find-uterm-lui
   #:find-uterm-cuilui
   #:find-uterm-in-ucon
   #:find-ustr-cuilui
   #:find-ustr-cuisui
   #:find-ustr-sui
   #:find-ustr-sab
   #:find-ustr-all
   #:find-string-sui
   #:find-uso-cuisui
   #:find-ucxt-cuisui
   #:find-usat-ui
   #:find-usab-all
   #:find-usab-rsab
   #:find-usab-vsab
   #:find-pfstr-cui
   #:find-ustr-in-uterm
   #:find-usty-tui
   #:find-usty-all
   #:find-usty_freq-all
   #:find-usrl-all
   #:find-usrl_freq-all
   #:find-cui-max
   #:find-ucon-tui
   #:find-ucon-word
   #:find-ucon-normalized-word
   #:find-cui-normalized-word
   #:find-lui-normalized-word
   #:find-sui-normalized-word
   #:find-ustr-word
   #:find-ustr-normalized-word
   #:find-uterm-multiword
   #:find-uterm-word
   #:find-uterm-normalized-word
   #:find-ucon-multiword
   #:find-ucon-normalized-multiword
   #:find-ustr-multiword
   #:find-ustr-normalized-multiword
   #:find-lexterm-eui
   #:find-lexterm-word
   #:find-labr-eui
   #:find-labr-bas
   #:find-lagr-eui
   #:find-lcmp-eui
   #:find-lmod-eui
   #:find-lnom-eui
   #:find-lprn-eui
   #:find-lprp-eui
   #:find-lspl-eui
   #:find-ltrm-eui
   #:find-ltyp-eui
   #:find-lwd-wrd
   #:find-sdef-ui
   #:find-sstre1-ui
   #:find-sstre1-ui2
   #:find-sstr2-sty
   #:find-sstr-rl
   #:find-sstr-styrl
   #:display-con
   #:display-term
   #:display-str
   #:find-ustats-all
   #:find-ustats-srl
   #:find-bsab-sab
   #:find-bsab-all
   #:find-btty-all
   #:find-btty-tty
   #:find-brel-rel

   ;; composite.lisp
   #:tui-finding
   #:tui-sign-or-symptom
   #:tui-disease-or-syndrome
   #:ucon-is-tui?
   #:find-ucon2-tui
   #:find-ucon2-coc-tui
   #:find-ucon2-rel-tui
   #:find-ucon2_freq-coc-tui
   #:find-ucon2-str&sty
   #:find-ucon2-coc-str&sty
   #:find-ucon2-rel-str&sty
   #:find-ucon2_freq-tui-all
   #:find-ucon2_freq-rel-tui-all
   #:find-ucon2_freq-coc-tui-all
   #:ucon_freq
   #:ustr_freq
   #:usty_freq
   #:usrl_freq
   )))

(defpackage umlisp-orf-user
  (:use  #:kmrcl #:common-lisp #:hyperobject)
  (:import-from :umlisp-orf . #1#)
  (:export . #1#)
  (:documentation "User package for UMLisp")))


