;;; Copyright (c) Juha Arpiainen 2005-06

;;; This file is part of Bourbaki.

;;; Bourbaki is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.

;;; Bourbaki is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Bourbaki; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; -------

;;; *****
;;; Parsing proof and subproofs
;;; *****

(in-package :bourbaki)

(defun parse-subproof (item)
  (typecase item
    ;; A subproof
    (subproof item)
    ;; A symtree: either a direct theorem reference
    ;;            or a wff to seek
    (cons (case (context-class (car item))
            ;; Simple theorem reference
            ((:theorem :axiom :definition) (parse-thr-ref item))
            ;; A wff: try to find a matching theorem
            ;; TODO: move to its own function
            ;; TODO: multilevel seek
            ((:prim :sym)
;             (format t "Seek: ~a~%" item)
             (aif (match-tree *theorem-patterns* item)
               (dolist (thref it (error (format nil "No matching theorem found for ~A" item)))
;                 (format t "Seek: Trying ~a~%" thref)
                 ;; for each candidate, check hypotheses and DVCs
                 (let ((smap (smap (car thref) (cdr thref)))
                       (varlist (context-vars *current-context*))
                       (dist (context-distinct *current-context*)))
                   (let ((step (make-subproof :ref thref
                                                :hypo (mapcar #'(lambda (x) (replace-vars x smap))
                                                              (context-hypo (car thref)))
                                                :assert (list item)
                                                :sub nil)))
                     (if (ignore-errors (verify-subproof step varlist dist nil))
                       (return-from parse-subproof step)))))
               (error (format nil "No matching theorem found for ~A" item))))
            (otherwise (error (format nil "Unexpected object on a proof line: ~A" item)))))
    ;; Rpn proof line
    ((or symref context)
     (let ((ref (mkcontext item))
           (smap nil))
       (loop for j in *current-proof*
             for i in (context-hypo ref)
             do (setf smap (unify-line i j smap)))
       (parse-subproof (cons ref (mapcar #'(lambda (x) (cdr (assoc x smap)))
                                           (context-vars ref))))))
    (otherwise (error (format nil "Unexpected object ~S at proof line." item)))))

(defmacro subproof ((&key hypo assert) &body body)
  (with-gensyms (res item tail)
    `(let ((,res (make-subproof :ref nil
                                :hypo ,hypo
                                :assert ,assert
                                :sub nil)))
       (let ((*current-proof* (cons (make-pattern-tree) *current-proof*))
             (,tail nil))
         (dolist (h (subproof-hypo ,res))
           (insert-wff (car *current-proof*) h))
         (macrolet ((line (thing)
                      `(let ((,',item (parse-subproof ,thing)))
                         (dolist (a (subproof-assert ,',item))
                           (insert-wff (car *current-proof*) a))
                         (if ,',tail
                           (setf (cdr ,',tail) (setf ,',tail (list ,',item)))
                           (setf ,',tail (setf (subproof-sub ,',res) (list ,',item))))
                         ,',item))
                    (hypo (thing)
                      `(let ((,',item ,thing))
                         (push ,',item (subproof-hypo ,',res))
                         (insert-wff (car *current-proof*) ,',item)
                         ,',item))
                    (ass (thing)
                      `(let ((,',item ,thing))
                         (push ,',item (subproof-assert ,',res))
                         ,',item)))
           ,@body)
         ,res))))

(defmacro linear-subproof (hyp ass &body lines)
  `(subproof (:hypo (list ,@hyp)
              :assert (list ,@ass))
     ,@(loop for i in lines
             collect `(line ,i))))

(defmacro proof (&body lines)
  `(setf (context-proof *current-context*)
     (subproof (:hypo (context-hypo *current-context*)
                :assert (context-assert *current-context*))
       ,@(loop for i in lines
               collect `(line ,i)))))
