# hu.dwim.bluez
A Common Lisp [FFI](https://en.wikipedia.org/wiki/Foreign_function_interface)
for [Bluez](http://www.bluez.org/) (aka libbluetooth), which is a Linux Bluetooth
stack.

## Bugs, contributions
Written by [attila@lendvai.name](mailto:attila@lendvai.name). The primary
communication channel is the facilities on
[the project's GitHub page](https://github.com/attila-lendvai/hu.dwim.bluez).
