;;; Copyright (c) Juha Arpiainen 2005-06

;;; This file is part of Bourbaki.

;;; Bourbaki is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.

;;; Bourbaki is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Bourbaki; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; *****
;;; Symbol trees
;;; *****

(in-package :bourbaki)

;;; Functions related to symtrees:
;;; - parsing
;;; - comparing
;;; - replacing variables
;;; - checking for well-formedness and type

;;; Symbols are stored in trees, example:
;;; (-> (x) (-> (y) (x)))

;;; terminology: any tree (possibly nil) containing only symbols as leafs is a /symtree/ "symbol tree"
;;; a /wff/ "well formed formula" is a symtree of the form (op arg_1 ... arg_n) where each arg_i is
;;; a wff of correct type

(defun wff-type (str)
  "Type of a symtree"
  (cond ((null str) nil)
        ((typep str 'context) (context-type str))
        ((listp str) (wff-type (car str)))
        (t nil)))

(defun wffp (str)
  "Check if str is a well-formed symtree"
  (and (consp str)
       (typep (car str) 'context)
       (= (length (context-vars (car str)))
          (length (cdr str)))
       (loop for i in (reverse (context-vars (car str)))
             for j in (cdr str)
             do (unless (and (wffp j)
                             (subkindp (wff-type j) (context-type i)))
                  (return nil))
             finally (return t))))

;;; test if lhs and rhs are equal symtrees. This means their list structure is "equal"
;;; and the actual symbols are "eq"
;;; NOTE: "sym" is _not_ equal to "(sym)"
(defun wff-equal (lhs rhs)
  "Check if two symtrees are equal"
  (tree-equal lhs rhs :test #'eq))

(defun collect-vars (str varlist)
  "The list of symbols from varlist that occur in str"
  (cond ((null str) nil)
        ((typep str 'context) (if (member str varlist :test #'eq) (cons str nil)))
        (t (union (collect-vars (car str) varlist) (collect-vars (cdr str) varlist) :test #'eq))))

;;; For checking soundness of definitions
(defun potential-vars (lst dist expr)
  "List of variables that potentially occur in a symtree"
  (let ((sym (car expr)))
    (if (eq (context-class sym) :arg)
      ;; If the expression is a single variable return all vars not
      ;; distinct from it
      (let ((res (remove-if #'(lambda (x) (member (cons x sym) dist :test #'pair-eq)) (copy-list lst)))) res)
      ;; Otherwise remove anything substituted for a :bound variable ...
      (let* ((bound (loop for i in (reverse (context-vars sym))
                          for j in (cdr expr)
                          ;; XXX this only works if the thing substituted is a single variable
                          if (gethash :bound (context-meta i))
                            collect (car j)))
             (free? (set-difference lst bound :test #'eq)))
        ;; ... and test all subexpression for remaining candidates
        (remove-duplicates (mapcan #'(lambda (x) (potential-vars free? dist x))
                                   (cdr expr))
                           :test #'eq)))))

;;; Substitute variables in a wff.
;;; smap is a list of pairs (var . subst)
;;; where subst is a wff.
(defun replace-vars (str smap)
  "Substitute some variables in a wff"
  (symtree-reparse (sublis smap str :test #'eq)))

(defun replace-proof-vars (pf smap)
  "Substitute some variables in a proof"
  (flet ((repl (x) (replace-vars x smap)))
    (make-subproof :ref (repl (subproof-ref pf))
                   :hypo   (mapcar #'repl (subproof-hypo pf))
                   :assert (mapcar #'repl (subproof-assert pf))
                   :prev nil
                   :sub (mapcar #'(lambda (x) (replace-proof-vars x smap))
                                (subproof-sub pf)))))

(defun symtree-reparse-aux (str)
  "Helper function for replace-vars"
  (loop for arg on (cdr str)
        do (if (listp (caar arg))
             (setf (car arg) (caar arg))
             (symtree-reparse-aux (car arg)))))

;;; Make the result of variable substitution a wff
;;; The tree is destructively modified; this should
;;; only be called from replace-vars
(defun symtree-reparse (str)
  "Helper function for replace-vars"
  (symtree-reparse-aux str)
  (if (listp (car str)) (car str) str))

;;; for use in "metatheorems" in converting theorem variable list to
;;; a proof line
(defun makevarlist (thr)
  (nreverse (mapcar #'(lambda (x) (cons x nil))
                    (context-vars thr))))

(defun makevarlist-r (thr)
  (mapcar #'(lambda (x) (cons x nil))
          (context-vars thr)))

;(defun depend-syms (str)
;  (cond ((null str) nil)
;        ((typep str 'context) (if (member (context-class str) '(:arg :loc)) nil (cons str nil)))
;        (t (union (depend-syms (car str)) (depend-syms (cdr str)) :test #'eq))))

(defun smap (thr expr)
  (pairlis (reverse (context-vars thr)) expr))
