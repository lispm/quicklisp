
(in-package :bitfield-schema)

(defun power-of-2-p (number)
  (zerop (logand number (1- number))))

(defun gen-add (&rest forms)
  (iter (for form in forms)
        (unless (eql form 0)
          (collect form into filtered-forms))
        (finally
         (return
           (cond ((null filtered-forms) 0)
                 ((= (length filtered-forms) 1) (car filtered-forms))
                 (t (cons '+ filtered-forms)))))))

(defun gen-multiply (form multiplier)
  (if (numberp multiplier)
      (let ((cache (gensym "CACHE")))
        (flet ((caching (sexp) `(let ((,cache ,form)) ,sexp)))
          (cond ((= multiplier 0) 0)
                ((= multiplier 1) form)
                ((= multiplier 3) (caching `(+ ,cache ,cache ,cache)))
                ((power-of-2-p multiplier)
                 `(ash ,form ,(1- (integer-length multiplier))))
                ((and (> multiplier 5) (power-of-2-p (1- multiplier)))
                 (caching `(+ ,(gen-multiply cache (1- multiplier)) ,cache)))
                ((and (> multiplier 5) (power-of-2-p (1+ multiplier)))
                 (caching `(- ,(gen-multiply cache (1+ multiplier)) ,cache)))
                (t `(* ,form ,multiplier)))))
      (list '* form multiplier)))

(defun gen-quasiquote (sexp)
  (cond ((listp sexp)
         (if (eq (car sexp) :keep)
             (second sexp)
             (cons 'list (mapcar #'gen-quasiquote sexp))))
        ((symbolp sexp) `(quote ,sexp))
        (t sexp)))
              
(defun gen-noquote (sexp)
  (if (listp sexp)
      (if (eq (car sexp) :keep)
          (gen-noquote (second sexp))
          (mapcar #'gen-noquote sexp))
      sexp))

