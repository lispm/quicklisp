;;; string-escape.asd -- allow (require '#:string-escape) and (ql:quickload '#:string-escape)
;;; Copyright (c) 2014-2015 Devon Sean McCullough

(asdf:defsystem #:string-escape
  :description "Emacs and Python style string escapes in #\"...\" form."
  :author "Devon at ai.NO.mit.SPAM.edu"
  :license "GPLv3"
  :version "0.0"
  :components ((:file "string-escape")))

;;; string-escape.asd end