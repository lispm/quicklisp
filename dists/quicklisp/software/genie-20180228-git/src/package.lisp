(in-package :cl-user)

(defpackage #:genie
  (:use #:cl)
  (:export #:make-seed
           #:make-generator
           #:gen))
