# DO NOT EDIT --- generated from source code.
#+LaTeX: \label{def-form-criterion:primary}
The =def-form-criterion= macro was deprecated as of NST 1.3.0. /Code using
=def-form-criterion= in any but the simplest ways is very likely to fail./ Use
=def-criterion= instead.

