(in-package :simple-logger)

(defvar *log-levels* '(:trace :debug :info :warn :error :fatal))
(defvar *current-level* :info
  "The logging level to use. Can be one of: :TRACE, :DEBUG, :INFO, :WARN, :ERROR, :FATAL")

(defun message-timestamp ()
  "The current timestamp string formatted for display in a message."
  (let ((fmt '("["
               (:year 4) "-"
               (:month 2) "-"
               (:day 2) " "
               (:hour 2) ":"
               (:min 2) ":"
               (:sec 2)
               "]")))
    (local-time:format-timestring nil (local-time:now) :format fmt)))

(defgeneric emit (name &rest vars)
  (:method (name &rest vars)
    (declare (ignore vars)))
  (:documentation "Emit the message defined with the given name."))

(defmacro define-message (level name message)
  "Macro for defining a message to be emitted later."
  (alexandria:with-gensyms (index vars)
    (alexandria:once-only (level name message)
      `(defmethod emit ((,name (eql ,name)) &rest ,vars)
         (alexandria:when-let ((,index (position *current-level* *log-levels*)))
           (when (>= (position ,level *log-levels*) ,index)
             (apply #'format t
                    (concatenate 'string "[~5A] ~A " ,message "~%")
                    ,level (message-timestamp) ,vars)))))))
