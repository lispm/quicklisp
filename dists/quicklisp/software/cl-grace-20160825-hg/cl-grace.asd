;;; -*- Mode: Lisp; Package: CL-User -*-
;;;
;;; Copyright (c) 2009 - 2016 Paul Onions
;;; See LICENCE file for licence information.
;;;
;;; ASDF system definition for the CL-GRACE system.
;;;
(in-package :cl-user)

(asdf:defsystem :cl-grace
  :author "Paul Onions"
  :version "0.14.0"
  :licence "MIT"
  :description "CL-GRACE: an interface to the Grace 2D plotting program."
  :depends-on (:cl-fad)
  :components ((:file "packages")
               (:file "utils" :depends-on ("packages"))
               (:file "grace-interface" :depends-on ("packages" "utils"))
               (:file "grace-plot" :depends-on ("packages" "utils" "grace-interface"))))
