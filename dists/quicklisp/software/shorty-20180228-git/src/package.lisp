(in-package :cl-user)

(defpackage #:shorty
  (:use #:cl)
  (:export #:shorten-file
           #:unshorten-file))
