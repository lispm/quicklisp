;;; Copyright (c) Juha Arpiainen 2005-06

;;; This file is part of Bourbaki.

;;; Bourbaki is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.

;;; Bourbaki is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Bourbaki; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; -------

;;; Reader macros for symtrees, theorem references, misc.

(in-package :bourbaki)

(defun bsym-reader (stream alt)
  (peek-char t stream) ; Skip whitespace

  (case (if alt nil (peek-char nil stream t nil t))
    (#\? (list 'quote (read stream t nil t)))
    (#\( (read stream t nil t))
    (#\, (read-char stream t nil t) (read stream t nil t))
    (#\! (thref-reader stream (read-char stream t nil t)))
    (#\[ (symstr-reader stream (read-char stream t nil t)))
    (otherwise
      (let ((name ""))
        (do ((chr (read-char stream t nil t) (read-char stream t nil t)))
          ((or (find chr "] ") (not (graphic-char-p chr)))
           (if (char= chr #\]) (unread-char chr stream)))
          (setq name (concatenate 'string name (list chr))))
        (if (string= name "") nil
          (if alt name
            (list 'seek-sym :rel name)))))))
  
;; read a sequence of bsyms into a "flat" symstr
(defun symstr-reader-aux (stream alt)
  (loop for token = (bsym-reader stream alt) then (bsym-reader stream alt)
        until (null token)
        collect token into tree
        finally (progn
                  ;; Eat the terminating ']'
                  (peek-char #\] stream t nil t) (read-char stream t nil t)
                  (return (if alt
                            tree
                            (list 'symtree-parse (cons 'list tree)))))))

(defun symstr-reader (stream char)
  (declare (ignore char))
  (symstr-reader-aux stream nil))

(defun read-segment (stream)
  (if (char= (peek-char nil stream t nil t) #\")
    (prog1
      (read stream t nil t) ; Read a quoted string with Lisp syntax
      (if (char= (peek-char nil stream t nil t) #\!) (read-char stream t nil t)))
    (do ((name "" (concatenate 'string name (list chr)))
         (chr (read-char stream t nil t) (read-char stream t nil t)))
        ((case chr
           ((#\( #\) #\[ #\] #\Space) (unread-char chr stream) t)
           (#\! t)
           (otherwise (unless (graphic-char-p chr)
                        (unread-char chr stream) t)))
         name))))

(defun read-names (stream)
  (read-char stream t nil t)
  (symstr-reader-aux stream t))

;; Read a theorem reference
(defun thref-reader (stream char)
  (declare (ignore char))
  (let ((names nil) (mode :rel))
    
    (case (peek-char nil stream t nil t) ; !! means absolute path search
      (#\! (read-char stream t nil t)
       (let ((module (read-segment stream)))
         (return-from thref-reader (list 'require module module))))
;      (#\@ (read-char stream t nil t)
;           (let ((int (read stream t nil t)))
;             (return-from thref-reader `(nth ,(1- int) (subproof-prev *current-proof*)))))
      (#\/ (read-char stream t nil t) (setf mode :abs))
      (#\[ (return-from thref-reader (read-names stream))))
    
    (do ((name (read-segment stream) (read-segment stream)))
        ((string= name ""))
      (push name names))
    (apply #' list 'seek-sym mode (nreverse names))))

(set-macro-character #\[ #'symstr-reader)
(set-macro-character #\! #'thref-reader)
(set-syntax-from-char #\] #\))
