CL-ENUMERATIONS
===============

Marco Antoniotti (c) 2004-2018
------------------------------

The directory containing this file you are reading should contain the
code and the documentation of the CL-ENUMERATIONS package.

The package is a rendition of the well known Java Enumeration/Iterator
interfaces, and it is provided as help to Java, C++, Python and other
languages programmers migrating to Common Lisp.

It is also hoped that Common Lisp vendors will adopt the interface and
provided specialized and efficient implementations of some of the
package components.


A NOTE ON FORKING
-----------------

Of course you are free to fork the project subject to the current
licensing scheme.  However, before you do so, I ask you to consider
plain old "cooperation" by asking me to become a developer.
It helps keeping the entropy level at an acceptable level.


Enjoy.
