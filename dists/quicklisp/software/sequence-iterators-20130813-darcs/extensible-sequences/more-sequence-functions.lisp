;;;; -*- Mode: Lisp; indent-tabs-mode: nil -*-

(in-package :tcr.extensible-sequences)

(defun map-into-subseq (sequence function &key start end from-end key)
  (check-sequence-bounds sequence start end)
  (let ((key (canonicalize-key key)))
    (dosequence (elt sequence sequence :start start :end end
                                       :from-end from-end
                                       :place ptr)
      (setf (ptr) (funcall function (funcall key elt))))))

(defun split-sequence (item sequence &key (start 0) end from-end key
                                          test test-not)
  (check-sequence-bounds sequence start end)
  (let ((key  (canonicalize-key key))
        (test (canonicalize-test test test-not))
        (prev-idx (1- start))
        (chunks))
    (dosequence ((elt idx) sequence nil :start start :end end
                                        :from-end from-end)
      (when (funcall test item (funcall key elt))
        (push (subseq sequence (1+ prev-idx) idx) chunks)
        (setq prev-idx idx)))
    (when (< (1+ prev-idx) end)         ; push last chunk.
      (push (subseq sequence (1+ prev-idx) end) chunks))
    (nreverse chunks)))

(declaim (inline coerce-subseq))
(defun coerce-subseq (sequence start end &optional result-type)
  
  (if (null result-type)
      (subseq sequence start end)
      (let ((end (or end (length sequence))))
        (cl:replace (make-sequence result-type (- end start)) sequence))))

(defun take-while (predicate sequence &key key (start 0) end from-end result-type)
  (let ((pos (position-if-not predicate sequence
                              :key key :start start :end end
                              :from-end from-end)))
    (coerce-subseq sequence
                   (if from-end (if pos (1+ pos) start) start)
                   (if from-end end                     (or pos end))
                   result-type)))

(defun drop-while (predicate sequence &key key (start 0) end from-end result-type)
  
  (let* ((pos (position-if-not predicate sequence
                               :key key :start start :end end
                               :from-end from-end)))
    (if (not pos)
        (coerce-subseq sequence 0 0 result-type)
        (coerce-subseq sequence
                       (if from-end start    pos)
                       (if from-end (1+ pos) end)
                       result-type))))



;;;
;;; (PARTITION P S) == (VALUES (REMOVE-IF-NOT P S) (REMOVE-IF P S))
;;;

#+nil
(defun npartition (predicate sequence &key key (start 0) end &aux length)
  (check-sequence-bounds sequence start end length)
  (cond ((listp sequence)
         )
        (t
         (let ((key (if key (ensure-function key) #'identity))
               (positions '())
               (count 0))
           (declare (sequence-length count))
           (dosequence ((elt idx) sequence nil :start start :end end)
             (when (funcall predicate (funcall key elt))
               (incf count)
               (push idx positions)))
           (format t "COUNT = ~S, POSITIONS = ~S~%" count positions)
           (let ((result-yes (make-sequence-like sequence count))
                 (result-no  (make-sequence-like sequence (- length count))))
             (with-sequence-iterator (advance-yes result-yes :place yes-ptr)
               (with-sequence-iterator (advance-no result-no :place no-ptr)
                 (dosequence ((elt idx) sequence nil :start start :end end)
                   (let ((pos (pop positions)))
                     (if (or (null pos) (/= pos idx))
                         (progn (setf (no-ptr)  elt) (advance-no))
                         (progn (setf (yes-ptr) elt) (advance-yes)))))))
             (values result-yes
                     result-no))))))


;;;
;;; (GROUP 'LIST "Mississippi") ==> ("M" "i" "ss" "i" "ss" "i" "pp" "i")
;;;

;; (defun group (result-type sequence &key key test test-not start end from-end)
;;   ;; TODO
;;   )


;;;
;;; (ORDERED-INSERT 3 '(0 1 2 4 5)) ==> (0 1 2 3 4 5)
;;;

;; (defun ordered-insert (item sequence &key key test test-not start end from-end)
;;   ;; TODO
;;   )


;;;
;;; REPLACE-MATCH
;;;

;; (defun replace-match (dst src &key start1 end1 start2 end2 from-end)
;;   ;; TODO
;;   )


;;;
;;; REPLACE-ALL-MATCHES
;;;

;; (defun replace-all-matches (sequence match &key start1 end1 start2 end2 count from-end)
;;   ;; TODO
;;   )

