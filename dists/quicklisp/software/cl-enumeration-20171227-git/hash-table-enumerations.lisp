;;;; -*- Mode: Lisp -*-

;;;; hash-table-enumerations.lisp --
;;;;
;;;; See file COPYING for copyright and licensing information.

(in-package "CL.EXTENSIONS.DACF.ENUMERATIONS")


;;;;===========================================================================
;;;; Hash Table Enumerations.

(defclass hash-table-enumeration (enumeration)
  ((keysp :reader keysp :initarg :keys)
   (valuesp :reader valuesp :initarg :values)
   (kv-pairs-p :reader key-value-pairs-p :initarg :key-value-pairs)
   (underlying-enumeration :accessor underlying-enumeration)
   )
  (:documentation "The Hash Table Enumeration Class.")
  (:default-initargs :keys nil :values t :key-value-pairs nil))


(defgeneric hash-table-enumeration-p (x)
  (:method ((x t)) nil)
  (:method ((x hash-table-enumeration)) t))


(defmethod initialize-instance :after ((x hash-table-enumeration)
				       &key
				       keys
				       (values t)
				       (key-value-pairs nil kvp-supplied-p)
				       &allow-other-keys)
  (let ((ht (enumeration-object x)))
    (when (and keys values kvp-supplied-p (not key-value-pairs))
      (cerror "Keys and values will be collected."
              "The enumeration of ~S was asked to supply keys and values, but not key-value pairs.  This is inconsistent."
              ht))
    (setf (underlying-enumeration x)
	  (enumerate (cond ((or (and keys values) key-value-pairs)
			    (loop for k being the hash-key of ht
				  using (hash-value v)
				  when key-value-pairs collect (cons k v)))
                           (keys
			    (loop for k being the hash-key of ht
				  collect k))
			   (values
			    (loop for v being the hash-value of ht
				  collect v)))
                     ))))



(defmethod enumerate ((x hash-table)
		      &key
                      start
                      end
		      keys
                      (values t)
                      key-value-pairs
                      &allow-other-keys)
  "Calling ENUMERATE on a HASH-TABLE returns a HASH-TABLE-ENUMERATION instance.

If KEYS is true, the HASH-TABLE-ENUMERATION scans the keys of the
underlying HASH-TABLE.  If VALUES is true (the default), the
HASH-TABLE-ENUMERATION scans the values of the underlying HASH-TABLE.
If KEY-VALUE-PAIRS is true, then the HASH-TABLE-ENUMERATION yields
key-values dotted pairs.

Note that it makes no sense to set \"bounds\" on a
HASH-TABLE-ENUMERATION, as an HASH-TABLE is an unordered data
structure.  START and END are ignored."
  (declare (ignore start end))
  (make-instance 'hash-table-enumeration
		 :object x
		 :keys keys
		 :values values
		 :key-value-pairs key-value-pairs))


(defmethod has-more-elements-p ((x hash-table-enumeration))
  (has-more-elements-p (underlying-enumeration x)))


(defmethod has-next-p ((x hash-table-enumeration))
  (has-next-p (underlying-enumeration x)))


(defmethod next ((x hash-table-enumeration) &optional default)
  (declare (ignore default))
  (next (underlying-enumeration x)))


(defmethod current ((x hash-table-enumeration) &optional (errorp t) (default nil))
  (cond ((and errorp (not (has-next-p x)))
         (error 'no-such-element :enumeration x))
        ((and (not errorp) (not (has-next-p x)))
         default)
        (t (current (underlying-enumeration x)))))


(defmethod reset ((x hash-table-enumeration))
  (reset (underlying-enumeration x)))


;;; Extra hash-table utilities.

(defun enumerate-keys (ht)
  (declare (type hash-table ht))
  (enumerate ht :keys t :values nil :key-value-pairs nil))


(defun enumerate-values (ht)
  (declare (type hash-table ht))
  (enumerate ht :keys nil :values t :key-value-pairs nil))


(defun enumerate-key-value-pairs (ht)
  (declare (type hash-table ht))
  (enumerate ht :keys nil :values nil :key-value-pairs t))


;;;; end of file -- hash-table-enumerations.lisp --
