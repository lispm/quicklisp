;;; -*- mode: Lisp; Syntax: Common-Lisp; -*-
;;;
;;; Copyright (c) 2009 by the authors.
;;;
;;; See LICENCE for details.

(in-package :hu.dwim.presentation)

;;;;;;
;;; layer/mixin

(def (component e) layer/mixin ()
  ((layer (contextl:with-inactive-layers (backend-layer)
            (current-layer)))))

(def component-environment layer/mixin
  (funcall-with-layer-context (adjoin-layer (layer-of -self-) (current-layer-context)) #'call-next-method))

(def (function ie) current-layer ()
  (contextl::layer-context-prototype (current-layer-context)))
