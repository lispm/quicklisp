#!/bin/bash -e

dup reversi -Ufiles.kpe.io -D/home/ftp/reversi -su \
    -C"(umask 022; cd /srv/www/html/reversi; make install; find . -type d |xargs chmod go+rx; find . -type f | xargs chmod go+r)" $*
