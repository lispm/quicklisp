;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          hyperobject.asd
;;;; Purpose:       ASDF system definition for hyperobject package
;;;; Author:        Kevin M. Rosenberg
;;;; Date Started:  Apr 2000
;;;;
;;;; $Id$
;;;; *************************************************************************

(defpackage hyperobject-system (:use #:asdf #:cl))
(in-package :hyperobject-system)

(defsystem hyperobject
  :name "hyperobject"
  :author "Kevin M. Rosenberg <kevin@rosenberg.net>"
  :version "2.7.x"
  :maintainer "Kevin M. Rosenberg <kmr@debian.org>"
  :licence "BSD-like License"

  :components
  ((:file "package")
   (:file "metaclass" :depends-on ("package"))
   (:file "mop" :depends-on ("metaclass"))
   (:file "rules" :depends-on ("mop"))
   (:file "connect" :depends-on ("mop"))
   (:file "sql" :depends-on ("connect"))
   (:file "views" :depends-on ("mop"))
   (:file "base-class" :depends-on ("views" "sql" "rules"))
   )
  :depends-on (:kmrcl :clsql))

(defmethod perform ((o test-op) (c (eql (find-system :hyperobject))))
  (operate 'load-op 'hyperobject-tests)
  (operate 'test-op 'hyperobject-tests :force t))
