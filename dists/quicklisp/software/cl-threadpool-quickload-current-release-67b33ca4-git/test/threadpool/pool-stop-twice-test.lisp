
(in-package :cl-threadpool-test)

(define-test pool-stop-twice-test-1 ()
  "Nested pool stopping (this is allowed)."
  (let ((pool (cl-threadpool:make-threadpool 5)))
    (cl-threadpool:start pool)
    (cl-threadpool:add-job
     pool
     (lambda ()))
    (cl-threadpool:stop pool)
    (let ((got-error nil))
      (handler-case 
	  (cl-threadpool:stop pool)
	(error (err) (setf got-error err)))
      (assert-false got-error))))

