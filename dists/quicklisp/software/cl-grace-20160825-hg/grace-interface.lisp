;;; -*- Mode: Lisp; Package: CL-Grace -*-
;;;
;;; Copyright (c) 2009 - 2016 Paul Onions
;;; See LICENCE file for licence information.
;;;
;;; The CL-GRACE system
;;;
(in-package :cl-grace)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Conditions
;;;
(define-condition grace-error (error)
  ((text :initarg :text :reader grace-error-text)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Utility functions
;;;
(defun on-or-off (val)
  (if (or (eql val t) (equalp (string val) "on")) :on :off))

(defparameter *xytype-names* '(:XY :BAR :BARDY :BARDYDY :XYZ :XYDX :XYDY :XYDXDX
                               :XYDYDY :XYDXDY :XYDXDXDYDY :XYHILO :XYR :XYSIZE
                               :XYCOLOR :XYCOLPAT :XYVMAP :XYBOXPLOT :XYSTRING))

(defun xytype-name (val)
  (let ((sym (typecase val
               (number
                (elt *xytype-names* val))
               (symbol
                (find val *xytype-names*))
               (string
                (find (intern (string-upcase val) :keyword) *xytype-names*)))))
    (unless sym
      (error 'grace-error :text (format nil "Unknown XYTYPE: ~A" sym)))
    (symbol-name sym)))

(defparameter *format-names*
  '(:DECIMAL :EXPONENTIAL :GENERAL :SCIENTIFIC :ENGINEERING :COMPUTING :POWER
    :DDMMYY :MMDDYY :YYMMDD :MMYY :MMDD :MONTHDAY :DAYMONTH :MONTHS :MONTHSY
    :MONTHL :DAYOFWEEKS :DAYOFWEEKL :DAYOFYEAR :HMS :MMDDHMS :MMDDYYHMS
    :YYMMDDHMS :DEGREESLON :DEGREESMMLON :DEGREESMMSSLON :MMSSLON :DEGREESLAT
    :DEGREESMMLAT :DEGREESMMSSLAT :MMSSLAT))

(defun format-name (val)
  (let ((sym (typecase val
               (number
                (elt *format-names* val))
               (symbol
                (find val *format-names*))
               (string
                (find (intern (string-upcase val) :keyword) *format-names*)))))
    (unless sym
      (error 'grace-error :text (format nil "Unknown FORMAT: ~A" sym)))
    (symbol-name sym)))

(defparameter *color-names*
  '(("white" . 0) ("black" . 1) ("red" . 2) ("green" . 3)
    ("blue" . 4) ("yellow" . 5) ("brown" . 6) ("grey" . 7)
    ("violet" . 8) ("cyan" . 9) ("magenta" . 10) ("orange" . 11)
    ("indigo" . 12) ("maroon" . 13) ("turquoise" . 14) ("green4" . 15)))

(defun color-string (val)
  (typecase val
    (number
     (format nil "~A" (round (abs (realpart val)))))
    ((or cons vector)
     (format nil "(~A,~A,~A)" (elt val 0) (elt val 1) (elt val 2)))
    ((or symbol string)
     (let ((itm (assoc (string val) *color-names* :test #'equalp)))
       (if itm (format nil "~A" (cdr itm)) "1")))
    (t "1")))

(defparameter *shape-names*
  '(("none" . 0) ("circle" . 1) ("square" . 2) ("diamond" . 3) ("triangle-up" . 4)
    ("triangle-left" . 5) ("triangle-down" . 6) ("triangle-right" . 7)
    ("plus" . 8) ("x" . 9) ("star" . 10) ("char" . 11)))

(defun shape-type (val)
  (typecase val
    (number
     (values (round (abs (realpart val)))))
    ((or symbol string)
     (let ((itm (assoc (string val) *shape-names* :test #'equalp)))
       (if itm (cdr itm) 0)))
    (t 0)))

(defparameter *line-types*
  '(("none" . 0) ("straight" . 1) ("left-stairs" . 2) ("right-stairs" . 3)
    ("segments" . 4) ("3-segments" . 5)))

(defun line-type (val)
  (typecase val
    (number
     (values (round (abs (realpart val)))))
    ((or symbol string)
     (let ((itm (assoc (string val) *line-types* :test #'equalp)))
       (if itm (cdr itm) 0)))
    (t 0)))

(defparameter *line-styles*
  '(("none" . 0) ("solid" . 1) ("dot" . 2) ("dash" . 3)
    ("longdash" . 4) ("dot-dash" . 5) ("dot-longdash" . 6)
    ("dot-dash-dot" . 7) ("dash-dot-dash" . 8)))

(defun line-style (val)
  (typecase val
    (number
     (values (round (abs (realpart val)))))
    ((or symbol string)
     (let ((itm (assoc (string val) *line-styles* :test #'equalp)))
       (if itm (cdr itm) 0)))
    (t 0)))

(defparameter *fill-types*
  '(("none" . 0) ("as-polygon" . 1) ("to-baseline" . 2)))

(defun fill-type (val)
  (typecase val
    (number
     (values (round (abs (realpart val)))))
    ((or symbol string)
     (let ((itm (assoc (string val) *fill-types* :test #'equalp)))
       (if itm (cdr itm) 0)))
    (t 0)))

(defparameter *fill-rules*
  '(("winding" . 0) ("even-odd" . 1)))

(defun fill-rule (val)
  (typecase val
    (number
     (values (round (abs (realpart val)))))
    ((or symbol string)
     (let ((itm (assoc (string val) *fill-rules* :test #'equalp)))
       (if itm (cdr itm) 0)))
    (t 0)))

(defparameter *baseline-types*
  '(("zero" . 0) ("set-min" . 1) ("set-max" . 2) ("graph-min" . 3)
    ("graph-max" . 4) ("set-average" . 5)))

(defun baseline-type (val)
  (typecase val
    (number
     (values (round (abs (realpart val)))))
    ((or symbol string)
     (let ((itm (assoc (string val) *baseline-types* :test #'equalp)))
       (if itm (cdr itm) 0)))
    (t 0)))

(defun char-integer (val)
  (typecase val
    (number
     (format nil "~A" (round (abs (realpart val)))))
    (character
     (char-code val))
    (t 65)))

(defun font-string (val)
  (typecase val
    (number
     (format nil "~A" (round (abs (realpart val)))))
    (string
     (format nil "~S" val))
    (symbol
     (format nil "~S" (string-capitalize (symbol-name val))))
    (t "0")))

(defparameter *annotation-types*
  '(("none" . 0) ("x" . 1) ("y" . 2) ("xy" . 3)
    ("string" . 4) ("z" . 5)))

(defun annotation-type (val)
  (typecase val
    (number
     (values (round (abs (realpart val)))))
    ((or symbol string)
     (let ((itm (assoc (string val) *annotation-types* :test #'equalp)))
       (if itm (cdr itm) 0)))
    (t 0)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Low-level API
;;;
(defvar *grace-program* "xmgrace"
  "Name of Grace executable to launch.")

(defvar *grace-args* nil
  "List of strings to be supplied to the Grace executable as arguments.")

(defclass plot ()
  ()
  (:documentation "A base class for plots."))

(defclass grace-plot (plot)
  ((grace-program :initarg  :grace-program :accessor grace-program)
   (grace-args    :initarg  :grace-args    :accessor grace-args)
   (grace-process :initform nil            :accessor grace-process)
   (grace-stream  :initform nil            :accessor grace-stream))
  (:documentation "Encapsulate a grace process object."))

(defmethod initialize-instance :after ((plot grace-plot) &key pipe
                                                           (grace-program *grace-program*)
                                                           (grace-args *grace-args*)
                                                           &allow-other-keys)
  "Initialize a GRACE-PLOT instance.
If PIPE is NIL then start an instance of the grace program in a new
process, using GRACE-PROGRAM and GRACE-ARGS.  Otherwise, if PIPE is
non-NIL, it should either be a stream object or the name of a
filesystem pipe.  In either case, do not start an instance of grace,
but simply pass all commands to the stream so denoted."
  (etypecase pipe
    (null
     (let* ((proc
             #+clozure (ccl:run-program grace-program
                                        (list* "-dpipe" "0" grace-args)
                                        :input :stream :output nil
                                        :error nil :wait nil)
             #+sbcl (sb-ext:run-program grace-program
                                        (list* "-dpipe" "0" grace-args)
                                        :input :stream :output nil
                                        :error nil :wait nil :search t))
            (strm
             #+clozure (ccl:external-process-input-stream proc)
             #+sbcl    (sb-ext:process-input proc)))
       (setf (slot-value plot 'grace-process) proc)
       (setf (slot-value plot 'grace-stream) strm)))
    (stream
     (setf (slot-value plot 'grace-stream) pipe))
    (string
     (setf (slot-value plot 'grace-stream)
           (open pipe :direction :output :if-exists :append :if-does-not-exist :error)))))

(defun new-plot (&rest args &key (class 'grace-plot) &allow-other-keys)
  "Create a new grace plot.
Return a plot object, of class CLASS, to which commands can be sent.
Any arguments in ARGS are given to INITIALIZE-INSTANCE."
  (apply #'make-instance class (append (remove-kw-args '(:class) args)
                                       (list :allow-other-keys t))))

(defun open-plot (filename &rest args)
  "Open an existing grace project file.
Supply ARGS to the NEW-PLOT function.  Return a PLOT object to which
commands can be sent."
  (let ((plot (apply #'new-plot args)))
    (send-command plot (format nil "LOAD ~S" (namestring filename)))
    plot))

(defun connect-plot (pipe-name &rest args)
  "Connect to a Grace plot via a named pipe."
  (apply #'new-plot :pipe pipe-name args))

(defgeneric active-plot-p (plot &key &allow-other-keys)
  (:documentation "Test if connection to Grace is still active.")
  (:method ((plot plot) &key)
    t)
  (:method ((plot grace-plot) &key)
    (cond ((grace-process plot)
           (and
            #+clozure (eql (ccl:external-process-status (grace-process plot)) :running)
            #+sbcl    (eql (sb-ext:process-status (grace-process plot)) :running)
            (open-stream-p (grace-stream plot))))
          ((grace-stream plot)
           (open-stream-p (grace-stream plot)))
          (t
           nil))))

(defgeneric save-plot (plot filename &key &allow-other-keys)
  (:documentation "Save PLOT to FILENAME.")
  (:method ((plot plot) filename &key)
    "Save PLOT to FILENAME as a Grace project file."
    (send-command plot (format nil "SAVEALL ~S" (namestring filename)))))

(defgeneric close-plot (plot &key &allow-other-keys)
  (:documentation "Close a Grace plot.")
  (:method ((plot plot) &key)
    (send-command plot "EXIT"))
  (:method ((plot grace-plot) &key)
    (call-next-method)
    (close (grace-stream plot))))

(defgeneric disconnect-plot (plot &key &allow-other-keys)
  (:documentation "Disconnect from a Grace plot.")
  (:method ((plot grace-plot) &key)
    (close (grace-stream plot))))

(defgeneric plot-command-stream (plot)
  (:documentation "Return the output stream to use for sending commands to the given plot.")
  (:method ((plot plot))
    *standard-output*)
  (:method ((plot grace-plot))
    (grace-stream plot)))

(defgeneric send-command (plot command &key &allow-other-keys)
  (:documentation "Send command to the given plot.")
  (:method ((plot plot) (command string) &key (flush t))
    (let ((strm (plot-command-stream plot)))
      (when strm
        (write-line command strm)
        (when flush
          (finish-output strm))))))

(defgeneric refresh-plot (plot &key update-gui redraw &allow-other-keys)
  (:documentation "Refresh the given plot.
Update the GUI and redraw the given plot.  Override these
individual behaviours with UPDATE-GUI and REDRAW when supplied.")
  (:method ((plot plot) &key (update-gui nil update-gui-supplied)
                          (redraw nil redraw-supplied))
    (unless (and update-gui-supplied (not update-gui))
      (send-command plot "UPDATEALL"))
    (unless (and redraw-supplied (not redraw))
      (send-command plot "REDRAW"))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Mid-level API -- graph functions
;;;
(defvar *refresh-plot* t
  "Default value for :REFRESH keyword arguments.")

(defmacro without-refresh (&body body)
  `(let ((*refresh-plot* nil))
     ,@body))

(defgeneric create-graph (plot graph &key &allow-other-keys)
  (:documentation "Create a graph.")
  (:method ((plot plot) (graph integer) &key (refresh *refresh-plot*)
                                          (redraw nil redraw-supplied)
                                          (update-gui nil update-gui-supplied))
    (send-command plot (format nil "G~D ON" graph))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

(defgeneric delete-graph (plot graph &key &allow-other-keys)
  (:documentation "Delete a graph.")
  (:method ((plot plot) (graph integer) &key (refresh *refresh-plot*)
                                          (redraw nil redraw-supplied)
                                          (update-gui nil update-gui-supplied))
    (send-command plot (format nil "KILL G~D" graph))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

(defgeneric arrange-graphs (plot nrows ncols &key &allow-other-keys)
  (:documentation "Arrange graphs on a plot page.")
  (:method ((plot plot) (nrows integer) (ncols integer) &key (offset 0) (hgap 0) (vgap 0)
                                                          hvinv hinv vinv snake
                                                          (refresh *refresh-plot*)
                                                          (redraw nil redraw-supplied)
                                                          (update-gui nil update-gui-supplied))
    "Arrange graphs on a plot page.
    NROWS      : non-negative integer
    NCOLS      : non-negative integer
    OFFSET     : real
    HGAP       : real
    VGAP       : real
    HVINV      : boolean or string-designator (\"on\", \"off\")
    HINV       : boolean or string-designator (\"on\", \"off\")
    VINV       : boolean or string-designator (\"on\", \"off\")
    SNAKE      : boolean or string-designator (\"on\", \"off\")
    REFRESH    : boolean
    UPDATE-GUI : boolean
    REDRAW     : boolean"
    (send-command plot (format nil "ARRANGE(~D,~D,~,15F,~,15F,~,15F,~A,~A,~A,~A)"
                               nrows ncols offset hgap vgap
                               (on-or-off hvinv) (on-or-off hinv)
                               (on-or-off vinv) (on-or-off snake)))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

(defgeneric set-graph-type (plot graph type &key &allow-other-keys)
  (:documentation "Set the type of a graph.")
  (:method ((plot plot) (graph integer) type &key (refresh *refresh-plot*)
                                               (redraw nil redraw-supplied)
                                               (update-gui nil update-gui-supplied))
    "Set the type of a graph.
    TYPE       : string-designator
    REFRESH    : boolean
    UPDATE-GUI : boolean
    REDRAW     : boolean"
    (send-command plot (format nil "G~D TYPE ~A" graph type))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

(defgeneric set-graph-title (plot graph &key &allow-other-keys)
  (:documentation "Set the title of a graph.")
  (:method ((plot plot) (graph integer) &key title font size color (refresh *refresh-plot*)
                                          (redraw nil redraw-supplied)
                                          (update-gui nil update-gui-supplied))
    "Set the title properties of a graph.
    TITLE      : string
    FONT       : non-negative integer
    SIZE       : non-negative integer
    COLOR      : non-negative integer, string-designator or RGB triple sequence
    REFRESH    : boolean
    UPDATE-GUI : boolean
    REDRAW     : boolean"
    (send-command plot (format nil "WITH G~D" graph))
    (when title (send-command plot (format nil "TITLE ~S" title)))
    (when font  (send-command plot (format nil "TITLE FONT ~S" font)))
    (when size  (send-command plot (format nil "TITLE SIZE ~A" size)))
    (when color (send-command plot (format nil "TITLE COLOR ~A" (color-string color))))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))
  
(defgeneric set-graph-subtitle (plot graph &key &allow-other-keys)
  (:documentation "Set the subtitle of a graph.")
  (:method ((plot plot) (graph integer) &key subtitle font size color (refresh *refresh-plot*)
                                          (redraw nil redraw-supplied)
                                          (update-gui nil update-gui-supplied))
    "Set the subtitle properties of a graph.
    SUBTITLE   : string
    FONT       : non-negative integer
    SIZE       : non-negative integer
    COLOR      : non-negative integer, string-designator or RGB triple sequence
    REFRESH    : boolean
    UPDATE-GUI : boolean
    REDRAW     : boolean"
    (send-command plot (format nil "WITH G~D" graph))
    (when subtitle (send-command plot (format nil "SUBTITLE ~S" subtitle)))
    (when font (send-command plot (format nil "SUBTITLE FONT ~S" font)))
    (when size (send-command plot (format nil "SUBTITLE SIZE ~A" size)))
    (when color (send-command plot (format nil "SUBTITLE COLOR ~A" (color-string color))))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

(defgeneric set-graph-view (plot graph &key &allow-other-keys)
  (:documentation "Set the view location of a graph.")
  (:method ((plot plot) (graph integer) &key xmin xmax ymin ymax (refresh *refresh-plot*)
                                          (redraw nil redraw-supplied)
                                          (update-gui nil update-gui-supplied))
    "Set the viewport properties of a graph.
    XMIN       : real
    XMAX       : real
    YMIN       : real
    YMAX       : real
    REFRESH    : boolean
    UPDATE-GUI : boolean
    REDRAW     : boolean"
    (send-command plot (format nil "WITH G~D" graph))
    (when xmin (send-command plot (format nil "VIEW XMIN ~A" xmin)))
    (when xmax (send-command plot (format nil "VIEW XMAX ~A" xmax)))
    (when ymin (send-command plot (format nil "VIEW YMIN ~A" ymin)))
    (when ymax (send-command plot (format nil "VIEW YMAX ~A" ymax)))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

(defgeneric set-graph-axes (plot graph &key &allow-other-keys)
  (:documentation "Set the axes properties of a graph.")
  (:method ((plot plot) (graph integer) &key xlabel ylabel xmin xmax ymin ymax xscale yscale
                                          (xtick nil xtick-supplied)
                                          (xtick-label nil xtick-label-supplied)
                                          (ytick nil ytick-supplied)
                                          (ytick-label nil ytick-label-supplied)
                                          xtick-format xtick-precision
                                          ytick-format ytick-precision
                                          xtick-major xtick-minor ytick-major ytick-minor
                                          (xinv nil xinv-supplied) (yinv nil yinv-supplied)
                                          (refresh *refresh-plot*)
                                          (redraw nil redraw-supplied)
                                          (update-gui nil update-gui-supplied))
    "Set the axes properties of a graph.
    XLABEL          : string
    YLABEL          : string
    XMIN            : real
    XMAX            : real
    YMIN            : real
    YMAX            : real
    XSCALE          : string-designator (\"normal\", \"log\", \"recip\")
    YSCALE          : string-designator (\"normal\", \"log\", \"recip\")
    XTICK           : boolean or string-designator (\"on\", \"off\")
    XTICK-LABEL     : boolean or string-designator (\"on\", \"off\")
    YTICK           : boolean or string-designator (\"on\", \"off\")
    YTICK-LABEL     : boolean or string-designator (\"on\", \"off\")
    XTICK-FORMAT    : string-designator
    XTICK-PRECISION : real
    YTICK-FORMAT    : string-designator
    YTICK-PRECISION : real
    XTICK-MAJOR     : real
    XTICK-MINOR     : real
    YTICK-MAJOR     : real
    YTICK-MINOR     : real
    XINV            : boolean or string-designator (\"on\", \"off\")
    YINV            : boolean or string-designator (\"on\", \"off\")
    REFRESH         : boolean
    UPDATE-GUI      : boolean
    REDRAW          : boolean"
    (send-command plot (format nil "WITH G~D" graph))
    (when xlabel (send-command plot (format nil "XAXIS LABEL ~S" xlabel)))
    (when ylabel (send-command plot (format nil "YAXIS LABEL ~S" ylabel)))
    (when xmin   (send-command plot (format nil "WORLD XMIN ~,15F" xmin)))
    (when xmax   (send-command plot (format nil "WORLD XMAX ~,15F" xmax)))
    (when ymin   (send-command plot (format nil "WORLD YMIN ~,15F" ymin)))
    (when ymax   (send-command plot (format nil "WORLD YMAX ~,15F" ymax)))
    (when xscale
      (cond ((equalp (string xscale) "normal")
             (send-command plot "XAXES SCALE NORMAL"))
            ((equalp (subseq (string xscale) 0 3) "log")
             (send-command plot "XAXES SCALE LOGARITHMIC"))
            ((equalp (subseq (string xscale) 0 5) "recip")
             (send-command plot "XAXES SCALE RECIPROCAL"))
            (t
             (error 'grace-error :text (format nil "Unknown XSCALE option: ~A" xscale)))))
    (when yscale
      (cond ((equalp (string yscale) "normal")
             (send-command plot "YAXES SCALE NORMAL"))
            ((equalp (subseq (string yscale) 0 3) "log")
             (send-command plot "YAXES SCALE LOGARITHMIC"))
            ((equalp (subseq (string yscale) 0 5) "recip")
             (send-command plot "YAXES SCALE RECIPROCAL"))
            (t
             (error 'grace-error :text (format nil "Unknown YSCALE option: ~A" yscale)))))
    (when xtick-supplied       (send-command plot (format nil "XAXIS TICK ~A" (on-or-off xtick))))
    (when xtick-label-supplied (send-command plot (format nil "XAXIS TICK LABEL ~A" (on-or-off xtick-label))))
    (when ytick-supplied       (send-command plot (format nil "YAXIS TICK ~A" (on-or-off ytick))))
    (when ytick-label-supplied (send-command plot (format nil "YAXIS TICK LABEL ~A" (on-or-off ytick-label))))
    (when xtick-format
      (send-command plot (format nil "XAXIS TICKLABEL FORMAT ~A" (string-upcase (string xtick-format)))))
    (when xtick-precision
      (send-command plot (format nil "XAXIS TICKLABEL PREC ~,15F" xtick-precision)))
    (when ytick-format
      (send-command plot (format nil "YAXIS TICKLABEL FORMAT ~A" (string-upcase (string ytick-format)))))
    (when ytick-precision
      (send-command plot (format nil "YAXIS TICKLABEL PREC ~,15F" ytick-precision)))
    (when xtick-major          (send-command plot (format nil "XAXIS TICK MAJOR ~,15F" xtick-major)))
    (when xtick-minor          (send-command plot (format nil "XAXIS TICK MINOR ~,15F" xtick-minor)))
    (when ytick-major          (send-command plot (format nil "YAXIS TICK MAJOR ~,15F" ytick-major)))
    (when ytick-minor          (send-command plot (format nil "YAXIS TICK MINOR ~,15F" ytick-minor)))
    (when xinv-supplied        (send-command plot (format nil "XAXES INVERT ~A" (on-or-off xinv))))
    (when yinv-supplied        (send-command plot (format nil "YAXES INVERT ~A" (on-or-off yinv))))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

(defgeneric set-graph-legend (plot graph &key &allow-other-keys)
  (:documentation "Set the legend properties of a graph.")
  (:method ((plot plot) (graph integer) &key (onoff nil onoff-supplied)
                                          location loctype font
                                          char-size color vgap hgap length 
                                          (invert nil invert-supplied)
                                          (box nil box-supplied) box-color
                                          box-pattern box-linestyle box-linewidth
                                          (box-fill nil box-fill-supplied)
                                          box-fill-color box-fill-pattern
                                          (refresh *refresh-plot*)
                                          (redraw nil redraw-supplied)
                                          (update-gui nil update-gui-supplied))
    "Set the legend properties of a graph.
    ONOFF            : boolean or string-designator (\"on\", \"off\")
    LOCATION         : sequence of two reals
    LOCTYPE          : string-designator (\"world\", \"view\")
    FONT             : non-negative integer
    CHAR-SIZE        : non-negative integer
    COLOR            : non-negative integer, string-designator or RGB triple sequence
    VGAP             : non-negative integer
    LENGTH           : non-negative integer
    INVERT           : boolean or string-designator (\"on\", \"off\")
    BOX              : boolean or string-designator (\"on\", \"off\")
    BOX-COLOR        : non-negative integer, string-designator or RGB triple sequence
    BOX-PATTERN      : non-negative integer
    BOX-LINESTYLE    : non-negative integer
    BOX-LINEWIDTH    : non-negative integer
    BOX-FILL         : boolean or string-designator (\"on\", \"off\")
    BOX-FILL-COLOR   : non-negative integer, string-designator or RGB triple sequence
    BOX-FILL-PATTERN : non-negative integer
    REFRESH          : boolean
    UPDATE-GUI       : boolean
    REDRAW           : boolean"
    (send-command plot (format nil "WITH G~D" graph))
    (when onoff-supplied (send-command plot (format nil "LEGEND ~A" (on-or-off onoff))))
    (when location
      (send-command plot (format nil "LEGEND ~A, ~A" (elt location 0) (elt location 1))))
    (when loctype
      (cond ((equalp (string loctype) "world")
             (send-command plot "LEGEND LOCTYPE WORLD"))
            ((equalp (string loctype) "view")
             (send-command plot "LEGEND LOCTYPE VIEW"))
            (t
             (error 'grace-error :text (format nil "Unknown LOCTYPE option: ~A" loctype)))))
    (when font              (send-command plot (format nil "LEGEND FONT ~A" font)))
    (when char-size         (send-command plot (format nil "LEGEND CHAR SIZE ~A" char-size)))
    (when color             (send-command plot (format nil "LEGEND COLOR ~A" (color-string color))))
    (when vgap              (send-command plot (format nil "LEGEND VGAP ~A" vgap)))
    (when hgap              (send-command plot (format nil "LEGEND HGAP ~A" hgap)))
    (when length            (send-command plot (format nil "LEGEND LENGTH ~A" length)))
    (when invert-supplied   (send-command plot (format nil "LEGEND INVERT ~A" (on-or-off invert))))
    (when box-supplied      (send-command plot (format nil "LEGEND BOX ~A" (on-or-off box))))
    (when box-color         (send-command plot (format nil "LEGEND BOX COLOR ~A" (color-string box-color))))
    (when box-pattern       (send-command plot (format nil "LEGEND BOX PATTERN ~A" box-pattern)))
    (when box-linestyle     (send-command plot (format nil "LEGEND BOX LINESTYLE ~A" box-linestyle)))
    (when box-linewidth     (send-command plot (format nil "LEGEND BOX LINEWIDTH ~A" box-linewidth)))
    (when box-fill-supplied (send-command plot (format nil "LEGEND BOX FILL ~A" (on-or-off box-fill))))
    (when box-fill-color    (send-command plot (format nil "LEGEND BOX FILL COLOR ~A" (color-string box-fill-color))))
    (when box-fill-pattern  (send-command plot (format nil "LEGEND BOX FILL PATTERN ~A" box-fill-pattern)))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

(defgeneric autoscale-graph (plot graph &key &allow-other-keys)
  (:documentation "Autoscale the given graph.")
  (:method ((plot plot) (graph integer) &key wrt (refresh *refresh-plot*)
                                          (redraw nil redraw-supplied)
                                          (update-gui nil update-gui-supplied))
    "Autoscale a graph with respect to a given dataset, or restricted to one axis.
    WRT         : non-negative integer or string designator (\"xaxis\", \"yaxis\")
    REFRESH     : boolean
    UPDATE-GUI  : boolean
    REDRAW      : boolean"
    (send-command plot (format nil "WITH G~D" graph))
    (cond ((integerp wrt)
           (send-command plot (format nil "AUTOSCALE S~D" wrt)))
          ((or (equalp (string wrt) "xaxis")
               (equalp (string wrt) "xaxes"))
           (send-command plot (format nil "AUTOSCALE XAXES")))
          ((or (equalp (string wrt) "yaxis")
               (equalp (string wrt) "yaxes"))
           (send-command plot (format nil "AUTOSCALE YAXES")))
          (t
           (send-command plot (format nil "AUTOSCALE"))))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

(defgeneric autotick-graph (plot graph &key &allow-other-keys)
  (:documentation "Autotick the given graph.")
  (:method ((plot plot) (graph integer) &key (refresh *refresh-plot*)
                                          (redraw nil redraw-supplied)
                                          (update-gui nil update-gui-supplied))
    "Automatically setup axis ticks for a graph.
    REFRESH     : boolean
    UPDATE-GUI  : boolean
    REDRAW      : boolean"
    (send-command plot (format nil "WITH G~D" graph))
    (send-command plot "AUTOTICKS")
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Mid-level API -- dataset functions
;;;
(defgeneric create-dataset (plot graph dataset &key &allow-other-keys)
  (:documentation "Create a dataset.")
  (:method ((plot plot) (graph integer) (dataset integer) &key type (refresh *refresh-plot*)
                                                            (redraw nil redraw-supplied)
                                                            (update-gui nil update-gui-supplied))
    (send-command plot (format nil "G~D.S~D ON" graph dataset))
    (when type
      (send-command plot (format nil "G~D.S~D TYPE ~A" graph dataset (xytype-name type))))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

(defgeneric delete-dataset (plot graph dataset &key &allow-other-keys)
  (:documentation "Delete a dataset.")
  (:method ((plot plot) (graph integer) (dataset integer) &key (refresh *refresh-plot*)
                                                            (redraw nil redraw-supplied)
                                                            (update-gui nil update-gui-supplied))
    (send-command plot (format nil "KILL G~D.S~D" graph dataset))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

(defgeneric flatten-datum (datum &key &allow-other-keys)
  (:documentation "Flatten the given datum into a list of numbers.")
  (:method ((datum number) &key)
    (list datum))
  (:method ((datum sequence) &key)
    (apply #'append (map 'list #'flatten-datum datum))))

(defgeneric encode-datum (datum &key &allow-other-keys)
  (:documentation "Encode the given datum to a string representation.
Any structure in the datum is flattened, returning a string of comma
and space separated floating point values.")
  (:method ((datum real) &key)
    (format nil "~,15F" datum))
  (:method ((datum sequence) &key)
    (format nil "~{~A~^, ~}" (map 'list #'encode-datum datum))))

(defgeneric send-data (plot graph dataset data &key type append refresh update-gui redraw &allow-other-keys)
  (:documentation "Send data to a particular PLOT/GRAPH/SET.
Replace the set's data with that given, unless APPEND is non-null in
which case append to it.  The data is assumed to be a sequence of
dataset datum points, where each point resolves to a sequence of real
numbers (via FLATTEN-DATUM and REALPART) representing X,Y,... values.
The length of each point sequence should be consistent with TYPE, if
supplied, which is a string-designator specifying the type of the
dataset (e.g. \"XYZ\").
If REFRESH is non-nil then update the GUI and redraw the plot, or
override these individual behaviours with UPDATE-GUI and REDRAW.")
  (:method ((plot plot) (graph integer) (dataset integer) (data sequence) &key type append
                                                                            (refresh *refresh-plot*)
                                                                            (redraw nil redraw-supplied)
                                                                            (update-gui nil update-gui-supplied))
    (unless append
      (send-command plot (format nil "KILL G~D.S~D SAVEALL" graph dataset)))
    (when type
      (send-command plot (format nil "G~D.S~D TYPE ~A" graph dataset (xytype-name type))))
    (reduce (lambda (dummy datum)
              (let* ((nums (mapcar #'realpart (flatten-datum datum)))
                     (x (or (first nums) 0))
                     (y (or (second nums) 0))
                     (yns (rest (rest nums))))
                (send-command plot (format nil "G~D.S~D POINT ~,15F, ~,15F" graph dataset x y))
                (loop :for yn :in yns
                      :for n :from 1
                      :do (send-command plot (format nil "G~D.S~D.Y~D[G~D.S~D.LENGTH - 1] = ~,15F"
                                                     graph dataset n graph dataset yn))))
              dummy)
            data :initial-value nil)
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

(defgeneric send-dataset (plot graph dataset data &key type refresh update-gui redraw &allow-other-keys)
  (:documentation "Send a complete dataset to Grace.
Instruct Grace to read in a whole dataset from a file, writing the
data out to a temporary file first if necessary.  The data is assumed
to be a sequence of dataset datum points, where each point resolves to
a sequence of floating point numbers (via ENCODE-DATUM) representing
X,Y,... values, as appropriate to TYPE (if supplied, a string
designator for the Grace set type, e.g.  \"XYDXDY\").  If REFRESH is
non-nil then update the GUI and redraw the plot.  Alternatively,
override these individual behaviours with UPDATE-GUI and REDRAW.")
  (:method ((plot plot) (graph integer) (dataset integer) (data sequence) &key type
                                                                            (refresh *refresh-plot*)
                                                                            (redraw nil redraw-supplied)
                                                                            (update-gui nil update-gui-supplied))
    (send-command plot (format nil "KILL G~D.S~D SAVEALL" graph dataset))
    (when type
      (send-command plot (format nil "G~D.S~D TYPE ~A" graph dataset (xytype-name type))))
    (send-command plot (format nil "TARGET G~D.S~D" graph dataset))
    (let ((tmp-file (with-output-to-temporary-file (strm)
                      (dolist (datum data)
                        (format strm "~A~%" (encode-datum datum))))))
      (send-command plot (format nil "READ ~A ~S" type (namestring tmp-file))))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

(defgeneric set-dataset-line (plot graph dataset &key &allow-other-keys)
  (:documentation "Set the line parameters of a dataset.")
  (:method ((plot plot) (graph integer) (dataset integer) &key type style width color pattern
                                                            (dropline nil dropline-supplied)
                                                            (refresh *refresh-plot*)
                                                            (redraw nil redraw-supplied)
                                                            (update-gui nil update-gui-supplied))
    "Set the line parameters of a dataset.
    TYPE          : non-negative integer or string-designator
    STYLE         : non-negative integer or string-designator
    WIDTH         : real
    COLOR         : non-negative integer, string-designator or RGB triple sequence
    PATTERN       : non-negative integer
    DROPLINE      : boolean or string-designator (\"on\", \"off\")
    REFRESH       : boolean
    UPDATE-GUI    : boolean
    REDRAW        : boolean"
    (when type  (send-command plot (format nil "G~D.S~D LINE TYPE ~D" graph dataset (line-type type))))
    (when style (send-command plot (format nil "G~D.S~D LINE LINESTYLE ~D" graph dataset (line-style style))))
    (when width (send-command plot (format nil "G~D.S~D LINE LINEWIDTH ~,15F" graph dataset width)))
    (when color (send-command plot (format nil "G~D.S~D LINE COLOR ~A" graph dataset (color-string color))))
    (when pattern (send-command plot (format nil "G~D.S~D LINE PATTERN ~D" graph dataset pattern)))
    (when dropline-supplied (send-command plot (format nil "G~D.S~D DROPLINE ~A" graph dataset (on-or-off dropline))))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

(defgeneric set-dataset-fill (plot graph dataset &key &allow-other-keys)
  (:documentation "Set the fill parameters of a dataset.")
  (:method ((plot plot) (graph integer) (dataset integer) &key type rule pattern color
                                                            (baseline nil baseline-supplied)
                                                            baseline-type
                                                            (refresh *refresh-plot*)
                                                            (redraw nil redraw-supplied)
                                                            (update-gui nil update-gui-supplied))
    "Set the fill parameters of a dataset.
    TYPE          : non-negative integer or string-designator
    RULE          : non-negative integer or string-designator
    PATTERN       : non-negative integer
    COLOR         : non-negative integer, string-designator or RGB triple sequence
    BASELINE      : boolean or string-designator (\"on\", \"off\")
    BASELINE-TYPE : non-negative integer or string-designator
    REFRESH       : boolean
    UPDATE-GUI    : boolean
    REDRAW        : boolean"
    (when type (send-command plot (format nil "G~D.S~D FILL TYPE ~D" graph dataset (fill-type type))))
    (when rule (send-command plot (format nil "G~D.S~D FILL RULE ~D" graph dataset (fill-rule type))))
    (when pattern (send-command plot (format nil "G~D.S~D FILL PATTERN ~D" graph dataset pattern)))
    (when color (send-command plot (format nil "G~D.S~D FILL COLOR ~A" graph dataset (color-string color))))
    (when baseline-supplied (send-command plot (format nil "G~D.S~D BASELINE ~A" graph dataset (on-or-off baseline))))
    (when baseline-type (send-command plot (format nil "G~D.S~D BASELINE TYPE ~D" graph dataset (baseline-type type))))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

(defgeneric set-dataset-symbol (plot graph dataset &key &allow-other-keys)
  (:documentation "Set the symbol parameters of a dataset.")
  (:method ((plot plot) (graph integer) (dataset integer) &key type style width size color
                                                            pattern fill-color fill-pattern
                                                            char char-font skip
                                                            (refresh *refresh-plot*)
                                                            (redraw nil redraw-supplied)
                                                            (update-gui nil update-gui-supplied))
    "Set the symbol parameters of a dataset.
    TYPE         : non-negative integer or string-designator
    STYLE        : non-negative integer
    WIDTH        : real
    SIZE         : real
    COLOR        : non-negative integer, string-designator or RGB triple sequence
    PATTERN      : non-negative integer
    FILL-COLOR   : non-negative integer, string-designator or RGB triple sequence
    FILL-PATTERN : non-negative integer
    CHAR         : non-negative integer
    CHAR-FONT    : non-negative integer or string-designator
    SKIP         : non-negative integer
    REFRESH      : boolean
    UPDATE-GUI   : boolean
    REDRAW       : boolean"
    (when type  (send-command plot (format nil "G~D.S~D SYMBOL ~D" graph dataset (shape-type type))))
    (when style (send-command plot (format nil "G~D.S~D SYMBOL LINESTYLE ~D" graph dataset style)))
    (when width (send-command plot (format nil "G~D.S~D SYMBOL LINEWIDTH ~,15F" graph dataset width)))
    (when size  (send-command plot (format nil "G~D.S~D SYMBOL SIZE ~,15F" graph dataset size)))
    (when color (send-command plot (format nil "G~D.S~D SYMBOL COLOR ~A" graph dataset (color-string color))))
    (when pattern (send-command plot (format nil "G~D.S~D SYMBOL PATTERN ~D" graph dataset pattern)))
    (when fill-color (send-command plot (format nil "G~D.S~D SYMBOL FILL COLOR ~A" graph dataset (color-string fill-color))))
    (when fill-pattern (send-command plot (format nil "G~D.S~D SYMBOL FILL PATTERN ~D" graph dataset fill-pattern)))
    (when char (send-command plot (format nil "G~D.S~D SYMBOL CHAR ~D" graph dataset (char-integer char))))
    (when char-font (send-command plot (format nil "G~D.S~D SYMBOL CHAR FONT ~A" graph dataset (font-string char-font))))
    (when skip (send-command plot (format nil "G~D.S~D SYMBOL SKIP ~D" graph dataset skip)))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

(defgeneric set-dataset-legend (plot graph dataset &key &allow-other-keys)
  (:documentation "Set the legend properties of a dataset.")
  (:method ((plot plot) (graph integer) (dataset integer) &key text
                                                            (refresh *refresh-plot*)
                                                            (redraw nil redraw-supplied)
                                                            (update-gui nil update-gui-supplied))
    "Set the symbol parameters of a dataset.
    TEXT         : string
    REFRESH      : boolean
    UPDATE-GUI   : boolean
    REDRAW       : boolean"
    (when text (send-command plot (format nil "G~D.S~D LEGEND ~S" graph dataset text)))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))

(defgeneric set-dataset-annotation (plot graph dataset &key &allow-other-keys)
  (:documentation "Set the annotation properties of a dataset.")
  (:method ((plot plot) (graph integer) (dataset integer) &key (enable nil enable-supplied)
                                                            type font size color rotation
                                                            format precision prepend append
                                                            offset
                                                            (refresh *refresh-plot*)
                                                            (redraw nil redraw-supplied)
                                                            (update-gui nil update-gui-supplied))
    "Set the symbol parameters of a dataset.
    ENABLE       : boolean or string-designator (\"on\", \"off\")
    TYPE         : non-negative integer or string-designator
    FONT         : non-negative integer or string-designator
    SIZE         : non-negative integer
    COLOR        : non-negative integer, string-designator or RGB triple sequence
    ROTATION     : non-negative integer
    FORMAT       : string-designator
    PRECISION    : non-negative integer
    PREPEND      : string
    APPEND       : string
    OFFSET       : complex number
    REFRESH      : boolean
    UPDATE-GUI   : boolean
    REDRAW       : boolean"
    (when enable-supplied (send-command plot (format nil "G~D.S~D AVALUE ~A" graph dataset (on-or-off enable))))
    (when type (send-command plot (format nil "G~D.S~D AVALUE TYPE ~D" graph dataset (annotation-type type))))
    (when font (send-command plot (format nil "G~D.S~D AVALUE FONT ~A" graph dataset (font-string font))))
    (when size (send-command plot (format nil "G~D.S~D AVALUE CHAR SIZE ~D" graph dataset size)))
    (when color (send-command plot (format nil "G~D.S~D AVALUE COLOR ~A" graph dataset (color-string color))))
    (when rotation (send-command plot (format nil "G~D.S~D AVALUE ROT ~D" graph dataset rotation)))
    (when format (send-command plot (format nil "G~D.S~D AVALUE FORMAT ~A" graph dataset (format-name format))))
    (when precision (send-command plot (format nil "G~D.S~D AVALUE PREC ~D" graph dataset precision)))
    (when prepend (send-command plot (format nil "G~D.S~D AVALUE PREPEND ~S" graph dataset prepend)))
    (when append (send-command plot (format nil "G~D.S~D AVALUE APPEND ~S" graph dataset append)))
    (when offset (send-command plot (format nil "G~D.S~D AVALUE OFFSET ~A,~A" graph dataset (realpart offset) (imagpart offset))))
    (refresh-plot plot :redraw (if redraw-supplied redraw refresh)
                       :update-gui (if update-gui-supplied update-gui refresh))))
