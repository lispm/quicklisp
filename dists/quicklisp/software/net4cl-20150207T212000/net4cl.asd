;;;  net4cl.asd --- A network library for CL

;;;  Copyright (C) 2005, 2006 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.org>
;;;  Project: net4cl

#+cmu (ext:file-comment "$Module: net4cl.asd, Time-stamp: <2006-10-23 21:48:18 wcp> $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

(defpackage :net4cl-system
  (:use :common-lisp :asdf))

(in-package :net4cl-system)

(defsystem net4cl
    :name "NET4CL"
    :author "Walter C. Pelissero <walter@pelissero.de>"
    :maintainer "Walter C. Pelissero <walter@pelissero.de>"
    :licence "Lesser General Public License"
    :description "NET4CL a Common Lisp library to handle e-mail"
    :long-description
    "NET4CL is a library for Common Lisp to send e-mail via SMTP."
    :depends-on (:sclf #+sbcl :sb-bsd-sockets)
    :components
    ((:file "net")))

(defmethod perform ((o test-op) (c (eql (find-system 'net4cl))))
  (oos 'load-op 'net4cl-tests)
  (oos 'test-op 'net4cl-tests :force t))
