(in-package :flac-parser)

;;; Classes

(defclass flac-file ()
  ((path :reader path
         :initarg :path)
   (buffer :reader buffer
           :initarg :buffer)
   (size :accessor size
         :initarg :size)
   (parse-tree :accessor parse-tree)
   (type-reports :accessor type-reports
                 :initform nil)))

(defclass parse-tree ()
  ((marker :reader marker)
   (stream-info :reader stream-info)
   (blocks :reader blocks)
   (frames :reader frames)))

(defclass metadata-block ()
  ((header :reader header)
   (data :reader data)))

(defclass metadata-block-header ()
  ((last-flag :reader last-flag)
   (type :reader block-type)
   (length :reader block-length)))

(defclass metadata-block-data () ())

(defclass metadata-block-stream-info (metadata-block-data)
  ((minimum-block-size :reader minimum-block-size)
   (maximum-block-size :reader maximum-block-size)
   (minimum-frame-size :reader minimum-frame-size)
   (maximum-frame-size :reader maximum-frame-size)
   (sample-rate :reader sample-rate)
   (channel-count :reader channel-count)
   (bits-per-sample :reader bits-per-sample)
   (sample-count :reader sample-count)
   (md5 :reader md5)))

(defclass metadata-block-padding (metadata-block-data)
  ((data :reader data)))

(defclass metadata-block-application (metadata-block-data)
  ((id :reader id)))

(defclass metadata-block-seek-table (metadata-block-data)
  ((seek-points :accessor seek-points
                :initform nil)))

(defclass metadata-block-seek-table/seek-point ()
  ((sample :reader sample
           :initarg :sample)
   (offset :reader offset
           :initarg :offset)
   (sample-count :reader sample-count
                 :initarg :sample-count)))

(defclass metadata-block-vorbis-comment (metadata-block-data)
  ((vendor-length :reader vendor-length)
   (vendor :reader vendor)
   (user-comments-count :reader user-comments-count)
   (user-comments :reader user-comments)))

(defclass metadata-block-cue-sheet (metadata-block-data)
  ((media-catalog-id :reader media-catalog-id)
   (lead-in-sample-count :reader lead-in-sample-count)
   (cd-flag :reader cd-flag)
   (track-count :reader track-count)
   (tracks :reader tracks)))

(defclass metadata-block-cue-sheet/track ()
  ((offset :reader offset)
   (number :reader track-number)
   (isrc :reader isrc)
   (type :reader track-type)
   (pre-emphasis-flag :reader pre-emphasis-flag)
   (index-count :reader index-count)
   (indices :reader indices)))

(defclass metadata-block-cue-sheet/track-index ()
  ((offset :reader offset
           :initarg :offset)
   (number :reader track-index-number
           :initarg :number)))

(defclass metadata-block-picture (metadata-block-data)
  ((type :reader picture-type)
   (mime-type-length :reader mime-type-length)
   (mime-type :reader mime-type)
   (description-length :reader description-length)
   (description :reader description)
   (width :reader width)
   (height :reader height)
   (bits-per-pixel :reader bits-per-pixel)
   (indexed-color-count :reader indexed-color-count)
   (size :reader picture-size)
   (data :reader data)))

(defclass type-check-report ()
  ((id :reader id
       :initarg :id)
   (level :reader level
          :initarg :level
          :initform :error)
   (node :reader node
         :initarg :node)
   (context :reader context
            :initarg :context)))

;;; Pretty printers

(defmethod print-object ((o flac-file) stream)
  (print-unreadable-object (o stream :type t)
    (format stream "~A" (path o))))

(defmethod print-object ((o metadata-block) stream)
  (print-unreadable-object (o stream :type t)
    (format stream "~A" (block-type-name (header o)))))

(defmethod print-object ((o type-check-report) stream)
  (print-unreadable-object (o stream)
    (format stream "~A: ~A" (level o) (id o))))

;;; Generic Functions

(defgeneric parse (file node &key &allow-other-keys))

(defgeneric type-check-rule (file node id &key &allow-other-keys))

(defgeneric type-check (file node &key &allow-other-keys))

;;; Conditions

(define-condition flac-error (error)
  ((file :reader file
         :initarg :file)))

(define-condition invalid-file (flac-error) ()
  (:report (lambda (c s)
             (format s "File is not a valid FLAC audio file: ~A."
                     (path (file c))))))
