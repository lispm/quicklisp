;;;  html-sugar.asd --- system definition

;;;  Copyright (C) 2005, 2006, 2007 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: HTML-SUGAR

#+cmu (ext:file-comment "$Module: html-sugar.asd, Time-stamp: <2007-02-13 18:23:35 wcp> $")

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

#+nil
(error "The feature NIL has been defined.  This has been reserved to comment out sexps.")

(in-package :cl-user)

(defpackage :html-sugar-system
  (:use :common-lisp :asdf))

(in-package :html-sugar-system)

(defsystem html-sugar
    :name "HTML-SUGAR"
    :author "Walter C. Pelissero <walter@pelissero.de>"
    :maintainer "Walter C. Pelissero <walter@pelissero.de>"
    ;; :version "0.0"
    :description "HTML Helper Functions"
    :long-description
    "A collection of functions to help generating HTML with (P)AServe."
    :licence "LGPL"
    :depends-on (:aserve :webactions :sclf :uffi)
    :components
    ((:file "package")
     (:file "crypt-loader" :depends-on ("package"))
     (:file "sugar" :depends-on ("package" "crypt-loader"))))
