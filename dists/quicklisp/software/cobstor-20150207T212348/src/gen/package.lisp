;;;  package.lisp --- description of the agent generator package

;;;  Copyright (C) 2005, 2009 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: cobstor

#+cmu (ext:file-comment "$Module: package.lisp $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

(in-package :cl-user)

(defpackage :cobstor-generator
  (:use :common-lisp :npg :sclf :stateful-streams :cobstor)
  (:shadowing-import-from :stateful-streams #:open)
  (:export #:*decimal-point*
	   #:make-cobol-agent
	   #:make-cobol-proxy-stub
	   #:read-field-declarations
	   #:generate-cobstor-interface
	   ;; the classes
	   #:field-declaration
	   #:leaf-field-declaration
	   #:container-field-declaration
	   ;; accessors
	   #:field-level
	   #:field-name
	   #:field-size
	   #:field-redefines
	   #:field-alternates
	   #:field-times
	   #:field-type
	   #:field-length
	   #:field-picture
	   #:field-signed
	   #:field-sign-position
	   #:field-sign-separated
	   #:field-decimals
	   #:field-subfields))
