(in-package :tcr.sequence-iterators.test)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defparameter +default-test-clauses+
    '((:sequences :all       :start 0      :end nil)
      (:sequences :all       :start 0      :end *size*)
      (:sequences :all       :start 0      :end 0)
      (:sequences :all       :start *size* :end *size*)
      (:sequences :non-empty :start 1      :end *size*)
      (:sequences :non-empty :start 0      :end (1- *size*))
      (:sequences :non-empty :start 1      :end (1- *size*)))))

;;; VALIDATE-SEQUENCE-BOUNDS

(define-sequence-test validate-sequence-bounds.1
    #.+default-test-clauses+
  (signals-condition-p 'sequence-bounding-indices-error
    (validate-sequence-bounds *sequence* *start* *end*))
  nil)

(define-sequence-test validate-sequence-bounds.2
    ((:sequences :all :start 0           :end (1+ *size*))
     (:sequences :all :start (1+ *size*) :end *size*)
     (:sequences :all :start 1           :end 0))
  (signals-condition-p 'sequence-bounding-indices-error
    (validate-sequence-bounds *sequence* *start* *end*))
  t)

(define-sequence-test validate-sequence-bounds.2a
    ((:sequences :all :start nil :end *size*))
  (signals-condition-p 'type-error
    (validate-sequence-bounds *sequence* *start* *end*))
  t)

(define-sequence-test validate-sequence-bounds.3
    ((:sequences :all :start 0 :end *size*)
     (:sequences :all :start 0 :end nil))
  (validate-sequence-bounds *sequence* *start* *end*)
  
  *sequence*
  0
  *size*
  *size*)

(define-sequence-test validate-sequence-bounds.4
    ((:sequences :all :start 0 :end *size*)
     (:sequences :all :start 0 :end nil))
  (validate-sequence-bounds *sequence* *start* *end* *size*)
  
  *sequence*
  0
  *size*
  *size*)

;;; MAKE-SEQUENCE-LIKE

(define-sequence-test make-sequence-like.1
    ((:sequences :all))
  (let ((seq (make-sequence-like *sequence* (floor *size* 2))))
    (values (type= (sequence-type-of seq)
                   (sequence-type-of *sequence*))
            (length seq)))
  
  t
  (floor *size* 2))

#+nil
(define-sequence-test make-sequence-like.2 (:all)
    (let* ((elt (prototype (sequence-element-type *sequence*)))
           (seq (make-sequence-like *sequence* 1 :initial-element elt)))
      (values (length seq)
              (eql elt (elt seq 0))))
  1
  t)

(define-sequence-test make-sequence-like.3
    ((:sequences :all))
  (make-sequence-like *sequence* (length *sequence*)
                      :initial-contents *sequence*)
  *sequence*)

;;; DOSEQUENCE

(define-sequence-test dosequence.1
    #.+default-test-clauses+  
  (let ((count 0))
    (dosequence (_ *sequence* count :start *start* :end *end*)
      (incf count)))
  
  (length (subseq *sequence* (or *start* 0) *end*)))

(define-sequence-test dosequence.2
    #.+default-test-clauses+  
  (let ((result (make-sequence-like *sequence*
                                    (- (or *end* *size*)
                                       (or *start* 0))))
        (idx 0))
    (dosequence (elt *sequence* result :start *start* :end *end* :from-end t)
      (setf (elt result idx) elt)
      (incf idx)))
  
  (reverse (subseq *sequence* (or *start* 0) *end*)))

(deftest dosequence.3
    (let (indices)
      (dosequence ((_ idx) (make-list 5) indices)
        (push idx indices)))
  (4 3 2 1 0))

;;; DOSEQUENCES*

(define-sequence-test dosequences*.1
    #.+default-test-clauses+
  (let ((result (make-sequence-like *sequence*
                                    (- (or *end* *size*)
                                       (or *start* 0)))))
    (dosequences* ((elt *sequence* result :start *start* :end *end* :from-end t)
                   (_ result (assert nil) :place result-ptr))
      (setf (result-ptr) elt)))
  
  (reverse (subseq *sequence* (or *start* 0) *end*)))

;;; DO-SEQUENCE-ITERATORS

(define-sequence-test do-sequence-iterators.1
    #.+default-test-clauses+
  (with-sequence-iterator (it1 *sequence* :start *start* :end *end*
                                          :from-end *from-end*)
    (with-sequence-iterator (it2 (reverse *sequence*) :start (- *size* (or *end* *size*))
                                                      :end   (- *size* (or *start* 0))
                                                      :from-end (not *from-end*))
      (do-sequence-iterators ((e1 it1 :it1)
                              (e2 it2 :it2))
        (unless (eql e1 e2)
          (return nil)))))
  :it1
  :it2)

;;; DO-SEQUENCE-ITERATORS*

(define-sequence-test do-sequence-iterators*.1
    #.+default-test-clauses+
  (with-sequence-iterator (it1 *sequence* :start *start* :end *end*
                                          :from-end *from-end*)
    (with-sequence-iterator (it2 (reverse *sequence*) :start (- *size* (or *end* *size*))
                                                      :end   (- *size* (or *start* 0))
                                                      :from-end (not *from-end*))
      (do-sequence-iterators* ((e1 it1 :it1)
                               (e2 it2 :it2))
        (unless (eql e1 e2)
          (return nil)))))
  :it1)

;;; WITH-SEQUENCE-ITERATOR

(define-sequence-test with-sequence-iterator.1
    #.+default-test-clauses+
  (with-sequence-iterator (it *sequence* :start *start*
                                         :end *end*
                                         :place ptr)
    (multiple-value-bind (more? elt idx) (it)
      (or (null more?)
          (eql elt (ptr)))))
  t)

(define-sequence-test with-sequence-iterator.2
    #.+default-test-clauses+
  (with-sequence-iterator (it *sequence* :start *start*
                                         :end *end*
                                         :from-end t
                                         :place ptr)
    (multiple-value-bind (more? elt idx) (it)
      (or (null more?)
          (eql elt (ptr)))))
  t)
