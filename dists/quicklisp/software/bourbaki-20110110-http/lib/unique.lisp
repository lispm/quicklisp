;; Bourbaki -- Existential uniqueness "∃1" -- 'Exists at most one "∃*"'
;; Converted more or less directly from set.mm

(module "unique"

  (symkind "WFF")
  (symkind "SET")
  (symkind "VAR" :super set)
  (import [!!equ-ax])
  (import [!!equ])

  (meta (:comment . "Exists unique, exists at most one"))

  (local "ex1" (meta (:comment . "Exists unique.")))
  (local "ex?" (meta (:comment . "Exists at most one.")))

  (def "df-Ex1" wff "∃1" ((:bound var) "x" wff "φ") (var "y")
       [∃ y ∀ x ↔ φ = x y])
;    ((meta (:html-sym . "_e1")
;          (:html-fn . #'polish-prefix)))

  (def "df-Ex?" wff "∃?" ((:bound var) "x" wff "φ") ()
       [→ ∃ x φ ∃1 x φ])
;    ((meta (:html-sym . "_em1")
;          (:html-fn . #'polish-prefix)))

(in-context !ex1
  ; weaker def (only Bvp y φ) was df-alt
  (th "df-f" (var "x" wff "φ" var "y")
      (hypo [Bvp y φ])
      (dist (!x !y))
      (ass [↔ ∃1 x φ ∃ y ∀ x ↔ φ = x y])
      (loc var "z")
      (proof
        [df-Ex1 x φ z]
        [Bvp y [= x z]]
        [Bvp y [↔ φ = x z]]
        [Bvp y [∀ x ↔ φ = x z]]
        [ax17-bv z [∀ x ↔ φ = x y]]
        [→ = z y ↔ = x z = x y]
        [ded !bi!eqt> [= x z] [= x y] φ [= z y]]
        [distv !al!eqtd x [= z y] [↔ φ = x z] [↔ φ = x y]]
        [cbvex z y [∀ x ↔ φ = x z] [∀ x ↔ φ = x y]]
        [bitr [∃1 x φ] [∃ z ∀ x ↔ φ = x z] [∃ y ∀ x ↔ φ = x y]]))

  (th "eqt" (var "x" wff ![φ ψ])
      (ass [→ ∀ x ↔ φ ψ ↔ ∃1 x φ ∃1 x ψ])
      (loc var "y")
      (proof
        [df-Ex1 x φ y]
        [df-Ex1 x ψ y]
        [!bi!eqt< φ ψ [= x y]]
        [!al!imti x [↔ φ ψ] [↔ ↔ φ = x y ↔ ψ = x y]]
        [ded !al!eqt x [↔ φ = x y] [↔ ψ = x y] [∀ x ↔ φ ψ]]
        [ax17-bv y [∀ x ↔ φ ψ]]
        [!ex!eqtd y [∀ x ↔ φ ψ] [∀ x ↔ φ = x y] [∀ x ↔ ψ = x y]]
        [!bi!<trg [∀ x ↔ φ ψ] [∃ y ∀ x ↔ φ = x y] [∃ y ∀ x ↔ ψ = x y]
                              [∃1 x φ] [∃1 x ψ]]))

  (th "eqtd" (var "x" wff ![φ ψ χ])
      (hypo [Bvp x φ] [→ φ ↔ ψ χ])
      (ass [→ φ ↔ ∃1 x ψ ∃1 x χ])
      (loc var "y")
      (proof
        [ded !bi!eqt< ψ χ [= x y] φ]
        [!al!eqtd x φ [↔ ψ = x y] [↔ χ = x y]]
        [distv !ex!eqtd y φ [∀ x ↔ ψ = x y] [∀ x ↔ χ = x y]]
        [df-Ex1 x ψ y]
        [df-Ex1 x χ y]
        [!bi!<trg φ [∃ y ∀ x ↔ ψ = x y] [∃ y ∀ x ↔ χ = x y]
                    [∃1 x ψ]            [∃1 x χ]]))

  (th "eqti" (var "x" wff ![φ ψ])
      (hypo [↔ φ ψ])
      (ass [↔ ∃1 x φ ∃1 x ψ])
      (proof
        [= x x]
        [!bv!true x [= x x]]
        [infer ax1 [↔ φ ψ] [= x x]]
        [eqtd x [= x x] φ ψ]
        [ax-mp [= x x] [↔ ∃1 x φ ∃1 x ψ]]))

  (th "bv-b" (var "x" wff "φ")
      (ass [Bvp x ∃1 x φ])
      (loc var "y")
      (proof
        [Bvp x [∀ x ↔ φ = x y]]
        [Bvp x ∃ y [∀ x ↔ φ = x y]]
        [!bv!exp x [∃ y ∀ x ↔ φ = x y]]
        [df-Ex1 x φ y]
        [!al!eqti x [∃1 x φ] [∃ y ∀ x ↔ φ = x y]]
        [<imtr [∃ y ∀ x ↔ φ = x y] [∀ x ∃ y ∀ x ↔ φ = x y]
               [∃1 x φ]            [∀ x ∃1 x φ]]
        [!bv!imp x [∃1 x φ]]))
  (th-pattern !bv-b)

  (th "bv-" (var "y" wff "φ" var "x")
      (hypo [Bvp x φ])
      (ass [Bvp x ∃1 y φ])
      (loc var "z")
      (proof
        [ax10 y x [∀ y ↔ φ = y z]]
        [+syl !distinctor!com x y [→ ∀ y ∀ y ↔ φ = y z ∀ x ∀ y ↔ φ = y z]]
        [Bvp y [∀ y ↔ φ = y z]]
        [→ ∀ y ↔ φ = y z ∀ y ∀ y ↔ φ = y z]
        [!im!rp32 [∀ x = x y] [∀ y ∀ y ↔ φ = y z] [∀ x ∀ y ↔ φ = y z] [∀ y ↔ φ = y z]]
        [Bvp x ∀ x = x y]
        [imgen>i x [∀ x = x y] [→ ∀ y ↔ φ = y z ∀ x ∀ y ↔ φ = y z]]
        [ded df-Bvp x [∀ y ↔ φ = y z] [∀ x = x y]]
        [!distinctor!nbv y x y]
        [!distinctor!nbv x x y]
        [infer ax1 [Bvp x φ] [¬ ∀ x = x y]]
        [dveeq1 x y z]
        [imgen>i x [¬ ∀ x = x y] [→ = y z ∀ x = y z]]
        [ded df-Bvp x [= y z] [¬ ∀ x = x y]]
        [!bv!bid x [¬ ∀ x = x y] φ [= y z]]
        [!bv!ald x y [¬ ∀ x = x y] [↔ φ = y z]]
        [casei [∀ x = x y] [Bvp x [∀ y ↔ φ = y z]]] ; [[→ ∀ y ↔ φ = y z ∀ x ∀ y ↔ φ = y z]]
        [Bvp x [∃ z ∀ y ↔ φ = y z]]
        [df-Ex1 y φ z]
        [!bv!def x [∃1 y φ] [∃ z ∀ y ↔ φ = y z]]))
  (th-pattern !bv-)

  (th "chvar" (var ![y x] wff "φ")
      (hypo [Bvp y φ])
      (dist (!x !y))
      (ass [↔ ∃1 x φ ∃1 y sb y x φ])
      (loc var "z")
      (proof
        [ax17eq x z y]
        [Bvp y ↔ φ = x z]
        [!subst!cbv-al y x [↔ φ = x z]]
        [!subst!eqat< x z y]
        [!subst!bi> y x [= x z] [= y z] φ]
        [!al!eqti y [sb y x ↔ φ = x z] [↔ sb y x φ = y z]]
        [b2i bitr [∀ x ↔ φ = x z] [∀ y sb y x ↔ φ = x z] [∀ y ↔ sb y x φ = y z]]
        [!ex!eqti z [∀ x ↔ φ = x z] [∀ y ↔ sb y x φ = y z]]
        [df-Ex1 x φ z]
        [df-Ex1 y [sb y x φ] z]
        [!bi!<tr [∃ z ∀ x ↔ φ = x z] [∃ z ∀ y ↔ sb y x φ = y z]
                 [∃1 x φ]            [∃1 y sb y x φ]]))

  (th "cbv" (var ![x y] wff ![φ ψ])
      (hypo [Bvp y φ] [Bvp x ψ] [→ = x y ↔ φ ψ])
      (dist (!x !y))
      (ass [↔ ∃1 x φ ∃1 y ψ])
      (proof
        [chvar y x φ]
        [!subst!xpl y x φ ψ]
        [eqti y [sb y x φ] ψ]
        [bitr [∃1 x φ] [∃1 y sb y x φ] [∃1 y ψ]]))

  (th "df-alt1" (var "x" wff "φ" var "y")
      (hypo [Bvp y φ])
      (dist (!x !y))
      (ass [↔ ∃1 x φ ∃ x ∧ φ ∀ y → sb y x φ = x y])
      (proof
        [!subst!bv-var y x φ]
        [df-f y [sb y x φ] x]
        [chvar y x φ]
        [↔ = x y = y x]
        [infer !im!eqt> [= x y] [= y x] [sb y x φ]]
        [!al!eqti y [→ sb y x φ = x y] [→ sb y x φ = y x]]
        ;; to sb6rf
          [!subst!df-dval x y [sb y x φ]]
          [!subst!comp x y x φ]
          [!subst!id x φ]
          [bitr [sb x y sb y x φ] [sb x x φ] φ]
          [b2i bitr φ [sb x y sb y x φ] [∀ y → = y x sb y x φ]]
        ;; end sb6rf
        [!an!eqti [∀ y → sb y x φ = x y] [∀ y → sb y x φ = y x]
                   φ [∀ y → = y x sb y x φ]]
        [!an!com φ [∀ y → sb y x φ = x y]]
        [!al!dfbi y [sb y x φ] [= y x]]
        [!bi!<tr [∧ ∀ y → sb y x φ = x y φ] [∧ ∀ y → sb y x φ = y x ∀ y → = y x sb y x φ]
                 [∧ φ ∀ y → sb y x φ = x y] [∀ y ↔ sb y x φ = y x]] ;; this is a good result on its own
        [!ex!eqti x [∧ φ ∀ y → sb y x φ = x y] [∀ y ↔ sb y x φ = y x]]
        [!bi!<tr [∃1 y sb y x φ] [∃ x ∀ y ↔ sb y x φ = y x]
                 [∃1 x φ]        [∃ x ∧ φ ∀ y → sb y x φ = x y]]
        )))

(in-context !ex?
  (th "df-alt0" (var ![x y] wff "φ")
      (hypo [Bvp y φ])
      (dist (!x !y))
      (ass [↔ ∃ y ∀ x → φ = x y ∀ x ∀ y → ∧ φ sb y x φ = x y])
      (loc var "z")
      (proof
        [ax17eq x z y]
        [Bvp y [→ φ = x z]]
        [Bvp y [∀ x → φ = x z]]
        [ax17-bv z [∀ x → φ = x y]]
        [→ = z y ↔ = x z = x y]
        [ded !im!eqt> [= x z] [= x y] φ [= z y]]
        [distv !al!eqtd x [= z y] [→ φ = x z] [→ φ = x y]]
        [cbvex z y [∀ x → φ = x z] [∀ x → φ = x y]]
        [!subst!bv-var y x φ]
        [ax17eq y z x]
        [Bvp x [→ sb y x φ = y z]]
        [!subst!id2 y x φ]
        [ded bi< φ [sb y x φ] [= x y]]
        [ax8 x y z]
        [!im!imtd [= x y] [sb y x φ] φ [= x z] [= y z]]
        [cbv3 x y [→ φ = x z] [→ sb y x φ = y z]]
        [infer !an!join [∀ x → φ = x z] [∀ y → sb y x φ = y z]]
        [!al!arrange x y [→ φ = x z] [→ sb y x φ = y z]]
        [b2i syl [∀ x → φ = x z] [∧ ∀ x → φ = x z ∀ y → sb y x φ = y z] [∀ x ∀ y ∧ → φ = x z → sb y x φ = y z]]
        [!an!imt φ [= x z] [sb y x φ] [= y z]]
        [→ ∧ = x z = y z = x y]
        [!im!rp33 [∧ → φ = x z → sb y x φ = y z] [∧ φ sb y x φ] [∧ = x z = y z] [= x y]]
        [!al!imti y [∧ → φ = x z → sb y x φ = y z] [→ ∧ φ sb y x φ = x y]]
        [!al!imti x [∀ y ∧ → φ = x z → sb y x φ = y z] [∀ y → ∧ φ sb y x φ = x y]]
        [syl [∀ x → φ = x z] [∀ x ∀ y ∧ → φ = x z → sb y x φ = y z] [∀ x ∀ y → ∧ φ sb y x φ = x y]]
        [distv imgen<i z [∀ x → φ = x z] [∀ x ∀ y → ∧ φ sb y x φ = x y]]
        [b2i syl [∃ y ∀ x → φ = x y] [∃ z ∀ x → φ = x z] [∀ x ∀ y → ∧ φ sb y x φ = x y]] ;; One half

        [!al!imt x [sb y x φ] [→ φ = x y]]
        [!al!imti y [∀ x → sb y x φ → φ = x y] [→ ∀ x sb y x φ ∀ x → φ = x y]]
        [+syl ax7 x y [→ sb y x φ → φ = x y] [∀ y → ∀ x sb y x φ ∀ x → φ = x y]]
        [!ex!imt y [∀ x sb y x φ] [∀ x → φ = x y]]
        [syl [∀ x ∀ y → sb y x φ → φ = x y] [∀ y → ∀ x sb y x φ ∀ x → φ = x y] [→ ∃ y ∀ x sb y x φ ∃ y ∀ x → φ = x y]]
        [!subst!bv-var y x φ]
        [→ sb y x φ ∀ x sb y x φ]
        [!ex!imti y [sb y x φ] [∀ x sb y x φ]]
        [!im!rp32 [∀ x ∀ y → sb y x φ → φ = x y] [∃ y ∀ x sb y x φ] [∃ y ∀ x → φ = x y] [∃ y sb y x φ]]
        [com12i [∀ x ∀ y → sb y x φ → φ = x y] [∃ y sb y x φ] [∃ y ∀ x → φ = x y]]
        [impexp φ [sb y x φ] [= x y]]
        [!im!bcom12 φ [sb y x φ] [= x y]]
        [bitr [→ ∧ φ sb y x φ = x y] [→ φ → sb y x φ = x y] [→ sb y x φ → φ = x y]]
        [!al!eqti y [→ ∧ φ sb y x φ = x y] [→ sb y x φ → φ = x y]]
        [!al!eqti x [∀ y → ∧ φ sb y x φ = x y] [∀ y → sb y x φ → φ = x y]]
        [b2i !im!rp32 [∃ y sb y x φ] [∀ x ∀ y → sb y x φ → φ = x y] [∃ y ∀ x → φ = x y]
                      [∀ x ∀ y → ∧ φ sb y x φ = x y]] ;; Other half, in the case [∃ y sb y x φ]
        [!al!neg y [sb y x φ]]
        [Bvp x [¬ sb y x φ]]
        [Bvp y ¬ φ]
        [ded bi> φ [sb y x φ] [= x y]]
        [+syl [→ = y x = x y] [→ φ sb y x φ]]
        [ded con φ [sb y x φ] [= y x]]
        [cbv3 y x [¬ sb y x φ] [¬ φ]]
        [→ ¬ φ → φ [= x y]]
        [!al!imti x [¬ φ] [→ φ = x y]]
        [!ex!spec1 y [∀ x → φ = x y]]
        [3syl [∀ y ¬ sb y x φ] [∀ x ¬ φ] [∀ x → φ = x y] [∃ y ∀ x → φ = x y]]
        [b2i syl [¬ ∃ y sb y x φ] [∀ y ¬ sb y x φ] [∃ y ∀ x → φ = x y]] ;; Other half, in the case [¬ ∃ y sb y x φ]
        [ded ax1 [∃ y ∀ x → φ = x y] [∀ x ∀ y → ∧ φ sb y x φ = x y] [¬ ∃ y sb y x φ]]
        [casei [∃ y sb y x φ] [→ ∀ x ∀ y → ∧ φ sb y x φ = x y ∃ y ∀ x → φ = x y]]
        [>bii [∃ y ∀ x → φ = x y] [∀ x ∀ y → ∧ φ sb y x φ = x y ∃ y ∀ x → φ = x y]]
        )) )
      
  (th "Ex1->Ex" (var "x" wff "φ")
      (ass [→ ∃1 x φ ∃ x φ])
      (loc var "y")
      (proof
        [ax17-bv y φ]
        [!ex1!df-alt1 x φ y]
        [!ex!di-an x φ [∀ y → sb y x φ = x y]]
        [b2i syl [∃1 x φ] [∃ x ∧ φ ∀ y → sb y x φ = x y] [∧ ∃ x φ ∃ x ∀ y → sb y x φ = x y]]
        [ded !an!sim< [∃ x φ] [∃ x ∀ y → sb y x φ = x y] [∃1 x φ]]))

  (th "Ex1->Ex?-0" (var ![x y] wff "φ")
      (dist (!x !y))
      (hypo [Bvp y φ])
      (ass [→ ∃1 x φ ∃ y ∀ x → φ = x y])
      (proof
        [!ex1!df-f x φ y]
        [bi> φ [= x y]]
        [!al!imti x [↔ φ = x y] [→ φ = x y]]
        [!ex!imti y [∀ x ↔ φ = x y] [∀ x → φ = x y]]
        [b2i syl [∃1 x φ] [∃ y ∀ x ↔ φ = x y] [∃ y ∀ x → φ = x y]]))

(in-context !ex1
  (th "df-alt2" (var "x" wff "φ" var "y")
      (dist (!x !y))
      (hypo [Bvp y φ])
      (ass [↔ ∃1 x φ ∧ ∃ x φ ∀ x ∀ y → ∧ φ sb y x φ = x y])
      (proof
        [Ex1->Ex x φ]
        [Ex1->Ex?-0 x y φ]
        [!ex?!df-alt0 x y φ]
        [b2i syl [∃1 x φ] [∃ y ∀ x → φ = x y] [∀ x ∀ y → ∧ φ sb y x φ = x y]]
        [jca [∃1 x φ] [∃ x φ] [∀ x ∀ y → ∧ φ sb y x φ = x y]] ;; One direction
        [!ex!di-an<r x φ [∀ y → ∧ φ sb y x φ = x y]]
        [impexp φ [sb y x φ] [= x y]]
        [!al!eqti y [→ ∧ φ sb y x φ = x y] [→ φ → sb y x φ = x y]]
        [imgen> y φ [→ sb y x φ = x y]]
        [bitr [∀ y → ∧ φ sb y x φ = x y] [∀ y → φ → sb y x φ = x y] [→ φ ∀ y → sb y x φ = x y]]
        [infer !an!eqt> [∀ y → ∧ φ sb y x φ = x y] [→ φ ∀ y → sb y x φ = x y] φ]
        [abai φ [∀ y → sb y x φ = x y]]
        [b2i bitr [∧ φ ∀ y → ∧ φ sb y x φ = x y] [∧ φ → φ ∀ y → sb y x φ = x y] [∧ φ ∀ y → sb y x φ = x y]]
        [!ex!eqti x [∧ φ ∀ y → ∧ φ sb y x φ = x y] [∧ φ ∀ y → sb y x φ = x y]]
        [b2i syl [∧ ∃ x φ ∀ x ∀ y → ∧ φ sb y x φ = x y] [∃ x ∧ φ ∀ y → ∧ φ sb y x φ = x y]
                 [∃ x ∧ φ ∀ y → sb y x φ = x y]]
        [df-alt1 x φ y]
        [b2i syl [∧ ∃ x φ ∀ x ∀ y → ∧ φ sb y x φ = x y] [∃ x ∧ φ ∀ y → sb y x φ = x y] [∃1 x φ]]
        [>bii [∃1 x φ] [∧ ∃ x φ ∀ x ∀ y → ∧ φ sb y x φ = x y]])) 

  (th "df-alt3" (var "x" wff "φ" var "y")
      (dist (!x !y))
      (hypo [Bvp y φ])
      (ass [↔ ∃1 x φ ∧ ∃ x φ ∃ y ∀ x → φ = x y])
      (proof
        [df-alt2 x φ y]
        [!ex?!df-alt0 x y φ]
        [infer !an!eqt> [∃ y ∀ x → φ = x y] [∀ x ∀ y → ∧ φ sb y x φ = x y] [∃ x φ]]
        [b2i bitr [∃1 x φ] [∧ ∃ x φ ∀ x ∀ y → ∧ φ sb y x φ = x y] [∧ ∃ x φ ∃ y ∀ x → φ = x y]]))

  (th "->or" (var "x" wff ![φ ψ])
      (hypo [Bvp x φ])
      (ass [→ ∧ ¬ φ ∃1 x ψ ∃1 x ∨ φ ψ])
      (proof
        [Bvp x ¬ φ]
        [!or!false< φ ψ]
        [eqtd x [¬ φ] ψ [∨ φ ψ]]
        [bi>an [¬ φ] [∃1 x ψ] [∃1 x ∨ φ ψ]])) )

(in-context !ex?
  (th "df-alt2" (var "x" wff "φ" var "y")
      (dist (!x !y))
      (hypo [Bvp y φ])
      (ass [↔ ∃? x φ ∃ y ∀ x → φ = x y])
      (proof
        [df-Ex? x φ]
        [!al!neg x φ]
        [→ ¬ φ → φ [= x y]]
        [!al!imti x [¬ φ] [→ φ = x y]]
        [!ex!spec1 y [∀ x → φ = x y]]
        [syl [∀ x ¬ φ] [∀ x → φ = x y] [∃ y ∀ x → φ = x y]]
        [b2i syl [¬ ∃ x φ] [∀ x ¬ φ] [∃ y ∀ x → φ = x y]]
        [Ex1->Ex?-0 x y φ]
        [!im!ja [∃ x φ] [∃1 x φ] [∃ y ∀ x → φ = x y]]
        [!ex1!df-alt3 x φ y]
        [b2i exp [∃ x φ] [∃ y ∀ x → φ = x y] [∃1 x φ]]
        [com12i [∃ x φ] [∃ y ∀ x → φ = x y] [∃1 x φ]]
        [>bii [→ ∃ x φ ∃1 x φ] [∃ y ∀ x → φ = x y]]
        [bitr [∃? x φ] [→ ∃ x φ ∃1 x φ] [∃ y ∀ x → φ = x y]]))

  (th "df-alt3" (var "x" wff "φ" var "y")
      (dist (!x !y))
      (hypo [Bvp y φ])
      (ass [↔ ∃? x φ ∀ x ∀ y → ∧ φ sb y x φ = x y])
      (proof
        [df-alt2 x φ y]
        [df-alt0 x y φ]
        [bitr [∃? x φ] [∃ y ∀ x → φ = x y] [∀ x ∀ y → ∧ φ sb y x φ = x y]]))

  (th "df-alt4" (var ![x y] wff ![φ ψ])
      (dist (!x !y) (!φ !y))
      (hypo [Bvp x ψ] [→ = x y ↔ φ ψ])
      (ass [↔ ∃? x φ ∀ x ∀ y → ∧ φ ψ = x y])
      (proof
        [ax17-bv y φ]
        [df-alt3 x φ y]
        [!subst!xpl y x φ ψ]
        [infer !an!eqt> [sb y x φ] ψ φ]
        [infer !im!eqt< [∧ φ sb y x φ] [∧ φ ψ] [= x y]]
        [!al!eqti y [→ ∧ φ sb y x φ = x y] [→ ∧ φ ψ = x y]]
        [!al!eqti x [∀ y → ∧ φ sb y x φ = x y] [∀ y → ∧ φ ψ = x y]]
        [bitr [∃? x φ] [∀ x ∀ y → ∧ φ sb y x φ = x y] [∀ x ∀ y → ∧ φ ψ = x y]]))

  (th "eqtd" (var "x" wff ![φ ψ χ])
      (hypo [Bvp x φ] [→ φ ↔ ψ χ])
      (ass [→ φ ↔ ∃? x ψ ∃? x χ])
      (proof
        [!ex!eqtd x φ ψ χ]
        [!ex1!eqtd x φ ψ χ]
        [!im!eqtd φ [∃ x ψ] [∃ x χ] [∃1 x ψ] [∃1 x χ]]
        [df-Ex? x ψ]
        [df-Ex? x χ]
        [!bi!<trg φ [→ ∃ x ψ ∃1 x ψ] [→ ∃ x χ ∃1 x χ] [∃? x ψ] [∃? x χ]]))

  (th "eqti" (var "x" wff ![φ ψ])
      (hypo [↔ φ ψ])
      (ass [↔ ∃? x φ ∃? x ψ])
      (proof
        [= x x]
        [!bv!true x [= x x]]
        [infer ax1 [↔ φ ψ] [= x x]]
        [infer eqtd x [= x x] φ ψ]))

  (th "bv-b" (var "x" wff "φ")
      (ass [Bvp x ∃? x φ])
      (proof
        [Bvp x [∃ x φ]]
        [Bvp x [∃1 x φ]]
        [Bvp x [→ ∃ x φ ∃1 x φ]]
        [df-Ex? x φ]
        [!bv!def x [∃? x φ] [→ ∃ x φ ∃1 x φ]]))
  (th-pattern !bv-b)

  (th "bv-" (var "y" wff "φ" var "x")
      (hypo [Bvp x φ])
      (ass [Bvp x ∃? y φ])
      (proof
        [Bvp x [∃1 y φ]]
        [Bvp x [∃  y φ]]
        [Bvp x [→ ∃ y φ ∃1 y φ]]
        [df-Ex? y φ]
        [!bv!def x [∃? y φ] [→ ∃ y φ ∃1 y φ]]))
  (th-pattern !bv-)

  (th "cbv" (var ![x y] wff ![φ ψ])
      (dist (!x !y))
      (hypo [Bvp y φ] [Bvp x ψ] [→ = x y ↔ φ ψ])
      (ass [↔ ∃? x φ ∃? y ψ])
      (proof
        [cbvex x y φ ψ]
        [!ex1!cbv x y φ ψ]
        [!im!eqti [∃ x φ] [∃ y ψ] [∃1 x φ] [∃1 y ψ]]
        [df-Ex? x φ]
        [df-Ex? y ψ]
        [!bi!<tr [→ ∃ x φ ∃1 x φ] [→ ∃ y ψ ∃1 y ψ] [∃? x φ] [∃? y ψ]])))

(in-context !ex1
  (th "df-alt5" (var "x" wff "φ")
      (ass [↔ ∃1 x φ ∧ ∃ x φ ∃? x φ])
      (loc var "y")
      (proof
        [ax17-bv y φ]
        [df-alt3 x φ y]
        [!ex?!df-alt2 x φ y]
        [infer !an!eqt> [∃? x φ] [∃ y ∀ x → φ = x y] [∃ x φ]]
        [b2i bitr [∃1 x φ] [∧ ∃ x φ ∃ y ∀ x → φ = x y] [∧ ∃ x φ ∃? x φ]]))

  (th "df-alt4" (var ![x y] wff ![φ ψ])
      (dist (!x !y) (!x !ψ) (!y !φ))
      (hypo [→ = x y ↔ φ ψ])
      (ass [↔ ∃1 x φ ∧ ∃ x φ ∀ x ∀ y → ∧ φ ψ = x y])
      (proof
        [df-alt5 x φ]
        [distv !ex?!df-alt4 x y φ ψ]
        [infer !an!eqt> [∃? x φ] [∀ x ∀ y → ∧ φ ψ = x y] [∃ x φ]]
        [bitr [∃1 x φ] [∧ ∃ x φ ∃? x φ] [∧ ∃ x φ ∀ x ∀ y → ∧ φ ψ = x y]])) )

  (th "Ex1->Ex?" (var "x" wff "φ")
      (ass [→ ∃1 x φ ∃? x φ])
      (proof
        [!ex1!df-alt5 x φ]
        [b2i ded !an!sim> [∃ x φ] [∃? x φ] [∃1 x φ]]))

  (th "ex?mid" (var "x" wff "φ")
      (ass [∨ ∃ x φ ∃? x φ])
      (proof
        [<neg [∃ x φ] [∃1 x φ]]
        [df-Ex? x φ]
        [b2i syl [¬ ∃ x φ] [→ ∃ x φ ∃1 x φ] [∃? x φ]]
        [infer df-or [∃ x φ] [∃? x φ]]))
#|
  ;;;;;;; Uniqueness and "at most one" with substitution and set terms

  (th "mo-sb1" (set "a" var "x" wff "φ")
      (dist (!x !a))
      (ass [→ ∀ x → φ = x a ∃mo x φ])
      (loc var "z")
      (proof
        [!eq!eqt> z a x]
        [bi.bldim>d [= z a] [= x z] [= x a] φ]
        [(distv !bi.bldald) x [= z a] [→ φ = x z] [→ φ = x a]]
        [ax17-bv z [∀ x → φ = x a]]
        [(bi< !sb-xplw) a z [∀ x → φ = x z] [∀ x → φ = x a]]
        [(deduct !sb-spec-ex) a z [∀ x → φ = x z] [∀ x → φ = x a]]
        [(distv (bi< !df-∃mo-alt2)) x φ z]
        [syl [∀ x → φ = x a] [∃ z ∀ x → φ = x z] [∃mo x φ]]))

  ;; TODO: get rid of (!x !a)
  (th "mo-sb2" (set ![a b] var "x" wff "φ")
      (dist (!x !a))
      (ass [→ ∃mo x φ → ∧ sb a x φ sb b x φ = a b])
      (loc var "y")
      (proof
        [(bi> (distv !df-∃mo-alt3)) x φ y]
        [(deduct !spec) a x [∀ y → ∧ φ sb y x φ = x y] [∃mo x φ]]
        [!eq!eqt< x a y]
        [eq-sb>b a x φ]
        [(deduct !bi>) φ [sb a x φ] [= x a]]
        [bi.bldan<d [= x a] φ [sb a x φ] [sb y x φ]]
        [bi.bldim<>d [= x a] [∧ φ sb y x φ] [∧ sb a x φ sb y x φ] [= x y] [= a y]]
        [(distv !bi.bldald) y [= x a] [→ ∧ φ sb y x φ = x y] [→ ∧ sb a x φ sb y x φ = a y]]
        ;; hbuilder
          [ax17-bv x [= a y]]
          [bv-sbb-dv a x φ]
          [bv-sbb-dv y x φ]
          [bv-∧ [sb a x φ] [sb y x φ] x]
          [bv-→ [∧ sb a x φ sb y x φ] [= a y] x]
          [bv-∀ y [→ ∧ sb a x φ sb y x φ = a y] x]
        [sb-xplw a x [∀ y → ∧ φ sb y x φ = x y] [∀ y → ∧ sb a x φ sb y x φ = a y]]
        [brpi22 [∃mo x φ] [sb a x ∀ y → ∧ φ sb y x φ = x y] [∀ y → ∧ sb a x φ sb y x φ = a y]]
        [(deduct !spec) b y [→ ∧ sb a x φ sb y x φ = a y] [∃mo x φ]]
        [!eq!eqt> y b a]
        [eq-sb>b b y [sb y x φ]]
        [(distv !sbco2w) b y x φ]
        [(infer !ax1) [↔ sb b y sb y x φ sb b x φ] [= y b]]
        [bitrd [= y b] [sb y x φ] [sb b y sb y x φ] [sb b x φ]]
        [(deduct !bi>) [sb y x φ] [sb b x φ] [= y b]]
        [bi.bldan>d [= y b] [sb y x φ] [sb b x φ] [sb a x φ]]
        [bi.bldim<>d [= y b] [∧ sb a x φ sb y x φ] [∧ sb a x φ sb b x φ] [= a y] [= a b]]
        [ax17-bv y [→ ∧ sb a x φ sb b x φ = a b]]
        [sb-xplw b y [→ ∧ sb a x φ sb y x φ = a y] [→ ∧ sb a x φ sb b x φ = a b]]
        [brpi22 [∃mo x φ] [sb b y → ∧ sb a x φ sb y x φ = a y] [→ ∧ sb a x φ sb b x φ = a b]]
        ))

  ;; Contraposition of mo-sb2
  ;; also gets rid of the dvc (a . x)
  ;; If φ is true for two non-equal sets, [∃mo x φ] is false
  (th "mo-sb2con" (set ![a b] var "x" wff "φ")
      (ass [→ ∧ sb a x φ sb b x φ → ¬ = a b ¬ ∃mo x φ])
      (loc var "y")
      (proof
        [mo-sb2 y b x φ]
        [com12i [∃mo x φ] [∧ sb y x φ sb b x φ] [= y b]]
        [(deduct !con) [∃mo x φ] [= y b] [∧ sb y x φ sb b x φ]]
        
        [sb-theor a y [→ ∧ sb y x φ sb b x φ → ¬ = y b ¬ ∃mo x φ]]
        [(infer> !sb-im) a y [∧ sb y x φ sb b x φ] [→ ¬ = y b ¬ ∃mo x φ]]

        [ax17-bv y φ]
        [ax17-bv y [sb b x φ]]
        [sbco2 a y x φ]
        [sb-bv a y [sb b x φ]]
        [bi.bldan<> [sb a y sb y x φ] [sb a x φ]
                    [sb a y sb b x φ] [sb b x φ]]
        [(bi< !sb-an) a y [sb y x φ] [sb b x φ]]
        [brpi21< [∧ sb a y sb y x φ sb a y sb b x φ] [sb a y ∧ sb y x φ sb b x φ]
                 [∧ sb a x φ sb b x φ]]

        [(bi> !sb-im) a y [¬ = y b] [¬ ∃mo x φ]]
        [sb-eqat< y b a]
        [bi.bldneg [sb a y = y b] [= a b]]
        [sb-neg a y [= y b]]
        [bitr [sb a y ¬ = y b] [¬ sb a y = y b] [¬ = a b]]
        [ax17-bv y [¬ ∃mo x φ]]
        [sb-bv a y [¬ ∃mo x φ]]
        [bi.bldim<> [sb a y ¬ = y b] [¬ = a b]
                    [sb a y ¬ ∃mo x φ] [¬ ∃mo x φ]]
        [brpi22 [sb a y → ¬ = y b ¬ ∃mo x φ] [→ sb a y ¬ = y b sb a y ¬ ∃mo x φ]
                [→ ¬ = a b ¬ ∃mo x φ]]

        [3syl [∧ sb a x φ sb b x φ] [sb a y ∧ sb y x φ sb b x φ]
              [sb a y → ¬ = y b ¬ ∃mo x φ] [→ ¬ = a b ¬ ∃mo x φ]]))

  (th "mo-sb3" (set "a" var "x" wff "φ")
      (dist (!x !a))
      (ass [→ ∧ sb a x φ ∃mo x φ ∀ x → φ = x a])
      (proof
        [mo-sb2 a x x φ]
        [exp3a [∃mo x φ] [sb a x φ] [sb x x φ] [= a x]]
        [imp [∃mo x φ] [sb a x φ] [→ sb x x φ = a x]]
        [(bi< !sbid) x φ]
        [!im!rp32 [∧ ∃mo x φ sb a x φ] [sb x x φ] [= a x] φ]
        [eqcomi a x]
        [!im!rp33 [∧ ∃mo x φ sb a x φ] φ [= a x] [= x a]]
        [(+syl> !ancom) [sb a x φ] [∃mo x φ] [→ φ = x a]]
        [bv-∃mob x φ]
        [bv-sbb-dv a x φ]
        [bv-∧ [sb a x φ] [∃mo x φ] x]
        [imgen>i x [∧ sb a x φ ∃mo x φ] [→ φ = x a]]))

  (th "eu-sb1" (set "a" var "x" wff "φ")
      (dist (!x !a))
      (ass [→ ∧ sb a x φ ∀ x → φ = x a ∃1 x φ])
      (proof
        [siman> [sb a x φ] [∀ x → φ = x a]]
        [(deduct !mo-sb1) a x φ [∧ sb a x φ ∀ x → φ = x a]]
        [siman< [sb a x φ] [∀ x → φ = x a]]
        [(deduct !sb-spec-ex) a x φ [∧ sb a x φ ∀ x → φ = x a]]
        [jca [∧ sb a x φ ∀ x → φ = x a] [∃ x φ] [∃mo x φ]]
        [df-∃1-alt5 x φ]
        [brpi22< [∧ sb a x φ ∀ x → φ = x a] [∧ ∃ x φ ∃mo x φ] [∃1 x φ]]))

  (th "eu-sb2" (set "a" var "x" wff "φ")
      (dist (!x !a))
      (ass [↔ ∧ sb a x φ ∀ x → φ = x a ∧ sb a x φ ∃1 x φ])
      (proof
        [eu-sb1 a x φ]
        [siman< [sb a x φ] [∀ x → φ = x a]]
        [jca [∧ sb a x φ ∀ x → φ = x a] [sb a x φ] [∃1 x φ]]
        ;; --
        [mo-sb3 a x φ]
        [∃1→∃mo x φ]
        [im.bldan> [∃1 x φ] [∃mo x φ] [sb a x φ]]
        [syl [∧ sb a x φ ∃1 x φ] [∧ sb a x φ ∃mo x φ] [∀ x → φ = x a]]
        [siman< [sb a x φ] [∃1 x φ]]
        [jca [∧ sb a x φ ∃1 x φ] [sb a x φ] [∀ x → φ = x a]]
        ;; --
        [>bii [∧ sb a x φ ∀ x → φ = x a] [∧ sb a x φ ∃1 x φ]]))|#

) ; end module "unique"
