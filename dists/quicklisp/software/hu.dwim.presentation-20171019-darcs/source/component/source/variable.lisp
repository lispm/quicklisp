;;; -*- mode: Lisp; Syntax: Common-Lisp; -*-
;;;
;;; Copyright (c) 2009 by the authors.
;;;
;;; See LICENCE for details.

(in-package :hu.dwim.presentation)

;;;;;;
;;; symbol/special-variable-name/inspector

(def (component e) symbol/special-variable-name/inspector (t/inspector)
  ())
