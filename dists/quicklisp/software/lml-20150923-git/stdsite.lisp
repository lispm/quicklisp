;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          stdsite.lisp
;;;; Purpose:       Functions to create my standard style sites
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  Aug 2002
;;;;
;;;; $Id$
;;;;
;;;; This file, part of LML, is Copyright (c) 2002 by Kevin M. Rosenberg
;;;;
;;;; LML users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License v2
;;;; (http://www.gnu.org/licenses/gpl.html)
;;;; *************************************************************************

;;; A "standard site" is a format for a certain style of web page.
;;; It is based on the LML package.
;;; A stdsite page expects to include the following files:
;;;  head.lml_
;;;  banner.lml_
;;;  content.lml_
;;;  footer.lml_

(in-package #:lml)

(defmacro std-head (title &body body)
  `(head
    (title ,title)
    (lml-load "head.lml_")
    ,@body))


(defun std-footer (file)
  (div-c "disclaimsec"
    (let ((src-file (make-pathname
                     :defaults *sources-dir*
                     :type "lml"
                     :name (pathname-name file))))
      (when (probe-file src-file)
        (div-c "lastmod"
          (lml-format  "Last modified: ~A" (date-string (file-write-date src-file))))))
    (lml-load "footer.lml_"))
  (values))


(defmacro std-body (file &body body)
  `(body
    (lml-load "banner.lml_")
    (table-c "stdbodytable" :border "0" :cellpadding "3"
             (tbody
              (tr :valign "top"
                  (td-c "stdcontentcell"
                        (lml-load "contents.lml_"))
                  (td :valign "top"
                      ,@body
                      (std-footer ,file)))))
    (lml-load "final.lml_")))


(defmacro print-std-page (file title &body body)
  `(progn
     (xhtml-prologue)
     (html :xmlns "http://www.w3.org/1999/xhtml"
           (std-head ,title)
           (std-body ,file ,@body))))

(defmacro std-page (out-file title &body body)
  `(let ((*indent* 0))
     (with-open-file (*html-output* (lml-file-name ',out-file :output)
                      :direction :output
                      :if-exists :supersede)
       (print-std-page (lml-file-name ',out-file :source) ,title ,@body))))

(defmacro titled-pre-section (title &body body)
  `(progn
     (h1 ,title)
     (pre :style "padding-left:30pt;"
          ,@body)))



