# DO NOT EDIT --- generated from source code.
#+LaTeX: \label{def-criterion:primary}
The =def-criterion= macro defines a new criterion for use in NST tests.
These criteria definitions are like generic function method definitions with two
sets of formal parameters: the forms provided as the actual parameters of the
criterion itself, and the values arising from the evaluation of the forms under
test.
#+begin_example
(def-criterion (name criterion-lambda-list values-lambda-list)
  [ doc-string )
  form
  form
  ...
  form)
#+end_example

- name :: Name of the criterion.
- criterion-lambda-list :: Lambda list for the arguments to the criterion.  Optionally, the first element of the list is a symbol specifying the parameter-passing semantics for the criterion arguments: =:values= for call-by-value, or =:forms for call-by-name (the default).  The list may include the keywords =&key=, =&optional=, =&body= and =&rest= but may not use =&whole= or =&environment=.  Apart from this restriction, in the former case the list may be any ordinary lambda list as for =defun=, and in the latter case the list may be any macro lambda list as for =defmacro=.
- values-lambda-list :: Lambda list for the forms under test.  Optionally, the first element of the list is a symbol specifying the parameter-passing semantics for the criterion arguments: :values for call-by-value (the default), or :form for call-by-name.  In the former case, the list may include the keywords =&key=, =&optional=, =&body= and =&rest=, but not =&whole= or =&environment=; apart from that restriction, list may be any ordinary lambda list as for =defun=.  In the latter case, the remainder of the list must contain exactly one symbol, to which a form which would evaluate to the values under test will be bound.
                        If the criterion ignores the values, then instead of a lambda list, this argument may be the symbol =:ignore=.  On many platforms, listing a dummy parameter which is then =declare=d =ignore= or =ignorable= will produce a style warning: the body of a =def-criterion= should not be assumed to correspond directly to the body of a =defmethod=; in general there will be surrounding =destructuring-bind=s.
- documentation :: An optional documentation string for the criterion.
- form :: The body of the criterion definition should return a test result report contructed with the =make-success-report=, etc. functions.
Examples:
#+begin_example
(def-criterion (:true () (bool))
  (if bool
      (make-success-report)
      (make-failure-report :format "Expected non-null, got: ~s"
                    :args (list bool))))

(def-criterion (:eql (target) (actual))
  (if (eql (eval target) actual)
      (make-success-report)
      (make-failure-report :format "Not eql to value of ~s"
                    :args (list target))))
#+end_example


