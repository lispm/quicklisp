(symkind "SET")
(symkind "VAR" :super set)
(symkind "WFF")

(module "logic"
  (prim wff "↔" (wff ![φ ψ]))
  (prim wff "→" (wff ![φ ψ]))
  (prim wff "¬" (wff "φ"))
  (prim wff "∧" (wff ![φ ψ]))
  (prim wff "∨" (wff ![φ ψ]))
  (prim wff "∀" (var "x" wff "φ"))
  (prim set "∅" ())
  (prim wff "=" (set ![x y])))

(theory "Topo" (set "X")
  (import [!/logic])
  (prim wff "T-space" ())
  (hypo [T-space])

  (prim set "clos" (set "A"))

  (ax "clos-th" (set "A")
      (ass [= clos clos A clos A])))

(prim set "test-T" ())
(ax "ax-test-T" ()
    (ass [!/Topo!T-space test-T]))

(theory "Metric" (set "d")
  (import [!/logic])
  (prim wff "M-space" ())
  (hypo [M-space])

  (format t "******Declare induced-topo~%")
  (prim set "induced-topo" ())

  (format t "******Axiom Metric-to-topo~%")
  (ax "Metric-to-Topo" ()
      (ass [!/Topo!T-space induced-topo]))

  (format t "******Export !/Topo~%")
  (export [!/Topo induced-topo]
          :proof (linear-subproof ([M-space]) ([!/Topo!T-space induced-topo])
                   [Metric-to-Topo]))

  (format t "******Theorem M-clos~%")
  (th "M-clos" (set "A")
    (ass [= clos clos A clos A])
    (proof [clos-th A])))

(prim set "test-M" ())
(ax "ax-test-M" ()
    (ass [!/Metric!M-space test-M]))

(theory "Normed" (set "||")
  (import [!/logic])
  (prim wff "B-space" ())
  (hypo [B-space])

  (format t "******Declare induced-metric~%")
  (prim set "induced-metric" ())

  (format t "******Axiom Metric-to-topo~%")
  (ax "Norm-to-Metric" ()
      (ass [!/Metric!M-space induced-metric]))

  (format t "******Export !/Metric~%")
  (export [!/Metric induced-metric]
          :proof (linear-subproof ([B-space]) ([!/Metric!M-space induced-metric])
                   [Norm-to-Metric]))

  (format t "******Theorem Norm-clos~%")
  (th "Norm-clos" (set "A")
    (ass [= clos clos A clos A])
    (proof [clos-th A])))

(theory "Hilbert" (set "<,>")
  (import [!/logic])
  (prim wff "H-space" ())
  (hypo [H-space])

  (format t "******Declare induced-norm~%")
  (prim set "induced-norm" ())

  (format t "******Axiom Hilbert-to-Banach~%")
  (ax "Hilbert-to-Banach" ()
      (ass [!/Normed!B-space induced-norm]))

  (format t "******Export !/Normed~%")
  (export [!/Normed induced-norm]
          :proof (linear-subproof ([H-space]) ([!/Normed!B-space induced-norm])
                   [Hilbert-to-Banach]))

  (format t "******Theorem M-clos~%")
  (th "H-clos" (set "A")
    (ass [= clos clos A clos A])
    (proof [clos-th A])))
