(defpackage :levenshtein
  (:use :cl)
  (:export #:distance))
