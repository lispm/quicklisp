;;; -*- mode: Lisp; Syntax: Common-Lisp; -*-
;;;
;;; Copyright (c) 2009 by the authors.
;;;
;;; See LICENCE for details.

(in-package :hu.dwim.presentation)

;;;;;;
;;; primitive/editor

(def (component e) primitive/editor (primitive/presentation t/editor)
  ()
  (:documentation "A PRIMITIVE/EDITOR edits existing values of primitive TYPEs."))
