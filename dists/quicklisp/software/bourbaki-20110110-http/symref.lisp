;;; Copyright (c) Juha Arpiainen 2005-06

;;; This file is part of Bourbaki.

;;; Bourbaki is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.

;;; Bourbaki is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Bourbaki; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; -------

;;; *****
;;; Symbol references (symrefs)
;;; *****

(in-package :bourbaki)

;(defun compose-proof (lhs rhs)
;  (if (null (symref-proof rhs))
;    (symref-proof lhs)
    ;; This assumes lhs is exact
;    (let ((smap (smap (symref-target rhs)
;                      (apply (symref-fn lhs)
;                             (r-int-list (symref-args-needed lhs))))))
;      (append (symref-proof lhs)
;              (mapcar #'(lambda (x) (replace-vars x smap))
;                      (symref-proof rhs))))))

;; Assume at least one of lhs,rhs is not nil
(defun compose-proof (lhs rhs)
  ;; hypo -> lhs.hypo
  ;; assr -> rhs.assr
  ;; sub -> append lhs.sub (use lhs-fn on rhs.sub)
  #'(lambda (&rest vars)
      (let ((lhs-proof (aif (symref-proof lhs) (apply it (subseq vars (symref-args-needed rhs)))))
            (rhs-proof (aif (symref-proof rhs) (apply it (simple-splice vars (symref-args-needed rhs) (symref-fn lhs))))))
        (make-subproof :hypo   (aif lhs-proof (subproof-hypo it)   (subproof-hypo rhs-proof))
                       :assert (aif rhs-proof (subproof-assert it) (subproof-assert lhs-proof))
                       :ref nil
                       :sub (append (aif lhs-proof (subproof-sub it))
                                    (aif rhs-proof (subproof-sub it)))))))

;; TODO: Use something like :fn #'(lambda (&rest foo) (apply (context-fn ctx) fn2 foo))
;; to clean the :arg hack in seek-sym
;;  ( (context-fn ctx) would be (lambda (fn &rest foo) (splice foo (vars-needed this) fn))
;;    for most symbols, but (lambda (foo) nil) for locals )
(defun compose-ref (lhs rhs)
  "Helper function for seek-sym.
Creates a composed symref from a context and a symref."
  (let ((fn (if (> 0 (symref-args-needed rhs))
              #'(lambda (&rest x)
                  (apply (symref-fn rhs) (splice x (symref-args-needed lhs) (symref-fn lhs))))
              #'(lambda (&rest x)
                  (apply (symref-fn rhs) (simple-splice x (symref-args-needed rhs) (symref-fn lhs)))))))
    (make-symref :target (symref-target rhs)
                 :class :parse
                 :args-needed (if (> 0 (symref-args-needed rhs))
                                -1
                                (+ (symref-args-needed lhs) (symref-args-needed rhs)))
                 :fn fn
                 :proof (if (or (symref-proof lhs) (symref-proof rhs)) (compose-proof lhs rhs)))))

(defun apply-ref (ref args)
  (aif (symref-target ref)
    (cons it (nreverse (apply (symref-fn ref) args)))
    (apply (symref-fn ref) args)))

(defun apply-ref-proof (ref args)
  (apply (symref-proof ref) args))

(defun ref-to-pattern (ref)
  (apply-ref ref (r-int-list (symref-args-needed ref))))

(defun pattern-to-ref (pat &optional class)
  (let ((highest (highest-number-in pat)))
    (make-symref :target (car pat)
                 :class class
                 :args-needed (1+ highest)
                 :proof nil
                 :fn #'(lambda (&rest x)
                         (nreverse (sublis (pairlis (r-int-list (1+ highest)) x)
                                           (cdr pat)))))))
