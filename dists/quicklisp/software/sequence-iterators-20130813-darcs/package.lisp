
(in-package :cl-user)

(defpackage :tcr.sequence-iterators
  (:use :common-lisp :tcr.parse-declarations-1.0)
  (:nicknames :sequence-iterators)
  (:export #:canonicalize-key
           #:canonicalize-test
           #:check-sequence-bounds
           #:dosequence
           #:dosequences*
           #:do-sequence-iterator
           #:do-sequence-iterators
           #:do-sequence-iterators*
           #:make-sequence-like
           #:sequence-bounding-indices-error
           #:sequence-index-error
           #:sequence-index
           #:sequence-length
           #:sequence-out-of-bounds-error
           #:sequence-out-of-bounds-error--end
           #:sequence-out-of-bounds-error--index
           #:sequence-out-of-bounds-error--sequence
           #:sequence-out-of-bounds-error--start
           #:validate-sequence-bounds
           #:with-sequence-iterator)
  (:documentation
   "
* Required Libraries

  This library depends on parse-declarations, available at

    http://common-lisp.net/project/parse-declarations/

* Notes on the API

  This library has not been publically released yet. The API as
  described here should be pretty stable, and changes should hopefully
  only happen as additions. (No guarantee on that, of course.)

  If you find use of this library, I'm eager to receive feedback about
  inconveniences or improvements to the API.

* Examples

  There's a presentation available at

    http://common-lisp.net/~trittweiler/talks/sequence-iterators-2009.pdf

  which is an introduction to the library, including examples.

* Acknowledgements

  I'd like to thank Paul Khuong and Christophe Rhodes for valuable
  comments regarding details of the API.
"))
