;;; -*- mode: lisp; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; nlsolve.lisp - Provide nonlinear solvers
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2003 Nicolas Neuss, University of Heidelberg.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF HEIDELBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.iteration)

(file-documentation
 "This file provides some solvers for nonlinear problems.")

(defclass <nonlinear-solver> (<discrete-iterative-solver>)
  ((linear-solver
    :reader linear-solver :initarg :linear-solver
    :documentation "The linear solver for solving the linearization."))
  (:documentation "Class for general nonlinear iterative solvers."))

(defmethod inner-iteration ((nlsolve <nonlinear-solver>))
  (and (slot-boundp nlsolve 'linear-solver)
       (linear-solver nlsolve)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Newton iteration
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass <newton> (<nonlinear-solver>)
  ((step-control
    :reader step-control :initform nil :initarg :step-control
    :documentation "NIL means that no step-control is done.  If
                 step-control is a number, then it denotes the maximum
                 number of halving steps which is tried before giving
                 up."))
  (:documentation "Class for the Newton iteration."))

;;; The following does not work, because blackboard is not available.  This
;;; might suggest a change in interface such that all solvers and
;;; strategies should carry their own blackboards around as a slot.
#+(or)
(defmethod slot-unbound (class (newton <newton>) slot)
  (case slot
    (linear-solver (select-linear-solver newton blackboard))))

(defmethod next-step ((newton <newton>) blackboard)
  "Simply calls the linear solver on the linearized problem."
  (with-items (&key solution residual residual-p linearization)
      blackboard
    (unless (slot-boundp newton 'linear-solver)
      (setf (slot-value newton 'linear-solver)
	    (select-linear-solver linearization blackboard)))
    (when (step-control newton)
      ;; remember old solution for step control
      (with-slots (old-solution) newton
        (if old-solution
            (copy! solution old-solution)
            (setf old-solution (copy solution)))))
    ;; perform the full newton step
    (solve (linear-solver newton)
           (blackboard :problem linearization :solution solution
                       :residual residual :residual-p t))
    ;; at the moment, the nonlinear residual is not correct anymore
    (setq residual-p nil)
    ;; a rather simple-minded way of step-control
    (when (step-control newton)
      (with-items (&key problem defnorm) blackboard
        (with-slots (old-solution) newton
          (loop with old-defnorm = defnorm
                repeat (step-control newton)
                for omega = 1.0 then (/ omega 2)
                for new-defnorm =
                                (progn (ensure-residual problem blackboard)
                                       (funcall (residual-norm newton) residual))
                until (< new-defnorm
                         (* (- 1.0 (* .5 omega))
                            old-defnorm))
                do
                   (axpy! 0.5 old-solution (scal! 0.5 solution))
                   (setq residual-p nil)
                ))))
    ))

(defvar *suggested-nonlinear-solver* nil
  "Can be used for very rudimentary influence on GPS.")

(defmethod select-solver ((problem <nonlinear-problem>) blackboard)
  (declare (ignore blackboard))
  (or *suggested-nonlinear-solver*
      (make-instance
       '<newton>
       :success-if '(and (> :step 1) (or (= :defnorm 0) (and (< :defnorm 1.0e-9) (> :step-reduction 0.9))))
       :failure-if '(and (> :step 1) (> :step-reduction 1.0) (> :defnorm 1.0e-9)))))


(defun qlsolve (f df rhs x0)
  "Solves a quasilinear equation of the form f(x)*x=r with a Newton iteration"
  (let ((nlse
          (nlse :linearization
                #'(lambda (solution)
                    (let ((x (mref solution 0 0)))
                      (lse :matrix (make-real-matrix (vector (+ (funcall f x)
                                                                (* (funcall df x) x))))
                           :rhs (make-real-matrix (vector (+ rhs (* x (funcall df x) x))))))))))
        (solve (make-instance
                '<newton> :output t
                          :success-if '(and (> :step 2) (or (< :defnorm 1e-14) (> :step-reduction 0.9)))
                          :observe (append *discrete-iterative-solver-observe*
                                           `(("                 solution" "~25,15,2E"
                                                                          ,#'(lambda (blackboard)
                                                                               (with-items (&key solution) blackboard
                                                                                 (vref solution 0)))))))
               (blackboard :problem nlse :solution (ensure-matlisp x0)))))

#+(or)
(defun qlsolve (f df rhs x0)
  "Solves a quasilinear equation of the form f(x)*x=r with a Newton iteration"
  (let ((nlse
          (nlse :linearization
                #'(lambda (x)
                    (lse :matrix (+ (funcall f x)
                                    (* (funcall df x) x))
                           :rhs (+ rhs (* x (funcall df x) x)))))))
        (solve (make-instance
                '<newton> :output t
                          :success-if '(and (> :step 2) (or (< :defnorm 1e-14) (> :step-reduction 0.9)))
                          :observe (append *discrete-iterative-solver-observe*
                                           `(("                 solution" "~25,15,2E"
                                                                          ,#'(lambda (blackboard)
                                                                               (with-items (&key solution) blackboard
                                                                                 solution))))))
               (blackboard :problem nlse :solution x0))))

;;;; Testing

(defun test-nlsolve ()

  (qlsolve #'identity (constantly 1.0) 2.0 1.0)

  (let* ((Ms 200.0)
         (mu0 (* 4 pi 1d-4))
         (xi 5.0)
         (H 1.0))
    (labels ((h (c)
               #I"2.0*xi*mu0*(1.0-c)*Ms*langevinx(xi*mu0*norm(H))")
             (dh (c)
               (declare (ignore c))
               #I"-2.0*xi*mu0*Ms*langevinx(xi*mu0*norm(H))"))
      (qlsolve #'h #'dh .2 0.7)))

  (let ((nlse
	 (nlse :linearization
	       #'(lambda (solution)
		   (let ((x (mref solution 0 0)))
		     (lse :matrix (make-real-matrix (vector (* 2.0 x)))
			  :rhs (make-real-matrix (vector (+ 2.0 (* x x))))))))))
    (solve (make-instance
	    '<newton> :output t
	    :success-if '(and (> :step 2) (> :step-reduction 0.9))
	    :observe (append *discrete-iterative-solver-observe*
			     `(("                 solution" "~25,15,2E"
				,#'(lambda (blackboard)
				    (with-items (&key solution) blackboard
				      (vref solution 0)))))))
	   (blackboard :problem nlse :solution #m(1.0))))
  )
  
;;;; Testing: (test-nlsolve)
(fl.tests:adjoin-test 'test-nlsolve)
