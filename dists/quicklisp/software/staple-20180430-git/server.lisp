#|
 This file is a part of Staple
(c) 2015 Shirakumo http://tymoon.eu (shinmera@tymoon.eu)
 Author: Nicolas Hafner <shinmera@tymoon.eu>
|#

(in-package #:cl-user)
(defpackage #:staple-server
  (:nicknames #:org.tymoonnext.staple.server)
  (:use #:cl)
  (:export
   #:start
   #:stop
   #:recache))
(in-package #:staple-server)

(defvar *acceptor* NIL)
(defvar *cache* NIL)

(defun start ()
  "Start the documentation server if it isn't already running.
This will launch an HTTP server on port 8080."
  (when *acceptor*
    (error "Server already running!"))
  (recache)
  (let ((acceptor (make-instance 'hunchentoot:easy-acceptor
                                 :port 8080
                                 :message-log-destination NIL
                                 :access-log-destination NIL)))
    (hunchentoot:start acceptor)
    (setf *acceptor* acceptor)
    (setf hunchentoot:*dispatch-table* (list #'handler))
    (setf hunchentoot:*show-lisp-errors-p* T)
    (setf hunchentoot:*show-lisp-backtraces-p* T)
    (format T "~&Your documentation browser is now running on http://localhost:8080/~%")))

(defun stop ()
  "Stop the documentation server if it is running."
  (unless *acceptor*
    (error "Server is not running!"))
  (hunchentoot:stop *acceptor*)
  (setf *acceptor* NIL))

(defun all-systems ()
  (let ((systems ()))
    (asdf:map-systems (lambda (sys) (push sys systems)))
    (sort systems #'string< :key #'asdf:component-name)))

(defun recache (&optional (systems (all-systems)))
  "Produce freshly cached documentation pages for the given list of ASDF systems."
  (setf *cache* (make-hash-table :test 'equalp))
  (format T "~&Recomputing system cache")
  (loop with failed = ()
        for system in systems
        for i = 0 then (mod (1+ i) 50)
        do (when (= 0 i) (fresh-line))
           (format T ".")
           (let* ((*standard-output* (make-broadcast-stream))
                  (*error-output* *standard-output*)
                  (*terminal-io* *standard-output*)
                  (*query-io* *standard-output*)
                  (*trace-output* *standard-output*))
             (handler-case
                 (handler-bind ((error (lambda (err)
                                         (pushnew system failed)
                                         (when (find-restart 'asdf:accept err)
                                           (invoke-restart 'asdf:accept))
                                         (when (find-restart 'continue err)
                                           (invoke-restart 'continue)))))
                   (show-system (etypecase system
                                  ((or string symbol) system)
                                  (asdf:system (asdf:component-name system)))))
               (condition (c) (declare (ignore c)))))
        finally (when failed
                  (format T "~&~%The following systems could not be processed cleanly:~%~a"
                          failed))))

(defun split (char string &key (start 0) (end (length string)))
  (loop with result = ()
        with buffer = (make-string-output-stream)
        for i from start below end
        for item = (elt string i)
        do (cond ((char= char item)
                  (push (get-output-stream-string buffer) result)
                  (setf buffer (make-string-output-stream)))
                 (T
                  (write-char item buffer)))
        finally (progn
                  (push (get-output-stream-string buffer) result)
                  (return (nreverse result)))))

(defun show-system-list ()
  (let ((*package* (find-package :staple)))
    (plump:serialize
     (clip:process (asdf:system-relative-pathname :staple-server "server.ctml")
                   :systems (all-systems))
     NIL)))

(defun system-url (name)
  (format NIL "/~a/" (hunchentoot:url-encode name)))

(defun find-package* (name)
  (or (find-package name)
      (find-package (string-upcase name))))

(defun starts-with-p (start string &key (test 'eql))
  (and (<= (length start) (length string))
       (loop for a across start
             for b across string
             always (funcall test a b))))

(defun smart-find-packages (name)
  (delete-if-not
   (lambda (name)
     (when (find-package* name)
       (do-external-symbols (symbol (find-package* name))
         (when (eql (find-package* name) (symbol-package symbol))
           (return T)))))
   (delete-duplicates
    (cons name
          (loop for package in (list-all-packages)
                when (starts-with-p name (package-name package) :test #'char-equal)
                collect (package-name package)))
    :test #'string-equal)))

(defun smart-find-logo (name)
  (let ((dir (asdf:system-source-directory name)))
    (when dir
      (dolist (file (uiop:directory-files dir))
        (when (search "logo" (pathname-name file) :test #'char-equal)
          (return
            (format NIL "~a.~a" (pathname-name file) (pathname-type file))))))))

(defun show-system (name)
  (or (gethash name *cache*)
      (setf (gethash name *cache*)
            (let ((page (staple:generate
                         name
                         :packages (smart-find-packages name)
                         :logo (smart-find-logo name)
                         :out T)))
              (lquery:$ page "head" "style,link" (remove))
              (lquery:$ page "head" (append "<link rel=\"stylesheet\" type=\"text/css\" href=\"/server.css\"/>"))
              (plump:serialize page NIL)))))

(defun handler (request)
  (let* ((path (hunchentoot:url-decode (hunchentoot:script-name request)))
         (dirs (split #\/ path :start 1)))
    (cond
      ((string= path "/server.css")
       (hunchentoot:handle-static-file
        (asdf:system-relative-pathname :staple-server "server.css")))
      ((and (null (cdr dirs))
            (string= (car dirs) ""))
       (lambda ()
         (show-system-list)))
      ((string= (car (last dirs)) "")
       (lambda ()
         (show-system (format NIL "~{~a~^/~}" (butlast dirs)))))
      (T
       (lambda ()
         (hunchentoot:handle-static-file
          (asdf:system-relative-pathname
           (format NIL "~{~a~^/~}" (butlast dirs))
           (car (last dirs)))))))))

