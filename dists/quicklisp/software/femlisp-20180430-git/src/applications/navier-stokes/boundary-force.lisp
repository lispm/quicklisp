(in-package :fl.flow-application)

(file-documentation "Routines for calculating the force along a
certain boundary part.  The boundary force is transformed into a
volume integral, which corresponds essentially to testing the
variational form with a matrix-valued test function which is the
(dim,dim) identity matrix along the boundary for which the forces have
to be computed and the (dim,dim) zero matrix on all other
boundaries.

The easiest way to specify a boundary along which the force is
computed is by adding the classifier :evaluate-force-boundary to the
domain specification.  Another is to supply an appropriate function
evaluate-force-boundary-p as a key parameter.")

(defun evaluate-force-boundary-test (mesh)
  "Returns a function which checks if a cell is on a boundary
  classified with the key :evaluate-force-boundary."
  (lambda (cell)
    (member :evaluate-force-boundary
            (patch-classification (patch-of-cell cell mesh)
                                  (domain mesh)))))

(defun make-special-local-vec (as cell evaluate-force-boundary-p)
  (let* ((vecfe (get-fe as cell))
         (subcells (subcells cell))
         (mesh (mesh as))
         (dim (dimension mesh)))
    (lret ((result (fl.discretization::make-local-vec as cell :multiplicity dim)))
      (loop for k from 0 below dim
            for fe across (components vecfe)
            for comp = (aref result k) do
              (do-dof (dof fe :type :dof)
                (when (funcall evaluate-force-boundary-p
                               (aref subcells (dof-subcell-index dof)))
                  (dbg :evaluate-force-boundary "Active: ~A~%" dof)
                  (setf (mref comp (dof-index dof) k) 1.0)))))))

(defvar *use-problem-p* nil)
(defvar *with-convection-p* t)

(defun drag/lift-contribution (cell solution evaluate-force-boundary-p
                               &key viscosity (eta 1.0))
  "The idea is to discretize locally a special problem containing the
stress tensor part of the momentum equation."
  (let* ((as (ansatz-space solution))
         (mesh (mesh as))
         (dim (dimension mesh))
         (problem (problem as))
         (viscosity
          (or viscosity
              (get-property problem 'viscosity)))
         (fe (get-fe as cell))
         (qrule (quadrature-rule fe))
         (patch (patch-of-cell cell mesh))
         (patch-properties (skel-ref (domain mesh) patch))
         (geometry (fe-cell-geometry
                    cell (integration-points qrule)
                    :weights (integration-weights qrule)
                    :metric (getf patch-properties 'FL.MESH::METRIC)
                    :volume (getf patch-properties 'FL.MESH::VOLUME)))
         (values (get-local-from-global-vec cell solution)))
    (assert (typep problem 'fl.navier-stokes::<navier-stokes-problem>))
    (unless viscosity
      (error "The viscosity is important for this function, and there
is no default value available.  Please provide a value."))
    (lret* ((coeffs (if *use-problem-p*
                        ;; ist nicht sicher, weil Femlisp auch GUGV verwenden könnte
                        (filter-applicable-coefficients
                         (coefficients-of-cell cell mesh problem)
                         cell patch :constraints nil)
                        (list* (fl.navier-stokes::dudv-coefficient dim viscosity)
                               (fl.navier-stokes::p-div-coefficient dim eta)
                               (when *with-convection-p*
                                 (make-coefficients-for
                                  problem 'FL.ELLSYS::C patch '(u)
                                  (lambda (u)
                                    (diagonal-sparse-tensor u dim)))))))
            (fe-paras (loop for obj in (required-fe-functions coeffs)
                         collect obj collect
                           (get-local-from-global-vec
                            cell (get-property problem (car obj)))))
            (fl.discretization::*use-pool-p* nil)
            (local-v (make-special-local-vec as cell evaluate-force-boundary-p))
            (lmat (make-filled-array '(3 3)
                                     :initializer (_ (zeros dim 1))))
            (result (zeros dim 1)))
      (fl.discretization::discretize-locally
       (problem as) coeffs fe qrule geometry
       :local-u values
       :local-v local-v
       :matrix lmat
       :residual-p t
       :fe-parameters fe-paras)
      ;; sum all contributions of the momentum part
      (dotimes (i dim)
        (dotimes (j (1+ dim))
          (m+! (aref lmat i j) result)))
      (dbg :evaluate-force-boundary "Cell contribution=~A" result)
      )))

(defun calculate-drag/lift (solution evaluate-force-boundary-p &key viscosity)
  "Calculate drag/lift on a portion of the boundary determined by the
predicate @arg{evaluate-force-boundary-p}.

The optional viscosity parameter allows to use this function, even if the
viscosity is not available as a property of the problem, and it also allows
to use it in the context of friction boundary laws with a friction
coefficient different from normal viscosity.  The 'viscosity' parameter
should then be equal to the friction coefficient in the boundary
condition."
  (let* ((mesh (mesh solution))
         (dim (dimension mesh)))
    (ensure evaluate-force-boundary-p
            (evaluate-force-boundary-test mesh))
    (lret ((result (zeros dim 1)))
      (doskel (cell mesh :where :surface :dimension :highest)
        (dbg :evaluate-force-boundary "Handling cell ~A" cell)
        (when (some evaluate-force-boundary-p (boundary cell))
          (m+! (drag/lift-contribution cell solution evaluate-force-boundary-p
                                       :viscosity viscosity)
               result))))))

