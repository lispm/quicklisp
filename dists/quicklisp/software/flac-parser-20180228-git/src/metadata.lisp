(in-package :flac-parser)

(defun block-type-name (header)
  (case (block-type header)
    (0 :stream-info)
    (1 :padding)
    (2 :application)
    (3 :seek-table)
    (4 :vorbis-comment)
    (5 :cue-sheet)
    (6 :picture)
    (127 :metadata-block-invalid)
    (otherwise :metadata-block-unknown)))

(defun last-block-p (metadata-block)
  (= (last-flag (header metadata-block)) 1))

(defun list-blocks-by-type (file block-type)
  (remove-if
   (complement
    (lambda (x)
      (eq (block-type-name (header x)) block-type)))
   (blocks (parse-tree file))))

(defun list-pictures-by-type (file picture-type)
  (remove-if
   (complement
    (lambda (x)
      (= (picture-type (data x)) picture-type)))
   (list-blocks-by-type file :picture)))

(defun seek-point-placeholder-p (seek-point)
  (= (sample seek-point) (1- (expt 2 64))))
