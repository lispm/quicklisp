(in-package :box.frame)

(simple-logger:define-message :debug :frame-manager.rate
  "Frame rate: ~,2f fps (~,3f ms/f)")

(simple-logger:define-message :debug :frame-manager.periodic-update
  "Periodic update performed (every ~d seconds).")
