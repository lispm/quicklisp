# gamebox-math

A high performance math library useful for making games.

## Overview

A library written in portable Common Lisp, providing support for common math functions related to
game development. Supported are:

* 2D vectors
* 3D vectors
* 4D vectors
* 2x2 matrices
* 3x3 matrices
* 4x4 matrices
* Quaternions
* Dual quaternions

## Install

``` lisp
(ql:quickload :gamebox-math)
```

## License

Copyright © 2014-2018 [Michael Fiano](mailto:mail@michaelfiano.com).

Licensed under the MIT License.
