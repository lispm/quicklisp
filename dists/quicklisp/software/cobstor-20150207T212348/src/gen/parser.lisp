;;;  parser.lisp --- restricted Cobol parser

;;;  Copyright (C) 2005, 2009 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: cobstor

#+cmu (ext:file-comment "$Module: parser.lisp $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

(in-package :cobstor-generator)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass field-declaration ()
  ((level :initarg :level
	  :reader field-level
	  :type integer)
   (name :initarg :name
	 :accessor field-name
	 :type (or string null)
	 :documentation
	 "Name of the field or NIL if it's a filler.")
   (size :lazy compute-field-size
	 :reader field-size
	 :documentation
	 "The size of the field in bytes.  This is not multiplied by times (below).")
   (redefines :initarg :redefines
	      :reader field-redefines
	      :initform nil
	      :type (or string null))
   (alternates :initarg :alternates
	       :accessor field-alternates
	       :initform '()
	       :type list)
   (times :type (or integer null)
	  :reader field-times
	  :initarg :times
	  :initform nil
	  :documentation
	  "Cardinality of this field."))
  (:metaclass lazy-metaclass)
  (:documentation
   "Abstract base class for field declarations.  This is not the
kind of objects instantiated at run-time in the proxy library."))

(defmethod print-object ((obj field-declaration) stream)
  (print-unreadable-object (obj stream :type t)
    (format stream "(~A)~A"
	    (field-level obj)
	    (field-name obj))))

(defclass leaf-field-declaration (field-declaration)
  ((type :initarg :type
	 :reader field-type
	 :type symbol)
   (length :type integer
	   :initarg :length
	   :reader field-length
	   :documentation
	   "The size in charcater positions the field occupies.
This is not its size in bytes.")
   (picture :initarg :picture
	    :reader field-picture
	    :type string)
   (signed :type boolean
	   :initarg :signed
	   :initform nil
	   :reader field-signed
	   :documentation
	   "True if field is a signed numeric field.")
   (sign-position :type (or keyword null)
		  :initform nil
		  :reader field-sign-position
		  :initarg :sign-position)
   (sign-separated :type boolean
		   :initform nil
		   :reader field-sign-separated
		   :initarg :sign-separated)
   (decimals :type (or integer null)
	     :reader field-decimals
	     :initform nil
	     :initarg :decimals
	     :documentation
	     "The number of decimals places in a numeric field.
These are included in length."))
  (:metaclass lazy-metaclass)
  (:documentation
   "Class for Cobol simple field declarations; those without
subfields."))

(defclass container-field-declaration (field-declaration)
  ((subfields :initarg :subfields
	      :accessor field-subfields
	      :initform '()
	      :type list))
  (:metaclass lazy-metaclass)
  (:documentation
   "Class for container objects; those made up of subfields."))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun field-name-as-cobol (decl)
  (or (field-name decl)
      "filler"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric compute-field-size (field)
  (:documentation
   "Return the size in bytes occupied by the binary encoding of field.
This doesn't take in count repetitions as for TIMES."))

(defmethod compute-field-size ((field container-field-declaration))
  (reduce #'+ (mapcar #'(lambda (f)
			  (* (compute-field-size f)
			     (or (field-times f)
				 1)))
		      (field-subfields field))))

(defmethod compute-field-size ((field leaf-field-declaration))
  (let ((props (cddr (field-declaration->sexp field))))
    (remf props :alternates)
    (remf props :times)
    (apply (cobstor::field-type->sizer (field-type field))
	   props)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric field-declaration->sexp (field-declaration)
  (:documentation
   "Return a sexp of a FIELD-DECLARATION object suitable to be used in
a DEFINE-COBOL-RECORD macro.  The sexp has the form:

  (name ToS . properties)

where ToS is a storage type in the case leaf fields, and it's a list
of subfields (same sexp format) in case of container fields."))

(defmethod field-declaration->sexp ((decl leaf-field-declaration))
  (macrolet ((prop (tag)
	       `(when (slot-value decl ',tag)
		  (list ,(keywordify tag) (slot-value decl ',tag)))))
    (append (list (field-name decl)
		  (field-type decl))
	    (prop length)
	    (prop times)
	    (prop decimals)
	    (prop signed)
	    (prop sign-separated)
	    (prop sign-position)
	    (awhen (slot-value decl 'alternates)
	      (list :alternates (mapcar #'field-declaration->sexp it))))))

(defmethod field-declaration->sexp ((decl container-field-declaration))
  (macrolet ((prop (tag reader)
	       `(awhen (,reader decl)
		  (list ,tag it))))
    (append (list (field-name decl)
		  (mapcar #'field-declaration->sexp (field-subfields decl)))
	    (prop :times field-times)
	    (awhen (field-alternates decl)
	      (list :alternates (mapcar #'field-declaration->sexp it))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun sexp->field-declaration (sexp)
  "Create FIELD-DECLARATION object from a sexp of the form found in a
DEFINE-COBOL-RECORD macro.  See FIELD-DECLARATION->SEXP for further
details."
  (destructuring-bind (name type &rest properties) sexp
    (if (listp type)
	(apply #'make-instance 'container-field-declaration
	       :name name
	       :subfields (sexp->field-declaration type)
	       properties)
	(apply #'make-instance 'leaf-field-declaration
	       :name name
	       :type type
	       properties))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun structure-records (parsed-fields)
  "Structure a list of fields into a list of trees according to the
level number which appears in front of each cbfield.  If in FIELDS
there is only one record (just one level 01) the returned list will
contain just one tree.  The PARSED-FIELDS list is made up of elements
of the kind:

  (LEVEL NAME TYPE . PROPERTIES-LIST)

TYPE can be NIL if field is a composite field (with subfields)."
  (labels ((find-field (name)
	     "Find a field by name in PARSED-FIELDS."
	     (car (member name parsed-fields
			  :key #'field-name
			  :test #'string-equal)))
	   (add-alternate (field alternate)
	     (push alternate (field-alternates field)))
	   (connect-redefinitions (fields)
	     "Redefining fields are collected under the redefined field."
	     (dolist (f fields)
	       (be redefines (field-redefines f)
		 (when redefines
		   (be redefined-field (find-field redefines)
		     (if redefined-field
			 (add-alternate redefined-field f)
			 (warn "Field ~A redefines unknown ~A; omitting."
			       (field-name f) redefines))))))))
    (be fields parsed-fields
      (labels ((collect (current-level current-field)
		 (if fields
		     (let* ((next-field (car fields))
			    (next-level (field-level next-field)))
		       (cond ((> next-level current-level)
			      (pop fields)
			      (unless (typep current-field 'container-field-declaration)
				(error "~A cannot contain subfields." current-field))
			      (setf (field-subfields current-field)
				    (collect next-level next-field))
			      (collect current-level current-field))
			     ((= next-level current-level)
			      (pop fields)
			      ;; If next-field redefines something
			      ;; else, exclude it from the list; in
			      ;; connect-redefinitions it was
			      ;; connected to another field.
			      (be following-fields (collect next-level next-field)
				(when (field-redefines next-field)
				  (pop following-fields))
				(cons current-field
				      following-fields)))
			     (t		; (< next-level current-field)
			      (list current-field))))
		     (list current-field))))
	;; first sort out all the redefinitions under the
	;; respective redefined fields
	(connect-redefinitions fields)
	;; then collect the fields in a hierarchical fashion;
	;; last
	(be first-field (pop fields)
	  (collect (field-level first-field) first-field))))))

(defun expand-picture (picture)
  "Return a new picture string where all repeated characters in a
PICTURE have been expanded.  Pictures of the form \"9(5)\" are
expanded to \"99999\"."
  (declare (type string picture))
  (with-output-to-string (out)
    (flet ((repeat-char (char times)
	     (dotimes (i times)
	       (write-char char out))))
      (loop
	 for previous-end = 0 then (1+ end)
	 for start = (position #\( picture :start previous-end)
	 while start
	 for end = (position #\) picture :start start)
	 for times = (parse-integer picture :start (1+ start) :end end)
	 do
	   (write-string picture out :start previous-end :end start)
	   (repeat-char (char picture (1- start)) (1- times))
	 finally
	   (write-string picture out :start previous-end)))))

(defun parse-picture (picture)
  "Interprete the meaning of a Cobol picture returning useful
facts like: the type, the total length of the field, the decimals
and the presence of the sign."
  (declare (type string picture))
  (setf picture (expand-picture picture))
  (flet ((numeric-p ()
	   (loop
	      for c across picture
	      always (find c "9VS" :test #'char-equal)))
	 (signed-p ()
	   (char-equal #\S (char picture 0)))
	 (decimals ()
	   (be comma (position #\V picture :test #'char-equal)
	     (when comma
	       (- (length picture) comma 1))))
	 (number-of-9s ()
	   (loop
	      for c across picture
	      when (char-equal #\9 c)
	      sum 1)))
    (if (numeric-p)
	(values 'numeric (number-of-9s) (decimals) (signed-p))
	(values 'string (length picture)))))

(defun make-field-declaration (field)
  "Get a parsed field and return a FIELD-DECLARATION object.
Guess from the picture string (property :PICTURE) of FIELD the
missing parameters such as the type, length, decimals, and so on
and so forth.  The input FIELD is a list of at least two
elements:

  (LEVEL NAME . PROPERTIES)

where NAME can be NIL for fillers."
  (be props (cddr field)
    (macrolet ((prop (name)
		 `(getf props ,name)))
      (be picture (prop :picture)
	(if picture
	    ;; it's a simple (leaf) field
	    (be encoding (prop :encoding)
	      (multiple-value-bind (type length decimals signed)
		  (parse-picture picture)
		(make-instance 'leaf-field-declaration
			       :level (car field)
			       :name (cadr field)
			       :type (or encoding type)
			       :picture picture
			       :length length
			       :decimals decimals
			       :signed signed
			       :times (prop :times)
			       :redefines (prop :redefines)
			       :sign-position (prop :sign-position)
			       :sign-separated (prop :sign-separated))))
	    ;; it's a structured field; return it unchanged
	    (make-instance 'container-field-declaration
			   :level (car field)
			   :name (cadr field)
			   :times (prop :times)
			   :redefines (prop :redefines)))))))

(defun parse-file (source-pathname)
  "Parse a Cobol file containing a data record description (and
only that) and return a list of its fields."
  (be grammar (force define-grammar)
    (with-parser-stream (stream source-pathname grammar)
      (loop
	 for field = (parse grammar 'data-description-entry stream)
	 until (eq field :eof)
	 unless (eq (car field) :fd)	;skip the FD lines
	 collect (make-field-declaration field)))))

(defun read-field-declarations (pathname)
  "Return the list of field declarations layed in a hyerarchical
form, found in Cobol file at PATHNAME."
  (structure-records (parse-file pathname)))

