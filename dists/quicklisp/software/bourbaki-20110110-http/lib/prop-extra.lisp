;;; Bourbaki -- Propositional Calculus, extra theorems from set.mm
;;; implication "→" -- negation "¬" -- biconditional "↔"
;;; conjunction "∧" -- disjunction "∨"
;;; Converted from the "set.mm" file of Metamath

;; Naming convention:
;; ax-   axiom
;; df-   definition
;; rpixy implication of x wffs, replace y'th
;; -c, -closed   closed form if -i is used more often
;; b-    biconditional version

;; Theorem groups:
;; rpiXY, comXYi, imabs*, con*
;; *bitr*

(module "prop-extra"

  (bimport [!!prop-ax])
  (bimport [!!prop])
  
  (bimport [!!prop-meta])

  (meta (:comment . "Propositional Calculus (extra theorems)"))

;;; ************
;;; Theorems
;;; ************

;;; I - Implication

(th "com34" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Commutation of antecedents. Swap 3rd and 4th."))
    (hypo [→ φ → ψ → χ → θ τ])
    (ass [→ φ → ψ → θ → χ τ])
    (proof
      [!im!com12 χ θ τ]
      [!im!rp33 φ ψ [→ χ → θ τ] [→ θ → χ τ]]))

(th "com24" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Commutation of antecedents. Swap 2nd and 4th."))
    (hypo [→ φ → ψ → χ → θ τ])
    (ass [→ φ → θ → χ → ψ τ])
    (proof
      [com34 φ ψ χ θ τ]
      [!im!com23i φ ψ θ [→ χ τ]]
      [com34 φ θ ψ χ τ]))

(th "com14" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Commutation of antecedents. Swap 1st and 4th."))
    (hypo [→ φ → ψ → χ → θ τ])
    (ass [→ θ → ψ → χ → φ τ])
    (proof
      [com24 φ ψ χ θ τ]
      [!im!com12i φ θ [→ χ → ψ τ]]
      [com24 θ φ χ ψ τ]))

(th "com4l" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Commutation of antecedents. Rotate left."))
    (hypo [→ φ → ψ → χ → θ τ])
    (ass [→ ψ → χ → θ → φ τ])
    (proof
      [com14 φ ψ χ θ τ]
      [!im!com3li θ ψ χ [→ φ τ]]))

(th "com4t" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Commutation of antecedents. Rotate twice."))
    (hypo [→ φ → ψ → χ → θ τ])
    (ass [→ χ → θ → φ → ψ τ])
    (proof
      [com4l φ ψ χ θ τ]
      [com4l ψ χ θ φ τ]))

(th "com4r" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Commutation of antecedents. Rotate right."))
    (hypo [→ φ → ψ → χ → θ τ])
    (ass [→ θ → φ → ψ → χ τ])
    (proof
      [com4t φ ψ χ θ τ]
      [com4l χ θ φ ψ τ]))

(th "a1dd" (pr ![φ ψ χ θ])
    (meta (:comment . "Deduction introducing a nested embedded antecedent."))
    (hypo [→ φ → ψ χ])
    (ass [→ φ → ψ → θ χ])
    (proof
      [(ded !ax1) [→ ψ χ] θ φ]
      [!im!com23i φ θ ψ χ]))

(th "mp2" (pr ![φ ψ χ])
    (meta (:comment . "A double modus ponens inference."))
    (hypo [φ] [ψ] [→ φ → ψ χ])
    (ass [χ])
    (proof
      [ax-mp φ [→ ψ χ]]
      [ax-mp ψ χ]))

(th "mpdd" (pr ![φ ψ χ θ])
    (meta (:comment . "A nested modus ponens deduction."))
    (hypo [→ φ → ψ χ] [→ φ → ψ → χ θ])
    (ass [→ φ → ψ θ])
    (proof
      [(ded !ax2) ψ χ θ φ]
      [mpd φ [→ ψ χ] [→ ψ θ]]))

(th "mpid" (pr ![φ ψ χ θ])
    (meta (:comment . "A nested modus ponens deduction."))
    (hypo [→ φ χ] [→ φ → ψ → χ θ])
    (ass [→ φ → ψ θ])
    (proof
      [(ded !ax1) χ ψ φ]
      [mpdd φ ψ χ θ]))

(th "mpdi" (pr ![φ ψ χ θ])
    (meta (:comment . "A nested modus ponens deduction."))
    (hypo [→ ψ χ] [→ φ → ψ → χ θ])
    (ass [→ φ → ψ θ])
    (proof
      [com12i φ ψ [→ χ θ]]
      [mpid ψ φ χ θ]
      [com12i ψ φ θ]))

(th "mpcom" (pr ![φ ψ χ])
    (meta (:comment . "Modus ponens inference with commutation of antecedents."))
    (hypo [→ ψ φ] [→ φ → ψ χ])
    (ass [→ ψ χ])
    (proof
      [com12i φ ψ χ]
      [mpd ψ φ χ]))

(th "syldd" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Nested syllogism deduction."))
    (hypo [→ φ → ψ → χ θ] [→ φ → ψ → θ τ])
    (ass [→ φ → ψ → χ τ])
    (proof
      [syl< θ τ χ]
      [!im!rp33 φ ψ [→ θ τ] [→ → χ θ → χ τ]]
      [mpdd φ ψ [→ χ θ] [→ χ τ]]))

(th "sylcom" (pr ![φ ψ χ θ])
    (meta (:comment . "Syllogism inference with commutation of antecedents."))
    (hypo [→ φ → ψ χ] [→ ψ → χ θ])
    (ass [→ φ → ψ θ])
    (proof
      [com12i φ ψ χ]
      [!im!trd ψ φ χ θ]
      [com12i ψ φ θ]))

(th "syl5com" (pr ![φ ψ χ θ])
    (meta (:comment . "Syllogism inference with commuted antecedents."))
    (hypo [→ φ → ψ χ] [→ θ ψ])
    (ass [→ θ → φ χ])
    (proof
      [!im!rp32 φ ψ χ θ]
      [com12i φ θ χ]))

(th "syl6com" (pr ![φ ψ χ θ])
    (meta (:comment . "Syllogism inference with commuted antecedents."))
    (hypo [→ φ → ψ χ] [→ χ θ])
    (ass [→ ψ → φ θ])
    (proof
      [!im!rp33 φ ψ χ θ]
      [com12i φ ψ θ]))

(th "syli" (pr ![φ ψ χ θ])
    (meta (:comment . "Syllogism inference with common nested antecedent."))
    (hypo [→ ψ → φ χ] [→ χ → φ θ])
    (ass [→ ψ → φ θ])
    (proof
      [com12i χ φ θ]
      [sylcom ψ φ χ θ]))

(th "pm2.43a" (pr ![φ ψ χ])
    (meta (:comment . "Inference absorbing redundant antecedent."))
    (hypo [→ ψ → φ → ψ χ])
    (ass [→ ψ → φ χ])
    (proof
      [!im!com23i ψ φ ψ χ]
      [(infer !im!abs) [ψ] [→ φ χ]]))

(th "pm2.43b" (pr ![φ ψ χ])
    (meta (:comment . "Inference absorbing redundant antecedent."))
    (hypo [→ ψ → φ → ψ χ])
    (ass [→ φ → ψ χ])
    (proof
      [pm2.43a φ ψ χ]
      [com12i ψ φ χ]))

(th "loolin" (pr ![φ ψ])
    (meta (:comment . "The Linearity Axiom.")
          (:description . "The linearity Axiom of the infinite-valued sentential logic
(L-infinity) of Lukasiewicz."))
    (ass [→ [→ → φ ψ → ψ φ] [→ ψ φ]])
    (proof
      [ax1 ψ φ]
      [(infer !syl>) ψ [→ φ ψ] [→ ψ φ]]
      [(ded !im!abs) ψ φ [→ → φ ψ → ψ φ]]))

(th "loowoz" (pr ![φ ψ χ])
    (meta (:comment . "The Linearity Axiom.")
          (:description . "An alternate for the linearity axiom of the infinite-valued
sentential logic (L-infinity) of Lukasiewicz, due to Barbara Wozniakowska,
<i>Reports on Mathematical Logic</i> 10, 129-137 (1978)."))
    (ass [→ [→ → φ ψ → φ χ] [→ → ψ φ → ψ χ]])
    (proof
      [ax1 ψ φ]
      [(infer !syl>) ψ [→ φ ψ] [→ φ χ]]
      [(ded !ax2) ψ φ χ [→ → φ ψ → φ χ]]))

;; II - Negation

(th "looinv" (pr ![φ ψ])
    (meta (:comment . "The Inversion Axiom.")
          (:description . "The Inversion Axiom of the infinite-valued
sentential logic (L-infinity) of Lukasiewicz. It essentially expresses
\"disjunction commutes.\" Theorem *2.69 of Principia Mathematica."))
    (ass [→ [→ → φ ψ ψ] [→ → ψ φ φ]])
    (proof
      [syl> [→ φ ψ] ψ φ]
      [peirce φ ψ]
      [!im!rp33 [→ → φ ψ ψ] [→ ψ φ] [→ → φ ψ φ] φ]))

(th "pm2.36" (pr ![φ ψ χ])
    (meta (:description . "Theorem *2.36 of Principia Mathematica p. 105."))
    (ass [→ → ψ χ → → ¬ φ ψ → ¬ χ φ])
    (proof
      [syl< ψ χ [¬ φ]]
      [con< φ χ]
      [!im!rp33 [→ ψ χ] [→ ¬ φ ψ] [→ ¬ φ χ] [→ ¬ χ φ]]))

(th "pm2.37" (pr ![φ ψ χ])
    (meta (:description . "Theorem *2.37 of Principia Mathematica p. 105."))
    (ass [→ → ψ χ → → ¬ ψ φ → ¬ φ χ])
    (proof
      [con< ψ φ]
      [(ded !syl>) [¬ φ] ψ χ [→ ¬ ψ φ]]
      [com12i [→ ¬ ψ φ] [→ ψ χ] [→ ¬ φ χ]]))

(th "pm2.38" (pr ![φ ψ χ])
    (meta (:description . "Theorem *2.38 of Principia Mathematica p. 105."))
    (ass [→ → ψ χ → → ¬ ψ φ → ¬ χ φ])
    (proof
      [con ψ χ]
      [(ded !syl>) [¬ χ] [¬ ψ] φ [→ ψ χ]]))

(th "pm2.52" (pr ![φ ψ])
    (meta (:description . "Theorem *2.52 of Principia Mathematica p. 107."))
    (ass [→ ¬ → φ ψ → ¬ φ ¬ ψ])
    (proof
      [ax1 ψ φ]
      [(infer !con) ψ [→ φ ψ]]
      [(ded !ax1) [¬ ψ] [¬ φ] [¬ → φ ψ]]))

(th "pm2.521" (pr ![φ ψ])
    (meta (:description . "Theorem *2.521 of Principia Mathematica p. 107."))
    (ass [→ ¬ → φ ψ → ψ φ])
    (proof
      [pm2.52 φ ψ]
      [(ded !ax3) φ ψ [¬ → φ ψ]]))

(th "mt2i" (pr ![φ ψ χ])
    (meta (:comment . "Modus tollens inference."))
    (hypo [χ] [→ φ → ψ ¬ χ])
    (ass [→ φ ¬ ψ])
    (proof
      [(ded !con>) ψ χ φ]
      [mpi φ χ [¬ ψ]]))

(th "mt2d" (pr ![φ ψ χ])
    (meta (:comment . "Modus tollens deduction."))
    (hypo [→ φ χ] [→ φ → ψ ¬ χ])
    (ass [→ φ ¬ ψ])
    (proof
      [(ded !con>) ψ χ φ]
      [mpd φ χ [¬ ψ]]))

(th "mt3" (pr ![φ ψ])
    (meta (:comment . "A rule similar to modus tollens."))
    (hypo [¬ ψ] [→ ¬ φ ψ])
    (ass [φ])
    (proof
      [(infer !con<) φ ψ]
      [ax-mp [¬ ψ] φ]))

(th "mt3i" (pr ![φ ψ χ])
    (meta (:comment . "Modus tollens inference."))
    (hypo [¬ χ] [→ φ → ¬ ψ χ])
    (ass [→ φ ψ])
    (proof
      [(ded !con<) ψ χ φ]
      [mpi φ [¬ χ] ψ]))

(th "mt3d" (pr ![φ ψ χ])
    (meta (:comment . "Modus tollens deduction."))
    (hypo [→ φ ¬ χ] [→ φ → ¬ ψ χ])
    (ass [→ φ ψ])
    (proof
      [(ded !con<) ψ χ φ]
      [mpd φ [¬ χ] [ψ]]))

(th "nsyl" (pr ![φ ψ χ])
    (meta (:comment . "A negated syllogism inference."))
    (hypo [→ φ ¬ ψ] [→ χ ψ])
    (ass [→ φ ¬ χ])
    (proof
      [(infer !con) χ ψ]
      [syl φ [¬ ψ] [¬ χ]]))

(th "nsyld" (pr ![φ ψ χ τ])
    (meta (:comment . "A negated syllogism deduction."))
    (hypo [→ φ → ψ ¬ χ] [→ φ → τ χ])
    (ass [→ φ → ψ ¬ τ])
    (proof
      [(ded !con) τ χ φ]
      [!im!trd φ ψ [¬ χ] [¬ τ]]))

(th "nsyl2" (pr ![φ ψ χ])
    (meta (:comment . "A negated syllogism inference."))
    (hypo [→ φ ¬ ψ] [→ ¬ χ ψ])
    (ass [→ φ χ])
    (proof
      [(infer !con<) χ ψ]
      [syl φ [¬ ψ] χ]))

(th "nsyl3" (pr ![φ ψ χ])
    (meta (:comment . "A negated syllogism inference."))
    (hypo [→ φ ¬ ψ] [→ χ ψ])
    (ass [→ χ ¬ φ])
    (proof
      [(infer !con>) φ ψ]
      [syl χ ψ [¬ φ]]))

(th "nsyl4" (pr ![φ ψ χ])
    (meta (:comment . "A negated syllogism inference."))
    (hypo [→ φ ψ] [→ ¬ φ χ])
    (ass [→ ¬ χ ψ])
    (proof
      [(infer !con<) φ χ]
      [syl [¬ χ] φ ψ]))

(th "nsyli" (pr ![φ ψ χ θ])
    (meta (:comment . "A negated syllogism inference."))
    (hypo [→ φ → ψ χ] [→ θ ¬ χ])
    (ass [→ φ → θ ¬ ψ])
    (proof
      [(ded !con) ψ χ φ]
      [!im!rp32 φ [¬ χ] [¬ ψ] θ]))

(th "mth8" (pr ![φ ψ])
    (meta (:description . "Theorem 8 of [Margaris] p. 60."))
    (ass [→ φ → ¬ ψ ¬ → φ ψ])
    (proof
      [mpclosed φ ψ]
      [(ded !con) [→ φ ψ] ψ φ]))

(th "pm2.61d1" (pr ![φ ψ χ])
    (meta (:comment . "Inference eliminating an antecedent."))
    (hypo [→ φ → ψ χ] [→ ¬ ψ χ])
    (ass [→ φ χ])
    (proof
      [(infer !ax1) [→ ¬ ψ χ] φ]
      [cased φ ψ χ]))

;(th "pm2.61d2" (pr ![φ ψ χ])
 ;   (meta (:comment . "Inference eliminating an antecedent."))
  ;  (hypo [→ φ → ¬ ψ χ] [→ ψ χ])
   ; (ass [→ φ χ])
    ;(proof
     ; [(infer !ax1) [→ ¬ ψ χ] φ]
      ;[cased φ ψ χ]))

(th "pm2.61nii" (pr ![φ ψ χ])
    (meta (:comment . "Inference eliminating two antecedents."))
    (hypo [→ φ → ψ χ] [→ ¬ φ χ] [→ ¬ ψ χ])
    (ass [χ])
    (proof
      [pm2.61d1 φ ψ χ]
      [casei φ χ]))

(th "pm2.61iii" (pr ![φ ψ χ θ])
    (meta (:comment . "Inference eliminating three antecedents."))
    (hypo [→ ¬ φ → ¬ ψ → ¬ χ θ] [→ φ θ] [→ ψ θ] [→ χ θ])
    (ass [θ])
    (proof
      [(ded !ax1) θ [¬ χ] φ]
      [(ded !ax1) [→ ¬ χ θ] [¬ ψ] φ]
      [casei φ [→ ¬ ψ → ¬ χ θ]]
      [caseii ψ χ θ]))

(th "pm2.65i" (pr ![φ ψ])
    (meta (:comment . "Inference rule for proof by contradiction."))
    (hypo [→ φ ψ] [→ φ ¬ ψ])
    (ass [¬ φ])
    (proof
      [ncase φ ψ]
      [ax-mp [→ φ ψ] [→ → φ ¬ ψ ¬ φ]]
      [ax-mp [→ φ ¬ ψ] [¬ φ]]))

(th "pm2.65d" (pr ![φ ψ χ])
    (meta (:comment . "Deduction rule for proof by contradiction."))
    (hypo [→ φ → ψ χ] [→ φ → ψ ¬ χ])
    (ass [→ φ ¬ ψ])
    (proof
      [ncase ψ χ]
      [!im!sylc [→ ψ χ] [→ ψ ¬ χ] [¬ ψ] φ]))

;; III - bi-implication

(th "pm4.8" (pr "φ")
   (meta (:description . "Theorem *4.8 of Principia Mathematica p. 122."))
   (ass [↔ → φ ¬ φ ¬ φ])
   (proof
     [neg- φ]
     [ax1 [¬ φ] φ]
     [>bii [→ φ ¬ φ] [¬ φ]]))

(th "pm4.81" (pr "φ")
    (meta (:description . "Theorem *4.81 of Principia Mathematica p. 122."))
    (ass [↔ → ¬ φ φ φ])
    (proof
      [neg+ φ]
      [>neg φ φ]
      [>bii [→ ¬ φ φ] φ]))

(th "pm5.41" (pr ![φ ψ χ])
    (meta (:description . "Theorem *5.41 of Principia Mathematica p. 125."))
    (ass [↔ → → φ ψ → φ χ → φ → ψ χ])
    (proof
      [ax2-converse φ ψ χ]
      [ax2 φ ψ χ]
      [>bii [→ → φ ψ → φ χ] [→ φ → ψ χ]]))

(th "pm4.2i" (pr ![φ ψ])
    (meta (:comment . "Principle of identity with antecedent."))
    (ass [→ φ ↔ ψ ψ])
    (proof
      [!bi!refl ψ]
      [(infer !ax1) [↔ ψ ψ] φ]))

  (th "mpbi" (pr ![φ ψ])
    (meta (:comment . "An inference from a biconditional, related to modus ponens."))
    (hypo [φ] [↔ φ ψ])
    (ass [ψ])
    (proof
      [(infer !bi>) φ ψ]
      [ax-mp φ ψ]))

  (th "mpbir" (pr ![φ ψ])
    (meta (:comment . "An inference from a biconditional, related to modus ponens."))
    (hypo [ψ] [↔ φ ψ])
    (ass [φ])
    (proof
      [(infer !bi<) φ ψ]
      [ax-mp ψ φ]))

  (th "mtbi" (pr ![φ ψ])
    (meta (:comment . "An inference from a biconditional, related to modus tollens."))
    (hypo [¬ φ] [↔ φ ψ])
    (ass [¬ ψ])
    (proof
      [(infer !bi<) φ ψ]
      [mto ψ φ]))

  (th "mtbir" (pr ![φ ψ])
    (meta (:comment . "An inference from a biconditional, related to modus tollens."))
    (hypo [¬ ψ] [↔ φ ψ])
    (ass [¬ φ])
    (proof
      [(infer !bi>) φ ψ]
      [mto φ ψ]))

(th "a1bi" (pr ![φ ψ])
    (meta (:comment . "Inference rule introducing a theorem as an antecedent."))
    (hypo [φ])
    (ass [↔ ψ → φ ψ])
    (proof
      [ax1 ψ φ]
      [mpclosed φ ψ]
      [ax-mp φ [→ → φ ψ ψ]]
      [>bii ψ [→ φ ψ]]))

  ; was brpi22
  (th "sylib" (pr ![φ ψ χ])
    (meta (:comment . "A mixed syllogism inference from an implication and a biconditional."))
    (hypo [→ φ ψ] [↔ ψ χ])
    (ass [→ φ χ])
    (proof
      [(infer !bi>) ψ χ]
      [syl φ ψ χ]))

  ; was brpi21
  (th "sylbi" (pr ![φ ψ χ])
    (meta (:comment . "A mixed syllogism inference from a biconditional and an implication.")
          (:description . "Useful for substituting an antecedent with a definition."))
    (hypo [→ φ ψ] [↔ χ φ])
    (ass [→ χ ψ])
    (proof
      [(infer !bi>) χ φ]
      [syl χ φ ψ]))

  ; was brpi22<
  (th "sylibr" (pr ![φ ψ χ])
    (meta (:comment . "A mixed syllogism inference from an implication and a biconditional.")
          (:description . "Useful for substituting a consequent with a definition."))
    (hypo [→ φ ψ] [↔ χ ψ])
    (ass [→ φ χ])
    (proof
      [(infer !bi<) χ ψ]
      [syl φ ψ χ]))

  ; was brpi21<
  (th "sylbir" (pr ![φ ψ χ])
    (meta (:comment . "A mixed syllogism inference from a biconditional and an implication."))
    (hypo [→ φ ψ] [↔ φ χ])
    (ass [→ χ ψ])
    (proof
      [(infer !bi<) φ χ]
      [syl χ φ ψ]))

  ; was brpi22d
  (th "sylibd" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism deduction."))
    (hypo [→ φ → ψ χ] [→ φ ↔ χ θ])
    (ass [→ φ → ψ θ])
    (proof
      [(ded !bi>) χ θ φ]
      [!im!trd φ ψ χ θ]))

  ; was brpi21d
  (th "sylbid" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism deduction."))
    (hypo [→ φ → ψ χ] [→ φ ↔ θ ψ])
    (ass [→ φ → θ χ])
    (proof
      [(ded !bi>) θ ψ φ]
      [!im!trd φ θ ψ χ]))

  ; was brpi22<d
  (th "sylibrd" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism deduction."))
    (hypo [→ φ → ψ χ] [→ φ ↔ θ χ])
    (ass [→ φ → ψ θ])
    (proof
      [(ded !bi<) θ χ φ]
      [!im!trd φ ψ χ θ]))

  ; was brpi21<d
  (th "sylbird" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism deduction."))
    (hypo [→ φ → ψ χ] [→ φ ↔ ψ θ])
    (ass [→ φ → θ χ])
    (proof
      [(ded !bi<) ψ θ φ]
      [!im!trd φ θ ψ χ]))

  ; was brpi32
  (th "syl5ib" (pr ![φ ψ χ θ])
    (meta (:comment . "A mixed syllogism inference from a nested implication and a biconditional.")
          (:description . "Useful for substituting an embedded antecedent with a definition."))
    (hypo [→ φ → ψ χ] [↔ θ ψ])
    (ass [→ φ → θ χ])
    (proof
      [(infer !bi>) θ ψ]
      [!im!rp32 φ ψ χ θ]))

  ; was brpi32<
  (th "syl5ibr" (pr ![φ ψ χ θ])
    (meta (:comment . "A mixed syllogism inference from a nested implication and a biconditional."))
    (hypo [→ φ → ψ χ] [↔ ψ θ])
    (ass [→ φ → θ χ])
    (proof
      [(infer !bi<) ψ θ]
      [!im!rp32 φ ψ χ θ]))

  ; was brpi33
  (th "syl6ib" (pr ![φ ψ χ θ])
    (meta (:comment . "A mixed syllogism inference from a nested implication and a biconditional"))
    (hypo [→ φ → ψ χ] [↔ χ θ])
    (ass [→ φ → ψ θ])
    (proof
      [(infer !bi>) χ θ]
      [!im!rp33 φ ψ χ θ]))

  ; was brpi33<
  (th "syl6ibr" (pr ![φ ψ χ θ])
    (meta (:comment . "A mixed syllogism inference from a nested implication and a biconditional.")
          (:description . "Useful for substituting an embedded consequent with a definition."))
    (hypo [→ φ → ψ χ] [↔ θ χ])
    (ass [→ φ → ψ θ])
    (proof
      [(infer !bi<) θ χ]
      [!im!rp33 φ ψ χ θ]))

  ; was brpi43
  (th "syl7ib" (pr ![φ ψ χ θ τ])
    (meta (:comment . "A mixed syllogism inference from a doubly nested implication and a biconditional."))
    (hypo [→ φ → ψ → χ θ] [↔ τ χ])
    (ass [→ φ → ψ → τ θ])
    (proof
      [(infer !bi>) τ χ]
      [!im!rp43 φ ψ χ θ τ]))

  ; was brpi44
  (th "syl8ib" (pr ![φ ψ χ θ τ])
    (meta (:comment . "A syllogism rule of inference.")
          (:description . "The second premise is used to replace the consequent of the first premise."))
    (hypo [→ φ → ψ → χ θ] [↔ θ τ])
    (ass [→ φ → ψ → χ τ])
    (proof
      [(infer !bi>) θ τ]
      [!im!rp44 φ ψ χ θ τ]))

;; IV - Conjunction, Disjunction

(th "pm4.64" (pr ![φ ψ])
    (meta (:description . "Theorem *4.64 of Principia Mathematica p. 120."))
    (ass [↔ → ¬ φ ψ ∨ φ ψ])
    (proof
      [df-or φ ψ]
      [!bi!comi [∨ φ ψ] [→ ¬ φ ψ]]))

(th "pm2.54" (pr ![φ ψ])
    (meta (:description . "Theorem *2.54 of Principia Mathematica p. 107."))
    (ass [→ → ¬ φ ψ ∨ φ ψ])
    (proof
      [(bi< !df-or) φ ψ]))

(th "pm4.63" (pr ![φ ψ])
    (meta (:description . "Theorem *4.63 of Principia Mathematica p. 120."))
    (ass [↔ ¬ → φ ¬ ψ ∧ φ ψ])
    (proof
      [df-an φ ψ]
      [!bi!comi [∧ φ ψ] [¬ → φ ¬ ψ]]))

(th "pm4.62" (pr ![φ ψ])
    (meta (:description . "Theorem *4.62 of Principia Mathematica p. 120."))
    (ass [↔ → φ ¬ ψ ∨ ¬ φ ¬ ψ])
    (proof
      [!im!df.or φ [¬ ψ]]))

(th "pm4.66" (pr ![φ ψ])
    (meta (:description . "Theorem *4.66 of Principia Mathematica p. 120."))
    (ass [↔ → ¬ φ ¬ ψ ∨ φ ¬ ψ])
    (proof
      [pm4.64 φ [¬ ψ]]))

; !im!neg?
(th "pm4.61" (pr ![φ ψ])
    (meta (:description . "Theorem *4.61 of Principia Mathematica p. 120."))
    (ass [↔ ¬ → φ ψ ∧ φ ¬ ψ])
    (proof
      [!an!df.nim φ ψ]
      [!bi!comi [∧ φ ¬ ψ] [¬ → φ ψ]]))

(th "pm4.65" (pr ![φ ψ])
    (meta (:description . "Theorem *4.65 of Principia Mathematica p. 120."))
    (ass [↔ ¬ → ¬ φ ψ ∧ ¬ φ ¬ ψ])
    (proof
      [pm4.61 ¬ φ ψ]))

(th "pm4.67" (pr ![φ ψ])
    (meta (:description . "Theorem *4.67 of Principia Mathematica p. 120."))
    (ass [↔ ¬ → ¬ φ ¬ ψ ∧ ¬ φ ψ])
    (proof
      [pm4.63 ¬ φ ψ]))

(th "pm4.25" (pr "φ")
    (meta (:description . "Theorem *4.25 of Principia Mathematica p. 117."))
    (ass [↔ φ ∨ φ φ])
    (proof
      [!or!idm φ]
      [!bi!comi [∨ φ φ] φ]))

(th "pm1.2" (pr "φ")
    (meta (:description . "Axiom *1.2 (Taut) of Principia Mathematica p. 96."))
    (ass [→ ∨ φ φ φ])
    (proof
      [(bi> !or!idm) φ]))

(th "pm1.4" (pr ![φ ψ])
    (meta (:description . "Axiom *1.4 of Principia Mathematica p. 96."))
    (ass [→ ∨ φ ψ ∨ ψ φ])
    (proof
      [(bi> !or!com) φ ψ]))

;(th "pm2.62" (pr ![φ ψ])
;    (meta (:description . "Theorem *2.62 of Principia Mathematica p. 107."))
;    (ass [→ ∨ φ ψ → → φ ψ ψ])
;    (proof
;      [(bi> !or!df2) φ ψ]))

(th "pm2.621" (pr ![φ ψ])
    (meta (:description . "Theorem *2.621 of Principia Mathematica p. 107."))
    (ass [→ → φ ψ → ∨ φ ψ ψ])
    (proof
      [(bi> !or!df2) φ ψ]
      [com12i [∨ φ ψ] [→ φ ψ] ψ]))

(th "pm2.68" (pr ![φ ψ])
    (meta (:description . "Theorem *2.68 of Principia Mathematica p. 108."))
    (ass [→ → → φ ψ ψ ∨ φ ψ])
    (proof
      [(bi< !or!df2) φ ψ]))

(th "pm2.25" (pr ![φ ψ])
    (meta (:description . "Theorem *2.25 of Principia Mathematica p. 104."))
    (ass [∨ φ → ∨ φ ψ ψ])
    (proof
      [!or!elim< φ ψ]
      [(infer< !df-or) φ [→ ∨ φ ψ ψ]]))

(th "pm2.53" (pr ![φ ψ])
    (meta (:description . "Theorem *2.53 of Principia Mathematica p. 107."))
    (ass [→ ∨ φ ψ → ¬ φ ψ])
    (proof
      [(bi> !df-or) φ ψ]))

(th "pm1.5" (pr ![φ ψ χ])
    (meta (:description . "Axiom *1.5 (Assoc) of Principia Mathematica p. 96."))
    (ass [→ ∨ φ ∨ ψ χ ∨ ψ ∨ φ χ])
    (proof
      [(bi> !or!12ass) φ ψ χ]))

(th "pm2.31" (pr ![φ ψ χ])
    (meta (:description . "Theorem *2.31 of Principia Mathematica p. 104."))
    (ass [→ ∨ φ ∨ ψ χ ∨ ∨ φ ψ χ])
    (proof
      [(bi< !or!ass) φ ψ χ]))

(th "pm2.32" (pr ![φ ψ χ])
    (meta (:description . "Theorem *2.32 of Principia Mathematica p. 105."))
    (ass [→ ∨ ∨ φ ψ χ ∨ φ ∨ ψ χ])
    (proof
      [(bi> !or!ass) φ ψ χ]))

(th "or23" (pr ![φ ψ χ])
    (meta (:comment . "A rearrangement of disjucts.")
          (:description . "or23 in set.mm."))
    (ass [↔ ∨ ∨ φ ψ χ ∨ ∨ φ χ ψ])
    (proof
      [!or!com ψ χ]
      [(infer !or!eqt>) [∨ ψ χ] [∨ χ ψ] [φ]]
      [!or!ass φ ψ χ]
      [!or!ass φ χ ψ]
      [<bitr [∨ φ ∨ ψ χ] [∨ φ ∨ χ ψ]
             [∨ ∨ φ ψ χ] [∨ ∨ φ χ ψ]]))

(th "or42" (pr ![φ ψ χ θ])
    (meta (:comment . "Rearrangement of 4 disjuncts."))
    (ass [↔ ∨ ∨ φ ψ ∨ χ θ ∨ ∨ φ χ ∨ θ ψ])
    (proof
      [!or!4ass φ ψ χ θ]
      [!or!com ψ θ]
      [(infer !or!eqt>) [∨ ψ θ] [∨ θ ψ] [∨ φ χ]]
      [bitr [∨ ∨ φ ψ ∨ χ θ] [∨ ∨ φ χ ∨ ψ θ] [∨ ∨ φ χ ∨ θ ψ]]))

(th "pm2.07" (pr "φ")
    (meta (:description . "Theorem *2.07 of Principia Mathematica p. 101."))
    (ass [→ φ ∨ φ φ])
    (proof
      [(bi< !or!idm) φ]))

(th "pm2.45" (pr ![φ ψ])
    (meta (:description . "Theorem *2.45 of Principia Mathematica p. 106."))
    (ass [→ ¬ ∨ φ ψ ¬ φ])
    (proof
      [or< φ ψ]
      [(infer !con) φ [∨ φ ψ]]))

(th "pm2.46" (pr ![φ ψ])
    (meta (:description . "Theorem *2.46 of Principia Mathematica p. 106."))
    (ass [→ ¬ ∨ φ ψ ¬ ψ])
    (proof
      [or> φ ψ]
      [(infer !con) ψ [∨ φ ψ]]))

(th "pm2.47" (pr ![φ ψ])
    (meta (:description . "Theorem *2.47 of Principia Mathematica p. 107."))
    (ass [→ ¬ ∨ φ ψ ∨ ¬ φ ψ])
    (proof
      [pm2.45 φ ψ]
      [(ded !or<) [¬ φ] ψ [¬ ∨ φ ψ]]))

(th "pm2.48" (pr ![φ ψ])
    (meta (:description . "Theorem *2.48 of Principia Mathematica p. 107."))
    (ass [→ ¬ ∨ φ ψ ∨ φ ¬ ψ])
    (proof
      [pm2.46 φ ψ]
      [(ded !or>) φ [¬ ψ] [¬ ∨ φ ψ]]))

(th "pm2.49" (pr ![φ ψ])
    (meta (:description . "Theorem *2.49 of Principia Mathematica p. 107."))
    (ass [→ ¬ ∨ φ ψ ∨ ¬ φ ¬ ψ])
    (proof
      [pm2.45 φ ψ]
      [(ded !or<) [¬ φ] [¬ ψ] [¬ ∨ φ ψ]]))

;(th "pm2.67" (pr ![φ ψ])
;    (meta (:description . "Theorem *2.67 of Principia Mathematica p. 107."))
;    (ass [→ → ∨ φ ψ ψ → φ ψ])
;    (proof
;      [or< φ ψ]
;      [(infer !syl>) φ [∨ φ ψ] ψ]))

(th "pm3.37" (pr ![φ ψ χ])
    (meta (:description . "Theorem *3.37 of Principia Mathematica p. 112."))
    (ass [→ → ∧ φ ψ χ → ∧ φ ¬ χ ¬ ψ])
    (proof
      [>and-r ψ φ]
      [(ded !syl>) φ [∧ φ ψ] χ ψ]
      [com12i ψ [→ ∧ φ ψ χ] [→ φ χ]]
      [(bi> !im!df.an) φ χ]
      [!im!rp33 [→ ∧ φ ψ χ] ψ [→ φ χ] [¬ ∧ φ ¬ χ]]
      [(ded !con>) ψ [∧ φ ¬ χ] [→ ∧ φ ψ χ]]))

(th "pm4.52" (pr ![φ ψ])
    (meta (:description . "Theorem *4.52 of Principia Mathematica p. 120."))
    (ass [↔ ∧ φ ¬ ψ ¬ ∨ ¬ φ ψ])
    (proof
      [!an!df.or φ ¬ ψ]
      [bneg> ψ]
      [(infer !or!eqt>) ψ [¬ ¬ ψ] [¬ φ]]
      [(infer !neqt) [∨ ¬ φ ψ] [∨ ¬ φ ¬ ¬ ψ]]
      [!bi!tri< [∧ φ ¬ ψ] [¬ ∨ ¬ φ ¬ ¬ ψ] [¬ ∨ ¬ φ ψ]]))

(th "pm4.53" (pr ![φ ψ])
    (meta (:description . "Theorem *4.53 of Principia Mathematica p. 120."))
    (ass [↔ ¬ ∧ φ ¬ ψ ∨ ¬ φ ψ])
    (proof
      [pm4.52 φ ψ]
      [conb>i [∧ φ ¬ ψ] [∨ ¬ φ ψ]]
      [!bi!comi [∨ ¬ φ ψ] [¬ ∧ φ ¬ ψ]]))

(th "pm4.54" (pr ![φ ψ])
    (meta (:description . "Theorem *4.54 of Principia Mathematica p. 120."))
    (ass [↔ ∧ ¬ φ ψ ¬ ∨ φ ¬ ψ])
    (proof
      [!an!df.or [¬ φ] ψ]
      [bneg> φ]
      [(infer !or!eqt<) φ [¬ ¬ φ] [¬ ψ]]
      [(infer !neqt) [∨ φ ¬ ψ] [∨ ¬ ¬ φ ¬ ψ]]
      [!bi!tri< [∧ ¬ φ ψ] [¬ ∨ ¬ ¬ φ ¬ ψ] [¬ ∨ φ ¬ ψ]]))

(th "pm4.55" (pr ![φ ψ])
    (meta (:description . "Theorem *4.55 of Principia Mathematica p. 120."))
    (ass [↔ ¬ ∧ ¬ φ ψ ∨ φ ¬ ψ])
    (proof
      [pm4.54 φ ψ]
      [conb>i [∧ ¬ φ ψ] [∨ φ ¬ ψ]]
      [!bi!comi [∨ φ ¬ ψ] [¬ ∧ ¬ φ ψ]]))

(th "pm4.56" (pr ![φ ψ])
    (meta (:description . "Theorem *4.56 of Principia Mathematica p. 120."))
    (ass [↔ ∧ ¬ φ ¬ ψ ¬ ∨ φ ψ])
    (proof
      [!or!neg φ ψ]
      [!bi!comi [¬ ∨ φ ψ] [∧ ¬ φ ¬ ψ]]))

(th "pm4.57" (pr ![φ ψ])
    (meta (:description . "Theorem *4.57 of Principia Mathematica p. 120."))
    (ass [↔ ¬ ∧ ¬ φ ¬ ψ ∨ φ ψ])
    (proof
      [!or!df.an φ ψ]
      [!bi!comi [∨ φ ψ] [¬ ∧ ¬ φ ¬ ψ]]))

(th "pm3.1" (pr ![φ ψ])
    (meta (:description . "Theorem *3.1 of Principia Mathematica p. 111."))
    (ass [→ ∧ φ ψ ¬ ∨ ¬ φ ¬ ψ])
    (proof
      [(bi> !an!df.or) φ ψ]))

(th "pm3.11" (pr ![φ ψ])
    (meta (:description . "Theorem *3.11 of Principia Mathematica p. 111."))
    (ass [→ ¬ ∨ ¬ φ ¬ ψ ∧ φ ψ])
    (proof
      [(bi< !an!df.or) φ ψ]))

(th "pm3.12" (pr ![φ ψ])
    (meta (:description . "Theorem *3.12 of Principia Mathematica p. 111."))
    (ass [∨ ∨ ¬ φ ¬ ψ ∧ φ ψ])
    (proof
      [pm3.11 φ ψ]
      [(infer< !df-or) [∨ ¬ φ ¬ ψ] [∧ φ ψ]]))

(th "pm3.13" (pr ![φ ψ])
    (meta (:description . "Theorem *3.13 of Principia Mathematica p. 111."))
    (ass [→ ¬ ∧ φ ψ ∨ ¬ φ ¬ ψ])
    (proof
      [pm3.11 φ ψ]
      [(infer !con<) [∨ ¬ φ ¬ ψ] [∧ φ ψ]]))

(th "pm3.14" (pr ![φ ψ])
    (meta (:description . "Theorem *3.14 of Principia Mathematica p. 111."))
    (ass [→ ∨ ¬ φ ¬ ψ ¬ ∧ φ ψ])
    (proof
      [pm3.1 φ ψ]
      [(infer !con>) [∧ φ ψ] [∨ ¬ φ ¬ ψ]]))

(th "pm3.26bd" (pr ![φ ψ χ])
    (meta (:comment . "Deduction eliminating a conjunct."))
    (hypo [↔ φ ∧ ψ χ])
    (ass [→ φ ψ])
    (proof
      [(infer !bi>) φ [∧ ψ χ]]
      [(ded !an!sim<) ψ χ φ]))

(th "pm3.27bd" (pr ![φ ψ χ])
    (meta (:comment . "Deduction eliminating a conjunct."))
    (hypo [↔ φ ∧ ψ χ])
    (ass [→ φ χ])
    (proof
      [(infer !bi>) φ [∧ ψ χ]]
      [(ded !an!sim>) ψ χ φ]))

(th "pm3.41" (pr ![φ ψ χ])
    (meta (:description . "Theorem *3.41 of Principia Mathematica p. 113."))
    (ass [→ → φ χ → ∧ φ ψ χ])
    (proof
      [!an!sim< φ ψ]
      [(infer !syl>) [∧ φ ψ] φ χ]))

(th "pm3.42" (pr ![φ ψ χ])
    (meta (:description . "Theorem *3.42 of Principia Mathematica p. 113."))
    (ass [→ → ψ χ → ∧ φ ψ χ])
    (proof
      [!an!sim> φ ψ]
      [(infer !syl>) [∧ φ ψ] ψ χ]))

(th "pm3.45im" (pr ![φ ψ])
    (meta (:comment . "Conjunction with implication.")
          (:description . "Compare theorem *4.45 of Principia Mathematica p. 119."))
    (ass [↔ φ ∧ φ → ψ φ])
    (proof
      [ax1 φ ψ]
      [id φ] ; !an!join
      [jca φ φ [→ ψ φ]]
      [!an!sim< φ [→ ψ φ]]
      [>bii φ [∧ φ → ψ φ]]))

(th "pm2.3" (pr ![φ ψ χ])
    (meta (:description . "Theorem *2.3 of Principia Mathematica p. 104."))
    (ass [→ ∨ φ ∨ ψ χ ∨ φ ∨ χ ψ])
    (proof
      [(bi> !or!com) ψ χ]
      [(infer !or!imt>) [∨ ψ χ] [∨ χ ψ] φ]))

(th "pm2.41" (pr ![φ ψ])
    (meta (:description . "Theorem *2.41 of Principia Mathematica p. 106."))
    (ass [→ ∨ ψ ∨ φ ψ ∨ φ ψ])
    (proof
      [or> φ ψ]
      [id [∨ φ ψ]]
      [jaoi ψ [∨ φ ψ] [∨ φ ψ]]))

(th "pm2.42" (pr ![φ ψ])
    (meta (:description . "Theorem *2.42 of Principia Mathematica p. 106."))
    (ass [→ ∨ ¬ φ → φ ψ → φ ψ])
    (proof
      [<neg φ ψ]
      [id [→ φ ψ]]
      [jaoi [¬ φ] [→ φ ψ] [→ φ ψ]]))

(th "pm2.4" (pr ![φ ψ])
    (meta (:description . "Theorem *2.4 of Principia Mathematica p. 106."))
    (ass [→ ∨ φ ∨ φ ψ ∨ φ ψ])
    (proof
      [or< φ ψ]
      [id [∨ φ ψ]]
      [jaoi φ [∨ φ ψ] [∨ φ ψ]]))
    
(th "pm5.63" (pr ![φ ψ])
    (meta (:description . "Theorem *5.63 of Principia Mathematica p. 125."))
    (ass [↔ ∨ φ ψ ∨ φ ∧ ¬ φ ψ])
    (proof
      [pm2.53 φ ψ]
      [(ded !an!join) [¬ φ] ψ [∨ φ ψ]]
      [(ded< !df-or) φ [∧ ¬ φ ψ] [∨ φ ψ]]
      [>neg φ ψ]
      [an->im [¬ φ] ψ]
      [jaoi φ [∧ ¬ φ ψ] [→ ¬ φ ψ]]
      [(ded< !df-or) φ ψ [∨ φ ∧ ¬ φ ψ]]
      [>bii [∨ φ ψ] [∨ φ ∧ ¬ φ ψ]]))

(th "pm3.3" (pr ![φ ψ χ])
    (meta (:description . "Theorem *3.3 (Exp) of Principia Mathematica p. 112."))
    (ass [→ → ∧ φ ψ χ → φ → ψ χ])
    (proof
      [(bi> !impexp) φ ψ χ]))

(th "pm3.31" (pr ![φ ψ χ])
    (meta (:description . "Theorem *3.31 (Imp) of Principia Mathematica p. 112."))
    (ass [→ → φ → ψ χ → ∧ φ ψ χ])
    (proof
      [(bi< !impexp) φ ψ χ]))

(th "impcom" (pr ![φ ψ χ])
    (meta (:comment . "Importation inference with commutation of antecedents."))
    (hypo [→ φ → ψ χ])
    (ass [→ ∧ ψ φ χ])
    (proof
      [com12i φ ψ χ]
      [imp ψ φ χ]))

(th "pm4.14" (pr ![φ ψ χ])
    (meta (:description . "Theorem *4.14 of Principia Mathematica p. 117."))
    (ass [↔ → ∧ φ ψ χ → ∧ φ ¬ χ ¬ ψ])
    (proof
      (suppose [→ ∧ φ ψ χ]
        [exp φ ψ χ]
        [(ded !con) ψ χ φ]
        [imp φ [¬ χ] [¬ ψ]])
      (claim [→ → ∧ φ ψ χ → ∧ φ ¬ χ ¬ ψ])
      (suppose [→ ∧ φ ¬ χ ¬ ψ]
        [exp φ [¬ χ] [¬ ψ]]
        [(ded !ax3) χ ψ φ]
        [imp φ ψ χ])
      (claim [→ → ∧ φ ¬ χ ¬ ψ → ∧ φ ψ χ])
      [>bii [→ ∧ φ ψ χ] [→ ∧ φ ¬ χ ¬ ψ]]))

(th "pm4.15" (pr ![φ ψ χ])
    (meta (:description . "Theorem *4.15 of Principia Mathematica p. 117."))
    (ass [↔ → ∧ φ ψ ¬ χ → ∧ ψ χ ¬ φ])
    (proof
      [impexp φ ψ [¬ χ]]
      [!im!df.nan ψ χ]
      [(infer !im!eqt>) [→ ψ ¬ χ] [¬ ∧ ψ χ] φ]
      [bcon> φ [∧ ψ χ]]
      [2bitr [→ ∧ φ ψ ¬ χ] [→ φ → ψ ¬ χ] [→ φ ¬ ∧ ψ χ] [→ ∧ ψ χ ¬ φ]]))

(th "pm4.78" (pr ![φ ψ χ])
    (meta (:description . "Theorem *4.78 of Principia Mathematica p. 121."))
    (ass [↔ ∨ → φ ψ → φ χ → φ ∨ ψ χ])
    (proof
      (suppose [→ φ ψ]
        [(ded !or<) ψ χ φ])
      (claim [→ → φ ψ → φ ∨ ψ χ])
      (suppose [→ φ χ]
        [(ded !or>) ψ χ φ])
      (claim [→ → φ χ → φ ∨ ψ χ])
      [jaoi [→ φ ψ] [→ φ χ] [→ φ ∨ ψ χ]]
      
      (suppose [∧ → φ ∨ ψ χ ¬ → φ ψ]
        [siman [→ φ ∨ ψ χ] [¬ → φ ψ]]
        [(infer< !an!df.nim) φ ψ]
        [siman φ [¬ ψ]]
        [ax-mp φ [∨ ψ χ]]
        [(infer> !df-or) ψ χ]
        [ax-mp [¬ ψ] χ]
        [(infer !ax1) χ φ])
      (claim [→ ∧ → φ ∨ ψ χ ¬ → φ ψ → φ χ])
      [exp [→ φ ∨ ψ χ] [¬ → φ ψ] [→ φ χ]]
      [(ded< !df-or) [→ φ ψ] [→ φ χ] [→ φ ∨ ψ χ]]
      [>bii [∨ → φ ψ → φ χ] [→ φ ∨ ψ χ]]))

(th "pm4.79" (pr ![φ ψ χ])
    (meta (:description . "Theorem *4.79 of Principia Mathematica p. 121."))
    (ass [↔ ∨ → ψ φ → χ φ → ∧ ψ χ φ])
    (proof
      [!an!sim< ψ χ]
      [(infer !syl>) [∧ ψ χ] ψ φ]
      [!an!sim> ψ χ]
      [(infer !syl>) [∧ ψ χ] χ φ]
      [jaoi [→ ψ φ] [→ χ φ] [→ ∧ ψ χ φ]]
      (suppose [¬ ∨ → ψ φ → χ φ]
        [(infer> !or!neg) [→ ψ φ] [→ χ φ]]
        [siman [¬ → ψ φ] [¬ → χ φ]]
        [(infer< !an!df.nim) ψ φ]
        [(infer< !an!df.nim) χ φ]
        [siman ψ [¬ φ]]
        [(infer !an!sim<) χ [¬ φ]]
        [>andi ψ χ]
        [(infer !neg>) [∧ ψ χ]]
        [>nor [¬ ∧ ψ χ] φ]
        [(bi> !im!df.or) [∧ ψ χ] φ]
        [(infer !con) [→ ∧ ψ χ φ] [∨ ¬ ∧ ψ χ φ]]
        [ax-mp [¬ ∨ ¬ ∧ ψ χ φ] [¬ → ∧ ψ χ φ]])
      (claim [→ ¬ ∨ → ψ φ → χ φ ¬ → ∧ ψ χ φ])
      [(infer !ax3) [∨ → ψ φ → χ φ] [→ ∧ ψ χ φ]]
      [>bii [∨ → ψ φ → χ φ] [→ ∧ ψ χ φ]]))

(th "pm4.87" (pr ![φ ψ χ])
    (meta (:description . "Theorem *4.87 of Principia Mathematica p. 122."))
    (ass [∧ ∧ [↔ → ∧ φ ψ χ → φ → ψ χ] [↔ → φ → ψ χ → ψ → φ χ] [↔ → ψ → φ χ → ∧ ψ φ χ]])
    (proof
      [impexp φ ψ χ]
      [!im!bcom12 φ ψ χ]
      [>andi [↔ → ∧ φ ψ χ → φ → ψ χ] [↔ → φ → ψ χ → ψ → φ χ]]
      [impexp ψ φ χ]
      [!bi!comi [→ ∧ ψ φ χ] [→ ψ → φ χ]]
      [>andi [∧ ↔ → ∧ φ ψ χ → φ → ψ χ ↔ → φ → ψ χ → ψ → φ χ] [↔ → ψ → φ χ → ∧ ψ φ χ]]))

(th "pm3.34" (pr ![φ ψ χ])
    (meta (:description . "Theorem *3.34 (Syll) of Principia Mathematica p. 112."))
    (ass [→ ∧ → ψ χ → φ ψ → φ χ])
    (proof
      [!im!tr> ψ χ φ]
      [imp [→ ψ χ] [→ φ ψ] [→ φ χ]]))

(th "pm5.31" (pr ![φ ψ χ])
    (meta (:description . "Theorem *5.31 of Principia Mathematica p. 125."))
    (ass [→ ∧ χ → φ ψ → φ ∧ ψ χ])
    (proof
      [>and-r χ ψ]
      [(ded !syl<) ψ [∧ ψ χ] φ χ]
      [imp χ [→ φ ψ] [→ φ ∧ ψ χ]]))

(th "imp4c" (pr ![φ ψ χ θ τ])
    (meta (:comment . "An importation inference."))
    (hypo [→ φ → ψ → χ → θ τ])
    (ass [→ φ → ∧ ∧ ψ χ θ τ])
    (proof
      [(ded< !impexp) ψ χ [→ θ τ] φ]
      [(ded< !impexp) [∧ ψ χ] θ τ φ]))

(th "imp4d" (pr ![φ ψ χ θ τ])
    (meta (:comment . "An importation inference."))
    (hypo [→ φ → ψ → χ → θ τ])
    (ass [→ φ → ∧ ψ ∧ χ θ τ])
    (proof
      [imp4a φ ψ χ θ τ]
      [(ded< !impexp) ψ [∧ χ θ] τ φ]))

(th "imp41" (pr ![φ ψ χ θ τ])
    (meta (:comment . "An importation inference."))
    (hypo [→ φ → ψ → χ → θ τ])
    (ass [→ ∧ ∧ ∧ φ ψ χ θ τ])
    (proof
      [imp φ ψ [→ χ → θ τ]]
      [imp [∧ φ ψ] χ [→ θ τ]]
      [imp [∧ ∧ φ ψ χ] θ τ]))

(th "imp42" (pr ![φ ψ χ θ τ])
    (meta (:comment . "An importation inference."))
    (hypo [→ φ → ψ → χ → θ τ])
    (ass [→ ∧ ∧ φ ∧ ψ χ θ τ])
    (proof
      [imp32 φ ψ χ [→ θ τ]]
      [imp [∧ φ ∧ ψ χ] θ τ]))

(th "imp43" (pr ![φ ψ χ θ τ])
    (meta (:comment . "An importation inference."))
    (hypo [→ φ → ψ → χ → θ τ])
    (ass [→ ∧ ∧ φ ψ ∧ χ θ τ])
    (proof
      [imp4a φ ψ χ θ τ]
      [imp φ ψ [→ ∧ χ θ τ]]
      [imp [∧ φ ψ] [∧ χ θ] τ]))

(th "imp44" (pr ![φ ψ χ θ τ])
    (meta (:comment . "An importation inference."))
    (hypo [→ φ → ψ → χ → θ τ])
    (ass [→ ∧ φ ∧ ∧ ψ χ θ τ])
    (proof
      [(ded< !impexp) ψ χ [→ θ τ] φ]
      [(ded< !impexp) [∧ ψ χ] θ τ φ]
      [imp φ [∧ ∧ ψ χ θ] τ]))

(th "imp45" (pr ![φ ψ χ θ τ])
    (meta (:comment . "An importation inference."))
    (hypo [→ φ → ψ → χ → θ τ])
    (ass [→ ∧ φ ∧ ψ ∧ χ θ τ])
    (proof
      [imp4a φ ψ χ θ τ]
      [(ded< !impexp) ψ [∧ χ θ] τ φ]
      [imp φ [∧ ψ ∧ χ θ] τ]))

(th "expcom" (pr ![φ ψ χ])
    (meta (:comment . "Exportation inference with commutation of antecedents."))
    (hypo [→ ∧ φ ψ χ])
    (ass [→ ψ → φ χ])
    (proof
      [exp φ ψ χ]
      [com12i φ ψ χ]))

(th "exp4b" (pr ![φ ψ χ θ τ])
    (meta (:comment . "An exportation inference."))
    (hypo [→ ∧ φ ψ → ∧ χ θ τ])
    (ass [→ φ → ψ → χ → θ τ])
    (proof
      [exp φ ψ [→ ∧ χ θ τ]]
      [exp4a φ ψ χ θ τ]))

(th "exp4c" (pr ![φ ψ χ θ τ])
    (meta (:comment . "An exportation inference."))
    (hypo [→ φ → ∧ ∧ ψ χ θ τ])
    (ass [→ φ → ψ → χ → θ τ])
    (proof
      [(ded> !impexp) [∧ ψ χ] θ τ φ]
      [(ded> !impexp) ψ χ [→ θ τ] φ]))

(th "exp4d" (pr ![φ ψ χ θ τ])
    (meta (:comment . "An exportation inference."))
    (hypo [→ φ → ∧ ψ ∧ χ θ τ])
    (ass [→ φ → ψ → χ → θ τ])
    (proof
      [(ded> !impexp) ψ [∧ χ θ] τ φ]
      [exp4a φ ψ χ θ τ]))

(th "exp41" (pr ![φ ψ χ θ τ])
    (meta (:comment . "An exportation inference."))
    (hypo [→ ∧ ∧ ∧ φ ψ χ θ τ])
    (ass [→ φ → ψ → χ → θ τ])
    (proof
      [exp [∧ ∧ φ ψ χ] θ τ]
      [exp [∧ φ ψ] χ [→ θ τ]]
      [exp φ ψ [→ χ → θ τ]]))

(th "exp42" (pr ![φ ψ χ θ τ])
    (meta (:comment . "An exportation inference."))
    (hypo [→ ∧ ∧ φ ∧ ψ χ θ τ])
    (ass [→ φ → ψ → χ → θ τ])
    (proof
      [exp31 φ [∧ ψ χ] θ τ]
      [(ded> !impexp) ψ χ [→ θ τ] φ]))

(th "exp43" (pr ![φ ψ χ θ τ])
    (meta (:comment . "An exportation inference."))
    (hypo [→ ∧ ∧ φ ψ ∧ χ θ τ])
    (ass [→ φ → ψ → χ → θ τ])
    (proof
      [exp [∧ φ ψ] [∧ χ θ] τ]
      [exp4b φ ψ χ θ τ]))

(th "exp44" (pr ![φ ψ χ θ τ])
    (meta (:comment . "An exportation inference."))
    (hypo [→ ∧ φ ∧ ∧ ψ χ θ τ])
    (ass [→ φ → ψ → χ → θ τ])
    (proof
      [exp32 φ [∧ ψ χ] θ τ]
      [(ded> !impexp) ψ χ [→ θ τ] φ]))

(th "exp45" (pr ![φ ψ χ θ τ])
    (meta (:comment . "An exportation inference."))
    (hypo [→ ∧ φ ∧ ψ ∧ χ θ τ])
    (ass [→ φ → ψ → χ → θ τ])
    (proof
      [exp φ [∧ ψ ∧ χ θ] τ]
      [exp4d φ ψ χ θ τ]))

(th "impac" (pr ![φ ψ χ])
    (meta (:comment . "Importation with conjunction in consequent."))
    (hypo [→ φ → ψ χ])
    (ass [→ ∧ φ ψ ∧ χ ψ])
    (proof
      [(ded !an!join-r) ψ χ φ]
      [imp φ ψ [∧ χ ψ]]))

(th "adantll" (pr ![φ ψ χ θ])
    (meta (:comment . "Deduction adding a conjunct to an antecedent."))
    (hypo [→ ∧ φ ψ χ])
    (ass [→ ∧ ∧ θ φ ψ χ])
    (proof
      [exp φ ψ χ]
      [(infer !an!add<) φ [→ ψ χ] θ]
      [imp [∧ θ φ] ψ χ]))

(th "adantlr" (pr ![φ ψ χ θ])
    (meta (:comment . "Deduction adding a conjunct to an antecedent."))
    (hypo [→ ∧ φ ψ χ])
    (ass [→ ∧ ∧ φ θ ψ χ])
    (proof
      [exp φ ψ χ]
      [(infer !an!add>) φ [→ ψ χ] θ]
      [imp [∧ φ θ] ψ χ]))

(th "adantrl" (pr ![φ ψ χ θ])
    (meta (:comment . "Deduction adding a conjunct to an antecedent."))
    (hypo [→ ∧ φ ψ χ])
    (ass [→ ∧ φ ∧ θ ψ χ])
    (proof
      [exp φ ψ χ]
      [(ded !an!add<) ψ χ θ φ]
      [imp φ [∧ θ ψ] χ]))

(th "adantrr" (pr ![φ ψ χ θ])
    (meta (:comment . "Deduction adding a conjunct to an antecedent."))
    (hypo [→ ∧ φ ψ χ])
    (ass [→ ∧ φ ∧ ψ θ χ])
    (proof
      [exp φ ψ χ]
      [(ded !an!add>) ψ χ θ φ]
      [imp φ [∧ ψ θ] χ]))

(th "adantlll" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Deduction adding a conjunct to an antecedent."))
    (hypo [→ ∧ ∧ φ ψ χ θ])
    (ass [→ ∧ ∧ ∧ τ φ ψ χ θ])
    (proof
      [exp31 φ ψ χ θ]
      [(infer !ax1) [→ φ → ψ → χ θ] τ]
      [imp41 τ φ ψ χ θ]))

(th "adantllr" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Deduction adding a conjunct to an antecedent."))
    (hypo [→ ∧ ∧ φ ψ χ θ])
    (ass [→ ∧ ∧ ∧ φ τ ψ χ θ])
    (proof
      [exp31 φ ψ χ θ]
      [(ded !ax1) [→ ψ → χ θ] τ φ]
      [imp41 φ τ ψ χ θ]))

(th "adantlrl" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Deduction adding a conjunct to an antecedent."))
    (hypo [→ ∧ ∧ φ ψ χ θ])
    (ass [→ ∧ ∧ φ ∧ τ ψ χ θ])
    (proof
      [exp31 φ ψ χ θ]
      [(ded !ax1) [→ ψ → χ θ] τ φ]
      [imp42 φ τ ψ χ θ]))

(th "adantlrr" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Deduction adding a conjunct to an antecedent."))
    (hypo [→ ∧ ∧ φ ψ χ θ])
    (ass [→ ∧ ∧ φ ∧ ψ τ χ θ])
    (proof
      [exp31 φ ψ χ θ]
      [a1dd φ ψ [→ χ θ] τ]
      [imp42 φ ψ τ χ θ]))

(th "adantrll" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Deduction adding a conjunct to an antecedent."))
    (hypo [→ ∧ φ ∧ ψ χ θ])
    (ass [→ ∧ φ ∧ ∧ τ ψ χ θ])
    (proof
      [exp32 φ ψ χ θ]
      [(ded !ax1) [→ ψ → χ θ] τ φ]
      [imp44 φ τ ψ χ θ]))

(th "adantrlr" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Deduction adding a conjunct to an antecedent."))
    (hypo [→ ∧ φ ∧ ψ χ θ])
    (ass [→ ∧ φ ∧ ∧ ψ τ χ θ])
    (proof
      [exp32 φ ψ χ θ]
      [a1dd φ ψ [→ χ θ] τ]
      [imp44 φ ψ τ χ θ]))

(th "adantrrl" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Deduction adding a conjunct to an antecedent."))
    (hypo [→ ∧ φ ∧ ψ χ θ])
    (ass [→ ∧ φ ∧ ψ ∧ τ χ θ])
    (proof
      [exp32 φ ψ χ θ]
      [a1dd φ ψ [→ χ θ] τ]
      [imp45 φ ψ τ χ θ]))

(th "adantrrr" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Deduction adding a conjunct to an antecedent."))
    (hypo [→ ∧ φ ∧ ψ χ θ])
    (ass [→ ∧ φ ∧ ψ ∧ χ τ θ])
    (proof
      [(ded !ax1) θ τ [∧ φ ∧ ψ χ]]
      [exp32 φ ψ χ [→ τ θ]]
      [imp45 φ ψ χ τ θ]))

(th "ad2antrr" (pr ![φ ψ χ θ])
    (meta (:comment . "Deduction adding conjuncts to an antecedent."))
    (hypo [→ φ ψ])
    (ass [→ ∧ ∧ φ χ θ ψ])
    (proof
      [(infer !an!add>) φ ψ χ]
      [(infer !an!add>) [∧ φ χ] ψ θ]))

(th "ad2antlr" (pr ![φ ψ χ θ])
    (meta (:comment . "Deduction adding conjuncts to an antecedent."))
    (hypo [→ φ ψ])
    (ass [→ ∧ ∧ χ φ θ ψ])
    (proof
      [(infer !an!add<) φ ψ χ]
      [(infer !an!add>) [∧ χ φ] ψ θ]))

(th "ad2antrl" (pr ![φ ψ χ θ])
    (meta (:comment . "Deduction adding conjuncts to an antecedent."))
    (hypo [→ φ ψ])
    (ass [→ ∧ χ ∧ φ θ ψ])
    (proof
      [(infer !an!add>) φ ψ θ]
      [(infer !an!add<) [∧ φ θ] ψ χ]))

(th "ad2antll" (pr ![φ ψ χ θ])
    (meta (:comment . "Deduction adding conjuncts to an antecedent."))
    (hypo [→ φ ψ])
    (ass [→ ∧ χ ∧ θ φ ψ])
    (proof
      [(infer !an!add<) φ ψ θ]
      [(infer !an!add<) [∧ θ φ] ψ χ]))

(th "ad2ant2l" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Deduction adding two conjuncts to antecedent."))
    (hypo [→ ∧ φ ψ χ])
    (ass [→ ∧ ∧ θ φ ∧ τ ψ χ])
    (proof
      [adantrl φ ψ χ τ]
      [adantll φ [∧ τ ψ] χ θ]))

(th "ad2ant2r" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Deduction adding two conjuncts to antecedent."))
    (hypo [→ ∧ φ ψ χ])
    (ass [→ ∧ ∧ φ θ ∧ ψ τ χ])
    (proof
      [adantrr φ ψ χ τ]
      [adantlr φ [∧ ψ τ] χ θ]))

(th "pm4.77" (pr ![φ ψ χ])
    (meta (:description . "Theorem *4.77 of Principia Mathematica p. 121."))
    (ass [↔ ∧ → ψ φ → χ φ → ∨ ψ χ φ])
    (proof
      [bjao ψ χ φ]
      [!bi!comi [→ ∨ ψ χ φ] [∧ → ψ φ → χ φ]]))

(th "jaoian" (pr ![φ ψ χ θ])
    (meta (:comment . "Inference disjoining the antecedents of two implications."))
    (hypo [→ ∧ φ ψ χ] [→ ∧ θ ψ χ])
    (ass [→ ∧ ∨ φ θ ψ χ])
    (proof
      [exp φ ψ χ]
      [exp θ ψ χ]
      [jaoi φ θ [→ ψ χ]]
      [imp [∨ φ θ] ψ χ]))

(th "jaodan" (pr ![φ ψ χ θ])
    (meta (:comment . "Deduction disjoining the antecedents of two implications."))
    (hypo [→ ∧ φ ψ χ] [→ ∧ φ θ χ])
    (ass [→ ∧ φ ∨ ψ θ χ])
    (proof
      [exp φ ψ χ]
      [exp φ θ χ]
      [jaod φ ψ θ χ]
      [imp φ [∨ ψ θ] χ]))

(th "pm2.63" (pr ![φ ψ])
    (meta (:description . "Theorem *2.63 of Principia Mathematica p. 107."))
    (ass [→ ∨ φ ψ → ∨ ¬ φ ψ ψ])
    (proof
      [(bi> !df-or) φ ψ]
      [!im!refld [∨ φ ψ] ψ]
      [jaod [∨ φ ψ] [¬ φ] ψ ψ]))

(th "pm2.64" (pr ![φ ψ])
    (meta (:description . "Theorem *2.64 of Principia Mathematica p. 107."))
    (ass [→ ∨ φ ψ → ∨ φ ¬ ψ φ])
    (proof
      [ax1 φ [∨ φ ψ]]
      [!or!elim> φ ψ]
      [jaoi φ [¬ ψ] [→ ∨ φ ψ φ]]
      [com12i [∨ φ ¬ ψ] [∨ φ ψ] φ]))

(th "pm3.44" (pr ![φ ψ χ])
    (meta (:description . "Theorem *3.44 of Principia Mathematica p. 113."))
    (ass [→ ∧ → ψ φ → χ φ → ∨ ψ χ φ])
    (proof
      [(bi< !bjao) ψ χ φ]))

(th "pm4.43" (pr ![φ ψ])
    (meta (:description . "Theorem *4.43 of Principia Mathematica p. 119."))
    (ass [↔ φ ∧ ∨ φ ψ ∨ φ ¬ ψ])
    (proof
      [or< φ ψ]
      [or< φ [¬ ψ]]
      [jca φ [∨ φ ψ] [∨ φ ¬ ψ]]
      [pm2.64 φ ψ]
      [imp [∨ φ ψ] [∨ φ ¬ ψ] φ]
      [>bii φ [∧ ∨ φ ψ ∨ φ ¬ ψ]]))

(th "pm4.24" (pr "φ")
    (meta (:comment . "Idempotent law for conjunction.")
          (:description . "Theorem *4.24 of Principia Mathematica p. 117."))
    (ass [↔ φ ∧ φ φ])
    (proof
      [!an!idm φ]
      [!bi!comi [∧ φ φ] φ]))

(th "pm3.22" (pr ![φ ψ])
    (meta (:comment . "Commutative law for conjunction.")
          (:description . "Theorem *3.22 of Principia Mathematica p. 3.22."))
    (ass [→ ∧ φ ψ ∧ ψ φ])
    (proof
      [(bi> !an!com) φ ψ]))

(th "imdistanri" (pr ![φ ψ χ])
    (meta (:comment . "Distribution of implication over conjunction."))
    (hypo [→ φ → ψ χ])
    (ass [→ ∧ ψ φ ∧ χ φ])
    (proof
      [com12i φ ψ χ]
      [impac ψ φ χ]))

(th "pm5.3" (pr ![φ ψ χ])
    (meta (:description . "Theorem *5.3 of Principia Mathematica p. 125."))
    (ass [↔ → ∧ φ ψ χ → ∧ φ ψ ∧ φ χ])
    (proof
      [(bi> !impexp) φ ψ χ]
      [(ded> !im!distan) φ ψ χ [→ ∧ φ ψ χ]]
      [!an!sim> φ χ]
      [(infer !syl<) [∧ φ χ] χ [∧ φ ψ]]
      [>bii [→ ∧ φ ψ χ] [→ ∧ φ ψ ∧ φ χ]]))

(th "pm5.61" (pr ![φ ψ])
    (meta (:comment . "Elimination of a redundant disjunct.")
          (:description . "Theorem *5.61 of Principia Mathematica p. 125."))
    (ass [↔ ∧ ∨ φ ψ ¬ ψ ∧ φ ¬ ψ])
    (proof
      [!or!elim> φ ψ]
      [imdistanri [¬ ψ] [∨ φ ψ] φ]
      [or< φ ψ]
      [(infer !an!imt<) φ [∨ φ ψ] [¬ ψ]]
      [>bii [∧ ∨ φ ψ ¬ ψ] [∧ φ ¬ ψ]]))

  (th "sylanb" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference."))
    (hypo [→ ∧ φ ψ χ] [↔ θ φ])
    (ass [→ ∧ θ ψ χ])
    (proof
      [(infer !bi>) θ φ]
      [!an!syl< φ ψ χ θ]))

  (th "sylanbr" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference."))
    (hypo [→ ∧ φ ψ χ] [↔ φ θ])
    (ass [→ ∧ θ ψ χ])
    (proof
      [(infer !bi<) φ θ]
      [!an!syl< φ ψ χ θ]))

  (th "sylan2b" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference."))
    (hypo [→ ∧ φ ψ χ] [↔ θ ψ])
    (ass [→ ∧ φ θ χ])
    (proof
      [(infer !bi>) θ ψ]
      [!an!syl> φ ψ χ θ]))

  (th "sylan2br" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference."))
    (hypo [→ ∧ φ ψ χ] [↔ ψ θ])
    (ass [→ ∧ φ θ χ])
    (proof
      [(infer !bi<) ψ θ]
      [!an!syl> φ ψ χ θ]))

  (th "syl2anb" (pr ![φ ψ χ θ τ])
    (meta (:comment . "A double syllogism inference."))
    (hypo [→ ∧ φ ψ χ] [↔ θ φ] [↔ τ ψ])
    (ass [→ ∧ θ τ χ])
    (proof
      [sylanb  φ ψ χ θ]
      [sylan2b θ ψ χ τ]))

  (th "syl2anbr" (pr ![φ ψ χ θ τ])
    (meta (:comment . "A double syllogism inference."))
    (hypo [→ ∧ φ ψ χ] [↔ φ θ] [↔ ψ τ])
    (ass [→ ∧ θ τ χ])
    (proof
      [sylanbr  φ ψ χ θ]
      [sylan2br θ ψ χ τ]))

(th "sylanl1" (pr ![φ ψ χ θ τ])
    (meta (:comment . "A syllogism inference."))
    (hypo [→ ∧ ∧ φ ψ χ θ] [→ τ φ])
    (ass [→ ∧ ∧ τ ψ χ θ])
    (proof
      [(infer !an!imt<) τ φ ψ]
      [!an!syl< [∧ φ ψ] χ θ [∧ τ ψ]]))

(th "sylanl2" (pr ![φ ψ χ θ τ])
    (meta (:comment . "A syllogism inference."))
    (hypo [→ ∧ ∧ φ ψ χ θ] [→ τ ψ])
    (ass  [→ ∧ ∧ φ τ χ θ])
    (proof
      [(infer !an!imt>) τ ψ φ]
      [!an!syl< [∧ φ ψ] χ θ [∧ φ τ]]))

(th "sylanr1" (pr ![φ ψ χ θ τ])
    (meta (:comment . "A syllogism inference."))
    (hypo [→ ∧ φ ∧ ψ χ θ] [→ τ ψ])
    (ass [→ ∧ φ ∧ τ χ θ])
    (proof
      [(infer !an!imt<) τ ψ χ]
      [!an!syl> φ [∧ ψ χ] θ [∧ τ χ]]))

(th "sylanr2" (pr ![φ ψ χ θ τ])
    (meta (:comment . "A syllogism inference."))
    (hypo [→ ∧ φ ∧ ψ χ θ] [→ τ χ])
    (ass [→ ∧ φ ∧ ψ τ θ])
    (proof
      [(infer !an!imt>) τ χ ψ]
      [!an!syl> φ [∧ ψ χ] θ [∧ ψ τ]]))

(th "syldan" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism deduction with conjoined antecedents."))
    (hypo [→ ∧ φ ψ χ] [→ ∧ φ χ θ])
    (ass  [→ ∧ φ ψ θ])
    (proof
      [!an!sim< φ ψ]
      [jca [∧ φ ψ] φ χ]
      [syl [∧ φ ψ] [∧ φ χ] θ]))

(th "sylan9r" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Nested syllogism inference conjoining dissimilar antecedents."))
    (hypo [→ φ → ψ χ] [→ θ → χ τ])
    (ass [→ ∧ θ φ → ψ τ])
    (proof
      [!im!syl9r φ ψ χ θ τ]
      [imp θ φ [→ ψ τ]]))

(th "sylancb" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference combined with contraction."))
    (hypo [→ ∧ φ ψ χ] [↔ θ φ] [↔ θ ψ])
    (ass [→ θ χ])
    (proof
      [(infer !bi>) θ φ]
      [(infer !bi>) θ ψ]
      [!an!sylc φ ψ χ θ]))

(th "sylancbr" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference combined with contraction."))
    (hypo [→ ∧ φ ψ χ] [↔ φ θ] [↔ ψ θ])
    (ass [→ θ χ])
    (proof
      [(infer !bi<) φ θ]
      [(infer !bi<) ψ θ]
      [!an!sylc φ ψ χ θ]))

(th "pm5.53" (pr ![φ ψ χ θ])
    (meta (:description . "Theorem *5.53 of Principia Mathematica p. 125."))
    (ass [↔ → ∨ ∨ φ ψ χ θ ∧ ∧ → φ θ → ψ θ → χ θ])
    (proof
      [bjao [∨ φ ψ] χ θ]
      [bjao φ ψ θ]
      [(infer !an!eqt<) [→ ∨ φ ψ θ] [∧ → φ θ → ψ θ] [→ χ θ]]
      [bitr [→ ∨ ∨ φ ψ χ θ] [∧ → ∨ φ ψ θ → χ θ] [∧ ∧ → φ θ → ψ θ → χ θ]]))

(th "anabs1" (pr ![φ ψ])
    (meta (:comment . "Absorption into embedded conjunct."))
    (ass [↔ ∧ ∧ φ ψ φ ∧ φ ψ])
    (proof
      [id [∧ φ ψ]]
      [!an!sim< φ ψ]
      [jca [∧ φ ψ] [∧ φ ψ] φ]
      [!an!sim< [∧ φ ψ] φ]
      [>bii [∧ ∧ φ ψ φ] [∧ φ ψ]]))

(th "anabsi5" (pr ![φ ψ χ])
    (meta (:comment . "Absorption of antecedent into conjunction."))
    (hypo [→ φ → ∧ φ ψ χ])
    (ass  [→ ∧ φ ψ χ])
    (proof
      [(infer !an!add>) φ [→ ∧ φ ψ χ] ψ]
      [(infer !im!abs) [∧ φ ψ] χ]))

(th "anabsi6" (pr ![φ ψ χ])
    (meta (:comment . "Absorption of antecedent into conjunction."))
    (hypo [→ φ → ∧ ψ φ χ])
    (ass  [→ ∧ φ ψ χ])
    (proof
      [!an!comsd φ ψ φ χ]
      [anabsi5 φ ψ χ]))

(th "anabsi7" (pr ![φ ψ χ])
    (meta (:comment . "Absorption of antecedent into conjunction."))
    (hypo [→ ψ → ∧ φ ψ χ])
    (ass  [→ ∧ φ ψ χ])
    (proof
      [(infer !an!add<) ψ [→ ∧ φ ψ χ] φ]
      [(infer !im!abs) [∧ φ ψ] χ]))

(th "anabsi8" (pr ![φ ψ χ])
    (meta (:comment . "Absorption of antecedent into conjunction."))
    (hypo [→ ψ → ∧ ψ φ χ])
    (ass  [→ ∧ φ ψ χ])
    (proof
      [!an!comsd ψ ψ φ χ]
      [anabsi7 φ ψ χ]))

(th "anabss1" (pr ![φ ψ χ])
    (meta (:comment . "Absorption of antecedent into conjunction."))
    (hypo [→ ∧ ∧ φ ψ φ χ])
    (ass [→ ∧ φ ψ χ])
    (proof
      [id [∧ φ ψ]]
      [!an!sim< φ ψ]
      [!an!sylc [∧ φ ψ] φ χ [∧ φ ψ]]))

(th "anabss3" (pr ![φ ψ χ])
    (meta (:comment . "Absorption of antecedent into conjunction."))
    (hypo [→ ∧ ∧ φ ψ ψ χ])
    (ass [→ ∧ φ ψ χ])
    (proof
      [id [∧ φ ψ]]
      [!an!sim> φ ψ]
      [!an!sylc [∧ φ ψ] ψ χ [∧ φ ψ]]))

(th "anabss4" (pr ![φ ψ χ])
    (meta (:comment . "Absorption of antecedent into conjunction."))
    (hypo [→ ∧ ∧ ψ φ ψ χ])
    (ass [→ ∧ φ ψ χ])
    (proof
      [(bi> !an!com) φ ψ]
      [!an!sim> φ ψ]
      [!an!sylc [∧ ψ φ] ψ χ [∧ φ ψ]]))

(th "anabsan" (pr ![φ ψ χ])
    (meta (:comment . "Absorption of antecedent with conjunction."))
    (hypo [→ ∧ ∧ φ φ ψ χ])
    (ass  [→ ∧ φ ψ χ])
    (proof
      [(+syl< !an!23ass) φ φ ψ χ]
      [anabss1 φ ψ χ]))

(th "anabsan2" (pr ![φ ψ χ])
    (meta (:comment . "Absorption of antecedent with conjunction."))
    (hypo [→ ∧ φ ∧ ψ ψ χ])
    (ass  [→ ∧ φ ψ χ])
    (proof
      [(+syl> !an!ass) φ ψ ψ χ]
      [anabss3 φ ψ χ]))

(th "syl5bb" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference from two biconditionals."))
    (hypo [→ φ ↔ ψ χ] [↔ θ ψ])
    (ass  [→ φ ↔ θ χ])
    (proof
      [(infer !ax1) [↔ θ ψ] φ]
      [!bi!trd φ θ ψ χ]))

(th "syl5rbb" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference from two biconditionals."))
    (hypo [→ φ ↔ ψ χ] [↔ θ ψ])
    (ass  [→ φ ↔ χ θ])
    (proof
      [syl5bb φ ψ χ θ]
      [(ded> !bi!com) θ χ φ]))

(th "syl5bbr" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference from two biconditionals."))
    (hypo [→ φ ↔ ψ χ] [↔ ψ θ])
    (ass  [→ φ ↔ θ χ])
    (proof
      [!bi!comi ψ θ]
      [syl5bb φ ψ χ θ]))

(th "syl5rbbr" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference from two biconditionals."))
    (hypo [→ φ ↔ ψ χ] [↔ ψ θ])
    (ass  [→ φ ↔ χ θ])
    (proof
      [!bi!comi ψ θ]
      [syl5rbb φ ψ χ θ]))

(th "syl6bb" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference from two biconditionals."))
    (hypo [→ φ ↔ ψ χ] [↔ χ θ])
    (ass  [→ φ ↔ ψ θ])
    (proof
      [(infer !ax1) [↔ χ θ] φ]
      [!bi!trd φ ψ χ θ]))

(th "syl6rbb" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference from two biconditionals."))
    (hypo [→ φ ↔ ψ χ] [↔ χ θ])
    (ass  [→ φ ↔ θ ψ])
    (proof
      [syl6bb φ ψ χ θ]
      [(ded> !bi!com) ψ θ φ]))

(th "syl6bbr" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference from two biconditionals."))
    (hypo [→ φ ↔ ψ χ] [↔ θ χ])
    (ass  [→ φ ↔ ψ θ])
    (proof
      [!bi!comi θ χ]
      [syl6bb φ ψ χ θ]))

(th "syl6rbbr" (pr ![φ ψ χ θ])
    (meta (:comment . "A syllogism inference from two biconditionals."))
    (hypo [→ φ ↔ ψ χ] [↔ θ χ])
    (ass  [→ φ ↔ θ ψ])
    (proof
      [!bi!comi θ χ]
      [syl6rbb φ ψ χ θ]))

(th "im2anan9r" (pr ![φ ψ χ θ τ η])
    (meta (:comment . "Deduction joining nested implications to form implication of conjunctions."))
    (hypo [→ φ → ψ χ] [→ θ → τ η])
    (ass [→ ∧ θ φ → ∧ ψ τ ∧ χ η])
    (proof
      [(infer !an!add<) φ [→ ψ χ] θ]
      [(infer !an!add>) θ [→ τ η] φ]
      [!an!imtd [∧ θ φ] ψ χ τ η]))

(th "pm2.73" (pr ![φ ψ χ])
    (meta (:description . "Theorem *2.73 of Principia Mathematica p. 108."))
    (ass [→ → φ ψ → ∨ ∨ φ ψ χ ∨ ψ χ])
    (proof
      [pm2.621 φ ψ]
      [(ded !or!imt<) [∨ φ ψ] ψ χ [→ φ ψ]]))

(th "pm2.74" (pr ![φ ψ χ])
    (meta (:description . "Theorem *2.74 of Principia Mathematica p. 108."))
    (ass [→ → ψ φ → ∨ ∨ φ ψ χ ∨ φ χ])
    (proof
      [!or!elim> φ ψ]
      [(ded !or!imt<) [∨ φ ψ] φ χ [¬ ψ]]
      [or< φ χ]
      [(ded !ax1) [∨ φ χ] [∨ ∨ φ ψ χ] φ]
      [!im!ja ψ φ [→ ∨ ∨ φ ψ χ ∨ φ χ]]))

(th "pm2.75" (pr ![φ ψ χ])
    (meta (:description . "Theorem *2.75 of Principia Mathematica p. 108."))
    (ass [→ ∨ φ ψ → ∨ φ → ψ χ ∨ φ χ])
    (proof
      [or< φ χ]
      [(ded !ax1) [∨ φ χ] [∨ φ → ψ χ] φ]
      [mpclosed ψ χ]
      [(ded !or!imt>) [→ ψ χ] χ φ ψ]
      [jaoi φ ψ [→ ∨ φ → ψ χ ∨ φ χ]]))

(th "pm2.76" (pr ![φ ψ χ])
    (meta (:description . "Theorem *2.76 of Principia Mathematica p. 108."))
    (ass [→ ∨ φ → ψ χ → ∨ φ ψ ∨ φ χ])
    (proof
      [pm2.75 φ ψ χ]
      [com12i [∨ φ ψ] [∨ φ → ψ χ] [∨ φ χ]]))

(th "pm2.8" (pr ![ψ χ θ])
    (meta (:description . "Theorem *2.8 of Principia Mathematica p. 108."))
    (ass [→ ∨ ψ χ → ∨ ¬ χ θ ∨ ψ θ])
    (proof
      [or< ψ θ]
      [(ded !ax1) [∨ ψ θ] [∨ ¬ χ θ] ψ]
      [>neg χ ψ]
      [(ded !or!imt<) [¬ χ] ψ θ χ]
      [jaoi ψ χ [→ ∨ ¬ χ θ ∨ ψ θ]]))

(th "pm2.81" (pr ![φ ψ χ θ])
    (meta (:description . "Theorem *2.81 of Principia Mathematica p. 108."))
    (ass [→ [→ ψ → χ θ] → ∨ φ ψ [→ ∨ φ χ ∨ φ θ]])
    (proof
      [!or!imt> ψ [→ χ θ] φ]
      [pm2.76 φ χ θ]
      [!im!rp33 [→ ψ → χ θ] [∨ φ ψ] [∨ φ → χ θ] [→ ∨ φ χ ∨ φ θ]]))

(th "pm2.82" (pr ![φ ψ χ θ])
    (meta (:description . "Theorem *2.82 of Principia Mathematica p. 108."))
    (ass [→ ∨ ∨ φ ψ χ → ∨ ∨ φ ¬ χ θ ∨ ∨ φ ψ θ])
    (proof
      [ax1 [∨ φ ψ] [∨ φ ¬ χ]]
      [>neg χ ψ]
      [(ded !or!imt>) [¬ χ] ψ φ χ]
      [jaoi [∨ φ ψ] χ [→ ∨ φ ¬ χ ∨ φ ψ]]
      [(ded !or!imt<) [∨ φ ¬ χ] [∨ φ ψ] θ [∨ ∨ φ ψ χ]]))

(th "orabs" (pr ![φ ψ])
    (meta (:comment . "Absorption of redundant internal disjunct.")
          (:description . "Compare Theorem *4.45 of Principia Mathematica p. 119."))
    (ass [↔ φ ∧ ∨ φ ψ φ])
    (proof
      [or< φ ψ]
      [(infer !an!join-r) φ [∨ φ ψ]]
      [!an!sim> [∨ φ ψ] φ]
      [>bii φ [∧ ∨ φ ψ φ]]))

(th "oranabs" (pr ![φ ψ])
    (meta (:comment . "Absorb a disjunct into a conjunct."))
    (ass [↔ ∧ ∨ φ ¬ ψ ψ ∧ φ ψ])
    (proof
      [>and φ ψ]
      [<neg ψ [∧ φ ψ]]
      [jaoi φ [¬ ψ] [→ ψ ∧ φ ψ]]
      [imp [∨ φ ¬ ψ] ψ [∧ φ ψ]]
      [or< φ [¬ ψ]]
      [(infer !an!imt<) φ [∨ φ ¬ ψ] ψ]
      [>bii [∧ ∨ φ ¬ ψ ψ] [∧ φ ψ]]))

(th "mpbidi" (pr ![φ ψ χ θ])
    (meta (:comment . "A deduction from a biconditional, related to modus ponens."))
    (hypo [→ θ → φ ψ] [→ φ ↔ ψ χ])
    (ass [→ θ → φ χ])
    (proof
      [(bi> (infer> !im!di-bi<)) φ ψ χ]
      [syl θ [→ φ ψ] [→ φ χ]]))

;(th "ibibr" (pr ![φ ψ])
;    (meta (:comment . "Implication in terms of implication and biconditional."))
;    (ass [↔ → φ ψ → φ ↔ ψ φ])
;    (proof
;      [im.imbi φ ψ]
;      [!bi!com φ ψ]
;      [(infer !im!eqt>) [↔ φ ψ] [↔ ψ φ] φ]
;      [bitr [→ φ ψ] [→ φ ↔ φ ψ] [→ φ ↔ ψ φ]]))

;(th "ibir" (pr ![φ ψ])
;    (meta (:comment . "Inference that converts a biconditional implied by one of its arguments, into an implication."))
;    (hypo [→ φ ↔ ψ φ])
;    (ass [→ φ ψ])
;    (proof
;      [(infer< !ibibr) φ ψ]))

(th "pm4.76" (pr ![φ ψ χ])
    (meta (:comment . "Distributive law for implication over conjunction.")
          (:description . "Theorem *4.76 of Principia Mathematica p. 121."))
    (ass [↔ ∧ → φ ψ → φ χ → φ ∧ ψ χ])
    (proof
      [!im!di-an< φ ψ χ]
      [!bi!comi [→ φ ∧ ψ χ] [∧ → φ ψ → φ χ]]))

(th "jctild" (pr ![φ ψ χ θ])
    (meta (:comment . "Deduction conjoining a theorem to left of consequent in an implication."))
    (hypo [→ φ → ψ χ] [→ φ θ])
    (ass [→ φ → ψ ∧ θ χ])
    (proof
      [(ded !ax1) θ ψ φ]
      [jcad φ ψ θ χ]))

(th "jctird" (pr ![φ ψ χ θ])
    (meta (:comment . "Deduction conjoining a theorem to right of consequent in an implication."))
    (hypo [→ φ → ψ χ] [→ φ θ])
    (ass [→ φ → ψ ∧ χ θ])
    (proof
      [(ded !ax1) θ ψ φ]
      [jcad φ ψ χ θ]))

(th "pm3.43" (pr ![φ ψ χ])
    (meta (:description . "Theorem *3.43 (Comp) of Principia Mathematica p. 113."))
    (ass [→ ∧ → φ ψ → φ χ → φ ∧ ψ χ])
    (proof
      [(bi< !im!di-an<) φ ψ χ]))

(th "bi2anan9r" (pr ![φ ψ χ θ τ η])
    (meta (:comment . "Deduction joining two equivalences to form equivalence of conjunctions."))
    (hypo [→ φ ↔ ψ χ] [→ θ ↔ τ η])
    (ass [→ ∧ θ φ ↔ ∧ ψ τ ∧ χ η])
    (proof
      [!an!2eqt φ ψ χ θ τ η]
      [(+syl< !an!com) φ θ [↔ ∧ ψ τ ∧ χ η]]))


(th "bi2bian9" (pr ![φ ψ χ θ τ η])
    (meta (:comment . "Deduction joining two biconditionals with different antecedents."))
    (hypo [→ φ ↔ ψ χ] [→ θ ↔ τ η])
    (ass [→ ∧ φ θ ↔ ∧ ψ τ ∧ χ η])
    (proof
      [(infer !an!add>) φ [↔ ψ χ] θ]
      [(infer !an!add<) θ [↔ τ η] φ]
      [!an!eqtd [∧ φ θ] ψ χ τ η]))

;; 600

(th "pm5.33" (pr ![φ ψ χ])
    (meta (:description . "Theorem *5.33 of Principia Mathematica p. 125."))
    (ass [↔ ∧ φ → ψ χ ∧ φ → ∧ φ ψ χ])
    (proof
      [!an!true< φ ψ]
      [(ded !im!eqt<) ψ [∧ φ ψ] χ φ]
      [(infer> !im.bidian) φ [→ ψ χ] [→ ∧ φ ψ χ]]))

(th "pm5.36" (pr ![φ ψ])
    (meta (:description . "Theorem *5.36 of Principia Mathematica p. 125."))
    (ass [↔ ∧ φ ↔ φ ψ ∧ ψ ↔ φ ψ])
    (proof
      [id [↔ φ ψ]]
      [(infer> !im.bidian-r) [↔ φ ψ] φ ψ]))

(th "pm5.42" (pr ![φ ψ χ])
    (meta (:description . "Theorem *5.42 of Principia Mathematica p. 125."))
    (ass [↔ → φ → ψ χ → φ → ψ ∧ φ χ])
    (proof
      [!an!true< φ χ]
      [(ded !im!eqt>) χ [∧ φ χ] ψ φ]
      [(infer> !im!di-bi<) φ [→ ψ χ] [→ ψ ∧ φ χ]]))


(th "oibabs" (pr ![φ ψ])
    (meta (:comment . "Absorption of disjunction into equivalence."))
    (ass [↔ ↔ φ ψ → ∨ φ ψ ↔ φ ψ])
    (proof
      [ax1 [↔ φ ψ] [∨ φ ψ]]
      [or< φ ψ]
      [(infer !im!imt<) φ [∨ φ ψ] [↔ φ ψ]]
      [(ded< !im.imbi) φ ψ [→ ∨ φ ψ ↔ φ ψ]]
      [or> φ ψ]
      [(infer !im!imt<) ψ [∨ φ ψ] [↔ φ ψ]]
      [(ded< !im.imbi-r) ψ φ [→ ∨ φ ψ ↔ φ ψ]]
      [>bid [→ ∨ φ ψ ↔ φ ψ] φ ψ]
      [>bii [↔ φ ψ] [→ ∨ φ ψ ↔ φ ψ]]))

(th "pm2.1" (pr "φ")
    (meta (:comment . "Law of excluded middle.")
          (:description . "Theorem *2.1 of Principia Mathematica p. 101."))
    (ass [∨ ¬ φ φ])
    (proof
      [exmid φ]
      [(infer> !or!com) φ [¬ φ]]))

(th "pm2.13" (pr "φ")
    (meta (:description . "Theorem *2.13 of Principia Mathematica p. 101."))
    (ass [∨ φ ¬ ¬ ¬ φ])
    (proof
      [neg> ¬ φ]
      [(infer< !df-or) φ [¬ ¬ ¬ φ]]))

(th "pm3.26" (pr ![φ ψ])
    (meta (:description . "Theorem *3.26 of Principia Mathematica p. 104."))
    (ass [∨ ¬ φ → → φ ψ ψ])
    (proof
      [neg< φ]
      [(infer !im!imt<) [¬ ¬ φ] φ ψ]
      [com12i [→ φ ψ] [¬ ¬ φ] ψ]
      [(infer< !df-or) [¬ φ] [→ → φ ψ ψ]]))


(th "pm5.11" (pr ![φ ψ])
    (meta (:description . "Theorem *5.11 of Principia Mathematica p. 123."))
    (ass [∨ → φ ψ → ¬ φ ψ])
    (proof
      [!lemmas!pm2.5 φ ψ]
      [(infer< !df-or) [→ φ ψ] [→ ¬ φ ψ]]))

(th "pm5.12" (pr ![φ ψ])
    (meta (:description . "Theorem *5.12 of Principia Mathematica p. 123."))
    (ass [∨ → φ ψ → φ ¬ ψ])
    (proof
      [!lemmas!pm2.51 φ ψ]
      [(infer< !df-or) [→ φ ψ] [→ φ ¬ ψ]]))

(th "pm5.13" (pr ![φ ψ])
    (meta (:description . "Theorem *5.13 of Principia Mathematica p. 123."))
    (ass [∨ → φ ψ → ψ φ])
    (proof
      [pm2.521 φ ψ]
      [(infer< !df-or) [→ φ ψ] [→ ψ φ]]))

(th "pm5.14" (pr ![φ ψ χ])
    (meta (:description . "Theorem *5.14 of Principia Mathematica p. 123."))
    (ass [∨ → φ ψ → ψ χ])
    (proof
      [ax1 ψ φ]
      [(infer !con) ψ [→ φ ψ]]
      [(ded !<neg) ψ χ [¬ → φ ψ]]
      [(infer< !df-or) [→ φ ψ] [→ ψ χ]]))

(th "pm5.17" (pr ![φ ψ])
    (meta (:description . "Theorem *5.17 of Principia Mathematica p. 124."))
    (ass [↔ ∧ ∨ φ ψ ¬ ∧ φ ψ ↔ φ ¬ ψ])
    (proof
      [!xor!df.an φ ψ]
      [!bi!neg> φ ψ]
      [!bi!tri> [∧ ∨ φ ψ ¬ ∧ φ ψ] [¬ ↔ φ ψ] [↔ φ ¬ ψ]]))

(th "pm5.55" (pr ![φ ψ])
    (meta (:description . "Theorem *5.55 of Principia Mathematica p. 125."))
    (ass [∨ ↔ ∨ φ ψ φ ↔ ∨ φ ψ ψ])
    (proof
      [pm5.13 ψ φ]
      [!im!df.cup ψ φ]
      [!or!com ψ φ]
      [(infer !bi!eqt>) [∨ ψ φ] [∨ φ ψ] φ]
      [!bi!com φ [∨ φ ψ]]
      [2bitr [→ ψ φ] [↔ φ ∨ ψ φ] [↔ φ ∨ φ ψ] [↔ ∨ φ ψ φ]]
      [!im!df.cup φ ψ]
      [!bi!com ψ [∨ φ ψ]]
      [bitr [→ φ ψ] [↔ ψ ∨ φ ψ] [↔ ∨ φ ψ ψ]]
      [(bi> !or!eqti) [→ ψ φ] [↔ ∨ φ ψ φ] [→ φ ψ] [↔ ∨ φ ψ ψ]]
      [ax-mp [∨ → ψ φ → φ ψ] [∨ ↔ ∨ φ ψ φ ↔ ∨ φ ψ ψ]]))

(th "pm5.21nd" (pr ![φ ψ χ θ])
    (meta (:comment . "Eliminate an antecedent implied by each side of a biconditional."))
    (hypo [→ ∧ φ ψ θ] [→ ∧ φ χ θ] [→ θ ↔ ψ χ])
    (ass [→ φ ↔ ψ χ])
    (proof
      [exp φ ψ θ]
      [(ded !con) ψ θ φ]
      [exp φ χ θ]
      [(ded !con) χ θ φ]
      [jcad φ [¬ θ] [¬ ψ] [¬ χ]]
      [false-eq ψ χ]
      [!im!rp33 φ [¬ θ] [∧ ¬ ψ ¬ χ] [↔ ψ χ]]
      [cased2 φ θ [↔ ψ χ]]))

(th "pm5.35" (pr ![φ ψ χ])
    (meta (:description . "Theorem *5.35 of Principia Mathematica p. 125."))
    (ass [→ ∧ → φ ψ → φ χ → φ ↔ ψ χ])
    (proof
      [true-eq [→ φ ψ] [→ φ χ]]
      [(ded< !im!di-bi<) φ ψ χ [∧ → φ ψ → φ χ]]))

(th "pm5.54" (pr ![φ ψ])
    (meta (:description . "Theorem *5.54 of Principia Mathematica p. 125."))
    (ass [∨ ↔ ∧ φ ψ φ ↔ ∧ φ ψ ψ])
    (proof
      [true-eq [∧ φ ψ] φ]
      [anabss1 φ ψ [↔ ∧ φ ψ φ]]
      [!an!true> φ ψ]
      [(ded> !bi!com) φ [∧ φ ψ] ψ]
      [false-eqi [∧ φ ψ] ψ [↔ ∧ φ ψ φ]]
      [(infer< !df-or) [↔ ∧ φ ψ φ] [↔ ∧ φ ψ ψ]]))

(th "elimant" (pr ![φ ψ χ θ])
    (meta (:comment . "Elimination of antecedents in an implication."))
    (ass [→ [∧ → φ ψ → → ψ χ → φ θ] [→ φ → χ θ]])
    (proof
      (suppose [∧ → φ ψ → → ψ χ → φ θ]
        [(infer !an!sim>) [→ φ ψ] [→ → ψ χ → φ θ]]
        [(+syl !ax1) χ ψ [→ φ θ]]
        [com12i χ φ θ])
      (claim [→ [∧ → φ ψ → → ψ χ → φ θ] [→ φ → χ θ]])))

(th "pm5.44" (pr ![φ ψ χ])
    (meta (:description . "Theorem *5.44 of Principia Mathematica p. 125."))
    (ass [→ → φ ψ ↔ → φ χ → φ ∧ ψ χ])
    (proof
      [!im!di-an< φ ψ χ]
      [!an!true<i-r [→ φ ∧ ψ χ] [→ φ ψ] [→ φ χ]]))

(th "orcana" (pr ![φ ψ χ])
    (meta (:comment . "Disjunction in consequent versus conjunction in antecedent.")
          (:description . "Similar to Theorem *5.6 of Principia Mathematica p. 125."))
    (ass [↔ → φ ∨ ψ χ → ∧ φ ¬ ψ χ])
    (proof
      [df-or ψ χ]
      [(infer !im!eqt>) [∨ ψ χ] [→ ¬ ψ χ] φ]
      [impexp φ [¬ ψ] χ]
      [!bi!tri< [→ φ ∨ ψ χ] [→ φ → ¬ ψ χ] [→ ∧ φ ¬ ψ χ]]))

(th "pm5.6" (pr ![φ ψ χ])
    (meta (:description . "Theorem *5.6 of Principia Mathematica p. 125."))
    (ass [↔ → ∧ φ ¬ ψ χ → φ ∨ ψ χ])
    (proof
      [orcana φ ψ χ]
      [!bi!comi [→ φ ∨ ψ χ] [→ ∧ φ ¬ ψ χ]]))

(th "biluk" (pr ![φ ψ χ])
    (meta (:comment . "Lukasiewicz's shortest axiom for equivalential calculus.")
          (:description . "Storrs McCall, ed., <i>Polish Logic 1920-1939</i> (Oxford, 1967), p. 96."))
    (ass [↔ ↔ φ ψ ↔ ↔ χ ψ ↔ φ χ])
    (proof
      [!bi!com φ ψ]
      [(infer !bi!eqt<) [↔ φ ψ] [↔ ψ φ] χ]
      [!bi!ass ψ φ χ]
      [bitr [↔ ↔ φ ψ χ] [↔ ↔ ψ φ χ] [↔ ψ ↔ φ χ]]
      [(infer (bi> !bi!ass)) [↔ φ ψ] χ [↔ ψ ↔ φ χ]]
      [!bi!ass χ ψ [↔ φ χ]]
      [!bi!tri< [↔ φ ψ] [↔ χ ↔ ψ ↔ φ χ] [↔ ↔ χ ψ ↔ φ χ]]))

(th "pm5.7" (pr ![φ ψ χ])
    (meta (:comment . "Disjunction distributes over the biconditional.")
          (:description . "Theorem *5.7 of Principia Mathematica p. 125."))
    (ass [↔ ↔ ∨ φ χ ∨ ψ χ ∨ χ ↔ φ ψ])
    (proof
      [!or!di-bi< χ φ ψ]
      [!or!com χ φ]
      [!or!com χ ψ]
      [!bi!eqti [∨ χ φ] [∨ φ χ] [∨ χ ψ] [∨ ψ χ]]
      [!bi!tri-r [∨ χ ↔ φ ψ] [↔ ∨ χ φ ∨ χ ψ] [↔ ∨ φ χ ∨ ψ χ]]))

(th "bigolden" (pr ![φ ψ])
    (meta (:comment . "Dijkstra-Scholten's Golden Rule for calculational proofs."))
    (ass [↔ ↔ ∧ φ ψ φ ↔ ψ ∨ φ ψ])
    (proof
      [!im!df.cap φ ψ]
      [!im!df.cup φ ψ]
      [!bi!com φ [∧ φ ψ]]
      [!bi!>tr-r [→ φ ψ]     [↔ φ ∧ φ ψ]
                 [↔ ψ ∨ φ ψ] [↔ ∧ φ ψ φ]]))

(th "pm5.71" (pr ![φ ψ χ])
    (meta (:description . "Theorem *5.71 of Principia Mathematica p. 125."))
    (ass [→ → ψ ¬ χ ↔ ∧ ∨ φ ψ χ ∧ φ χ])
    (proof
      [!or!elim> φ ψ]
      [or< φ ψ]
      [(infer !ax1) [→ φ ∨ φ ψ] [¬ ψ]]
      [>bid [¬ ψ] [∨ φ ψ] φ]
      [(ded !an!eqt<) [∨ φ ψ] φ χ [¬ ψ]]
      [<neg χ [↔ ∨ φ ψ φ]]
      [(ded> !im.bidian-r) χ [∨ φ ψ] φ [¬ χ]]
      [!im!ja ψ [¬ χ] [↔ ∧ ∨ φ ψ χ ∧ φ χ]]))

(th "pm5.75" (pr ![φ ψ χ])
    (meta (:description . "Theorem *5.75 of Principia Mathematica p. 126."))
    (ass [→ ∧ → χ ¬ ψ ↔ φ ∨ ψ χ ↔ ∧ φ ¬ ψ χ])
    (proof
      (suppose [∧ → χ ¬ ψ ↔ φ ∨ ψ χ]
        [siman [→ χ ¬ ψ] [↔ φ ∨ ψ χ]]
        [(infer !bi<) φ [∨ ψ χ]]
        [(+syl !or>) ψ χ φ]
        [jca χ φ [¬ ψ]]
        [(infer !bi>) φ [∨ ψ χ]]
        [(ded> !df-or) ψ χ φ]
        [imp φ [¬ ψ] χ]
        [>bii [∧ φ ¬ ψ] χ])
      (claim [→ ∧ → χ ¬ ψ ↔ φ ∨ ψ χ ↔ ∧ φ ¬ ψ χ])))

(th "nan" (pr ![φ ψ χ])
    (meta (:comment . "Theorem to move a conjunct in and out of a negation."))
    (ass [↔ → φ ¬ ∧ ψ χ → ∧ φ ψ ¬ χ])
    (proof
      [impexp φ ψ [¬ χ]]
      [!im!df.nan ψ χ]
      [(infer !im!eqt>) [→ ψ ¬ χ] [¬ ∧ ψ χ] φ]
      [!bi!tri-r [→ ∧ φ ψ ¬ χ] [→ φ → ψ ¬ χ] [→ φ ¬ ∧ ψ χ]]))

(th "orcanai" (pr ![φ ψ χ])
    (meta (:comment . "Change disjunction in consequent to conjunction in antecedent."))
    (hypo [→ φ ∨ ψ χ])
    (ass [→ ∧ φ ¬ ψ χ])
    (proof
      [(ded> !df-or) ψ χ φ]
      [imp φ [¬ ψ] χ]))

(th "mpani" (pr ![φ ψ χ θ])
    (meta (:comment . "An inference based on modus ponens."))
    (hypo [→ φ → ∧ ψ χ θ] [ψ])
    (ass [→ φ → χ θ])
    (proof
      [(ded> !impexp) ψ χ θ φ]
      [mpi φ ψ [→ χ θ]]))

(th "mpan2i" (pr ![φ ψ χ θ])
    (meta (:comment . "An inference based on modus ponens."))
    (hypo [→ φ → ∧ ψ χ θ] [χ])
    (ass [→ φ → ψ θ])
    (proof
      [(ded> !impexp) ψ χ θ φ]
      [mpii φ ψ χ θ]))

(th "mp2ani" (pr ![φ ψ χ θ])
    (meta (:comment . "An inference based on modus ponens."))
    (hypo [→ φ → ∧ ψ χ θ] [ψ] [χ])
    (ass [→ φ θ])
    (proof
      [mpani φ ψ χ θ]
      [mpi φ χ θ]))

(th "mpand" (pr ![φ ψ χ θ])
    (meta (:comment . "A deduction based on modus ponens."))
    (hypo [→ φ ψ] [→ φ → ∧ ψ χ θ])
    (ass [→ φ → χ θ])
    (proof
      [(ded> !impexp) ψ χ θ φ]
      [mpd φ ψ [→ χ θ]]))

(th "mpan2d" (pr ![φ ψ χ θ])
    (meta (:comment . "A deduction based on modus ponens."))
    (hypo [→ φ χ] [→ φ → ∧ ψ χ θ])
    (ass [→ φ → ψ θ])
    (proof
      [(ded> !impexp) ψ χ θ φ]
      [!im!com23i φ ψ χ θ]
      [mpd φ χ [→ ψ θ]]))

(th "mp2and" (pr ![φ ψ χ θ])
    (meta (:comment . "A deduction based on modus ponens."))
    (hypo [→ φ → ∧ ψ χ θ] [→ φ ψ] [→ φ χ])
    (ass [→ φ θ])
    (proof
      [mpand φ ψ χ θ]
      [mpd φ χ θ]))

(th "mpan11" (pr ![φ ψ χ θ])
    (meta (:comment . "An inference based on modus ponens."))
    (hypo [φ] [→ ∧ ∧ φ ψ χ θ])
    (ass [→ ∧ ψ χ θ])
    (proof
      [exp [∧ φ ψ] χ θ]
      [!an!mp< φ ψ [→ χ θ]]
      [imp ψ χ θ]))

(th "mpan12" (pr ![φ ψ χ θ])
    (meta (:comment . "An inference based on modus ponens."))
    (hypo [ψ] [→ ∧ ∧ φ ψ χ θ])
    (ass [→ ∧ φ χ θ])
    (proof
      [exp [∧ φ ψ] χ θ]
      [!an!mp> φ ψ [→ χ θ]]
      [imp φ χ θ]))

(th "mpanl12" (pr ![φ ψ χ θ])
    (meta (:comment . "An inference based on modus ponens."))
    (hypo [φ] [ψ] [→ ∧ ∧ φ ψ χ θ])
    (ass [→ χ θ])
    (proof
      [>andi φ ψ]
      [!an!mp< [∧ φ ψ] χ θ]))

(th "mpanr1" (pr ![φ ψ χ θ])
    (meta (:comment . "An inference based on modus ponens."))
    (hypo [ψ] [→ ∧ φ ∧ ψ χ θ])
    (ass [→ ∧ φ χ θ])
    (proof
      [exp φ [∧ ψ χ] θ]
      [mpani φ ψ χ θ]
      [imp φ χ θ]))

(th "mpanr2" (pr ![φ ψ χ θ])
    (meta (:comment . "An inference based on modus ponens."))
    (hypo [χ] [→ ∧ φ ∧ ψ χ θ])
    (ass [→ ∧ φ ψ θ])
    (proof
      [exp φ [∧ ψ χ] θ]
      [mpan2i φ ψ χ θ]
      [imp φ ψ θ]))

(th "anmplr1" (pr ![φ ψ χ θ τ])
    (meta (:comment . "An inference based on modus ponens."))
    (hypo [→ ∧ ∧ φ ∧ ψ χ θ τ] [ψ])
    (ass [→ ∧ ∧ φ χ θ τ])
    (proof
      [exp [∧ φ ∧ ψ χ] θ τ]
      [mpanr1 φ ψ χ [→ θ τ]]
      [imp [∧ φ χ] θ τ]))

(th "mtt" (pr ![φ ψ])
    (meta (:comment . "Modus-tollens-like theorem."))
    (ass [→ ¬ φ ↔ ¬ ψ → ψ φ])
    (proof
      [<neg ψ φ]
      [(infer !ax1) [→ ¬ ψ → ψ φ] [¬ φ]]
      [con ψ φ]
      [com12i [→ ψ φ] [¬ φ] [¬ ψ]]
      [>bid [¬ φ] [¬ ψ] [→ ψ φ]]))

(th "mt2bi" (pr ![φ ψ])
    (meta (:comment . "A false consequent falsifies an antecedent."))
    (hypo [φ])
    (ass [↔ ¬ ψ → ψ ¬ φ])
    (proof
      [<neg ψ [¬ φ]]
      [con> ψ φ]
      [mpi [→ ψ ¬ φ] φ [¬ ψ]]
      [>bii [¬ ψ] [→ ψ ¬ φ]]))

(th "mtbid" (pr ![φ ψ χ])
    (meta (:comment . "A deduction from a biconditional, similar to modus tollens."))
    (hypo [→ φ ¬ ψ] [→ φ ↔ ψ χ])
    (ass [→ φ ¬ χ])
    (proof
      [(ded !bi<) ψ χ φ]
      [mtod φ χ ψ]))

(th "mtbird" (pr ![φ ψ χ])
    (meta (:comment . "A deduction from a biconditional, similar to modus tollens."))
    (hypo [→ φ ¬ χ] [→ φ ↔ ψ χ])
    (ass [→ φ ¬ ψ])
    (proof
      [(ded !bi>) ψ χ φ]
      [mtod φ ψ χ]))

(th "mtbii" (pr ![φ ψ χ])
    (meta (:comment . "A deduction from a biconditional, similar to modus tollens."))
    (hypo [¬ ψ] [→ φ ↔ ψ χ])
    (ass [→ φ ¬ χ])
    (proof
      [(infer !ax1) [¬ ψ] [φ]]
      [mtbid φ ψ χ]))

(th "mtbiri" (pr ![φ ψ χ])
    (meta (:comment . "A deduction from a biconditional, similar to modus tollens."))
    (hypo [¬ χ] [→ φ ↔ ψ χ])
    (ass [→ φ ¬ ψ])
    (proof
      [(infer !ax1) [¬ χ] φ]
      [mtbird φ ψ χ]))

(th "mpbir2an" (pr ![φ ψ χ])
    (meta (:comment . "Detach a conjunction of truths in a biconditional."))
    (hypo [↔ φ ∧ ψ χ] [ψ] [χ])
    (ass [φ])
    (proof
      [>andi ψ χ]
      [(infer !bi<) φ [∧ ψ χ]]
      [ax-mp [∧ ψ χ] φ]))

(th "pm5.5" (pr ![φ ψ])
    (meta (:description . "Theorem *5.5 of Principia Mathematica p. 125."))
    (ass [→ φ ↔ → φ ψ ψ])
    (proof
      [!im!true< φ ψ]
      [(ded> !bi!com) ψ [→ φ ψ] φ]))

(th "pm5.62" (pr ![φ ψ])
    (meta (:description . "Theorem *5.62 of Principia Mathematica p. 125."))
    (ass [↔ ∨ ∧ φ ψ ¬ ψ ∨ φ ¬ ψ])
    (proof
      [!or!di-an> φ ψ [¬ ψ]]
      [exmid ψ]
      [(infer !an!true>i) [∨ ∧ φ ψ ¬ ψ] [∨ φ ¬ ψ] [∨ ψ ¬ ψ]]))

(th "pm4.82" (pr ![φ ψ])
    (meta (:description . "Theorem *4.82 of Principia Mathematica p. 122."))
    (ass [↔ ∧ → φ ψ → φ ¬ ψ ¬ φ])
    (proof
      [ncase φ ψ]
      [imp [→ φ ψ] [→ φ ¬ ψ] ¬ φ]
      [<neg φ ψ]
      [<neg φ [¬ ψ]]
      [jca [¬ φ] [→ φ ψ] [→ φ ¬ ψ]]
      [>bii [∧ → φ ψ → φ ¬ ψ] [¬ φ]]))

(th "pm4.83" (pr ![φ ψ])
    (meta (:description . "Theorem *4.83 of Principia Mathematica p. 122"))
    (ass [↔ ∧ → φ ψ → ¬ φ ψ ψ])
    (proof
      [case φ ψ]
      [imp [→ φ ψ] [→ ¬ φ ψ] ψ]
      [ax1 ψ φ]
      [ax1 ψ [¬ φ]]
      [jca ψ [→ φ ψ] [→ ¬ φ ψ]]
      [>bii [∧ → φ ψ → ¬ φ ψ] ψ]))

(th "pclem6" (pr ![φ ψ])
    (meta (:comment . "Negation inferred from embedded conjunct."))
    (ass [→ ↔ φ ∧ ψ ¬ φ ¬ ψ])
    (proof
      [!an!true< ψ [¬ φ]]
      [(ded !bi!eqt>) [¬ φ] [∧ ψ ¬ φ] φ ψ]
      [(ded !neqt) [↔ φ ¬ φ] [↔ φ ∧ ψ ¬ φ] ψ]
      [(ded !bi>) [¬ ↔ φ ¬ φ] [¬ ↔ φ ∧ ψ ¬ φ] ψ]
      [!xor!refl φ]
      [mpi ψ [¬ ↔ φ ¬ φ] [¬ ↔ φ ∧ ψ ¬ φ]]
      [(ded !<neg) [↔ φ ∧ ψ ¬ φ] [¬ ψ] ψ]
      [ax1 [¬ ψ] [↔ φ ∧ ψ ¬ φ]]
      [casei ψ [→ ↔ φ ∧ ψ ¬ φ ¬ ψ]]))

(th "bimsc1" (pr ![φ ψ χ])
    (meta (:comment . "Removal of conjunct from one side of an equivalence."))
    (ass [→ ∧ → φ ψ ↔ χ ∧ ψ φ ↔ χ φ])
    (proof
      [id ↔ χ ∧ ψ φ]
      [(bi> !im!df.cap-r) φ ψ]
      [(ded> !bi!com) φ [∧ ψ φ] [→ φ ψ]]
      [!an!bsyl9-r [↔ χ ∧ ψ φ] χ [∧ ψ φ] [→ φ ψ] φ]))

(th "ecase2d" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Deduction for elimination by cases."))
    (hypo [→ φ ψ] [→ φ ¬ ∧ ψ χ] [→ φ ¬ ∧ ψ θ] [→ φ ∨ τ ∨ χ θ])
    (ass [→ φ τ])
    (proof
      (suppose [φ]
        [(infer> !an!neg) ψ χ]
        [(infer< !im!df.or) ψ [¬ χ]]
        [ax-mp ψ [¬ χ]]
        [(infer> !an!neg) ψ θ]
        [(infer< !im!df.or) ψ [¬ θ]]
        [ax-mp ψ [¬ θ]]
        [>nor χ θ]
        [(infer !or!elim>) τ [∨ χ θ]]
        [ax-mp [∨ τ ∨ χ θ] τ])
      (claim [→ φ τ])))

(th "ecase3" (pr ![φ ψ χ])
    (meta (:comment . "Inference for elimination by cases."))
    (hypo [→ φ χ] [→ ψ χ] [→ ¬ ∨ φ ψ χ])
    (ass [χ])
    (proof
      [(bi< !or!neg) φ ψ]
      [syl [∧ ¬ φ ¬ ψ] [¬ ∨ φ ψ] χ]
      [exp [¬ φ] [¬ ψ] χ]
      [caseii φ ψ χ]))

(th "ecase" (pr ![φ ψ χ])
    (meta (:comment . "Inference for elimination by cases."))
    (hypo [→ ¬ φ χ] [→ ¬ ψ χ] [→ ∧ φ ψ χ])
    (ass [χ])
    (proof
      [exp φ ψ χ]
      [pm2.61nii φ ψ χ]))

(th "ecase3d" (pr ![φ ψ χ θ])
    (meta (:comment . "Deduction for elimination by cases."))
    (hypo [→ φ → ψ θ] [→ φ → χ θ] [→ φ → ¬ ∨ ψ χ θ])
    (ass [→ φ θ])
    (proof
      [com12i φ ψ θ]
      [com12i φ χ θ]
      [com12i φ [¬ ∨ ψ χ] θ]
      [ecase3 ψ χ [→ φ θ]]))

(th "caselem" (pr ![φ ψ χ θ])
    (meta (:comment . "Lemma for combining cases."))
    (ass [↔ [∧ ∨ φ ψ ∨ χ θ] [∨ ∨ ∧ φ χ ∧ ψ χ ∨ ∧ φ θ ∧ ψ θ]])
    (proof
      [!an!di-or< [∨ φ ψ] χ θ]
      [!an!di-or> φ ψ χ]
      [!an!di-or> φ ψ θ]
      [!or!eqti [∧ ∨ φ ψ χ] [∨ ∧ φ χ ∧ ψ χ]
                [∧ ∨ φ ψ θ] [∨ ∧ φ θ ∧ ψ θ]]
      [bitr [∧ ∨ φ ψ ∨ χ θ] [∨ ∧ ∨ φ ψ χ ∧ ∨ φ ψ θ] [∨ ∨ ∧ φ χ ∧ ψ χ ∨ ∧ φ θ ∧ ψ θ]]))

(th "ccase" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Inference for combining cases."))
    (hypo [→ ∧ φ ψ τ] [→ ∧ χ ψ τ] [→ ∧ φ θ τ] [→ ∧ χ θ τ])
    (ass [→ ∧ ∨ φ χ ∨ ψ θ τ])
    (proof
      [jaoi [∧ φ ψ] [∧ χ ψ] τ]
      [jaoi [∧ φ θ] [∧ χ θ] τ]
      [jaoi [∨ ∧ φ ψ ∧ χ ψ] [∨ ∧ φ θ ∧ χ θ] τ]
      [(+syl> !caselem) φ χ ψ θ τ]))

(th "ccased" (pr ![φ ψ χ θ τ η])
    (meta (:comment . "Deduction for combining cases."))
    (hypo [→ φ → ∧ ψ χ η] [→ φ → ∧ θ χ η] [→ φ → ∧ ψ τ η] [→ φ → ∧ θ τ η])
    (ass [→ φ → ∧ ∨ ψ θ ∨ χ τ η])
    (proof
      (suppose [φ]
        [ccase ψ χ θ τ η])
      (claim [→ φ → ∧ ∨ ψ θ ∨ χ τ η])))

(th "ccase2" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Inference for combining cases."))
    (hypo [→ ∧ φ ψ τ] [→ χ τ] [→ θ τ])
    (ass [→ ∧ ∨ φ χ ∨ ψ θ τ])
    (proof
      [(+syl !an!sim<) χ ψ τ]
      [(+syl !an!sim>) φ θ τ]
      [(+syl !an!sim>) χ θ τ]
      [ccase φ ψ χ θ τ]))

(th "4cases" (pr ![φ ψ χ])
    (meta (:comment . "Inference eliminating two antecedents from the four possible cases
that result from their true/false combinations."))
    (hypo [→ ∧ φ ψ χ] [→ ∧ φ ¬ ψ χ] [→ ∧ ¬ φ ψ χ] [→ ∧ ¬ φ ¬ ψ χ])
    (ass [χ])
    (proof
      [exp φ ψ χ]
      [exp [¬ φ] ψ χ]
      [casei φ [→ ψ χ]]
      [exp φ [¬ ψ] χ]
      [exp [¬ φ] [¬ ψ] χ]
      [casei φ [→ ¬ ψ χ]]
      [casei ψ χ]))

(th "niabn" (pr ![φ ψ χ])
    (meta (:comment . "Miscellaneous inference relating falsehoods."))
    (hypo [φ])
    (ass [→ ¬ ψ ↔ ∧ χ ψ ¬ φ])
    (proof
      [!an!sim> χ ψ]
      [(infer !>neg) φ ψ]
      [false-eqi [∧ χ ψ] [¬ φ] ψ]))

(th "dedlema" (pr ![φ ψ χ])
    (meta (:comment . "Lemma for weak deduction theorem."))
    (ass [→ φ ↔ ψ ∨ ∧ ψ φ ∧ χ ¬ φ])
    (proof
      (suppose [φ]
        [!an!jct> ψ φ]
        [(ded !or<) [∧ ψ φ] [∧ χ ¬ φ] ψ]
        [!an!sim> χ [¬ φ]]
        [(infer (infer !con>)) [∧ χ ¬ φ] φ]
        [(infer !or!elim>) [∧ ψ φ] [∧ χ ¬ φ]]
        [(ded !an!sim<) ψ φ [∨ ∧ ψ φ ∧ χ ¬ φ]]
        [>bii ψ [∨ ∧ ψ φ ∧ χ ¬ φ]])
      (claim [→ φ ↔ ψ ∨ ∧ ψ φ ∧ χ ¬ φ])))

(th "dedlemb" (pr ![φ ψ χ])
    (meta (:comment . "Lemma for weak deduction theorem."))
    (ass [→ ¬ φ ↔ χ ∨ ∧ ψ φ ∧ χ ¬ φ])
    (proof
      (suppose [¬ φ]
        [!an!jct> χ [¬ φ]]
        [(ded !or>) [∧ ψ φ] [∧ χ ¬ φ] χ]
        [!an!sim> ψ φ]
        [(infer (infer !con)) [∧ ψ φ] φ]
        [(infer !or!elim<) [∧ ψ φ] [∧ χ ¬ φ]]
        [(ded !an!sim<) χ [¬ φ] [∨ ∧ ψ φ ∧ χ ¬ φ]]
        [>bii χ [∨ ∧ ψ φ ∧ χ ¬ φ]])
      (claim [→ ¬ φ ↔ χ ∨ ∧ ψ φ ∧ χ ¬ φ])))

(th "elimh" (pr ![φ ψ χ θ τ])
    (meta (:comment . "Hypothesis builder for weak deduction theorem."))
    (hypo [→ ↔ φ ∨ ∧ φ χ ∧ ψ ¬ χ ↔ χ τ]
          [→ ↔ ψ ∨ ∧ φ χ ∧ ψ ¬ χ ↔ θ τ] [θ])
    (ass [τ])
    (proof
      [dedlema χ φ ψ]
      [syl χ [↔ φ ∨ ∧ φ χ ∧ ψ ¬ χ] [↔ χ τ]]
      [(infer< !im.imbi) χ τ]
      [dedlemb χ φ ψ]
      [syl [¬ χ] [↔ ψ ∨ ∧ φ χ ∧ ψ ¬ χ] [↔ θ τ]]
      [!bi!mpi> [¬ χ] θ τ]
      [casei χ τ]))

(th "dedt" (pr ![φ ψ χ θ τ])
    (meta (:comment . "The weak deduction theorem."))
    (hypo [→ ↔ φ ∨ ∧ φ χ ∧ ψ ¬ χ ↔ θ τ] [τ])
    (ass [→ χ θ])
    (proof
      [dedlema χ φ ψ]
      [!bi!mpi< [↔ φ ∨ ∧ φ χ ∧ ψ ¬ χ] θ τ]
      [syl χ [↔ φ ∨ ∧ φ χ ∧ ψ ¬ χ] θ]))

(th "consensus" (pr ![φ ψ χ])
    (meta (:comment . "The consensus theorem.")
          (:description . "This theorem and its dual are commonly used in computer logic design
to eliminate redundant terms from Boolean expressions. Specifically, we show the term -- on the
left-hand side is redundant."))
    (ass [↔ ∨ [∨ ∧ φ ψ ∧ ¬ φ χ] ∧ ψ χ [∨ ∧ φ ψ ∧ ¬ φ χ]])
    (proof
      [or< [∨ ∧ φ ψ ∧ ¬ φ χ] [∧ ψ χ]]
      [id [∨ ∧ φ ψ ∧ ¬ φ χ]]
      [!an!sim< ψ χ]
      [or< [∧ φ ψ] [∧ ¬ φ χ]]
      [exp φ ψ [∨ ∧ φ ψ ∧ ¬ φ χ]]
      [!im!rp32 φ ψ [∨ ∧ φ ψ ∧ ¬ φ χ] [∧ ψ χ]]
      [!an!sim> ψ χ]
      [or> [∧ φ ψ] [∧ ¬ φ χ]]
      [exp [¬ φ] χ [∨ ∧ φ ψ ∧ ¬ φ χ]]
      [!im!rp32 [¬ φ] χ [∨ ∧ φ ψ ∧ ¬ φ χ] [∧ ψ χ]]
      [casei φ [→ ∧ ψ χ ∨ ∧ φ ψ ∧ ¬ φ χ]]
      [jaoi [∨ ∧ φ ψ ∧ ¬ φ χ] [∧ ψ χ] [∨ ∧ φ ψ ∧ ¬ φ χ]]
      [>bii [∨ ∨ ∧ φ ψ ∧ ¬ φ χ ∧ ψ χ] [∨ ∧ φ ψ ∧ ¬ φ χ]]))

(th "pm4.42" (pr ![φ ψ])
    (meta (:description . "Theorem *4.42 of Principia Mathematica p. 119."))
    (ass [↔ φ ∨ ∧ φ ψ ∧ φ ¬ ψ])
    (proof
      [dedlema ψ φ φ]
      [dedlemb ψ φ φ]
      [casei ψ [↔ φ ∨ ∧ φ ψ ∧ φ ¬ ψ]]))

(th "ninba" (pr ![φ ψ χ])
    (meta (:comment . "Miscellaneous inference relating falsehoods."))
    (hypo [φ])
    (ass [→ ¬ ψ ↔ ¬ φ ∧ χ ψ])
    (proof
      [niabn φ ψ χ]
      [(ded> !bi!com) [∧ χ ψ] [¬ φ] [¬ ψ]]))

#|
(th "eq.iman+" (pr ("φ" "ψ"))
    (ass [→ ↔ φ ψ → φ ∧ φ ψ])
    (proof
      [banc< [φ] [ψ]]
      [(infer !bi>) [→ φ ψ] [→ φ ∧ φ ψ]]
      [bi> [φ] [ψ]]
      [syl [↔ φ ψ] [→ φ ψ] [→ φ ∧ φ ψ]]))

(th "eq.iman-" (pr ("φ" "ψ"))
    (ass [→ ↔ φ ψ → ¬ φ ∧ ¬ φ ¬ ψ])
    (proof
      [eq.iman+ [¬ φ] [¬ ψ]]
      [(+syl> !conb) [φ] [ψ] [→ ¬ φ ∧ ¬ φ ¬ ψ]]))

(th "bi.bldor.idm" (pr ("φ" "ψ"))
    (ass [→ ↔ φ ψ → ∨ φ ψ ψ])
    (proof
      [bi.bldor<t [φ] [ψ] [ψ]]
      [(ded !bi>) [∨ φ ψ] [∨ ψ ψ] [↔ φ ψ]]
      [(bi> !oridm) [ψ]]
      [(infer !ax1) [→ ∨ ψ ψ ψ] [↔ φ ψ]]
      [syld [↔ φ ψ] [∨ φ ψ] [∨ ψ ψ] [ψ]]))
|#
) ; end module "prop-extra"
