;;;  binge-tests.asd --- ASDF system file for BINGE test suite

;;;  Copyright (C) 2004, 2005 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-A6DF2650AAC675D161949D049718BB30.asd,v 1.5 2005/11/01 10:37:24 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-A6DF2650AAC675D161949D049718BB30.asd,v 1.5 2005/11/01 10:37:24 wcp Exp $")

(defpackage :binge-test-system
  (:use :common-lisp :asdf))

(in-package :binge-test-system)

(defsystem binge-tests
    :name "BINGE tests"
    :author "Walter C. Pelissero <walter@pelissero.de>"
    :maintainer "Walter C. Pelissero <walter@pelissero.de>"
    :description "Tests for the Bovine INterface GEnerator (BINGE)"
    :long-description
    "This is a regression test suite for BINGE."
    :licence "GPL"
    :depends-on (:binge)
    :components
    ((:module test
	      :components
	      ((:file "rt")
	       (:file "package" :depends-on ("rt"))
	       (:file "backend" :depends-on ("package"))
	       (:file "parser" :depends-on ("package"))
	       (:file "compiler" :depends-on ("package" "backend"))))))


;; when loading this form the regression-test, package is yet to be
;; loaded so we cannot use rt:do-tests directly or we would get a
;; reader error (unknown package)
(defmethod perform ((o test-op) (c (eql (find-system :binge-tests))))
  (or (funcall (intern "DO-TESTS" "REGRESSION-TEST"))
      (error "test-op failed")))
