;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          umlisp-tests.asd
;;;; Purpose:       ASDF system definitionf for umlisp testing package
;;;; Author:        Kevin M. Rosenberg
;;;; Date Started:  Apr 2003
;;;; *************************************************************************

(defpackage #:umlisp-tests-system
  (:use #:asdf #:cl))
(in-package #:umlisp-tests-system)

(defsystem umlisp-tests
    :depends-on (:rt :umlisp)
    :components
    ((:module tests
	      :serial t
	      :components
	      ((:file "package")
	       (:file "init")
	       (:file "basic")
	       (:file "parse")))))

(defmethod perform ((o test-op) (c (eql (find-system 'umlisp-tests))))
  (or (funcall (intern (symbol-name '#:run-tests)
		       (find-package '#:umlisp-tests)))
      (error "test-op failed")))
