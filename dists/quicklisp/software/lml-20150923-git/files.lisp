;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          files.cl
;;;; Purpose:       File and directory functions for LML
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  Aug 2002
;;;;
;;;; This file, part of LML, is Copyright (c) 2002 by Kevin M. Rosenberg
;;;;
;;;; LML users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License v2
;;;; (http://www.gnu.org/licenses/gpl.html)
;;;; *************************************************************************

(in-package #:lml)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *output-dir* nil)
  (defvar *sources-dir* nil)
  )

(defvar *html-output* *standard-output*)

(defun lml-file-name (f &optional (type :source))
  (when (and (consp f) (eql (car f) 'cl:quote))
    (setq f (cadr f)))
  (when (symbolp f)
    (setq f (string-downcase (symbol-name f))))
  (when (stringp f)
    (unless (position #\. f)
      (setq f (concatenate 'string f ".html"))))
  (if *sources-dir*
      (make-pathname :defaults (ecase type
                                 (:source *sources-dir*)
                                 (:output *output-dir*))
                     :name (pathname-name f)
                     :type (pathname-type f))
      (if (stringp f)
          (nth-value 0 (parse-namestring f))
          f)))

(defmacro with-dir ((output &key sources) &body body)
  (let ((output-dir (gensym))
        (sources-dir (gensym)))
  `(let ((,output-dir ,output)
         (,sources-dir ,sources))
    (when (stringp ,output-dir)
      (setq ,output-dir (parse-namestring ,output-dir)))
    (when (stringp ,sources-dir)
      (setq ,sources-dir (parse-namestring ,sources-dir)))
    (unless ,sources-dir
      (setq ,sources-dir ,output-dir))
    (let ((*output-dir* ,output-dir)
          (*sources-dir* ,sources-dir))
      ,@body))))

(defun lml-load-path (file)
  (if (probe-file file)
      (with-open-file (in file :direction :input)
        (do ((form (read in nil 'eof) (read in nil 'eof)))
            ((eq form 'eof))
          (eval form)))
    (format *trace-output* "Warning: unable to load LML file ~S" file)))

(defun process-dir (dir &key sources)
  (with-dir (dir :sources sources)
    (let ((lml-files (directory
                      (make-pathname :defaults *sources-dir*
                                     :name :wild
                                     :type "lml"))))
      (dolist (file lml-files)
        (format *trace-output* "~&; Processing ~A~%" file)
        (lml-load-path file)))))

(defun lml-load (file)
  (lml-load-path (eval `(lml-file-name ,file :source))))

(defun include-file (file)
  (print-file-contents file *html-output*))
