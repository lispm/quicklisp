;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          pipes.asd
;;;; Purpose:       ASDF system definition for PIPES package
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  Apr 2000
;;;;
;;;; $Id$
;;;; *************************************************************************

(defpackage #:pipes-system (:use #:cl #:asdf))
(in-package :pipes-system)

(defsystem pipes
    :components 
    ((:file "package")
     (:file "src" :depends-on ("package"))))


