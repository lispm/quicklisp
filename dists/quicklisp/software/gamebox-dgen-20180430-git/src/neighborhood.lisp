(in-package :box.dgen)

(defstruct extent
  (minimum 0)
  (maximum 1))

(defstruct (nh (:constructor %make-nh))
  stage
  x
  y
  (extent (make-extent))
  (set-fn #'nset-default)
  (map-fn #'nmap-default))

(defun stage-coords (nh x y)
  (values (+ (nh-x nh) x) (+ (nh-y nh) y)))

(defun nmap (nh func &rest args)
  (apply (nh-map-fn nh) nh func args))

(defun nsetp (nh x y)
  (funcall (nh-set-fn nh) nh x y))

(defun nfilter (nh filter)
  (remove nil (nmap nh (lambda (x) (when (funcall filter x) x)))))

(defun nref (nh x y)
  (when (nsetp nh x y)
    (multiple-value-bind (sx sy) (stage-coords nh x y)
      (valid-cell-p (nh-stage nh) sx sy))))

(defun origin (nh)
  (nref nh 0 0))

(defun n (nh &optional (distance 1))
  (nref nh 0 distance))

(defun nw (nh &optional (distance 1))
  (nref nh (- distance) distance))

(defun w (nh &optional (distance 1))
  (nref nh (- distance) 0))

(defun sw (nh &optional (distance 1))
  (nref nh (- distance) (- distance)))

(defun s (nh &optional (distance 1))
  (nref nh 0 (- distance)))

(defun se (nh &optional (distance 1))
  (nref nh distance (- distance)))

(defun e (nh &optional (distance 1))
  (nref nh distance 0))

(defun ne (nh &optional (distance 1))
  (nref nh distance distance))

(defun make-nh (set-fn map-fn extent-args)
  (lambda (stage x y)
    (%make-nh :stage stage
              :x x
              :y y
              :extent (apply #'make-extent extent-args)
              :set-fn set-fn
              :map-fn map-fn)))

(defun cell-nh (stage cell layout)
  (funcall layout stage (x cell) (y cell)))

(defgeneric layout (name &rest extent-args))

(defmethod layout ((name (eql :horizontal)) &rest extent-args)
  (make-nh #'nset-horizontal #'nmap-horizontal extent-args))

(defmethod layout ((name (eql :vertical)) &rest extent-args)
  (make-nh #'nset-vertical #'nmap-vertical extent-args))

(defmethod layout ((name (eql :orthogonal)) &rest extent-args)
  (make-nh #'nset-orthogonal #'nmap-orthogonal extent-args))

(defmethod layout ((name (eql :rect)) &rest extent-args)
  (make-nh #'nset-rect #'nmap-rect extent-args))

(defun nh-realize (nh-generator stage x y)
  (funcall nh-generator stage x y))

(defun nset-horizontal (nh x y)
  (and (zerop y)
       (<= (abs x) (extent-maximum (nh-extent nh)))
       (not (zerop x))))

(defun nmap-horizontal (nh func)
  (let ((results)
        (max (extent-maximum (nh-extent nh))))
    (loop :for x :from (- max) :below 0
          :for cell = (nref nh x 0)
          :when cell
            :do (push (funcall func cell) results))
    (loop :for x :from 1 :to max
          :for cell = (nref nh x 0)
          :when cell
            :do (push (funcall func cell) results))
    results))

(defun nset-vertical (nh x y)
  (and (zerop x)
       (<= (abs y) (extent-maximum (nh-extent nh)))
       (not (zerop y))))

(defun nmap-vertical (nh func)
  (let ((results)
        (max (extent-maximum (nh-extent nh))))
    (loop :for y :from (- max) :below 0
          :for cell = (nref nh 0 y)
          :when cell
            :do (push (funcall func cell) results))
    (loop :for y :from 1 :to max
          :for cell = (nref nh 0 y)
          :when cell
            :do (push (funcall func cell) results))
    results))

(defun nset-orthogonal (nh x y)
  (let ((max (extent-maximum (nh-extent nh))))
    (and (<= (abs x) max)
         (<= (abs y) max)
         (or (and (zerop x) (zerop y))
             (and (zerop x) (not (zerop y)))
             (and (not (zerop x)) (zerop y))))))

(defun nmap-orthogonal (nh func)
  (let ((results)
        (max (extent-maximum (nh-extent nh))))
    (loop :for y :from (- max) :to max
          :for cell = (nref nh 0 y)
          :when cell
            :do (push (funcall func cell) results))
    (loop :for x :from (- max) :below 0
          :for cell = (nref nh x 0)
          :when cell
            :do (push (funcall func cell) results))
    (loop :for x :from 1 :to max
          :for cell = (nref nh x 0)
          :when cell
            :do (push (funcall func cell) results))
    results))

(defun nset-rect (nh x y)
  (nset-default nh x y))

(defun nmap-rect (nh func)
  (nmap-default nh func))

(defun nset-default (nh x y)
  (let ((max (extent-maximum (nh-extent nh))))
    (and (>= x (- max))
         (>= y (- max))
         (<= x max)
         (<= y max))))

(defun nmap-default (nh func)
  (let ((results)
        (max (extent-maximum (nh-extent nh))))
    (loop :for y :from max :downto (- max)
          :do (loop :for x :from (- max) :to max
                    :for cell = (nref nh x y)
                    :when cell
                      :do (push (funcall func cell) results)))
    results))

(defmacro nmap-short (nh func reduce &key (test 'when) (return-val nil))
  (alexandria:with-gensyms (block-name cell return-func)
    `(block ,block-name
       (let ((,return-func ,return-val))
         (,reduce
          (nmap ,nh
                (lambda (,cell)
                  (,test (funcall ,func ,cell)
                         (return-from ,block-name
                           (cond
                             ((eq ,return-func nil) nil)
                             ((eq ,return-func t) t)
                             ((functionp ,return-func)
                              (funcall ,return-func ,cell))))))))))))

(defun convolve (stage layout filter effect &key (x1 1) (x2 -1) (y1 1) (y2 -1))
  (loop :with affectedp
        :for x :from x1 :below (+ (width stage) x2)
        :do (loop :for y :from y1 :below (+ (height stage) y2)
                  :for nh = (nh-realize layout stage x y)
                  :when (funcall filter stage nh)
                    :do (let ((value (funcall effect stage nh)))
                          (setf affectedp (or affectedp value))))
        :finally (return affectedp)))

(defun convolve-all (stage effect)
  (convolve stage (layout :rect) (constantly t) effect :x1 0 :x2 0 :y1 0 :y2 0))

(defun collect (stage layout filter &rest args &key (x1 1) (x2 -1) (y1 1) (y2 -1))
  (declare (ignore x1 x2 y1 y2))
  (let ((items))
    (apply
     #'convolve
     stage
     layout
     filter
     (lambda (s n)
       (declare (ignore s))
       (push n items))
     args)
    items))

(defun process (stage layout filter processor &key (items nil itemsp) (nh-generator #'identity))
  (loop :with items = (if itemsp items (collect stage layout filter))
        :while items
        :for nh = (funcall nh-generator (pop items))
        :when (funcall filter stage nh)
          :do (alexandria:when-let ((new (funcall processor stage nh)))
                (push new items))))
