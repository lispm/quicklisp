(defpackage #:delta-debug
  (:use :common-lisp
        :alexandria
        :named-readtables
        :curry-compose-reader-macros)
  (:export :minimize))
