;;;  record.lisp --- definition of a cobol record

;;;  Copyright (C) 2005, 2006, 2008, 2009 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: cobstor

#+cmu (ext:file-comment "$Module: record.lisp $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA


(in-package :cobstor)


;; Considering that we compile using IBM numeric memory layout (option
;; -Dci of ccbl), the COMP-4 looks good."
(defvar *comp-alias* 'comp4
  "Class to use to represent comp fields.  The Cobol manual says:
In VAX/COBOL compatibility mode, a COMP data item is the same as
COMP-4 and is treated as binary data. In RM/COBOL compatibility
lmode, COMP is the same as COMP-2. You can use compile-time
options to change the default behavior.")

(defvar *packed-decimal-alias* 'comp3
  "Class to use to represent packed decimal fields.  The Cobol
manual says: Data items described as PACKED-DECIMAL are identical
to COMP-3. You can cause unsigned PACKED-DECIMAL to be treated as
COMP-6 by using a compile-time option.")

(defvar *binary-alias* 'comp4
  "Class to use to represent binary fields.  The Cobol manual
says: By default, a BINARY data item is identical to a COMP-4
data item. The compile-time option \"-D5\" treats BINARY data items
as COMP-5 items instead.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro defsizer (type args &body forms)
  (be name (intern (s+ "SIZE-OF-CBL-" (string type)))
    `(eval-when (:compile-toplevel :load-toplevel)
       (defun ,name ,args ,@forms)
       (setf (get ',type 'sizer) ',name))))

(defun field-type->sizer (type)
  ;; for backward compatibility we accept keywords as well
  (when (keywordp type)
    (setf type (intern (string type) #.*package*)))
  (get type 'sizer))

(defsizer string (&key length)
  (assert length)
  length)

(defsizer binary-area (&key length)
  (assert length)
  length)

(defsizer comp (&rest args)
  (apply (field-type->sizer *comp-alias*) args))

(defsizer packed (&rest args)
  (apply (field-type->sizer *packed-decimal-alias*) args))

(defsizer binary (&rest args)
  (apply (field-type->sizer *binary-alias*) args))

(defsizer numeric (&key length signed decimals sign-separated sign-position)
  (declare (ignore decimals sign-position))
  (+ length
     (if (and signed sign-separated) 1 0)))

(defsizer comp1 ()
  2)

(defsizer comp2 (&key length signed decimals)
  (declare (ignore decimals))
  (+ length
     (if signed 1 0)))

(defsizer comp3 (&key length signed decimals)
  (declare (ignore signed decimals))
  (ceiling (1+ length) 2))

(defsizer comp4 (&key length signed decimals)
  (declare (ignore decimals))
  (size-of-comp4/5 length signed))

(defsizer comp5 (&key length signed decimals endianness)
  (declare (ignore decimals endianness))
  (size-of-comp4/5 length signed))

(defsizer comp6 (&key length decimals)
  (declare (ignore decimals))
  (ceiling length 2))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro with-indexes ((offset cardinalities) &body forms)
  (with-gensyms (sizes maxs)
    `(let (,sizes ,maxs indexes)
       (loop
	  for (size . max) in ,cardinalities
	  for i from 1
	  do
	    (push (intern (format nil "I~D" i)) indexes)
	    (push size ,sizes)
	    (push max ,maxs))
       (setf ,sizes (nreverse ,sizes)
	     ,maxs (nreverse ,maxs)
	     indexes (nreverse indexes))
       (be declarations (mapcar #'(lambda (i)
				    `(declare (type fixnum ,i)))
				indexes)
	   assertions (mapcar #'(lambda (i m)
				  `(assert (<= 0 ,i ,(1- m))))
			      indexes ,maxs)
	   offset-form (if indexes
			   `(+ ,,offset
			       ,@(mapcar #'(lambda (i s)
					     `(* ,i ,s))
					 indexes ,sizes))
			   ,offset)
	   ,@forms))))

(eval-when (:compile-toplevel :load-toplevel)
  (defvar *field-accessors* :method
    "Type of accessor that is created for record fields.  It can be
either :METHOD, to create CLOS methods, or :FUNCTION, to create normal
Lisp functions.  If you don't expect name clashes :FUNCTION is the
best choice, as it avoids the CLOS overhead.")
  (defvar *setter-prefix* 'set-
    "Prefix to be prepended to function names of field setters.  This
should be a symbol, possibly ending in hyphen."))

(defmacro defaccmkr (type args &body forms)
  (be name (intern (s+ "MAKE-CBL-" (string type) "-ACCESSOR"))
      decls (loop
	       for f in forms
	       while (and (consp f) (eq (car f) 'declare))
	       collect (pop forms))
    `(progn
       (defun ,name (slot-name class offset size cardinalities ,@args)
	 ,@decls
	 (with-indexes (offset cardinalities)
	   (flet ((reader (&rest body)
		    (let ((function-skeleton `(let ((offset ,offset-form)
						    (storage (record-storage object)))
						,@body)))
		      (ecase *field-accessors*
			(:method
			    `(defmethod ,slot-name ((object ,class) ,@indexes)
			       ,@(when indexes `((declare (type fixnum ,@indexes))))
			       ,@declarations
			       ,@assertions
			       ,function-skeleton))
			(:function
			 `(defun ,slot-name (object ,@indexes)
			    (declare (type ,class object))
			    ,@(when indexes `((declare (type fixnum ,@indexes))))
			    ,@declarations
			    ,@assertions
			    ,function-skeleton)))))
		  (storage ()
		    (let ((name (intern (s+ (string slot-name) "-STORAGE")))
			  (function-skeleton `(let ((offset ,offset-form))
						(make-array ,size
							    :element-type 'ubyte
							    :displaced-to (record-storage object)
							    :displaced-index-offset offset))))
		      (ecase *field-accessors*
			(:method
			    `(defmethod ,name ((object ,class) ,@indexes)
			       ,@(when indexes `((declare (type fixnum ,@indexes))))
			       ,@declarations
			       ,@assertions
			       ,function-skeleton))
			(:function
			 `(defun ,name (object ,@indexes)
			    (declare (type ,class object))
			    ,@(when indexes `((declare (type fixnum ,@indexes))))
			    ,@declarations
			    ,@assertions
			    ,function-skeleton)))))
		  (setters (&rest body)
		    (let ((setter-name (intern (s+ (string *setter-prefix*) (string slot-name))))
			  (function-skeleton `(let ((offset ,offset-form)
						    (storage (record-storage object)))
						,@body)))
		      (ecase *field-accessors*
			(:method
			   `((defmethod ,setter-name ((object ,class) ,@indexes value)
			       ,@(when indexes `((declare (type fixnum ,@indexes))))
			       ,@declarations
			       ,@assertions
			       ,function-skeleton)
			     (defmethod (setf ,slot-name) (value (object ,class) ,@indexes)
			       ,@(when indexes `((declare (type fixnum ,@indexes))))
			       ,@declarations
			       ,@assertions
			       (,setter-name object ,@indexes value))))
			(:function
			 `((defun ,setter-name (object ,@indexes value)
			      (declare (type ,class object))
			      ,@(when indexes `((declare (type fixnum ,@indexes))))
			      ,@declarations
			      ,@assertions
			      ,function-skeleton)
			    (defun (setf ,slot-name) (value object ,@indexes)
			      (declare (type ,class object))
			      ,@(when indexes `((declare (type fixnum ,@indexes))))
			      ,@declarations
			      ,@assertions
			      (,setter-name object ,@indexes value))))))))
	     (declare (ignorable (function storage)))
	     ,@forms)))
       (setf (get ',type 'accessor-maker) ',name))))

(defun field-type->accessor-maker (type)
  ;; for backward compatibility we accept keywords as well
  (when (keywordp type)
    (setf type (intern (string type) #.*package*)))
  (get type 'accessor-maker))

(defaccmkr string (&key length)
  (assert length)
  (list* (reader `(get-string-field-value storage offset ,length))
	 (storage)
	 (setters `(set-string-field-value value storage offset ,length))))

(defaccmkr binary-area (&key length)
  (assert length)
  (list* (reader `(get-area-field-value storage offset ,length))
	 (storage)
	 (setters `(set-area-field-value value storage offset ,length))))

(defaccmkr comp (&rest args)
  (apply (field-type->accessor-maker *comp-alias*) slot-name class offset size cardinalities args))

(defaccmkr packed (&rest args)
  (apply (field-type->accessor-maker *packed-decimal-alias*) slot-name class offset size cardinalities args))

(defaccmkr binary (&rest args)
  (apply (field-type->accessor-maker *binary-alias*) slot-name class offset size cardinalities args))

(defaccmkr numeric (&key length signed (decimals 0) sign-separated (sign-position *default-sign-position*))
  (declare (ignore length))
  (list* (reader `(get-numeric-field-value storage offset ,size ,signed ,sign-separated ,sign-position ,decimals))
	 (storage)
	 (setters `(set-numeric-field-value value storage offset ,size ,signed ,sign-separated ,sign-position ,decimals))))

(defaccmkr comp1 ()
  (list* (reader `(get-comp1-field-value storage offset))
	 (storage)
	 (setters `(set-numeric-field-value value storage offset))))

(defaccmkr comp2 (&key length signed (decimals 0))
  (declare (ignore length))
  (list* (reader `(get-comp2-field-value storage offset ,size ,signed ,decimals))
	 (storage)
	 (setters `(set-comp2-field-value value storage offset ,size ,signed ,decimals))))

(defaccmkr comp3 (&key length signed (decimals 0))
  (declare (ignore length))
  (list* (reader `(get-comp3-field-value storage offset ,size ,signed ,decimals))
	 (storage)
	 (setters `(set-comp3-field-value value storage offset ,size ,signed ,decimals))))

(defaccmkr comp4 (&key length signed (decimals 0))
  (declare (ignore length signed))  ; don't care for signed? -wcp8/1/09.
  (list* (reader `(get-field-value-comp4/5-be storage offset ,size ,decimals))
	 (storage)
	 (setters `(set-field-value-comp4/5-be value storage offset ,size ,decimals))))

(defaccmkr comp5 (&key length signed (decimals 0) (endianness *default-endianness*))
  (declare (ignore length signed))  ; don't care for signed? -wcp8/1/09.
  (ecase endianness
    (:big-endian
     (list* (reader `(get-field-value-comp4/5-be storage offset ,size ,decimals))
	    (storage)
	    (setters `(set-field-value-comp4/5-be value storage offset ,size ,decimals))))
    (:little-endian
     (list* (reader `(get-field-value-comp4/5-le storage offset ,size ,decimals))
	    (storage)
	    (setters `(set-field-value-comp4/5-le value storage offset ,size ,decimals))))))

(defaccmkr comp6 (&key length (decimals 0))
  (assert length)
  (list* (reader `(get-comp6-field-value storage offset ,size ,decimals))
	 (storage)
	 (setters `(set-comp6-field-value value storage offset ,size ,decimals))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass cbl-record ()
  ((size :reader record-size
	 :type fixnum)
   (storage :initarg :storage
	    :reader record-storage
	    :type field-storage))
  (:documentation
   "Base class for cobol records.  Subclasses must specify an initform
for the SIZE slot so that the default INITIALIZE-INSTANCE method will
instantiate the storage automatically."))

(defmethod initialize-instance ((record cbl-record) &key &allow-other-keys)
  (call-next-method)
  (with-slots (size storage) record
    (if (slot-boundp record 'storage)
	(assert (= size (length storage)))
	(setf storage (make-sequence 'field-storage size)))))

(defgeneric write-record (record stream)
  (:documentation
   "Write the storage of RECORD to STREAM."))

(defmethod write-record ((record cbl-record) stream)
  (write-sequence (record-storage record) stream))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun cbl-map-fields (proc fields &key leaves-only-p named-only-p)
  "Apply PROC to FIELDS.  If LEAVES-ONLY-P is true, don't apply PROC
to fields that contain subfields."
  (labels ((iterate-fields (fields cardinality)
	     (dolist (f fields)
	       (destructuring-bind (name type &rest props &key alternates times &allow-other-keys) f
		 (when times
		   (push times cardinality))
		 (unless (or (and leaves-only-p
				  (consp type))
			     (and named-only-p
				  (not name)))
		   (apply proc name type cardinality props))
		 ;; if it's got alternates do them
		 (when alternates
		   (iterate-fields alternates cardinality))
		 ;; if it's got subfields do them
		 (when (consp type)
		   (iterate-fields type cardinality))))))
    (iterate-fields fields '())))

(defun collect-names (fields)
  (be names '()
    (cbl-map-fields #'(lambda (name type cardinality &rest props)
			(declare (ignore type props cardinality))
			(push name names))
		    fields :named-only-p t)
    (nreverse names)))

(defun add-field-sizes (fields)
  "Add the :SIZE property to FIELDS, which is a list of fields as
expected by DEFINE-COBOL-RECORD.  Return a modified list of fields."
  (if fields
      (be field (car fields)
	(destructuring-bind (name type &rest args &key alternates times &allow-other-keys) field
	  (declare (ignore name))
	  (mapc #'(lambda (f)
		    (add-field-sizes (list f)))
		alternates)
	  (be size (if (consp type)
		       ;; type is actually a list of subfields
		       (add-field-sizes type)
		       (be args (copy-list args)
			 (remf args :alternates)
			 (remf args :times)
			 (apply (field-type->sizer type) args)))
	    (setf (cddr field)
		  (append (list :size size)
			  (cddr field)))
	    (+ (* size (or times 1))
	       (add-field-sizes (cdr fields))))))
      0))

(defmacro define-cobol-record ((name &key alternates documentation) &rest declarations)
  (be accessors '()
    (labels ((output-accessors (fields offset cardinality)
	       (when fields
		 (destructuring-bind (field-name type &rest args &key alternates times size &allow-other-keys) (car fields)
		   (dolist (f alternates)
		     (output-accessors (list f) offset cardinality))
		   (be args (copy-list args)
		     (remf args :alternates)
		     (remf args :times)
		     (remf args :size)
		     (output-accessors (cdr fields) (+ offset (* size (or times 1))) cardinality)
		     (when times
		       (push (cons size times) cardinality))
		     (when (consp type)
		       ;; type is actually a list of subfields
		       (output-accessors type offset cardinality)
		       (setf type 'binary-area
			     (getf args :length) size))
		     (setf accessors
			   (append (when field-name
				     (apply (field-type->accessor-maker type)
					    field-name name offset size cardinality args))
				   accessors)))))))
      (be total-size (if alternates
			 (reduce #'max (mapcar #'(lambda (dec)
						   (add-field-sizes (list dec)))
					       declarations))
			 (add-field-sizes declarations))
	`(progn
	   (defclass ,name (cbl-record)
	     ((size :initform ,total-size))
	     ,@(when documentation '((:documentation documentation))))
	   ,@(if alternates
		 (loop
		    for dec in declarations
		    do (setf accessors '())
		      (output-accessors (list dec) 0 '())
		    append accessors)
		 (progn
		   (output-accessors declarations 0 '())
		   accessors))
	   (setf (get ',name 'record-fields) ',declarations
		 (get ',name 'record-length) ,total-size))))))

(defun cbl-record-fields (record)
  "Return the tree of fields of Cobol record RECORD, which is a
symbol."
  (get record 'record-fields))

(defun cbl-record-field-names (record)
  "Return the list of field names of Cobol record RECORD, which is a
symbol.  Return a list of symbols."
  (collect-names (cbl-record-fields record)))

(defmethod record-size ((record symbol))
  "Return the size of RECORD, which is a symbol."
  (get record 'record-length))

(defmethod record-size ((record string))
  "Return the size of RECORD, which is a string."
  (record-size (intern record #.*package*)))

(defun copy-record (record)
  (be new (make-instance (class-of record))
    (replace (record-storage new) (record-storage record))
    new))

(defmacro with-record-accessors (accessors record &body forms)
  "Execute FORMS while locally creating macrolets to access RECORD's
ACCESSORS."
  (with-gensyms (rec)
    `(be ,rec ,record
       (symbol-macrolet ,(mapcar #'(lambda (accessor)
				     (list accessor
					   (list accessor rec)))
				 accessors)
	 ,@forms))))

(defmacro with-record (record class &body forms)
  "Locally create macrolets to access all RECORD's fields.  RECORD is
assumed to be of type CLASS."
  `(with-record-accessors ,(cbl-record-field-names class) ,record
     ,@forms))

(defmacro with-records (records &body forms)
  "Locally create macrolets to access all fields in RECORDS.  RECORDS
is an alist of variable names and record classes.  Each variable is
assumed to be of the respective type and macrolet accessors are
generated.  See also WITH-RECORD-ACCESSORS."
  (labels ((decl (records)
	     (if records
		 (destructuring-bind (record class) (car records)
		   `((with-record-accessors ,(cbl-record-field-names class) ,record
		       ,@(decl (cdr records)))))
		 forms)))
    (car (decl records))))
