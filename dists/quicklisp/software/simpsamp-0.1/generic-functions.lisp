(in-package #:simpsamp)

(defgeneric sample (n set)
  (:documentation "Returns a simple random sample of size N, without
replacement, from SET.  The output will be of type SEQUENCE.  (Whether
a LIST is returned or a VECTOR depends on which is most efficient for
the type of SET.)

If N is larger than the size of SET, then the resulting sample will
just contain all the elements of SET--that is, N is automatically
lowered to the size of SET when necessary.

When SET is a mapping type (e.g. a HASH-TABLE), the returned elements
will be CONSes of the form (KEY . VALUE).

The order of the returned elements is not assured to be random.  The
only thing that is random is which selection is made of all the
possible subsets of SET of size N."))

(defgeneric list-sample (n set)
  (:documentation "Like SAMPLE, but the result is a LIST."))

(defgeneric vector-sample (n set)
  (:documentation "Like SAMPLE, but the result is a VECTOR."))

(defgeneric map-sample (function n set)
  (:documentation "Like SAMPLE, but applies FUNCTION to each element
of the resulting selection.

When SET is a mapping type (e.g. a HASH-TABLE), FUNCTION is applied
with two arguments: the key and the value."))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defmacro define-typical-sample-methods (specialized-for-class-name
					   generic-sample-function-name
					   list-sample-function-name
					   vector-sample-function-name
					   map-sample-function-name)
    `(progn
       (defmethod sample ((n integer) (set ,specialized-for-class-name))
	 (,generic-sample-function-name n set))
       (defmethod list-sample ((n integer) (set ,specialized-for-class-name))
	 (,list-sample-function-name n set))
       (defmethod vector-sample ((n integer) (set ,specialized-for-class-name))
	 (,vector-sample-function-name n set))
       (defmethod map-sample (function (n integer) (set ,specialized-for-class-name))
	 (,map-sample-function-name function n set)))))

(define-typical-sample-methods list
                               vector-sample-of-list
                               list-sample-of-list
			       vector-sample-of-list
			       map-sample-of-list)
(define-typical-sample-methods vector
			       vector-sample-of-vector
			       list-sample-of-vector
			       vector-sample-of-vector
			       map-sample-of-vector)
(define-typical-sample-methods hash-table
			       vector-sample-of-hash-table-pairs
			       list-sample-of-hash-table-pairs
			       vector-sample-of-hash-table-pairs
			       map-sample-of-hash-table)
