;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: umlisp -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:     classes.lisp
;;;; Purpose:  Class defintions for UMLisp
;;;; Author:   Kevin M. Rosenberg
;;;; Created:  Apr 2000
;;;;
;;;; $Id$
;;;;
;;;; This file, part of UMLisp, is
;;;;    Copyright (c) 2000-2004 by Kevin M. Rosenberg, M.D.
;;;;
;;;; UMLisp users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License.
;;;; *************************************************************************

(in-package #:umlisp-orf)

(defclass umlsclass (hyperobject)
  ()
  (:metaclass hyperobject-class)
  (:description "Parent class of all UMLS objects. It is based on the HYPEROBJECT-CLASS metaclass that provides object printing functions."))


(defclass usrl (umlsclass)
  ((sab :value-type string :initarg :sab :reader sab)
   (srl :value-type fixnum :initarg :srl :reader srl))
  (:metaclass hyperobject-class)
  (:user-name "Source Restriction Level")
  (:default-print-slots sab srl)
  (:description "Custom Table: Source Restriction Level"))


(defclass urank (umlsclass)
  ((rank :value-type fixnum :initarg :rank :reader rank)
   (sab :value-type string :initarg :sab :reader sab)
   (tty :value-type string :initarg :tty :reader tty)
   (supres :value-type string :initarg :supres :reader supres))
  (:metaclass hyperobject-class)
  (:user-name "Rank")
  (:default-print-slots rank sab tty supres))

(defclass udef (umlsclass)
  ((def :value-type cdata :initarg :def :reader def)
   (sab :value-type string :initarg :sab :reader sab :hyperlink find-usab-rsab))
  (:metaclass hyperobject-class)
  (:user-name "Definition")
  (:default-print-slots sab def))

(defclass usat (umlsclass)
  ((sab :value-type string :initarg :sab :reader sab :hyperlink find-usab-rsab)
   (code :value-type string :initarg :code :reader code)
   (atn :value-type string :initarg :atn :reader atn)
   (atv :value-type cdata :initarg :atv :reader atv))
  (:metaclass hyperobject-class)
  (:user-name "Simple Attribute")
  (:default-print-slots sab code atn atv))

(defclass usab (umlsclass)
  ((vcui :value-type fixnum :initarg :vcui :reader vcui :print-formatter fmt-cui)
   (rcui :value-type fixnum :initarg :rcui :reader rcui :print-formatter fmt-cui)
   (vsab :value-type string :initarg :vsab :reader vsab)
   (rsab :value-type string :initarg :rsab :reader rsab :hyperlink find-ustr-sab
         :hyperlink-parameters (("subobjects" . "no")))
   (son :value-type string :initarg :son :reader son)
   (sf :value-type string :initarg :sf :reader sf)
   (sver :value-type string :initarg :sver :reader sver)
   (vstart :value-type string :initarg :vstart :reader vstart)
   (vend :value-type string :initarg :vend :reader vend)
   (imeta :value-type string :initarg :imeta :reader imeta)
   (rmeta :value-type string :initarg :rmeta :reader rmeta)
   (slc :value-type cdata :initarg :slc :reader slc)
   (scc :value-type cdata :initarg :scc :reader scc)
   (srl :value-type fixnum :initarg :srl :reader srl)
   (tfr :value-type fixnum :initarg :tfr :reader tfr :print-formatter fmt-comma-integer)
   (cfr :value-type fixnum :initarg :cfr :reader cfr :print-formatter fmt-comma-integer)
   (cxty :value-type string :initarg :cxty :reader cxty)
   (ttyl :value-type string :initarg :ttyl :reader ttyl)
   (atnl :value-type string :initarg :atnl :reader atnl)
   (lat :value-type string :initarg :lat :reader lat)
   (cenc :value-type string :initarg :cenc :reader cenc)
   (curver :value-type string :initarg :curver :reader curver)
   (sabin :value-type string :initarg :sabin :reader sabin))
  (:metaclass hyperobject-class)
  (:user-name "Source Abbreviation")
  (:default-print-slots vcui rcui vsab rsab son sf sver vstart vend imeta
                rmeta slc scc srl tfr cfr cxty ttyl atnl lat cenc
                curver sabin))

(defclass uso (umlsclass)
  ((sab :value-type string :initarg :sab :reader sab :hyperlink find-usab-rsab)
   (code :value-type string :initarg :code :reader code)
   (tty :value-type string :initarg :tty :reader tty :hyperlink find-btty-tty)
   (srl :value-type fixnum :initarg :srl :reader srl))
  (:metaclass hyperobject-class)
  (:user-name "Source")
  (:default-print-slots sab code tty srl))

(defclass ucxt (umlsclass)
  ((sab :value-type string :initarg :sab :reader sab :hyperlink find-usab-rsab)
   (code :value-type string :initarg :code :reader code)
   (rnk :value-type fixnum :initarg :rnk :reader rnk)
   (cxn :value-type fixnum :initarg :cxn :reader cxn)
   (cxl :value-type string :initarg :cxl :reader cxl)
   (cxs :value-type cdata :initarg :cxs :reader cxs)
   (cui2 :value-type fixnum :initarg :cui2 :reader cui2 :hyperlink find-ucon-cui
         :print-formatter fmt-cui)
   (hcd :value-type string :initarg :hcd :reader hcd)
   (rela :value-type string :initarg :rela :reader rela)
   (xc :value-type string  :initarg :xc :reader xc))
  (:metaclass hyperobject-class)
  (:user-name "Context")
  (:default-print-slots sab code rnk cxn cxl hcd rela xc cui2 cxs))

(defclass ustr (umlsclass)
  ((sui :value-type fixnum :initarg :sui :reader sui :print-formatter fmt-sui
        :hyperlink find-ustr-sui)
   (cui :value-type fixnum :initarg :cui :reader cui :print-formatter fmt-cui
        :hyperlink find-ucon-cui)
   (lui :value-type fixnum :initarg :lui :reader lui :print-formatter fmt-lui
        :hyperlink find-uterm-lui)
   (cuisui :value-type integer :initarg :cuisui :reader cuisui )
   (str :value-type cdata :initarg :str :reader str)
   (lrl :value-type fixnum :initarg :lrl :reader lrl)
   (stt :value-type string :initarg :stt :reader stt)
   (s#so :reader s#so :subobject (find-uso-cuisui cui sui))
   (s#sat :reader s#sat :subobject (find-usat-ui cui lui sui))
   (s#cxt :reader s#cxt :subobject (find-ucxt-cuisui cui sui)))
  (:metaclass hyperobject-class)
  (:user-name "String")
  (:default-print-slots sui stt lrl str))

(defclass ulo (umlsclass)
  ((isn :value-type string :initarg :isn :reader isn)
   (fr :value-type fixnum :initarg :fr :reader fr)
   (un :value-type string :initarg :un :reader un)
   (sui :value-type fixnum :initarg :sui :reader sui :print-formatter fmt-sui)
   (sna :value-type string :initarg :sna :reader sna)
   (soui :value-type string :initarg :soui :reader soui))
  (:metaclass hyperobject-class)
  (:user-name "Locator")
  (:default-print-slots isn fr un sna soui sui))

(defclass uterm (umlsclass)
  ((lui :value-type fixnum :initarg :lui :reader lui :print-formatter fmt-lui
        :hyperlink find-uterm-lui)
   (cui :value-type fixnum :initarg :cui :reader cui :print-formatter fmt-cui
        :hyperlink find-ucon-cui)
   (lat :value-type string :initarg :lat :reader lat)
   (ts :value-type string  :initarg :ts :reader ts)
   (lrl :value-type fixnum :initarg :lrl :reader lrl)
   (s#str :reader s#str :subobject (find-ustr-cuilui cui lui))
   (s#sat :reader s#sat :subobject (find-usat-ui cui lui)))
  (:metaclass hyperobject-class)
  (:user-name "Term")
  (:default-print-slots lui lat ts lrl))

(defclass usty (umlsclass)
  ((tui :value-type fixnum :initarg :tui :reader tui :print-formatter fmt-tui
        :hyperlink find-ucon-tui
        :hyperlink-parameters (("subobjects" . "no")))
   (sty :value-type string :initarg :sty :reader sty))
  (:metaclass hyperobject-class)
  (:user-name "Semantic Type")
  (:default-print-slots tui sty))

(defclass urel (umlsclass)
  ((rel :value-type string :initarg :rel :reader rel :hyperlink find-brel-rel)
   (cui1 :value-type fixnum :initarg :cui1 :reader cui1 :print-formatter fmt-cui)
   (cui2 :value-type fixnum :initarg :cui2 :reader cui2 :hyperlink find-ucon-sui
         :print-formatter fmt-cui)
   (pfstr2 :value-type cdata :initarg :pfstr2 :reader pfstr2)
   (rela :value-type string :initarg :rela :reader rela)
   (sab :value-type string :initarg :sab :reader sab :hyperlink find-usab-rsab)
   (sl :value-type string  :initarg :sl :reader sl)
   (mg :value-type string  :initarg :mg :reader mg))
  (:metaclass hyperobject-class)
  (:user-name "Relationship")
  (:default-print-slots rel rela sab sl mg cui2 pfstr2))

(defclass ucoc (umlsclass)
  ((cui1 :value-type fixnum :initarg :cui1 :reader cui1 :print-formatter fmt-cui)
   (cui2 :value-type fixnum :initarg :cui2 :reader cui2 :print-formatter fmt-cui
         :hyperlink find-ucon-cui)
   (pfstr2 :value-type cdata :initarg :pfstr2 :reader pfstr2)
   (soc :value-type string :initarg :soc :reader soc)
   (cot :value-type string :initarg :cot :reader cot)
   (cof :value-type fixnum :initarg :cof :reader cof)
   (coa :value-type cdata :initarg :coa :reader coa))
  (:metaclass hyperobject-class)
  (:user-name "Co-occuring Concept")
  (:default-print-slots soc cot cof coa cui2 pfstr2))


(defclass uatx (umlsclass)
  ((sab :value-type string :initarg :sab :reader sab)
   (rel :value-type string :initarg :rel :reader rel)
   (atx :value-type cdata :initarg :atx :reader atx))
  (:metaclass hyperobject-class)
  (:user-name "Associated Expression")
  (:default-print-slots sab rel atx))

(defclass ucon (umlsclass)
  ((cui :value-type fixnum :initarg :cui :reader cui :print-formatter fmt-cui
        :hyperlink find-ucon-cui)
   (lrl :value-type fixnum :initarg :lrl :reader lrl)
   (pfstr :value-type cdata :initarg :pfstr :reader pfstr)
   (s#def :reader s#def :subobject (find-udef-cui cui))
   (s#sty :reader s#sty :subobject (find-usty-cui cui))
   (s#atx :reader s#atx :subobject (find-uatx-cui cui))
   (s#lo :reader s#lo :subobject (find-ulo-cui cui))
   (s#term :reader s#term :subobject (find-uterm-cui cui))
   (s#sat :reader s#sat :subobject (find-usat-ui cui))
   (s#rel :reader s#rel :subobject (find-urel-cui cui))
   (s#coc :reader s#coc :subobject (find-ucoc-cui cui)))
  (:metaclass hyperobject-class)
  (:user-name "Concept")
  (:default-print-slots cui lrl pfstr))

(defclass uxw (umlsclass)
  ((wd :value-type string :initarg :wd :reader wd)
   (cui :value-type fixnum :initform nil :initarg :cui :reader cui :print-formatter fmt-cui)
   (lui :value-type fixnum :initform nil :initarg :lui :reader lui :print-formatter fmt-lui)
   (sui :value-type fixnum :initform nil :initarg :sui :reader sui :print-formatter fmt-sui))
  (:metaclass hyperobject-class)
  (:user-name "XW Index" "XW Indices")
  (:default-print-slots wd cui lui sui))

(defclass uxw-noneng (umlsclass)
  ((lat :value-type string :initarg :lat :reader lat)
   (wd :value-type string :initarg :wd :reader wd)
   (cui :value-type fixnum :initform nil :initarg :cui :reader cui :print-formatter fmt-cui)
   (lui :value-type fixnum :initform nil :initarg :lui :reader lui :print-formatter fmt-lui)
   (sui :value-type fixnum :initform nil :initarg :sui :reader sui :print-formatter fmt-sui)
   (lrl :value-type fixnum :initform nil :initarg :lrl :reader lrl))
  (:metaclass hyperobject-class)
  (:user-name "XW Non-English Index" "XW Non-English Indices")
  (:default-print-slots wd cui lui sui))

(defclass uxnw (umlsclass)
  ((lat :value-type string :initarg :lat :reader lat)
   (nwd :value-type string :initarg :nwd :reader nwd)
   (cuilist :value-type list :initarg :cuilist :reader uxnw-cuilist))
  (:metaclass hyperobject-class)
  (:user-name "XNW Index" "XNW Indices")
  (:default-print-slots lat nwd cuilist))

(defclass uxns (umlsclass)
  ((lat :value-type string :initarg :lat :reader lat)
   (nstr :value-type string :initarg :nstr :reader nstr)
   (cuilist :value-type list :initarg :cuilist :reader cuilist))
  (:metaclass hyperobject-class)
  (:user-name "XNS Index" "XNS Indices")
  (:default-print-slots lat nstr cuilist))


;;; LEX objects

(defclass lexterm (umlsclass)
  ((eui :value-type fixnum :initarg :eui :reader eui :print-formatter fmt-eui
        :hyperlink find-lexterm-eui)
   (wrd :value-type string :initarg :wrd :reader wrd)
   (s#abr :reader s#abr :subobject (find-labr-eui eui))
   (s#agr :reader s#agr :subobject (find-lagr-eui eui))
   (s#cmp :reader s#cmp :subobject (find-lcmp-eui eui))
   (s#mod :reader s#mod :subobject (find-lmod-eui eui))
   (s#nom :reader s#nom :subobject (find-lnom-eui eui))
   (s#prn :reader s#prn :subobject (find-lprn-eui eui))
   (s#prp :reader s#prp :subobject (find-lprp-eui eui))
   (s#spl :reader s#spl :subobject (find-lspl-eui eui))
   (s#trm :reader s#trm :subobject (find-ltrm-eui eui))
   (s#typ :reader s#typ :subobject (find-ltyp-eui eui)))
  (:metaclass hyperobject-class)
  (:user-name "Lexical Term")
  (:default-print-slots eui wrd))


(defclass labr  (umlsclass)
  ((eui :value-type integer :initarg :eui :reader eui :print-formatter fmt-eui)
   (bas :value-type string :initarg :bas :reader bas)
   (abr :value-type string :initarg :abr :reader abr)
   (eui2 :value-type integer :initarg :eui2 :reader eui2 :print-formatter fmt-eui)
   (bas2 :value-type string :initarg :bas2 :reader bas2))
  (:metaclass hyperobject-class)
  (:user-name "Abbreviations and Acronym")
  (:default-print-slots eui bas abr eui2 bas2))

(defclass lagr  (umlsclass)
  ((eui :value-type integer :initarg :eui :reader eui :print-formatter fmt-eui)
   (str :value-type string :initarg :str :reader str)
   (sca :value-type string :initarg :sca :reader sca)
   (agr :value-type string :initarg :agr :reader agr)
   (cit :value-type string :initarg :cit :reader cit)
   (bas :value-type string :initarg :bas :reader bas))
  (:metaclass hyperobject-class)
  (:user-name "Agreement and Inflection")
  (:default-print-slots eui str sca agr cit bas))

(defclass lcmp  (umlsclass)
  ((eui :value-type integer :initarg :eui :reader eui :print-formatter fmt-eui)
   (bas :value-type string :initarg :bas :reader bas)
   (sca :value-type string :initarg :sca :reader sca)
   (com :value-type string :initarg :com :reader com))
  (:metaclass hyperobject-class)
  (:user-name "Complementation")
  (:default-print-slots eui bas sca com))

(defclass lmod  (umlsclass)
  ((eui :value-type integer :initarg :eui :reader eui :print-formatter fmt-eui)
   (bas :value-type string :initarg :bas :reader bas)
   (sca :value-type string :initarg :sca :reader sca)
   (psnmod :value-type string :initarg :psnmod :reader psnmod)
   (fea :value-type string :initarg :fea :reader fea))
  (:metaclass hyperobject-class)
  (:user-name "Modifier")
  (:default-print-slots eui bas sca psnmod fea))

(defclass lnom  (umlsclass)
  ((eui :value-type integer :initarg :eui :reader eui :print-formatter fmt-eui)
   (bas :value-type string :initarg :bas :reader bas)
   (sca :value-type string :initarg :sca :reader sca)
   (eui2 :value-type integer :initarg :eui2 :reader eui2 :print-formatter fmt-eui)
   (bas2 :value-type string :initarg :bas2 :reader bas2)
   (sca2 :value-type string :initarg :sca2 :reader sca2))
  (:metaclass hyperobject-class)
  (:user-name "Nominalization")
  (:default-print-slots eui bas sca eui2 bas2 sca2))

(defclass lprn  (umlsclass)
  ((eui :value-type integer :initarg :eui :reader eui :print-formatter fmt-eui)
   (bas :value-type string :initarg :bas :reader bas)
   (num :value-type string :initarg :num :reader num)
   (gnd :value-type string :initarg :gnd :reader gnd)
   (cas :value-type string :initarg :cas :reader cas)
   (pos :value-type string :initarg :pos :reader pos)
   (qnt :value-type string :initarg :qnt :reader qnt)
   (fea :value-type string :initarg :fea :reader fea))
  (:metaclass hyperobject-class)
  (:user-name "Pronoun")
  (:default-print-slots eui bas num gnd cas pos qnt fea))

(defclass lprp  (umlsclass)
  ((eui :value-type integer :initarg :eui :reader eui :print-formatter fmt-eui)
   (bas :value-type string :initarg :bas :reader bas)
   (str :value-type string :initarg :str :reader str)
   (sca :value-type string :initarg :sca :reader sca)
   (fea :value-type string :initarg :fea :reader fea))
  (:metaclass hyperobject-class)
  (:user-name "Property" "Properties")
  (:default-print-slots eui bas str sca fea))


(defclass lspl  (umlsclass)
  ((eui :value-type integer :initarg :eui :reader eui :print-formatter fmt-eui)
   (spv :value-type string :initarg :spv :reader spv)
   (bas :value-type string :initarg :bas :reader bas))
  (:metaclass hyperobject-class)
  (:user-name "Spelling Variant")
  (:default-print-slots eui spv bas))


(defclass ltrm  (umlsclass)
  ((eui :value-type integer :initarg :eui :reader eui :print-formatter fmt-eui)
   (bas :value-type string :initarg :bas :reader bas)
   (gen :value-type string :initarg :gen :reader gen))
  (:metaclass hyperobject-class)
  (:user-name "Trade Mark")
  (:default-print-slots eui bas gen))

(defclass ltyp  (umlsclass)
  ((eui :value-type integer :initarg :eui :reader eui :print-formatter fmt-eui)
   (bas :value-type string :initarg :bas :reader bas)
   (sca :value-type string :initarg :sca :reader sca)
   (typ :value-type string :initarg :typ :reader typ))
  (:metaclass hyperobject-class)
  (:user-name "Inflection Type")
  (:default-print-slots eui bas sca typ))

(defclass lwd (umlsclass)
  ((wrd :value-type string :initarg :wrd :reader wrd)
   (euilist :value-type list :initarg :euilist :reader euilist))
  (:metaclass hyperobject-class)
  (:user-name "Lexical Word Index" "Lexical Word Indices")
  (:default-print-slots wrd euilist))

;;; Semantic NET objects

(defclass sdef (umlsclass)
  ((rt :value-type string :initarg :rt :reader rt)
   (ui :value-type integer :initarg :ui :reader ui :print-formatter fmt-tui)
   (styrl :value-type string :initarg :styrl :reader styrl)
   (stnrtn :value-type string :initarg :stnrtn :reader stnrtn)
   (def :value-type string :initarg :def :reader def)
   (ex :value-type string :initarg :ex :reader ex)
   (un :value-type string :initarg :un :reader un)
   (rh :value-type string :initarg :rh :reader rh)
   (abr :value-type string :initarg :abr :reader abr)
   (rin :value-type string :initarg :rin :reader rin))
  (:metaclass hyperobject-class)
  (:user-name "Basic information about Semantic Types and Relation")
  (:default-print-slots rt ui styrl stnrtn def ex un rh abr rin))

(defclass sstr (umlsclass)
  ((styrl :value-type string :initarg :styrl :reader styrl)
   (rl :value-type string :initarg :rl :reader rl)
   (styrl2 :value-type string :initarg :styrl2 :reader styrl2)
   (ls :value-type string :initarg :ls :reader ls))
  (:metaclass hyperobject-class)
  (:user-name "Structure of the Network")
  (:default-print-slots styrl rl styrl2 ls))

(defclass sstre1 (umlsclass)
  ((ui :value-type integer :initarg :ui :reader ui :print-formatter fmt-tui)
   (ui2 :value-type integer :initarg :ui2 :reader ui2 :print-formatter fmt-tui)
   (ui3 :value-type integer :initarg :ui3 :reader ui3 :print-formatter fmt-tui))
  (:metaclass hyperobject-class)
  (:user-name "Fully Inherited Set of Relation (TUIs)"
              "Fully Inherited Set of Relations (TUIs)")
  (:default-print-slots ui ui2 ui3))

(defclass sstre2 (umlsclass)
  ((sty :value-type string :initarg :ui :reader sty)
   (rl :value-type string :initarg :ui2 :reader rl)
   (sty2 :value-type string :initarg :ui3 :reader sty2))
  (:metaclass hyperobject-class)
  (:user-name "Fully Inherited Set of Relation (strings)"
              "Fully Inherited Set of Relations (strings)")
  (:default-print-slots sty rl sty2))


;;; **************************
;;; Local Classes
;;; **************************

(defclass ustats (umlsclass)
  ((name :value-type string :initarg :name :reader name)
   (hits :value-type integer :initarg :hits :reader hits
         :user-name "count"
         :print-formatter fmt-comma-integer)
   (srl :value-type fixnum :initarg :srl :reader srl))
  (:metaclass hyperobject-class)
  (:default-initargs :name nil :hits nil :srl nil)
  (:user-name "UMLS Statistic")
  (:default-print-slots name hits srl)
  (:documentation "Custom Table: UMLS Database statistics."))


(defclass bsab (umlsclass)
  ((sab :value-type string :initarg :sab :reader sab
        :hyperlink find-ustr-sab
        :hyperlink-parameters (("subobjects" . "no")))
   (name :value-type string :initarg :name :reader name)
   (hits :value-type fixnum :initarg :hits :reader hits
         :user-name "count"
         :print-formatter fmt-comma-integer))
  (:metaclass hyperobject-class)
  (:default-initargs :sab nil :name nil :hits nil)
  (:user-name "Source of Abbreviation")
  (:default-print-slots sab name hits)
  (:documentation "Bonus SAB file"))

(defclass btty (umlsclass)
  ((tty :value-type string :initarg :tty :reader tty)
   (name :value-type string :initarg :name :reader name)
   (hits :value-type fixnum :initarg :hits :reader hits
         :user-name "count"
         :print-formatter fmt-comma-integer))
  (:metaclass hyperobject-class)
  (:default-initargs :tty nil :name nil :hits nil)
  (:user-name "Bonus TTY")
  (:default-print-slots tty name hits)
  (:documentation "Bonus TTY file"))

(defclass brel (umlsclass)
  ((sab :value-type string :initarg :sab :reader sab)
   (sl :value-type string :initarg :sl :reader sl)
   (rel :value-type string :initarg :rel :reader rel)
   (rela :value-type string :initarg :rela :reader rela)
   (hits :value-type fixnum :initarg :hits :reader hits
         :user-name "count"
         :print-formatter fmt-comma-integer))
  (:metaclass hyperobject-class)
  (:default-initargs :sab nil :sl nil :rel nil :rela nil :hits nil)
  (:user-name "Bonus REL")
  (:default-print-slots sab sl rel rela hits)
  (:documentation "Bonus REL file"))

(defclass batn (umlsclass)
  ((sab :value-type string :initarg :sab :reader sab)
   (atn :value-type string :initarg :atn :reader atn)
   (hits :value-type fixnum :initarg :hits :reader hits
         :user-name "count"
         :print-formatter fmt-comma-intger))
  (:metaclass hyperobject-class)
  (:default-initargs :sab nil :atn nil)
  (:user-name "Bonus ATN")
  (:default-print-slots sab atn hits)
  (:documentation "Bonus ATN file"))
