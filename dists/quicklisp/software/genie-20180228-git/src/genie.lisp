(in-package :genie)

(simple-logger:define-message :info :genie.seed "Random seed: ~A")

(defun make-seed ()
  "Compute a seed to be given to a generator."
  (mod
   (parse-integer
    (alexandria:shuffle
     (format nil "~d~d"
             (get-universal-time)
             (get-internal-real-time))))
   (expt 2 48)))

(defun make-generator (&optional (seed (make-seed)))
  "Create a random number generator object, using SEED to initialize it. If SEED is not supplied, a
new seed will be generated."
  (simple-logger:emit :genie.seed seed)
  (cl-variates:make-random-number-generator seed 'cl-variates:ranq1-random-number-generator))

(defgeneric gen (kind generator &key)
  (:documentation "Generate a random number of a certain kind."))

(defmethod gen ((kind (eql :bool)) generator &key (probability 0.5d0))
  "Generate a true or false value, with the given PROBABILITY for true."
  (cl-variates:random-boolean generator probability))

(defmethod gen ((kind (eql :range)) generator &key (min 0.0d0) (max 1.0d0))
  "Generate a number between MIN and MAX."
  (cl-variates:random-range generator min max))

(defmethod gen ((kind (eql :int)) generator &key (min 0) (max 1) parityp)
  "Generate an integer between MIN and MAX (inclusive). If PARITYP is non-nil, generate an even
number if MIN is even, or an odd number if MIN is odd."
  (if parityp
      (+ min (* 2 (cl-variates:integer-random generator 0 (floor (/ (- max min) 2)))))
      (cl-variates:integer-random generator min max)))

(defmethod gen ((kind (eql :elt)) generator &key sequence)
  "Randomly select an element from SEQUENCE."
  (cl-variates:random-element generator sequence))
