;;; ************
;;; Metatheorems
;;; ************

(module "prop-meta"
;  (import [!!prop-ax])
  (import [!!prop])

  ;; TODO: multiple assertions
  (metath "infer" (ref &rest goals)
    (declare (ignore goals))
    (let* ((pfl (parse-subproof ref))
           (assert (car (subproof-assert pfl))))
      ;; match implication
      (in-context *meta-context*
        (match-case assert
          ([→ ?x ?y] (linear-subproof (?x) (?y)
                       pfl
                       [ax-mp ,?x ,?y]))
          ;; else match biconditional
          ([↔ ?x ?y] (acond
            ((provenp ?x) (linear-subproof (?x) (?y)
                            pfl
                            [infer bi> ,?x ,?y]
                            [ax-mp ,?x ,?y]))
            ((provenp ?y) (linear-subproof (?y) (?x)
                            pfl
                            [infer bi< ,?x ,?y]
                            [ax-mp ,?y ,?x]))
            (t (error "Infer: cannot find ~a or ~a" ?x ?y))))
          (t pfl)))))

  ;; TODO: multiple assertions
  (metath "ded" (ref var)
    (let* ((pfl (parse-subproof ref))
           (assert (car (subproof-assert pfl))))
      ;; match implication
      (in-context *meta-context*
        (if-match [→ ?x ?y] assert
          (linear-subproof ([→ ,var ,?x]) ([→ ,var ,?y])
            pfl
            [!im!tri ,var ,?x ,?y])
          ;; else match bi-implication
          (if-match [↔ ?x ?y] assert
            (let ((lhs [→ ,var ,?x])
                  (rhs [→ ,var ,?y]))
              (if (provenp lhs)
                (linear-subproof (lhs) (rhs)
                  pfl
                  [infer bi> ,?x ,?y]
                  [!im!tri ,var ,?x ,?y])
                (if (provenp rhs)
                  (linear-subproof (rhs) (lhs)
                    pfl
                    [infer bi< ,?x ,?y]
                    [!im!tri ,var ,?y ,?x])))))))))

  ;; TODO: multiple assertions
  (metath "+syl" (ref var)
    (let* ((pfl (parse-subproof ref))
           (assert (car (subproof-assert pfl))))
      ;; match implication
      (in-context *meta-context*
        (if-match [→ ?x ?y] assert
          (linear-subproof ([→ ,?y ,var]) ([→ ,?x ,var])
            pfl
            [!im!tri ,?x ,?y ,var])
          ;; else match bi-implication
          (if-match [↔ ?x ?y] assert
            (let ((lhs [→ ,?x ,var])
                  (rhs [→ ,?y ,var]))
              (if (provenp lhs)
                (linear-subproof (lhs) (rhs)
                  pfl
                  [infer bi< ,?x ,?y]
                  [!im!tri ,?y ,?x ,var])
                (if (provenp rhs)
                  (linear-subproof (rhs) (lhs)
                    pfl
                    [infer bi> ,?x ,?y]
                    [!im!tri ,?x ,?y ,var])))))))))

  (metath "b2i" (ref)
    (in-context *meta-context*
    (let* ((pfl (parse-subproof ref)))
      (subproof (:assert (subproof-assert pfl))
        (dolist (h (subproof-hypo pfl))
          (match-case h 
            ([→ ?x ?y] (acond
              ((provenp h) (hypo h))
              ((provenp ?y) (hypo it) (line [infer ax1 ,?y ,?x]))
              ;; Convert biconditionals to implications
              ((provenp [↔ ,?x ,?y]) (hypo it) (line [infer bi> ,?x ,?y]))
              ((provenp [↔ ,?y ,?x]) (hypo it) (line [infer bi< ,?y ,?x]))
              (t (error "b2i: cannot prove ~a" h))))
            ;; Handle biconditionals in reverse direction
            ([↔ ?x ?y] (acond
              ((provenp h) (hypo h))
              ((provenp [↔ ,?y ,?x]) (hypo it) (line [infer !bi!com ,?y ,?x]))
              (t (error "b2i: cannot prove ~a" h))))
            (t (hypo h))))
        (line pfl)))))

  (defun dedth-p (thr)
    (gethash :ded (context-meta thr)))

  ;;; Convert a proof segment for {h} => {a}
  ;;; into one for {str -> h} => {str -> a}
  (metath "dedth" (prf str)
    (in-context *meta-context*
      (let ((*current-proof* (list (make-pattern-tree))))
        (labels ((rec (prf)
                   (subproof (:assert (loop for i in (subproof-assert prf)
                                            collect [→ ,str ,i]))
                     ;; Case 1: theorem reference
                     (aif (subproof-ref prf)
                       (progn
;                         (format t "Ded: direct ref ~a~%" it)
                         ;; "Import" statements proved at upper levels
                         (dolist (h (subproof-hypo prf))
                           (unless (provenp [→ ,str ,h])
;                             (format t "Ded: Hypo ~a not found, importing~%" h)
                             (line [infer ax1 ,h ,str])))
                         (if (subproof-hypo prf)
                           (if (dedth-p (car it))
                             (let* ((prev (car (last it)))
                                    (new [∧ ,str ,prev]))
;                               (format t "Ded: thr already in dedth-form~%")
                               (dolist (h (subproof-hypo prf))
                                 (line [imp ,str ,prev ,h]))
                               (line `(,(car it) ,@(butlast it) ,new))
                               (dolist (a (subproof-assert prf))
                                 (line [exp ,str ,prev ,a])))
                             (progn
;                               (format t "Ded: calling dedth-1~%")
                               (line `(,[dedth-1 ,(car it)] ,@(cdr it) ,str))))
                           ;; Simple case: no hypotheses
                           (progn
;                             (format t "Ded: no hypotheses, using ax1~%")
                             (line prf)
                             (dolist (a (subproof-assert prf))
                               (line [infer ax1 ,a ,str]))))))
                       ;; Case 2: sequence of subproofs
                       (progn
;                         (format t "Ded: subproof[~d]~%" (length (subproof-sub prf)))
                         (dolist (s (subproof-sub prf))
                           (line (rec s)))))))
          (dolist (h (subproof-hypo prf))
            (insert-wff (car *current-proof*) [→ ,str ,h]))
          (rec prf)))))

  (defmacro suppose (hypo assr &body lines)
    `(let ((prf (linear-subproof ,hypo ,assr ,@lines)))
       (let ((num-hypo (length (subproof-hypo prf))))
         (case num-hypo
           (0 prf) ;; TODO: print warning? 
           (t (let ((conj (reduce #'(lambda (x y) [∧ ,x ,y])
                                  (subproof-hypo prf))))
                (subproof (:assert (loop for i in (subproof-assert prf)
                                        collect [→ ,conj ,i]))
                  (if (> num-hypo 1)
                    (line [siman ,conj]) ;; TODO: multiple hypo needs multiple siman
                    (line [id ,conj]))
                  (line [dedth ,prf ,conj]))))))))

  (simple-metath "dedth-1" (thr)
    (in-context *meta-context*
      (let-once (mp (symref-target !ax-mp))
        (if (eq thr mp)
          (symref-target !mpd)
          (copy-th thr "-d1" (vars distinct)
            (let ((x (var wff :arg)))
              (dolist (hp (context-hypo thr))
                (hypo [→ ,x ,hp]))
              (dolist (assr (context-assert thr))
                (ass [→ ,x ,assr]))
              (proof
                [dedth ,(context-proof thr) ,x])))))))

  ) ; end module prop-meta
