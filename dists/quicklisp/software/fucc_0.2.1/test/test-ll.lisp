(cl:in-package #:fucc-generator)

(defun make-lexer (tokens)
  (lambda ()
    (let ((val (pop tokens)))
      (format t "LEXER: ~S~%" val)
      (values val val))))

(defun make-simple-grammar ()
  (let ((grammar (parse-grammar 's
                                '(a b c)
                                '((s (:action #'list)
                                   k l)
                                  (k (:action #'list)
                                   a b)
                                  (l (:action #'list)
                                   a c)))))
    (calculate-first grammar)
    (calculate-follow grammar)
    grammar))

(defun make-debug-ll-table (grammar)
  (convert-to-deterministic-ll-table
   (make-ll-table grammar)))

(defun run-simple-ll-test ()
  (let ((grammar (make-simple-grammar)))
    (let ((table (make-debug-ll-table grammar)))
      (parse-ll (make-lexer '(1 2 1 3 0)) table grammar))))
