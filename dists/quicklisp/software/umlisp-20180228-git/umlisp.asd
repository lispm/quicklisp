;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: umlisp -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          umlisp.asd
;;;; Purpose:       ASDF system definition file for UMLisp
;;;; Author:        Kevin M. Rosenberg
;;;; Date Started:  Apr 2000
;;;;
;;;; This file, part of UMLisp, is
;;;;    Copyright (c) 2000-2006 by Kevin M. Rosenberg, M.D.
;;;;
;;;; UMLisp users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License.
;;;; *************************************************************************

(defpackage #:umlisp-system (:use #:asdf #:cl))
(in-package #:umlisp-system)

(defsystem umlisp
    :components
  ((:file "package")
   (:file "data-structures" :depends-on ("package"))
   (:file "utils" :depends-on ("data-structures"))
   (:file "sql" :depends-on ("utils"))
   (:file "parse-macros"  :depends-on ("sql"))
   (:file "parse-rrf"  :depends-on ("parse-macros"))
   (:file "parse-common"  :depends-on ("parse-rrf"))
   (:file "create-sql" :depends-on ("parse-common"))
   (:file "sql-classes" :depends-on ("sql"))
   (:file "classes" :depends-on ("sql-classes"))
   (:file "class-support" :depends-on ("classes"))
   (:file "composite" :depends-on ("sql-classes")))
  :depends-on (clsql clsql-mysql kmrcl hyperobject))

(defmethod perform ((o test-op) (c (eql (find-system 'umlisp))))
  (operate 'load-op 'umlisp-tests)
  (operate 'test-op 'umlisp-tests :force t))

(defmethod perform :after ((o load-op) (c (eql (find-system 'umlisp))))
  (let ((init-file (or (probe-file
                        (merge-pathnames
                         (make-pathname :name ".umlisprc")
                         (user-homedir-pathname)))
                       #+(or mswin windows win32)
                       (probe-file "c:\\etc\\umlisp-init.lisp"))))
    (when init-file
      (format t "loading umlisp init file ~A~%" init-file)
      (load init-file))))

