;;;  db.lisp --- example Berkeley DB FFI generation module

;;;  Copyright (C) 2004 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-3D7D20BCF38AF8BA1CADF8631B1807DC.lisp,v 1.9 2004/04/07 15:27:50 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:
;;;
;;;  This works with gcc on FreeBSD.

#+cmu (ext:file-comment "$Id: F-3D7D20BCF38AF8BA1CADF8631B1807DC.lisp,v 1.9 2004/04/07 15:27:50 wcp Exp $")

(in-package :binge-examples)

(defvar *db-ffi-c-constants*
  '("BTREEMAGIC"
    "BTREEVERSION"
    "DB_LOCK"
    "DB_LOCK"
    "DB_SHMEM"
    "DB_SHMEM"
    "DB_TXN"
    "DB_TXN"
    "HASHMAGIC"
    "HASHVERSION"
    "MAX_PAGE_NUMBER"
    "MAX_PAGE_OFFSET"
    "MAX_REC_NUMBER"
    "RET_ERROR"
    "RET_SPECIAL"
    "RET_SUCCESS"
    "R_CURSOR"
    "R_DUP"
    "R_FIRST"
    "R_FIXEDLEN"
    "R_IAFTER"
    "R_IBEFORE"
    "R_LAST"
    "R_NEXT"
    "R_NOKEY"
    "R_NOOVERWRITE"
    "R_PREV"
    "R_RECNOSYNC"
    "R_SETCURSOR"
    "R_SNAPSHOT"))

(defun create-db-ffi ()
  (let ((namespace (make-instance 'c-namespace)))
    (binge-compiler:add-declaration *gcc-builtin-va_list* namespace)
    (populate-namespace namespace
			(extract-from-headers '("db.h")
					      :defines *gcc-null-defines*
					      :constants *db-ffi-c-constants*))
    (make-ffi #P"db-ffi.lisp" namespace :variables t :functions t :constants t
	      :types '("DB" "DBTYPE"))))
