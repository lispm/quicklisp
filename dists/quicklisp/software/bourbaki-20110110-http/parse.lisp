;;; Copyright (c) Juha Arpiainen 2005-06

;;; This file is part of Bourbaki.

;;; Bourbaki is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.

;;; Bourbaki is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Bourbaki; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; -------

;;; Macros for defining theorems, symbols, ...

(in-package :bourbaki)

(defmacro in-context (ref &body body)
  "Enter an existing context"
  `(let ((*current-context* (mkcontext ,ref)))
     (progn . ,body)))

(defmacro module (name &body body)
  "Enter a top-level context, creating it if it did not already exist"
  `(let ((*current-context* (seek-create-ctx ,name :module *root-context*)))
     (progn . ,body)))

(defmacro local (name &body body)
  "Enter a subcontext, creating it if it did not already exist"
  `(let ((*current-context* (seek-create-ctx ,name :context *current-context*)))
     (progn . ,body)))

;;; Define a named theorem or axiom
(defmacro defcontext (class name vars &body body)
  (let ((thrsym (gensym)) (nvars (gensym)))
    `(let ((,thrsym (create-context :class ,class :name ,name :parent *current-context*)) ,nvars)
       ;; enter theorem context
       (let ((*current-context* ,thrsym))
         ;; parse variables
         (setf ,nvars (parse-vars ,vars))
         
         ;; execute body as progn
         (progn . ,body))
       (wrap-ctx ,thrsym ,nvars))))

;(defvar *meta-proof-fn*
;  (html-dump-line
;    #'(lambda (ref stream)
;        (html-dump-ref (gethash :original (context-meta ref)) stream))
;    #'(lambda (x) (length (context-assert x)))
;    #'(lambda (line stream)
;        (let* ((ref (car line))
;               (smap (smap ref (cdr line)))
;               (lst (mapcar #'(lambda (x) (replace-vars x smap)) (context-assert ref))))
;          (html-dump-expr lst stream)))))

;; For metatheorems: Create a threorem with imports from *current-context*, but
;; other properties from thr.
(defmacro copy-th (thr suffix items &body body)
  (let ((thrsym (gensym)) (origsym (gensym)))
    `(let* ((,origsym ,thr)
            (,thrsym (make-context :class :theorem :name (concatenate 'string (context-name ,origsym) ,suffix)
                                   :imports (context-imports *current-context*))))
       (let ((*current-context* ,thrsym))
         (setf ,@(copy-items items origsym))
         (meta (:original .
                 (multiple-value-bind (o found?) (gethash :original (context-meta ,origsym))
                   (if found? o ,origsym))))
         (progn . ,body))
       ,thrsym)))
;       (wrap-ctx ,thrsym))))

;; XXX hack
(defun copy-items (lst from)
  (mapcan #'(lambda (x)
              (let ((sym (intern (concatenate 'string "CONTEXT-" (symbol-name x)))))
                `((,sym *current-context*) (,sym ,from))))
          lst))

;; Define a named theorem and insert it into the current context
(defmacro th (name vars &body body)
  `(insert-sym (defcontext :theorem ,name ,vars . ,body)))

;; Define a named axiom in current context
(defmacro ax (name vars &body body)
  `(insert-sym (defcontext :axiom ,name ,vars . ,body)))

;; A group of theorems with common variables and hypotheses
(defmacro theory (name vars &body body)
  `(insert-sym (defcontext :context ,name ,vars . ,body)))

(defun parse-varspec (type names class)
  (let ((namelist (mklist names)))
    (loop for name in namelist
          collect `(var2 ,type ,name ,class))))

;;; parse the variables list of a theorem or definition
;;; format: (type1 ("var_11" "var_12" ... "var_1n") type2 ("var_21" ... "var_2m") ...)
;;; TODO: Should print warning if odd number of arguments is used
;;; NOTE: The order of variables is now reversed for linking to parent
(defmacro parse-vars (varlist &optional (class :arg))
  (let ((result
          (loop for varspec on varlist by #'cddr
                nconc (parse-varspec (first varspec) (second varspec) class))))
    `(progn ,@result ,(length result))))

(defmacro symkind (name &key (super nil super-provided-p))
  (let ((namesym (gensym))
        (kindsym (gensym)))
    `(let* ((,namesym ,name)
            (,kindsym (make-context :syms nil :name ,namesym :class :symkind)))
       ,(if super-provided-p `(setf (gethash :super (context-meta ,kindsym)) (get-symkind ',super)))
       (aif (seek-ctx-imports ,namesym *root-context*)
          it
          (insert-sym (make-symref :target ,kindsym
                                   :args-needed 0
                                   :fn (load-time-value (lambda () nil))
                                   :class :symkind
                                   :proof nil)
                      *root-context*
                      ,namesym)))))

(defun get-symkind (sym)
  (mkcontext (seek-sym :abs (symbol-name sym))))

(defmacro set-symkind-eq-op (kind op)
  `(setf (gethash :eq-op (context-meta (get-symkind ',kind)))
         (mkcontext ,op)))

;;; axiom or theorem assertion
;;; format: (ass [assert1] [assert2] ...)
(defmacro ass (&rest lst)
  "Axiom or theorem assertion"
  (let ((asrsym (gensym)))
    `(dolist (,asrsym (list . ,lst))
       (push ,asrsym (context-assert *current-context*)))))

;;; hypothesis
;;; format: (hypo [hypo1] [hypo2] ...)
(defmacro hypo (&rest lst)
  "Axiom or theorem hypotheses"
  (let ((hyposym (gensym)))
    `(dolist (,hyposym (list . ,lst))
       (push ,hyposym (context-hypo *current-context*)))))

;;; List of distinct variable conditions
;;; format: (dist list_1 list_2 ... list_n)
;;;         list_i: (sym_1 ... sym_m)
(defmacro dist (&rest lst)
  "Distinct variable conditions"
  (let ((temp nil))
    (dolist (cnd lst)
      (push `(dvc-list (list . ,cnd) *current-context*) temp))
    (cons 'progn temp)))

;;; meta information
(defmacro meta (&rest specs)
  `(progn
     ,@(loop for spec in specs
             collect `(setf (gethash ,(car spec) (context-meta *current-context*))
                            ,(cdr spec)))))

;;; Create and return a primitive symbol
;;; "dummy" symbols are made for the arguments; their sole purpose is to hold
;;; the type information; they are not inserted into the symbol's context
(defmacro prim (type name vars &body body)
  "A primitive symbol"
  (let ((the-sym (gensym)) (counter (gensym)))
    `(let ((,the-sym (create-context :type (get-symkind ',type) :name ,name :class :prim :parent *current-context*))
           (,counter 0))
       (let ((*current-context* ,the-sym))
         (setf ,counter (parse-vars ,vars))
         (progn . ,body))
       (insert-sym (wrap-ctx ,the-sym ,counter)))))

;;; Create a "dummy" symbol and insert it to the variable list of *current-context*
(defmacro var (type class)
  `(let ((varsym (mkvar (get-symkind ',type) ,class)))
     (push varsym (context-vars *current-context*))
     (wrap-ctx varsym)))

(defmacro var2 (type name class)
  (let ((rtype (if (consp type) (cadr type) type))
        (bound? (if (consp type) (car type) nil)))
    `(let ((varsym (mkvar (get-symkind ',rtype) ,class :name ,name :bound ,bound?)))
       (insert-sym (wrap-ctx varsym))
       ,(if (eq class :arg) '(push varsym (context-vars *current-context*)))
       varsym)))

;;; Create and return a new "dummy" or "local" variable.
;;; The variable is marked distinct from other variables in the context
;;; TODO: use varspec
;(defmacro loc (type name)
;  "Create a dummy variable"
;  `(let ((var (mkvar ,type :loc :name ,name)))
;     (insert-sym (wrap-ctx var))
;     var))
(defmacro loc (&rest varspec)
  `(parse-vars ,varspec :loc))

(defmacro providing (ctx &body body)
  (let ((root (gensym)))
    `(let ((,root (mkrootctx)))
       ,@(loop for i in ctx
               collect (if (consp i)
                         `(setf (gethash ,(car i) (context-syms ,root)) ,(cdr i))
                         `(setf (gethash ,i (context-syms ,root))
                                (seek-ctx-imports ,i *root-context*))))
       (let ((*root-context* ,root))
         (progn . ,body)))))

(defun import (str &key proof)
  (insert-import str :proof proof))

(defun export (str &key proof)
  (insert-export str :proof proof))

(defmacro th-pattern (ref)
  `(insert-pat *theorem-patterns* ,ref))
