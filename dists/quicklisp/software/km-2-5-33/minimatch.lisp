
(unless (find-package :km) (make-package :km :use '(:common-lisp)))
(in-package :km)

;;; File: minimatch.lisp
;;; Author: Peter Clark
;;; Date: August 1994
;;; Purpose: Simplistic pattern-matching (see examples below)
;;; The system matches items with variables, returning a list of the
;;; matched items. All variables are anonymous.

 ;;; Here where we know there's just ONE item
(defun minimatch1 (item pattern) (first (minimatch item pattern)))

;;; (find-pattern1 '((a r b) (a r c)) '(a r ?x)) -> b
;;; (find-pattern '((a r b) (a r c)) '(?x r ?y)) -> (a b)
(defun find-pattern1 (list pattern) (first (find-pattern list pattern)))

;;; Mini-matching -- doesn't keep an explicit binding list, but just the
;;; values which matched with variables, in order.
;;; (minimatch 'x 'y) => nil
;;; (minimatch '(a b c) '(a ?x ?y)) => (b c)
;;; (minimatch '(a b c) '(a ?x ?x)) => (b c)
;;; (minimatch '(a b) '(a b)) => t
;;; (minimatch '(a b c (d e)) '(a b ?x (?y ?z))) => (c d e)
;;; (minimatch '(a b c (d e)) '(a b ?x ?y)) => (c (d e))
;;; (minimatch '(a b c (d e)) '(a b &rest) => ((c (d e)))

(defun mv-minimatch (item pattern)
  (values-list (minimatch item pattern)))

(defun anonymous-minimatch-varp (var) (member var '(|?ANY| |?any| |?*|)))
(defun wildcard-varp (var) (eq var '?*))

;;; Must distinguish failure (nil) and no bindings (t)
;;; Mar'04 - use of wildcard variable ?*
;;; CL-USER(28): (minimatch '(1 2 3 4 5 6 7 8) '(?* 3 ?x ?* 6 ?y ?z ?*))
;;; (4 7 8)
(defun minimatch (item pattern)
   (cond 
      ((anonymous-minimatch-varp pattern) 't)
      ((var-p pattern) (list item))
      ((and (singletonp pattern) (restvar-p (first pattern))) (list item))
      ((atom pattern)
       (cond ((equal item pattern) 't)))
      ((listp item)
       (cond ((wildcard-varp (first pattern))		; '(1 2 3) '(?* 3)
	      (or (minimatch item (rest pattern))			; ?* = no elements
		  (and item (minimatch (rest item) (rest pattern)))	; ?* = 1 element
		  (and item (minimatch (rest item) pattern))))		; ?* = 2 or more elements
	     (item
	      (let ( (carmatch (minimatch (car item) (car pattern))) )
		(cond (carmatch
		       (join-binds carmatch
				   (minimatch (cdr item) (cdr pattern)))))))))))

(defun join-binds (binds1 binds2)
   (cond ((null binds1) nil)
	 ((null binds2) nil)
         ((equal binds1 't) binds2)
	 ((equal binds2 't) binds1)
	 (t (append binds1 binds2))))

;;; Modified faster version thanks to Adam Farquhar!
;;; Renamed from varp to avoid name clash with Novak's code
;;; Synonymous with km-varp
(defun var-p (var)
  (and (symbolp var)
       (symbol-starts-with var #\?)))

(defun restvar-p (x) 
; (and (symbolp x) (starts-with (string-downcase x) "&rest")) - less efficient
  (member x '(&rest &rest1 &rest2 &rest3 &rest4 &rest5 |&rest| |&rest1| |&rest2| |&rest3| |&rest4| |&rest5|)))

(defun find-pattern (list pattern)
  (cond ((endp list) nil)
	((minimatch (first list) pattern))
	(t (find-pattern (rest list) pattern))))

;;; ======================================================================
;;;	USE OF THE MINIMATCHER TO SELECT A LAMBDA EXPRESSION
;;; ======================================================================

#|
find-handler -- finds a (pattern function) pair where pattern matches the input
expr, and returns a LIST of THREE things:
  - function 
  - a list of values in expr which matched the variables in pattern
  - the entire pattern which the input expr matched

e.g., 
(find-handler '(the house of john) *km-handler-alist*) =>
    (#'(lambda (slot path) (getval slot path)) 
     (house john)
     (the ?slot of ?expr))
|#
(defun find-handler (expr handler-alist &key (fail-mode 'fail))
  (cond ((endp handler-alist)
	 (cond ((eq fail-mode 'error)
		(format t "ERROR! Can't find handler for expression ~a!~%"
			expr) nil)))
	(t (let* ( (pattern+handler (first handler-alist))
		   (pattern (first  pattern+handler))
		   (handler (second pattern+handler))
		   (bindings (minimatch expr pattern)) )
	     (cond ((eq bindings 't) (list handler nil pattern))
		   (bindings (list handler bindings pattern))
		   (t (find-handler expr (rest handler-alist) :fail-mode fail-mode)))))))

;;; Default method of applying
;;; Or could apply with extra args, eg.
;;; 	(apply (first handler) (cons depth (second handler)))
(defun apply-handler (handler) 
  (apply (first handler) (second handler)))

(defun find-and-apply-handler (expr handler-alist &key (fail-mode 'fail))
  (let ( (handler (find-handler expr handler-alist :fail-mode fail-mode)) )
    (cond (handler (apply-handler handler)))))
  
;;; ======================================================================
;;;		SAME, EXCEPT FOR STRINGS
;;; ======================================================================

;;; If :case-sensitivep = nil, then string matches are case-insensitive
(defun string-match1 (item pattern &key case-sensitivep) 
  (first (string-match item pattern :case-sensitivep case-sensitivep)))

(defun mv-string-match (string pattern &key case-sensitivep)
  (values-list (string-match string pattern :case-sensitivep case-sensitivep)))

;;; (string-match "the cat sat" '("the" ?cat "sat")) --> (" cat ")
;;; (string-match "the cat sat" '(?var "the" ?cat "sat")) --> ("" " cat ")
;;; Expand to allow ?any as a variable
;;; (string-match "the cat sat" '(?any " " ?word " " ?any)) --> ("cat")
(defun string-match (string pattern &key case-sensitivep)
  (let ( (pattern-el (first pattern)) )
    (cond ((null pattern) 
	   (cond ((string= string "") t)))
;	  ((member pattern '((&rest) (|&rest|)) :test #'equal) (list string))
	  ((and (singletonp pattern) (restvar-p (first pattern))) (list string))
	  ((stringp pattern-el)
	   (cond 
	    ((and (>= (length string) (length pattern-el))
		  (or (string= string pattern-el :end1 (length pattern-el))
		      (and (not case-sensitivep) 
			   (string= (string-downcase string) (string-downcase pattern-el) :end1 (length pattern-el)))))
	     (string-match (subseq string (length pattern-el))
			   (cdr pattern)
			   :case-sensitivep case-sensitivep))))
	  ((and (anonymous-minimatch-varp pattern-el)
		(singletonp pattern)) t)
	  ((and (var-p pattern-el)
		(singletonp pattern)) (list string))
	  ((and (var-p pattern-el)
		(stringp (second pattern)))
	   (let ((end-string-posn (cond (case-sensitivep (search (second pattern) string))
					(t (search (string-downcase (second pattern)) (string-downcase string))))))
	     (cond (end-string-posn
		    (let ((rest-matches (string-match (subseq string
							      (+ end-string-posn
								 (length (second pattern))))
						      (cddr pattern)
						      :case-sensitivep case-sensitivep)))
		      (cond ((anonymous-minimatch-varp pattern-el) rest-matches)
			    (t (cons-binding (subseq string 0 end-string-posn) rest-matches))))))))
	  (t (format t "ERROR! (string-match ~s ~s) bad syntax!~%"
		     string pattern) nil))))

;;; binding or bindings = nil imply match-failure
(defun cons-binding (binding bindings)
  (cond ((null bindings) nil)
	((null binding) nil)
	((equal bindings 't) (list binding))
	(t (cons binding bindings))))

;;; ======================================================================

;;; (full-match '(a b (c)) '(?a ?b ?c)) -> ((?a . a) (?b . b) (?c . (c)))
;;; (full-match '(a b c d) '(?any ?b &rest)) -> ((?b . b) (&rest . (c d)))
;;; (full-match '(a b (c)) '(?any ?b &rest)) -> ((?b . b) (&rest . ((c))))
;;; (full-match 1 1) -> ((t . t))
;;; (val-of '?b '((?b . b))) -> b
(defun full-match (item pattern &key (bindings *null-bindings*))
   (cond 
      ((anonymous-minimatch-varp pattern) bindings)
      ((var-p pattern) (add-binding pattern item bindings))
;     ((member pattern '((&rest) (|&rest|)) :test #'equal) bindings)
      ((and (singletonp pattern) (restvar-p (first pattern))) (add-binding (first pattern) item bindings))
      ((atom pattern)
       (cond ((equal item pattern) bindings)))
      ((listp item)
       (cond ;((wildcard-varp (first pattern))		; '(1 2 3) '(?* 3)
	     ; (or (full-match item (rest pattern))			; ?* = no elements
	     ;	  (and item (full-match (rest item) (rest pattern)))	; ?* = 1 element
	     ;	  (and item (full-match (rest item) pattern))))		; ?* = 2 or more elements
	     (item
	      (let ((new-bindings (full-match (car item) (car pattern) :bindings bindings)) )
		(cond (new-bindings (full-match (cdr item) (cdr pattern) :bindings new-bindings)))))))))

