;;;; Equality and Substitution

(module "equ-ax"

  (meta (:comment . "Axioms for equality and substitution.")
        (:description . "Axioms 8, 9, 11, and the definition of proper
substitution are stated for set terms (st) to be introduced with the
iota descriptor. Our axioms 9 and 11 also have distinct variable
conditions."))

  (symkind "SET")
  (symkind "VAR" :super set)

  (export [!!prop-ax])
  (export [!!pred-ax])

  (prim wff "=" (set ![a b]))
;    (meta (:html-sym . "eq")
;          (:html-fn . #'binary-infix)))

;; Set equality operator
;; (no need to set for sv, as we don't define any bound set variables)

  (set-symkind-eq-op set !=)

  ;; Axioms

  ; Theorem 2.2 of [Bernays]
  (ax "ax8" (set ![x y z])
      (meta (:comment . "Axiom of Equality.")
            (:description . "One of the 5 equality axioms of predicate calculus. This is similar to, but not quite, a transitive law
for equality (proved later as !eqtr). Axiom C8' in [Megill] p. 448 (p. 16 of the preprint). Also appears as Axiom C7 of [Monk2] p.105.
<p>Note that in our version the variables stand for arbtrary set /terms/ making this technically an axiom scheme."))
      (ass [→ = x y → = x z = y z]))

  (ax "ax9" (var "x" set "y")
      (meta (:comment . "Axiom of Existence.")
            (:description . "One of the 5 equality axioms of predicate calculus. This axiom in effect tells us that at least
one thing exists. In this version y can be any set term where x does not occur. The Metamath version without the distinct
varible condition is proved in !ax9-sv.
<p>Raph Levien proved the independence of this axiom from the others on 12-Apr-2005. See item 16 at
http://us2.metamath.org:8888/award2003.html"))
      (dist (!x !y))
      (ass [¬ ∀ x ¬ = x y]))

  (ax "ax10" (var ![x y] wff "φ")
      (meta (:comment . "Axiom of Quantifier Substitution.")
            (:description . "One of the 5 equality axioms of predicate calculus. This is a technical axiom wherein the antecedent
is true only if x and y are the same variable, and in this case it doesn't matter which one you use in a quantifier.
Axiom scheme C11' in [Megill] p. 448, ax-10 in set.mm.
<p>It apparently does not otherwise appear in the literature but is easily proved from
textbook predicate calculus by cases. (Strictly speaking, the antecedent is also true when x and y are different variables in the
case of a one-edement domain of discourse, but then the consequent is also true in a one-edement domain. For compatibility with
traditional predicate calculus all our predicate calculus axioms hold in a one-element domain, but this becomes unimportant in
set theory where we show that at least 2 things exist.)"))
      (ass [→ ∀ x = x y → ∀ x φ ∀ y φ]))

  (ax "ax11" (var "x" set "a" wff "φ")
      (meta (:comment . "Axiom of Variable Substitution.")
            (:description . "One of the 5 equality axioms of predicate calculus. The consequent
Ax (x = y -> ph) is a way of expressing \"y substituted for x in wff ph\". Variant of axiom scheme C15' in [Megill] p. 448
with a distinct variable condition instead of a distinctor antecedent. It is based on Lemma 16 of [Tarski] p. 70 and Axiom C8
of [Monk2] p. 105, from which it can be proved by cases.
<p>Juha Arpiainen proved the independence of this axiom from the others on 19-Jan-2006. See item 9a at
http://us2.metamath.org:8888/award2003.html.
<p>Interestingly, if the wff expression substituted for ph contains no wff variables, the resulting statement /can/ be proved
without invoking this axiom. This means that even though this axiom is /metalogically/ independent from the others, it is not
/logically/ independent."))
      (dist (!x !a))
      (ass [→ = x a → φ ∀ x → = x a φ]))

  (ax "ax12" (var ![x y z])
      (meta (:comment . "Axiom of Quantifier Introduction.")
            (:description . "One of the 5 equality axioms of predicate calculus. Informally, it says that whenever
z is distinct from x and y, and x = y is true, then x = y quantified with z is also true. In other words, z is
irrelevant to the truth of x = y. Axiom scheme C9' in [Megill] p. 448. It apparently does not otherwise appear in
the literature but is easily proved from textbook predicate calculus by cases."))
      (ass [→ ¬ ∀ z = z x → ¬ ∀ z = z y → = x y ∀ z = x y]))

  (ax "ax16" (var "x" set "y" wff "φ")
      (meta (:comment . "Axiom of Distinct Variables (1)."))
      (dist (!x !y))
      (ass [→ ∀ x = x y → φ ∀ x φ]))

  (ax "ax17" (var "x" wff "φ")
      (meta (:comment . "Axiom of Distinct Variables (2)."))
      (dist (!x !φ))
      (ass [→ φ ∀ x φ]))

;; Define proper substitution
;; [sb y x φ] == the wff resulting from proper substitution of y for x in φ

  (def "df-sb" wff "sb" (set "y" var "x" wff "φ") (var "z")
    [∃ z ∧ = z y ∃ x ∧ = x z φ]
    (meta (:comment . "Proper Substitution.")
          (:description . "This definition (sb7 in set.mm) is adopted instead of Metamath's
because it works for any set term y.")))
;          (:html-sym . "stopsign")
;          (:html-fn . #'(lambda (tree stream)
;                          (format stream "[")
;                          (html-dump-symtree (fourth tree) stream)
;                          (format stream "/")
;                          (html-dump-symtree (third tree) stream)
;                          (format stream "]")
;                          (html-dump-symtree (second tree) stream)))))
) ; End "equ-ax"
