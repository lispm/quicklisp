;;;; -*- Mode: Lisp -*-

;;;; foreach.lisp --
;;;;
;;;; See file COPYING for copyright and licensing information.


(in-package "CL.EXTENSIONS.DACF.ENUMERATIONS")

;;;; foreach --
;;;; Of course it'd be better to do a DEFINE-LOOP-PATH or whatever.  But this ain't standard.
;;;; Note also that declarations are not properly handled.

(defmacro foreach ((var object &rest keys) &body forms-and-clauses)
  "Simplified iteration construct over an `enumerable' object.

FOREACH is a thin macro over LOOP and it mixes, in a hybrid, and maybe
not so beautiful, style the DOTIMES/DOLIST shape with LOOP clauses.

FORMS-AND-CLAUSES are executed in an environment where VAR is bound to the
elements of the result of (APPLY #'ENUM:ENUMERATE OBJECT KEYS).

If KEYS contains a non-NIL :REVERSE keyword, then the enumeration must
be bi-directional and PREVIOUS and HAS-PREVIOUS-P will be used to
traverse it.  :REVERSE and its value will be still passed to the
embedded call to ENUMERATE.  This is obviously only useful only for
enumerations that can start \"in the middle\".

FORMS-AND-CLAUSES can start with some declarations and then continue
with either regular forms or LOOP clauses.  After the first LOOP
clause appears in FORMS-AND-CLAUSES, standard LOOP rules should be
followed.

FOREACH returns whatever is returned by FORMS-AND-CLAUSES according to
standard LOOP semantics.


Examples:

;;; Here are some examples of FOREACH.

cl-prompt> (setf le (enumerate '(1 2 3)))
#<CONS enumeration ...>
 
cl-prompt> (foreach (i le) (print i))
1
2
3
NIL

;;; Of course, FOREACH is smarter than that:

cl-prompt> (foreach (i (vector 'a 's 'd))
             (declare (type symbol i))
             (print i))
A
S
D
NIL

;;; Apart from declarations, FOREACH is just a macro built on top of
;;; LOOP, therefore you can leverage all the LOOP functionality.

cl-prompt> (foreach (i '(1 2 3 4))
             (declare (type fixnum i))
             when (evenp i)
               collect i)
(2 4)

;;; While this creates an admittedly strange hybrid between the
;;; standard DO... operators and LOOP, it does serve the purpose. The
;;; right thing would be to have a standardized way of extending LOOP.
;;; Alas, there is no such luxury.

;;; Finally an example of a reverse enumeration:

cl-prompt> (foreach (i (vector 1 2 3 4 5 6 7 8) :reverse t :start 4)
               when (evenp i)
                 collect i)
(4 2)
"
  (flet ((decl-form-p (form)
           (and (consp form) (eq (first form) 'declare)))
         )
    (let* ((e-var (gensym "ENUMERATION-VAR-"))
           (decls (remove-if (complement #'decl-form-p) forms-and-clauses))
           (forms-only (remove-if #'decl-form-p forms-and-clauses))
           (from-end-p (getf keys :from-end nil))
           (test (if from-end-p 'has-previous-p 'has-next-p))
           (step (if from-end-p 'previous 'next))
           )
    
      `(let* ((,e-var (enumerate ,object ,@keys))
              (,var (current ,e-var))
              )
         ,@decls
         (loop while (,test ,e-var)
               do (setf ,var (,step ,e-var))
               ,@forms-only ; This trick allows us to reuse the full LOOP forms.
               )))))

;;;; end of file -- foreach.lisp --
