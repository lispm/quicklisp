(in-package :cl-user)

(defpackage :bourbaki
  (:use :common-lisp)
  (:shadow
    #:export
    #:import
    #:provide
    #:require)
  
  (:export
    ;; context.lisp
    ; #:seek-ctx-imports
    ; #:seek-ctx-exports
    #:seek-sym
    #:create-context
    #:seek-create-ctx
    #:insert-sym
    #:insert-import
    #:insert-export
    #:wrap-ctx
    #:mkcontext
    #:flush
    #:require
    #:symtree-parse
    ; #:theorem-sym-p
    ; #:parse-thr-ref
    ; #:filter-syms

    ;; define.lisp
    #:def
    #:alias
    #:pattern

    ;; distinct.lisp
    #:dvc-list
    #:dvc-var

    ;; metatheorem.lisp
    #:metath
    #:virtual-metath
    #:extend-metath

    ;; parse.lisp
    #:in-context
    #:module
    #:local
    
    #:defcontext
    #:copy-th

    #:th
    #:ax ; #:def is in define.lisp
    #:theory

    ; #:parse-varspec
    #:parse-vars
    #:symkind
    #:set-symkind-eq-op

    #:ass
    #:hypo ; #:proof is in parse-proof.lisp
    #:dist

    #:meta

    #:prim
    #:var
    #:var2
    #:loc

    #:providing
    #:import
    #:export

    #:th-pattern

    ;; parse-proof.lisp
    #:parse-subproof
    #:subproof
    #:linear-subproof
    #:proof
    #:line
    ; #:hypo ; also in parse.lisp
    ; #:ass

    ;; print.lisp
    #:print-symtree
    #:print-theorem
    #:show-proof

    ;; read.lisp
;    #:bsym-reader
;    #:symstr-reader
;    #:thref-reader

    ;; symbol.lisp
    #:subkindp
    #:mkvar
    ; #:*mkvar-counter*
    ; #:theorem-sym-p

    ;; symtree.lisp
    #:replace-vars
    #:wff-type
    #:wffp
    #:wff-equal
    ; #:collect-vars
    #:makevarlist
    #:smap

    ;; symref.lisp
    ; #:compose-proof
    ; #:compose-ref
    ; #:apply-ref

    ;; types.lisp
    ;; symbol kind
    #:symkind
;    #:make-symkind
;    #:symkind-eq-op ; TODO: delete these
;    #:symkind-super
;    #:symkind-name
    ;; context
    #:context
    #:make-context
    #:context-syms
    #:context-imports
    #:context-exports
    #:context-type
    #:context-class
    #:context-meta
    #:context-name
    #:context-vars
    #:context-distinct
    #:context-hypo
    #:context-assert
    #:context-proof
    ;; symbol reference
    #:symref
    #:make-symref
    #:symref-target
    #:symref-args-needed
    #:symref-fn
    #:symref-proof
    ;; subproof
    #:subproof
    #:make-subproof
    #:subproof-hypo
    #:subproof-assert
    #:subproof-prev
    #:subproof-ref
    #:subproof-sub
    ;; pattern tree
    #:pattern-tree
    #:make-pattern-tree
    #:pattern-tree-ref
    #:pattern-tree-syms

    ;; util.lisp
    #:it
    ; #:pair-eq
    ; #:cartesian-product
    ; #:push-second
    #:flatten
    #:mklist
    #:load-aux
    #:aif
    #:acond
    #:with-gensyms
    #:collecting
    #:collect

    ;; variables.lisp
    ; #:mkrootctx
    #:*root-context*
    #:*current-context*
    #:*meta-context*
    #:*current-proof*
    #:*bourbaki-debug*
    #:*bourbaki-library-path*
    #:*theorem-patterns*
    #:*bourbaki-version*

    ;; verify.lisp
    #:verify
    ; #:verify-1
    ; #:verify-subproof

    ;; stats.lisp
    #:collect-stats
    #:show-stats
    #:show-unused

    ;; proof.lisp
    #:provenp
    ; #:do-proof
    ; #:walk-proof-tree

    ;; pattern.lisp
    #:match-tree
    #:in-tree
    #:insert-pat
    #:insert-wff
    #:let-once
    #:if-match
    #:match-case
    ))

(defpackage :bourbaki-user
  (:use :common-lisp :bourbaki)
  (:shadowing-import-from :bourbaki #:export #:import #:provide #:require))
