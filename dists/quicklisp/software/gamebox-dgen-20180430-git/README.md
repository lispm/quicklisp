# gamebox-dgen

A procedural dungeon generation library.

![Example](https://files.lispcoder.net/images/projects/gamebox-dgen/example1.png)

## Overview

This library can be used to generate a random dungeon for use in game development.

## Install

``` lisp
(ql:quickload :gamebox-dgen)
```

## Usage

If you just want to generate the map data:

``` lisp
(box.dgen:make-stage)
```

If you want to preview the generated map, drawing it with unicode characters in your REPL:

``` lisp
(box.dgen:print-stage (box.dgen:make-stage))
```

The result will look something like this:

![Example](https://files.lispcoder.net/images/projects/gamebox-dgen/example2.png)

`MAKE-STAGE` takes several optional arguments to influence the generator.

`:SEED` specifies the random seed integer to use. This makes it possible to regenerate the same
dungeon at a later time. If this is not specified, it will be chosen randomly.

`:WIDTH` specifies the width of the stage in cells.

`:HEIGHT` specifies the height of the stage in cells.

`:CORRIDOR-WINDINESS` specifies how windy corridors will be, as a range from 0.0 to 1.0. This has
minimal effect for densely packed rooms - see `:ROOM-DENSITY` below to control this. Default value
is 0.0.

`:ROOM-DENSITY` specifies how densely rooms should be packed in the stage, as a range from 0.1 (10%)
to 1.0 (100%). Note: This is only an estimation. Default value is 0.65.

`:ROOM-SIZE-MIN` specifies the minimum size in cells the width or height of a room is allowed to be,
within the range 3 to 99. Note: This should be supplied as an odd integer value, else it will be
incremented by 1. Default value is 3.

`:ROOM-SIZE-MAX` specifies the maximum size in cells the width or height of a room is allowed to be,
within the range `ROOM-SIZE-MIN` to 99. Note: This should be supplied as an odd integer value, else
it will be incremented by 1. Default value is 11.

`:DOOR-RATE` specified the percentage of junctions that should be doors, as a range from 0.0 to 1.0.
A junction is an intersection between a room and a corridor. Default value is 0.5.

All of the data you need to render a map is located in the `GRID` slot of the returned stage data
object. This is an array of `CELL` objects, each having the following slots:

`X`: The X location in the map.

`Y`: The Y location in the map.

`CARVED`: Determines whether this cell is ground that can be walked upon.

`REGION-ID`: An integer representing the region. A region is a group of adjacent cells. Each room's
cells are of the same `REGION-ID`. Likewise, a corridor between two rooms (or more, in the case of
branching) is of the same `REGION-ID`. You can think of a `REGION-ID` as belonging to a set of cells
as if it was flood-filled, stopping at junctions (what this library calls doors or whatever your
game may define them as).

`FEATURES`: A list of symbols identifying a special cell property, if any. Currently, this can be
one or more of the following:

* `:JUNCTION`: The cell joins 2 unique regions.
* `:DOOR`: A junction has a chance of becoming a door.
* `:STAIRS-UP`: The cell with an entrance staircase.
* `:STAIRS-DOWN`: The cell with an exit staircase.
* `:ROOM`: The cell is part of a room.
* `:CORRIDOR`: The cell is part of a corridor.
* `:WALL`: The cell is part of a wall.

## License

Copyright © 2015-2018 [Michael Fiano](mailto:mail@michaelfiano.com).

Licensed under the MIT License.
