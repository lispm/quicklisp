(in-package :cl-user)

(defpackage :bourbaki-system 
  (:use :cl :asdf))

(in-package :bourbaki-system)
  
(asdf:defsystem :bourbaki
    :version "3.5"
    :components
    (
     (:module bourbaki
              :pathname ""
              :components ((:file "package")

                           ;; Helper files (types, utilities, ...)
                           (:file "types" :depends-on ("package"))
                           (:file "variables" :depends-on ("types"))
                           (:file "symbol" :depends-on ("types"))
                           (:file "util" :depends-on ("variables"))
                           (:file "symref" :depends-on ("types" "util"))
                           (:file "pattern" :depends-on ("util" "symtree" "symref"))
                           (:file "proof" :depends-on ("pattern"))
                           (:file "print" :depends-on ("types" "util" "variables" "proof"))
                           (:file "distinct" :depends-on ("types" "util"))

                           ;; The "core" files                           
                           (:file "symtree" :depends-on ("symbol" "util"))
                           (:file "context" :depends-on ("types" "variables" "util" "symtree" "print" "symref"))
                           (:file "verify" :depends-on ("symtree" "util" "print" "context" "proof" "distinct"))

                           ;; I/O to other formats
                           (:file "stats" :depends-on ("verify"))
;                           (:file "html" :depends-on ("types" "util" "symtree"))                            

                           ;; Implement syntactical sugar
                           (:file "parse" :depends-on ("context" "symtree" "util" "distinct"))
                           (:file "define" :depends-on ("parse"))
                           (:file "read" :depends-on ("package"))
                           (:file "metatheorem" :depends-on ("context"))
                           (:file "parse-proof" :depends-on ("pattern" "verify"))
			   
			   ))
     ))

