;;;  generator.lisp --- code generator

;;;  Copyright (C) 2005, 2009 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: cobstor

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

(in-package :cobstor-generator)

(defun copy-file-pathname (name)
  "Given a copy file NAME (possibly a pathname) return an
absolute pathname.  The defaults are relative to COBSTOR-DYNAMIC
logial pathname host."
  (make-pathname :defaults
		 (translate-logical-pathname "cobstor-copys:")
		 :name name))

(defun make-agent-and-proxy-stub (name keys input-files package compile force)
  (let ((copy-pathnames (mapcar #'copy-file-pathname input-files))
	(stub (proxy-stub-file-pathname name))
	(agent (cobstor::agent-file-pathname name)))
    (ensure-directories-exist stub)
    (ensure-directories-exist agent)
    (be records (lazy (mapcan #'read-field-declarations copy-pathnames))
      (when (or force
		(outdated-p stub copy-pathnames))
	(format t "~&; Generating proxy stub for ~A~%" name)
	(make-cobol-proxy-stub stub (force records) keys :package package)
	(when compile
	  (compile-proxy-stub stub)))
      (when (or force
		(outdated-p agent copy-pathnames))
	(format t "~&; Generating cobol agent for ~A~%" name)
	(make-cobol-agent name (force records) keys agent)
	(when compile
	  (compile-cobol-agent agent))))))

(defun generate-cobstor-interface (files-list &key package (compile t) force)
  "Generate all the proxy stubs and agent in FILES-LIST.  If
PACKAGE is specified the generated Lisp code will be part of it.
If COMPILE is true (the default) the generated code is compiled
as well.  If FORCE is true the generation takes place even if the
module looks to be up to date."
  (loop
     for (name keys copy-files) in files-list
     do
       (make-agent-and-proxy-stub name keys copy-files package compile force)))
