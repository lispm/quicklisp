;;;  ss.lisp --- Stateful Streams

;;;  Copyright (C) 2004-2006 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-96A058B158688446A0E05CF70C0E700B.lisp,v 1.4 2004/03/25 23:50:31 wcp Exp $

;;;  This program is free software; you can redistribute it and/or
;;;  modify it under the terms of the GNU General Public License as
;;;  published by the Free Software Foundation; either version 2, or
;;;  (at your option) any later version.
;;;  This program is distributed in the hope that it will be useful,
;;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;  General Public License for more details.
;;;  You should have received a copy of the GNU General Public License
;;;  along with this program; see the file COPYING.  If not, write to
;;;  the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;;  Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-96A058B158688446A0E05CF70C0E700B.lisp,v 1.4 2004/03/25 23:50:31 wcp Exp $")

#+(and cmu (not gray-streams))
(eval-when (:compile-toplevel :load-toplevel :execute)
  (ext:without-package-locks
    (load "library:subsystems/gray-streams-library")))

(in-package :binge-ss)

(eval-when (:compile-toplevel :load-toplevel)
  (defstruct (file-offset (:conc-name fpos-))
    line
    column))

(defclass stateful-stream (fundamental-character-input-stream)
  ((column :initform 0
	   :reader ss-column
	   :type integer)
   (line :initform 0
	 :reader ss-line
	 :type integer)
   (real-stream :initarg :stream)
   (bol :type boolean
	:reader ss-bol
	:initform t)))

(defmethod print-object ((obj stateful-stream) stream)
  (with-slots (real-stream) obj
    (format stream "#<Stateful Stream for ~S>" real-stream)))

(defmethod print-object ((obj file-offset) stream)
  (format stream "line ~A col ~A" (fpos-line obj) (fpos-column obj)))

(defmethod npg:later-position ((pos1 file-offset) (pos2 file-offset))
  (or (> (fpos-line pos1) (fpos-line pos2))
      (and (= (fpos-line pos1) (fpos-line pos2))
	   (> (fpos-column pos1) (fpos-column pos2)))))

(defmethod stream-read-char ((stream stateful-stream))
  (with-slots (real-stream line column bol) stream
    (let ((c (read-char real-stream nil :eof)))
      (when bol
	(setf column 0)
	(incf line)
	(setf bol nil))
      (incf column)
      (when (eq c #\newline)
	(setf bol t))
      c)))

(defmethod close ((stream stateful-stream) &key abort)
  (with-slots (real-stream) stream
    (close real-stream :abort abort)))

(defun open-stateful-stream (pathname &rest open-options)
  (make-instance 'stateful-stream :stream (apply #'open pathname open-options)))

(defmacro with-open-stateful-stream ((stream pathname &rest open-options) &body body)
  `(let ((,stream (open-stateful-stream ,pathname ,@open-options)))
     (unwind-protect (progn ,@body)
       (close stream))))

;; Considering that it's forbidden to unread more than one character
;; in a row the BOL flag helps us to keep LINE and COLUMN in a
;; consistent state: LINE is increased only upon returning the first
;; character of the line (after the newline), too late to back up a
;; newline and no need to guess what the previous line's column was.
(defmethod stream-unread-char ((stream stateful-stream) char)
  (with-slots (real-stream line column bol) stream
    (decf column)
    (when (char= char #\newline)
      (setf bol nil))
    (unread-char char real-stream)))

(defmethod stream-peek-char ((stream stateful-stream))
  (with-slots (real-stream) stream
    (peek-char nil real-stream nil :eof)))

(defgeneric get-position (stream)
  (:documentation
   "Return position of stream.  The position may be an object of
the class FILE-OFFSET."))

(defmethod get-position ((stream stateful-stream))
  (with-slots (line column) stream
    (make-file-offset :line line :column column)))
