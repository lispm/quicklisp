;;;  parser.lisp --- tests for the parser part

;;;  Copyright (C) 2004 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-BADE77201C17CABDCED735251C9BA922.lisp,v 1.14 2004/04/08 14:38:34 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-BADE77201C17CABDCED735251C9BA922.lisp,v 1.14 2004/04/08 14:38:34 wcp Exp $")

(in-package :binge-tests)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric sexp-of (obj)
  (:documentation
   "Convert OBJ in a human and machine readable sexp.  This is
used mainly for test purposes."))

(defmethod sexp-of ((obj c-constant-declaration))
  (list :constant (c-name obj) (c-value obj)))

(defmethod sexp-of ((obj c-typedef-declaration))
  (list :typedef (c-name obj) (sexp-of (c-type obj))))

(defmethod sexp-of ((obj c-function))
  (list :function (sexp-of (c-type obj)) (mapcar #'sexp-of (c-arguments obj))))

(defmethod sexp-of ((obj c-function-declaration))
  (list :function (c-name obj) (sexp-of (c-type obj)) (mapcar #'sexp-of (c-arguments obj))))

(defmethod sexp-of ((obj c-struct-declaration))
  (list :struct (if (stringp (c-name obj))
		    (c-name obj)
		    nil)
	(mapcar #'sexp-of (c-slots obj))))

(defmethod sexp-of ((obj c-union-declaration))
  (list :union (if (stringp (c-name obj))
		   (c-name obj)
		   nil)
	(mapcar #'sexp-of (c-slots obj))))

(defmethod sexp-of ((obj c-enum-declaration))
  (list :enum (if (stringp (c-name obj))
		  (c-name obj)
		  nil)
	(c-constants obj)))


(defmethod sexp-of ((obj c-variable-declaration))
  (list :variable (c-name obj) (sexp-of (c-type obj))))

(defmethod sexp-of ((obj c-typed-name))
  (list (sexp-of (c-type obj)) (c-name obj)))

(defmethod sexp-of ((obj c-argument))
  (list (sexp-of (c-type obj)) (c-name obj)))

(defmethod sexp-of ((obj c-pointer))
  (list :pointer (sexp-of (c-type obj))))

(defmethod sexp-of ((obj c-array))
  (list :array (sexp-of (c-type obj)) (c-dimensions obj)))

(defmethod sexp-of ((obj c-user-type-ref))
  (list :user-type (c-ref-name obj)))

(defmethod sexp-of ((obj c-struct-ref))
  (list :struct (c-ref-name obj)))

(defmethod sexp-of ((obj c-union-ref))
  (list :union (c-ref-name obj)))

(defmethod sexp-of ((obj c-enum-ref))
  (list :enum (c-ref-name obj)))

(defmethod sexp-of ((obj symbol))
  (list obj))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun efs (string)
  "Extract From String.  It wraps binge:extracts returning a list
of sexps that can be reliably compared in the regression tests."
  (mapcar #'sexp-of (binge:extract string)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-basic-type-variable.1
    (efs "int a;")
  ((:variable "a" (:int))))

(deftest parser-basic-type-variable.2
    (efs "extern char l;")
  ((:variable "l" (:char))))

(deftest parser-basic-type-variable.3
    (efs "long cOnst;")
  ((:variable "cOnst" (:long))))

(deftest parser-basic-type-variable.4
    (efs "extern float floaT;")
  ((:variable "floaT" (:float))))

(deftest parser-basic-type-variable.5
    (efs "double _0a;")
  ((:variable "_0a" (:double))))

(deftest parser-basic-type-variable.6
    (efs "long double _0_1;")
  ((:variable "_0_1" (:long-double))))

(deftest parser-basic-type-variable.8
    (efs "unsigned long int Long;")
  ((:variable "Long" (:unsigned-long))))

(deftest parser-basic-type-variable.9
    (efs "unsigned int ___;")
  ((:variable "___" (:unsigned-int))))

(deftest parser-basic-type-variable.10
    (efs "unsigned uNsIgNeD;")
  ((:variable "uNsIgNeD" (:unsigned-int))))

(deftest parser-basic-type-variable.11
    (efs "signed lonG;")
  ((:variable "lonG" (:int))))

(deftest parser-basic-type-variable.12
    (efs "signed int __a__;")
  ((:variable "__a__" (:int))))

(deftest parser-basic-type-variable.13
    (efs "signed long a;")
  ((:variable "a" (:long))))

(deftest parser-basic-type-variable.14
    (efs "short a;")
  ((:variable "a" (:short))))

(deftest parser-basic-type-variable.15
    (efs "signed short a;")
  ((:variable "a" (:short))))

(deftest parser-basic-type-variable.16
    (efs "unsigned short a;")
  ((:variable "a" (:unsigned-short))))

(deftest parser-basic-type-variable.17
    (efs "unsigned long a;")
  ((:variable "a" (:unsigned-long))))

(deftest parser-basic-type-variable.18
    (efs "signed char a;")
  ((:variable "a" (:char))))

(deftest parser-basic-type-variable.19
    (efs "unsigned char a;")
  ((:variable "a" (:unsigned-char))))

(deftest parser-basic-type-variable.20
    (efs "static long double x;")
  nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-pointer-variable.1
    (efs "char *a;")
  ((:variable "a" (:pointer (:char)))))

(deftest parser-pointer-variable.2
    (efs "char **a;")
  ((:variable "a" (:pointer (:pointer (:char))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-array-variable.1
    (efs "char a[10];")
  ((:variable "a" (:array (:char) (10)))))

(deftest parser-array-variable.2
    (efs "extern short a[10][20];")
  ((:variable "a" (:array (:short) (10 20)))))

(deftest parser-array-variable.3
    (efs "double a[10][20][30][];")
  ((:variable "a" (:array (:double) (10 20 30 nil)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-user-type-variable.1
    (efs "foo a;")
  ((:variable "a" (:user-type "foo"))))

(deftest parser-user-type-variable.2
    (efs "struct foo a;")
  ((:variable "a" (:struct "foo"))))

(deftest parser-user-type-variable.3
    (efs "union foo a;")
  ((:variable "a" (:union "foo"))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-function.1
    (efs "int f();")
  ((:variable "f" (:function (:int) nil))))

(deftest parser-function.2
    (efs "extern int f(void);")
  ((:variable "f" (:function (:int) nil))))

(deftest parser-function.3
    (efs "int f(char *s);")
  ((:variable "f" (:function (:int) (((:pointer (:char)) "s"))))))

(deftest parser-function.4
    (efs "extern const char *f(int a, long);")
  ((:variable "f" (:function (:pointer (:char)) (((:int) "a") ((:long) nil))))))

(deftest parser-function.5
    (efs "extern mytype f(mytype x);")
  ((:variable "f" (:function (:user-type "mytype") (((:user-type "mytype") "x"))))))

(deftest parser-function.6
    (efs "int f(char *fmt, ...);")
  ((:variable "f" (:function (:int) (((:pointer (:char)) "fmt") ((:vararg) nil))))))

(deftest parser-function.7
    (efs "int f(int (*fmt)(void), ...);")
  ((:variable "f" (:function (:int) (((:pointer (:function (:int) nil)) "fmt") ((:vararg) nil))))))

(deftest parser-function.8
    ;; function accepting a int and returning a pointer to a function
    ;; accepting a char and returning a long
    (efs "extern long (*(f)(int x))(char);")
  ((:variable "f" (:function (:pointer (:function (:long) (((:char) nil)))) (((:int) "x"))))))

;; static functions should be skipped
(deftest parser-function.9
    (efs "static int *f(const char *fmt, ...);")
  nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-function-pointer-variable.1
    (efs "extern char (*a)();")
  ((:variable "a" (:pointer (:function (:char) nil)))))

(deftest parser-function-pointer-variable.2
    (efs "void (*a)(void);")
  ((:variable "a" (:pointer (:function (:void) nil)))))

(deftest parser-function-pointer-variable.3
    (efs "extern void *(*a)(void *x);")
  ((:variable "a" (:pointer (:function (:pointer (:void)) (((:pointer (:void)) "x")))))))

(deftest parser-function-pointer-variable.4
    (efs "foo (*f) (char *fmt, ...);")
  ((:variable "f" (:pointer (:function (:user-type "foo") (((:pointer (:char)) "fmt") ((:vararg) nil)))))))

(deftest parser-function-pointer-variable.5
    (efs "long (*(*f)(int))(char);")
  ((:variable "f" (:pointer (:function (:pointer (:function (:long) (((:char) nil)))) (((:int) nil)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-variables-group.1
    (efs "int a, b, c;")
  ((:variable "a" (:int))
   (:variable "b" (:int))
   (:variable "c" (:int))))

(deftest parser-variables-group.2
    (efs "unsigned int a, b, c;")
  ((:variable "a" (:unsigned-int))
   (:variable "b" (:unsigned-int))
   (:variable "c" (:unsigned-int))))

(deftest parser-variables-group.3
    (efs "unsigned long a, b, c;")
  ((:variable "a" (:unsigned-long))
   (:variable "b" (:unsigned-long))
   (:variable "c" (:unsigned-long))))

(deftest parser-variables-group.4
    (efs "extern long long a, b;")
  ((:variable "a" (:long-long))
   (:variable "b" (:long-long))))

(deftest parser-variables-group.5
    (efs "char a, *b, **c, *d, e;")
  ((:variable "a" (:char))
   (:variable "b" (:pointer (:char)))
   (:variable "c" (:pointer (:pointer (:char))))
   (:variable "d" (:pointer (:char)))
   (:variable "e" (:char))))

(deftest parser-variables-group.6
    (efs "int a, *b, **c[10][20][];")
  ((:variable "a" (:int))
   (:variable "b" (:pointer (:int)))
   (:variable "c" (:array
		   (:pointer
		    (:pointer (:int)))
		   (10 20 nil)))))

(deftest parser-variables-group.7
    (efs "int (*f1)(char, long), *(**f2)(float), **(*f3[4][5])(double);")
  ((:variable "f1" (:pointer
		    (:function (:int)
			       (((:char) nil)
				((:long) nil)))))
   (:variable "f2" (:pointer
		    (:pointer
		     (:function (:pointer (:int))
				(((:float) nil))))))
   (:variable "f3" (:array
		    (:pointer
		     (:function (:pointer (:pointer (:int)))
				(((:double) nil))))
		    (4 5)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-struct.1
    (efs "struct s { int a; };")
  ((:struct "s" (((:int) "a")))))

(deftest parser-struct.2
    (efs "struct s { int a, b; long c, d; };")
  ((:struct "s" (((:int) "a")
		 ((:int) "b")
		 ((:long) "c")
		 ((:long) "d")))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-enum.1
    (efs "enum e { a, b, c, d };")
  ((:enum "e" ("a" "b" "c" "d"))))

(deftest parser-enum.2
    (efs "enum e { a };")
  ((:enum "e" ("a"))))

(deftest parser-enum.3
    (efs "enum e { a, b = 13, c, d = 2 };")
  ((:enum "e" ("a" ("b" 13) "c" ("d" 2)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-enum-variable.1
    (efs "enum { a } v;")
  ((:variable "v" (:enum nil ("a")))))

(deftest parser-enum-variable.2
    (efs "enum { a, b=10, c } v;")
  ((:variable "v" (:enum nil ("a" ("b" 10) "c")))))

(deftest parser-enum-variable.3
    (efs "enum e { a, b, c } v;")
  ((:variable "v" (:enum "e" ("a" "b" "c")))))

(deftest parser-enum-variable.4
    (efs "enum e { a, b, c } v1, v2, *v3, *v4[10];")
  ((:variable "v1" (:enum "e" ("a" "b" "c")))
   (:variable "v2" (:enum "e" ("a" "b" "c")))
   (:variable "v3" (:pointer (:enum "e" ("a" "b" "c"))))
   (:variable "v4" (:array (:pointer (:enum "e" ("a" "b" "c"))) (10)))))

(deftest parser-enum-variable.5
    (efs "enum { a, b, c } v1, v2, *v3, v4[10], *v5[10][];")
  ((:variable "v1" (:enum nil ("a" "b" "c")))
   (:variable "v2" (:enum nil ("a" "b" "c")))
   (:variable "v3" (:pointer (:enum nil ("a" "b" "c"))))
   (:variable "v4" (:array (:enum nil ("a" "b" "c")) (10)))
   (:variable "v5" (:array (:pointer (:enum nil ("a" "b" "c"))) (10 nil)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-union.1
    (efs "union s { int a; };")
  ((:union "s" (((:int) "a")))))

(deftest parser-union.2
    (efs "union s { int a, b; long c, d; };" )
  ((:union "s" (((:int) "a")
		((:int) "b")
		((:long) "c")
		((:long) "d")))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-struct-variable.1
    (efs "struct { int a; } var;")
  ((:variable "var" (:struct nil (((:int) "a"))))))

(deftest parser-struct-variable.2
    (efs "struct str { int a; } var;")
  ((:variable "var" (:struct "str" (((:int) "a"))))))

(deftest parser-struct-variable.3
    (efs "struct str { int a; struct str *next; } var1, *var2;")
  ((:variable "var1" (:struct "str"
		       (((:int) "a")
			((:pointer (:struct "str")) "next"))))
   (:variable "var2" (:pointer
	       (:struct "str"
			(((:int) "a") ((:pointer (:struct "str")) "next")))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-union-variable.1
    (efs "union { int a; } var;")
  ((:variable "var" (:union nil
			    (((:int) "a"))))))

(deftest parser-union-variable.2
    (efs "union str { int a; } var;")
  ((:variable "var" (:union "str"
			    (((:int) "a"))))))

(deftest parser-union-variable.3
    (efs "union str { int a; union str *next; } var;")
  ((:variable "var" (:union "str"
		      (((:int) "a")
		       ((:pointer (:union "str")) "next"))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-basic-typedef.1
    (efs "typedef int mytype;")
  ((:typedef "mytype" (:int))))

(deftest parser-basic-typedef.2
    (efs "typedef unsigned long mytype;")
  ((:typedef "mytype" (:unsigned-long))))

(deftest parser-basic-typedef.3
    (efs "typedef unsigned long mytype, *pmytype, *apmytype[];")
  ((:typedef "mytype" (:unsigned-long))
   (:typedef "pmytype" (:pointer (:unsigned-long)))
   (:typedef "apmytype" (:array (:pointer (:unsigned-long)) (nil)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-typedef.1
    (efs "typedef struct str *mystr;")
  ((:typedef "mystr" (:pointer (:struct "str")))))

(deftest parser-typedef.2
    (efs "typedef union un *myun;")
  ((:typedef "myun" (:pointer (:union "un")))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-struct-typedef.1
    (efs "typedef struct { int a; } mystr;")
  ((:typedef "mystr" (:struct nil (((:int) "a"))))))

(deftest parser-struct-typedef.2
    (efs "typedef struct str { int a; struct str *next; } mystr;")
  ((:typedef "mystr"
	     (:struct "str"
		      (((:int) "a")
		       ((:pointer (:struct "str")) "next"))))))

(deftest parser-struct-typedef.3
    (efs "typedef struct { int a, b; foo *c; } mystr;")
  ((:typedef "mystr"
	     (:struct nil
		      (((:int) "a")
		       ((:int) "b")
		       ((:pointer (:user-type "foo")) "c"))))))

(deftest parser-struct-typedef.4
    (efs "typedef struct { int a; } *mystr;")
  ((:typedef "mystr"
	     (:pointer
	      (:struct nil
		       (((:int) "a")))))))

(deftest parser-struct-typedef.5
    (efs "typedef struct { int a; } *mystr[5];")
  ((:typedef "mystr"
	     (:array
	      (:pointer
	       (:struct nil
			(((:int) "a"))))
	      (5)))))

(deftest parser-struct-typedef.6
    (efs "typedef struct { int a; struct { char c; } c, *d; } mystruct;")
  ((:typedef "mystruct"
	     (:struct nil
		      (((:int) "a")
		       ((:struct nil (((:char) "c"))) "c")
		       ((:pointer (:struct nil (((:char) "c")))) "d"))))))

(deftest parser-struct-typedef.7
    (efs "typedef struct { int a; union { char c; int d; } e, *f; } mystruct;")
  ((:typedef "mystruct"
	     (:struct nil
		      (((:int) "a")
		       ((:union nil (((:char) "c") ((:int) "d"))) "e")
		       ((:pointer
			 (:union nil (((:char) "c") ((:int) "d")))) "f"))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-array-typedef.1
    (efs "typedef int mystring[5];")
  ((:typedef "mystring" (:array (:int) (5)))))

(deftest parser-array-typedef.2
    (efs "typedef char *mystring[5];")
  ((:typedef "mystring" (:array (:pointer (:char)) (5)))))

(deftest parser-array-typedef.3
    (efs "typedef struct { int a; union { char c; int d; } e, *f; } *mystruct[4][];")
  ((:typedef "mystruct"
	     (:array
	      (:pointer
	       (:struct nil
			(((:int) "a")
			 ((:union nil (((:char) "c") ((:int) "d"))) "e")
			 ((:pointer (:union nil (((:char) "c") ((:int) "d")))) "f"))))
	      (4 nil)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-pointer-typedef.1
    (efs "typedef char *mystring;")
  ((:typedef "mystring" (:pointer (:char)))))

(deftest parser-pointer-typedef.2
    (efs "typedef int * const *mystring;")
  ((:typedef "mystring" (:pointer (:pointer (:int))))))

(deftest parser-pointer-typedef.3
    (efs "typedef struct { int a; } (*myfun)(char *b);")
  ((:typedef "myfun"
	     (:pointer
	      (:function (:struct nil (((:int) "a")))
			 (((:pointer (:char)) "b")))))))

(deftest parser-pointer-typedef.4
    (efs "typedef struct { int a; union { char c; int d; } e, *f; } **mystruct;")
  ((:typedef "mystruct"
	     (:pointer
	      (:pointer
	       (:struct nil
			(((:int) "a")
			 ((:union nil (((:char) "c") ((:int) "d"))) "e")
			 ((:pointer (:union nil (((:char) "c") ((:int) "d")))) "f"))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-function-typedef.1
    (efs "typedef int myfun(char *, long);")
  ((:typedef "myfun"
	     (:function (:int)
			(((:pointer (:char)) nil)
			 ((:long) nil))))))

(deftest parser-function-typedef.2
    (efs "typedef unsigned myfun(short, ...);")
  ((:typedef "myfun" (:function (:unsigned-int) (((:short) nil) ((:vararg) nil))))))

(deftest parser-function-typedef.3
    (efs "typedef signed short myfun(char *(*f)(int, ...), unsigned, ...);")
  ((:typedef "myfun"
	     (:function (:short)
			(((:pointer
			   (:function (:pointer (:char))
				      (((:int) nil) ((:vararg) nil))))
			  "f")
			 ((:unsigned-int) nil) ((:vararg) nil))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-enum-typedef.1
    (efs "typedef enum { DB_BTREE, DB_HASH, DB_RECNO } DBTYPE;")
  ((:typedef "DBTYPE" (:enum nil ("DB_BTREE" "DB_HASH" "DB_RECNO")))))

(deftest parser-enum-typedef.2
    (efs "typedef enum e { a, b = 3, c } et;")
  ((:typedef "et" (:enum "e" ("a" ("b" 3) "c")))))

(deftest parser-enum-typedef.3
    (efs "typedef enum { a = 4, b = 3, c = 2 } et;")
  ((:typedef "et" (:enum nil (("a" 4) ("b" 3) ("c" 2))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-function-definition.1
    (efs "extern int f() { blah blah blah }")
  ((:function "f" (:int) nil)))

(deftest parser-function-definition.2
    (efs "int *f(const char *fmt, ...) { blah blah blah }")
  ((:function "f" (:pointer (:int)) (((:pointer (:char)) "fmt") ((:vararg) nil)))))

;; static functions should be skipped
(deftest parser-function-definition.3
    (efs "static int *f(const char *fmt, ...) { @#!%$^&%@#& }")
  nil)

(deftest parser-function-definition.4
    (efs "struct { int x; } *f() {...}")
  ((:function "f" (:pointer (:struct nil (((:int) "x")))) nil)))

(deftest parser-function-definition.5
    (efs "static struct { int x; } *f() { die die die! }")
  nil)

(deftest parser-function-definition.6
    (efs "union { int x, y, *z; } **f() { some random rubbish }")
  ((:function "f" (:pointer (:pointer (:union nil (((:int) "x")
						   ((:int) "y")
						   ((:pointer (:int)) "z"))))) nil)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-forward-reference.1
    (efs "struct foo;")
  ((:struct "foo" ())))

(deftest parser-forward-reference.2
    (efs "union bar;")
  ((:union "bar" ())))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest parser-constant.1
    (efs "const \"foo\" = 10;")
  ((:constant "foo" 10)))

(deftest parser-constant.2
    (efs "const \"foo\" = 1U;")
  ((:constant "foo" 1)))

(deftest parser-constant.3
    (efs "const \"foo\" = 0x1;")
  ((:constant "foo" 1)))

(deftest parser-constant.4
    (efs "const \"foo\" = 0x10;")
  ((:constant "foo" 16)))

(deftest parser-constant.5
    (efs "const \"foo\" = 010;")
  ((:constant "foo" 8)))

(deftest parser-constant.6
    (efs "const \"foo\" = -22;")
  ((:constant "foo" -22)))

(deftest parser-constant.7
    ;; this is a nonsense but syntactically correct
    (efs "const \"foo\" = (-0U);")
  ((:constant "foo" 0)))

(deftest parser-constant.8
    (efs "const \"foo\" = (-1234L);")
  ((:constant "foo" -1234)))

