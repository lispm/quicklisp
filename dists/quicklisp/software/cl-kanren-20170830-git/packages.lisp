;; Copyright © 2016, cage
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:

;;     * Redistributions of source code must retain the above copyright
;; notice, this list of conditions and the following disclaimer.
;;     * Redistributions in binary form must reproduce the above copyright
;; notice, this list of conditions and the following disclaimer in the
;; documentation and/or other materials provided with the distribution.

;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
;; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
;; THE POSSIBILITY OF SUCH DAMAGE.

(defpackage :mu-kanren
  (:use :cl
	:alexandria)
  (:export
   :+succeed+
   :+fail+
   :+failed-unification+
   :failed-unification-p
   :lambda-g
   :lambda-$
   :+empty-state+
   :unit
   :+mzero+
   :mu-var
   :id
   :mu-var
   :mu-var-p
   :mu-var=
   :var-exists-in-subst-p
   :walk
   :extend-subst
   :unify-impl
   :equivp
   :unify
   :call/fresh
   :==
   :mplus
   :bind
   :conj
   :disj))

(defpackage :interface
  (:use :cl
	:alexandria
	:mu-kanren)
  (:export
   :equivp
   :unify-impl
   :walk-impl
   :reify-subst-impl
   :reify-subst
   :walk*
   :reify-name))

(defpackage :mu-kanren-goodies
  (:use :cl
	:alexandria
	:mu-kanren
	:interface)
  (:export
   :zzz
   :+succeed+
   :+fail+
   :conj-plus
   :disj-plus
   :fresh
   :run
   :run*
   :else
   :conde
   :ifte
   :once))

(defpackage :mini-kanren
  (:use :cl
	:alexandria
	:mu-kanren
	:interface
	:mu-kanren-goodies)
  (:export
   :+succeed+
   :+fail+
   :fresh
   :run
   :run*
   :else
   :equivp
   :unify-impl
   :==
   :project
   :conde
   :conda
   :condu
   :condi ;; actually an alias for 'conde'
   :==-check
   ;;;basic queries
   :nullo
   :conso
   :caro
   :cdro
   :pairo
   :eq-caro
   :listo
   :membero
   :appendo
   :secondo
   :thirdo
   :brancho
   :flatteno
   :all
   :alli
   ;;;lib-functions
   :choice-case
   :map-choice
   :make-nary-relation
   :permute-binary-relation
   :make-binary-relation
   :permute-ternary-relation
   :make-ternary-relation))

(defpackage :cl-kanren
  (:use :cl
	:mini-kanren)
  (:export
   :+succeed+
   :+fail+
   :fresh
   :run
   :run*
   :else
   :equivp
   :unify-impl
   :==
   :project
   :conde
   :conda
   :condu
   :condi ;; actually an alias for 'conde'
   :==-check
   ;;;basic queries
   :nullo
   :conso
   :caro
   :cdro
   :pairo
   :eq-caro
   :listo
   :membero
   :appendo
   :secondo
   :thirdo
   :brancho
   :flatteno
   :all
   :alli
   ;;;lib-functions
   :choice-case
   :map-choice
   :make-nary-relation
   :permute-binary-relation
   :make-binary-relation
   :permute-ternary-relation
   :make-ternary-relation))
