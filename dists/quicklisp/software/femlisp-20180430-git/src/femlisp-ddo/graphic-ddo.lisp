(in-package :fl.graphic)

(defmethod graphic-file-name (object (program (eql :dx)) &key &allow-other-keys)
  "For allowing plots by each MPI worker without desturbing the others."
  (make-pathname :name (format nil "output-~D.dx" (ddo:mpi-rank))
                 :directory (pathname-directory (images-pathname))))
