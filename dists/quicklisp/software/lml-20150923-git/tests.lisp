;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          lml-tests.lisp
;;;; Purpose:       lml tests file
;;;; Author:        Kevin M. Rosenberg
;;;; Date Started:  Apr 2003
;;;;
;;;; $Id$
;;;;
;;;; This file is Copyright (c) 2000-2002 by Kevin M. Rosenberg
;;;;
;;;; *************************************************************************

(in-package #:cl)
(defpackage #:lml-tests
  (:use #:lml #:cl #:rtest))
(in-package #:lml-tests)

(rem-all-tests)

(deftest lml.0
  (with-output-to-string (s)
    (let ((*html-output* s))
      (div)))
  "<div></div>")

(deftest lml.1
  (with-output-to-string (s)
    (let ((*html-output* s))
      (span-c foo "Foo Bar")))
  "<span class=\"foo\">Foo Bar</span>")

(deftest lml.2
  (with-output-to-string (s)
    (let ((*html-output* s))
      (table-c foo :style "width:80%" "Foo" " Bar" " test")))
  "<table class=\"foo\" style=\"width:80%\">Foo Bar test</table>")

(deftest lml.3
  (with-output-to-string (s)
    (let ((*html-output* s)
          (a 5.5d0))
      (p a)))
  "<p>5.5d0</p>")

(deftest lml.4
  (with-output-to-string (s)
    (let ((*html-output* s)
          (a 0.75))
      (img "http://localhost/test.png" :width a)))
  "<img src=\"http://localhost/test.png\" width=\"0.75\" />")

(deftest lml.5
  (with-output-to-string (s)
    (let ((*html-output* s))
      (div "Start"
           (p "Testing"))))
  "<div>Start<p>Testing</p></div>")

(deftest lml.6
  (with-output-to-string (s)
    (let ((*html-output* s))
      (div :style "font-weight:bold"
           "Start"
           (p-c a_class "Testing"))))
  "<div style=\"font-weight:bold\">Start<p class=\"a_class\">Testing</p></div>")

