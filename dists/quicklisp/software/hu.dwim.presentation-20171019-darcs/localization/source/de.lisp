;;; -*- mode: Lisp; Syntax: Common-Lisp; -*-
;;;
;;; Copyright (c) 2009 by the authors.
;;;
;;; See LICENCE for details.

(in-package :hu.dwim.presentation)

(def localization de
  (class-name.symbol "symbol")
  (class-name.number "number")
  (class-name.integer "integer")
  (class-name.string "string")
  (class-name.system "system")
  (class-name.package "package")
  (class-name.standard-class "standard class"))
