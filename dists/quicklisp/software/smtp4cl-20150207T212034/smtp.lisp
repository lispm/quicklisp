;;;  smtp.lisp --- SMTP client library

;;;  Copyright (C) 2005-2009 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.org>
;;;  Project: smtp4cl

#+cmu (ext:file-comment "$Module: smtp.lisp $")

;;;  This program is free software; you can redistribute it and/or
;;;  modify it under the terms of the GNU General Public License as
;;;  published by the Free Software Foundation; either version 2, or
;;;  (at your option) any later version.
;;;  This program is distributed in the hope that it will be useful,
;;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;  General Public License for more details.
;;;  You should have received a copy of the GNU General Public License
;;;  along with this program; see the file COPYING.  If not, write to
;;;  the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;;  Boston, MA 02111-1307, USA.

;;;  Commentary:

;;; Inspired by code written by Franz Inc. (smtp.cl).

(in-package :cl-user)

(defpackage :smtp4cl
  (:nicknames :smtp)
  (:use :common-lisp :net4cl :sclf)
  (:export #:send-message
	   #:make-stream-from
	   #:send-via-smtp			; low level
	   #:with-smtp-connection
	   #:call-with-smtp-connection
	   #:feed-smtp-server-with-message
	   #:write-message-to-smtp-server
	   #:*smtp-server*
	   #:*smtp-port*
	   #:*smtp-debug*))

(in-package :smtp4cl)

(defconst +mailer-name+ "SMTP4CL")

(defvar *smtp-port* 25
  "The SMTP port number.  Normally 25.")

(defvar *smtp-debug* nil
  "Either nil (no debug output is performed), t (*standard-output*) or
an output stream")

(defun write-crlf (stream)
  "Terminate a line according to SMTP protocol.  That is, with a
CR-LF"
  (write-char #\return stream)
  (write-char #\newline stream))

(defvar *smtp-server* "localhost"
  "Name of the default SMTP server to be used to send email messages.")

(defun send-message (from to message-body &key cc bcc subject reply-to headers
		     (server *smtp-server*) (port *smtp-port*))
  "Sends a message via the mail SERVER.  FROM is the address to be
given as the sender.  TO can be a string or a list of strings naming
recipients.

MESSAGE-BODY is the message to be sent; it can be a string, or
	anything else that can be promoted to a MIME-PART; it should
	not contain headers.
CC and BCC can either be a string or a  list of strings
	naming recipients.  All CCs and BCCs are sent the message
	but the BCCs aren't included in the message header.
REPLY-TO's value is a string and, if present, a Reply-To header
	will be created.
HEADERS is an alist of symbol and strings. These are header lines
	added to the header build to be sent out.
	Example:
	  ((:x-foo . \"bar\") (:x-friends . \"foo bar foobar\"))"
  (flet ((addh (title contents)
	   (when contents
	     (setf headers
		   (cons (cons title (if (listp contents)
					 (format nil "~{~A~^ ~}" contents)
					 contents))
			 (remove title headers :test #'string-equal :key #'car)))))
	 (as-list (x)
	   (if (listp x)
	       x (list x))))
    (macrolet ((listify (x)
		 `(setf ,x (as-list ,x))))
      (listify to)
      (listify cc)
      (listify bcc))
    (addh :reply-to reply-to)
    (addh :subject subject)
    (addh :cc cc)
    (addh :to to)
    (addh :from from)
    (unless (assoc :x-mailer headers)
      (addh :x-mailer (format nil "~A (~A ~A)"
			      +mailer-name+
			      (lisp-implementation-type)
			      (lisp-implementation-version))))
    ;; User can specify header names as either keywords or strings.
    ;; We convert them to strings here.
    (setf headers
	  (loop
	     for (header . value) in headers
	     collect (if (stringp header)
			 (cons header value)
			 (cons (string-capitalize (string header)) value))))
    (send-via-smtp from (append to cc bcc)
		   (list (make-instance 'mime:mime-message
					:headers headers
					:body (mime:mime-part message-body)))
		   :server server
		   :port port)))

(defun call-with-smtp-connection (proc &key (server *smtp-server*) (port *smtp-port*))
  "Make that initial connection to the mail SERVER returning a socket
connected to it and signaling an error if it can't be made"
  (with-open-socket (socket :host server :port port :element-type 'character)
    (be stream (socket-stream socket)
      (expect stream 2 "initial connection")
      (smtp-command-or-fail stream 2 "HELO ~a" (get-host-name))
      (funcall proc stream)
      (smtp-command-or-fail stream 2 "QUIT"))))

(defmacro with-smtp-connection ((var &rest keys) &body body)
  "Execute BODY within the lexical scope of VAR which is bound to a
socket connected to a SMTP server.  KEYS arguments are those supported
by CALL-WITH-SMTP-CONNECTION."
  `(call-with-smtp-connection #'(lambda (,var) ,@body) ,@keys))

(defgeneric make-stream-from (object))

(defmethod make-stream-from ((object string))
  (make-string-input-stream object))

(defmethod make-stream-from ((object pathname))
  (open object))

(defmethod make-stream-from ((object stream))
  object)

(defmethod make-stream-from ((object mime:mime-part))
  (be stream (open-temp-file nil :direction :io)
    (delete-file (pathname stream))
    (mime:encode-mime-part object stream)
    (file-position stream 0)
    stream))

(defmacro with-input-from ((var object) &body body)
  "Generalized macro to read from objects of different types.
OBJECT can be anything that has a MAKE-STREAM-FROM defined on.
VAR is bound to an input stream from OBJECT and execute BODY in
that lexical scoping.  On exit of this block the stream is
closed."
  `(with-open-stream (,var (make-stream-from ,object))
     ,@body))

(defgeneric write-message-to-smtp-server (message socket))

(defun write-smtp-line (line stream)
  (write-string line stream)
  (if (string-ends-with #.(string #\return) line)
      (terpri stream)
      (write-crlf stream)))

(defmethod write-message-to-smtp-server (message socket)
  (with-input-from (stream message)
    (do ((line #1=(read-line stream nil) #1#))
	((null line))
      ;; escape lines made out of just one dot
      (when (equal line ".")
	(write-char #\. socket))
      (write-smtp-line line socket))))

(defun feed-smtp-server-with-message (sock from recipients message)
  (smtp-command-or-fail sock 2 "MAIL from:<~A>" from)
  (dolist (to recipients)
    (smtp-command-or-fail sock 2 "RCPT to:<~A>" to))
  (smtp-command-or-fail sock 3 "DATA")
  (write-message-to-smtp-server message sock)
  (write-crlf sock)
  (write-smtp-line "." sock)
  (expect sock 2 "message delivery")
  (force-output t))

(defun send-via-smtp (from recipients messages &key (server *smtp-server*) (port *smtp-port*))
  "This is the low level version of `SEND-MESSAGE'.  It doesn't build
a header, so MESSAGES should contain one (if not then the MTA may
build one for you).  Each message in MESSAGES can be a string, a
pathname or an input-stream.  RECIPIENTS is a list of strings and each
string should be in RFC822 format (\"foo@bar.com\")"
  (with-smtp-connection (sock :server server :port port)
    (dolist (msg messages)
      (feed-smtp-server-with-message sock from recipients msg))))

(defun wait-for-response (stream)
  "Read the response of the smtp server, collect it in a string.
Return three values: response-class whole-response-string
response-code"
  ;; Multi-line responses have an hyphen after the code.  The code
  ;; (and the hyphen) is repeated for every continued line.  The last
  ;; line doesn't have the hyphen.
  (labels ((get-class (string)
	     (digit-char-p (char string 0)))
	   (get-code (string)
	     (parse-integer string :end 3))
	   (contp (string)
	     (eq #\- (char string 3)))
	   (get-response ()
	     (with-output-to-string (response)
	       (loop
		  for line = (read-line stream nil)
		  while line
		  do (write-smtp-line line response)
		  unless (contp line)
		  return nil))))
    (let ((response (get-response)))
      (if (zerop (length response))
	  (values nil response nil)
	  (values (get-class response)
		  response
		  (get-code response))))))

(defun expect (stream code &optional error-msg)
  "Wait for a result CODE from STREAM and generate an ERROR-MSG error
if we receive something else"
  (finish-output stream)
  (multiple-value-bind (class result result-code)
      (wait-for-response stream)
    (unless (= class code)
      (error "SMTP error on ~S (expeting ~A got ~A) : ~S"
	     error-msg code result-code result))))

(defun smtp-command-or-fail (stream expected-result-code &rest format-args)
  "Send through stream the SMTP command specified by FORMAT-ARGS which
are formatted, guess, how, with FORMAT.  Generate an error if the SMTP
command didn't yield the EXPECTED-RESULT-CODE"
  (apply #'smtp-command stream format-args)
  (expect stream expected-result-code format-args))

(defun smtp-command (stream &rest format-args)
  "Send a command to the SMTP server.  Command is built using
FORMAT-ARGS"
  (let ((command (apply #'format nil format-args)))
    (when *smtp-debug*
      (format *smtp-debug* "SMTP command: ~S~%" command)
      (force-output *smtp-debug*))
    (write-smtp-line command stream)
    (force-output stream)))
