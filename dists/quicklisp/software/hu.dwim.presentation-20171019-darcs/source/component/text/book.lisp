;;; -*- mode: Lisp; Syntax: Common-Lisp; -*-
;;;
;;; Copyright (c) 2009 by the authors.
;;;
;;; See LICENCE for details.

(in-package :hu.dwim.presentation)

;;;;;;
;;; book/alternator/inspector

(def (icon e) book)

(def (component e) book/alternator/inspector (text/alternator/inspector exportable/component)
  ())

(def subtype-mapper *inspector-type-mapping* (or null book) book/alternator/inspector)

(def layered-method make-alternatives ((component book/alternator/inspector) (class standard-class) (prototype book) (value book))
  (list* (make-instance 'book/text/inspector :component-value value)
         (make-instance 'book/toc/inspector :component-value value)
         (call-next-layered-method)))

(def layered-method export-file-name (format (component book/alternator/inspector) (value book))
  (title-of value))

(def (function e) make-book-menu-item (name)
  (menu-item/widget ()
      (replace-target-place/widget ()
          (icon/widget book :label (title-of (find-book name)))
        (make-value-viewer (find-book name)))))

;;;;;;
;;; t/reference/inspector

(def layered-method make-reference-content ((component t/reference/inspector) (class standard-class) (prototype book) (value book))
  (title-of value))

;;;;;;
;;; book/text/inspector

(def (component e) book/text/inspector (t/text/inspector
                                        collapsible-contents/component
                                        title/mixin
                                        exportable/component)
  ((toc :type component)))

(def refresh-component book/text/inspector
  (bind (((:slots toc component-value) -self-))
    (setf toc (make-instance 'book/toc/inspector :component-value component-value))))

(def render-xhtml book/text/inspector
  (with-render-style/component (-self-)
    (render-collapse-or-expand-command-for -self-)
    (render-title-for -self-)
    (foreach #'render-author (authors-of (component-value-of -self-)))
    (when (expanded-component? -self-)
      <div (:class "separator") <br>>
      (render-component (toc-of -self-))
      <div (:class "content")
        ,(render-contents-for -self-)>)))

(def render-odt book/text/inspector
  <text:h ,(render-title-for -self-)>
  (awhen (authors-of (component-value-of -self-))
    <text:p ,(foreach #'render-author it)>)
  (render-contents-for -self-))

(def render-text book/text/inspector
  (write-text-line-begin)
  (bind ((position (file-position *text-stream*)))
    (render-title-for -self-)
    (write-text-line-separator)
    (write-text-line-begin)
    (write-characters #\= (- (file-position *text-stream*) position 1) *text-stream*))
  (write-text-line-separator)
  (write-text-line-begin)
  (foreach #'render-author (authors-of (component-value-of -self-)))
  (write-text-line-separator)
  (call-next-layered-method))

(def render-ods book/text/inspector
  (render-title-for -self-)
  (foreach #'render-author (authors-of (component-value-of -self-)))
  (foreach #'render-component (contents-of -self-)))

(def layered-method make-title ((self book/text/inspector) class prototype (value book))
  (title-of value))

;;;;;;
;;; Author

(def (layered-function e) render-author (component)
  (:method :in xhtml-layer ((self number))
    <div (:class "author")
      ,(render-component self)>)

  (:method :in xhtml-layer ((self string))
    <div (:class "author")
      ,(render-component self)>)

  (:method ((self string))
    (render-component self))

  (:method ((self component))
    (render-component self)))

;;;;;;
;;; book/tree-level/inspector

(def (component e) book/tree-level/inspector (t/tree-level/inspector)
  ())

(def layered-method make-path-presentation ((component book/tree-level/inspector) (class standard-class) (prototype title-mixin) (value list))
  (make-instance 't/tree-level/path/inspector :component-value value))

(def layered-method make-content-presentation ((component t/tree-level/path/inspector) (class standard-class) (prototype title-mixin) (value title-mixin))
  (make-instance 't/tree-level/reference/inspector :component-value value))

(def layered-method make-previous-sibling-presentation ((component book/tree-level/inspector) (class standard-class) (prototype title-mixin) value)
  (make-instance 't/tree-level/reference/inspector :component-value value))

(def layered-method make-next-sibling-presentation ((component book/tree-level/inspector) (class standard-class) (prototype title-mixin) value)
  (make-instance 't/tree-level/reference/inspector :component-value value))

(def layered-method make-descendants-presentation ((component book/tree-level/inspector) (class standard-class) (prototype title-mixin) (value title-mixin))
  (make-instance 't/tree-level/tree/inspector :component-value value))

(def layered-method make-node-presentation ((component book/tree-level/inspector) (class standard-class) (prototype title-mixin) (value title-mixin))
  (make-instance 't/tree-level/reference/inspector :component-value value))

(def layered-method collect-presented-children ((component book/tree-level/inspector) (class standard-class) (prototype title-mixin) (value title-mixin))
  (collect-if (of-type 'title-mixin) (contents-of value)))

(def layered-method collect-presented-children ((component t/node/inspector) (class standard-class) (prototype title-mixin) (value title-mixin))
  (collect-if (of-type 'title-mixin) (contents-of value)))

(def layered-method make-reference-content ((component t/tree-level/reference/inspector) (class standard-class) (prototype title-mixin) (value title-mixin))
  (title-of value))
