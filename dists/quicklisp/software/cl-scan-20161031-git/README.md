## cl-scan
**cl-scan** is a simple port scanner.

```common-lisp
(ql:quickload :cl-scan)
(cl-scan:main "google.com" 1 1000 0.1)
```
