;;;; -*- Mode: Lisp; indent-tabs-mode: nil -*-

(in-package :tcr.extensible-sequences.test)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defparameter +example+ '(0 1 2 3 4 5 6 7 5 9))
  (defparameter +take/drop-while-clauses+
    '((:sequences :non-empty :start 0           :end nil         :contents +example+)
      (:sequences :non-empty :start 1           :end nil         :contents +example+)
      (:sequences :non-empty :start 0           :end 3           :contents +example+)
      (:sequences :non-empty :start 1           :end 3           :contents +example+)
      (:sequences :non-empty :start 0           :end 5           :contents +example+)
      (:sequences :non-empty :start 1           :end 5           :contents +example+)
      (:sequences :non-empty :start *size*      :end nil         :contents +example+)
      (:sequences :non-empty :start (1- *size*) :end nil         :contents +example+)
      (:sequences :non-empty :start (2- *size*) :end (1- *size*) :contents +example+)))
  (defun smaller (n)
    #'(lambda (x) (< x n))))


(define-sequence-test take/drop-while.1
    #.+take/drop-while-clauses+
  (concatenate (sequence-type-of *sequence*)
               (take-while (smaller 4) *sequence* :start *start* :end *end*)
               (drop-while (smaller 4) *sequence* :start *start* :end *end*))
  (subseq *sequence* *start* *end*))

(define-sequence-test take/drop-while.2
    #.+take/drop-while-clauses+
  (concatenate (sequence-type-of *sequence*)
               (drop-while (smaller 4) *sequence*
                           :start *start* :end *end* :from-end t)
               (take-while (smaller 4) *sequence*
                           :start *start* :end *end* :from-end t))
  (subseq *sequence* *start* *end*))

(define-sequence-test take/drop-while.3
    #.+take/drop-while-clauses+
  (drop-while (smaller 4) *sequence*
              :start *start* :end *end*)
  (take-while (complement (smaller 4)) *sequence*
              :start *start* :end *end* :from-end t))

(define-sequence-test take/drop-while.4
    #.+take/drop-while-clauses+
  (take-while (smaller 4) *sequence*
              :start *start* :end *end*)
  (drop-while (complement (smaller 4)) *sequence*
              :start *start* :end *end* :from-end t))

(define-sequence-test take/drop-while.5
    #.+take/drop-while-clauses+
  (let ((s1 (drop-while (smaller 4) *sequence* :start *start* :end *end*))
        (s2 (take-while (complement (smaller 4)) *sequence*
                        :start *start* :end *end* :from-end t)))
    (values (type= (sequence-type-of s1) (sequence-type-of *sequence*))
            (type= (sequence-type-of s2) (sequence-type-of *sequence*))
            (type= (type-of s1) (type-of s2))))
  t
  t
  t)

(define-sequence-test take/drop-while.6
    #.+take/drop-while-clauses+
  (let ((s1 (drop-while (smaller 4) *sequence* :start *start* :end *end*
                        :result-type (sequence-type-of *sequence*)))
        (s2 (take-while (complement (smaller 4)) *sequence*
                        :start *start* :end *end* :from-end t
                        :result-type (sequence-type-of *sequence*)))
        (s3 (take-while (smaller 4) *sequence* :start *start* :end *end*
                        :result-type (sequence-type-of *sequence*)))
        (s4 (drop-while (complement (smaller 4)) *sequence*
                        :start *start* :end *end* :from-end t
                        :result-type (sequence-type-of *sequence*))))
    (values (type= (type-of s1) (type-of s2))
            (type= (sequence-type-of s2) (sequence-type-of *sequence*))
            (type= (type-of s3) (type-of s4))            
            (type= (sequence-type-of s4) (sequence-type-of *sequence*))))
  t t
  t t)

(define-sequence-test take/drop-while.7
    ((:sequences :empty))
  (let ((s1 (drop-while (smaller 4) *sequence* :start *start* :end *end*))
        (s2 (drop-while (smaller 4) *sequence* :start *start* :end *end* :from-end t))
        (s3 (take-while (smaller 4) *sequence* :start *start* :end *end*))
        (s4 (take-while (smaller 4) *sequence* :start *start* :end *end* :from-end t)))
    (values s1 s2 s3 s4
            (type= (type-of s1) (type-of s2))
            (type= (type-of s2) (type-of s3))
            (type= (type-of s3) (type-of s4))
            (type= (sequence-type-of s4) (sequence-type-of *sequence*))))
  *sequence*
  *sequence*
  *sequence*
  *sequence*
  t t t t
  )
