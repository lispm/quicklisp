;;;  namespace.lisp --- namespace manipulation primitives

;;;  Copyright (C) 2004 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-CE6BBCEF2EF2753C0DE5D8CFCDDF0450.lisp,v 1.3 2004/03/26 01:06:54 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-CE6BBCEF2EF2753C0DE5D8CFCDDF0450.lisp,v 1.3 2004/03/26 01:06:54 wcp Exp $")

(in-package :binge-compiler)

(defclass c-namespace ()
  ((constants :initform (make-hash-table :test 'equalp)
	      :reader c-constants
	      :type hash-table)
   (typedefs :initform (make-hash-table :test 'equalp)
	     :reader c-typedefs
	     :type hash-table)
   ;; structs, unions and enums go in the same namespace
   (structs :initform (make-hash-table :test 'equalp)
	    :reader c-structs
	    :type hash-table)
   (functions :initform (make-hash-table :test 'equalp)
	      :reader c-functions
	      :type hash-table)
   (variables :initform (make-hash-table :test 'equalp)
	      :reader c-variables
	      :type hash-table))
  (:documentation
   "Objects of this class represent all the declarations taken
from parsing a C source/header file.  Structs, unions and enums
share the same hashtable as in C they share the same namespace."))

(defgeneric find-c-typedef (namespace name)
  (:documentation
   "Find typedef declaration named NAME in NAMESPACE."))
(defgeneric (setf find-c-typedef) (declaration namespace name)
  (:documentation
   "Insert typedef DECLARATION with NAME in NAMESPACE."))

(defgeneric find-c-struct (namespace name)
  (:documentation
   "Find struct declaration named NAME in NAMESPACE."))
(defgeneric (setf find-c-struct) (declaration namespace name)
  (:documentation
   "Insert struct DECLARATION named NAME in NAMESPACE."))

(defgeneric find-c-function (namespace name)
  (:documentation
   "Find function declaration named NAME in NAMESPACE."))
(defgeneric (setf find-c-function) (declaration namespace name)
  (:documentation
   "Insert function DECLARATION named NAME in NAMESPACE."))

(defgeneric find-c-constant (namespace name)
  (:documentation
   "Find constant declaration named NAME in NAMESPACE."))
(defgeneric (setf find-c-constant) (declaration namespace name)
  (:documentation
   "Insert constant DECLARATION named NAME in NAMESPACE."))

(defgeneric find-c-variable (namespace name)
  (:documentation
   "Find variable declaration named NAME in NAMESPACE."))
(defgeneric (setf find-c-variable) (declaration namespace name)
  (:documentation
   "Insert variable DECLARATION named NAME in NAMESPACE."))

(defgeneric find-c-enum (namespace name)
  (:documentation
   "Find enum declaration named NAME in NAMESPACE."))
(defgeneric (setf find-c-enum) (declaration namespace name)
  (:documentation
   "Insert enum DECLARATION named NAME in NAMESPACE."))

(defgeneric find-c-union (namespace name)
  (:documentation
   "Find union declaration named NAME in NAMESPACE."))
(defgeneric (setf find-c-union) (declaration namespace name)
  (:documentation
   "Insert union DECLARATION named NAME in NAMESPACE."))

(defgeneric find-declaration (decl namespace)
  (:documentation
   "Find DECL declaration in NAMESPACE and return it,
otherwise return nil."))
(defgeneric (setf find-declaration) (value decl namespace)
  (:documentation
   "Insert DECL declaration in NAMESPACE."))

(defgeneric add-declaration (declaration namespace)
  (:documentation
   "Add DECLARATION in NAMESPACE, replacing original declaration
if already present."))

(defgeneric check-reference (decl namespace)
  (:documentation
   "Make sure that the type referred by DECL exists.  This is a
nop if DECL is not a reference to a type.  If the referenced type
doesn't exist it's created and added to namespace."))

(defgeneric make-sure-referred-types-exist (obj namespace)
  (:documentation
   "Make sure all the types referenced by C object OBJ exist.  If
necessary populate namespace with dummies of the missing
types."))

(defgeneric substitute-declarations-with-references (object namespace)
  (:documentation
  "Extract type definitions, if any, from C OBJECT, populating
NAMESPACE with new declarations, and susbstitute when necessary
with references to types.  Return the possibly modified object."))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun add-new-declaration (decl namespace)
  "Add DECL declaration in NAMESPACE namespace only if it's not
already in."
  (declare (type c-declaration decl)
	   (type c-namespace namespace))
  (unless (find-declaration decl namespace)
    (add-declaration decl namespace)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod find-c-typedef ((namespace c-namespace) name)
  (gethash name (c-typedefs namespace)))

(defmethod (setf find-c-typedef) (value (namespace c-namespace) name)
  (setf (gethash name (c-typedefs namespace)) value))

(defmethod find-c-struct ((namespace c-namespace) name)
  (gethash name (c-structs namespace)))

(defmethod (setf find-c-struct) (value (namespace c-namespace) name)
  (setf (gethash name (c-structs namespace)) value))

(defmethod find-c-enum ((namespace c-namespace) name)
  (gethash name (c-structs namespace)))

(defmethod (setf find-c-enum) (value (namespace c-namespace) name)
  (setf (gethash name (c-structs namespace)) value))

(defmethod find-c-union ((namespace c-namespace) name)
  (gethash name (c-structs namespace)))

(defmethod (setf find-c-union) (value (namespace c-namespace) name)
  (setf (gethash name (c-structs namespace)) value))

(defmethod find-c-function ((namespace c-namespace) name)
  (gethash name (c-functions namespace)))

(defmethod (setf find-c-function) (value (namespace c-namespace) name)
  (setf (gethash name (c-functions namespace)) value))

(defmethod find-c-constant ((namespace c-namespace) name)
  (gethash name (c-constants namespace)))

(defmethod (setf find-c-constant) (value (namespace c-namespace) name)
  (setf (gethash name (c-constants namespace)) value))

(defmethod find-c-variable ((namespace c-namespace) name)
  (gethash name (c-variables namespace)))

(defmethod (setf find-c-variable) (value (namespace c-namespace) name)
  (setf (gethash name (c-variables namespace)) value))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod find-declaration ((decl c-function-declaration) (namespace c-namespace))
  (find-c-function namespace (c-name decl)))

(defmethod (setf find-declaration) (value (decl c-function-declaration) (namespace c-namespace))
  (setf (find-c-function namespace (c-name decl)) value))

(defmethod find-declaration ((decl c-struct-declaration) (namespace c-namespace))
  (find-c-struct namespace (c-name decl)))

(defmethod (setf find-declaration) (value (decl c-struct-declaration) (namespace c-namespace))
  (setf (find-c-struct namespace (c-name decl)) value))

(defmethod find-declaration ((decl c-union-declaration) (namespace c-namespace))
  (find-c-union namespace (c-name decl)))

(defmethod (setf find-declaration) (value (decl c-union-declaration) (namespace c-namespace))
  (setf (find-c-union namespace (c-name decl)) value))

(defmethod find-declaration ((decl c-enum-declaration) (namespace c-namespace))
  (find-c-enum namespace (c-name decl)))

(defmethod (setf find-declaration) (value (decl c-enum-declaration) (namespace c-namespace))
  (setf (find-c-enum namespace (c-name decl)) value))

(defmethod find-declaration ((decl c-variable-declaration) (namespace c-namespace))
  (find-c-variable namespace (c-name decl)))

(defmethod (setf find-declaration) (value (decl c-variable-declaration) (namespace c-namespace))
  (setf (find-c-variable namespace (c-name decl)) value))

(defmethod find-declaration ((decl c-typedef-declaration) (namespace c-namespace))
  (find-c-typedef namespace (c-name decl)))

(defmethod (setf find-declaration) (value (decl c-typedef-declaration) (namespace c-namespace))
  (setf (find-c-typedef namespace (c-name decl)) value))

(defmethod find-declaration ((decl c-constant-declaration) (namespace c-namespace))
  (find-c-constant namespace (c-name decl)))

(defmethod (setf find-declaration) (value (decl c-constant-declaration) (namespace c-namespace))
  (setf (find-c-constant namespace (c-name decl)) value))

(defmethod find-declaration ((obj c-user-type-ref) (namespace c-namespace))
  (find-c-typedef namespace (c-ref-name obj)))

(defmethod (setf find-declaration) (value (obj c-user-type-ref) (namespace c-namespace))
  (setf (find-c-typedef namespace (c-ref-name obj)) value))

(defmethod find-declaration ((obj c-struct-ref) (namespace c-namespace))
  (find-c-struct namespace (c-ref-name obj)))

(defmethod (setf find-declaration) (value (obj c-struct-ref) (namespace c-namespace))
  (setf (find-c-struct namespace (c-ref-name obj)) value))

(defmethod find-declaration ((obj c-union-ref) (namespace c-namespace))
  (find-c-union namespace (c-ref-name obj)))

(defmethod (setf find-declaration) (value (obj c-union-ref) (namespace c-namespace))
  (setf (find-c-union namespace (c-ref-name obj)) value))

(defmethod find-declaration ((obj c-enum-ref) (namespace c-namespace))
  (find-c-enum namespace (c-ref-name obj)))

(defmethod (setf find-declaration) (value (obj c-enum-ref) (namespace c-namespace))
  (setf (find-c-enum namespace (c-ref-name obj)) value))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun hash-table-values-list (hash-table)
  (declare (type hash-table hash-table))
  ;; Although sorting the output is not strictly necessary for the
  ;; normal use it's fundamental for the regression tests and may make
  ;; the generated FFI file a bit prettier.
  (sort (loop
	   for decl being each hash-value of hash-table
	   collect decl)
	#'string< :key #'c-name))

(defun get-all-c-constants (namespace)
  (declare (type c-namespace namespace))
  (hash-table-values-list (c-constants namespace)))

(defun get-all-c-typedefs (namespace)
  (declare (type c-namespace namespace))
  (hash-table-values-list (c-typedefs namespace)))

(defun get-all-c-structs (namespace)
  (declare (type c-namespace namespace))
  (hash-table-values-list (c-structs namespace)))

(defun get-all-c-functions (namespace)
  (declare (type c-namespace namespace))
  (hash-table-values-list (c-functions namespace)))

(defun get-all-c-variables (namespace)
  (declare (type c-namespace namespace))
  (hash-table-values-list (c-variables namespace)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; default just use the find-declaration
(defmethod add-declaration ((decl c-declaration) (namespace c-namespace))
  (setf (find-declaration decl namespace) decl))

(defmethod add-declaration ((decl c-variable-declaration) (namespace c-namespace))
  (let ((maybe-function (c-type decl)))
    ;; The parser recognises functions as variables of type function.
    ;; We correct this here.
    (if (eq (type-of maybe-function) 'c-function)
	(add-declaration
	 (make-instance 'c-function-declaration
			:name (c-name decl)
			:type (c-type maybe-function)
			:arguments (c-arguments maybe-function))
	 namespace)
	(setf (find-c-variable namespace (c-name decl)) decl))))

(defmethod add-declaration :after (decl (namespace c-namespace))
  (substitute-declarations-with-references decl namespace)
  (make-sure-referred-types-exist decl namespace))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod substitute-declarations-with-references (obj (namespace c-namespace))
  ;; default case, declaration without types
  obj)

(defmethod substitute-declarations-with-references ((obj c-typed-object) (namespace c-namespace))
  (setf (c-type obj) (transform-to-reference (c-type obj) namespace))
  obj)

(defmethod substitute-declarations-with-references ((decl c-container) (namespace c-namespace))
  (setf (c-slots decl)
	(mapcar #'(lambda (slot)
		    (transform-to-reference slot namespace))
		(c-slots decl)))
  decl)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric transform-to-reference (object namespace)
  (:documentation
   "If possible transform C OBJECT into a reference and populate
NAMESPACE with its declaration."))

(defmethod transform-to-reference (obj (namespace c-namespace))
  ;; default case; primitive type
  obj)

(defmethod transform-to-reference ((obj c-typed-object) (namespace c-namespace))
  (setf (c-type obj) (transform-to-reference (c-type obj) namespace))
  obj)

(defmethod transform-to-reference ((obj c-declaration) (namespace c-namespace))
  (let ((ref (make-c-reference obj)))
    (add-declaration obj namespace)
    ref))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod make-sure-referred-types-exist (obj (namespace c-namespace))
  ;; default case, untyped declaration
  (values))

(defmethod make-sure-referred-types-exist ((obj c-type-ref) (namespace c-namespace))
  (check-reference obj namespace))

(defmethod make-sure-referred-types-exist ((obj c-typed-object) (namespace c-namespace))
  (make-sure-referred-types-exist (c-type obj) namespace))

(defmethod make-sure-referred-types-exist ((obj c-container) (namespace c-namespace))
  (dolist (slot (c-slots obj))
    (make-sure-referred-types-exist slot namespace)))

(defmethod make-sure-referred-types-exist ((obj c-function) (namespace c-namespace))
  (call-next-method)
  (dolist (arg (c-arguments obj))
    (make-sure-referred-types-exist arg namespace)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod check-reference (decl (namespace c-namespace))
  (values))

(defmethod check-reference ((decl c-struct-ref) (namespace c-namespace))
  (unless (find-c-struct namespace (c-ref-name decl))
    (add-declaration
     (make-instance 'c-struct-declaration
		    :name (c-ref-name decl)
		    :slots '())
     namespace)))

(defmethod check-reference ((decl c-union-ref) (namespace c-namespace))
  (unless (find-c-union namespace (c-ref-name decl))
    (add-declaration
     (make-instance 'c-union-declaration
		    :name (c-ref-name decl)
		    :slots '())
     namespace)))

(defmethod check-reference ((decl c-enum-ref) (namespace c-namespace))
  (unless (find-c-enum namespace (c-ref-name decl))
    (add-declaration
     (make-instance 'c-enum-declaration
		    :name (c-ref-name decl)
		    :constants '())
     namespace)))

(defmethod check-reference ((decl c-user-type-ref) (namespace c-namespace))
  (cond ((symbolp (c-ref-name decl))
	 t)
	((not (find-c-typedef namespace (c-ref-name decl)))
	 (error "unknown type ~S~%" (c-ref-name decl)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun populate-namespace (namespace declarations-list)
  "Populate NAMESPACE it with declarations from
DECLARATIONS-LIST."
  (declare (type c-namespace namespace)
	   (type list declarations-list))
  (loop
     for decl of-type c-declaration in declarations-list
     do (add-declaration decl namespace)
     finally (return namespace)))

