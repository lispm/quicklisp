;;;; -*- Mode: lisp; indent-tabs-mode: nil -*-

(defpackage #:singleton-classes-system
	(:use #:cl #:asdf))

(in-package #:singleton-classes-system)

(defsystem singleton-classes
    :description "Extends the MOP to allow `singleton` classes."
    :author "Tim Bradshaw (tfb at lostwithiel)"
    :version "1.7"
    :components
    ((:file "singleton-class"))
    :depends-on
    ("closer-mop"))




              
