; (require :agnostic-lizard)
(asdf:load-system :agnostic-lizard)
; (use-package :agnostic-lizard)

(defpackage :agnostic-lizard-tests
  (:use :common-lisp :agnostic-lizard))
(in-package :agnostic-lizard-tests)

(assert 
  (equal
    (let ((unbound-vars nil))
      (walk-form
        `(let ((qqq 1))
           z
           (let ((y x)) (+ y z)))
        nil
        :on-every-atom
        (lambda (f e)
          (unless 
            (or
              (not (symbolp f))
              (find f (metaenv-variable-like-entries e) :key 'first))
            (pushnew f unbound-vars))
          f))
      unbound-vars)
    '(x z)))

(format *error-output* "Example run complete.~%")
