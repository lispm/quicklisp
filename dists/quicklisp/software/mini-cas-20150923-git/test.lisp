(in-package :mini-cas)

(defmacro check (form expected)
  `(progn
     (format t "checking ~A~%" ',form)
     (let ((result ,form))
       (format t "output: ~A~%" result)
       (unless (cas-equal result ,expected)
         (format t "ERROR: expected ~A~%" ,expected)
         (cerror "proceed to next test"
                 "incorrect output")))))

(defun test-full-derivative ()
  ;; basic ops
  (check (full-derivative 42 'x)
         0)
  (check (full-derivative '(+ u v w) 'x)
         '(+ (d u x) (d v x) (d w x)))
  (check (full-derivative '(- u) 'x)
         '(- (d u x)))
  (check (full-derivative '(* u v w) 'x)
         '(+ (* (d u x) v w) (* u (d v x) w) (* u v (d w x))))
  (check (full-derivative '(/ u v) 'x)
         '(/ (+ (* v (d u x)) (- (* u (d v x)))) (* v v)))
  (check (full-derivative '(sin u) 'x)
         '(* (cos u) (d u x)))
  (check (full-derivative '(cos u) 'x)
         '(- (* (sin u) (d u x))))

  ;; simple composition
  (check (full-derivative '(* 5 u) 'x)
         '(+ (* 0 u) (* 5 (d u x))))
  (check (full-derivative '(* (+ u v) w) 'x)
         '(+ (* (+ (d u x) (d v x)) w) (* (+ u v) (d w x))))

  ;; matrices
  (check (full-derivative '#2A((1 u) ((* 3 v) (+ u v))) 'x)
         '#2A((0 (d u x)) ((+ (* 0 v) (* 3 (d v x))) (+ (d u x) (d v x)))))
  t)

(defun test-simplification ()
  (check (full-simplify '(+ 1 y (+ 3 x 4) #2A((1 2)(3 4)) (+ (+ 5 6) #2A((1 0) (0 1)) 7)))
         '(+ 26 x y #2A((2 2) (3 5))))
  (check (full-simplify '(+ 2 (- (+ 1 1))))
         0)
  (check (full-simplify
          '(*
            #2A(((cos t) (- (sin t)) 0 0) ((sin t) (cos t) 0 0) (0 0 1 0) (0 0 0 1))
            #2A((1 0 0 a) (0 1 0 0) (0 0 1 d) (0 0 0 1))
            #2A((1 0 0 0) (0 (cos b) (- (sin b)) 0) (0 (sin b) (cos b) 0) (0 0 0 1))))
         #2A(((cos t) (* -1 (cos b) (sin t)) (* (sin b) (sin t)) (* a (cos t)))
             ((sin t) (* (cos b) (cos t)) (* -1 (cos t) (sin b)) (* a (sin t)))
             (0 (sin b) (cos b) d)
             (0 0 0 1)) )
  (check (full-simplify
          '(+ x x (f x) (* (f x) 3) (f x y) x))
         '(+ (* 3 x) (* 4 (f x)) (f x y)))
  (check (full-simplify
          '(* (+ (* m1 x) (* m2 y)) (+ 1 x)))
         '(+ (* m1 x) (* m1 x x) (* m2 x y) (* m2 y)))

  t)
