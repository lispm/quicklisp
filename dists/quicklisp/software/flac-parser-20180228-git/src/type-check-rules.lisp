(in-package :flac-parser)

(defun report-type (file node id &key (level :error) context)
  (let ((report (make-instance 'type-check-report :id id :level level :node node :context context)))
    (push report (type-reports file))))

(defmethod type-check-rule (file node (id (eql :block-invalid)) &key)
  (mapcar
   (lambda (x)
     (unless (typep x 'metadata-block)
       (report-type file node id :context x)))
   (blocks node)))

(defmethod type-check-rule (file node (id (eql :duplicate-block-stream-info)) &key)
  (mapcar
   (lambda (x)
     (report-type file node id :context x))
   (list-blocks-by-type file :stream-info)))

(defmethod type-check-rule (file node (id (eql :duplicate-block-vorbis-comment)) &key)
  (let ((blocks (list-blocks-by-type file :vorbis-comment)))
    (when (> (length blocks) 1)
      (mapcar
       (lambda (x)
         (report-type file node id :context x))
       blocks))))

(defmethod type-check-rule (file node (id (eql :duplicate-block-seek-table)) &key)
  (let ((blocks (list-blocks-by-type file :seek-table)))
    (when (> (length blocks) 1)
      (mapcar
       (lambda (x)
         (report-type file node id :context x))
       blocks))))

(defmethod type-check-rule (file node (id (eql :duplicate-block-cue-sheet)) &key)
  (let ((blocks (list-blocks-by-type file :cue-sheet)))
    (when (> (length blocks) 1)
      (mapcar
       (lambda (x)
         (report-type file node id :context x))
       blocks))))

(defmethod type-check-rule (file node (id (eql :duplicate-block-picture-type-1)) &key)
  (let ((blocks (list-pictures-by-type file 1)))
    (when (> (length blocks) 1)
      (mapcar
       (lambda (x)
         (report-type file node id :context x))
       blocks))))

(defmethod type-check-rule (file node (id (eql :duplicate-block-picture-type-2)) &key)
  (let ((blocks (list-pictures-by-type file 2)))
    (when (> (length blocks) 1)
      (mapcar
       (lambda (x)
         (report-type file node id :context x))
       blocks))))

(defmethod type-check-rule (file node (id (eql :block-size-limit-exceeded)) &key)
  (when (> (block-length (header node)) (expt 2 24))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :block-type-reserved)) &key)
  (when (<= 7 (block-type (header node)) 126)
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :block-type-invalid)) &key)
  (when (= (block-type (header node)) 127)
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :minimum-block-size-invalid)) &key)
  (when (< (minimum-block-size (data node)) 16)
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :maximum-block-size-invalid)) &key)
  (when (< (maximum-block-size (data node)) 16)
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :sample-rate-invalid)) &key)
  (unless (<= 1 (sample-rate (data node)) 655350)
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :channel-count-invalid)) &key)
  (unless (<= 0 (channel-count (data node)) 7)
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :bits-per-sample-invalid)) &key)
  (unless (<= 3 (bits-per-sample (data node)) 31)
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :seek-table-detected)) &key)
  (report-type file node id :level :warning))

(defmethod type-check-rule (file node (id (eql :seek-table-size-exceeded)) &key)
  (loop :with max-points = (/ (block-length (header node)) 18)
        :for point :in (seek-points (data node))
        :for i :from 1
        :when (> i max-points)
          :do (report-type file node id :context point)))

(defmethod type-check-rule (file node (id (eql :seek-table-unsorted)) &key)
  (unless (apply #'< (mapcar #'sample (seek-points (data node))))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :seek-point-placeholder)) &key)
  (mapcar
   (lambda (x)
     (when (seek-point-placeholder-p x)
       (report-type file node id :context x :level :warning)))
   (seek-points (data node))))

(defmethod type-check-rule (file node (id (eql :seek-point-not-unique)) &key)
  (loop :with last-value
        :with items = (remove-if #'seek-point-placeholder-p (seek-points (data node)))
        :for item :in (sort (copy-seq items) #'< :key #'sample)
        :for value = (sample item)
        :do (if (and last-value (= value last-value))
                (report-type file node id :context item)
                (setf last-value value))))

(defmethod type-check-rule (file node (id (eql :vorbis-comment-detected)) &key)
  (report-type file node id :level :warning))

(defmethod type-check-rule (file node (id (eql :vorbis-comment-size-exceeded)) &key)
  (loop :with max-comments = (user-comments-count (data node))
        :for comment :in (hash-table-keys (user-comments (data node)))
        :for i :from 1
        :when (> i max-comments)
          :do (report-type file node id :context comment)))

(defmethod type-check-rule (file node (id (eql :vorbis-comment-key-invalid)) &key)
  (flet ((char-valid-p (char)
           (and (char<= #\Space char #\})
                (not (char= char #\=)))))
    (mapcar
     (lambda (x)
       (unless (every #'char-valid-p x)
         (report-type file node id :context x)))
     (hash-table-keys (user-comments (data node))))))

(defmethod type-check-rule (file node (id (eql :vorbis-comment-key-lowercase)) &key)
  (flet ((char-lowercase-p (char)
           (char<= #\a char #\z)))
    (mapcar
     (lambda (x)
       (when (some #'char-lowercase-p x)
         (report-type file node id :context x)))
     (hash-table-keys (user-comments (data node))))))

(defmethod type-check-rule (file node (id (eql :application-detected)) &key)
  (report-type file node id :level :warning))

(defmethod type-check-rule (file node (id (eql :cue-sheet-detected)) &key)
  (report-type file node id :level :warning))

(defmethod type-check-rule (file node (id (eql :media-catalog-id-invalid)) &key)
  (when (or (and (= (cd-flag (data node)) 1)
                 (not (= (length (media-catalog-id (data node))) 13)))
            (some (lambda (x)
                    (char<= #\Space x #\~))
                  (media-catalog-id (data node))))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :lead-in-sample-count-invalid-for-cd)) &key)
  (when (and (= (cd-flag (data node)) 1)
             (< (lead-in-sample-count (data node)) 88200))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :lead-in-sample-count-invalid)) &key)
  (when (and (= (cd-flag (data node)) 0)
             (plusp (lead-in-sample-count (data node))))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :track-count-invalid-for-cd)) &key)
  (when (and (= (cd-flag (data node)) 1)
             (> (track-count (data node)) 100))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :track-count-invalid)) &key)
  (when (zerop (track-count (data node)))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :track-invalid)) &key)
  (mapcar
   (lambda (x)
     (unless (typep x 'metadata-block-cue-sheet/track)
       (report-type file node id :context x)))
   (tracks (data node))))

(defmethod type-check-rule (file node (id (eql :lead-out-number-invalid-for-cd)) &key)
  (let ((lead-out (first (last (tracks (data node))))))
    (when (and lead-out
               (= (cd-flag (data node)) 1)
               (not (= (track-number lead-out) 170)))
      (report-type file node id :context lead-out))))

(defmethod type-check-rule (file node (id (eql :lead-out-number-invalid)) &key)
  (let ((lead-out (first (last (tracks (data node))))))
    (when (and lead-out
               (= (cd-flag (data node)) 0)
               (not (= (track-number lead-out) 255)))
      (report-type file node id :context lead-out))))

(defmethod type-check-rule (file node (id (eql :lead-out-index-count-invalid)) &key)
  (let ((lead-out (first (last (tracks (data node))))))
    (when (and lead-out (plusp (index-count lead-out)))
        (report-type file node id :context lead-out))))

(defmethod type-check-rule (file node (id (eql :track-index-count-invalid)) &key lastp)
  (when (and (not lastp)
             (zerop (index-count node)))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :track-index-count-invalid-for-cd)) &key cdp lastp)
  (when (and (not lastp)
             cdp
             (> (index-count node) 100))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :track-offset-invalid-for-cd)) &key cdp)
  (unless (and cdp (zerop (mod (offset node) 588)))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :track-number-invalid-for-cd)) &key cdp)
  (when (and cdp
             (not (or (<= 1 (track-number node) 99)
                      (= (track-number node) 170))))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :track-number-invalid)) &key cdp)
  (when (and (not cdp) (not (<= 1 (track-number node) 255)))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :track-isrc-invalid)) &key)
  (when (or (and (not (zerop (length (isrc node))))
                 (not (= (length (isrc node)) 12)))
            (and (= (length (isrc node)) 12)
                 (or (not (every (lambda (x) (char<= #\A x #\Z))
                                 (subseq (isrc node) 0 2)))
                     (not (every (lambda (x) (or (char<= #\A x #\Z)
                                                 (char<= #\0 x #\9)))
                                 (subseq (isrc node) 2 5)))
                     (not (every (lambda (x) (char<= #\0 x #\9))
                                 (subseq (isrc node) 5 12))))))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :track-isrc-lowercase)) &key)
  (when (some (lambda (x) (char<= #\a x #\z)) (isrc node))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :track-index-invalid)) &key)
  (mapcar
   (lambda (x)
     (unless (typep x 'metadata-block-cue-sheet/track-index)
       (report-type file node id :context x)))
   (indices node)))

(defmethod type-check-rule (file node (id (eql :padding-detected)) &key)
  (report-type file node id :level :warning))

(defmethod type-check-rule (file node (id (eql :padding-not-zero)) &key)
  (unless (every #'zerop (data node))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :picture-detected)) &key)
  (report-type file node id :level :warning))

(defmethod type-check-rule (file node (id (eql :picture-type-invalid)) &key)
  (unless (<= 0 (picture-type (data node)) 20)
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :picture-mime-type-invalid)) &key)
  (unless (or (every (lambda (x) (char<= #\Space x #\~)) (mime-type (data node)))
              (string= (mime-type (data node)) "-->"))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :picture-width-invalid)) &key)
  (when (zerop (width (data node)))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :picture-height-invalid)) &key)
  (when (zerop (height (data node)))
    (report-type file node id)))

(defmethod type-check-rule (file node (id (eql :picture-color-depth-invalid)) &key)
  (when (zerop (bits-per-pixel (data node)))
    (report-type file node id)))
