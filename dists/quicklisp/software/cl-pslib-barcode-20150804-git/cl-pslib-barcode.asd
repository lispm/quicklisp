;; This software is Copyright (c) cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL



(asdf:defsystem cl-pslib-barcode
  :author "cage <cage@katamail.com>"
  :description "A barcode generator for the cl-pslib library."
  :licence "LLGPL"
  :maintainer "cage <cage@katamail.com>"
  :version "0.0.1"
  :depends-on (:alexandria
               :cffi
	       :cl-colors
	       :cl-ppcre-unicode
	       :cl-pslib)
  :components ((:file "package")
	       (:file "conditions"
		      :depends-on ("package"))
	       (:file "utils"
		      :depends-on ("package"))
	       (:file "barcode"
		      :depends-on ("conditions"))
	       (:file "code128"
		      :depends-on ("utils"
				   "barcode"))))
	       

