(in-package :mini-cas)

;;;; protocols for simplification

#|
  canonic order:
  - numbers
  - symbols
  - forms
  - arrays

  Assume everything associates, and everything commutes except for arrays.

  Should canonic include reusing eq objects?

  Really should calculate a "complexity function" and work to minimize its cost.
|#

(defgeneric canonic-order-rank (form)
  (:documentation "ordering of form types")
  ;; one useful lesson from BASIC:  leave space for new values...
  ;; also note that compound expressions may have high-ranking children
  (:method ((form number)) 0)
  (:method ((form symbol)) 10)
  (:method ((form list))
    (max 20
         (loop :for x :in form
            :maximize (canonic-order-rank x))))
  (:method ((form array))
    (let ((max 30))
      (dotimes (i (array-dimension form 0))
        (dotimes (j (array-dimension form 1))
          (setf max (max max (canonic-order-rank (aref form i j))))))
      max)))
(defgeneric canonic-order-pred (a b)
  (:documentation "predicate for sorting terms, extend for entries in the same class")
  ;; don't reorder anything containing a matrix...
  (:method (a b)
    (< (canonic-order-rank a)
       (canonic-order-rank b)))
  (:method ((a number) (b number))
    (< a b))
  (:method ((a symbol) (b symbol))
    (string< (symbol-name a)
             (symbol-name b))))
(defmethod canonic-order-pred ((a list) (b list))
  (loop
     for x in a
     for y in b
     do (let ((order (canonic-order-pred x y)))
          (unless (equal order
                         (canonic-order-pred y x))
            (return order)))
     finally (return (<= (length a)
                         (length b)))))


(defgeneric canonic-order (form))
(defgeneric canonic-order-list (car cdr))

(defmethod canonic-order (form)
  form)
(defmethod canonic-order ((form list))
  (canonic-order-list (car form) (cdr form)))


(defmethod canonic-order-list (car cdr)
  (cons car cdr)) ;; don't "simplify" arbitrary expressions like (sin x)

(defmethod canonic-order-list ((car (eql '+)) cdr)
  (cons '+
        (stable-sort (copy-list cdr) 'canonic-order-pred)))
(defmethod canonic-order-list ((car (eql '*)) cdr)
  (cons '*
        (stable-sort (copy-list cdr) 'canonic-order-pred)))


#|
  equality testing
  - check form structure, not mathematical equality
  - this is currently just to help "simplification"
  - cl:equal doesn't inspect matrices properly
|#
(defgeneric cas-equal (a b))

(defmethod cas-equal (a b)
  (equal a b))

(defmethod cas-equal ((a list) (b list))
  (and
   (every (lambda (x) x)
          (mapcar 'cas-equal a b))
   (= (length a) (length b))))

(defmethod cas-equal ((a array) (b array))
  (unless
      (equal (array-dimensions a) (array-dimensions b))
    (return-from cas-equal nil))
  (dotimes (i (array-dimension a 0))
    (dotimes (j (array-dimension b 1))
      (unless (cas-equal (aref a i j) (aref b i j))
        (return-from cas-equal nil))))
  t)


#|
  "simplificiation"

  stuff like 0+x=x, 0*x=0, 1*x=x, x+x=2*x, x+(y+z)=x+y+z, sin(number)=number, ...
  also perform matrix ops, or should that be a separate expansion phase?

  standard technique seems to be loop through canonic-order and simplify until a fixed point is reached

  Should revisit Joel Cohen's CAS books.
|#

(defgeneric simplify (form))
(defgeneric simplify-list (car cdr))

(defmethod simplify (form)
  form)
(defmethod simplify ((form list))
  (simplify-list (car form) (cdr form)))

(defmethod simplify-list (car cdr)
  (cons car cdr))

(defgeneric +reduce (a b)
  (:documentation "Methods for reducing added pairs - return nil or the sum")
  (:method ((a number) (b number)) (+ a b))
  (:method ((a (eql 0)) b) b)
  (:method (a b) ; maintain symbolic list
    nil))
(defmethod simplify-list ((car (eql '+)) cdr)
  #|
    identities to look for:
    0+x=x
    x+x+...+x=n*x
    (a+b)+(c+d)=a+b+c+d ; i.e. pull up subforms for canonic ordering
    x-x=0
  |#
  ;; un-nest additions
  (let (new)
    (dolist (x cdr)
      (if (and (listp x) (eql (car x) '+))
          (setf new (append new (cdr x)))
          (setf new (append new (list x)))))
    (setf cdr (canonic-order new)))
  ;; canonicalize and simplify addends
  (let ((new (loop for x in (cdr (canonic-order (cons '+ cdr)))
                collecting (simplify x))))
    (setf cdr new))
  ;; merge into rolling sums
  (let (new x)
    (dolist (y cdr)
      (let ((sum (+reduce x y)))
        (if sum
            (setf x sum)
            (progn
              (when x
                (setf new (append new (list x))))
              (setf x y)))))
    (when x
      (setf new (append new (list x))))
    (setf cdr new))
  #|
  ;; pull common factors out of subexpressions
  ;; e.g. (+ (* k ...) (* k ...) ... (* k ...)) => (* k (+ ...))
  |#
  ;; collect multiples -- e.g. (+ a a a) => (* 3 a)
  (let ((terms nil #|an alist of form, multiplicity|#))
    (dolist (form cdr)
      (let ((multiplicity 1))
        (when (and (listp form)
                   (eql (car form) '*))
          (when (numberp (second form))
            (setf multiplicity (second form)
                  form (cons '* (cddr form))))
          (case (length form)
            (1 (setf form 0))
            (2 (setf form (second form)))))
        (let ((term (assoc form terms :test #'cas-equal)))
          (if term
              (incf (cdr term) multiplicity)
              (push (cons form multiplicity) terms)))))
    (setf cdr nil)
    (dolist (term terms)
      (let ((form (car term))
            (multiplicity (cdr term)))
        (cond
          ((= 0 multiplicity))
          ((= 1 multiplicity)
           (push form cdr))
          (t (push (simplify (cons '* (cons multiplicity (list form))))
                   cdr))))))
  ;; collect sums, constant products, and negations for each term
  ;; eliminate null or unary sums
  (let ((x (car cdr)))
    (case (length cdr)
      (0 0)
      (1 x)
      (t (if (and (numberp x) (= x 0))
             (cons '+ (cdr cdr))
             (cons '+ cdr))))))

(defgeneric *reduce (a b)
  (:documentation "Methods for reducing added pairs - return nil or the sum")
  (:method ((a number) (b number)) (* a b))
  (:method ((a (eql 0)) b) 0)
  (:method ((a (eql 1)) b) b)
  (:method (a b) ; maintain symbolic list
    nil))
(defmethod simplify-list ((car (eql '*)) cdr)
  ;; un-nest multiplications
  (let (new)
    (dolist (x cdr)
      (if (and (listp x) (eql (car x) '*))
          (setf new (append new (cdr x)))
          (setf new (append new (list x)))))
    (setf cdr (canonic-order new)))
  ;; canonicalize and simplify products
  (setf cdr (loop for x in (cdr (canonic-order (cons '* cdr)))
               collecting (simplify x)))
  ;; merge into rolling products
  (let (new x)
    (dolist (y cdr)
      (let ((sum (*reduce x y)))
        (if sum
            (setf x sum)
            (progn
              (when x
                (setf new (append new (list x))))
              (setf x y)))))
    (when x
      (setf new (append new (list x))))
    (setf cdr new))
  ;; apply the distributive property
  ;; (a+b)*c = ac+bc, a*(b+c)=ab+ac
  ;; (should do a full-simplify with this; then disable it and refactor, keeping the minimal cost solution)
  (let* (dist ; products to be summed together
         (forms (reverse cdr)))
    (let ((x (car forms))
          (y (rest forms)))
      (setf dist (if (cas+? x)
                     (cdr x)
                     (list x))
            forms y))
    (dolist (f forms)
      (setf dist
            (if (cas+? f)
                (loop for x in (cdr f) ; all terms after '+
                   appending (loop for y in dist ; all previous partial products
                                collecting (list '* x y)))
                (loop for y in dist
                   collecting (list '* f y)))))
    (when (second dist) ; i.e. multiple products
      (return-from simplify-list (cons '+ dist))))
  ;; eliminate null or unary products
  (let ((x (car cdr)))
    (case (length cdr)
      (0 1)
      (1 x)
      (t (cond
           ((and (numberp x) (= 0 x))
            0)
           ((and (numberp x) (= 1 x))
            (cons '* (cdr cdr)))
           (t (cons '* cdr)))))))

(defmethod simplify-list ((car (eql '-)) cdr)
  ;; negation is simply multiplication by -1...
  (append (list '* -1) cdr))

(defgeneric /reduce (a b)
  (:documentation "Methods for reducing a/b.  Return nil if no reduction is possible.")
  (:method ((a number) (b number))
    (/ a b))
  (:method (a b)
    (when (cas-equal a b)
      1)))
(defmethod simplify-list ((car (eql '/)) cdr)
  (let* ((a (first cdr))
         (b (second cdr))
         (new (/reduce a b)))
    (if new
        new
        (list '/
              (simplify a)
              (simplify b)))))

(defmethod simplify-list ((car (eql 'd)) cdr)
  (let ((u (first cdr))
        (x (second cdr)))
    (cond
      ((cas-equal u x)
       1)
      #| Wrong.  Converts to partial derivatives...
      ((symbolp u)
       0 ;; since not equal to x
       )
      |#
      (t (full-derivative (simplify u) (simplify x))))))

(defmethod simplify-list ((car (eql 'sin)) cdr)
  (let ((angle (car cdr)))
    (if (numberp angle)
        (if (eql angle 0) ; preserve exact values
            0
            (sin angle))
        (cons car cdr))))

(defmethod simplify-list ((car (eql 'cos)) cdr)
  (let ((angle (car cdr)))
    (if (numberp angle)
        (if (eql angle 0)
            1
            (cos angle))
        (cons car cdr))))

(defun full-simplify (form)
  "Keep simplifying the form until a fixed point is reached."
  (do ((a form b)
       (b (simplify (canonic-order form))
          (simplify (canonic-order b))))
      ((cas-equal a b)
       a)))
