;;;  binge.asd --- binge system definition

;;;  Copyright (C) 2004-2006 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-D2BCAB6DEBC15505DBE8636CE51729A2.asd,v 1.28 2004/12/16 11:58:10 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-D2BCAB6DEBC15505DBE8636CE51729A2.asd,v 1.28 2004/12/16 11:58:10 wcp Exp $")

#+nil
(error "The feature NIL has been defined.  This is normally reserved to comment out sexps.")

(defpackage :binge-system
  (:use :common-lisp :asdf))

(in-package :binge-system)

(defvar *binge-base-directory*
  (make-pathname :name nil :type nil :version nil
                 :defaults (parse-namestring *load-truename*)))

(setf (logical-pathname-translations "binge")
      `(("backends;*.*.*" ,(make-pathname :directory (append (pathname-directory *binge-base-directory*)
							     '("src" "backends"))
					  :defaults *binge-base-directory*))
	("**;*.*.*" ,*binge-base-directory*)))

(defclass inactive-file (static-file) ())
(defmethod source-file-type ((c inactive-file) (s module))
  "lisp")

(defsystem binge
    :name "BINGE"
    :author "Walter C. Pelissero <walter@pelissero.de>"
    :maintainer "Walter C. Pelissero <walter@pelissero.de>"
    ;; :version "0.0"
    :description "Bovine INterface GEnerator"
    :long-description
    "Bovine INterface GEnerator is a Common Lisp program that
extracts types, variable/function declarations and symbolic
constants from C header files and dumps a foreign function
interface for them."
    :licence "GPL"
    :depends-on (:npg
		 #+sbcl :sb-posix
		 :sclf)
    :components
    ((:doc-file "COPYING")
     (:doc-file "README")
     (:doc-file "ANNOUNCE")
     (:static-file ".project")		; project name
     (:module "src"
	      ;; The actual source code of BINGE
	      :components
	      ((:module "backends"
			:depends-on ("package" "backend" "compiler")
			:serial t
			:components
			;; backends can live peacefully together with
			;; each other, so we load everything that
			;; makes sense on this platform
			((:file "uffi")
			 (#+(or sbcl cmu) :file #-(or sbcl cmu) :inactive-file "sbcl")
			 (#+cmu :file #-cmu :inactive-file "cmucl")
			 (#+lispworks :file #-lispworks :inactive-file "lispworks")
			 (#+clisp :file #-clisp :inactive-file "clisp")))
	       (:file "package")
	       (:file "ss" :depends-on ("package"))
	       (:file "lexer" :depends-on ("package" "ss" "grammar"))
	       (:file "grammar" :depends-on ("package"))
	       (:file "decl" :depends-on ("package"))
	       (:file "binge" :depends-on ("package" "compiler" "decl"))
	       (:file "parser" :depends-on ("package" "grammar"))
	       (:file "backend" :depends-on ("package" "decl"))
	       (:file "namespace" :depends-on ("package" "decl"))
	       (:file "compiler" :depends-on ("package" "decl" "namespace"))))
     (:module "example"
	      :depends-on ("src")
	      :components
	      ((:file "package")
	       (:file "zlib" :depends-on ("package"))
	       (:file "ncurses" :depends-on ("package"))
	       (:file "db" :depends-on ("package"))
	       (:file "gtk" :depends-on ("package"))
	       (:file "xlib" :depends-on ("package"))))))

(defmethod perform ((o test-op) (c (eql (find-system 'binge))))
  (oos 'load-op 'binge-tests)
  (oos 'test-op 'binge-tests :force t))
