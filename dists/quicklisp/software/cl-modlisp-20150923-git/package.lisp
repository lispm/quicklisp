;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: modlisp; -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          package.lisp
;;;; Purpose:       Package definition for modlisp package
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  Dec 2002
;;;;
;;;; $Id$
;;;; *************************************************************************

(in-package #:cl-user)

(defpackage #:modlisp
  (:nicknames #:ml)
  (:use #:cl #:kmrcl)
  (:export

   ;; variables.lisp
   #:*modlisp-socket*
   #:*number-worker-requests*
   #:*number-server-requests*
   #:*ml-server*
   #:*close-modlisp-socket*

   ;; base.lisp
   #:modlisp-start
   #:modlisp-stop
   #:modlisp-stop-all
   #:header-value
   #:write-header-line
   #:set-close-modlisp-socket

   ;; utils.lisp
   #:output-ml-page
   #:output-html-page
   #:output-xml-page
   #:with-ml-page
   #:query-to-alist
   #:redirect-to-location
   ))

