MINI-CAS, a MINImal Computer Algebra System

Copyright (c) 2011 Daniel Herring
Distributed under the Boost Software License, Version 1.0.
   (See accompanying file LICENSE_1_0.txt or copy at
         http://www.boost.org/LICENSE_1_0.txt)


** Install **

What?!?  Not much to do.  I provide an ASDF system definition for convenience.


** What it does **
symbolic math
(sufficient for deriving my robot kinematic and dynamic equations)
(hopefully without introducing bugs)


** Why another CAS **
This is a hard question.  Good CAS software is hard to write, there are numerous mature systems, etc.

I was looking for
- a Common Lisp CAS library (for integration with other code)
- basic ops: + - * / sin cos
- matrix support
- derivatives

Original plan was to use [Open]Axiom/Fricas or Maxima; but these require glue code to generate and parse "human-style" input, spawn subprocesses, etc.  Not to mention their massive size.

Then I tried modernizing Mock-MMA.  Not fun.  Got the obvious things fixed; but there were still major issues (and no matrix support).

Where are the CL CAS libraries?  Not finding them, I slapped this together.

Anyone wanting to make a real CAS out of this should read some CAS books or at least know abstract algebra.  

Joel Cohen wrote a set of practical CAS books; most others focus on details such as polynomial factorization, Groebner bases, etc.
http://web.cs.du.edu/~jscohen/
Computer Algebra and Symbolic Computation: Elementary Algorithms, AK Peters Ltd., 2002
Computer Algebra and Symbolic Computation: Mathematical Methods, AK Peters Ltd., 2003


** License **

Normal disclaimers apply.  I only spot-checked the math; hopefully the simplifications are simple enough to not introduce bugs.  License described at http://www.boost.org/users/license.html
