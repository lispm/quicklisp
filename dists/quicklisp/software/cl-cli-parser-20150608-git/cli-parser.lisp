;;;; $Id: cli-parser.lisp,v 1.4 2005/07/29 21:27:03 dbueno Exp $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Denis Bueno
;;;;
;;;; This software is provided without warranty of any kind, and is
;;;; released under the BSD license. I reserve the right to change
;;;; that, but only in the "more permissive license" direction, if
;;;; such is possible.
;;;;
;;;; Copyright (c) 2004, Denis Bueno
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or
;;;; without modification, are permitted provided that the following
;;;; conditions are met:
;;;;
;;;;   Redistributions of source code must retain the above copyright
;;;;   notice, this list of conditions and the following
;;;;   disclaimer.
;;;;
;;;;   Redistributions in binary form must reproduce the above
;;;;   copyright notice, this list of conditions and the following
;;;;   disclaimer in the documentation and/or other materials
;;;;   provided with the distribution.
;;;;
;;;;   The name of Denis Bueno may not be used to endorse or promote
;;;;   products derived from this software without specific prior written
;;;;   permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
;;;; CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
;;;; INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;;;; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
;;;; ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;;;; POSSIBILITY OF SUCH DAMAGE.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Command-line argument parser. Mostly parses options of the same
;;;; form that getopt parses.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defpackage cli-parser
  (:use :common-lisp :common-lisp-user)
  (:export #:cli-parse
           #:cli-parse-hash
           #:cli-parse-assoc
	   #:cli-formatted-options
	   #:cli-usage

           ;; The CLI-OPTION class
           #:cli-option
           #:cli-option-abbr
           #:cli-option-full
           #:cli-option-requires-arguments
           #:cli-option-description
           #:cli-option-example
           #:cli-option-p
           #:make-cli-option)
  (:documentation
"Used for command-line-interface parsing, in the same tradition
as getopt, but, a bit more convenient. The three main functions
are:

* CLI-PARSE
* CLI-PARSE-HASH
* CLI-PARSE-ASSOC

CLI-PARSE actually just calls CLI-PARSE-HASH, which will parse a
list of command-line arguments against a list of CLI-OPTION
objects. CLI-PARSE-ASSOC, instead of returning a hash table of
results like CLI-PARSE-HASH does, returns an assoc list of
results.

The idea is that you create a bunch of cli-option instances (via
MAKE-CLI-OPTION) which represent all of the valid options the
user may pass in to your program. The actual options passed
in (as a list of strings, one for each option) along with the
list of valid options are passed to cli-parse, which will give
you a table of mappings, from the option to the setting specified
by the user."))

(in-package :cli-parser)
(declaim (optimize (debug 3) (safety 3) (speed 0) (compilation-speed 0)))

;;; TODO
;;;
;;; * decide what to do if see an option like -cookycrisp: continuable
;;;   error/condition restart, ignore?
;;;
;;; * depend on SPLIT-SEQUENCE rather than own STRING-TOKENIZE

(defclass cli-option ()
  ((abbreviation :initarg :abbr :accessor cli-option-abbr
                 :type (or null string))
   (longname :initarg :full :accessor cli-option-full
             :type string)
   (argumentsp :initform nil
               :initarg :requires-arguments
               :accessor cli-option-requires-arguments
               :type (member nil t :optional))
   (description :initform "Default description."
                :initarg :description :accessor cli-option-description
                :type string)
   (example :initarg :example :accessor cli-option-example
            :type string)))

(defmacro pprint-clos-class (instance slots stream
                             &key
                             (inter-slot-newline-style :linear)
                             (intra-slot-newline-style :fill)
                             (unbound-msg "<unbound>")
                             (slot-value-callback '(lambda (a1 a2)
                                                    (declare (ignore a1))
                                                    a2)))
  "Pretty print the SLOTS of a CLOS class INSTANCE, to
  STREAM.

  INTER-SLOT-NEWLINE-STYLE and INTRA-SLOT-NEWLINE-STYLE may be any
  value appropriate appropriate as the first argument to
  PPRINT-NEWLINE. A newline of INTER-SLOT-NEWLINE-STYLE will be
  printed between each of the slot-name/slot-value pairs of each
  slot in SLOTS. A newline of INTRA-SLOT-NEWLINE-STYLE will be
  printed between the slot-name and the slot-value of each slot
  in SLOTS.

  UNBOUND-MSG should be a string which will be printed as the
  slot-value for any slot in INSTANCE which is unbound.

  SLOT-VALUE-CALLBACK should be a function of two arguments, the
  slot-name and the slot-value, which should return an object
  which will be printed in place of the slot-value for
  slot-name.

  Example:

  > (defclass foo () (a b))
  #<STANDARD-CLASS FOO>
  > (defmethod cl:print-object ((f foo) stream)
      (pprint-clos-class f (a b) stream))
  #<STANDARD-METHOD PRINT-OBJECT (FOO T) {4865E569}>
  > (make-instance 'foo)
  #<FOO :A <unbound> :B <unbound>>
  > (setf (slot-value * 'a) 'bar)
  BAR
  > **
  #<FOO :A BAR :B <unbound>>"
  (macrolet ((with-gensyms ((&rest syms) &body body)
               `(let ,(loop for sym in syms
                            collect `(,sym (gensym ,(symbol-name sym))))
                  ,@body)))
    (with-gensyms (ginstance gslots gstream)
      `(let ((,ginstance ,instance)
             (,gslots ',slots)
             (,gstream ,stream))
         (print-unreadable-object (,ginstance ,gstream
                                   :type (class-name (class-of ,ginstance)))
           (loop for slot in ,gslots
                 for first-time-p = t then nil do
                 (unless first-time-p (write-char #\space ,gstream))
                 (pprint-newline ,inter-slot-newline-style ,gstream)
                 (write-string (format nil ":~a" (symbol-name slot)) ,gstream)
                 (write-char #\space ,gstream)
                 (pprint-newline ,intra-slot-newline-style ,gstream)
                 (if (slot-boundp ,ginstance slot)
                     (write (funcall ,slot-value-callback
                                     slot
                                     (slot-value ,ginstance slot))
                            :stream ,gstream)
                     (write-string ,unbound-msg ,gstream))))))))
(defmethod cl:print-object ((o cli-option) stream)
  (pprint-clos-class o (abbreviation longname argumentsp description example)
                     stream))

(defparameter *single-dash* "-"
  "Short option prefix.")
(defparameter *double-dash* "--"
  "Long option prefix.")
(defparameter *option-value-sep* " "
  "String used to separate option values.")
(defparameter *for-example-text* "e.g"
  "String used to display for example.")

(defun formatted-option-string (option)
  (format nil "~a~a, ~a~a~a ~28t~a~%~28t~a ~a~%" 
	  ;;(if (cli-option-abbr option)
	  ;;    ("      " 
	  *single-dash* (cli-option-abbr option)
	  *double-dash* (cli-option-full option)
	  (case (cli-option-requires-arguments option)
	    ((t) "=ARG  ")
	    (:optional "[=ARG]")
	    ((nil) "    "))
	  (cli-option-description option)
	  *for-example-text*
	  (cli-option-example option)))

(defun cli-formatted-options (option-list)
  (format nil "~{~a~}" (mapcar #'formatted-option-string option-list)))

(defun cli-usage (command-name options-list)
  "Print the command line usage"
  (format t "~%Usage: ~a [options]~%~%Where [options] are:~%~a~%" command-name 
	  (cli-formatted-options options-list)))


;; Main interface function. Call cli-parse on a list of the form:
;; ("--opt1=val1" "val2" "-p=another-option" "-q"), or a list of the
;; form ("--opt1=val1 val2" "-p=another-option" "-q") - both are
;; equivalent. Returns an association list of the form:
;; (("option-name" ("val1" ... "valn")) ...).  The cli-opts argument
;; should be a list of cli-option structures. They specify the
;; allowable command-line interface options.

(defun cli-parse (args cli-opts)
  "See CLI-PARSE-HASH."
  (cli-parse-hash args cli-opts))


(defun cli-parse-assoc (args cli-opts)
   "Parses command-line arguments much in the same format as the
cl-args that getopt() parses. That is, if you call any program
with: './prgm --opt1=value1 value2 -n', and you give
\"--opt1=value1\", \"value2\" and \"-n\" to cli-parse-assoc, it
returns and assoc-list of the form ((\"opt1\" (\"value1\"
\"value2\")) (\"n\" nil))."
   (to-full-opt-names (cli-parse-assoc-aux (coalesce-options args) nil)
                      cli-opts))
(defun cli-parse-assoc-aux (args results)
  "Helper for cli-parse."
  (cond ((endp args) (reverse results))
        (t (cli-parse-assoc-aux (cdr args)
                          (cons (list (opt-name (car args))
                                      (opt-values (car args)))
                                results)))))

(defun cli-parse-hash (args cli-opts)
  "Parses command-line arguments in the same form as specified
for CLI-PARSE-ASSOC, but returns a hash-table of the results,
instead of an assoc list."
  (cli-parse-hash-aux (coalesce-options args) cli-opts))
(defun cli-parse-hash-aux (args cli-opts)
  (let ((ret (make-hash-table :test #'equal)))
    (mapcar #'(lambda (arg val)
                (setf (gethash (abbr->full-opt-name arg cli-opts) ret) val))
            (mapcar #'opt-name args)
            (mapcar #'opt-values args))
    ret))


(defun coalesce-options (args)
   "Will convert a list of the form (\"--option-name=val1[,]\"
\"val2[,]\" \" ... \"valn\" ...) to a list of the form
\(\"--option-name=val1 val2 val3\" ...)."
   (coalesce-options-aux args nil))
(defun coalesce-options-aux (args results)
  "Helper for coalesce-options."
  (cond ((or (endp args)
             (endp (cdr args))) (nreverse (cons (car args) results)))
        ((and (opt-p (first args))
              (not (opt-p (second args))))
         (coalesce-options-aux
          (cons (concatenate 'string
                             (first args)
                             " "
                             (second args))
                (cddr args))
          results))
        (t (coalesce-options-aux (cdr args) (cons (car args) results)))))

(defun opt-name (opt)
  "Extract the name of an option: for example \"opt1\" is the name
from \"--opt1=val1\". Will return the argument if it is neither."
  (cond ((abbr-opt-p opt)
         (subseq opt 1 (end-opt-name opt)))
        ((full-opt-p opt)
         (subseq opt 2 (end-opt-name opt)))
        (t opt)))

(defun end-opt-name (opt)
  "Returns the index of the end of the option-name. For example,
end-opt-name would return 6 for the option \"--opt1=val1\""
  (let ((equal-pos (search "=" opt))
        (space-pos (search " " opt)))
    (cond ((and equal-pos space-pos) (min equal-pos space-pos))
          ((or  equal-pos space-pos) (or equal-pos space-pos))
          (t (length opt)))))

(defun abbr->full-opt-name (opt cli-opts)
  "Converts an abbreviated option (i.e. \"o\") to its corresponding full
option name. Returns the argument if no conversion is performed."
  (cond ((endp cli-opts) opt)
        ((equal opt (cli-option-abbr (car cli-opts)))
         (cli-option-full (car cli-opts)))
        ((equal opt (cli-option-full (car cli-opts)))
         opt)
        (t (abbr->full-opt-name opt (cdr cli-opts)))))

(defun to-full-opt-names (cl-args cli-opts)
  "Converts any abbreviated option list of command-line options to the
full option name."
  (let ((result nil)
        (cli-short-opts (mapcar #'cli-option-abbr cli-opts)))
    (dolist (obj cl-args)
      (if (member (car obj) cli-short-opts :test #'string-equal)
          (push (cons (abbr->full-opt-name (car obj) cli-opts)
                      (cdr obj))
                result)
        (push obj result)))
    (nreverse result)))

(defun to-full-opt-name (argname cli-opts)
  "Convert an option name to the full one, if necessary. Change \"o\"
to \"outfile\", for example."
  (dolist (cli-opt cli-opts)
    (when (string= argname (cli-option-abbr cli-opt))
      (return (cli-option-full cli-opt)))))

(defun opt-values (opt)
  "Extract the values of an option: for example \"val1\" is the value
from \"--opt1=val1\". If no values are specified, this function
returns nil."
  (let ((opt (string-trim '(#\Space) opt))
        (start (or (search "=" opt) (search " " opt))))
    (if start
        ;; separate entries by *option-value-sep*
        (string-tokenize (subseq opt (1+ start)) (list *option-value-sep* ","))
      nil)))



;; The distinction between whether an option is abbreviated or full
;; doesn't really matter (as far as getting the names and the
;; corresponding values), but it does matter for checking that options
;; are given in the correct form (i.e. -o=val instead of --o=val for
;; short options, and --thing=val instead of -thing=val for long
;; options).
(defun opt-p (opt)
  "Evaluates to true if opt is an abbreviated or a full option."
  (or (abbr-opt-p opt)
      (full-opt-p opt)))

(defun abbr-opt-p (opt)
  "Test whether opt is a short option of the form \"-o[=val]\""
  (and (stringp opt)
       (>= (length opt) 2)
       (equal (subseq opt 0 (length *single-dash*)) *single-dash*)
       (<= (end-opt-name opt) 2)))

(defun full-opt-p (opt)
  "Test whether opt is a long option of the form \"--opt[=val]\""
  (and (stringp opt)
       (>= (length opt) 4)
       (equal (subseq opt 0 (length *double-dash*)) *double-dash*)
       (> (end-opt-name opt) 3)))

;;; STRING-TOKENIZE

(defun string-tokenize (str val-separators &key (include-separators nil))
  "Breaks up a given string into string components by splitting
the string every time an element of val-separator is
encountered. Returns a list of strings, which are all the
tokens. If include-separators is non-nil, the separators
themselves will be included in the parse."
  (string-tokenize-aux str val-separators nil include-separators))
(defun string-tokenize-aux (str val-separators vals include-separators)
  "Helper for string-tokenize."
  ;; the if in the return clause takes care of the extra delimeter
  ;; variable consed onto the end of the list. This implementation is
  ;; cleaner than the alternative (which is to keep around a variable
  ;; which will allow the function to distinguish between the
  ;; first-time it's called, and the rest of the times it's called.
  (cond ((null str) (if include-separators
                        (remove-if #'(lambda (item) (equal "" item))
                                   (butlast (nreverse vals)))
                        (remove-if #'(lambda (item) (equal "" item))
                                   (nreverse vals))))
        (include-separators
         (multiple-value-bind (first separator)
             (st-first-val str val-separators)
           (string-tokenize-aux (st-rest-of str val-separators)
                                val-separators
                                (cons separator
                                      (cons first vals))
                                include-separators)))
        (t (string-tokenize-aux (st-rest-of str val-separators)
                                val-separators
                                (cons (st-first-val str val-separators) vals)
                                include-separators))))
(defun st-first-val (str val-separators)
  "Returns the first token by parsing str. Analagous to car, but for the
string tokenizer, instead of for lists."
  (let ((best-idx (st-closest-val str val-separators)))
    (values (subseq str 0 best-idx)
            (unless (equal (length str) best-idx)
              (subseq str best-idx (1+ best-idx))))))
(defun st-rest-of (str val-separators)
  "Returns the rest of the string, not including the first
token. Analagous to cdr, but for the string tokenizer, instead of for
lists."
  (let* ((str-length (length str))
         (best-idx (st-closest-val str val-separators)))
    (if (equal str-length best-idx)
        nil
        (subseq str (1+ best-idx)))))
(defun st-closest-val (str val-separators)
  "Returns the character in the string which both matches any of the
val-separators and minimizes the distance between it and the index 0
of the string. If there is no match, returns the length of the
string."
  (let* ((str-length (length str))
         (best-idx str-length)
         (sep-match nil))
    (mapc #'(lambda (separator)
              (let ((search-result (search separator str)))
                (unless (null search-result)
                  (when (< search-result best-idx)
                    (setf best-idx search-result)
                    (setf sep-match separator)))))
          val-separators)
    best-idx))


(pushnew :cli-parser *features*)
