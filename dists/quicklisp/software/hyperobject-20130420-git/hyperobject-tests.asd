;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          hyperobject-tests.asd
;;;; Purpose:       ASDF system definitionf for hyperobject testing package
;;;; Author:        Kevin M. Rosenberg
;;;; Date Started:  Apr 2003
;;;;
;;;; $Id$
;;;; *************************************************************************

(defpackage #:hyperobject-tests-system
  (:use #:asdf #:cl))
(in-package #:hyperobject-tests-system)

(defsystem hyperobject-tests
    :depends-on (:rt :hyperobject)
    :components ((:file "tests")))

(defmethod perform ((o test-op) (c (eql (find-system :hyperobject-tests))))
  (or (funcall (intern (symbol-name '#:do-tests)
		       (find-package '#:regression-test)))
      (error "test-op failed")))
