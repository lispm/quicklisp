;;;; -*- Mode: Lisp -*-

;;;; string-enumeration.lisp --
;;;;
;;;; See file COPYING for copyright and licensing information.

(in-package "CL.EXTENSIONS.DACF.ENUMERATIONS")

;;;;===========================================================================
;;;; String Enumeration.

(defclass string-enumeration (vector-enumeration)
  ()
  (:documentation "The String Enumeration Class."))

(defgeneric string-enumeration-p (x)
  (:method ((x t)) nil)
  (:method ((x string-enumeration)) t))

(defmethod enumerate ((s string) &key (start 0) end &allow-other-keys)
"Calling ENUMERATE on a STRING returns a STRING-ENUMERATION instance.

A STRING-ENUMERATION traverses the STRING s using the accessor CHAR.

START must be an integer between 0 and (length S). end must be an
integer between 0 and (length S) or NIL. The usual CL semantics
regarding sequence traversals applies."
  (make-instance 'string-enumeration :object s :start start :end end))

(defmethod next ((x string-enumeration) &optional default)
  (declare (ignore default))
  (prog1 (char (enumeration-object x) (enumeration-cursor x))
    (incf (enumeration-cursor x))))


(defmethod previous ((x string-enumeration) &optional default)
  (declare (ignore default))
  (decf (enumeration-cursor x))
  (char (enumeration-object x) (enumeration-cursor x)))



(defclass simple-string-enumeration (string-enumeration)
  ()
  (:documentation "The Simple String Enumeration Class."))


(defgeneric simple-string-enumeration-p (x)
  (:method ((x t)) nil)
  (:method ((x simple-string-enumeration)) t))


;;; The next methods may be a good idea, but CL does not support
;;; DEFMETHODs with a "type" discriminant, and SIMPLE-STRING is a type
;;; and not a class.  This does not seem to cause problems in LW and
;;; CMUCL, but it breaks ECL.

#|
(defmethod enumerate ((x simple-string) &key (start 0) end &allow-other-keys)
  (make-instance 'simple-string-enumeration :object x :start start :end end))


(defmethod next ((x simple-string-enumeration) &optional default)
  (declare (ignore default))
  (prog1 (schar (enumeration-object x) (enumeration-cursor x))
    (incf (enumeration-cursor x))))


(defmethod previous ((x simple-string-enumeration) &optional default)
  (declare (ignore default))
  (decf (enumeration-cursor x))
  (schar (enumeration-object x) (enumeration-cursor x)))
|#

;;; end of file -- string-enumerations.lisp --
