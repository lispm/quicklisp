(in-package :cl-user)

(defpackage #:box.dgen
  (:use #:cl)
  (:export #:stage
           #:make-stage
           #:print-stage
           #:width
           #:height
           #:grid
           #:seed
           #:cell
           #:x
           #:y
           #:cell-index
           #:carved
           #:region-id))
