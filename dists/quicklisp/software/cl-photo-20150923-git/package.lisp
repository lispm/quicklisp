;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          package.lisp
;;;; Purpose:       Package file for cl-photo
;;;; Programmer:    Kevin M. Rosenberg
;;;; Date Started:  April 2005
;;;;
;;;; $Id$
;;;;
;;;; This file, part of cl-photo, is Copyright (c) 2005 by Kevin M. Rosenberg
;;;;
;;;; cl-photo users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License v2
;;;; (http://www.gnu.org/licenses/gpl.html)
;;;;
;;;; *************************************************************************

(in-package #:cl-user)

(defpackage #:cl-photo
  (:use #:common-lisp #:kmrcl)
  (:nicknames #:photo)
  (:export

   ;; cameras.lisp
   #:pixel-dimensions
   #:pixel-size
   #:imager-dimensions
   #:output-dimensions
   #:*digital-cameras*
   #:*cameras*

   ;; fov.lisp
   #:aov
   #:aov-format
   #:fov
   #:magnification
   #:bellows-factor
   #:gaussian-lens
   #:close-up
   #:extension-tube

   ;; dof.lisp
   #:print-magnification
   #:coc
   #:coc-format
   #:coc-pixels
   #:coc-pixels-format
   #:coc-airy
   #:dof
   #:hyperfocal
   #:effective-aperture
   #:rayleigh-limit
   #:maximum-sharpness-aperture

   ;; tables.lisp
   #:hyperfocal-table
   #:aov-table
   #:fov-table
   #:dof-table
   ))

