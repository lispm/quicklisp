(in-package :mini-cas)

(defgeneric cas-substitute (new old form)
  (:documentation "Like CL:substitute, but do a deep recursive replacement.")
  (:method (new old form)
    (if (cas-equal old form)
        new
        form)))

(defmethod cas-substitute (new old (form list))
  (if (cas-equal old form)
      new
      (loop for f in form
         collecting (cas-substitute new old f))))

(defmethod cas-substitute (new old (form array))
  (if (cas-equal old form)
      new
      (let ((result (make-array (array-dimensions form))))
        (dotimes (i (array-dimension form 0))
          (dotimes (j (array-dimension form 1))
            (setf (aref result i j) (cas-substitute new old (aref form i j)))))
        result)))
