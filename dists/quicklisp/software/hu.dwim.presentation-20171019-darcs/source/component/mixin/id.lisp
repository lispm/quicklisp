;;; -*- mode: Lisp; Syntax: Common-Lisp; -*-
;;;
;;; Copyright (c) 2009 by the authors.
;;;
;;; See LICENCE for details.

(in-package :hu.dwim.presentation)

;;;;;;
;;; id/mixin

(def (component e) id/mixin ()
  ((id :type string :documentation "A life time permanent string identifier."))
  (:documentation "A COMPONENT with a life time permanent string identifier."))

(def layered-method render-component-stub :in xhtml-layer ((-self- id/mixin))
  (setf (rendered-component? -self-) :stub)
  (assert (id-of -self-))
  <div (:id ,(id-of -self-) :style "display: none;") "">)

(debug-only
  (def method (setf id-of) :before (new-value (self id/mixin))
    (assert (not (id-of self)))))

;;;;;;
;;; frame-unique-id/mixin

(def (component e) frame-unique-id/mixin (refreshable/mixin id/mixin)
  ((id nil))
  (:documentation "A COMPONENT with a permanent frame unique string identifier that is set at the first REFRESH-COMPONENT and never changed afterwards."))

(def function generate-unique-component-id (&optional prefix)
  (generate-unique-string prefix))

(def function ensure-frame-unique-id (component)
  (or (id-of component)
      (setf (id-of component) (generate-unique-component-id))))

(def layered-method refresh-component :before ((self frame-unique-id/mixin))
  (ensure-frame-unique-id self))
