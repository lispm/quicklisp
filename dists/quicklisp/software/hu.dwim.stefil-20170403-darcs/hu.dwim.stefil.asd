;;; -*- mode: Lisp; Syntax: Common-Lisp; -*-
;;;
;;; Copyright (c) 2006 by the authors.
;;;
;;; See LICENCE for details.

(defsystem :hu.dwim.stefil
  :defsystem-depends-on (:hu.dwim.asdf)
  :class "hu.dwim.asdf:hu.dwim.system"
  :description "A Simple Test Framework In Lisp."
  :depends-on (:alexandria)
  :components ((:module "source"
                :components ((:file "asserts" :depends-on ("infrastructure"))
                             (:file "package")
                             (:file "duplicates" :depends-on ("package"))
                             (:file "fixture" :depends-on ("test"))
                             (:file "infrastructure" :depends-on ("duplicates"))
                             (:file "test" :depends-on ("infrastructure"))
                             (:file "suite" :depends-on ("infrastructure" "test"))))))

(defmethod perform :after ((o hu.dwim.asdf:develop-op) (c (eql (find-system :hu.dwim.stefil))))
  (asdf:load-system :hu.dwim.stefil+swank))

(defsystem :hu.dwim.stefil/test
  :defsystem-depends-on (:hu.dwim.asdf)
  :class "hu.dwim.asdf:hu.dwim.test-system"
  :licence "BSD / Public domain"
  :depends-on (:hu.dwim.stefil)
  :components ((:module "test"
                :components ((:file "package")
                             (:file "basic" :depends-on ("package"))
                             (:file "fixtures" :depends-on ("package" "basic"))))))

(defsystem :hu.dwim.stefil/documentation
  :defsystem-depends-on (:hu.dwim.asdf)
  :class "hu.dwim.asdf:hu.dwim.documentation-system"
  :depends-on (:hu.dwim.stefil.test
               :hu.dwim.presentation)
  :components ((:module "documentation"
                :components ((:file "package")
                             (:file "stefil" :depends-on ("package"))))))
