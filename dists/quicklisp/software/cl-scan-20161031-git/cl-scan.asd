(asdf:defsystem #:cl-scan
  :description "a simple port scanner"
  :author "case <case@capsulecorp.org>"
  :license "ISC"
  :serial t 
  :depends-on (#:usocket
	       #:inferior-shell
	       #:lparallel)
  :components ((:file "package")
               (:file "cl-scan")))


