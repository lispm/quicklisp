# Some utilities for vectors

All these functions are inside of package `#:vec`

## Constructing and destructing vectors

*Function*

`vector-of type &rest elemnets`

Create a vector of type `type` containing `elements`.


*Function*

`slice vector &optional start end`

Create a slice of a `vector`, which is a subvector from `start` to `end` of `vector` and points to the same entries as `vector`.


*Macro*

`vector-bind list vec &body body`

`list`: A variable list

`vec`: A vector

Bind elements of `vec` to variables in `list`.


## Setting and creating vectors

*Function*

`mapvs size function vec &rest vecs`

For the first `size` entries of `vec` and `vecs` call `function` on an element at the same position and write it at the same position of `vec`.


*Function*

`mapvec function vec &rest vecs`

Same as  `apply #'map 'vector vec vecs`.


*Function*

`mapv function vec &rest vecs`

Same as `apply #'map-into vec vecs`.

*Function*

`vec-copy vec`

Create a new vector with the same contents as `vec`.


*Function*

`setv vec vec0`

Set all elements of `vec` to `vec0` without creating a new vector.


# Math functions

## Simple math

In following each pair of functions do the same, but the first version of the function create a new vector, the second write the result to the first vector.

Addition and subtraction only take vectors as arguments, multiplication and division only take a vector for the first argument.

```
;;Addition
v+
incv
;;Subtraction
v-
decv
;;Multiplication
v*
mulv
;;Division
v/
divv
```


## Vector specific math

*Function*

`absvec vec`

The length of the vector `vec`.

*Function*

`normalize vec`

Normalize the vector `vec` and get it.

*Function*

`unitvec vec`

A new unit vector with the direction of `vec`.
Same as `(normalize (copy-vec vec))`.

*Function*

`distance vec vec0`

Distance between the vectors `vec` and `vec0`


*Function*

`scalar vec vec0`

The scalar product of `vec` and `vec0`.

*Function*

`unit-scalar vec vec0`

Optimized version of `(scalar (unitvec vec) (unitvec vec0))`.

*Function*

`angle vec vec0`

Angle between `vec` and `vec0`.

*Function*

`cross3 vec vec0`

Cross product of the 3D-vectors `vec` and `vec0`.


# Some other functions

```
orthogonal-factor
orthogonal-vector
line-vector
line-distance
```

