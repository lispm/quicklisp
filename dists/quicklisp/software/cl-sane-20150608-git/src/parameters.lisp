(in-package :sane)

(defclass parameters ()
  ((format          :reader get-format          :initarg :format)
   (last-frame      :reader last-frame-p        :initarg :last-frame)
   (bytes-per-line  :reader get-bytes-per-line  :initarg :bytes-per-line)
   (pixels-per-line :reader get-pixels-per-line :initarg :pixels-per-line)
   (lines           :reader get-lines           :initarg :lines)
   (depth           :reader get-depth           :initarg :depth)))

(defun get-parameters (device)
  "Read parameters from DEVICE: evaluates to an instance of SANE::PARAMETERS"
  (with-foreign-object (cparams '(:struct sane-lowlevel::SANE_Parameters))
    (sane-lowlevel::sane_get_parameters (handle device)
                                        cparams)
    (with-foreign-slots ((sane-lowlevel::format
                          sane-lowlevel::last_frame
                          sane-lowlevel::bytes_per_line
                          sane-lowlevel::pixels_per_line
                          sane-lowlevel::lines
                          sane-lowlevel::depth) cparams
                         (:struct sane-lowlevel::SANE_Parameters))
      
      (make-instance 'parameters
                     :format (lispify-sane-frame-format sane-lowlevel::format)
                     :last-frame (when (= 1 sane-lowlevel::last_frame))
                     :bytes-per-line sane-lowlevel::bytes_per_line
                     :pixels-per-line sane-lowlevel::pixels_per_line
                     :lines sane-lowlevel::lines
                     :depth sane-lowlevel::depth))))

(defun lispify-sane-frame-format (fmt)
  (case fmt
    (:sane_frame_gray :gray)
    (:sane_frame_rgb :rgb)
    (:sane_frame_red :red)
    (:sane_frame_green :green)
    (:sane_frame_blue :blue)
    (t (error "Invalid frame format."))))
