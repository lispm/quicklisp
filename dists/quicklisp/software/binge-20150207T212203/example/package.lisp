;;;  package.lisp --- examples package

;;;  Copyright (C) 2004 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-8CA8636A94010C34743C8D91F1975539.lisp,v 1.3 2004/04/15 22:52:16 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-8CA8636A94010C34743C8D91F1975539.lisp,v 1.3 2004/04/15 22:52:16 wcp Exp $")

(defpackage :binge-examples
  (:use :common-lisp :binge)
  (:export #:create-db-ffi
	   #:create-zlib-ffi
	   #:create-ncurses-ffi
	   #:create-gtk-ffi
	   #:create-xlib-ffi))
