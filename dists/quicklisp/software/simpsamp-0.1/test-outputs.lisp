(in-package #:simpsamp)

;;; TEST-COMPUTED-SAMPLE can only be used for samples of type
;;; SEQUENCE.  For sample functions with other types of output,
;;; TEST-ACCUMULATED-SAMPLE can be used to iterate over the sample.

(defmacro test-accumulated-sample ((label accumulate
				    input-set-form requested-sample-size-form)
				   &body body)
  "Evaluates BODY, which accumulates the elements of a sample taken
from a set.  After evaluating BODY, tests the sample against the input
set and the requested sample size.  If there is an error in the
resulting sample, an error is signalled.  LABEL is included in any
error messages.

ACCUMULATE is defined with MACROLET for the evaluation of BODY.
\(ACCUMULATE ELEMENT) is used to accumulate each ELEMENT of the sample
that is taken.

INPUT-SET-FORM is evaluated to produce the input set (or an equivalent
copy), as a SEQUENCE.

REQUESTED-SAMPLE-SIZE-FORM is evaluated to produce the requested size
of the sample to take.

Note that the input set and requested sample size are not passed to
anything within BODY, but must be equivalent to the corresponding
arguments to the function or macro form used in BODY to produce the
sample."
  `(let ((input-set ,input-set-form)
	 (requested-sample-size ,requested-sample-size-form)
	 (sample (with-list-accumulator (,accumulate)
		   ,@body)))
     (test-computed-sample ',label sample 't input-set requested-sample-size)))

(defmacro test-do-sample ((label accumulate initial-form result-form
			   initial-set-form requested-sample-size-form)
			  &body body)
  "Evaluates BODY, which iterates over the elements of a sample taken
from a set.  After evaluating BODY, tests the sample against the input
set and the requested sample size.  If there is an error in the
resulting sample, an error is signalled.  LABEL is included in any
error messages.

ACCUMULATE is defined with MACROLET for the evaluation of BODY.  The
result of BODY must be the result a DO-SAMPLE-OF-* macro form that
includes at least the expression (ACCUMULATE ELEMENT) in its body
\(where ELEMENT is the variable naming the current element in the loop
over the resulting sample).  BODY should consist solely of the
DO-SAMPLE-OF-* macro form, and the body of that macro form should
consist solely of (ACCUMULATE ELEMENT).

INITIAL-FORM is defined with MACROLET for the evaluation of BODY.
\(INITIAL-FORM) must be the :INITIAL-FORM argument to the
DO-SAMPLE-OF-* macro form.

RESULT-FORM is defined with MACROLET for the evaluation of BODY.
\(RESULT-FORM) must be the :RESULT-FORM argument to the DO-SAMPLE-OF-*
macro form.

INPUT-SET-FORM is evaluated to produce the input set (or an equivalent
copy), as a SEQUENCE.

REQUESTED-SAMPLE-SIZE-FORM is evaluated to produce the requested size
of the sample to take.

Note that the input set and requested sample size are not passed to
anything within BODY, but must be equivalent to the corresponding
arguments to the DO-SAMPLE-OF-* macro form used in BODY to produce the
sample."
  (jpl-util:with-gensyms (initial-set%
			  requested-sample-size%
			  accumulate-result%
			  evaluated-initial-form%
			  began-body%
			  evaluated-result-form%)
    `(let ((,initial-set% ,initial-set-form)
	   (,requested-sample-size% ,requested-sample-size-form)
	   (,evaluated-initial-form% nil)
	   (,began-body% nil)
	   (,evaluated-result-form% nil))
       (test-accumulated-sample (,label ,accumulate-result%
				 ,initial-set% ,requested-sample-size%)
	 (macrolet ((,accumulate (x)
		      `(progn
			 (unless ,',evaluated-initial-form%
			   (error "Didn't evaluate INITIAL-FORM before BODY."))
			 (when ,',evaluated-result-form%
			   (error "Evaluated RESULT-FORM before BODY."))
			 (setf ,',began-body% t)
			 (,',accumulate-result% ,x)))
		    (,initial-form ()
		      `(progn
			 (when ,',evaluated-initial-form%
			   (error "Evaluated INITIAL-FORM more than once."))
			 (setf ,',evaluated-initial-form% t)))
		    (,result-form ()
		      `(progn
			 (unless ,',evaluated-initial-form%
			   (error "Didn't evaluate INITIAL-FORM before RESULT-FORM."))
			 (when ,',evaluated-result-form%
			   (error "Evaluated RESULT-FORM more than once."))
			 (setf ,',evaluated-result-form% t)
			 (values 'expected-1 'expected-2))))
	   (let ((do-result (multiple-value-list ,@body)))
	     (unless ,evaluated-result-form%
	       (error "Didn't evaluate RESULT-FORM."))
	     (unless (equal do-result '(expected-1 expected-2))
	       (error "DO-SAMPLE-OF-* macro returned unexpected result:~
                       ~:[~{ ~S~}~; [no value]~*~]"
		      (endp do-result) do-result))))))))

(defmacro test-list-sample ((label input-set-form requested-sample-size-form) &body body)
  "Evaluates BODY, which takes a sample from a set, evaluating to the
sample (as a LIST).  The sample is tested against the input set and
the requested sample size.  If there is an error in the resulting
sample, an error is signalled.  LABEL is included in any error
messages.

INPUT-SET-FORM and REQUESTED-SAMPLE-SIZE-FORM have the same meaning as
they do in the TEST-ACCUMULATED-SAMPLE macro."
  `(let ((input-set ,input-set-form)
	 (requested-sample-size ,requested-sample-size-form)
	 (sample (progn ,@body)))
     (test-computed-sample ',label sample 'list input-set requested-sample-size)))

(defmacro test-vector-sample ((label input-set-form requested-sample-size-form) &body body)
  "Evaluates BODY, which takes a sample from a set, evaluating to the
sample (as a VECTOR).  The sample is tested against the input set and
the requested sample size.  If there is an error in the resulting
sample, an error is signalled.  LABEL is included in any error
messages.

INPUT-SET-FORM and REQUESTED-SAMPLE-SIZE-FORM have the same meaning as
they do in the TEST-ACCUMULATED-SAMPLE macro."
  `(let ((input-set ,input-set-form)
	 (requested-sample-size ,requested-sample-size-form)
	 (sample (progn ,@body)))
     (test-computed-sample ',label sample 'vector input-set requested-sample-size)))

(defun test-computed-sample (label sample sample-type input-set requested-sample-size)
  (unless (typep sample sample-type)
    (error "~S: Expected that sample would be of type ~S, but it's really of type ~S."
	   label sample-type (type-of sample)))
  (let ((expected-sample-size (min requested-sample-size (length input-set))))
    (unless (= expected-sample-size (length sample))
      (error "~S: Expected sample of size ~D, but got sample of size ~D."
	     label expected-sample-size (length sample))))
  (map nil (lambda (element)
	     (when (null (position element input-set :test #'equalp))
	       (error "~S: Got unexpected element in sample: ~S"
		      label element)))
       sample)
  (unless (= (length sample)
	     (length (remove-duplicates sample :test #'equalp)))
    (error "~S: Got duplicate elements in sample." label)))

;;; Generation of test sets.

(defparameter *test-output-lower-bound* -20)
(defparameter *test-output-upper-bound* 20)

(defun make-test-set-as-list ()
  "Returns a sample set (as a list) of lists, each list consisting of
two elements."
  (loop for i from *test-output-lower-bound* below *test-output-upper-bound*
	collecting (list i
			 (if (zerop i) 0 (/ i)))))

(defun make-test-set-as-vector ()
  "Returns the sample set from MAKE-TEST-SET-AS-LIST, but as a vector."
  (coerce (make-test-set-as-list) 'vector))

(defun make-test-set-as-range ()
  "Returns three values to use as a range set: a lower bound, an upper
bound, and a list of the values of the range."
  (values *test-output-lower-bound*
	  *test-output-upper-bound*
	  (loop for i from *test-output-lower-bound* below *test-output-upper-bound*
		collecting i)))

(defun list->hash-table (list)
  "Given LIST, a list of 2-lists of the form (KEY VALUE), returns a
hash table mapping each KEY to each corresponding VALUE."
  (let ((ht (make-hash-table :test #'equalp :size (length list))))
    (map nil (jpl-util:lambda* ((key value))
	       (setf (gethash key ht) value))
	 list)
    ht))

(defun list->iterator-mv (list)
  "Returns two values: the length of LIST and a function that
successively returns the lists of LIST as multiple values."
  (let ((remaining list))
    (values (length list)
	    (lambda ()
	      (values-list (pop remaining))))))

(defun list->unbounded-iterator (list)
  "Returns two values: a function that successively returns the values
of LIST, and a function that returns a generalized boolean indicating
whether the first function has exhausted LIST."
  (let ((remaining list))
    (values (lambda ()
	      (pop remaining))
	    (lambda ()
	      (endp remaining)))))

;;; The test functions.

(defun test-output ()
  (let ((range (- *test-output-upper-bound* *test-output-lower-bound*)))
    (test-output-methods 0)
    (test-output-methods (truncate range 2))
    (test-output-methods range)
    (test-output-methods (* 2 range))))

(defun test-output-methods (sample-size)
  (test-output-as-iteration sample-size)
  (test-output-as-mapping sample-size)
  (test-output-as-list sample-size)
  (test-output-as-vector sample-size)
  (test-output-as-hash-table sample-size))

(defun test-output-as-iteration (sample-size)
  (let ((set (make-test-set-as-list)))
    (test-do-sample (do-sample-of-list acc initial-form result-form set sample-size)
      (do-sample-of-list (x sample-size set
			  :initial-form (initial-form)
			  :result-form (result-form))
	(acc x))))
  (let ((set (make-test-set-as-vector)))
    (test-do-sample (do-sample-of-vector acc initial-form result-form set sample-size)
      (do-sample-of-vector (x sample-size set
			    :initial-form (initial-form)
			    :result-form (result-form))
	(acc x))))
  (let ((set (make-test-set-as-list)))
    (test-do-sample (do-sample-of-hash-table acc initial-form result-form set sample-size)
      (do-sample-of-hash-table (key value sample-size (list->hash-table set)
				:initial-form (initial-form)
				:result-form (result-form))
	(acc (list key value)))))
  (multiple-value-bind (lower upper set)
      (make-test-set-as-range)
    (test-do-sample (do-sample-of-range acc initial-form result-form set sample-size)
      (do-sample-of-range (x sample-size lower upper
			   :initial-form (initial-form)
			   :result-form (result-form))
	(acc x))))
  (let ((set (make-test-set-as-list)))
    (multiple-value-bind (set-size set-next)
	(list->iterator-mv set)
      (test-do-sample (do-sample-of-iterator-1 acc initial-form result-form set sample-size)
	(do-sample-of-iterator ((x y) sample-size set-size set-next
				:initial-form (initial-form)
				:result-form (result-form))
	  (acc (list x y))))))
  (let ((set (make-test-set-as-list)))
    (multiple-value-bind (set-size set-next)
	(list->iterator-mv set)
      (test-do-sample (do-sample-of-iterator-2 acc initial-form result-form set sample-size)
	(do-sample-of-iterator (vals sample-size set-size set-next
				:initial-form (initial-form)
				:result-form (result-form))
	  (acc vals)))))
  (let ((set (make-test-set-as-list)))
    (multiple-value-bind (set-next set-exhausted?)
	(list->unbounded-iterator set)
      (test-do-sample (test-sample-of-unbounded-iterator acc initial-form result-form set sample-size)
	(do-sample-of-unbounded-iterator (x sample-size set-next set-exhausted?
					  :initial-form (initial-form)
					  :result-form (result-form))
	  (acc x))))))

(defun test-output-as-mapping (sample-size)
  (let ((set (make-test-set-as-list)))
    (test-accumulated-sample (map-sample-of-list acc set sample-size)
      (map-sample-of-list (lambda (x) (acc x)) sample-size set)))
  (let ((set (make-test-set-as-vector)))
    (test-accumulated-sample (map-sample-of-vector acc set sample-size)
      (map-sample-of-vector (lambda (x) (acc x)) sample-size set)))
  (let ((set (make-test-set-as-list)))
    (test-accumulated-sample (map-sample-of-hash-table-keys acc
			      (map 'list #'first set) sample-size)
      (map-sample-of-hash-table-keys (lambda (k) (acc k))
				     sample-size (list->hash-table set))))
  (let ((set (make-test-set-as-list)))
    (test-accumulated-sample (map-sample-of-hash-table-values acc
			      (map 'list #'second set) sample-size)
      (map-sample-of-hash-table-values (lambda (v) (acc v))
				       sample-size (list->hash-table set))))
  (let ((set (make-test-set-as-list)))
    (test-accumulated-sample (map-sample-of-hash-table acc set sample-size)
      (map-sample-of-hash-table (lambda (k v) (acc (list k v)))
				sample-size (list->hash-table set))))
  (multiple-value-bind (lower upper set)
      (make-test-set-as-range)
    (test-accumulated-sample (map-sample-of-range acc set sample-size)
      (map-sample-of-range (lambda (x) (acc x)) sample-size lower upper)))
  (let ((set (make-test-set-as-list)))
    (multiple-value-bind (set-size set-next)
	(list->iterator-mv set)
      (test-accumulated-sample (map-sample-of-iterator acc set sample-size)
	(map-sample-of-iterator (lambda (x y) (acc (list x y)))
				sample-size set-size set-next))))
  (let ((set (make-test-set-as-list)))
    (multiple-value-bind (set-next set-exhausted?)
	(list->unbounded-iterator set)
      (test-accumulated-sample (map-sample-of-unbounded-iterator acc set sample-size)
	(map-sample-of-unbounded-iterator (lambda (x) (acc x)) sample-size
					  set-next set-exhausted?)))))

(defun test-output-as-list (sample-size)
  (let ((set (make-test-set-as-list)))
    (test-list-sample (list-sample-of-list set sample-size)
      (list-sample-of-list sample-size set)))
  (let ((set (make-test-set-as-vector)))
    (test-list-sample (list-sample-of-vector set sample-size)
      (list-sample-of-vector sample-size set)))
  (let ((set (make-test-set-as-list)))
    (test-list-sample (list-sample-of-hash-table-keys
		       (map 'list #'first set) sample-size)
      (list-sample-of-hash-table-keys sample-size (list->hash-table set))))
  (let ((set (make-test-set-as-list)))
    (test-list-sample (list-sample-of-hash-table-values
		       (map 'list #'second set) sample-size)
      (list-sample-of-hash-table-values sample-size (list->hash-table set))))
  (let ((set (make-test-set-as-list)))
    (test-list-sample (list-sample-of-hash-table-pairs
		       (map 'list (jpl-util:lambda* ((key value))
				    (cons key value))
			    set)
		       sample-size)
      (list-sample-of-hash-table-pairs sample-size (list->hash-table set))))
  (multiple-value-bind (lower upper set)
      (make-test-set-as-range)
    (test-list-sample (list-sample-of-range set sample-size)
      (list-sample-of-range sample-size lower upper)))
  (let ((set (make-test-set-as-list)))
    (multiple-value-bind (set-size set-next)
	(list->iterator-mv set)
      (test-list-sample (list-sample-of-iterator (map 'list #'first set) sample-size)
	(list-sample-of-iterator sample-size set-size set-next))))
  (let ((set (make-test-set-as-list)))
    (multiple-value-bind (set-next set-exhausted?)
	(list->unbounded-iterator set)
      (test-list-sample (list-sample-of-unbounded-iterator set sample-size)
	(list-sample-of-unbounded-iterator sample-size set-next set-exhausted?)))))

(defun test-output-as-vector (sample-size)
  (let ((set (make-test-set-as-list)))
    (test-vector-sample (vector-sample-of-list set sample-size)
      (vector-sample-of-list sample-size set)))
  (let ((set (make-test-set-as-vector)))
    (test-vector-sample (vector-sample-of-vector set sample-size)
      (vector-sample-of-vector sample-size set)))
  (let ((set (make-test-set-as-list)))
    (test-vector-sample (vector-sample-of-hash-table-keys
			 (map 'list #'first set) sample-size)
      (vector-sample-of-hash-table-keys sample-size (list->hash-table set))))
  (let ((set (make-test-set-as-list)))
    (test-vector-sample (vector-sample-of-hash-table-values
			 (map 'list #'second set) sample-size)
      (vector-sample-of-hash-table-values sample-size (list->hash-table set))))
  (let ((set (make-test-set-as-list)))
    (test-vector-sample (vector-sample-of-hash-table-pairs
			 (map 'list (jpl-util:lambda* ((key value))
				      (cons key value))
			      set)
			 sample-size)
      (vector-sample-of-hash-table-pairs sample-size (list->hash-table set))))
  (multiple-value-bind (lower upper set)
      (make-test-set-as-range)
    (test-vector-sample (vector-sample-of-range set sample-size)
      (vector-sample-of-range sample-size lower upper)))
  (let ((set (make-test-set-as-list)))
    (multiple-value-bind (set-size set-next)
	(list->iterator-mv set)
      (test-vector-sample (vector-sample-of-iterator (map 'list #'first set) sample-size)
	(vector-sample-of-iterator sample-size set-size set-next))))
  (let ((set (make-test-set-as-list)))
    (multiple-value-bind (set-next set-exhausted?)
	(list->unbounded-iterator set)
      (test-vector-sample (vector-sample-of-unbounded-iterator set sample-size)
	(vector-sample-of-unbounded-iterator sample-size set-next set-exhausted?)))))

(defun test-output-as-hash-table (sample-size)
  (let ((set (make-test-set-as-list)))
    (test-accumulated-sample (hash-table-sample-of-hash-table acc set sample-size)
      (maphash (lambda (key value)
		 (acc (list key value)))
	       (hash-table-sample-of-hash-table
		sample-size (list->hash-table set))))))
