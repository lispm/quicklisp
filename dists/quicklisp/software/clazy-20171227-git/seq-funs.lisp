;;;; -*- Mode: Lisp -*-

;;;; seq-funs.lisp --
;;;;
;;;; The function in this file are taken from:
;;;; http://haskell.org/ghc/docs/latest/html/libraries/base-4.0.0.0/Data-List.html
;;;;
;;;; See the COPYING file for copyright and licensing information.
;;;;
;;;; Note that the list is incomplete.  Some functions are not
;;;; implemented and other may have a slightly differen semantics.  Plus,
;;;; many are CL-ified by adding the standard keywords a CLer expects.
;;;; Also, many functions are not as "lazy" as they should be.  The
;;;; actual lazy implementation could be dropped in place without problems.
;;;;
;;;; Note 20090220:
;;;; A note on the implementation style.  I decided to use [E]TYPECASE
;;;; in lieu of generic functions.  In a way this was done to keep
;;;; this initial design simple.  Defining generic functions is better
;;;; done in a general setting where *all* sequence functions are
;;;; "genericified".  It can be done later...

(in-package "CL.EXT.SEQUENCES.LAZY")


(deftype seq-index ()
  "The type of the indices inside a sequence.

Although not necessarily right for lists, the upper bound is set to
ARRAY-DIMENSION-LIMIT."
  `(integer 0 ,array-dimension-limit)) ; Is this right? Lists?


(deftype list-index ()
  "The type of the indices inside a list.

Although not necessarily right for lists, the upper bound is set to
MOST-POSITIVE-FIXNUM."
  `(integer 0 ,most-positive-fixnum))


#+full-lazy
(defun take (n seq)
  "Takes the first N elements of the sequence SEQ.

The result is a fresh sequence of the first N elements.  SEQ can be a
SEQUENCE or a LAZY-STREAM.  The type of the result is the same as the
type of SEQ if this is a SEQUENCE, otherwise it is a LIST."
  (declare (type seq-index n)
           (type (or sequence lazy-stream) seq))
  (etypecase seq
    (sequence
     (subseq seq 0 (max 0 (min n (length seq)))))
    (lazy-stream
     (if (minusp n)
         ;; (head seq)
         nil
	 (lazy:call 'cons
		    (head seq)
		    (take (1- n) (tail seq)))))
    ))


(defun take (n seq)
  "Takes the first N elements of the sequence SEQ.

The result is a fresh sequence of the first N elements.  SEQ can be a
SEQUENCE or a LAZY-STREAM.  The type of the result is the same as the
type of SEQ if this is a SEQUENCE, otherwise it is a LIST."
  (declare (type seq-index n)
           (type (or sequence lazy-stream) seq))
  (etypecase seq
    (sequence
     (subseq seq 0 (max 0 (min n (length seq)))))
    (lazy-stream
     (if (minusp n)
         ;; (head seq)
         nil
         (loop repeat n
               for st = seq then (tail st)
               collect (head st))))))


(defun drop (n seq)
  "Drops the first N elements of the sequence SEQ.

The result is a fresh sequence of the last (- (length SEQ) N)
elements.  SEQ can be a SEQUENCE or a LAZY-STREAM.  The type of the
result is the same as the type of SEQ if this is a SEQUENCE, otherwise
it is a LAZY-STREAM or NIL (if dropping more items than the - finite -
length of the LAZY-STREAM)."
  (declare (type seq-index n)
           (type (or sequence lazy-stream) seq))
  (etypecase seq
    (sequence
     (if (plusp n)
         (subseq seq (min n (length seq)))
         (copy-seq seq)))
    (lazy-stream
     (loop for st = seq then (tail st)
           repeat n
           finally (return st)))))


(defun split-at (n seq)
  "Returns two subsequences of SEQ split at the N-th element."
  (values (take n seq) (drop n seq)))


#+full-lazy
(defun take-while (p seq)
  "Takes the first elements of the sequence SEQ that satisfy the predicate P.

SEQ can be a SEQUENCE or a LAZY-STREAM.  The type of the result is the
same as the type of SEQ if this is a SEQUENCE, otherwise it is a
LIST."
  (declare (ftype (function (t) t) p)
           (type (or sequence lazy-stream)))
  (etypecase seq
    (sequence
     (let ((n (position-if (complement p) seq)))
       (declare (type (or null fixnum) n))
       (if n
           (take n seq)
           (copy-seq seq))))
    (lazy-stream
     (let ((x (head seq)))
       (when (funcall p x)
	 (lazy:call 'cons
		    x
		    (take-while p (tail seq))))))
    ))


(defun take-while (p seq)
  "Takes the first elements of the sequence SEQ that satisfy the predicate P.

SEQ can be a SEQUENCE or a LAZY-STREAM.  The type of the result is the
same as the type of SEQ if this is a SEQUENCE, otherwise it is a
LIST."
  (declare (type (function (t) t) p)
           (type (or sequence lazy-stream)))
  (etypecase seq
    (sequence
     (let ((n (position-if (complement p) seq)))
       (declare (type (or null fixnum) n))
       (if n
           (take n seq)
           (copy-seq seq))))
    (lazy-stream
     (loop for s = seq then (tail s)
           for e = (head s)
           while (funcall p e)
           collect e))))


#+full-lazy
(defun drop-while (p seq)
  "Drops the first elements of the sequence SEQ that satisfy the predicate P."
  (etypecase seq
    (null nil)
    (sequence
     (let ((n (position-if (complement p) seq)))
       (if n
           (drop n seq)
           (subseq seq 0 0))))
    (lazy-stream
     (let ((x (head seq)))
       (if (not (funcall p x))
	   seq
	   (drop-while p (tail seq)))))
    ))


(defun drop-while (p seq)
  "Drops the first elements of the sequence SEQ that satisfy the predicate P."
  (declare (type (function (t) boolean) p)
           (type (or sequence lazy-stream) seq))
  (etypecase seq
    (null nil)
    (sequence
     (let ((n (position-if (complement p) seq)))
       (if n
           (drop n seq)
           (subseq seq 0 0))))
    (lazy-stream
     (loop for s = seq then (tail s)
           for e = (head s)
           until (not (funcall p e))
           finally (return s)))))


(defun span (p seq)
  (declare (type (function (t) boolean) p)
           (type (or sequence lazy-stream) seq))
  (etypecase seq
    (sequence
     (let ((n (position-if (complement p) seq)))
       (if n
           (values (take n seq) (drop n seq))
           (values (copy-seq seq) (subseq seq 0 0)))))
    (lazy-stream
     (loop for s = seq then (tail s)
           for e = (head s)
           collect e into result-list
           until (not (funcall p e))
           finally (return (values result-list s))))))


(defun separate (p seq)
  (declare (type (function (t) boolean) p)
           (type (or sequence lazy-stream) seq))
  (span (complement p) seq))


;;;; strip-prefix

;;;; group

;;;; tails

(defun tails (seq)
  (etypecase seq
    (list (loop for i on seq collect (copy-list i)))
    (vector (loop for i from 0 below (length seq)
	          collect (subseq seq i)))
    (lazy-stream
     (lazy::lazily (cons (copy-lazy-stream seq) (tails (tail seq)))))
    ))

;;;; inits

#|
(defun inits (seq)
  (loop for i from 1 upto (length seq)
        collect (subseq seq 0 i)))
|#

#|
(defun prefixp (prefix seq
                       &key (test #'eql)
                       &aux
                       (lp (length prefix))
                       (ls (length seq)))
  (declare (type seq-index lp ls)
           (type sequence prefix seq))
  (cond ((= lp ls) (every test prefix seq))
        ((< (length prefix) (length seq))
         (= lp (mismatch prefix seq :test test)))))


(defun suffixp (suffix seq
                       &key (test #'eql)
                       &aux
                       (lp (length suffix))
                       (ls (length seq)))
  (declare (type seq-index lp ls)
           (type sequence suffix seq))
  (cond ((= lp ls) (every test suffix seq))
        ((< (length suffix) (length seq))
         (zerop (mismatch suffix seq
                          :test test
                          :from-end t)))))


(defun infixp (infix seq
                    &key (test #'eql))
  (declare (type sequence infix seq))
  (let ((il (length infix))
        (sl (length seq))
        (ip (search infix seq :test test))
        )
    (declare (type seq-index il sl)
             (type (or null seq-index) ip))
    (when (< il sl)
      (and ip
           (plusp ip)
           (plusp (- (length seq) (length infix) ip))))))
|#


;;; Not in Haskell Data.List

(defun take-nth (n seq)
  (declare (type fixnum n)
           (type (or sequence) seq))
  (etypecase seq
    (list (loop for es on seq by (lambda (l) (nthcdr n l))
                collect (first es)))
    (vector (loop for i = 0 then (+ i n)
                  while (< i (length seq))
                  collect (aref seq i) into res
                  finally (return
                           (loop with result = (make-array (list-length res))
                                 for e in res
                                 for i from 0
                                 do (setf (aref result i) e)
                                 finally (return result)))))
    ))


(defun zip (cons-fun seq1 seq2 &rest seqs)
  (if (and (not (lazy-stream-p seq1))
           (not (lazy-stream-p seq2))
           (notany 'lazy-stream-p seqs))
      (apply #'mapcar cons-fun seq1 seq2 seqs)
      (lazily (cons (apply cons-fun
                           (head seq1)
                           (head seq2)
                           (mapcar #'head seqs))
                    (apply #'zip cons-fun
                           (tail seq1)
                           (tail seq2)
                           (mapcar #'tail seqs))))))


(defun distinct (seq &rest keys
                     &key
                     (test #'eql)
                     (key #'identity)
                     ((distincts-table dst)
                      (make-hash-table :test test))
                     &allow-other-keys)
  "Returns a generalized sequence of distinct elements.

If SEQ is a SEQUENCE then REMOVE-DUPLICATES is called.  If it is a
LAZY-STREAM then a new LAZY-STREAM is returned, with repeated elements
removed.  Key arguments are only TEST and KEY.

Arguments and Values:

SEQ : a (OR NULL SEQUENCE LAZY-STREAM)
TEST : a (FUNCTION (T T) <generalized boolean>)
KEY : a (FUNCTION (T) T)
DISTINCTS-TABLE : a HASH-TABLE
KEYS : a LIST (actually a p-list of the keywords arguments)
result : a (OR NULL SEQUENCE LAZY-STREAM)
"
  (declare (type (or null sequence lazy-stream) seq)
           (type (function (T T) boolean) test)
           (type (function (T) T) key)
           (type hash-table distincts-table dst)
           (type list keys)
           )
  (etypecase seq
    (null ())
    (sequence (apply #'cl:remove-duplicates seq keys))
    (lazy-stream
     (let ((e (head seq)))
       (cond ((gethash e dst)
              (distinct (tail seq)
                        :test test
                        :key key
                        'distincts-table dst))
             (t (setf (gethash e dst) t)
                (cons-stream e
                             (distinct (tail seq)
                                       :test test
                                       :key key
                                       'distincts-table dst)))
             )))
    ))


;;;; end of file -- seq-funs.lisp --
