(in-package :mini-cas)

#|
  Represent functions in a SICP/MMA/defstruct(:type list)-style structure.

  The car is the function, the cdr holds the parameters.

  Currently, only '- does not satisfy ordinary CL semantics.
|#
(defun cas+ (&rest args)
  (cons '+ args))
(defun cas- (arg)
  (list '- arg))
(defun cas* (&rest args)
  (cons '* args))
(defun cas/ (num den)
  (list '/ num den))

(declaim (inline cas-list-pred))
(defun cas-list-pred (car form &optional length #| including the car |#)
  (and (listp form)
       (eql car (car form))
       (not (when length
              (assert (= (length form) length)
                      ()
                      "malformed expression: ~A~%Operator ~A requires ~A arguments."
                      form car (1- length))))))

(defun cas+? (form)
  (cas-list-pred '+ form))
(defun cas-? (form)
  (cas-list-pred '- form 2))
(defun cas*? (form)
  (cas-list-pred '* form))
(defun cas/? (form)
  (cas-list-pred '/ form 3))

(defun /num (quotient)
  (cadr quotient))
(defun /den (quotient)
  (caddr quotient))

(defun cas-cos (x)
  (list 'cos x))
(defun cas-sin (x)
  (list 'sin x))

(defun cas-cos? (form)
  (cas-list-pred 'cos form 2))
(defun cas-sin? (form)
  (cas-list-pred 'sin form 2))
