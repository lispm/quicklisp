;;;; Licensed under GPL V3.

(in-package #:eclecticse.omer)


(defparameter *pesach* "2018-04-02")


(defun omer ()
  (omer-for (today)))


(defun omer-for (date)
  "Print out the omer count output for the give DATE, which is
provided in ISO 8601 YYYY-MM-DD format."
  (let* ((day-count (day-count-to date))
         (remaining-days (- 50 day-count)))
    (flet ((normal-count ()
             (format t "~&~:@(Omer count for ~A~) ~A.~2%" (day-of-week date) date)
             (when (= 25 day-count)
               (format t "~&We are half-way to Shavuot!~%"))
             (format t "~&It is the ~:R day of fifty to Shavuot.~%There ~:[is~;are~] ~R more day~:P to go!~%"
                     day-count
                     (> remaining-days 1)
                     remaining-days)
             (format t "~&It is the ~:R day of the ~:R week since Pesach.~%"
                     (1+ (mod (1- (date-diff *pesach* date)) 7))
                     (1+ (truncate (1- (date-diff *pesach* date)) 7)))
             (let ((num-sabbaths (sabbath-count date)))
               (if (sabbath-p date)
                   (format t "~&It is the ~:R of the Sabbaths!~%" num-sabbaths)
                   (format t "~&We have counted ~R of the seven Sabbaths!~%" num-sabbaths)))))
      (cond ((< 0 day-count 50)
             (normal-count))
            ((= 0 day-count)
             (format t "~&~A is Pesach. Fifty more days to Shavuot.~%" date))
            ((= 50 day-count)
             (format t "~&Today (~A) is ...~2%" date)
             (shavuot))
            ((> day-count 50)
             (format t "~&Sorry, but you have missed Shavuot! :-(~%"))
            (t
             (format t "~&You have confused me.~%"))))
    :omer-count))


(defun sabbath-p (date)
  (= 7 (num-day-of-week date)))


(defun today ()
  (iso-8601-date-now))


(defun day-count-to (date)
  (date-diff *pesach* date))


(defun sabbath-count (date)
  (let* ((start-date (increment-date *pesach*))
         (end-date date)
         (this-date start-date))
    (count-if (lambda (dow)
                (= 7 dow))
              (loop while (<= 0 (date-diff this-date end-date))
                    collect (num-day-of-week this-date)
                    do (setf this-date (increment-date this-date))))))


(defun increment-date (date)
  "Increment DATE by one day. DATE must be in YYYY-MM-DD format. If there is a time portion, it will be ignored."
  (let ((local-time:*default-timezone* local-time:+utc-zone+)
        (date-only (date-portion date)))
    (local-time:format-timestring nil
                                  (local-time:timestamp+ (local-time:parse-timestring date-only) 1 :day)
                                  :format '((:year 4) #\- (:month 2) #\- (:day 2)))))



;;; START dkt-date functions ---------


(defparameter *week-days* '("Sunday" "Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Sabbath"))
;;; Saturday changed to Sabbath for this script.


(defun two-digit-str (int)
  (format nil "~2,1,0,'0@A" int))


(defun four-digit-str (int)
  (format nil "~4,1,0,'0@A" int))


(defun human-date (date-string)
  "Strip the T from ISO8601 dates, and remove decimal portion of seconds, if any."
  (let* ((date-no-t (substitute #\Space #\T date-string))
         (dec-point-pos (search "." date-no-t :from-end t)))
    (if (null dec-point-pos)
        date-no-t
        (subseq date-no-t 0 dec-point-pos))))


(defun iso-8601-date-now (&key (hyphen t))
  "Return a string representing the current date in ISO 8601 format."
  (let ((two-digits "~2,1,0,'0@A"))
    (multiple-value-bind (s m h dt mth yr day)
        (get-decoded-time)
      (declare (ignorable day h m s))
      (format nil (concatenate 'string
                               "~A" (if hyphen "-" "") two-digits (if hyphen "-" "") two-digits)
              yr mth dt))))


(defun iso-8601-time-now (&key (colon t))
  "Return a string representing the current time in ISO 8601 format."
  (let ((two-digits "~2,1,0,'0@A"))
    (multiple-value-bind (s m h dt mth yr day)
        (get-decoded-time)
      (declare (ignorable day yr mth dt))
      (format nil (concatenate 'string
                               two-digits (if colon ":" "") two-digits (if colon ":" "") two-digits)
              h m s))))


(defun iso-8601-date-time-now (&key (t-sep nil) (hyphen t) (colon t))
  "Return a string representing the current date and time in ISO 8601 format."
  (concatenate 'string (iso-8601-date-now :hyphen hyphen) (if t-sep "T" " ") (iso-8601-time-now :colon colon)))


(defun make-iso-8601-date (year month day &key (hyphen t))
  "Given numerical values for the year, month and day, return a
string representing the date in ISO 8601 format."
  (let ((two-digits "~2,1,0,'0@A")
        (four-digits "~4,1,0,'0@A"))
    (format nil (concatenate 'string
                             four-digits (if hyphen "-" "") two-digits (if hyphen "-" "") two-digits)
            year month day)))


(defun date-portion (iso-8601-date-time)
  (subseq iso-8601-date-time 0 10))


#+TODO(defun last-month ()
  (let* ((today-str (iso-8601-date-now :hyphen nil))
         (year (parse-integer (subseq today-str 0 4)))
         (month (parse-integer (subseq today-str 4 6)))
         (day (parse-integer (subseq today-str 6))))
    (multiple-value-bind (year month day) (date-calc:ADD-DELTA-YM year month day 0 -1)
      (declare (ignore day))
      (concatenate 'string (four-digit-str year) "-" (two-digit-str month)))))


(defun extract-year (iso-8601-date)
  (parse-integer (subseq iso-8601-date 0 4)))


(defun extract-month (iso-8601-date)
  (parse-integer (subseq iso-8601-date
                         (1+ (or (position #\- iso-8601-date :end 7) 3))
                         (or (position #\- iso-8601-date :start 5 :end 8) 6))))


(defun extract-day (iso-8601-date)
  (let ((second-hyphen-pos (position #\- iso-8601-date :start 5 :end 8)))
    (parse-integer (subseq iso-8601-date
                           (if (null second-hyphen-pos) 6 (1+ second-hyphen-pos))
                           (if (null second-hyphen-pos) 8 (+ 3 second-hyphen-pos))))))


(defun date-diff (date-1 date-2)
  (let ((local-time:*default-timezone* local-time:+utc-zone+))
    (truncate (local-time:timestamp-difference (local-time:parse-timestring date-2)
                                               (local-time:parse-timestring date-1))
              local-time:+seconds-per-day+)))


(defun num-day-of-week (date)
  (let ((day-num (mod (+ 8 (local-time:timestamp-day-of-week (local-time:parse-timestring date))) 7)))
    (if (= day-num 0)
        7
        day-num)))


(defun day-of-week (date)
  (nth (1- (num-day-of-week date)) *week-days*))


;;; FINISH dkt-date ---------


(defun es (line)
  (let ((lines #("SSSSSSS  "
                 "S        "
                 "SSSSSSS  "
                 "SSSSSSS  "
                 "      S  "
                 "SSSSSSS  ")))
    (format t "~A" (aref lines (1- line)))))


(defun h (line)
  (let ((lines #("H    H   "
                 "H    H   "
                 "HHHHHH   "
                 "HHHHHH   "
                 "H    H   "
                 "H    H   ")))
    (format t "~A" (aref lines (1- line)))))


(defun a (line)
  (let ((lines #("   A     "
                 "  A A    "
                 " A   A   "
                 "AAAAAAA  "
                 "A     A  "
                 "A     A  ")))
    (format t "~A" (aref lines (1- line)))))


(defun vee (line)
  (let ((lines #("V     V  "
                 " V   V   "
                 " V   V   "
                 " V   V   "
                 "  V V    "
                 "   V     ")))
    (format t "~A" (aref lines (1- line)))))


(defun u (line)
  (let ((lines #("U     U  "
                 "U     U  "
                 "U     U  "
                 "U     U  "
                 "U     U  "
                 " UUUUU   ")))
    (format t "~A" (aref lines (1- line)))))


(defun o (line)
  (let ((lines #(" OOOOO   "
                 "O     O  "
                 "O     O  "
                 "O     O  "
                 "O     O  "
                 " OOOOO   ")))
    (format t "~A" (aref lines (1- line)))))


(defun tee (line)
  (let ((lines #("TTTTTTT  "
                 "   T     "
                 "   T     "
                 "   T     "
                 "   T     "
                 "   T     ")))
    (format t "~A" (aref lines (1- line)))))


(defun shavuot ()
  (loop for line from 1 to 6 do
    (es line) (h line) (a line) (vee line) (u line) (o line) (tee line) (terpri))
  (terpri))
