(module "equ"
  (meta (:comment . "Equality and Substitution"))

  (symkind "WFF")
  (symkind "SET")
  (symkind "VAR" :super set)

  (export [!!equ-ax])
  (providing (("pred-ax" . !/equ-ax)
              "VAR" "WFF")
    (export [!!pred]))

; Theorem with distinct variable conditions
; For all hypotheses [→ φ ∀ x φ] in thr,
; add distinct variable condition [x φ]
(metath "distv" (ref)
  (let ((pfl (parse-subproof ref)))
    (subproof (:assert (subproof-assert pfl))
      (dolist (h (subproof-hypo pfl))
        (if-match [Bvp ?x ?ph] h
          (line [ax17-bv ,?x ,?ph])
          (hypo h)))
      (line pfl))))


  (local "distinctor")
  (local "subst" (meta (:comment . "Proper substitution.")))

  ;; Theorems

  (alias "≠" (set ![x y])
     [¬ = x y])

;; Variants of axioms
  
(th "ax9alt" (var "x" set "y" wff "φ")
    (meta (:comment . "A variant of !ax9.")
          (:description . "Axiom scheme C10' in [Megill] p. 448, ax9 in set.mm."))
    (dist (!x !y))
    (ass [→ ∀ x → = x y ∀ x φ φ])
    (proof
      [ax9 x y]
      [infer df-ex [x] [= x y]]
      [!ex!imt [x] [= x y] [∀ x φ]]
      [mpi [∀ x → = x y ∀ x φ] [∃ x = x y] [∃ x ∀ x φ]]
      [ax6e x φ]
      [syl [∀ x → = x y ∀ x φ] [∃ x ∀ x φ] φ]))

(th "ax9ex" (var "x" set "y")
    (meta (:comment . "At least one individual exists.")
          (:description . "This is not a theorem of free logic, which is sound in empty domains; for such logics we would add
this theorem as an axiom of set theory (Axiom 0 of [Kunen] p. 10). a9e in set.mm."))
    (dist (!x !y))
    (ass [∃ x = x y])
    (proof
      [ax9 x y]
      [infer df-ex [x] [= x y]]))

(th "ax17eq" (var ![x y z])
    (meta (:comment . "A special case of ax17."))
    (dist (!x !z) (!y !z))
    (ass [Bvp z = x y])
    (proof
      [ax12 x y z]
      [ax16 z x [= x y]]
      [ax16 z y [= x y]]
      [caseii [∀ z = z x] [∀ z = z y] [→ = x y ∀ z = x y]]
      [Bvp z [= x y]]))
(th-pattern !ax17eq)

(th "ax16-bv" (var "x" set "y" wff "φ")
    (meta (:comment . "ax16 restated using the 'Bvp' predicate."))
    (dist (!x !y))
    (ass [→ ∀ x = x y Bvp x φ])
    (proof
      [ax16 x y φ]
      [!al!imti x [∀ x = x y] [→ φ ∀ x φ]]
      [Bvp x [∀ x = x y]]
      [→ ∀ x = x y ∀ x ∀ x = x y]
      [syl [∀ x = x y] [∀ x ∀ x = x y] [∀ x → φ ∀ x φ]]
      [df-Bvp x φ]
      [b2i syl [∀ x = x y] [∀ x → φ ∀ x φ] [Bvp x φ]]))

(th "ax17-bv" (var "x" wff "φ")
    (meta (:comment . "ax17 restated using the 'Bvp' predicate."))
    (dist (!x !φ))
    (ass [Bvp x φ])
    (proof
      [ax17 x φ]
      [Bvp x φ]))

(th "ax12-bv" (var ![x y z])
    (meta (:comment . "ax12 restated using the 'Bvp' predicate."))
    (ass [→ ¬ ∀ z = z x → ¬ ∀ z = z y Bvp z = x y])
    (proof
      [ax12 x y z]
      [!al!2imti z [¬ ∀ z = z x] [¬ ∀ z = z y] [→ = x y ∀ z = x y]]
      [imp [∀ z ¬ ∀ z = z x] [∀ z ¬ ∀ z = z y] [∀ z → = x y ∀ z = x y]]
      [ded df-Bvp z [= x y] [∧ ∀ z ¬ ∀ z = z x ∀ z ¬ ∀ z = z y]]
      [exp [∀ z ¬ ∀ z = z x] [∀ z ¬ ∀ z = z y] [Bvp z = x y]]
      [Bvp z ∀ z = z x]
      [Bvp z ¬ ∀ z = z x]
      [→ ¬ ∀ z = z x ∀ z ¬ ∀ z = z x]
      [syl [¬ ∀ z = z x] [∀ z ¬ ∀ z = z x] [→ ∀ z ¬ ∀ z = z y Bvp z = x y]]
      [Bvp z ∀ z = z y]
      [Bvp z ¬ ∀ z = z y]
      [→ ¬ ∀ z = z y ∀ z ¬ ∀ z = z y]
      [!im!rp32 [¬ ∀ z = z x] [∀ z ¬ ∀ z = z y] [Bvp z = x y] [¬ ∀ z = z y]]))

;; Basic properties of equality

(th "eqid" (set "x")
    (meta (:comment . "Identity law for equality (reflexivity).")
          (:description . "Lemma 6 of [Tarski] p. 68, Theorem 2.5 of [Bernays]. This is often an axiom of equality in
textbook systems, but we don't need it as an axiom since it can be proved from our other axioms (althogh the proof is not as
obvious as you might think). This proof uses a dummy variable y."))
    (loc var "y")
    (ass [= x x])
    (proof
      [ax9ex y x]
      [ax17-bv y [= x x]]
      [ax8 y x x]
      [infer !im!abs [= y x] [= x x]]
      [→ ∃ y = y x = x x]
      [ax-mp [∃ y = y x] [= x x]]))
(th-pattern !eqid)

(th "eqcomi" (set ![x y])
    (meta (:comment . "Commutative law for equality.")
          (:description . "Lemma 7 of [Tarski] p. 69, Theorem 2.4 of [Bernays]."))
    (ass [→ = x y = y x])
    (proof
      [= x x]
      [ax8 x y x]
      [mpi [= x y] [= x x] [= y x]]))

(th "eqtr" (set ![x y z])
    (meta (:comment . "A transitive law for equality.")
          (:description . "Theorem 2.3 of [Bernays]."))
    (ass [→ = x y → = y z = x z])
    (proof
      [ax8 y x z]
      [+syl eqcomi x y [→ = y z = x z]]))

;; These satisfy the axioms in equiv-ax
(local "equiv-ax-="
  (alias "=" (set ![x y])
     [= x y])
  (alias "≠" (set ![x y])
     [¬ = x y])
  (alias "ax-tr" (set ![x y z])
     [eqtr x y z])
  (alias "ax-sym" (set ![x y])
     [eqcomi x y])
  (alias "df-neq" (set ![x y])
     [!bi!refl [¬ = x y]]))

;; Equality theorems for =
(local "eq"
  (providing (("equiv0-ax" . !equiv-ax-=)
              ("prop" . !equ)
              ("OBJ" . !SET)
              "WFF")
    (export [!!equiv0])))

;;; Properties of the "distinctor" [¬ ∀ x = x y]

(in-context !distinctor
  (th "com" (var ![x y])
    (meta (:comment . "Commutation law for identical variable specifiers.")
          (:description . "aleqcom in set.mm. The antecedent and consequent are true when x and y are substituted with the same variable."))
    (ass [→ ∀ x = x y ∀ y = y x])
    (proof
      [ax10 x y [= x y]]
      [infer !im!abs [∀ x = x y] [∀ y = x y]]
      [→ = x y = y x]
      [→ [∀ y = x y] [∀ y = y x]]
      [syl [∀ x = x y] [∀ y = x y] [∀ y = y x]]))

  (th "ncoms" (wff "φ" var ![x y])
    (meta (:comment . "A commutation rule for distinct variable specifiers.")
          (:description . "naleqcoms in set.mm."))
    (hypo [→ ¬ ∀ x = x y φ])
    (ass  [→ ¬ ∀ y = y x φ])
    (proof
      [com x y]
      [infer con< [∀ x = x y] φ]
      [syl [¬ φ] [∀ x = x y] [∀ y = y x]]
      [infer con< φ [∀ y = y x]]))

  (th "bv" (var ![z x y])
    (meta (:comment . "All variables are effectively bound in an identical variable specifier.")
          (:description . "hbae in set.mm."))
    (ass [Bvp z ∀ x = x y])
    (proof
      [ax12 x y z]
      [ax4 [x] [= x y]]
      [!im!rp43 [¬ ∀ z = z x] [¬ ∀ z = z y] [= x y] [∀ z = x y] [∀ x = x y]]
      [ax10 x z [= x y]]
      [+syl com z x [→ ∀ x = x y ∀ z = x y]]
      [ax10 y z [= x y]]
      [ax10 x y [= x y]]
      [infer !im!abs [∀ x = x y] [∀ y = x y]]
      [!im!rp32 [∀ y = y z] [∀ y = x y] [∀ z = x y] [∀ x = x y]]
      [+syl com z y [→ ∀ x = x y ∀ z = x y]]
      [caseii [∀ z = z x] [∀ z = z y] [→ ∀ x = x y ∀ z = x y]]
      [infer ax5 x [= x y] [∀ z = x y]]
      [ax7 x z [= x y]]
      [syl [∀ x = x y] [∀ x ∀ z = x y] [∀ z ∀ x = x y]]
      [Bvp z ∀ x = x y]))

  (th "nbv" (var ![z x y])
    (meta (:comment . "All variables are effectively bound in a distinct variable specifier.")
          (:description . "hbnae in set.mm. Lemma L19 in [Megill] p. 446."))
    (ass [Bvp z ¬ ∀ x = x y])
    (proof
      [bv z x y]
      [Bvp z ¬ ∀ x = x y])) )

;;; Variations of ax10

(th "ax10b" (var ![x y] wff "φ")
    (meta (:comment . "Biconditional version of ax10."))
    (ass [→ ∀ x = x y ↔ ∀ x φ ∀ y φ])
    (proof
      [ax10 x y φ]
      [ax10 y x φ]
      [+syl !distinctor!com x y [→ ∀ y φ ∀ x φ]]
      [>bid [∀ x = x y] [∀ x φ] [∀ y φ]]))
      
(th "ax10ex" (var ![x y] wff "φ")
    (meta (:comment . "Existential version of ax10."))
    (ass [→ ∀ x = x y ↔ ∃ x φ ∃ y φ])
    (proof
      [ax10b x y [¬ φ]]
      [↔ ∀ x ¬ φ ¬ ∃ x φ]
      [↔ ∀ y ¬ φ ¬ ∃ y φ]
      [!bi!>trg [∀ x = x y] [∀ x ¬ φ] [∀ y ¬ φ]
                           [¬ ∃ x φ] [¬ ∃ y φ]]
      [ded neqt [¬ ∃ x φ] [¬ ∃ y φ] [∀ x = x y]]
      [↔ ∃ x φ ¬ ¬ ∃ x φ]
      [↔ ∃ y φ ¬ ¬ ∃ y φ]
      [!bi!<trg [∀ x = x y] [¬ ¬ ∃ x φ] [¬ ¬ ∃ y φ] [∃ x φ] [∃ y φ]]))

;;; The set.mm version of ax9

(th "ax9-sv" (var ![x y])
    (meta (:comment . "A version of ax9 without distinct variable conditions.")
          (:description . "ax-9 in set.mm. In this form it was used in an axiom system of Tarski (see Axiom B7' in footnote 1
of [KalishMontague] p. 81.) It is equivalent to axiom scheme C10' in [Megill] p. 448."))
    (ass [∃ x = x y])
    (loc var "z")

    ;; Lemmas: Change-of-variable formulas in the case x and y are distinct
    (th "cbv1-dv" (var ![x y] wff ![φ ψ χ])
        (dist (!x !y))
        (hypo [→ φ → ψ ∀ y ψ] [→ φ → χ ∀ x χ] [→ φ → = x y → ψ χ])
        (ass [→ ∀ x ∀ y φ → ∀ x ψ ∀ y χ])
        (proof
          [ax4 y φ]
          [syl [∀ y φ] φ [→ ψ ∀ y ψ]]
          [!al!2imti x [∀ y φ] ψ [∀ y ψ]]
          [ax7 x y ψ]
          [!im!rp33 [∀ x ∀ y φ] [∀ x ψ] [∀ x ∀ y ψ] [∀ y ∀ x ψ]]
          [!im!com23i φ [= x y] ψ χ]
          [!im!rp33d φ ψ [= x y] χ [∀ x χ]]
          [!al!2imti x φ ψ [→ = x y ∀ x χ]]
          [ax9alt x y χ]
          [!im!rp33 [∀ x φ] [∀ x ψ] [∀ x → = x y ∀ x χ] [χ]]
          [!al!2imti y [∀ x φ] [∀ x ψ] χ]
          [ax7 x y φ]
          [syl [∀ x ∀ y φ] [∀ y ∀ x φ] [→ ∀ y ∀ x ψ ∀ y χ]]
          [!im!trd [∀ x ∀ y φ] [∀ x ψ] [∀ y ∀ x ψ] [∀ y χ]]))

    (th "cbv2-dv" (var ![x y] wff ![φ ψ χ])        
        (dist (!x !y))
        (hypo [→ φ → ψ ∀ y ψ] [→ φ → χ ∀ x χ] [→ φ → = x y ↔ ψ χ])
        (ass [→ ∀ x ∀ y φ ↔ ∀ x ψ ∀ y χ])
        (proof
          [bi> ψ χ]
          [!im!rp33 [φ] [= x y] [↔ ψ χ] [→ ψ χ]]
          [cbv1-dv x y φ ψ χ]
          [bi< ψ χ]
          [!im!rp33 [φ] [= x y] [↔ ψ χ] [→ χ ψ]]
          [→ = y x = x y]
          [!im!rp32 [φ] [= x y] [→ χ ψ] [= y x]]
          [cbv1-dv y x φ χ ψ]
          [ax7 x y φ]
          [syl [∀ x ∀ y φ] [∀ y ∀ x φ] [→ ∀ y χ ∀ x ψ]]
          [>bid [∀ x ∀ y φ] [∀ x ψ] [∀ y χ]]))

    (th "cbval-dv" (var ![x y] wff ![φ ψ])
        (dist (!x !y))
        (hypo [Bvp y φ] [Bvp x ψ] [→ = x y ↔ φ ψ])
        (ass [↔ ∀ x φ ∀ y ψ])
        (proof
          [→ φ ∀ y φ]
          [→ ψ ∀ x ψ]
          [infer syl< φ [∀ y φ] φ]
          [infer ax1 [→ ψ ∀ x ψ] [→ φ φ]]
          [infer ax1 [→ = x y ↔ φ ψ] [→ φ φ]]
          [cbv2-dv x y [→ φ φ] φ ψ]
          [id φ]
          [ax-gen y [→ φ φ]]
          [ax-gen x [∀ y → φ φ]]
          [ax-mp [∀ x ∀ y → φ φ] [↔ ∀ x φ ∀ y ψ]]))
    
    (th "cbvald-dv" (var ![x y] wff ![φ ψ χ])
        (dist (!x !φ) (!x !χ) (!x !y))
        (hypo [Bvp y φ] [→ φ Bvp y ψ]
              [→ φ → = x y ↔ ψ χ])
        (ass [→ φ ↔ ∀ x ψ ∀ y χ])
        (proof
          [ded df-Bvp y ψ φ]
          [ded ax4 y [→ ψ ∀ y ψ] φ]
          [!bv!imt y φ ψ]
          [ax17-bv x [→ φ χ]]
          [com12i φ [= x y] [↔ ψ χ]]
          [ded !im!di-bi< φ ψ χ [= x y]]
          [cbval-dv x y [→ φ ψ] [→ φ χ]]
          [distv imgen> x φ ψ]
          [imgen> y φ χ]
          [!bi!>tr [∀ x → φ ψ] [∀ y → φ χ]
                   [→ φ ∀ x ψ] [→ φ ∀ y χ]]
          [infer !im!di-bi< φ [∀ x ψ] [∀ y χ]]))
    
    (th "cbvexd-dv" (var ![x y] wff ![φ ψ χ])
        (dist (!x !φ) (!x !χ) (!x !y))
        (hypo [Bvp y φ] [→ φ Bvp y ψ]
              [→ φ → = x y ↔ ψ χ])
        (ass [→ φ ↔ ∃ x ψ ∃ y χ])
        (proof
          [!bv!negd y φ ψ]
          [conb ψ χ]
          [b2i !im!rp33 φ [= x y] [↔ ψ χ] [↔ ¬ ψ ¬ χ]]
          [cbvald-dv x y φ [¬ ψ] [¬ χ]]
          [ded neqt [∀ x ¬ ψ] [∀ y ¬ χ] φ]
          [df-ex x ψ]
          [df-ex y χ]
          [!bi!<trg [φ] [¬ ∀ x ¬ ψ] [¬ ∀ y ¬ χ] [∃ x ψ] [∃ y χ]]))
    
    (proof
      [ax9ex z y]
      [→ ∀ x = x y ∃ x = x y]
      [ax12-bv z y x]
      [ax16-bv x z [= z y]]
      [ded ax1 [Bvp x = z y] [¬ ∀ x = x y] [∀ x = x z]]
      [casei [∀ x = x z] [→ ¬ ∀ x = x y Bvp x = z y]]
      [Bvp x [∀ x = x y]]
      [Bvp x [¬ ∀ x = x y]]
      [→ = z x ↔ = z y = x y]
      [infer ax1 [→ = z x ↔ = z y = x y] [¬ ∀ x = x y]]
      [cbvexd-dv z x [¬ ∀ x = x y] [= z y] [= x y]]
      [!bi!mpi> [¬ ∀ x = x y] [∃ z = z y] [∃ x = x y]]
      [casei [∀ x = x y] [∃ x = x y]]))

(th "ax9alt-sv" (var ![x y] wff "φ")
    (meta (:comment . "Alternative version of !ax9 without distinct variable conditions."))
    (ass [→ ∀ x → = x y ∀ x φ φ])
    (proof
      [ax9-sv x y]
      [!ex!imt x [= x y] [∀ x φ]]
      [mpi [∀ x → = x y ∀ x φ] [∃ x = x y] [∃ x ∀ x φ]]
      [ax6e x φ]
      [syl [∀ x → = x y ∀ x φ] [∃ x ∀ x φ] φ]))

;; Formulae for implicit substitution

(th "eqs3lem" (var "x" set "y" wff "φ")
    (meta (:comment . "Lemma used in proofs of substitution properties.")
          (:description . "equs3 in set.mm."))
    (ass [↔ ∃ x ∧ = x y φ ¬ ∀ x → = x y ¬ φ])
    (proof
      [↔ ∀ x → = x y ¬ φ ¬ ∃ x ∧ = x y φ]
      [conb>i [∀ x → = x y ¬ φ] [∃ x ∧ = x y φ]]))

(th "eqs4lem" (var "x" set "y" wff "φ")
    (meta (:comment . "Lemma used in proofs of substitution properties.")
          (:description . "equs4 in set.mm, but with x and y distinct."))
    (dist (!x !y))
    (ass [→ ∀ x → = x y φ ∃ x ∧ = x y φ])

    ; Lemma - proposition calculus
    (th "msca" (wff ![φ ψ χ θ])
        (hypo [→ φ → ψ χ] [→ θ → ψ ¬ χ])
        (ass [→ φ → ψ ¬ θ])
        (proof
          [!an!sim> φ ψ]
          [imp φ ψ χ]
          [!im!jc [∧ φ ψ] ψ χ]
          [infer con θ [→ ψ ¬ χ]]
          [syl [∧ φ ψ] [¬ → ψ ¬ χ] [¬ θ]]
          [exp φ ψ [¬ θ]]))
	
    (proof
      [ax4 x [→ = x y φ]]
      [ax4 x [→ = x y ¬ φ]]
      [msca [∀ x → = x y φ] [= x y] [φ] [∀ x → = x y ¬ φ]]
      [Bvp x [∀ x → = x y ¬ φ]]
      [Bvp x [¬ ∀ x → = x y ¬ φ]]
      [!bv!exp x [¬ ∀ x → = x y ¬ φ]]
      [!im!rp33 [∀ x → = x y φ] [= x y] [¬ ∀ x → = x y ¬ φ] [∀ x ¬ ∀ x → = x y ¬ φ]]
      [infer ax5 x [→ = x y φ] [→ = x y ∀ x ¬ ∀ x → = x y ¬ φ]]
      [ax9alt x y [¬ ∀ x → = x y ¬ φ]]
      [syl [∀ x → = x y φ] [∀ x → = x y ∀ x ¬ ∀ x → = x y ¬ φ] [¬ ∀ x → = x y ¬ φ]]
      [eqs3lem x y φ]
      [b2i syl [∀ x → = x y φ] [¬ ∀ x → = x y ¬ φ] [∃ x ∧ = x y φ]]))

(th "eqs5lem" (var "x" set "y" wff "φ")
    (meta (:comment . "Lemma used in proofs of substitution properties.")
          (:description . "equs5 in set.mm, with x and y distinct."))
    (dist (!x !y))
    (ass [→ ∃ x ∧ = x y φ ∀ x → = x y φ])
    (proof
      [ax11 x y φ]
      [imp [= x y] φ [∀ x → = x y φ]]
      [Bvp x [∀ x → = x y φ]]
      [→ ∃ x ∧ = x y φ ∀ x → = x y φ]))

(th "eqsal" (var "x" set "y" wff ![φ ψ])
    (meta (:comment . "A useful equivalence related to substitution.")
          (:description . "From set.mm. In this version x must not occur in y."))
    (dist (!x !y))
    (hypo [Bvp x ψ] [→ = x y ↔ φ ψ])
    (ass [↔ ∀ x → = x y φ ψ])
    (proof
      [!bv!eq.al x ψ]
      [infer ax1 [↔ ψ ∀ x ψ] [= x y]]
      [!bi!trd [= x y] φ ψ [∀ x ψ]]
      [infer !im!di-bi< [= x y] [φ] [∀ x ψ]]
      [↔ [∀ x → = x y φ] [∀ x → = x y ∀ x ψ]]
      [ax1 [∀ x ψ] [= x y]]
      [infer ax5 x ψ [→ = x y ∀ x ψ]]
      [→ ψ ∀ x ψ]
      [syl ψ [∀ x ψ] [∀ x → = x y ∀ x ψ]]
      [ax9alt x y ψ]
      [>bii [ψ] [∀ x → = x y ∀ x ψ]]
      [b2i bitr [∀ x → = x y φ] [∀ x → = x y ∀ x ψ] ψ]))

(th "eqsex" (var "x" set "y" wff ![φ ψ])
    (meta (:comment . "A useful equivalence related to substitution.")
          (:description . "From set.mm. In this version x must not occur in y."))
    (dist (!x !y))
    (hypo [Bvp x ψ] [→ = x y ↔ φ ψ])
    (ass [↔ ∃ x ∧ = x y φ ψ])
    (proof
      [↔ ∃ x ¬ → = x y ¬ φ ¬ ∀ x → = x y ¬ φ]
      [df-an [= x y] [φ]]
      [!ex!eqti x [∧ = x y φ] [¬ → = x y ¬ φ]]
      [Bvp x ¬ ψ]
      [ded neqt φ ψ [= x y]]
      [eqsal x y [¬ φ] [¬ ψ]]
      [conb>i [∀ x → = x y ¬ φ] [ψ]]
      [!bi!<tr [∃ x ¬ → = x y ¬ φ] [¬ ∀ x → = x y ¬ φ]
               [∃ x ∧ = x y φ] [ψ]]))

(th "dral1" (var ![x y] wff ![φ ψ])
    (meta (:comment . "Formula-building lemma for use with the Distinctor Reduction Theorem.")
          (:description . "From set.mm. Part of Theorem 9.4 of [Megill] p. 448."))
    (hypo [→ ∀ x = x y ↔ φ ψ])
    (ass [→ ∀ x = x y ↔ ∀ x φ ∀ y ψ])
    (proof
      [ded bi> φ ψ [∀ x = x y]]
      [!al!2imti x [∀ x = x y] φ ψ]
      [!distinctor!bv x x y]
      [!bv!syl x [∀ x = x y] [→ ∀ x φ ∀ x ψ]]
      [ax10 x y ψ]
      [!im!trd [∀ x = x y] [∀ x φ] [∀ x ψ] [∀ y ψ]]
      [ded bi< φ ψ [∀ x = x y]]
      [!al!2imti y [∀ x = x y] ψ φ]
      [!distinctor!bv y x y]
      [!bv!syl y [∀ x = x y] [→ ∀ y ψ ∀ y φ]]
      [ax10 y x φ]
      [+syl !distinctor!com x y [→ ∀ y φ ∀ x φ]]
      [!im!trd [∀ x = x y] [∀ y ψ] [∀ y φ] [∀ x φ]]
      [>bid [∀ x = x y] [∀ x φ] [∀ y ψ]]))

(th "dral2" (var ![x y z] wff ![φ ψ])
    (meta (:comment . "Formula-building lemma for use with the Distinctor Reduction Theorem.")
          (:description . "From set.mm. Part of Theorem 9.4 of [Megill] p. 448."))
    (hypo [→ ∀ x = x y ↔ φ ψ])
    (ass [→ ∀ x = x y ↔ ∀ z φ ∀ z ψ])
    (proof
      [!distinctor!bv z x y]
      [!al!eqtd z [∀ x = x y] φ ψ]))

(th "drex1" (var ![x y] wff ![φ ψ])
    (meta (:comment . "Formula-building lemma for use with the Distinctor Reduction Theorem.")
          (:description . "From set.mm. Part of Theorem 9.4 of [Megill] p. 448."))
    (hypo [→ ∀ x = x y ↔ φ ψ])
    (ass [→ ∀ x = x y ↔ ∃ x φ ∃ y ψ])
    (proof
      [ded neqt φ ψ [∀ x = x y]]
      [dral1 x y [¬ φ] [¬ ψ]]
      [ded neqt [∀ x ¬ φ] [∀ y ¬ ψ] [∀ x = x y]]
      [df-ex x φ]
      [df-ex y ψ]
      [!bi!<trg [∀ x = x y] [¬ ∀ x ¬ φ] [¬ ∀ y ¬ ψ] [∃ x φ] [∃ y ψ]]))

(th "drex2" (var ![x y z] wff ![φ ψ])
    (meta (:comment . "Formula-building lemma for use with the Distinctor Reduction Theorem.")
          (:description . "From set.mm. Part of Theorem 9.4 of [Megill] p. 448."))
    (hypo [→ ∀ x = x y ↔ φ ψ])
    (ass [→ ∀ x = x y ↔ ∃ z φ ∃ z ψ])
    (proof
      [!distinctor!bv z x y]
      [!ex!eqtd z [∀ x = x y] φ ψ]))

(th "ax4a" (var ![x y] wff ![φ ψ])
    (meta (:comment . "Specialization with implicit substitution.")
          (:description . "From set.mm. Compare Lemma 14 of [Tarski] p. 70."))
    (hypo [Bvp x ψ] [→ = x y → φ ψ])
    (ass [→ ∀ x φ ψ])
    (proof
      [→ ψ ∀ x ψ]
      [com12i [= x y] φ ψ]
      [!im!rp33 φ [= x y] ψ [∀ x ψ]]
      [!al!imti x φ [→ = x y ∀ x ψ]]
      [ax9alt-sv x y ψ]
      [syl [∀ x φ] [∀ x → = x y ∀ x ψ] ψ]))

(th "ax4c" (var ![x y] wff ![φ ψ])
    (meta (:comment . "Existential introduction with implicit substitution.")
          (:description . "From set.mm. Compare Lemma 14 of [Tarski] p. 70."))
    (hypo [Bvp x φ] [→ = x y → φ ψ])
    (ass [→ φ ∃ x ψ])
    (proof
      [Bvp x [¬ φ]]
      [ded con φ ψ [= x y]]
      [ax4a x y [¬ ψ] [¬ φ]]
      [infer con> [∀ x ¬ ψ] φ]
      [df-ex x ψ]
      [b2i syl φ [¬ ∀ x ¬ ψ] [∃ x ψ]]))

(th "ax4c1" (var ![x y] wff ![φ ψ χ])
    (meta (:comment . "A more general version of !a4c.")
          (:description . "From set.mm."))
    (hypo [Bvp x χ] [→ χ → φ ∀ x φ] [→ = x y → φ ψ])
    (ass [→ χ → φ ∃ x ψ])
    (proof
      [→ χ ∀ x χ]
      [infer !an!add> χ [∀ x χ] φ]
      [imp χ φ [∀ x φ]]
      [jca [∧ χ φ] [∀ x χ] [∀ x φ]]
      [!al!di-an x χ φ]
      [b2i syl [∧ χ φ] [∧ ∀ x χ ∀ x φ] [∀ x ∧ χ φ]]
      [Bvp x [∧ χ φ]]
      [ded !an!add< φ ψ χ [= x y]]
      [ax4c x y [∧ χ φ] ψ]
      [exp χ φ [∃ x ψ]]))

;;; Change-of-variables formulae

;; Change to use Bvp
(th "cbv1" (var ![x y] wff ![φ ψ χ])
    (meta (:comment . "Rule used to change bound variables with implicit substitution.")
          (:description . "From set.mm."))
    (hypo [→ φ → ψ ∀ y ψ] [→ φ → χ ∀ x χ] [→ φ → = x y → ψ χ])
    (ass [→ ∀ x ∀ y φ → ∀ x ψ ∀ y χ])
    (proof
      [ax4 y φ]
      [syl [∀ y φ] φ [→ ψ ∀ y ψ]]
      [!al!2imti x [∀ y φ] ψ [∀ y ψ]]
      [ax7 x y ψ]
      [!im!rp33 [∀ x ∀ y φ] [∀ x ψ] [∀ x ∀ y ψ] [∀ y ∀ x ψ]]
      [!im!com23i φ [= x y] ψ χ]
      [!im!rp33d φ ψ [= x y] χ [∀ x χ]]
      [!al!2imti x φ ψ [→ = x y ∀ x χ]]
      [ax9alt-sv x y χ]
      [!im!rp33 [∀ x φ] [∀ x ψ] [∀ x → = x y ∀ x χ] [χ]]
      [!al!2imti y [∀ x φ] [∀ x ψ] χ]
      [ax7 x y φ]
      [syl [∀ x ∀ y φ] [∀ y ∀ x φ] [→ ∀ y ∀ x ψ ∀ y χ]]
      [!im!trd [∀ x ∀ y φ] [∀ x ψ] [∀ y ∀ x ψ] [∀ y χ]]))

;; Change to use Bvp
(th "cbv2" (var ![x y] wff ![φ ψ χ])
    (meta (:comment . "Rule used to change bound variables with implicit substitution.")
          (:description . "From set.mm."))
    (hypo [→ φ → ψ ∀ y ψ] [→ φ → χ ∀ x χ] [→ φ → = x y ↔ ψ χ])
    (ass [→ ∀ x ∀ y φ ↔ ∀ x ψ ∀ y χ])
    (proof
      [bi> ψ χ]
      [!im!rp33 φ [= x y] [↔ ψ χ] [→ ψ χ]]
      [cbv1 x y φ ψ χ]
      [bi< ψ χ]
      [!im!rp33 φ [= x y] [↔ ψ χ] [→ χ ψ]]
      [→ = y x = x y]
      [!im!rp32 φ [= x y] [→ χ ψ] [= y x]]
      [cbv1 y x φ χ ψ]
      [ax7 x y φ]
      [syl [∀ x ∀ y φ] [∀ y ∀ x φ] [→ ∀ y χ ∀ x ψ]]
      [>bid [∀ x ∀ y φ] [∀ x ψ] [∀ y χ]]))

; -> cbv-ali
(th "cbv3" (var ![x y] wff ![φ ψ])
    (meta (:comment . "Rule used to change bound variables with implicit substitution.")
          (:description . "From set.mm."))
    (hypo [Bvp y φ] [Bvp x ψ] [→ = x y → φ ψ])
    (ass [→ ∀ x φ ∀ y ψ])
    (proof
      [→ φ ∀ y φ]
      [→ ψ ∀ x ψ]
      [infer syl< φ [∀ y φ] φ]
      [infer ax1 [→ ψ ∀ x ψ] [→ φ φ]]
      [infer ax1 [→ = x y → φ ψ] [→ φ φ]]
      [cbv1 x y [→ φ φ] φ ψ]
      [id φ]
      [ax-gen y [→ φ φ]]
      [ax-gen x [∀ y → φ φ]]
      [ax-mp [∀ x ∀ y → φ φ] [→ ∀ x φ ∀ y ψ]]))

; -> cbv-al
(th "cbval" (var ![x y] wff ![φ ψ])
    (meta (:comment . "Rule used to change bound variables with implicit substitution.")
          (:description . "From set.mm."))
    (hypo [Bvp y φ] [Bvp x ψ] [→ = x y ↔ φ ψ])
    (ass [↔ ∀ x φ ∀ y ψ])
    (proof
      [ded bi> φ ψ [= x y]]
      [cbv3 x y φ ψ]
      [ded bi< φ ψ [= x y]]
      [+syl [→ = y x = x y] [→ ψ φ]]
      [cbv3 y x ψ φ]
      [>bii [∀ x φ] [∀ y ψ]]))

; -> cbv-ex
(th "cbvex" (var ![x y] wff ![φ ψ])
    (meta (:comment . "Rule used to change bound variables with implicit substitution.")
          (:description . "From set.mm."))
    (hypo [Bvp y φ] [Bvp x ψ] [→ = x y ↔ φ ψ])
    (ass [↔ ∃ x φ ∃ y ψ])
    (proof
      [Bvp y ¬ φ]
      [Bvp x ¬ ψ]
      [ded neqt φ ψ [= x y]]
      [cbval x y [¬ φ] [¬ ψ]]
      [infer neqt [∀ x ¬ φ] [∀ y ¬ ψ]]
      [df-ex x φ]
      [df-ex y ψ]
      [!bi!<tr [¬ ∀ x ¬ φ] [¬ ∀ y ¬ ψ] [∃ x φ] [∃ y ψ]]))

; -> cbv-2al
(th "cbval2" (var ![x y z w] wff ![φ ψ])
    (meta (:comment . "Rule used to change bound variables with implicit substitution.")
          (:description . "From set.mm."))
    (dist (!x !y) (!y !z) (!z !w) (!w !x))
    (hypo [Bvp z φ] [Bvp w φ] [Bvp x ψ] [Bvp y ψ]
          [→ ∧ = x z = y w ↔ φ ψ])
    (ass [↔ ∀ x ∀ y φ ∀ z ∀ w ψ])
    (proof
      [Bvp z [∀ y φ]]
      [Bvp x [∀ w ψ]]
      [Bvp w [= x z]]
      [!bv!an w [= x z] φ]
      [Bvp y [= x z]]
      [!bv!an y [= x z] ψ]
      [exp [= x z] [= y w] [↔ φ ψ]]
      [com12i [= x z] [= y w] [↔ φ ψ]]
      [ded !im.bidian [= x z] φ ψ [= y w]]
      [cbval y w [∧ = x z φ] [∧ = x z ψ]]
      [!al!di-an< y [= x z] φ]
      [!al!di-an< w [= x z] ψ]
      [!bi!>tr [∀ y ∧ = x z φ] [∀ w ∧ = x z ψ]
               [∧ = x z ∀ y φ] [∧ = x z ∀ w ψ]]
      [infer !im.bidian [= x z] [∀ y φ] [∀ w ψ]]
      [cbval x z [∀ y φ] [∀ w ψ]]))

; -> cbv-2ex
(th "cbvex2" (var ![x y z w] wff ![φ ψ])
    (meta (:comment . "Rule used to change bound variables with implicit substitution.")
          (:description . "From set.mm."))
    (dist (!x !y) (!y !z) (!z !w) (!w !x))
    (hypo [Bvp z φ] [Bvp w φ] [Bvp x ψ] [Bvp y ψ]
          [→ ∧ = x z = y w ↔ φ ψ])
    (ass [↔ ∃ x ∃ y φ ∃ z ∃ w ψ])
    (proof
      [Bvp z [∃ y φ]]
      [Bvp x [∃ w ψ]]
      [Bvp w [= x z]]
      [Bvp w [∧ = x z φ]]
      [Bvp y [= x z]]
      [Bvp y [∧  = x z ψ]]
      [exp [= x z] [= y w] [↔ φ ψ]]
      [com12i [= x z] [= y w] [↔ φ ψ]]
      [ded !im.bidian [= x z] φ ψ [= y w]]
      [cbvex y w [∧ = x z φ] [∧ = x z ψ]]
      [!ex!di-an< y [= x z] φ]
      [!ex!di-an< w [= x z] ψ]
      [!bi!>tr [∃ y ∧ = x z φ] [∃ w ∧ = x z ψ]
               [∧ = x z ∃ y φ] [∧ = x z ∃ w ψ]]
      [infer !im.bidian [= x z] [∃ y φ] [∃ w ψ]]
      [cbvex x z [∃ y φ] [∃ w ψ]]))

(th "cbvald" (var ![x y] wff ![φ ψ χ])
    (meta (:comment . "Deduction used to change bound variables with implicit substitution.")
          (:description . "From set.mm. Particulary useful in conjunction with !dvelim."))
    (dist (!x !φ) (!x !χ))
    (hypo [Bvp y φ] [→ φ Bvp y ψ] ;[→ ψ ∀ y ψ]
          [→ φ → = x y ↔ ψ χ])
    (ass [→ φ ↔ ∀ x ψ ∀ y χ])
    (proof
      [ded df-Bvp y ψ φ]
      [ded ax4 y [→ ψ ∀ y ψ] φ]
      [!bv!imt y φ ψ]
      [ax17-bv x [→ φ χ]]
      [com12i φ [= x y] [↔ ψ χ]]
      [ded !im!di-bi< φ ψ χ [= x y]]
      [cbval x y [→ φ ψ] [→ φ χ]]
      [distv imgen> x φ ψ]
      [imgen> y φ χ]
      [!bi!>tr [∀ x → φ ψ] [∀ y → φ χ]
               [→ φ ∀ x ψ] [→ φ ∀ y χ]]
      [infer !im!di-bi< φ [∀ x ψ] [∀ y χ]]))

(th "cbvexd" (var ![x y] wff ![φ ψ χ])
    (meta (:comment . "Deduction used to change bound variable with implicit substitution.")
          (:description . "From set.mm. Particulary useful in conjunction with !dvelim."))
    (dist (!x !φ) (!x !χ))
    (hypo [Bvp y φ] [→ φ Bvp y ψ] ; [→ ψ ∀ y ψ]
          [→ φ → = x y ↔ ψ χ])
    (ass [→ φ ↔ ∃ x ψ ∃ y χ])
    (proof
      [!bv!negd y φ ψ]
      [conb ψ χ]
      [b2i !im!rp33 φ [= x y] [↔ ψ χ] [↔ ¬ ψ ¬ χ]]
      [cbvald x y φ [¬ ψ] [¬ χ]]
      [ded neqt [∀ x ¬ ψ] [∀ y ¬ χ] φ]
      [df-ex x ψ]
      [df-ex y χ]
      [!bi!<trg φ [¬ ∀ x ¬ ψ] [¬ ∀ y ¬ χ] [∃ x ψ] [∃ y χ]]))

(th "chvar" (var ![x y] wff ![φ ψ])
    (meta (:comment . "Implicit substitutino of y for x into a theorem.")
          (:description . "From set.mm."))
    (hypo [Bvp x ψ] [→ = x y ↔ φ ψ] [φ])
    (ass [ψ])
    (proof
      [ded bi> φ ψ [= x y]]
      [ax4a x y φ ψ]
      [mpg x φ ψ]))

(th "eqvini-sv" (var ![x y z])
    (meta (:comment . "A variable introduction law for equality.")
          (:description . "equvini in set.mm. Lemma 15 of [Monk2] p. 109, however we do not require z to be distinct from x and y."))
    (ass [→ = x y ∃ z ∧ = x z = z y])
    (proof
      [ax9-sv z y]
      [= z z]
      [!an!jct< [= z y] [= z z]]
      [!ex!imti z [= z y] [∧ = z z = z y]]
      [ax-mp [∃ z = z y] [∃ z ∧ = z z = z y]]
      [Bvp z [∀ z = z x]]
      [ax8 z x z]
      [+syl ax4 z [= z x] [→ = z z = x z]]
      [ded !an!imt< [= z z] [= x z] [= z y] [∀ z = z x]]
      [!ex!imtd z [∀ z = z x] [∧ = z z = z y] [∧ = x z = z y]]
      [mpi [∀ z = z x] [∃ z ∧ = z z = z y] [∃ z ∧ = x z = z y]]
      [ax9-sv z x]
      [→ = z x = x z]
      [ded !an!jct> [= x z] [= z z] [= z x]]
      [!ex!imti z [= z x] [∧ = x z = z z]]
      [ax-mp [∃ z = z x] [∃ z ∧ = x z = z z]]
      [Bvp z [∀ z = z y]]
      [ax1 [= z y] [= z z]]
      [+syl ax4 z [= z y] [→ = z z = z y]]
      [ded !an!imt> [= z z] [= z y] [= x z] [∀ z = z y]]
      [!ex!imtd z [∀ z = z y] [∧ = x z = z z] [∧ = x z = z y]]
      [mpi [∀ z = z y] [∃ z ∧ = x z = z z] [∃ z ∧ = x z = z y]]
      [jaoi [∀ z = z x] [∀ z = z y] [∃ z ∧ = x z = z y]]
      [ded ax1 [∃ z ∧ = x z = z y] [= x y] [∨ ∀ z = z x ∀ z = z y]]
      [!or!neg [∀ z = z x] [∀ z = z y]]
      [Bvp z [∀ z = z x]]
      [Bvp z [∀ z = z y]]
      [Bvp z [¬ ∀ z = z x]]
      [Bvp z [¬ ∀ z = z y]]
      [Bvp z [∧ ¬ ∀ z = z x ¬ ∀ z = z y]]
      [ax12 x y z]
      [imp [¬ ∀ z = z x] [¬ ∀ z = z y] [→ = x y ∀ z = x y]]
      [ax8 x z y]
      [infer !an!join2 [= x z] [= x y] [= z y]]
      [+syl [→ = z x = x z] [→ = x y ∧ = x z = z y]]
      [ax4c1 z x [= x y] [∧ = x z = z y] [∧ ¬ ∀ z = z x ¬ ∀ z = z y]]
      [infer bi> [¬ ∨ ∀ z = z x ∀ z = z y] [∧ ¬ ∀ z = z x ¬ ∀ z = z y]]
      [syl [¬ ∨ ∀ z = z x ∀ z = z y] [∧ ¬ ∀ z = z x ¬ ∀ z = z y] [→ = x y ∃ z ∧ = x z = z y]]
      [casei [∨ ∀ z = z x ∀ z = z y] [→ = x y ∃ z ∧ = x z = z y]]))

(th "eqvini" (set ![x y] var "z")
    (meta (:comment . "A variable introduction law for equality."))
    (dist (!z !x) (!z !y))
    (ass [→ = x y ∃ z ∧ = x z = z y])
    (proof
      [eqtr z x y]
      [+syl [→ = x z = z x] [→ = x y = z y]]
      [com12i [= x z] [= x y] [= z y]]
      [imp [= x y] [= x z] [= z y]]
      [!im!refld [= x y] [= x z]]
      [imp [= x y] [= x z] [= x z]]
      [jca [∧ = x y = x z] [= x z] [= z y]]
      [exp [= x y] [= x z] [∧ = x z = z y]]
      [ax17-bv z [= x y]]
      [distv !ex!imtd z [= x y] [= x z] [∧ = x z = z y]]
      [ax9ex z x]
      [→ = z x = x z]
      [!ex!imti z [= z x] [= x z]]
      [ax-mp [∃ z = z x] [∃ z = x z]]
      [mpi [= x y] [∃ z = x z] [∃ z ∧ = x z = z y]]))

(th "eqvin" (set ![x y] var "z")
    (meta (:comment . "A variable introduction law for equality."))
    (dist (!x !z) (!y !z))
    (ass [↔ = x y ∃ z ∧ = x z = z y])
    (proof
      [eqvini x y z]
      [ax17-bv z [= x y]]
      [eqtr x z y]
      [imp [= x z] [= z y] [= x y]]
      [imgen<i [z] [∧ = x z = z y] [= x y]]
      [>bii [= x y] [∃ z ∧ = x z = z y]]))

;;;;;;;;; Proper substitution (move to its own file?)
(in-context !subst

  ;; Alternate definitions

  (th "df-f" (set "y" var "x" wff "φ" var "z")
    (meta (:comment . "Definition of proper substitution")
          (:description . "Stronger than df-sb: z need not be distinct from phi."))
    (dist (!z !y) (!z !x))
    (hypo [Bvp z φ])
    (ass [↔ sb y x φ ∃ z ∧ = z y ∃ x ∧ = x z φ])
    (loc var "w")
    (proof
      [df-sb y x φ w]
      [→ = w z ↔ = x w = x z]
      [ded !an!eqt< [= x w] [= x z] φ [= w z]]
      [ax17-bv x [= w z]]
      [!ex!eqtd x [= w z] [∧ = x w φ] [∧ = x z φ]]
      [→ = w z ↔ = w y = z y]
      [!an!eqtd [= w z] [= w y] [= z y]
                [∃ x ∧ = x w φ] [∃ x ∧ = x z φ]]
      [ax17-bv w [∧ = z y ∃ x ∧ = x z φ]]
      [ax17-bv z [= w y]]
      [ax17-bv z [= x w]]
      [Bvp z [∧ = x w φ]]
      [Bvp z [∃ x ∧ = x w φ]]
      [Bvp z [∧ = w y ∃ x ∧ = x w φ]]
      [cbvex w z [∧ = w y ∃ x ∧ = x w φ] [∧ = z y ∃ x ∧ = x z φ]]
      [bitr [sb y x φ]
            [∃ w ∧ = w y ∃ x ∧ = x w φ]
            [∃ z ∧ = z y ∃ x ∧ = x z φ]]))

  (th "df-dvex" (set "y" var "x" wff "φ")
    (meta (:comment . "Define proper substitution")
          (:descrpition . "Simple case: x and y distinct"))
    (dist (!y !x))
    (ass [↔ sb y x φ ∃ x ∧ = x y φ])
    (loc var "z")
    (proof
      [df-sb y x φ z]
      [ax17-bv x [= z y]]
      [→ = z y ↔ = x z = x y]
      [ded !an!eqt< [= x z] [= x y] φ [= z y]]
      [!ex!eqtd x [= z y] [∧ = x z φ] [∧ = x y φ]]
      [ded bi> [∃ x ∧ = x z φ] [∃ x ∧ = x y φ] [= z y]]
      [ded bi< [∃ x ∧ = x z φ] [∃ x ∧ = x y φ] [= z y]]
      [!an!sim< [= z y] [∃ x ∧ = x z φ]]
      [!an!sim< [= z y] [∃ x ∧ = x y φ]]
      [!an!sim> [= z y] [∃ x ∧ = x z φ]]
      [!an!sim> [= z y] [∃ x ∧ = x y φ]]
      [syl [∧ = z y ∃ x ∧ = x z φ] [= z y] [→ ∃ x ∧ = x z φ ∃ x ∧ = x y φ]]
      [mpd [∃ x ∧ = x z φ] [∃ x ∧ = x y φ] [∧ = z y ∃ x ∧ = x z φ]]
      [jca [∧ = z y ∃ x ∧ = x z φ] [= z y] [∃ x ∧ = x y φ]] ;; One direction
      [syl [∧ = z y ∃ x ∧ = x y φ] [= z y] [→ ∃ x ∧ = x y φ ∃ x ∧ = x z φ]]
      [mpd [∃ x ∧ = x y φ] [∃ x ∧ = x z φ] [∧ = z y ∃ x ∧ = x y φ]]
      [jca [∧ = z y ∃ x ∧ = x y φ] [= z y] [∃ x ∧ = x z φ]] ;; Other direction
      [>bii [∧ = z y ∃ x ∧ = x z φ] [∧ = z y ∃ x ∧ = x y φ]]
      [!ex!eqti z [∧ = z y ∃ x ∧ = x z φ] [∧ = z y ∃ x ∧ = x y φ]]
      [ax17-bv z [∃ x ∧ = x y φ]]
      [!ex!di-an> z [= z y] [∃ x ∧ = x y φ]]
      [2bitr [sb y x φ] [∃ z ∧ = z y ∃ x ∧ = x z φ] [∃ z ∧ = z y ∃ x ∧ = x y φ]
             [∧ ∃ z = z y ∃ x ∧ = x y φ]]
      [ax9ex z y]
      [infer !an!true< [∃ z = z y] [∃ x ∧ = x y φ]]
      [b2i bitr [sb y x φ] [∧ ∃ z = z y ∃ x ∧ = x y φ] [∃ x ∧ = x y φ]]))

  ;; Equality theorems (substituted terms)

  ;; Equality theorem for proper substitution (was: eqt-sb)
  (th "eqt-term" (set ![x y] var "z" wff "φ")
    (meta (:comment . "Equality theorem for proper substitution."))
    (ass [→ = x y ↔ sb x z φ sb y z φ])
    (loc var "v")
    (proof
      [→ = x y ↔ = v x = v y]
      [ax17-bv v [= x y]]
      [ded !an!eqt< [= v x] [= v y] [∃ z ∧ = z v φ] [= x y]]
      [!ex!eqtd v [= x y] [∧ = v x ∃ z ∧ = z v φ]
                          [∧ = v y ∃ z ∧ = z v φ]]
      [df-sb x z φ v]
      [df-sb y z φ v]
      [!bi!<trg [= x y] [∃ v ∧ = v x ∃ z ∧ = z v φ] [∃ v ∧ = v y ∃ z ∧ = z v φ]
                        [sb x z φ]                  [sb y z φ]]))
  (th-pattern !eqt-term)

  ;; Equality theorem for proper substitution (was: eqt-sbi)
  ;; Theorem 2.1 of [Bernays]
  (th "imt-term" (set ![x y] var "z" wff "φ")
    (meta (:comment . "Equality theorem for proper substitution.")
          (:description . "Theorem 2.1 of [Bernays]"))
    (ass [→ = x y → sb x z φ sb y z φ])
    (proof
      [eqt-term x y z φ]
      [ded bi> [sb x z φ] [sb y z φ] [= x y]]))
  (th-pattern !imt-term) 

  ;; Bound variable condition (binds var, dist) (was: bv-sbb-dv)

  (th "bv-var" (set "y" var "x" wff "φ")
    (dist (!x !y))
    (ass [Bvp x sb y x φ])
    (loc var "z")
    (proof
      [Bvp x [∃ x ∧ = x z φ]]
      [ax17-bv x [= z y]]
      [Bvp x [∧ = z y ∃ x ∧ = x z φ]]
      [Bvp x [∃ z ∧ = z y ∃ x ∧ = x z φ]]
      [df-sb y x φ z]
      [!bv!def x [sb y x φ] [∃ z ∧ = z y ∃ x ∧ = x z φ]]))

  ;; Alternate definition (dist) dfsb-dval

  (th "df-dval" (set "y" var "x" wff "φ")
    (meta (:comment . "Alternate definition of proper substitution when x does not occur in y."))
    (dist (!y !x))
    (ass [↔ sb y x φ ∀ x → = x y φ])
    (proof
      [df-dvex y x φ]
      [eqs4lem x y φ]
      [eqs5lem x y φ]
      [>bii [∃ x ∧ = x y φ] [∀ x → = x y φ]]
      [bitr [sb y x φ] [∃ x ∧ = x y φ] [∀ x → = x y φ]]))

  ;; Composition (dist) sbco2w

  (th "comp-w" (set "z" var ![y x] wff "φ")
    (meta (:comment . "Composition of proper substitution (weak form)."))
    (dist (!y !z) (!y !x))
    (hypo [Bvp y φ])
    (ass [↔ sb z y sb y x φ sb z x φ])
    (proof
      [df-f z x φ y]
      [df-dvex z y [sb y x φ]]
      [df-dvex y x φ]
      [infer !an!eqt> [sb y x φ] [∃ x ∧ = x y φ] [= y z]]
      [!ex!eqti y [∧ = y z sb y x φ] [∧ = y z ∃ x ∧ = x y φ]]
      [bitr [sb z y sb y x φ] [∃ y ∧ = y z sb y x φ] [∃ y ∧ = y z ∃ x ∧ = x y φ]]
      [b2i bitr [sb z y sb y x φ] [∃ y ∧ = y z ∃ x ∧ = x y φ] [sb z x φ]]))

  ;; Alternate definition dfsb-alt

  (th "df-alt" (set "y" var "x" wff "φ" var "z")
    (meta (:comment . "Alternate definition of proper substitution with universal and existential quantifiers."))
    (dist (!z !y) (!z !x))
    (hypo [Bvp z φ])
    (ass [↔ sb y x φ ∃ z ∧ = z y ∀ x → = x z φ])
    (proof
      [comp-w y z x φ]
      [!bi!comi [sb y z sb z x φ] [sb y x φ]]
      [df-dval z x φ]
      [df-dvex y z [sb z x φ]]
      [infer !an!eqt> [sb z x φ] [∀ x → = x z φ] [= z y]]
      [!ex!eqti z [∧ = z y sb z x φ] [∧ = z y ∀ x → = x z φ]]
      [2bitr [sb y x φ] [sb y z sb z x φ] [∃ z ∧ = z y sb z x φ] [∃ z ∧ = z y ∀ x → = x z φ]]))

  ;; Identity law sbid

  (th "id" (var "x" wff "φ")
    (ass [↔ sb x x φ φ])
    (loc var "z")
    (proof
      [df-sb x x φ z]
      [ax9ex z x]
      [!an!jct< φ [∃ z = z x]]
      [ax17-bv z φ]
      [!ex!di-an> z [= z x] φ]
      [b2i syl φ [∧ ∃ z = z x φ] [∃ z ∧ = z x φ]]
      [eqcomi z x]
      [infer !an!imt< [= z x] [= x z] φ]
      [!ex!spec1 x [∧ = x z φ]]
      [syl [∧ = z x φ] [∧ = x z φ] [∃ x ∧ = x z φ]]
      [!an!sim< [= z x] φ]
      [jca [∧ = z x φ] [= z x] [∃ x ∧ = x z φ]]
      [!ex!imti z [∧ = z x φ] [∧ = z x ∃ x ∧ = x z φ]]
      [syl φ [∃ z ∧ = z x φ] [∃ z ∧ = z x ∃ x ∧ = x z φ]]
      [b2i syl φ [∃ z ∧ = z x ∃ x ∧ = x z φ] [sb x x φ]] ;; One half
      [df-alt x x φ z]
      [eqcomi z x]
      [ax4 x [→ = x z φ]]
      [infer !an!imt> [∀ x → = x z φ] [→ = x z φ] [= z x]]
      [infer !an!imt< [= z x] [= x z] [→ = x z φ]]
      [!an!mpclosed [= x z] φ]
      [3syl [∧ = z x ∀ x → = x z φ] [∧ = z x → = x z φ] [∧ = x z → = x z φ] φ]
      [imgen<i z [∧ = z x ∀ x → = x z φ] φ]
      [b2i syl [sb x x φ] [∃ z ∧ = z x ∀ x → = x z φ] φ] ;; The other half
      [>bii [sb x x φ] φ]))

  ;; Bound variables (var in φ, not in y) (was: bv-sb-d1)

  (th "bv1" (set "y" var "x" wff "φ" var "z")
    (dist (!z !y))
    (hypo [Bvp z φ])
    (ass [Bvp z [sb y x φ]])
    (loc var "w")
    (proof
      [ax10ex x z [∧ = x w φ]]
      [!distinctor!bv z x z]
      [Bvp z [¬ ∀ x = x z]]
      [Bvp z [∃ z ∧ = x w φ]]
      [!bv!defd z [∀ x = x z] [∃ x ∧ = x w φ] [∃ z ∧ = x w φ]] ; If x and z are same, Bvp(z, ∃ x ∧ = x w φ)
      [ax16 z w [= x w]]
      [ax12 x w z]
      [com12i [¬ ∀ z = z x] [¬ ∀ z = z w] [→ = x w ∀ z = x w]]
      [ded ax1 [→ = x w ∀ z = x w] [¬ ∀ z = z x] [∀ z = z w]]
      [casei [∀ z = z w] [→ ¬ ∀ z = z x → = x w ∀ z = x w]]
      [!distinctor!ncoms [→ = x w ∀ z = x w] z x]
      [Bvp x [∀ x = x z]]
      [Bvp x [¬ ∀ x = x z]]
      [→ φ ∀ z φ]
      [infer ax1 [→ φ ∀ z φ] [¬ ∀ x = x z]]
      [!bv!and z [¬ ∀ x = x z] [= x w] φ]
      [imgen>i z [¬ ∀ x = x z] [→ ∧ = x w φ ∀ z ∧ = x w φ]]
      [ded df-Bvp z [∧ = x w φ] [¬ ∀ x = x z]]
      [!bv!exd z x [¬ ∀ x = x z] [∧ = x w φ]] ; If x and z are distinct, --
      [casei [∀ x = x z] [Bvp z [∃ x ∧ = x w φ]]]
      [ax17-bv z [= w y]]
      [Bvp z [∧ = w y ∃ x ∧ = x w φ]]
      [Bvp z [∃ w ∧ = w y ∃ x ∧ = x w φ]]
      [df-sb y x φ w]
      [!bv!def z [sb y x φ] [∃ w ∧ = w y ∃ x ∧ = x w φ]]
      ))

  ;; Equality theorem (wffs) (was: bi.bldsb)
  (th "eqti" (set "y" var "x" wff ![φ ψ])
    (hypo [↔ φ ψ])
    (ass [↔ sb y x φ sb y x ψ])
    (loc var "z")
    (proof
      [infer !an!eqt> φ ψ [= x z]]
      [!ex!eqti x [∧ = x z φ] [∧ = x z ψ]]
      [infer !an!eqt> [∃ x ∧ = x z φ] [∃ x ∧ = x z ψ] [= z y]]
      [!ex!eqti z [∧ = z y ∃ x ∧ = x z φ] [∧ = z y ∃ x ∧ = x z ψ]]
      [df-sb y x φ z]
      [df-sb y x ψ z]
      [!bi!<tr [∃ z ∧ = z y ∃ x ∧ = x z φ] [∃ z ∧ = z y ∃ x ∧ = x z ψ]
               [sb y x φ]                  [sb y x ψ]]))

  ;; Composition (strong) sbco2
  ;; In this version, y need not be distinct from z or x
  (th "comp" (set "z" var ![y x] wff "φ")
    (hypo [Bvp y φ])
    (ass [↔ sb z y sb y x φ sb z x φ])
    (loc var "w")
    (proof
      [ax17-bv w [sb y x φ]]
      [ax17-bv w φ]
      [bv1 w x φ y]
      [comp-w z w y [sb y x φ]]
      [comp-w w y w [sb w x φ]]
      [id w [sb w x φ]]
      [bitr [sb w y sb y w sb w x φ] [sb w w sb w x φ] [sb w x φ]]
      [comp-w y w x φ]
      [eqti w y [sb y w sb w x φ] [sb y x φ]]
      [b2i bitr [sb w y sb y x φ] [sb w y sb y w sb w x φ] [sb w x φ]]
      [eqti z w [sb w y sb y x φ] [sb w x φ]]
      [comp-w z w x φ]
      [bitr [sb z w sb w y sb y x φ] [sb z w sb w x φ] [sb z x φ]]
      [b2i bitr [sb z y sb y x φ] [sb z w sb w y sb y x φ] [sb z x φ]]
      ))

  ;; Formula building: Negation
  ; sb-negw
  (th "neg-w" (set "a" var "x" wff "φ")
    (dist (!a !x))
    (ass [↔ sb a x ¬ φ ¬ sb a x φ])
    (proof
      [df-dvex a x φ]
      [infer neqt [sb a x φ] [∃ x ∧ = x a φ]]
      [↔ ∀ x ¬ [∧ = x a φ] ¬ ∃ x [∧ = x a φ]]
      [b2i bitr [∀ x ¬ ∧ = x a φ] [¬ ∃ x ∧ = x a φ] [¬ sb a x φ]]
      [↔ [→ = x a ¬ φ] [¬ ∧ = x a φ]]
      [!al!eqti x [→ = x a ¬ φ] [¬ ∧ = x a φ]]
      [df-dval a x [¬ φ]]
      [2bitr [sb a x ¬ φ] [∀ x → = x a ¬ φ] [∀ x ¬ ∧ = x a φ] [¬ sb a x φ]]))

  (th "neg" (set "a" var "x" wff "φ")
    (ass [↔ sb a x ¬ φ ¬ sb a x φ])
    (loc var "y")
    (proof
      [distv comp-w a y x [¬ φ]]
      [distv comp-w a y x φ]
      [infer neqt [sb a y sb y x φ] [sb a x φ]]
      [neg-w y x φ]
      [eqti a y [sb y x ¬ φ] [¬ sb y x φ]]
      [neg-w a y [sb y x φ]]
      [b2i bitr [sb a x ¬ φ] [sb a y sb y x ¬ φ] [sb a y ¬ sb y x φ]]
      [2bitr [sb a x ¬ φ] [sb a y ¬ sb y x φ] [¬ sb a y sb y x φ] [¬ sb a x φ]]))

  ;; Formula building: Disjunction
  ; sb-orw
  (th "or-w" (set "a" var "x" wff ![φ ψ])
    (dist (!a !x))
    (ass [↔ sb a x ∨ φ ψ ∨ sb a x φ sb a x ψ])
    (proof
      [df-dvex a x [∨ φ ψ]]
      [!an!di-or< [= x a] φ ψ]
      [!ex!eqti x [∧ = x a ∨ φ ψ] [∨ ∧ = x a φ ∧ = x a ψ]]
      [!ex!di-or x [∧ = x a φ] [∧ = x a ψ]]
      [2bitr [sb a x ∨ φ ψ] [∃ x ∧ = x a ∨ φ ψ] [∃ x ∨ ∧ = x a φ ∧ = x a ψ] [∨ ∃ x ∧ = x a φ ∃ x ∧ = x a ψ]]
      [df-dvex a x φ]
      [df-dvex a x ψ]
      [!or!eqti [sb a x φ] [∃ x ∧ = x a φ]
                 [sb a x ψ] [∃ x ∧ = x a ψ]]
      [b2i bitr [sb a x ∨ φ ψ] [∨ ∃ x ∧ = x a φ ∃ x ∧ = x a ψ] [∨ sb a x φ sb a x ψ]]))

  (th "or" (set "a" var "x" wff ![φ ψ])
    (ass [↔ sb a x ∨ φ ψ ∨ sb a x φ sb a x ψ])
    (loc var "y")
    (proof
      [or-w y x φ ψ]
      [eqti a y [sb y x ∨ φ ψ] [∨ sb y x φ sb y x ψ]]
      [or-w a y [sb y x φ] [sb y x ψ]]
      [bitr [sb a y sb y x ∨ φ ψ] [sb a y ∨ sb y x φ sb y x ψ] [∨ sb a y sb y x φ sb a y sb y x ψ]]
      [distv comp-w a y x [∨ φ ψ]]
      [distv comp-w a y x φ]
      [distv comp-w a y x ψ]
      [!/equ!or!eqti [sb a y sb y x φ] [sb a x φ]
                     [sb a y sb y x ψ] [sb a x ψ]]
      [!bi!>tr [sb a y sb y x ∨ φ ψ] [∨ sb a y sb y x φ sb a y sb y x ψ]
               [sb a x ∨ φ ψ]        [∨ sb a x φ sb a x ψ]]))

  ;; Formula building: Conjunction, (Bi-)Implication

  (th "an" (set "a" var "x" wff ![φ ψ])
    (ass [↔ sb a x ∧ φ ψ ∧ sb a x φ sb a x ψ])
    (proof
      [↔ ∧ φ ψ ¬ ∨ ¬ φ ¬ ψ]
      [eqti a x [∧ φ ψ] [¬ ∨ ¬ φ ¬ ψ]]
      [neg a x [∨ ¬ φ ¬ ψ]]
      [or a x [¬ φ] [¬ ψ]]
      [neg a x φ]
      [neg a x ψ]
      [!/equ!or!eqti [sb a x ¬ φ] [¬ sb a x φ]
                     [sb a x ¬ ψ] [¬ sb a x ψ]]
      [bitr [sb a x ∨ ¬ φ ¬ ψ] [∨ sb a x ¬ φ sb a x ¬ ψ] [∨ ¬ sb a x φ ¬ sb a x ψ]]
      [infer neqt [sb a x ∨ ¬ φ ¬ ψ] [∨ ¬ sb a x φ ¬ sb a x ψ]]
      [2bitr [sb a x ∧ φ ψ] [sb a x ¬ ∨ ¬ φ ¬ ψ] [¬ sb a x ∨ ¬ φ ¬ ψ] [¬ ∨ ¬ sb a x φ ¬ sb a x ψ]]
      [!/equ!an!df.or [sb a x φ] [sb a x ψ]]
      [b2i bitr [sb a x ∧ φ ψ] [¬ ∨ ¬ sb a x φ ¬ sb a x ψ] [∧ sb a x φ sb a x ψ]]))

  (th "im" (set "a" var "x" wff ![φ ψ])
    (ass [↔ sb a x → φ ψ → sb a x φ sb a x ψ])
    (proof
      [!/equ!im!df.or φ ψ]
      [eqti a x [→ φ ψ] [∨ ¬ φ ψ]]
      [or a x [¬ φ] ψ]
      [neg a x φ]
      [infer !/equ!or!eqt< [sb a x ¬ φ] [¬ sb a x φ] [sb a x ψ]]
      [2bitr [sb a x → φ ψ] [sb a x ∨ ¬ φ ψ] [∨ sb a x ¬ φ sb a x ψ] [∨ ¬ sb a x φ sb a x ψ]]
      [!/equ!im!df.or [sb a x φ] [sb a x ψ]]
      [b2i bitr [sb a x → φ ψ] [∨ ¬ sb a x φ sb a x ψ] [→ sb a x φ sb a x ψ]]))

  (th "bi" (set "a" var "x" wff ![φ ψ])
    (ass [↔ sb a x ↔ φ ψ ↔ sb a x φ sb a x ψ])
    (proof
      [dfbi φ ψ]
      [eqti a x [↔ φ ψ] [∧ → φ ψ → ψ φ]]
      [an a x [→ φ ψ] [→ ψ φ]]
      [im a x φ ψ]
      [im a x ψ φ]
      [!/equ!an!eqti [sb a x → φ ψ] [→ sb a x φ sb a x ψ]
                     [sb a x → ψ φ] [→ sb a x ψ sb a x φ]]
      [2bitr [sb a x ↔ φ ψ] [sb a x ∧ → φ ψ → ψ φ] [∧ sb a x → φ ψ sb a x → ψ φ] [∧ → sb a x φ sb a x ψ → sb a x ψ sb a x φ]]
      [dfbi [sb a x φ] [sb a x ψ]]
      [b2i bitr [sb a x ↔ φ ψ] [∧ → sb a x φ sb a x ψ → sb a x ψ sb a x φ] [↔ sb a x φ sb a x ψ]]))

  ;; Substitution with a variable not in φ
  ; sb-bvw
  (th "dist-w" (set "a" var "x" wff "φ")
    (dist (!a !x))
    (hypo [Bvp x φ])
    (ass [↔ sb a x φ φ])
    (proof
      [df-dvex a x φ]
      [!ex!di-an> x [= x a] φ]
      [bitr [sb a x φ] [∃ x ∧ = x a φ] [∧ ∃ x = x a φ]]
      [ax9ex x a]
      [infer !/equ!an!true< [∃ x = x a] φ]
      [b2i bitr [sb a x φ] [∧ ∃ x = x a φ] φ]))

  ; sb-bv
  (th "dist" (set "a" var "x" wff "φ")
    (hypo [Bvp x φ])
    (ass [↔ sb a x φ φ])
    (loc var "z")
    (proof
      [dist-w z x φ]
      [eqti a z [sb z x φ] φ]
      [distv dist-w a z φ]
      [bitr [sb a z sb z x φ] [sb a z φ] φ]
      [distv comp-w a z x φ]
      [b2i bitr [sb a x φ] [sb a z sb z x φ] φ]))
  (th-pattern !dist)

  ;; Explicit change of variables
  ; chvar-∀-1w
  (th "cbv-al-1w" (var ![y x] wff "φ")
    (dist (!y !x))
    (ass [↔ ∀ y sb y x φ ∀ x sb x y φ])
    (proof
      [df-dval y x φ]
      [!al!eqti y [sb y x φ] [∀ x → = x y φ]]
      [!al!com y x [→ = x y φ]]
      [↔ = x y = y x]
      [infer !/equ!im!eqt< [= x y] [= y x] φ]
      [!al!eqti y [→ = x y φ] [→ = y x φ]]
      [df-dval x y φ]
      [b2i bitr [∀ y → = x y φ] [∀ y → = y x φ] [sb x y φ]]
      [!al!eqti x [∀ y → = x y φ] [sb x y φ]]
      [2bitr [∀ y sb y x φ] [∀ y ∀ x → = x y φ] [∀ x ∀ y → = x y φ] [∀ x sb x y φ]]))

  ; chvar-∀-w
  (th "cbv-al-w" (var ![y x] wff "φ")
    (dist (!y !x))
    (hypo [Bvp y φ])
    (ass [↔ ∀ y sb y x φ ∀ x φ])
    (proof
      [cbv-al-1w y x φ]
      [dist x y φ]
      [!al!eqti x [sb x y φ] φ]
      [bitr [∀ y sb y x φ] [∀ x sb x y φ] [∀ x φ]]))

  ; chvar-∀
  (th "cbv-al" (var ![y x] wff "φ")
    (hypo [Bvp y φ])
    (ass [↔ ∀ y sb y x φ ∀ x φ])
    (loc var "z")
    (proof
      [ax17-bv z φ]
      [bv1 z x φ y]
      [cbv-al-w z x φ]
      [cbv-al-w y z [sb z x φ]]
      [bitr [∀ y sb y z sb z x φ] [∀ z sb z x φ] [∀ x φ]]
      [comp-w y z x φ]
      [!al!eqti y [sb y z sb z x φ] [sb y x φ]]
      [b2i bitr [∀ y sb y x φ] [∀ y sb y z sb z x φ] [∀ x φ]]))
  (th-pattern !cbv-al)

  ; chvar-∃
  (th "cbv-ex" (var ![y x] wff "φ")
    (hypo [Bvp y φ])
    (ass [↔ ∃ y sb y x φ ∃ x φ])
    (proof
      [Bvp y ¬ φ]
      [neg y x φ]
      [!al!eqti y [sb y x ¬ φ] [¬ sb y x φ]]
      [cbv-al y x [¬ φ]]
      [b2i bitr [∀ y ¬ sb y x φ] [∀ y sb y x ¬ φ] [∀ x ¬ φ]]
      [infer neqt [∀ y ¬ sb y x φ] [∀ x ¬ φ]]
      [df-ex y [sb y x φ]]
      [df-ex x φ]
      [!/equ!bi!<tr [¬ ∀ y ¬ sb y x φ] [¬ ∀ x ¬ φ]
                    [∃ y sb y x φ]     [∃ x φ]]))
  (th-pattern !cbv-ex)

  ;; Specialization, existential introduction

  (th "spec-w" (set "a" var "x" wff "φ")
    (dist (!x !a))
    (ass [→ ∀ x φ sb a x φ])
    (proof
      [ax1 φ [= x a]]
      [!al!imti x φ [→ = x a φ]]
      [df-dval a x φ]
      [b2i syl [∀ x φ] [∀ x → = x a φ] [sb a x φ]]))

  (th "spec" (set "a" var "x" wff "φ")
    (ass [→ ∀ x φ sb a x φ])
    (loc var "z")
    (proof
      [spec-w z x φ]
      [distv imgen>i z [∀ x φ] [sb z x φ]]
      [spec-w a z [sb z x φ]]
      [distv comp-w a z x φ]
      [b2i 3syl [∀ x φ] [∀ z sb z x φ] [sb a z sb z x φ] [sb a x φ]]))
  (th-pattern !spec)

  ; sb-spec-ex
  (th "spec-ex" (set "a" var "x" wff "φ")
    (ass [→ sb a x φ ∃ x φ])
    (proof
      [spec a x [¬ φ]]
      [neg a x φ]
      [b2i syl [∀ x ¬ φ] [sb a x ¬ φ] [¬ sb a x φ]]
      [infer con> [∀ x ¬ φ] [sb a x φ]]
      [df-ex x φ]
      [b2i syl [sb a x φ] [¬ ∀ x ¬ φ] [∃ x φ]]))
  (th-pattern !spec-ex)

  ;; Equality & order theorems (wffs)

  ; im.bldsbt
  (th "imt" (set "y" var "x" wff ![φ ψ])
    (ass [→ ∀ x → φ ψ → sb y x φ sb y x ψ])
    (proof
      [spec y x [→ φ ψ]]
      [im y x φ ψ]
      [b2i syl [∀ x → φ ψ] [sb y x → φ ψ] [→ sb y x φ sb y x ψ]]))

  ; im.bldsb
  (th "imti" (set "y" var "x" wff ![φ ψ])
    (hypo [→ φ ψ])
    (ass [→ sb y x φ sb y x ψ])
    (proof
      [infer imt y x φ ψ]))

  ; bi.bldsbt
  (th "eqt" (set "y" var "x" wff ![φ ψ])
    (ass [→ ∀ x ↔ φ ψ ↔ sb y x φ sb y x ψ])
    (proof
      [spec y x [↔ φ ψ]]
      [bi y x φ ψ]
      [b2i syl [∀ x ↔ φ ψ] [sb y x ↔ φ ψ] [↔ sb y x φ sb y x ψ]]))

  ; bi.bldsbd
  (th "eqtd" (set "y" var "x" wff ![φ ψ χ])
    (hypo [Bvp x φ] [→ φ ↔ ψ χ])
    (ass [→ φ ↔ sb y x ψ sb y x χ])
    (proof
      [imgen>i x φ [↔ ψ χ]]
      [eqt y x ψ χ]
      [syl φ [∀ x ↔ ψ χ] [↔ sb y x ψ sb y x χ]]))

  ;; Formula building: biconditionals

  ; sb-bi>
  (th "bi>" (set "y" var "x" wff ![φ ψ χ])
    (hypo [↔ sb y x φ ψ])
    (ass [↔ sb y x ↔ χ φ ↔ sb y x χ ψ])
    (proof
      [bi y x χ φ]
      [infer !/equ!bi!eqt> [sb y x φ] ψ [sb y x χ]]
      [bitr [sb y x ↔ χ φ] [↔ sb y x χ sb y x φ] [↔ sb y x χ ψ]]))

  ; sb-bi<
  (th "bi<" (set "y" var "x" wff ![φ ψ χ])
    (hypo [↔ sb y x φ ψ])
    (ass [↔ sb y x ↔ φ χ ↔ ψ sb y x χ])
    (proof
      [bi y x φ χ]
      [infer !/equ!bi!eqt< [sb y x φ] [ψ] [sb y x χ]]
      [bitr [sb y x ↔ φ χ] [↔ sb y x φ sb y x χ] [↔ ψ sb y x χ]]))

  ;; Formula building: For all, exists

  ; sbal-w
  (th "al-w" (set "a" var ![x z] wff "φ")
    (dist (!z !a) (!z !x) (!x !a))
    (ass [↔ sb a x ∀ z φ ∀ z sb a x φ])
    (proof
      [ax17-bv z [= x a]]
      [imgen> z [= x a] φ]
      [!al!eqti x [∀ z → = x a φ] [→ = x a ∀ z φ]]
      [!al!com z x [→ = x a φ]]
      [bitr [∀ z ∀ x → = x a φ] [∀ x ∀ z → = x a φ] [∀ x → = x a ∀ z φ]]
      [df-dval a x [∀ z φ]]
      [df-dval a x φ]
      [!al!eqti z [sb a x φ] [∀ x → = x a φ]]
      [!/equ!bi!<tr [∀ z ∀ x → = x a φ] [∀ x → = x a ∀ z φ]
                    [∀ z sb a x φ]      [sb a x ∀ z φ]]
      [!/equ!bi!comi [∀ z sb a x φ] [sb a x ∀ z φ]]))

  ; sbal
  (th "al" (set "a" var ![x z] wff "φ")
    (dist (!z !a) (!z !x))
    (ass [↔ sb a x ∀ z φ ∀ z sb a x φ])
    (loc var "w")
    (proof
      [al-w w x z φ]
      [eqti a w [sb w x ∀ z φ] [∀ z sb w x φ]]
      [al-w a w z [sb w x φ]]
      [bitr [sb a w sb w x ∀ z φ] [sb a w ∀ z sb w x φ] [∀ z sb a w sb w x φ]]
      [distv comp-w a w x φ]
      [!/equ!al!eqti z [sb a w sb w x φ] [sb a x φ]]
      [distv comp-w a w x [∀ z φ]]
      [!/equ!bi!>tr [sb a w sb w x ∀ z φ] [∀ z sb a w sb w x φ]
                    [sb a x ∀ z φ]        [∀ z sb a x φ]]))

  ; sbex
  (th "ex" (set "a" var ![x z] wff "φ")
    (dist (!z !a) (!z !x))
    (ass [↔ sb a x ∃ z φ ∃ z sb a x φ])
    (proof
      [neg a x [∀ z ¬ φ]]
      [al a x z [¬ φ]]
      [neg a x φ]
      [!/equ!al!eqti z [sb a x ¬ φ] [¬ sb a x φ]]
      [bitr [sb a x ∀ z ¬ φ] [∀ z sb a x ¬ φ] [∀ z ¬ sb a x φ]]
      [infer neqt [sb a x ∀ z ¬ φ] [∀ z ¬ sb a x φ]]
      [bitr [sb a x ¬ ∀ z ¬ φ] [¬ sb a x ∀ z ¬ φ] [¬ ∀ z ¬ sb a x φ]]
      [df-ex z φ]
      [eqti a x [∃ z φ] [¬ ∀ z ¬ φ]]
      [df-ex z [sb a x φ]]
      [!/equ!bi!<tr [sb a x ¬ ∀ z ¬ φ] [¬ ∀ z ¬ sb a x φ]
                    [sb a x ∃ z φ]     [∃ z sb a x φ]])) 

  ;; Identity law
  ; eq-sb>b
  (th "id2" (set "a" var "x" wff "φ")
    (ass [→ = x a ↔ φ sb a x φ])
    (proof
      [eqt-term x a x φ]
      [id x φ]
      [infer ax1 [↔ sb x x φ φ] [= x a]]
      [!equ!bi!trd> [= x a] [φ] [sb x x φ] [sb a x φ]]))

  ;; Substitution in atomic formulas

  ; sb-eqat<xw
  (th "eqat<xw" (set "y" var "x")
    (dist (!y !x))
    (ass [sb y x = x y])
    (loc var "z")
    (proof
      [= y y]
      [infer eqvini y y z]
      [→ = y z = z y]
      [infer !/equ!an!imt< [= y z] [= z y] [= z y]]
      [!/equ!ex!imti z [∧ = y z = z y] [∧ = z y = z y]]
      [eqvini z y x]
      [→ = z x = x z]
      [infer !/equ!an!imt< [= z x] [= x z] [= x y]]
      [ded !equ!ex!imti x [∧ = z x = x y] [∧ = x z = x y] [= z y]]
      [infer !/equ!an!imt> [= z y] [∃ x ∧ = x z = x y] [= z y]]
      [syl [∧ = y z = z y] [∧ = z y = z y] [∧ = z y ∃ x ∧ = x z = x y]]
      [infer !/equ!ex!imti z [∧ = y z = z y] [∧ = z y ∃ x ∧ = x z = x y]]
      [infer df-sb y x [= x y] z]))

  ; sb-eqat<x
  (th "eqat<x" (var ![y x])
    (ass [sb y x = x y])
    (loc var "z")
    (proof
      [= y y]
      [infer eqvini-sv y y z]
      [→ = y z = z y]
      [infer !/equ!an!imt< [= y z] [= z y] [= z y]]
      [infer !/equ!ex!imti z [∧ = y z = z y] [∧ = z y = z y]]
      [eqvini-sv z y x]
      [→ = z x = x z]
      [infer !/equ!an!imt< [= z x] [= x z] [= x y]]
      [ded !/equ!ex!imti x [∧ = z x = x y] [∧ = x z = x y] [= z y]]
      [infer !/equ!an!imt> [= z y] [∃ x ∧ = x z = x y] [= z y]]
      [infer !/equ!ex!imti z [∧ = z y = z y] [∧ = z y ∃ x ∧ = x z = x y]]
      [infer df-sb y x [= x y] z])) 

  ;; Convert implicit to explicit substitution

  ; sb-xplw
  (th "xplw" (set "a" var "x" wff ![φ ψ])
    (dist (!a !x))
    (hypo [Bvp x ψ] [→ = x a ↔ φ ψ])
    (ass [↔ sb a x φ ψ])
    (proof
      [dist a x ψ]
      [eqat<xw a x]
      [infer imti a x [= x a] [↔ φ ψ]]
      [infer bi> a x ψ ψ φ]))

  ; sb-xpl
  (th "xpl" (var ![y x] wff ![φ ψ])
    (hypo [Bvp x ψ] [→ = x y ↔ φ ψ])
    (ass [↔ sb y x φ ψ])
    (proof
      [dist y x ψ]
      [eqat<x y x]
      [infer imti y x [= x y] [↔ φ ψ]]
      [infer bi> y x ψ ψ φ]))

  ;; Substitution in a theorem
  ; sb-theor
  (th "theor" (set "a" var "x" wff "φ")
    (hypo [φ])
    (ass [sb a x φ])
    (proof
      [infer spec a x φ]))
 
  ;; Substitution in atomic formulas

  ; sb-eqat<w
  (th "eqat<w" (var "x" set ![y z])
    (dist (!x !z) (!x !y))
    (ass [↔ sb z x = x y = z y])
    (proof
      [ax17-bv x [= z y]]
      [→ = x z ↔ = x y = z y]
      [xplw z x [= x y] [= z y]]))

  (th "eqat<" (var "x" set ![y z])
    (dist (!x !y))
    (ass [↔ sb z x = x y = z y])
    (loc var "w")
    (proof
      [ax17-bv w [= x y]]
      [eqat<w x y w]
      [eqti z w [sb w x = x y] [= w y]]
      [eqat<w w y z]
      [bitr [sb z w sb w x = x y] [sb z w = w y] [= z y]]
      [comp z w x [= x y]]
      [b2i bitr [sb z x = x y] [sb z w sb w x = x y] [= z y]]))

  (th "eqat>" (set "x" var "y" set "z")
    (dist (!x !y))
    (ass [↔ sb z y = x y = x z])
    (proof
      [eqat< y x z]
      [↔ = x y = y x]
      [eqti z y [= x y] [= y x]]
      [↔ = x z = z x]
      [!/equ!bi!<tr [sb z y = y x] [= z x]
                    [sb z y = x y] [= x z]])) ) ; end submodule "sb"
#|
;;; Distinct variables and quantifiers

(th "exan>vv" (sv ("x" "y") pr ("φ" "ψ"))
    (dist (!x !ψ) (!y !ψ))
    (ass [↔ ∃ x ∃ y ∧ φ ψ ∧ ∃ x ∃ y φ ψ])
    (proof
      [(distv !exan>) [y] [φ] [ψ]]
      [eqt-∃-i [x] [∃ y ∧ φ ψ] [∧ ∃ y φ ψ]]
      [(distv !exan>) [x] [∃ y φ] [ψ]]
      [bitr [∃ x ∃ y ∧ φ ψ] [∃ x ∧ ∃ y φ ψ] [∧ ∃ x ∃ y φ ψ]]))

(th "exan>vvv" (sv ("x" "y" "z") pr ("φ" "ψ"))
    (dist (!x !ψ) (!y !ψ) (!z !ψ))
    (ass [↔ ∃ x ∃ y ∃ z ∧ φ ψ ∧ ∃ x ∃ y ∃ z φ ψ])
    (proof
      [exan>vv [y] [z] [φ] [ψ]]
      [eqt-∃-i [x] [∃ y ∃ z ∧ φ ψ] [∧ ∃ y ∃ z φ ψ]]
      [(distv !exan>) [x] [∃ y ∃ z φ] [ψ]]
      [bitr [∃ x ∃ y ∃ z ∧ φ ψ] [∃ x ∧ ∃ y ∃ z φ ψ] [∧ ∃ x ∃ y ∃ z φ ψ]]))

(th "exdistr" (sv ("x" "y") pr ("φ" "ψ"))
    (dist (!y !φ))
    (ass [↔ ∃ x ∃ y ∧ φ ψ ∃ x ∧ φ ∃ y ψ])
    (proof
      [(distv !exan<) [y] [φ] [ψ]]
      [eqt-∃-i [x] [∃ y ∧ φ ψ] [∧ φ ∃ y ψ]]))

(th "exan<vv" (sv ("x" "y") pr ("φ" "ψ"))
    (dist (!x !φ) (!y !φ))
    (ass [↔ ∃ x ∃ y ∧ φ ψ ∧ φ ∃ x ∃ y ψ])
    (proof
      [exdistr x y φ ψ]
      [(distv !exan<) [x] [φ] [∃ y ψ]]
      [bitr [∃ x ∃ y ∧ φ ψ] [∃ x ∧ φ ∃ y ψ] [∧ φ ∃ x ∃ y ψ]]))

(th "exdistr2" (sv ("x" "y" "z") pr ("φ" "ψ"))
    (dist (!y !φ) (!z !φ))
    (ass [↔ ∃ x ∃ y ∃ z ∧ φ ψ ∃ x ∧ φ ∃ y ∃ z ψ])
    (proof
      [exan<vv y z φ ψ]
      [eqt-∃-i [x] [∃ y ∃ z ∧ φ ψ] [∧ φ ∃ y ∃ z ψ]]))


(th "eeanv" (sv ("x" "y") pr ("φ" "ψ"))
    (dist (!y !φ) (!x !ψ))
    (ass [↔ ∃ x ∃ y ∧ φ ψ ∧ ∃ x φ ∃ y ψ])
    (proof
      [exdistr x y φ ψ]
      [ax17-bv x ψ]
      [bv-∃ y ψ x]
      [exan> x φ [∃ y ψ]]
      [bitr [∃ x ∃ y ∧ φ ψ] [∃ x ∧ φ ∃ y ψ] [∧ ∃ x φ ∃ y ψ]]))

(th "ee4anv" (sv ("x" "y" "z" "w") pr ("φ" "ψ"))
    (dist (!z !φ) (!w !φ) (!x !ψ) (!y !ψ) (!y !z) (!x !w))
    (ass [↔ ∃ x ∃ y ∃ z ∃ w ∧ φ ψ ∧ ∃ x ∃ y φ ∃ z ∃ w ψ])
    (proof
      [excom y z [∃ w ∧ φ ψ]]
      [eqt-∃-i [x] [∃ y ∃ z ∃ w ∧ φ ψ] [∃ z ∃ y ∃ w ∧ φ ψ]]
      [eeanv y w φ ψ]
      [eqt-∃-i [z] [∃ y ∃ w ∧ φ ψ] [∧ ∃ y φ ∃ w ψ]]
      [eqt-∃-i [x] [∃ z ∃ y ∃ w ∧ φ ψ] [∃ z ∧ ∃ y φ ∃ w ψ]]
      [eeanv [x] [z] [∃ y φ] [∃ w ψ]]
      [2bitr [∃ x ∃ y ∃ z ∃ w ∧ φ ψ] [∃ x ∃ z ∃ y ∃ w ∧ φ ψ]
             [∃ x ∃ z ∧ ∃ y φ ∃ w ψ] [∧ ∃ x ∃ y φ ∃ z ∃ w ψ]]))
|#
(th "dveeq1" (var ![x y z])
    (dist (!x !z))
    (ass [→ ¬ ∀ x = x y → = y z ∀ x = y z])
    (loc var "w")
    (proof
      [ax12 y z x]
      [com12i [¬ ∀ x = x y] [¬ ∀ x = x z] [→ = y z ∀ x = y z]]
      [ax16 x z [= y z]]
      [ded ax1 [→ = y z ∀ x = y z] [¬ ∀ x = x y] [∀ x = x z]]
      [casei [∀ x = x z] [→ ¬ ∀ x = x y → = y z ∀ x = y z]]
      ))

) ; End module "equ"
