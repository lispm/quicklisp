;;; -*- mode: Lisp; Syntax: Common-Lisp; -*-
;;;
;;; Copyright (c) 2009 by the authors.
;;;
;;; See LICENCE for details.

(in-package :hu.dwim.presentation)

;;;;;;
;;; structure/chart

(def (component e) structure/chart (standard/chart)
  ())

(def render-xhtml structure/chart
  (not-yet-implemented))
