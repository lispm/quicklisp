;;;  compiler.lisp --- test suite for the BINGE compiler

;;;  Copyright (C) 2004 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: BINGE (Bovine INterface GEnerator)
;;;  $Id: F-05720BB38093D99FD9DAF7DFEDCE7B6D.lisp,v 1.15 2004/04/07 13:29:21 wcp Exp $

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2, or (at
;;; your option) any later version.
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;; You should have received a copy of the GNU General Public License
;;; along with this program; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;; Boston, MA 02111-1307, USA.

;;;  Commentary:

#+cmu (ext:file-comment "$Id: F-05720BB38093D99FD9DAF7DFEDCE7B6D.lisp,v 1.15 2004/04/07 13:29:21 wcp Exp $")

(in-package :binge-tests)

(defun cfs (string &optional backend)
  "Compile From String.  Parse string, create a namespace and
output a pseudo FFI which is read back.  Return a sexp with the
complete pseudo FFI.  This is used by the regression tests only."
  (multiple-value-bind (sexp rubbish)
      (read-from-string
       (concatenate 'string "("
		    (make-ffi nil (populate-namespace (make-instance 'c-namespace) (extract string))
			      :backend (or backend
					   (make-instance 'binge-backend:test-backend)))
		    ")"))
    (declare (ignore rubbish))
    sexp))


(deftest compiler-output-order.1
    (cfs "struct s; struct s a, *pa; struct s { int x, y;} b, c[10];")
  ((:struct "s" (((:int) "x") ((:int) "y")))
   (:variable "a" (:struct "s"))
   (:variable "b" (:struct "s"))
   (:variable "c" (:array (:struct "s") (10)))
   (:variable "pa" (:pointer (:struct "s")))))

(deftest compiler-output-order.2
    (cfs "struct s; typedef struct s st; st a, *pa; struct s { int x, y;} b, c[10];")
  ((:struct "s" (((:int) "x") ((:int) "y")))
   (:typedef "st" (:struct "s"))
   (:variable "a" (:user-type "st"))
   (:variable "b" (:struct "s"))
   (:variable "c" (:array (:struct "s") (10)))
   (:variable "pa" (:pointer (:user-type "st")))))

(deftest compiler-output-order.3
    (cfs "struct s; int a;")
  ((:struct "s" nil)
   (:variable "a" (:int))))

(deftest compiler-output-order.4
    (cfs "typedef int t; int a;")
  ((:typedef "t" (:int))
   (:variable "a" (:int))))

(deftest compiler-output-order.5
    (cfs "struct s { int a, *b; struct s *ps; } x, y;")
  ((:struct "s"
	    (((:int) "a")
	     ((:pointer (:int)) "b")
	     ((:pointer (:struct "s")) "ps")))
   (:variable "x" (:struct "s"))
   (:variable "y" (:struct "s"))))

(deftest compiler-output-order.6
    (cfs "typedef struct s st; st *y; struct s *x;")
  ((:struct "s" nil) (:typedef "st" (:struct "s"))
   (:variable "x" (:pointer (:struct "s")))
   (:variable "y" (:pointer (:user-type "st")))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest compiler-inner-declarations.1
    (binge-tests:cfs "struct s1 { int a; struct s2 { int b; } j; } k;")
  ((:struct "s2" (((:int) "b")))
   (:struct "s1" (((:int) "a") ((:struct "s2") "j")))
   (:variable "k" (:struct "s1"))))

(deftest compiler-inner-declarations.2
    (cfs "struct s1 { int a; struct s2 { int b; struct s3 { int c; } ***x, **y, z[]; } *i, j; } k, m[10][];")
  ((:struct "s3" (((:int) "c")))
   (:struct "s2"
	    (((:int) "b")
	     ((:pointer (:pointer (:pointer (:struct "s3")))) "x")
	     ((:pointer (:pointer (:struct "s3"))) "y")
	     ((:array (:struct "s3") (nil)) "z")))
   (:struct "s1"
	    (((:int) "a")
	     ((:pointer (:struct "s2")) "i")
	     ((:struct "s2") "j")))
   (:variable "k" (:struct "s1"))
   (:variable "m" (:array (:struct "s1") (10 nil)))))

