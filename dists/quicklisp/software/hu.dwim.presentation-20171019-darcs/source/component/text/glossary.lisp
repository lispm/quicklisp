;;; -*- mode: Lisp; Syntax: Common-Lisp; -*-
;;;
;;; Copyright (c) 2009 by the authors.
;;;
;;; See LICENCE for details.

(in-package :hu.dwim.presentation)

;;;;;;
;;; glossary/inspector

(def (component e) glossary/inspector (t/inspector)
  ())

(def render-xhtml glossary/inspector
  (not-yet-implemented))
