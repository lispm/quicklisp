;; This software is Copyright (c) cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(in-package :cl-pslib-barcode)

(defun char@ (str pos)
  (if (< pos (length str))
      (string (elt str pos))
      nil))

(defun is-keyword-p (keyword str)
  (if (< (length str) (length keyword))
      nil
	(string= keyword (subseq str 0 (length keyword)))))
