(asdf:oos 'asdf:load-op 'reversi)

(let ((path (make-pathname :defaults *load-truename*
			   :name "fli-templates")))
  (compile-file path)
  (load (compile-file-pathname path)))

(deliver 'reversi::clim-reversi "cl-reversi" 5 :multiprocessing t)
