;;; -*- Mode: Lisp -*-

;;;; array-enumerations.lisp --
;;;;
;;;; See file COPYING for copyright and licensing information.

(in-package "CL.EXTENSIONS.DACF.ENUMERATIONS")

;;;;===========================================================================
;;;; Array Table Enumeration.

(defclass array-table-enumeration (bounded-enumeration
				   bi-directional-enumeration)
  ()
  (:documentation "The Array Table Enumeration Class."))


(defgeneric array-table-enumeration-p (x)
  (:method ((x t)) nil)
  (:method ((x array-table-enumeration)) t))


(defmethod enumerate ((A array) &key (start 0) end &allow-other-keys)
  "Calling ENUMERATE on an ARRAY returns an ARRAY-ENUMERATION instance.

START must be either an integer between 0 and (array-total-size a), or
a LIST of indices between 0 and the appropriate limit on the
array-dimension. end must be an integer between 0 and 
(array-total-size A), or a LIST of indices between 0 and the
appropriate limit on the array-dimension, or NIL. If END is NIL then
it is set to (array-total-size A).

The enumeration of a multidimensional array follows the row-major
order of Common Lisp Arrays.

Examples:

;;; The following example uses ENUM:FOREACH.

cl-prompt> (foreach (e #3A(((1 2 3) (4 5 6)) ((7 8 9) (10 11 12)))
                    :start (list 1 0 1))
               (print e))
8 
9 
10 
11 
12 
NIL
"
  (make-instance 'array-table-enumeration :object a :start start :end end))


(defmethod initialize-instance :after ((x array-table-enumeration)
				       &key (start 0)
				       end)
  (unless (integerp start)
    (if (consp start)
	(setf (enumeration-start x)
	      (apply #'array-row-major-index
		     (enumeration-object x)
		     start))
	(setf (enumeration-start x) 0)))

  (when (and end (not (integerp end)))
    (if (consp end)
	(setf (enumeration-end x)
	      (apply #'array-row-major-index
		     (enumeration-object x)
		     end))
	(setf (enumeration-end x)
	      (array-total-size (enumeration-object x)))))

  (setf (enumeration-cursor x) (enumeration-start x)))


(defmethod has-more-elements-p ((x array-table-enumeration))
  (< (enumeration-cursor x) (array-total-size (enumeration-object x))))


(defmethod has-next-p ((x array-table-enumeration))
  (< (enumeration-cursor x) (array-total-size (enumeration-object x))))


(defmethod next ((x array-table-enumeration) &optional default)
  (declare (ignore default))
  (prog1 (row-major-aref (enumeration-object x) (enumeration-cursor x))
    (incf (enumeration-cursor x))))


(defmethod has-previous-p ((x array-table-enumeration))
  (plusp (enumeration-cursor x)))


(defmethod previous ((x array-table-enumeration) &optional default)
  (declare (ignore default))
  (decf (enumeration-cursor x))
  (row-major-aref (enumeration-object x) (enumeration-cursor x)))


(defmethod current ((x array-table-enumeration) &optional (errorp t) (default nil))
  (cond ((and errorp (not (has-more-elements-p x)))
         (error 'no-such-element :enumeration x))
        ((and (not errorp) (not (has-more-elements-p x)))
         default)
        (t (row-major-aref (enumeration-object x) (enumeration-cursor x)))))


(defmethod reset ((x array-table-enumeration))
  (setf (enumeration-cursor x) (enumeration-start x)))


;;;; end of file -- array-enumerations.lisp --
