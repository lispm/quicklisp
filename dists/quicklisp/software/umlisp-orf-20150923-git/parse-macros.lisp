;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: umlisp -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:     parse-macros.lisp
;;;; Purpose:  Macros for UMLS file parsing
;;;; Author:   Kevin M. Rosenberg
;;;; Created:  Apr 2000
;;;;
;;;; $Id$
;;;;
;;;; This file, part of UMLisp, is
;;;;    Copyright (c) 2000-2004 by Kevin M. Rosenberg, M.D.
;;;;
;;;; UMLisp users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License.
;;;; *************************************************************************

(in-package #:umlisp-orf)

(defmacro with-umls-file ((line filename) &body body)
"Opens a UMLS and processes each parsed line with (body) argument"
  (let ((ustream (gensym "STRM-"))
        (eof (gensym "EOF-")))
    `(let ((,eof (gensym "EOFSYM-")))
      (with-open-file
          (,ustream (umls-pathname ,filename) :direction :input)
        (do ((,line (read-umls-line ,ustream ,eof)
                    (read-umls-line ,ustream ,eof)))
            ((eq ,line ,eof) t)
          ,@body)))))

(defmacro with-buffered-umls-file ((line filename) &body body)
  "Opens a UMLS and processes each parsed line with (body) argument"
  (let ((ustream (gensym "STRM-"))
        (buffer (gensym "BUF-"))
        (eof (gensym "EOF-")))
    `(let ((,buffer (make-fields-buffer))
           (,eof (gensym "EOFSYM-")))
      (with-open-file
          (,ustream (umls-pathname ,filename) :direction :input)
        (do ((,line (read-buffered-fields ,buffer ,ustream #\| ,eof)
                    (read-buffered-fields ,buffer ,ustream #\| ,eof)))
            ((eq ,line ,eof) t)
          ,@body)))))

(defmacro with-buffered2-umls-file ((line filename) &body body)
  "Opens a UMLS and processes each parsed line with (body) argument"
  (let ((ustream (gensym "STRM-"))
        (buffer (gensym "BUF-"))
        (eof (gensym "EOF-")))
    `(let ((,buffer (make-fields-buffer2))
           (,eof (gensym "EOFSYM-")))
      (with-open-file
          (,ustream (umls-pathname ,filename)
           :direction :input :if-exists :overwrite)
        (do ((,line (read-buffered-fields ,buffer ,ustream #\| ,eof)
                    (read-buffered-fields ,buffer ,ustream #\| ,eof)))
            ((eq ,line ,eof) t)
          ,@body)))))
