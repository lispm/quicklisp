(defun bloop (x)
  (* x x))

(defvar *blooper* nil)

(let ((*blooper* #'bloop))
  (defun test (x)
    (funcall (load-time-value (progn
                                (format t "Fn is ~A~%" *blooper*)
                                *blooper*))
             x)))
