;;;  metafs.asd --- Meta File System

;;;  Copyright (C) 2006, 2007 by Walter C. Pelissero

;;;  Author: Walter C. Pelissero <walter@pelissero.de>
;;;  Project: metafs

#+cmu (ext:file-comment "$Module: metafs.asd, Time-stamp: <2007-08-10 16:20:15 wcp> $")

;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 2.1
;;; of the License, or (at your option) any later version.
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, write to the Free
;;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
;;; 02111-1307 USA

#-(or cmu sbcl)
(warn "This code hasn't been tested on your Lisp system.")

(defpackage :metafs-system
  (:use :common-lisp :asdf #+asdfa :asdfa)
  (:export #:*base-directory*
	   #:*compilation-epoch*))

(in-package :metafs-system)

(defsystem metafs
    :name "METAFS"
    :author "Walter C. Pelissero <walter@pelissero.de>"
    :maintainer "Walter C. Pelissero <walter@pelissero.de>"
    :description "Meta-Data File System"
    :long-description
    "This is a library that allows the manipulation of files
associating metadata information to them."
    :licence "LGPL"
    :depends-on (:sclf)
    :components ((:static-file "Makefile")
		 (:static-file "test.lisp")
		 (:file "metafs")))

