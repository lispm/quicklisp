;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: umlisp-tests -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          basic.lisp
;;;; Purpose:       Basic tests for UMLisp
;;;; Author:        Kevin M. Rosenberg
;;;; Date Started:  May 2003
;;;;
;;;; $Id$
;;;;
;;;; This file, part of UMLisp, is
;;;;    Copyright (c) 2000-2002 by Kevin M. Rosenberg, M.D.
;;;;
;;;; UMLisp users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License.
;;;; *************************************************************************

(in-package #:umlisp-tests)

(defvar *rt-basic* nil)
(defvar *rt-parse* nil)
(defvar *error-count* 0)
(defvar *report-stream* *standard-output*)

(setq regression-test::*catch-errors* nil)

(defun run-tests ()
  (regression-test:rem-all-tests)
  (dolist (test-form (append *rt-basic* *rt-parse*))
    (eval test-form))
  (let ((remaining (regression-test:do-tests *report-stream*)))
    (when (regression-test:pending-tests)
      (incf *error-count* (length remaining))))
  *error-count*)
