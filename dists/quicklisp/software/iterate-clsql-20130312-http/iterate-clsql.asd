(defsystem iterate-clsql
  :name "iterate-clsql"
  :author "Kalyanov Dmitry <mo3r@nm.ru>"
  :version "0.2"
  :license "Public Domain"
  :description "Extension for iterate to support iterate over clsql queries"
  :components ((:file "iterate-clsql"))
  :depends-on (:clsql :iterate))