;; PEEK-CHAR is buggy in CMUCL 19b

(in-package :cl-user)
(ext:without-package-locks
  #+cmu
  (unless (member :gray-streams *features*)
    (load "library:subsystems/gray-streams-library")))

(in-package :lisp)
(ext:without-package-locks
  (defun peek-char (&optional (peek-type nil) (stream *standard-input*)
		    (eof-errorp t) eof-value recursive-p)
    "Peeks at the next character in the input Stream.  See manual for details."
    ;; FIXME: The type of PEEK-TYPE is also declared in a DEFKNOWN, but
    ;; the compiler doesn't seem to be smart enough to go from there to
    ;; imposing a type check. Figure out why (because PEEK-TYPE is an
    ;; &OPTIONAL argument?) and fix it, and then this explicit type
    ;; check can go away.
    (unless (typep peek-type '(or character boolean))
      (error 'simple-type-error
	     :datum peek-type
	     :expected-type '(or character boolean)
	     :format-control "~@@<bad PEEK-TYPE=~S, ~_expected ~S~:>"
	     :format-arguments (list peek-type '(or character boolean))))
    (let ((stream (in-synonym-of stream)))
      (if (typep stream 'echo-stream)
	  (echo-misc stream :peek-char peek-type (list eof-errorp eof-value))
	  (stream-dispatch stream
			   ;; simple-stream
			   (stream::%peek-char stream peek-type eof-errorp eof-value
					       recursive-p)
			   ;; lisp-stream
			   (generalized-peeking-mechanism
			    peek-type eof-value char
			    (read-char stream eof-errorp :eof)
			    :eof
			    (unread-char char stream)
			    nil
			    (eof-or-lose stream (or eof-errorp recursive-p) eof-value))
			   ;; fundamental-stream
			   (generalized-peeking-mechanism
			    peek-type :eof char
			    (if (null peek-type)
				(stream-peek-char stream)
				(stream-read-char stream))
			    :eof
			    (if (null peek-type)
				()
				(stream-unread-char stream char))
			    ()
			    (eof-or-lose stream eof-errorp eof-value)))))))
