(in-package #:simpsamp)

(defmacro do-sample-of-list ((var n-form list-form
			      &rest kw-args &key &allow-other-keys)
			     &body body)
  (jpl-util:with-gensyms (n% next% size% exhausted?%)
    `(let ((,n% ,n-form))
       (with-list-iterator (,next% ,size% ,exhausted?% ,list-form)
	 (do-sample-of-iterator-expr ((,var) ,n% (,size%) (,next%) ,@kw-args)
	   ,@body)))))

(defmacro do-sample-of-vector ((var n-form vector-form
				&rest kw-args &key &allow-other-keys)
			       &body body)
  (jpl-util:with-gensyms (n% vector% i%)
    `(let ((,n% ,n-form)
	   (,vector% ,vector-form))
       (unless (typep ,vector% 'vector)
	 (error "VECTOR-FORM must have a vector value, but its ~
                 actual value is ~S."
		,vector%))
       (do-sample-of-range (,i% ,n% 0 (length ,vector%) ,@kw-args)
	 (let ((,var (aref ,vector% ,i%)))
	   ,@body)))))

(defmacro do-sample-of-hash-table ((key-var value-var n-form hash-table-form
				    &rest kw-args &key &allow-other-keys)
				   &body body)
  (jpl-util:with-gensyms (n% hash-table% next% entry-p%)
    `(let ((,n% ,n-form)
	   (,hash-table% ,hash-table-form))
       (with-hash-table-iterator (,next% ,hash-table%)
	 (do-sample-of-iterator-expr ((,entry-p% ,key-var ,value-var) ,n%
				      (hash-table-count ,hash-table%) (,next%)
				      ,@kw-args)
	   ,@body)))))

(defmacro do-sample-of-range ((var n-form lower-form upper-form
			       &rest kw-args &key &allow-other-keys)
			      &body body)
  ;; FIXME: for sampling from a range of integers, I think Method D
  ;; described in "An Efficient Algorithm for Sequential Random
  ;; Sampling" (1987) by Jeffrey Scott Vitter
  ;; <http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.16.4335>
  ;; might be applicable and might run in time proportional to the
  ;; sample size, rather than the input set size.  Double-check
  ;; whether this is true and consider implementing it.  It looks like
  ;; implementation could be complicated by concerns of floating-point
  ;; accuracy.
  (jpl-util:with-gensyms (n% next% size% exhausted?%)
    `(let ((,n% ,n-form))
       (with-range-iterator (,next% ,size% ,exhausted?% ,lower-form ,upper-form)
	 (do-sample-of-iterator-expr ((,var) ,n% (,size%) (,next%) ,@kw-args)
	   ,@body)))))

(defmacro do-sample-of-iterator ((vars n-form size-form next-fn-form
				  &rest kw-args &key &allow-other-keys)
				   &body body)
  (jpl-util:with-gensyms (n% size% next-fn%)
    `(let ((,n% ,n-form)
	   (,size% ,size-form)
	   (,next-fn% ,next-fn-form))
       (do-sample-of-iterator-expr (,vars ,n% ,size% (funcall ,next-fn%)
				    ,@kw-args)
	 ,@body))))

(defmacro do-sample-of-unbounded-iterator ((var n-form next-fn-form exhausted?-fn-form
					    &key (effective-n-var nil effective-n-var-p)
					    (initial-form nil initial-form-p)
					    (result-form nil result-form-p))
					   &body body)
  (jpl-util:with-gensyms (selection% i%)
    `(loop with ,selection% = (vector-sample-of-unbounded-iterator
			       ,n-form ,next-fn-form ,exhausted?-fn-form)
	   ,@(when effective-n-var-p
	       `(with ,effective-n-var = (length ,selection%)))
	   ,@(when initial-form-p
	       `(initially ,initial-form))
	   for ,i% fixnum below (length ,selection%)
	   for ,var = (aref ,selection% ,i%)
	   doing (progn ,@body)
	   finally (return ,(if result-form-p
				result-form
				'(values))))))
