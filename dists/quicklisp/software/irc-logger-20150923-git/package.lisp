;;;;  -*- Mode: Lisp -*-
;;;; $Id: logger.lisp,v 1.11 2003/12/16 21:19:56 krosenberg Exp $
;;;;

(in-package #:cl-user)

(defpackage #:irc-logger
  (:use #:common-lisp #:irc #:cl-ppcre)
  (:export #:add-logger
           #:remove-logger
           #:add-channel-logger
           #:remove-channel-logger
           #:log-file-path
           #:add-hook-logger
           #:remove-hook-logger
           #:*loggers*))

