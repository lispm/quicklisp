;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: umlisp -*-
;;;; *************************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:     utils.lisp
;;;; Purpose:  Low-level utility functions for UMLisp
;;;; Author:   Kevin M. Rosenberg
;;;; Created:  Apr 2000
;;;;
;;;; $Id$
;;;;
;;;; This file, part of UMLisp, is
;;;;    Copyright (c) 2000-2004 by Kevin M. Rosenberg, M.D.
;;;;
;;;; UMLisp users are granted the rights to distribute and use this software
;;;; as governed by the terms of the GNU General Public License.
;;;; *************************************************************************

(in-package #:umlisp-orf)

(declaim (inline make-cuisui make-cuilui parse-ui parse-cui))

(defmacro def-metaclass-reader (field)
  "Create function for reading slot of metaclass"
  `(defun ,field (cl)
     (car (slot-value (class-of cl) ',field))))

(defmacro def-metaclass-reader-car (field)
  "Create function for reading slot of metaclass"
  `(defun ,field (cl)
     (car (slot-value (class-of cl) ',field))))

;;; Field transformations

(defun parse-ui (s &optional (nullvalue 0))
  "Return integer value for a UMLS unique identifier."
  (declare (simple-string s)
           (optimize (speed 3) (safety 0)))
  (if (< (length s) 2)
      nullvalue
    (nth-value 0 (parse-integer s :start 1))))

(defun parse-cui (cui)
  (declare (optimize (speed 3) (safety 0)))
  (if (stringp cui)
      (let ((ch (schar cui 0)))
        (if (char-equal ch #\C)
            (parse-ui cui)
            (nth-value 0 (parse-integer cui))))
    cui))

(defun parse-lui (lui)
  (declare (optimize (speed 3) (safety 0)))
  (if (stringp lui)
      (let ((ch (schar lui 0)))
        (if (char-equal ch #\L)
            (parse-ui lui)
            (nth-value 0 (parse-integer lui))))
    lui))

(defun parse-sui (sui)
  (declare (optimize (speed 3) (safety 0)))
  (if (stringp sui)
      (let ((ch (schar sui 0)))
        (if (char-equal ch #\S)
            (parse-ui sui)
            (nth-value 0 (parse-integer sui))))
    sui))

(defun parse-tui (tui)
  (declare (optimize (speed 3) (safety 0)))
  (if (stringp tui)
      (let ((ch (schar tui 0)))
        (if (char-equal ch #\T)
            (parse-ui tui)
            (nth-value 0 (parse-integer tui))))
    tui))

(defun parse-eui (eui)
  (declare (optimize (speed 3) (safety 0)))
  (if (stringp eui)
      (let ((ch (schar eui 0)))
        (if (char-equal ch #\E)
            (parse-ui eui)
            (nth-value 0 (parse-integer eui))))
    eui))

(defconstant +cuisui-scale+ 10000000)
(declaim (type fixnum +cuisui-scale+))

(defun make-cuisui (cui sui)
  (declare (fixnum cui sui)
           (optimize (speed 3) (safety 0) (space 0)))
  (+ (* +cuisui-scale+ cui) sui))

(defun make-cuilui (cui lui)
  (declare (fixnum cui lui)
           (optimize (speed 3) (safety 0) (space 0)))
  (+ (* +cuisui-scale+ cui) lui))

(defun decompose-cuisui (cuisui)
  "Returns the CUI and SUI of a cuisui number"
  (floor cuisui +cuisui-scale+))

;;; Lookup functions for uterms,ustr in ucons

(defun find-uterm-in-ucon (ucon lui)
  (find lui (s#term ucon) :key #'lui :test 'equal))

(defun find-ustr-in-uterm (uterm sui)
  (find sui (s#str uterm) :key #'sui :test 'equal))

(defun find-ustr-in-ucon (ucon sui)
  (dolist (uterm (s#term ucon))
    (dolist (ustr (s#str uterm))
      (when (string-equal sui (sui ustr))
        (return-from find-ustr-in-ucon ustr)))))
